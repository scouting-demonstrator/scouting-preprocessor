# 40 MHz scouting preprocessor

This board receives trigger data via optical links @ 10 Gb/s and provides their data to a CPU-based compute system.



## Project structure

The project structure is inspired by the the emp-framework (https://gitlab.cern.ch/p2-xware/firmware/emp-fwk). There are 4 main subdirectories:
* **`projects`**: Structured as projects/$board/$algo. It contains the top-level `.dep` and VHDL files defining entities and parameter values that are unique to a particular project.

* **`boards`**: It contains board-specific source code and constraints (one subdirectory per board). The supported boards employed up to now are:
  * `kcu1500`
  * `sb852`
  * `vcu128`

* **`components`**: It contains the source code of the components instantiated in the projects. 

* **`scripts`**: It contains the python/bash/tcl scripts to:
  * Build the projects
  * Perform operations on the board like loading the bitfile or reset
  * Generate the vhdl address table for SCONE

  The board-independent scripts are placed in the subsubdirectory `common`, while board specific scripts are placed in `scripts/$BOARD/`
<center>
    <img src="./project-structure.png" alt="Drawing" style="width: 400px"/>
</center>

Each individual board/component folder can have the following structure:

| Subdirectory   | Contents                                                                         |
|----------------|----------------------------------------------------------------------------------|
| `addrtab`      | Address table `json` files to generate the scouting registers package for SCONE  |
| `firmware/hdl` | Firmware source code (e.g. Verilog/VHDL)                                         |
| `firmware/cfg` | `.dep` files and `.tcl` scripts that should be run when creating vivado projects |
| `firmware/ucf` | Constraints files                                                                |



## Requirements

`ipbb` version 1.0a0+2022.dev3 is recommended. It must be in the path to excecute the make project and build scripts.  

The `ipbb` build environment requires Python 3, pip, and virtualenv to run. For the firmware build itself Xilinx Vivado at least 2019.2.



## Build instructions

* First remember to set your `BUILD_DIR` of choice.

* From the scouting-preprocessor directory level the project can be set up with:

  ```bash
  scripts/$BOARD/makeScout[Demux|Ugmt|Bril]Project.sh
  ```
  
  for the UGMT, Calo demux, and Bril versions of the code.
  The script will also generate the VHDL address table from the board specific json address table by calling `scripts/common/generate_decoder.py`

* A bitfile can be generated in the following way:

  ```bash
  scripts/buildFirmware.sh
  ```

  It can then be found in the folder `build/scouting/proj/scouting_build/package`.

*Note:* If the build scripts find the file `buildToolSetup.sh` in the base directory they will source it. It can be used to set the environment variables required for Vivado and Modelsim. A template (`buildToolSetup_template.sh`) is provided.



## To use simulation testbench

* For KCU1500 simply set `SIM = true` inside your `top_decl.vhd` file and run with the standard `kcu1500_top.vhd` as the simulation top level.
* For SB852 set `SIM = true` inside `top_decl.vhd` and comment in/out a few top level signals that can be found by searching SIMULATION in `sb852_user_top.vhd`. This includes the process runtb. You should now be ready to simulate.
* For VCU128 the simulation is currently in a seperate top file 


## Versioning

The scouting boards follow semantic versioning for which the 24 LSBs of the algorithm rev field are used. These are segmented into three eight bit fields, indicating major, minor, patch level.

The eight MSBs indicate the scouting board type where the following codes are currently in use:
* `0x0` ... Prototype board until late 2021
* `0x1` ... uGMT inputs
* `0x2` ... Layer-2 Demux inputs
* `0x3` ... uGMT inputs, bril histogramming enabled
* `0x4` ... uGT inputs
* `0x5` ... BMTF (twinmux data) inputs
