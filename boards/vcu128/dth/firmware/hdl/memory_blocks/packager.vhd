-- packetizer
--
-- encapsulator of packet in header and trailer words
--
-- R. Ardino January 2023

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.datatypes.all;





entity packager is
    generic (
        NSTREAMS : integer := 8
    );
    port (
        d_clk                   : in  std_logic;
        rst                     : in  std_logic;

        orbits_per_packet       : in  unsigned(15 downto 0);
        orbits_per_chunk        : in  std_logic_vector(19 downto 0);
        wait_for_oc1            : in  std_logic;

        d                       : in  adata(NSTREAMS -1 downto 0);
        d_ctrl                  : in  acontrol;

        m_aclk                  : in  std_logic;
        m_axis_tvalid           : out std_logic;
        m_axis_tready           : in  std_logic;
        m_axis_tdata            : out std_logic_vector(255 downto 0);
        m_axis_tkeep            : out std_logic_vector( 31 downto 0);
        m_axis_tlast            : out std_logic := '0';
        m_axis_tuser            : out std_logic_vector(  1 downto 0);

        dropped_orbit_counter   : out unsigned(63 downto 0);
        orbit_counter           : out unsigned(63 downto 0);
        axi_backpressure_seen   : out std_logic;
        orbit_length            : out unsigned(63 downto 0);
        orbit_length_cnt        : out unsigned(63 downto 0);
        orbit_exceeds_size      : out std_logic;
        in_autorealign_counter  : in  unsigned(63 downto 0);
        source_id               : in  std_logic_vector(31 downto 0);

        HBM_almost_full         : in  std_logic;

        -- header output
        header_dout_o           : out std_logic_vector(255 downto 0);
        header_dout_d1_o        : out std_logic_vector(255 downto 0);
        header_dout_d2_o        : out std_logic_vector(255 downto 0);
        header_dout_d3_o        : out std_logic_vector(255 downto 0);
        header_dout_d4_o        : out std_logic_vector(255 downto 0);
        header_dout_d5_o        : out std_logic_vector(255 downto 0);
        header_dout_d6_o        : out std_logic_vector(255 downto 0);
        header_wen_o            : out std_logic;
        header_wen_d1_o         : out std_logic;
        header_wen_d2_o         : out std_logic;
        header_wen_d3_o         : out std_logic;
        header_wen_d4_o         : out std_logic;
        header_wen_d5_o         : out std_logic;
        header_wen_d6_o         : out std_logic;

        -- fragment header output
        frag_header_dout_o      : out std_logic_vector(255 downto 0);
        frag_header_dout_d1_o   : out std_logic_vector(255 downto 0);
        frag_header_dout_d2_o   : out std_logic_vector(255 downto 0);
        frag_header_dout_d3_o   : out std_logic_vector(255 downto 0);
        frag_header_dout_d4_o   : out std_logic_vector(255 downto 0);
        frag_header_dout_d5_o   : out std_logic_vector(255 downto 0);
        frag_header_dout_d6_o   : out std_logic_vector(255 downto 0);
        frag_header_wen_o       : out std_logic;
        frag_header_wen_d1_o    : out std_logic;
        frag_header_wen_d2_o    : out std_logic;
        frag_header_wen_d3_o    : out std_logic;
        frag_header_wen_d4_o    : out std_logic;
        frag_header_wen_d5_o    : out std_logic;
        frag_header_wen_d6_o    : out std_logic

    );
end packager;





architecture Behavioral of packager is

    -----------------------------------------------------------------------------
    --
    -- Signal declaration
    --
    -----------------------------------------------------------------------------
    -- input data and control buffer
    signal d1                       : adata(NSTREAMS -1 downto 0);
    signal d_ctrl_d1                : acontrol;
    signal d_ctrl_d2                : acontrol;
    signal d_ctrl_d3                : acontrol;

    -- input to fifo chain
    signal s_axis_tvalid            : std_logic;
    signal s_axis_tready            : std_logic;
    signal s_axis_tlast             : std_logic;
    signal s_axis_tdata             : std_logic_vector(255 downto 0);
    signal s_axis_tuser             : std_logic_vector(  1 downto 0);
    signal s_axis_tvalid_reg        : std_logic;
    signal s_axis_tlast_reg         : std_logic;
    signal s_axis_tdata_reg         : std_logic_vector(255 downto 0);
    signal s_axis_tuser_reg         : std_logic_vector(  1 downto 0);

    -- filler state type and signal
    type t_FillerState is (FILLING, DROPPING, STARTING);
    signal fillerState : t_FillerState := STARTING;

    ---- counters
    -- orbits dropped (HBM almost full)
    signal s_dropped_orbit_counter  : unsigned(63 downto 0);
    -- orbits seen
    signal s_orbit_counter          : unsigned(63 downto 0);
    -- orbit length in frames
    signal orbit_length_int         : unsigned(63 downto 0);
    signal s_orbit_length_cnt       : unsigned(63 downto 0);
    -- orbits in packet
    signal orbits_in_packet         : unsigned(15 downto 0);
    signal dropped_orbits_in_packet : unsigned(15 downto 0);
    -- packet length in frames
    signal packet_length_int        : unsigned(63 downto 0);
    signal packet_length            : unsigned(63 downto 0);
    -- bx in orbit
    signal bx_counter_int           : unsigned(15 downto 0);
    signal bx_counter               : unsigned(15 downto 0);

    -- orbit number
    signal orbit_number             : std_logic_vector(31 downto 0);

    -- backpressure and rst_n
    signal s_axi_backpressure_seen  : std_logic;
    signal rst_n                    : std_logic;

    -- orbits per chunk
    signal orbits_per_chunk_mask    : std_logic_vector(19 downto 0);

    -- wait for oc1
    signal oc1_found                : std_logic := '0';

    -- packet header
    signal header_dout              : std_logic_vector(255 downto 0) := (others => '0');
    signal header_dout_d1           : std_logic_vector(255 downto 0) := (others => '0');
    signal header_dout_d2           : std_logic_vector(255 downto 0) := (others => '0');
    signal header_dout_d3           : std_logic_vector(255 downto 0) := (others => '0');
    signal header_dout_d4           : std_logic_vector(255 downto 0) := (others => '0');
    signal header_dout_d5           : std_logic_vector(255 downto 0) := (others => '0');
    signal header_dout_d6           : std_logic_vector(255 downto 0) := (others => '0');
    signal header_wen               : std_logic                      := '0';
    signal header_wen_d1            : std_logic                      := '0';
    signal header_wen_d2            : std_logic                      := '0';
    signal header_wen_d3            : std_logic                      := '0';
    signal header_wen_d4            : std_logic                      := '0';
    signal header_wen_d5            : std_logic                      := '0';
    signal header_wen_d6            : std_logic                      := '0';

    -- fragment header
    signal frag_header_dout         : std_logic_vector(255 downto 0) := (others => '0');
    signal frag_header_dout_d1      : std_logic_vector(255 downto 0) := (others => '0');
    signal frag_header_dout_d2      : std_logic_vector(255 downto 0) := (others => '0');
    signal frag_header_dout_d3      : std_logic_vector(255 downto 0) := (others => '0');
    signal frag_header_dout_d4      : std_logic_vector(255 downto 0) := (others => '0');
    signal frag_header_dout_d5      : std_logic_vector(255 downto 0) := (others => '0');
    signal frag_header_dout_d6      : std_logic_vector(255 downto 0) := (others => '0');
    signal frag_header_wen          : std_logic                      := '0';
    signal frag_header_wen_d1       : std_logic                      := '0';
    signal frag_header_wen_d2       : std_logic                      := '0';
    signal frag_header_wen_d3       : std_logic                      := '0';
    signal frag_header_wen_d4       : std_logic                      := '0';
    signal frag_header_wen_d5       : std_logic                      := '0';
    signal frag_header_wen_d6       : std_logic                      := '0';
    -----------------------------------------------------------------------------

begin

    with orbits_per_chunk select
        orbits_per_chunk_mask <= x"00000" when x"00001",
                                 x"00001" when x"00002",
                                 x"00003" when x"00004",
                                 x"00007" when x"00008",
                                 x"0000f" when x"00010",
                                 x"0001f" when x"00020",
                                 x"0003f" when x"00040",
                                 x"0007f" when x"00080",
                                 x"000ff" when x"00100",
                                 x"001ff" when x"00200",
                                 x"003ff" when x"00400",
                                 x"007ff" when x"00800",
                                 x"00fff" when x"01000",
                                 x"01fff" when x"02000",
                                 x"03fff" when x"04000",
                                 x"07fff" when x"08000",
                                 x"0ffff" when x"10000",
                                 x"1ffff" when x"20000",
                                 x"3ffff" when x"40000",
                                 x"7ffff" when x"80000",
                                 x"00000" when others;

    filler : process (d_clk) is
        variable b_orbit_exceeds_size : boolean;
        variable check_not_zero       : std_logic;
        variable check_mult_chunk     : std_logic;
        variable check_oc1            : std_logic;
    begin
        if (d_clk'event and d_clk = '1') then
            if (rst = '1') then
                -- after reset, the FSM should wait for chunk start
                -- thus we go into STARTING state after reset
                fillerState             <= STARTING;
                -- orbit counters
                s_orbit_counter         <= to_unsigned(0, 64);
                s_dropped_orbit_counter <= to_unsigned(0, 64);
                -- orbits per packet
                orbits_in_packet        <= to_unsigned(0, 16);
                dropped_orbits_in_packet<= to_unsigned(0, 16);
                -- backpressure
                s_axi_backpressure_seen <= '0';
                -- orbit length
                orbit_exceeds_size      <= '0';
                orbit_length_int        <= to_unsigned(0, 64);
                s_orbit_length_cnt      <= to_unsigned(0, 64);
                -- packet length
                packet_length_int       <= to_unsigned(0, 64);
                -- bx in orbit
                bx_counter_int          <= to_unsigned(0, 16);
                -- orbit number
                orbit_number            <= (others => '0');
                -- data
                s_axis_tdata            <= (others => '0');
                s_axis_tuser            <= (others => '0');
                s_axis_tlast            <= '0';
                s_axis_tvalid           <= '0';
                -- packet header
                header_wen              <= '0';
                header_dout             <= (others => '0');
                -- fragment header
                frag_header_wen         <= '0';
                frag_header_dout        <= (others => '0');
                -- oc1 found
                oc1_found               <= '0';
                --
                b_orbit_exceeds_size    := false;

            else

                -----------------------------------------------------------------
                --
                -- Orbit length counters start/reset/update
                --
                -----------------------------------------------------------------
                -- buffer data to detect valid transitions
                d1        <= d;
                d_ctrl_d1 <= d_ctrl;
                d_ctrl_d2 <= d_ctrl_d1;
                d_ctrl_d3 <= d_ctrl_d2;


                -- register outputs so that timing is easier to meet
                s_axis_tvalid_reg <= s_axis_tvalid;
                s_axis_tlast_reg  <= s_axis_tlast;
                s_axis_tdata_reg  <= s_axis_tdata;
                s_axis_tuser_reg  <= s_axis_tuser;


                -- by default, disable write header
                -- enable it only when necessary
                -- => pulse of 1 clk cycle for enable
                header_wen       <= '0';
                header_dout      <= (others => '0');
                frag_header_wen  <= '0';
                frag_header_dout <= (others => '0');

                -- START of orbit
                --         d3  d2  d1  d0
                -- header   x   0   1   0
                if d_ctrl_d1.header = '1' then
                    -- start counters from 1
                    orbit_length_int   <= to_unsigned(1, 64);
                    s_orbit_length_cnt <= to_unsigned(1, 64);
                    orbit_number       <= d1(0);
                end if;


                -- ON-GOING orbit
                --         d3  d2  d1  d0
                -- valid    x   1   1   x
                -- strobe   x   x   1   x
                if d_ctrl_d1.valid = '1' and d_ctrl_d2.valid = '1' and d_ctrl_d1.strobe = '1' then
                    -- increase counters
                    orbit_length_int   <= orbit_length_int   + 1;
                    s_orbit_length_cnt <= s_orbit_length_cnt + 1;

                    if d_ctrl_d2.bx_start = '1' then
                        bx_counter_int <= bx_counter_int + 1;
                    end if;

                    -- check if orbit length exceeds size
                    if orbit_length_int > 21600 then
                        orbit_exceeds_size   <= '1';
                        b_orbit_exceeds_size := true;
                    else
                        orbit_exceeds_size   <= '0';
                        b_orbit_exceeds_size := false;
                    end if;
                end if;


                -- END of orbit
                --         d3  d2  d1  d0
                -- last     0   1   0   x
                if d_ctrl_d2.last = '1' then
                    orbit_length         <= orbit_length_int;
                    orbit_length_int     <= to_unsigned(0, 64);
                    s_orbit_length_cnt   <= to_unsigned(0, 64);
                    bx_counter           <= bx_counter_int;
                    bx_counter_int       <= to_unsigned(0, 16);
                    orbit_exceeds_size   <= '0';
                    b_orbit_exceeds_size := false;
                end if;
                -----------------------------------------------------------------





                -----------------------------------------------------------------
                --
                -- FSM to fill/drop data
                --
                -----------------------------------------------------------------
                case fillerState is

                    ---- FSM in FILLER state
                    when FILLING =>

                        -- toggle backpressure flag
                        if s_axis_tvalid_reg = '1' and s_axis_tready = '0' then
                            s_axi_backpressure_seen <= '1';
                        end if;

                        s_axis_tvalid <= d_ctrl_d1.valid and d_ctrl_d1.strobe;

                        s_axis_tdata  <= (others => '0');
                        for i in d'range loop
                            s_axis_tdata(i*32 + 31 downto i*32) <= d1(i);
                        end loop;
                        s_axis_tlast  <= '0';
                        s_axis_tuser  <= "11"; -- data
                        -- last word of orbit trailer
                        --         d3  d2  d1  d0
                        -- last     x   0   1   0
                        if d_ctrl_d1.last = '1' and (orbits_in_packet < orbits_per_packet-1) then
                            s_axis_tlast  <= '1';
                            s_axis_tuser  <= "01";
                        end if;


                        -- packet length in frames and packet end
                        -- START of orbit
                        --         d3  d2  d1  d0
                        -- header   x   0   1   0
                        if d_ctrl_d1.header = '1' then
                            -- start of packet
                            if std_logic_vector(orbits_in_packet) = x"0000" then
                                packet_length_int <= to_unsigned(1, 64);
                            -- else packet on-going
                            else
                                packet_length_int <= packet_length_int + 1;
                            end if;

                        -- ON-GOING packet (and orbit)
                        --         d3  d2  d1  d0
                        -- valid    x   x   1   x
                        -- strobe   x   x   1   x
                        -- header   x   x   0   x
                        -- last     x   x   x   x
                        elsif d_ctrl_d1.valid = '1' and d_ctrl_d1.strobe = '1' then
                            packet_length_int <= packet_length_int + 1;

                        -- END of orbit
                        --         d3  d2  d1  d0
                        -- valid    x   1   x   x
                        -- strobe   x   1   x   x
                        -- header   x   0   0   x
                        -- last     0   1   0   x
                        elsif d_ctrl_d2.last = '1' then
                            -- ON-GOING packet
                            s_orbit_counter <= s_orbit_counter + 1;
                            if (orbits_in_packet < orbits_per_packet-1) then
                                orbits_in_packet <= orbits_in_packet + 1;
                            -- END of packet
                            else
                                -- create packet trailer and write to fifo chain
                                s_axis_tvalid     <= '1';
                                s_axis_tlast      <= '1';
                                s_axis_tuser      <= "01"; -- trailer
                                s_axis_tdata      <= std_logic_vector(s_orbit_counter + 1)          &   -- 64b orbits seen counter
                                                     std_logic_vector(s_dropped_orbit_counter)      &   -- 64b orbits dropped counter
                                                     std_logic_vector(in_autorealign_counter)       &   -- 64b autorealign trigger counter
                                                     x"deadbeefdeadbeef";                               -- 64b end of packet marker

                                -- update counters
                                packet_length     <= packet_length_int;
                                packet_length_int <= to_unsigned(0, 64);

                                -- create packet header and send out
                                header_dout       <= x"00000000"                                            &   -- 32b empty
                                                     std_logic_vector(dropped_orbits_in_packet)             &   -- 16b dropped orbits in packet
                                                     std_logic_vector(orbits_in_packet + 1)                 &   -- 16b orbits in packet
                                                     std_logic_vector(s_orbit_counter(31 downto 0) + 1)     &   -- 32b orbits seen counter
                                                     std_logic_vector(s_dropped_orbit_counter(31 downto 0)) &   -- 32b orbits dropped counter
                                                     std_logic_vector(packet_length_int(31 downto 0))       &   -- 32b packet length in frames
                                                     source_id                                              &   -- 32b scouting source id
                                                     x"feedbeeffeedbeef";                                       -- 64b start of packet marker
                                header_wen        <= '1';

                                -- update seen/dropped orbit in packet counters
                                orbits_in_packet         <= to_unsigned(0, 16);
                                dropped_orbits_in_packet <= to_unsigned(0, 16);
                            end if;

                            -- create fragment header and send out
                            frag_header_dout  <= std_logic_vector(s_orbit_counter + 1)              &   -- 64b orbits seen counter
                                                 std_logic_vector(s_dropped_orbit_counter)          &   -- 64b orbits dropped counter
                                                 orbit_number                                       &   -- 32b
                                                 x"0000"                                            &   -- 16b not yet used
                                                 std_logic_vector(bx_counter_int)                   &   -- 16b orbit length in BXs
                                                 std_logic_vector(orbit_length_int(31 downto 0))    &   -- 32b orbit length in frames
                                                 x"0000"                                            &   -- 16b flags (0x0001 -> dropped orbit)
                                                 x"4648";                                               -- 16b start of fragment marker
                            frag_header_wen   <= '1';

                            -- check if fifo is full to pass to DROPPING STATE
                            if HBM_almost_full = '1' then
                                fillerState <= DROPPING;
                            end if;
                        end if;



                    ---- FSM in DROPPING state
                    when DROPPING =>
                        -- send null and invalid data
                        s_axis_tvalid <= '0';
                        s_axis_tlast  <= '0';
                        s_axis_tuser  <= "00";
                        s_axis_tdata  <= (others => '0');

                        -- toggle backpressure flag
                        if s_axis_tvalid_reg = '1' and s_axis_tready = '0' then
                            s_axi_backpressure_seen <= '1';
                        end if;

                        -- START of orbit and check if HBM is not full to go into FILLING state
                        --         d3  d2  d1  d0
                        -- header   x   0   0   1
                        if d_ctrl.header = '1' then
                            if HBM_almost_full = '0' then
                                fillerState <= FILLING;
                            end if;
                        end if;

                        -- creation of dummy orbit in packet
                        -- packet length in frames and packet end
                        -- START of orbit
                        --         d3  d2  d1  d0
                        -- header   x   0   1   0
                        if d_ctrl_d1.header = '1' then
                            -- start of packet
                            if std_logic_vector(orbits_in_packet) = x"0000" then
                                packet_length_int <= to_unsigned(1, 64);
                            -- else packet on-going
                            else
                                packet_length_int <= packet_length_int + 1;
                            end if;
                            -- write only header and consider it as a trailer
                            s_axis_tvalid <= '1';
                            s_axis_tlast  <= '1';
                            if (orbits_in_packet < orbits_per_packet-1) then
                                s_axis_tuser  <= "01";
                            else
                                s_axis_tuser  <= "11";
                            end if;
                            -- write orbit number on data frame
                            for i in d'range loop
                                s_axis_tdata(i*32 + 31 downto i*32) <= d1(0);
                            end loop;

                        -- END of orbit
                        --         d3  d2  d1  d0
                        -- last     0   1   0   x
                        elsif d_ctrl_d2.last = '1' then
                            s_orbit_counter         <= s_orbit_counter + 1;
                            s_dropped_orbit_counter <= s_dropped_orbit_counter + 1;
                            if HBM_almost_full = '1' then
                                fillerState <= DROPPING;
                            end if;

                            -- ON-GOING packet
                            if (orbits_in_packet < orbits_per_packet-1) then
                                dropped_orbits_in_packet <= dropped_orbits_in_packet + 1;
                                orbits_in_packet         <= orbits_in_packet + 1;
                            -- END of packet
                            else
                                -- create packet trailer and write to fifo chain
                                s_axis_tvalid     <= '1';
                                s_axis_tlast      <= '1';
                                s_axis_tuser      <= "01"; -- trailer
                                s_axis_tdata      <= std_logic_vector(s_orbit_counter + 1)          &   -- 64b orbits seen counter
                                                     std_logic_vector(s_dropped_orbit_counter + 1)  &   -- 64b orbits dropped counter
                                                     std_logic_vector(in_autorealign_counter)       &   -- 64b autorealign trigger counter
                                                     x"deadbeefdeadbeef";                               -- 64b end of packet marker

                                -- update packet length counters
                                packet_length     <= packet_length_int;
                                packet_length_int <= to_unsigned(0, 64);

                                -- create packet header and send out
                                header_dout       <= x"00000000"                                                &   -- 32b empty
                                                     std_logic_vector(dropped_orbits_in_packet + 1)             &   -- 16b dropped orbits in packet
                                                     std_logic_vector(orbits_in_packet + 1)                     &   -- 16b orbits in packet
                                                     std_logic_vector(s_orbit_counter(31 downto 0) + 1)         &   -- 32b orbits seen counter
                                                     std_logic_vector(s_dropped_orbit_counter(31 downto 0) + 1) &   -- 32b orbits dropped counter
                                                     std_logic_vector(packet_length_int(31 downto 0))           &   -- 32b packet length in frames
                                                     source_id                                                  &   -- 32b scouting source id
                                                     x"feedbeeffeedbeef";                                           -- 64b start of packet marker
                                header_wen        <= '1';

                                -- update seen/dropped orbit in packet counters
                                orbits_in_packet         <= to_unsigned(0, 16);
                                dropped_orbits_in_packet <= to_unsigned(0, 16);
                            end if;

                            -- create fragment header and send out
                            frag_header_dout  <= std_logic_vector(s_orbit_counter + 1)              &   -- 64b orbits seen counter
                                                 std_logic_vector(s_dropped_orbit_counter + 1)      &   -- 64b orbits dropped counter
                                                 orbit_number                                       &   -- 32b
                                                 x"0000"                                            &   -- 16b not yet used
                                                 std_logic_vector(bx_counter_int)                   &   -- 16b orbit length in BXs
                                                 std_logic_vector(orbit_length_int(31 downto 0))    &   -- 32b orbit length in frames
                                                 x"0001"                                            &   -- 16b flags (0x0001 -> dropped orbit)
                                                 x"4648";                                               -- 16b start of fragment marker
                            frag_header_wen   <= '1';
                        end if;



                    ---- FSM in STARTING state
                    when STARTING =>
                        -- send null and invalid data
                        s_axis_tvalid <= '0';
                        s_axis_tlast  <= '0';
                        s_axis_tuser  <= "00";
                        s_axis_tdata  <= (others => '0');

                        -- START of orbit and detect chunk start
                        --         d3  d2  d1  d0
                        -- header   x   0   0   1
                        if d_ctrl.header = '1' then
                            -- check if 31 bits orbit number is not null and
                            -- start filling (or dropping) if the orbit is the start of the chunk
                            check_not_zero   := or d(0)(30 downto 0);
                            check_mult_chunk := not (or (d(0)(19 downto 0) and orbits_per_chunk_mask));

                            -- if requested, check if orbit 1 has been met
                            -- if not requested, don't care
                            if wait_for_oc1 and (not oc1_found) then
                                check_oc1 := (not (or d(0)(30 downto 1))) and d(0)(0);  -- check if only first bit is equal to 1
                                if check_oc1 then
                                    oc1_found <= '1';
                                end if;
                            else
                                check_oc1 := '1';
                            end if;

                            -- decide if staying in STARTING or DROPPING state depending on the previous conditions
                            if check_not_zero and check_mult_chunk and check_oc1 then
                                if HBM_almost_full = '0' then
                                    fillerState <= FILLING;
                                else -- TODO: this case should not happen, but we leave it hear as a protection
                                    fillerState <= DROPPING;
                                end if;
                            else
                                fillerState <= STARTING;
                            end if;
                        end if;

                end case;
                -----------------------------------------------------------------





                -----------------------------------------------------------------
                --
                -- Delay headers
                --
                -----------------------------------------------------------------
                header_dout_d1 <= header_dout;
                header_dout_d2 <= header_dout_d1;
                header_dout_d3 <= header_dout_d2;
                header_dout_d4 <= header_dout_d3;
                header_dout_d5 <= header_dout_d4;
                header_dout_d6 <= header_dout_d5;
                header_wen_d1  <= header_wen;
                header_wen_d2  <= header_wen_d1;
                header_wen_d3  <= header_wen_d2;
                header_wen_d4  <= header_wen_d3;
                header_wen_d5  <= header_wen_d4;
                header_wen_d6  <= header_wen_d5;

                frag_header_dout_d1 <= frag_header_dout;
                frag_header_dout_d2 <= frag_header_dout_d1;
                frag_header_dout_d3 <= frag_header_dout_d2;
                frag_header_dout_d4 <= frag_header_dout_d3;
                frag_header_dout_d5 <= frag_header_dout_d4;
                frag_header_dout_d6 <= frag_header_dout_d5;
                frag_header_wen_d1  <= frag_header_wen;
                frag_header_wen_d2  <= frag_header_wen_d1;
                frag_header_wen_d3  <= frag_header_wen_d2;
                frag_header_wen_d4  <= frag_header_wen_d3;
                frag_header_wen_d5  <= frag_header_wen_d4;
                frag_header_wen_d6  <= frag_header_wen_d5;

                m_axis_tdata  <= s_axis_tdata_reg;
                m_axis_tvalid <= s_axis_tvalid_reg;
                m_axis_tlast  <= s_axis_tlast_reg;
                m_axis_tkeep  <= (others => '1');
                m_axis_tuser  <= s_axis_tuser_reg;
                -----------------------------------------------------------------
            end if;
        end if;
    end process filler;





    -----------------------------------------------------------------------------
    --
    -- OUTPUT signals
    --
    -----------------------------------------------------------------------------
    orbit_length_cnt      <= s_orbit_length_cnt;
    orbit_counter         <= s_orbit_counter;
    dropped_orbit_counter <= s_dropped_orbit_counter;
    axi_backpressure_seen <= s_axi_backpressure_seen;

    header_dout_o         <= header_dout;
    header_dout_d1_o      <= header_dout_d1;
    header_dout_d2_o      <= header_dout_d2;
    header_dout_d3_o      <= header_dout_d3;
    header_dout_d4_o      <= header_dout_d4;
    header_dout_d5_o      <= header_dout_d5;
    header_dout_d6_o      <= header_dout_d6;
    header_wen_o          <= header_wen_d1;
    header_wen_d1_o       <= header_wen_d1;
    header_wen_d2_o       <= header_wen_d2;
    header_wen_d3_o       <= header_wen_d3;
    header_wen_d4_o       <= header_wen_d4;
    header_wen_d5_o       <= header_wen_d5;
    header_wen_d6_o       <= header_wen_d6;

    frag_header_dout_o    <= frag_header_dout;
    frag_header_dout_d1_o <= frag_header_dout_d1;
    frag_header_dout_d2_o <= frag_header_dout_d2;
    frag_header_dout_d3_o <= frag_header_dout_d3;
    frag_header_dout_d4_o <= frag_header_dout_d4;
    frag_header_dout_d5_o <= frag_header_dout_d5;
    frag_header_dout_d6_o <= frag_header_dout_d6;
    frag_header_wen_o     <= frag_header_wen_d1;
    frag_header_wen_d1_o  <= frag_header_wen_d1;
    frag_header_wen_d2_o  <= frag_header_wen_d2;
    frag_header_wen_d3_o  <= frag_header_wen_d3;
    frag_header_wen_d4_o  <= frag_header_wen_d4;
    frag_header_wen_d5_o  <= frag_header_wen_d5;
    frag_header_wen_d6_o  <= frag_header_wen_d6;
    -----------------------------------------------------------------------------

end Behavioral;
