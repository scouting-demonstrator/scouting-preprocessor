------------------------------------------------------
-- MEMORY TO STORE FRAGMENT DIVIDED IN 4KBYTES BLOCKS
-- WITH A HEADER IN THE FRONT TO SPECIFY THE CONTENT
-- OF THE BLOCK
--  Ver 6.00
--
-- Dominique Gigi JULY 2012
------------------------------------------------------
--  This file divide the fragment in blocks and add an DTH Header
--  This is not the final version . It is a first version to debug the HBM
--  It inputs data from the SR or Emulator
--  It outputs data i a 256b word and a packet size
--  Modifing to adapt to scout stream
------------------------------------------------------
LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity hbm_blocks_builder is
    generic(
        swapp : boolean := true
    );
    port(
        reset_p                     : in  std_logic;
        CLOCK                       : in  std_logic;

        -- INPUT DATA FROM SLINK 0-1
        write_dt_from_scouting      : in  std_logic;                        -- used to read the intermediate FIFO
        data_i_from_scouting        : in  std_logic_vector(255 downto 0);
        user_i_from_scouting        : in  std_logic_vector(  1 downto 0);
        uctrl_i_from_scouting       : in  std_logic;                        -- active high

        Write_Fragment_Header       : in  std_logic;
        Fragment_Header             : in  std_logic_vector(255 downto 0);
        Write_Orbit_Header          : in  std_logic;
        Orbit_Header                : in  std_logic_vector(255 downto 0);

        -- OUTPUT DATA TO Memory Stream
        Data_o_to_HBM               : out std_logic_vector(255 downto 0);
        Size_ready_to_HBM           : out std_logic_vector( 11 downto 0);   -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT Data_ready_to_HBM (4kBYTES)
        Data_Spec_to_HBM            : out std_logic_vector(  2 downto 0);
        trailer_word_to_HBM         : out std_logic;

        Data_ready_to_HBM           : out std_logic;                        -- DATA IN fifo
        Read_data_for_HBM           : in  std_logic;
        Block_read_done_for_HBM     : in  std_logic;

        internal_LFF                : out std_logic
    );
end hbm_blocks_builder;





architecture behavioral of hbm_blocks_builder is

    ----------------------------------------------------------------------------
    --
    -- Components
    --
    ----------------------------------------------------------------------------
    component memory_DP is
        port (
            resetp          : in  std_logic;
            clock           : in  std_logic;

            write_mem       : in  std_logic;
            address_wr      : in  std_logic_vector( 11 downto 0);
            data_i          : in  std_logic_vector(255 downto 0);
            data_i_extra    : in  std_logic_vector(  7 downto 0);
            Enable_words    : in  std_logic_vector(  1 downto 0);

            read_mem        : in  std_logic;
            address_rd      : in  std_logic_vector( 11 downto 0);
            data_o          : out std_logic_vector(255 downto 0);
            data_o_extra    : out std_logic_vector(  7 downto 0)
        );
    end component;


    component FIFO_block_ready
        PORT (
            clk         : in  std_logic;
            srst        : in  std_logic;
            din         : in  std_logic_vector(11 downto 0);
            wr_en       : in  std_logic;
            rd_en       : in  std_logic;
            dout        : out std_logic_vector(11 downto 0);
            full        : out std_logic;
            empty       : out std_logic;
            wr_rst_busy : out std_logic;
            rd_rst_busy : out std_logic
        );
    end component;
    ----------------------------------------------------------------------------





    ----------------------------------------------------------------------------
    --
    -- Signal declaration
    --
    ----------------------------------------------------------------------------
    signal Sync_reload                  : std_logic_vector(1 downto 0);
    signal Word_spec                    : std_logic_vector(2 downto 0);
    signal data_word                    : std_logic;
    signal Frag_HD_word                 : std_logic;
    signal Orbit_HD_word                : std_logic;

    signal wen_ff                       : std_logic;
    signal Write_block_size_last_data   : std_logic;
    signal add_wr                       : std_logic_vector(11 downto 0) := "000000000000";

    signal block_size_counter           : std_logic_vector(11 downto 0);
    signal block_size                   : std_logic_vector(11 downto 0);
    signal wr_block_size                : std_logic;
    signal FF_block_size_empty          : std_logic;

    signal diff_w_p                     : std_logic_vector(10 downto 0);
    signal rd_mem                       : STD_LOGIC;

    signal add_rd                       : std_logic_vector(11 downto 0) := (others => '0');
    signal diff_r_p                     : std_logic_vector(10 downto 0) := (others => '0');

    signal dataw_swapped                : std_logic_vector(255 downto 0);
    signal dataw                        : std_logic_vector(255 downto 0);

    signal LFF                          : std_logic;
    signal pre_empt                     : std_logic_vector(1 downto 0);
    signal Dummy                        : std_logic_vector(3 downto 0);
    signal trailer_word                 : std_logic;

    attribute mark_debug : string;
    ----------------------------------------------------------------------------

begin

    ---- resync preset for the Synchronous load
    process(reset_p,clock)
    begin
        if reset_p = '1' then
            Sync_reload     <= (others => '1');
        elsif rising_edge(clock) then
            Sync_reload(1)  <= Sync_reload(0);
            Sync_reload(0)  <= reset_p;
        end if;
    end process;



    ---- control the latest data of a block if it is not full (x0100 words)
    process(clock)
    begin
        if rising_edge(clock) then
            Write_block_size_last_data <= '0';

            -- detect the last word in Fragment (Trailer)
            if user_i_from_scouting = "01" and uctrl_i_from_scouting = '1' and write_dt_from_scouting = '1' and block_size_counter /= x"0FE" then
                Write_block_size_last_data <= '1';
            end if;
        end if;
    end process;



    ---- manage the address and the block size
    process(reset_p,clock)
    begin
        if reset_p = '1'then
            add_wr              <= (others => '0');
            block_size_counter  <= (others => '0');
        elsif rising_edge(clock) then
            if wen_ff = '1' then
                add_wr <= add_wr + '1';
            end if;

            -- reset the Block Orbit at at the end of a Fragment
            -- if Write_Orbit_Header = '1' then
            if Write_Fragment_Header = '1' then
                block_size_counter	<= (others => '0');

            -- start a new block (count only data word)
            elsif write_dt_from_scouting = '1' and (user_i_from_scouting = "11" or user_i_from_scouting = "01") and block_size_counter = x"100" then
                block_size_counter	<= x"002";

            -- increment the block size (count only data word)
            elsif write_dt_from_scouting = '1' and (user_i_from_scouting = "11" or user_i_from_scouting = "01") then
                block_size_counter	<= block_size_counter + "10";
            end if;

        end if;
    end process;



    ---- manage the write of the block size ready to be written in HBM
    process(clock)
    begin
        if rising_edge(clock) then
            wr_block_size		<= '0';

            -- fragment header is 128 bit
            -- for scouting it is 256 bits
            if Write_Fragment_Header = '1' then
                block_size      <= x"002";
                wr_block_size   <= '1';

            -- orbit header is 256 bit
            -- for scouting it is 256 bits
            elsif Write_Orbit_Header = '1' then
                block_size      <= x"002";
                wr_block_size   <= '1';

            elsif block_size_counter = x"0FE" and write_dt_from_scouting = '1' then
                block_size      <= x"100";
                wr_block_size   <= '1';

            elsif Write_block_size_last_data = '1' then
                block_size      <= block_size_counter;
                wr_block_size   <= '1';
            end if;

        end if;
    end process;



    ---- mux Data Headers and generate the write pulse
    process(clock)
    begin
        if rising_edge(clock) then
            wen_ff <= '0';

            -- write orbit header
            if Write_Orbit_Header = '1' then
                dataw       <= Orbit_Header;
                Word_spec   <= "100";
                wen_ff      <= '1';

            -- write fragment header
            elsif Write_Fragment_Header = '1' then
                dataw       <= Fragment_Header;
                Word_spec   <= "010";
                wen_ff      <= '1';

            -- write data
            elsif write_dt_from_scouting = '1' and (user_i_from_scouting = "11") then
                dataw       <= data_i_from_scouting;
                Word_spec   <= "001";
                wen_ff      <= '1';

            -- write last data (trailer)
            elsif write_dt_from_scouting = '1' and (user_i_from_scouting = "01") then
                dataw       <= data_i_from_scouting;
                Word_spec   <= "001";
                wen_ff      <= '1';
            end if;
        end if;
    end process;



    ---- swapped DTH header and payload
    process(dataw)
    begin
        if swapp then
            for I in 0 to 15 loop
                dataw_swapped((8*I)+7 downto (8*I))         <= dataw(127-(8*i) downto 120-(8*i));
                dataw_swapped((8*I)+135 downto (8*I)+128)   <= dataw(255-(8*i) downto 248-(8*i));
            end loop;
        else
                dataw_swapped                               <= dataw;
        end if;
    end process;



    ---- LFF control
    ---- 1 NOT FULL
    ---- 0 FULL
    process(reset_p,CLOCK)
    begin
        if reset_p = '1' then
            diff_w_p <= (others => '0');
        elsif rising_edge(CLOCK) then
            diff_w_p <= add_wr(10 downto 0) - add_rd(10 downto 0);
        end if;
    end process;



    ---- backpressure in term of words (in case of memory block almost FULL)
    process(reset_p,clock)
    begin
        if reset_p = '1' then
            LFF <= '0';
        elsif rising_edge(clock) then
            if    ( diff_w_p > "11111111000" ) then
                LFF <= '1';
            elsif ( diff_w_p < "11111100000" ) then
                LFF <= '0';
            end if;
        end if;
    end process;



    internal_LFF    <= LFF;             -- almost full on '1'

    data_word       <= Word_spec(0);
    Frag_HD_word    <= Word_spec(1);
    Orbit_HD_word   <= Word_spec(2);



    ---- Dual Port (DP) memory to record fragment divided in blocks
    memory_i1 : memory_DP
        port map (
            resetp                      => '0',
            clock                       => CLOCK,
            write_mem                   => wen_ff,
            address_wr(11 downto 0)     => add_wr(11 downto 0),
            data_i					    => dataw_swapped,
            data_i_extra(3 downto 0)    => "0000",
            data_i_extra(4)             => trailer_word,
            data_i_extra(5)             => data_word,               -- indicates that data is fragment words                            256-bit
            data_i_extra(6)             => Frag_HD_word,            -- indicates that data is Fragment Header in FIFO in parallel       256-bit
            data_i_extra(7)             => Orbit_HD_word,           -- indicates that data is Orbit block Header in FIFO in parallel    256-bit

            Enable_words(0)             => '1',
            Enable_words(1)             => '1',

            read_mem                    => rd_mem,                  -- We enable the read to save some power
            address_rd(11 downto 0)     => add_rd(11 downto 0),
            data_o                      => Data_o_to_HBM,
            data_o_extra(3 downto 0)    => Dummy,
            data_o_extra(4)             => trailer_word_to_HBM,     -- trailer
            data_o_extra(5)             => Data_Spec_to_HBM(0),     -- data
            data_o_extra(6)             => Data_Spec_to_HBM(1),     -- fragment header
            data_o_extra(7)             => Data_Spec_to_HBM(2)      -- orbit header
        );



    ---- check if there are any memory block ready to be written in TCP buffer
    ---- block   = header + payload
    ---- payload = words of 256 bits
    process(add_wr,add_rd)
    begin
        diff_r_p <= add_wr(10 downto 0) - add_rd(10 downto 0);
    end process;

    process(diff_r_p)
    begin
        if diff_r_p = "00000000000" then
            pre_empt(0) <= '0';
        else
            pre_empt(0) <= '1';
        end if;
    end process;

    process(reset_p,clock)
    begin
        if reset_p = '1' then
            pre_empt(1) <= '0';
        elsif rising_edge(clock) then
            if rd_mem = '1' then
                pre_empt(1) <= pre_empt(0);
            end if;
        end if;
    end process;



    ---- implement a first word fall through
    rd_mem <= '1' when Read_data_for_HBM = '1' or (pre_empt(0) = '1' and pre_empt(1) = '0') else '0';



    ---- address to read data
    process(Sync_reload(1),CLOCK)
    begin
        if Sync_reload(1) = '1' then
            add_rd <= (others => '0');
        elsif rising_edge(CLOCK) then
            if rd_mem = '1' and pre_empt(0) = '1' then
                add_rd <= add_rd + '1';
            end if;
        end if;
    end process;



    ---- store the size of blocks ready
    FF_size_block_ready_i1 : FIFO_block_ready
        port map (
            clk     => CLOCK,
            srst    => reset_p,
            din     => block_size,
            wr_en   => wr_block_size,
            rd_en   => Block_read_done_for_HBM,
            dout    => Size_ready_to_HBM,
            empty   => FF_block_size_empty
        );

    Data_ready_to_HBM <= not(FF_block_size_empty);

end behavioral;
