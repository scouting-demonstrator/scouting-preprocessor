----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.10.2018 13:09:31
-- Design Name: 
-- Module Name: Simple_512bit_memory - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
Library xpm;
use xpm.vcomponents.all;


entity Memory_DP is
  Port ( 
	resetp							: in std_logic;
  	clock							: in std_logic;
  
  	write_mem 						: in std_logic;
  	address_wr						: in std_logic_vector(11 downto 0);
  	data_i							: in std_logic_vector(255 downto 0);
  	data_i_extra					: in std_logic_vector(7 downto 0);
  	Enable_words					: in std_logic_vector(1 downto 0);
  
  	read_mem						: in std_logic;  
  	address_rd						: in std_logic_vector(11 downto 0);
	data_o							: out std_logic_vector(255 downto 0) ;
	data_o_extra					: out std_logic_vector(7 downto 0)
  );
end Memory_DP;

architecture Behavioral of Memory_DP is

component Simple_256bit_memory is
  Port ( 
	resetp							: in std_logic;
  	clock							: in std_logic;
  
  	write_mem 						: in std_logic;
  	address_wr						: in std_logic_vector(11 downto 0);
  	data_i							: in std_logic_vector(255 downto 0);
  	data_i_extra					: in std_logic_vector(7 downto 0);
  	Enable_words					: in std_logic_vector(1 downto 0);
  
  	read_mem						: in std_logic;  
  	address_rd						: in std_logic_vector(11 downto 0);
	data_o							: out std_logic_vector(255 downto 0);
	data_o_extra					: out std_logic_vector(7 downto 0)
  );
end component;

--*********************************************************************
--**************    CODE   START   HERE          *********************
--*********************************************************************
begin

mem_1:Simple_256bit_memory 
  Port Map( 
	resetp							=> resetp,
  	clock							=> clock,
									 
  	write_mem 						=> write_mem,
  	address_wr						=> address_wr,
  	data_i							=> data_i,
  	data_i_extra					=> data_i_extra,
  	Enable_words					=> Enable_words,
									 
  	read_mem						=> read_mem,
  	address_rd						=> address_rd,
	data_o							=> data_o,
	data_o_extra					=> data_o_extra
    );
    
end Behavioral;