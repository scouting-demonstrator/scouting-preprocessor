----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.10.2018 13:09:31
-- Design Name: 
-- Module Name: Simple_512bit_memory - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
Library xpm;
use xpm.vcomponents.all;


entity Simple_256bit_memory is
  Port ( 
	resetp							: in std_logic;
  	clock							: in std_logic;
  
  	write_mem 						: in std_logic;
  	address_wr						: in std_logic_vector(11 downto 0);
  	data_i							: in std_logic_vector(255 downto 0);
	data_i_extra					: in std_logic_vector(7 downto 0);
  	Enable_words					: in std_logic_vector(1 downto 0);
  
  	read_mem						: in std_logic;  
  	address_rd						: in std_logic_vector(11 downto 0);
	data_o							: out std_logic_vector(255 downto 0); 
	data_o_extra					: out std_logic_vector(7 downto 0) 
  );
end Simple_256bit_memory;

architecture Behavioral of Simple_256bit_memory is
 
signal dummy						: std_logic_vector(5 downto 0);
signal dummy_a						: std_logic_vector(5 downto 0);
signal gnd							: std_logic := '0';

signal Enable_words_cell			: std_logic_vector(3 downto 0);

--*********************************************************************
--**************    CODE   START   HERE          *********************
--*********************************************************************
begin

Enable_words_cell(0)		<= Enable_words(0);
Enable_words_cell(1)		<= Enable_words(0);
Enable_words_cell(2)		<= Enable_words(1);
Enable_words_cell(3)		<= Enable_words(1);

UltraRam_inst0:for I in 0 to 3 generate
xpm_memory_sdpram_inst : xpm_memory_sdpram
   generic map (
      ADDR_WIDTH_A            => 12,                      -- DECIMAL
      ADDR_WIDTH_B            => 12,                      -- DECIMAL
      AUTO_SLEEP_TIME         => 0,                       -- DECIMAL
      BYTE_WRITE_WIDTH_A      => 72,                      -- DECIMAL
      CLOCKING_MODE           => "common_clock",          -- String
      ECC_MODE                => "no_ecc",                -- String
      MEMORY_INIT_FILE        => "none",                  -- String
      MEMORY_INIT_PARAM       => "0",                     -- String
      MEMORY_OPTIMIZATION     => "true",                  -- String
      MEMORY_PRIMITIVE        => "ultra",                 -- String
      MEMORY_SIZE             => (4096)*72,             -- memory sie in bits   DECIMAL
      MESSAGE_CONTROL         => 0,                       -- DECIMAL
      READ_DATA_WIDTH_B       => 72,                      -- DECIMAL
      READ_LATENCY_B          => 1,                       -- DECIMAL
      READ_RESET_VALUE_B      => "0",                     -- String
      USE_EMBEDDED_CONSTRAINT => 0,                       -- DECIMAL
      USE_MEM_INIT            => 0,                       -- DECIMAL
      WAKEUP_TIME             => "disable_sleep",         -- String
      WRITE_DATA_WIDTH_A      => 72,                      -- DECIMAL
      WRITE_MODE_B            => "read_first"              -- String
   )
   port map (
   		rstb           	=> resetp,                      	
   		-- 1-bit input: Reset signal for the final port B output register
   		-- stage. Synchronously resets output port doutb to the value specified
   		-- by parameter READ_RESET_VALUE_B.
   		sleep          	=> gnd,                        
   		-- 1-bit input: sleep signal to enable the dynamic power saving feature.
   		
   		 dbiterrb       => open,                  -- 1-bit output: Status signal to indicate double bit error occurrence
   												      -- on the data output of port B.
   		injectdbiterra 	=> gnd,            
   		-- 1-bit input: Controls double bit error injection on input data when
   		-- ECC enabled (Error injection capability is not available in
   		-- "decode_only" mode).
   	
   		injectsbiterra 	=> gnd,            
   		-- 1-bit input: Controls single bit error injection on input data when
   		-- ECC enabled (Error injection capability is not available in
   		-- "decode_only" mode).
   	
   		sbiterrb       	=> open,                  -- 1-bit output: Status signal to indicate single bit error occurrence
   													  -- on the data output of port B.
   	
   		clka           	=> clock,                       
   		-- 1-bit input: Clock signal for port A. Also clocks port B when
   		-- parameter CLOCKING_MODE is "common_clock".
   		addra          	=> address_wr,                      
   		-- ADDR_WIDTH_A-bit input: Address for port A write operations.
   		
   		dina(63 downto 00)	=> data_i(( (64 * I) + 63) downto  (64 * I) ),
   		dina(65 downto 64)	=> data_i_extra(( (2 * I) + 1) downto  (2 * I) ),
   		dina(71 downto 66)	=> dummy ,
   		-- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
   		
   		ena            	=> write_mem,               	
   		-- 1-bit input: Memory enable signal for port A. Must be high on clock
   		-- cycles when write operations are initiated. Pipelined internally.
   		wea(0)         	=> Enable_words_cell(I),         
   		-- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input
   		-- data port dina. 1 bit wide when word-wide writes are used. In
   		-- byte-wide write configurations, each bit controls the writing one
   		-- byte of dina to address addra. For example, to synchronously write
   		-- only bits [15-8] of dina when WRITE_DATA_WIDTH_A is 32, wea would be
   		-- 4'b0010.
   
   		
   		clkb           	=> clock,                      
   		-- 1-bit input: Clock signal for port B when parameter CLOCKING_MODE is
   		-- "independent_clock". Unused when parameter CLOCKING_MODE is
   		-- "common_clock".
   	
   		addrb          	=> address_rd,                  
   		-- ADDR_WIDTH_B-bit input: Address for port B read operations.
   		
   		doutb(63 downto 00) => data_o(( (64 * I) + 63) downto  (64 * I) ),
   		doutb(65 downto 64)	=> data_o_extra(( (2 * I) + 1) downto  (2 * I) ),
   		doutb(71 downto 66)	=> dummy_a,
   		-- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
   		
   		enb            	=> read_mem,    
   		-- 1-bit input: Memory enable signal for port B. Must be high on clock
   		-- cycles when read operations are initiated. Pipelined internally
   		regceb			=> '1' 
   		);
end generate;

end Behavioral;
