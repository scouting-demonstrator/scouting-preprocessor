------------------------------------------------------
-- this boxe prepare the PING packet reply
--
-- Dominique Gigi
--
-- Oct 2012 ver 1.00
--
------------------------------------------------------
 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
--use IEEE.std_logic_unsigned.all;.
--library UNIMACRO;  
--use UNIMACRO.Vcomponents.all;
use work.interface.all;

entity PING_HD is
	port (
		-- INPUT
		RESET_n						    : IN STD_LOGIC;
		CLOCK							: IN STD_LOGIC;
				
		PING_request					: IN STD_LOGIC; -- request to send s PING request
		ICMP_prbl_req				    : IN STD_LOGIC; -- request a ICMP reply error
																-- if none of them PING reply
		Wr_DT_ping			    	    : IN STD_LOGIC;
		DATAi							: IN STD_LOGIC_VECTOR(63 DOWNTO 0); -- data and reply header is coming form this bus
		Last_dt				    	    : IN STD_LOGIC; 
		PING_FIFO_empty       	        : OUT STD_LOGIC;
		reset_PING_FIFO			        : IN STD_LOGIC;
		PREP							: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		PING_Idle					    : out std_logic;
 
		-- OUTPUT
		ICMP_prbl_pckt		   	        : OUT STD_LOGIC;
		tx_lbus 				    	: OUT tx_usr_record;
		write_lbus					    : out std_logic;					-- wr 4x128 bit when there are ready
		sel_lbus						: out std_logic_vector(3 downto 0); -- Ping is prepared  128 bit by 128 bit (slower than a normal transfert)
		PING_len						: OUT STD_LOGIC_VECTOR(15 DOWNTO 0)   -- used to build the IP header
		 		  
	);
end PING_HD;

--********************************************************************************
architecture core of PING_HD is


COMPONENT Ping_DT_FIFO
  PORT (
    clk 			: IN STD_LOGIC;
    srst 			: IN STD_LOGIC;
    din 			: IN STD_LOGIC_VECTOR(64 DOWNTO 0);
    wr_en 			: IN STD_LOGIC;
    rd_en 			: IN STD_LOGIC;
    dout 			: OUT STD_LOGIC_VECTOR(64 DOWNTO 0);
    full 			: OUT STD_LOGIC;
    empty 			: OUT STD_LOGIC;
    data_count		: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
    wr_rst_busy 	: OUT STD_LOGIC;
    rd_rst_busy 	: OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT Data_PING_request
  PORT (
    a 					: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    d 					: IN STD_LOGIC_VECTOR(64 DOWNTO 0);
    clk					: IN STD_LOGIC;
    we 					: IN STD_LOGIC;
    spo 				: OUT STD_LOGIC_VECTOR(64 DOWNTO 0)
  );
END COMPONENT;

type ping_pckt_type is (	idle,
							prep_word0,
							prep_word1,
							send_remainder,
							end_pckt							
							);
signal ping_pckt :ping_pckt_type;

signal wr_ff						 : std_logic;
signal almost_f					     : std_logic_vector(9 downto 0);
--attribute mark_debug of almost_f   : signal is "true";			
 
signal reset_p						 : std_logic;
signal cell_dt						 : std_logic_vector(63 downto 0);

signal empty_cell					 : std_logic;
signal req_ping_reg				     : std_logic;
signal read_fifo_p				     : std_logic;
  
signal Ping_address				     : std_logic_vector(3 downto 0);
signal data_ping_prep			     : std_logic_vector(63 downto 0);
signal end_ping_pckt				 : std_logic;
signal end_ping_pckt_mem		     : std_logic;

signal l_bus_dt_rg				     : std_logic_vector(127 downto 0);
signal l_bus_dt_tmp				     : std_logic_vector(15 downto 0);
signal l_bus_ena_rg				     : std_logic;
signal l_bus_sop_rg				     : std_logic;
signal l_bus_eop_rg				     : std_logic;
signal l_bus_empty_rg			     : std_logic_vector(3 downto 0);

signal change_request_to_reply		 : std_logic; -- change the IPMC 0x08 (Ping request ) to 0x00 IPMC PING reply)

signal l_bus_word					 : std_logic_vector(3 downto 0); 
 
signal write_lbus_rg				 : std_logic;
signal l_bus_word_rg  			     : std_logic_vector(3 downto 0); 

attribute mark_debug : string;
--attribute fsm_encoding : string;
--attribute fsm_encoding of ping_pckt: signal is "one_hot";
--attribute mark_debug of l_bus_dt_rg: signal is "true";
--*****************************************************************************************
--**************************  BEGINNING  **************************************************
--*****************************************************************************************
begin

reset_p <= '1' when RESET_n  = '0' or reset_PING_FIFO = '1' else '0';
 

ICMP_prbl_pckt			<= ICMP_prbl_req;
 
DataPing_FIFO_i1:Ping_DT_FIFO
  PORT map(
    clk 				=>	CLOCK			,--: IN STD_LOGIC;
    srst 				=>	reset_p		    ,--: IN STD_LOGIC;
    din(63 downto 0)	=>	datai			,--: IN STD_LOGIC_VECTOR(511 DOWNTO 0);
	din(64)			    =>  Last_dt		,
    wr_en 				=>	Wr_DT_ping	,--: IN STD_LOGIC;
	-- full 			=>				,--: OUT STD_LOGIC;
    
    rd_en 				=>	read_fifo_p	,--: IN STD_LOGIC;
    dout(63 downto 0)	=>	cell_dt		,--: OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
	dout(64)			=>	empty_cell	,
    empty 				=>	PING_FIFO_empty	,		 --: OUT STD_LOGIC;
    data_count 			=>	almost_f					 --: OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
    -- wr_rst_busy 		=>				,--: OUT STD_LOGIC;
    -- rd_rst_busy 		=>				 --: OUT STD_LOGIC
  );
 
process(ping_pckt, clock) 
begin
	if ping_pckt = Idle then
		Ping_address		<= (others => '0');
	elsif rising_edge(clock) then
		if ping_pckt = prep_word0 or ping_pckt = prep_word1 then
			Ping_address 	<= Ping_address + '1';
		end if;
	end if;
end process;

read_fifo_p	<= '1' when ping_pckt = prep_word0 or ping_pckt = prep_word1 else '0';

DataPing_ROM_i1:Data_PING_request
  PORT map(
    a 						=>	Ping_address			, 
  	d 						=>	(OTHERS => '0')			, 
  	clk 					=>	CLOCK					, 
  	we 						=>	'0'						, 
    spo(63 downto 0)		=>	data_ping_prep			, 
	 spo(64)				=> end_ping_pckt
	 );

process(PING_request,ICMP_prbl_req,almost_f)	 
begin
	if PING_request = '1' then
		PING_len(15 downto  0)		<= x"0068";  -- should change this length if we change the COE file associated to the Data+PING request ROM
	elsif ICMP_prbl_req = '1' then
		PING_len(15 downto  0)		<= x"0008";
	else
		PING_len(15 downto 13)		<= (others => '0');
		PING_len(12 downto  3)		<= almost_f;
		PING_len(2 downto  0)		<= (others => '0');
	end if;
end process;
--*********************************************************************************************
-- create each 128 bit words
work_structure:process(clock,RESET_n)
begin
	if RESET_n = '0'  then
		ping_pckt 			<= idle; 
	elsif rising_edge(clock) then
 
		Case ping_pckt is
			when idle => 
				if prep(0) = '1' then
					ping_pckt 	<= prep_word0;
				end if;
				
			-- data for PING is coming by 64bit used to build 4 x 128 bit bus (2 64bit word by 128 word delivered to the MAC)				
			when prep_word0 =>
				if (PING_request = '1' and end_ping_pckt = '1') or (PING_request = '0' and empty_cell = '1') then
					ping_pckt 	<= end_pckt;
				else	
					ping_pckt 	<= prep_word1;
				end if;
				
			when prep_word1 =>
				if (PING_request = '1' and end_ping_pckt = '1') or (PING_request = '0' and empty_cell = '1') then
					ping_pckt 	<= send_remainder; 
				else	
					ping_pckt 	<= prep_word0;
				end if;
				
			-- this state is needed if prep_word1 id the last 64bit word (64bit word are not aligned on the 128bit word for the MAC
			-- her is the alignement 1st 64 word go to bit 111..47 the 2nd on bit 47..00=> the remainder on the next 128 word (for MAC) bit 127..122)
			when send_remainder =>
				ping_pckt 		<= end_pckt;				
				
			when end_pckt =>
				ping_pckt 		<= idle;
				
			when others =>
				ping_pckt 		<= idle;
		end case;
	end if;
end process;

PING_Idle	<= '1' when ping_pckt = idle else '0';		 
-- select the appropriate LBUS (A,B,C or D)		 
process(clock,RESET_n)		
begin
	if RESET_n = '0'then
		l_bus_word <= "0100";  -- start ping on word 2 (first word is 0 Eth, 1 IP ,....)
	elsif rising_edge(clock) then
		if ping_pckt = prep_word1 then
			l_bus_word(3 downto 1)		<= l_bus_word(2 downto 0);
			l_bus_word(0)			  	<= l_bus_word(3);
		elsif prep(0) = '1' then
			l_bus_word 					<= "0100";  -- start ping on word 2 (first word is 0 Eth, 1 IP ,....)
		end if;
	end if;
end process;
 
 
-- signal to exchange the PING request 0x08  to PING reply 0x00
process(clock,RESET_n)
begin
	if RESET_n = '0' then
		change_request_to_reply	<= '0';
	elsif rising_edge(clock) then
		if 		ping_pckt = idle then
			change_request_to_reply	<= '1';
		elsif 	ping_pckt = prep_word1 then
			change_request_to_reply	<= '0';
		end if;
	end if;
end process; 
 
 
process(clock)
begin
	if rising_edge(clock) then
		--l_bus_dt_rg									<= (others => '0');

		if 	ping_pckt = prep_word0  or ping_pckt = send_remainder then
			if 	PING_request = '1' then
				l_bus_dt_rg(127 downto 112)		<= l_bus_dt_tmp;
				l_bus_dt_rg(111 downto 48)		<= data_ping_prep;
			elsif ICMP_prbl_req = '1' then
				l_bus_dt_rg(127 downto 112)		<= l_bus_dt_tmp;
				l_bus_dt_rg(111 downto 96)		<= x"0000"; --pointer
				l_bus_dt_rg(95 downto 80) 		<= x"0C02";
				l_bus_dt_rg(79 downto 64) 		<= x"F3FD"; --checksum as data are all 0 , the chcksum is "NOT" of (0C02) ;
				l_bus_dt_rg(63 downto 48) 		<= x"0000";
			else
				l_bus_dt_rg(127 downto 112) 	<= l_bus_dt_tmp;
				l_bus_dt_rg(111 downto 48)		<= cell_dt;
				if change_request_to_reply = '1' then
					l_bus_dt_rg(111 downto 104) <= x"00";
				end if;
				
			end if;
	
		elsif ping_pckt = prep_word1 then
			if 	PING_request = '1' then
				l_bus_dt_rg(47 downto 0) 		<= data_ping_prep(63 downto 16);
			elsif ICMP_prbl_req = '1' then
				l_bus_dt_rg						<= (others => '0'); 
			else
				l_bus_dt_rg(47 downto 0) 		<= cell_dt(63 downto 16);
			end if;		
						
		end if;
		
		-- write the 128 bit to LBUS selected
		l_bus_word_rg							<= l_bus_word;
		write_lbus_rg							<= '0';
		-- write 4 x 128 bit to MAC when
			   -- the 4 128 bit words are ready
		if 	   (ping_pckt = prep_word1 and l_bus_word = x"8")
		       -- at the end of the ping packet          Ping Request								Ping Reply      
			or (ping_pckt = prep_word0 and ((PING_request = '1' and end_ping_pckt = '1') or (PING_request = '0' and empty_cell = '1')))
			   -- finish the packet
			or (ping_pckt = send_remainder)  then
			write_lbus_rg						<= '1';
		end if;
	end if;
end process;

process(clock,ping_pckt)
begin
	if ping_pckt = Idle then
		l_bus_dt_tmp								<= (others => '0');
		l_bus_empty_rg								<= x"0";
		l_bus_eop_rg								<= '0';
	elsif rising_edge(clock) then
		if 	ping_pckt = prep_word0  or ping_pckt = send_remainder then

			if ping_pckt = send_remainder then
				l_bus_empty_rg						<= x"E";
				l_bus_eop_rg						<= '1';
			elsif (PING_request = '1' and end_ping_pckt = '1') or (PING_request = '0' and empty_cell = '1') then
				l_bus_empty_rg						<= x"6";
				l_bus_eop_rg						<= '1';
			end if;
			
		elsif ping_pckt = prep_word1 then
			if 	PING_request = '1' then
				l_bus_dt_tmp						<= data_ping_prep(15 downto 00);
			elsif ICMP_prbl_req = '1' then
				l_bus_dt_tmp						<= (others => '0');
			else
				l_bus_dt_tmp						<= cell_dt(15 downto 00);
			end if;		
		end if;
		
	end if;
end process;
		
tx_lbus.data_bus   <= l_bus_dt_rg;
tx_lbus.data_ena   <= '1';
tx_lbus.sopckt     <= '0';
tx_lbus.eopckt     <= l_bus_eop_rg;
tx_lbus.err_pckt   <= '0';
tx_lbus.empty_pckt <= l_bus_empty_rg;
		
		
write_lbus			<= write_lbus_rg;
sel_lbus			<= l_bus_word_rg;
 

end core;