------------------------------------------------------------------------------
--                     Compare values		                               --
------------------------------------------------------------------------------
--  Comparator	 
--   
--   Dominique Gigi 2014		 
--								 
--   ver 1.00							 
-- Compare
-- 
-- 
--
--
--
------------------------------------------------------------------------------
LIBRARY ieee;
 

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity comparator is
	generic( 	val_signed 	: boolean := true;
					comp_null	: boolean := true;
					pipe			: boolean := true; -- 1 clock
					wd_width		: natural := 32
		);
	port (
		reset		: in std_logic;
		clock		: in std_logic;
		dataa		: in std_logic_vector(wd_width-1 downto 0);
		datab		: in std_logic_vector(wd_width-1 downto 0);
		
		aeb		: out std_logic;
		agb		: out std_logic;
		ageb		: out std_logic;
		alb		: out std_logic;
		aleb		: out std_logic;
		aneb		: out std_logic		
	);
end comparator;

architecture behavioral of comparator is

signal val_b		: std_logic_vector(wd_width-1 downto 0);

--##################################################
--#################     BEGIN     ##################
--##################################################
begin

Nul_1:if not(comp_null) generate
begin
	val_b	<= datab;
end generate Nul_1;

Nul_2:if (comp_null) generate
begin
	val_b	<= (others => '0');
end generate Nul_2;

 

pipe_signed:if pipe and val_signed generate
	process(clock,reset)
	begin
		if reset = '0' then
			aeb			<= '0';
			agb			<= '0';
			ageb		<= '0';
			alb			<= '0';
			aleb		<= '0';
			aneb		<= '0';
		elsif rising_edge(clock) then
			aeb			<= '0';
			if SIGNED(dataa) = SIGNED(val_b) then -- Equal
				aeb		<= '1';
			end if;
			agb			<= '0';
			if SIGNED(dataa) > SIGNED(val_b) then	--Great
				agb	<= '1';
			end if;
			ageb		<= '0';
			if SIGNED(dataa) >= SIGNED(val_b) then --Great or equal
				ageb	<= '1';
			end if;
			alb			<= '0';
			if SIGNED(dataa) < SIGNED(val_b) then --lower
				alb		<= '1';
			end if;
			aleb			<= '0';
			if SIGNED(dataa) <= SIGNED(val_b) then -- lower or equal
				aleb		<= '1';
			end if;
			aneb			<= '0';
			if SIGNED(dataa) /= SIGNED(val_b) then -- not equal
				aneb		<= '1';
			end if;	
		end if;
	end process;
end generate pipe_signed;
				
pipe_unsigned:if pipe and not(val_signed) generate
	process(clock,reset)
	begin
		if reset = '0' then
			aeb			<= '0';
			agb			<= '0';
			ageb			<= '0';
			alb			<= '0';
			aleb			<= '0';
			aneb			<= '0';
		elsif rising_edge(clock) then				
			aeb			<= '0';
			if UNSIGNED(dataa) = UNSIGNED(val_b) then -- Equal
				aeb		<= '1';
			end if;
			agb			<= '0';
			if UNSIGNED(dataa) > UNSIGNED(val_b) then	--Great
				agb		<= '1';
			end if;
			ageb			<= '0';
			if UNSIGNED(dataa) >= UNSIGNED(val_b) then --Great or equal
				ageb		<= '1';
			end if;
			alb			<= '0';
			if UNSIGNED(dataa) < UNSIGNED(val_b) then --lower
				alb		<= '1';
			end if;
			aleb			<= '0';
			if UNSIGNED(dataa) <= UNSIGNED(val_b) then -- lower or equal
				aleb		<= '1';
			end if;
			aneb			<= '0';
			if UNSIGNED(dataa) /= UNSIGNED(val_b) then -- not equal
				aneb		<= '1';
			end if;									
		end if;
	end process;
end generate pipe_unsigned;



unpipe_signed:if not(pipe) and val_signed generate
	process(dataa,val_b)
	begin
		aeb			<= '0';
		if SIGNED(dataa) = SIGNED(val_b) then -- Equal
			aeb		<= '1';
		end if;
		agb			<= '0';
		if SIGNED(dataa) > SIGNED(val_b) then	--Great
			agb	<= '1';
		end if;
		ageb		<= '0';
		if SIGNED(dataa) >= SIGNED(val_b) then --Great or equal
			ageb	<= '1';
		end if;
		alb			<= '0';
		if SIGNED(dataa) < SIGNED(val_b) then --lower
			alb		<= '1';
		end if;
		aleb			<= '0';
		if SIGNED(dataa) <= SIGNED(val_b) then -- lower or equal
			aleb		<= '1';
		end if;
		aneb			<= '0';
		if SIGNED(dataa) /= SIGNED(val_b) then -- not equal
			aneb		<= '1';
		end if;			
	end process;
end generate unpipe_signed;


unpipe_unsigned:if not(pipe) and not(val_signed) generate
	process(dataa,val_b)
	begin
		aeb			<= '0';
		if UNSIGNED(dataa) = UNSIGNED(val_b) then -- Equal
			aeb		<= '1';
		end if;
		agb			<= '0';
		if UNSIGNED(dataa) > UNSIGNED(val_b) then	--Great
			agb	<= '1';
		end if;
		ageb		<= '0';
		if UNSIGNED(dataa) >= UNSIGNED(val_b) then --Great or equal
			ageb	<= '1';
		end if;
		alb			<= '0';
		if UNSIGNED(dataa) < UNSIGNED(val_b) then --lower
			alb		<= '1';
		end if;
		aleb			<= '0';
		if UNSIGNED(dataa) <= UNSIGNED(val_b) then -- lower or equal
			aleb		<= '1';
		end if;
		aneb			<= '0';
		if UNSIGNED(dataa) /= UNSIGNED(val_b) then -- not equal
			aneb		<= '1';
		end if;								
	end process;	
end generate unpipe_unsigned;

end behavioral;