------------------------------------------------------
-- remove CMC slink status word until the first Header on L0 & L1 before each event
--
--  Ver 1.00
--
-- Dominique Gigi Jan 2011
------------------------------------------------------
--   
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
 
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;
 

entity ack_statistic is

port (
		RESET					: in std_logic;
		RST_CNT					: in std_logic;
		CLOCK					: in std_logic;
		latch_reg				: in std_logic;
		
		ACK						: in std_logic;

		
		Max_T_between_ack		: out std_logic_vector(31 downto 0);
		Actual_ack				: out std_logic_vector(31 downto 0);
		NB_ack					: out std_logic_vector(31 downto 0);

		Accu_ACK_timer			: out std_logic_vector(63 downto 0)
	);
	
end ack_statistic;


architecture behavioral of ack_statistic is

component cnt32b_sclr_cken
	PORT
	(
		CLK 	: IN STD_LOGIC;
		CE 		: IN STD_LOGIC;
		SCLR 	: IN STD_LOGIC;
		Q 		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
	);
end component;

component comp_32b_unsigned_1ck
	PORT
	(
		aclr			: IN STD_LOGIC ;
		clock			: IN STD_LOGIC ;
		dataa			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		datab			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		aeb				: OUT STD_LOGIC ;
		agb				: OUT STD_LOGIC 
	);
end component;

signal time_ack_actua			: std_logic_vector(31 downto 0);
signal time_ack_reg				: std_logic_vector(31 downto 0);

signal mem_A					: std_logic_vector(31 downto 0);
signal mem_B					: std_logic_vector(31 downto 0);

signal timer_reg				: std_logic_vector(63 downto 0);
signal nb_ack_reg				: std_logic_vector(31 downto 0);

signal Act_G_MAX				: std_logic;

--*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
--*=*=*=*=*=*=*=*=*=*=*=*=       BEGIN          *=*=*=*=*=*=*=*=*=*=*=*=*=*
--*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
begin 

--========================================================================
-- instead of accumulation the time between 2 ack , I implement a counter 64bit
process(RST_CNT,CLOCK)
begin
	if RST_CNT = '1' then
			timer_reg 		<= (others => '0');
	elsif rising_edge(CLOCK) then
			timer_reg		<= timer_reg 	+ '1';
	end if;
end process;

--========================================================================
-- TCP Stream 0
-- Calcutate the time between two acknoledge (Actual, Max, (Accu & Nb) Average
i1:cnt32b_sclr_cken
PORT MAP
	(
		CLK			=> CLOCK,
		CE			=> '1',
		sclr		=> ACK,
		q			=> time_ack_actua
	);

process(RST_CNT,CLOCK)
begin
	if RST_CNT = '1' then
			time_ack_reg 	<= (others => '0');
			nb_ack_reg		<= (others => '0');
	elsif rising_edge(CLOCK) then
			if ACK = '1' then
				time_ack_reg <= time_ack_actua;
				nb_ack_reg	<= nb_ack_reg 	+ '1';
			end if;
	end if;
end process;

Actual_ACK		<= time_ack_reg;

i2:comp_32b_unsigned_1ck
PORT MAP
	(
		aclr		=> '0',
		clock		=> CLOCK,
		dataa		=> time_ack_actua,
		datab		=> mem_B ,
		agb		=> Act_G_MAX 
	);

process(RST_CNT,CLOCK)
begin
	if RST_CNT = '1' then
			mem_A 	<= (others => '0');
			mem_B 	<= (others => '0');	
	elsif rising_edge(CLOCK) then
			if Act_G_MAX = '1' then
				mem_B <= mem_A;
			end if;
			mem_A	<= time_ack_actua;
	end if;
end process;	

Max_T_between_ACK	<= mem_B;


--=============================================================================
-- Latch value to have the same time value on division
process(CLOCK)
begin
	if rising_edge(clock) then
		if latch_reg = '1' then
			NB_ack			<= nb_ack_reg;
			Accu_ACK_timer	<= timer_reg;
		end if;
	end if;
end process;

end behavioral;