------------------------------------------------------
-- statistic counters
--
--  Ver 1.00
--
-- Dominique Gigi Sept 2012
------------------------------------------------------
--  11/2014 add some statistic register (19 to 22)
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
 
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.address_table.all;

entity statistic_TCP is
generic (addr_offset		: integer := 0);
port (
	reset						: in std_logic;
	
	local_resetn				: in std_logic;
	TCP_clock					: in std_logic;
	latch_rcv_ack				: in std_logic; 
	 
	Ctrl_clock					: in std_logic;
	latch_value_to_be_rd		: in std_logic;
	ena_stat					: in std_logic_vector(18 downto 0);
	
	Stat_reg					: in std_logic_vector(31 downto 0);
	
	stat_send_byte				: in std_logic_vector(63 downto 0):= (others => '0');
	stat_snd_rexmit_byte		: in std_logic_vector(63 downto 0):= (others => '0');
	stat_dupack_max				: in std_logic_vector(31 downto 0):= (others => '0');
	stat_win_max				: in std_logic_vector(31 downto 0):= (others => '0');
	stat_win_min				: in std_logic_vector(31 downto 0):= (others => '0');
	measure_RTT_max				: in std_logic_vector(31 downto 0):= (others => '0');
	measure_RTT_min				: in std_logic_vector(31 downto 0):= (others => '0');
	measure_RTT_sum				: in std_logic_vector(63 downto 0):= (others => '0');
	RTT_Shift_MAX				: in std_logic_vector(31 downto 0):= (others => '0');
	RTT_Val						: in std_logic_vector(31 downto 0):= (others => '0');
	MSS_rcv						: in std_logic_vector(31 downto 0):= (others => '0');    
	ACK_rcv						: in std_logic; 
	TCP_state					: in std_logic_vector(2 downto 0);
	func						: in std_logic_vector(16383 downto 0);
	Ctrl_data_out				: out std_logic_vector(63 downto 0)
	);
	
end statistic_TCP;

architecture behavioral of statistic_TCP is

attribute mark_debug : string;

COMPONENT cnt32b_sclr_cken
  PORT (
    CLK 	: IN STD_LOGIC;
    CE 		: IN STD_LOGIC;
    SCLR 	: IN STD_LOGIC;
    Q 		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;

COMPONENT cnt64bit_cken
  PORT (
    CLK 	: IN STD_LOGIC;
    CE 		: IN STD_LOGIC;
    SCLR 	: IN STD_LOGIC;
    Q 		: OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
  );
END COMPONENT;

component resync_v4 is
port (
	aresetn			: in std_logic;
	clocki			: in std_logic;	
	input			: in std_logic;
	clocko			: in std_logic;
	output			: out std_logic
	);
end component;

component ack_statistic is
port (
		RESET					: in std_logic;
		RST_CNT					: in std_logic;
		CLOCK					: in std_logic;
		latch_reg				: in std_logic;
		ACK						: in std_logic;
		Max_T_between_ack		: out std_logic_vector(31 downto 0);
		Actual_ack				: out std_logic_vector(31 downto 0);
		NB_ack					: out std_logic_vector(31 downto 0);
		Accu_ACK_timer			: out std_logic_vector(63 downto 0)
	);
end component;

signal out_dt						: std_logic_vector(63 downto 0);

type stat_cnt_def is array (0 to 15) of std_logic_vector(31 downto 0);
signal stat_val 					:stat_cnt_def;

type stat_cntb_def is array (0 to 2) of std_logic_vector(31 downto 0);
signal statb_val  					:stat_cntb_def;

signal Max_T_between_ack			: std_logic_vector(31 downto 0);
signal Actual_ack					: std_logic_vector(31 downto 0);
signal NB_ack						: std_logic_vector(31 downto 0);
signal Accu_ACK_timer				: std_logic_vector(63 downto 0);
signal local_reset_p				: std_logic; 
signal ACK_rcv_cnt					: std_logic_vector(63 downto 0); 
signal latch_value_to_be_rd_resync	: std_logic;
 

--attribute mark_debug of out_dt: signal is "true"; 
--attribute mark_debug of stat_val: signal is "true"; 

--***************************************************************************
--**************************     BEGIN     **********************************
--***************************************************************************
begin 

local_reset_p	<=  local_resetn;

inst_g1:for I in 0 to 15 generate
	i1:cnt32b_sclr_cken
		PORT MAP
		(
			clk			=> TCP_clock,
			ce			=> ena_stat(I+3),
			sclr		=> local_reset_p,
			q			=> stat_val(I)
		);
end generate inst_g1;


inst_g2:for I in 0 to 2 generate
	i1:cnt32b_sclr_cken
		PORT MAP
		(
			clk			=> TCP_clock,
			ce			=> ena_stat(I),
			sclr		=> local_reset_p,
			q			=> statb_val(I)
		);
end generate inst_g2;


resync_lacth_pci_xgmii:resync_v4 
	port map(
		aresetn			=> reset,
		clocki			=> Ctrl_clock,
		clocko			=> TCP_clock,
		input			=> latch_value_to_be_rd,
		output			=> latch_value_to_be_rd_resync
		);
		
ack_stat_i1:ack_statistic  
port map(
		RESET					=>  reset,			 
		RST_CNT					=>  local_reset_p,			 
		CLOCK					=>  TCP_clock,			 
		latch_reg				=>  latch_value_to_be_rd_resync,	
		ACK						=>  ACK_rcv,			
		Max_T_between_ack	 	=>  Max_T_between_ack,	
		Actual_ack				=>  Actual_ack,		
		NB_ack					=>  NB_ack,			
		Accu_ACK_timer			=>  Accu_ACK_timer		
	);
 
ACK_rcv_cnt_i1:cnt64bit_cken
	PORT map	(
		sclr		=> local_reset_p,
		clk			=> TCP_clock,
		ce			=> ACK_rcv,
		q			=> ACK_rcv_cnt
	);
 
 
process(Ctrl_clock)
begin
if rising_edge(Ctrl_clock) then
	out_dt							<= (others => '0');
	 
		if func(TCP_100Gb_counter_RTT + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(5); --RTT counter
		elsif func(TCP_100Gb_counter_conn_attempt + addr_offset) = '1' then
			out_dt(15 downto 00) 	<= statb_val(1)(15 downto 0); --Connection Attempt
			out_dt(31 downto 16) 	<= (others => '0'); 
		elsif func(TCP_100Gb_Status_flags_SND_probe + addr_offset) = '1' then 
			out_dt(15 downto 0) 	<= stat_reg(15 downto 0);
			out_dt(31 downto 16) 	<= stat_val(2)(31 downto 16); -- SND probe
		elsif func(TCP_100Gb_counter_SND_pack + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(0); -- SND pack
		elsif func(TCP_100Gb_counter_Rexmit_pack + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(1); -- Rexmit pack
		elsif func(TCP_100Gb_counter_Rcv_Dupl_pack + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(3); -- rcv dupack
		elsif func(TCP_100Gb_counter_Fast_retransmit + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(4); -- fast retrasnmit
		elsif func(TCP_100Gb_counter_measure_rtt + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(6); -- measure rtt count (number of time we restart the counter, at each send packet)
		elsif func(TCP_100Gb_counter_seg_drop_after_ack + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(8); -- seg dropped after ack
		elsif func(TCP_100Gb_counter_seg_dropped + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(9); --seg dropped
		elsif func(TCP_100Gb_counter_seg_dropped_wRst + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(10); -- seg dropped with rst
		elsif func(TCP_100Gb_counter_persist_exit + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(11); -- persist existed
		elsif func(TCP_100Gb_counter_persist + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(12); -- persist 
		elsif func(TCP_100Gb_counter_send_bytes + addr_offset) = '1' then
			out_dt 					<= stat_send_byte(63 downto 00);
		elsif func(TCP_100Gb_counter_rexmit_bytes + addr_offset) = '1' then
			out_dt 					<= stat_snd_rexmit_byte(63 downto 00);
		elsif func(TCP_100Gb_counter_persist_closed_win + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(14);	--persist closed window
		elsif func(TCP_100Gb_counter_unaligned_to_64b + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(15);	--unaligned to 64 bit
		elsif func(TCP_100Gb_status_dupack_max + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_dupack_max;
		elsif func(TCP_100Gb_status_win_max + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_win_max;
		elsif func(TCP_100Gb_status_win_min + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_win_min;
		elsif func(TCP_100Gb_status_RTT_Max + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= measure_RTT_max;
		elsif func(TCP_100Gb_status_RTT_Min + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= measure_RTT_min;
		elsif func(TCP_100Gb_status_RTT_Sum + addr_offset) = '1' then
			out_dt 					<= measure_RTT_sum(63 downto 0);
		elsif func(TCP_100Gb_counter_persist_zero_wind + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= stat_val(13);-- persist zero WND
		elsif func(TCP_100Gb_status_RTT_shift_max + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= RTT_Shift_MAX;	
		elsif func(TCP_100Gb_status_RTT_val + addr_offset) = '1' then
			out_dt(31 downto 00) 	<= RTT_val;	
		elsif func(TCP_100Gb_status_Max_T_between_ack + addr_offset) = '1' then
			out_dt(31 downto 00) 	<=	Max_T_between_ack;
		elsif func(TCP_100Gb_status_Ack_current_time + addr_offset) = '1' then
			out_dt(31 downto 00) 	<=	Actual_ack;	
		elsif func(TCP_100Gb_counter_nb_ack + addr_offset) = '1' then
			out_dt(31 downto 00) 	<=	NB_ack;	
		elsif func(TCP_100Gb_Accu_ack_timer + addr_offset) = '1' then
			out_dt					<=	Accu_ACK_timer;	
		elsif func(TCP_100Gb_status_MSS_receive + addr_offset) = '1' then
			out_dt(31 downto 00) 	<=	MSS_rcv;	   
		elsif func(TCP_100Gb_counter_rcv_ack + addr_offset) = '1' then
			out_dt					<=	ACK_rcv_cnt;	  
		elsif func(TCP_100Gb_tcp_state + addr_offset) = '1' then
			out_dt(2 downto 0)		<=	TCP_state(2 downto 0);						
		elsif func(TCP_100Gb_test + addr_offset) = '1' then
			out_dt 					<=	x"4564560065465400";			
		-- elsif func(32) = '1' then
			-- out_dt(31 downto 00) <=	;			
		end if;

end if;
end process;

Ctrl_data_out <= out_dt;
 
end behavioral;
