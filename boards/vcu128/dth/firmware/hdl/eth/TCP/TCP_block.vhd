LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.interface.all;
use work.address_table.all; 
 
--ENTITY-----------------------------------------------------------
entity TCP_block is
	generic (TCP_Address_Offset				: integer);
	port
	(
		usr_clk							    : in std_logic;
		usr_rst_n						    : in std_logic;
		usr_func_wr						    : in std_logic_vector(16383 downto 0); 
		usr_wen							    : in std_logic;
		usr_data_wr					   		: in std_logic_vector(63 downto 0); 
		                              
		usr_func_rd						    : in std_logic_vector(16383 downto 0); 
		usr_rden							: in std_logic;
		usr_data_rd						    : out std_logic_vector(63 downto 0);
		usr_rd_val						    : out std_logic;
		
		TCP_clock							: in std_logic;
		TCP_ack_rcv				 			: in TCP_ack_rcvo_record;
		TCP_Ack_present						: in std_logic;
		rd_TCP_val_rcvo						: out std_logic;
		
		HBM_MEM_req_rd_blk					: out std_logic;
		HBM_MEM_Size_request				: out std_logic_vector(31 downto 0); -- number of bytes
		HBM_MEM_Add_request					: out std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes) 		
		
		HBM_blk_ready						: in std_logic;
		HBM_TCP_block						: out HBM_to_TCP_block_record;
		TCP_request_took					: in std_logic;
		TCP_Packet_Done						: in std_logic;
		
		TCP_Buf_SND_NEW                   	: in std_logic_vector(31 downto 0);
		TCP_Buf_SND_UNA                   	: out std_logic_vector(31 downto 0);
		TCP_Buf_VALID_P						: out std_logic;
		
		counter_ena_read_block              : out std_logic;
		close_FED							: out std_logic
	 );
end TCP_block;
 
--ARCHITECTURE------------------------------------------------------
architecture behavioral of TCP_block is

COMPONENT TCP_manage_0x IS
 generic (	constant TCP_THRESHOLD_RTTSHIFT	: std_logic_vector(7 downto 0):= x"06";
				constant TCP_MAX_RTTSHIFT			: std_logic_vector(7 downto 0):= x"0C";
				constant WINDOW						: std_logic_vector(7 downto 0):= x"01"
			-- constant snd_cwnd			: std_logic_vector(31 downto 0):= x"00020000";
			-- constant timer_rtt_init		: std_logic_vector(31 downto 0):= x"0002625A"; 
			-- constant timer_rtt_init_sync : std_logic_vector(31 downto 0):= x"12A05F20";
			-- constant timer_persist_init  : std_logic_vector(31 downto 0):= x"0004C4B4"
	 );
port (
	reset_n								            : in std_logic;  -- Active low
	rst_local							            : in std_logic;  -- Active high
	clock									        : in std_logic;

	ISS									            : in std_logic_vector(31 downto 0); -- Initial Segment Number
	f_Push								            : in std_logic;
	Req_Sync								        : in std_logic;
	Req_Rst								            : in std_logic;
	MSS_srv								            : in std_logic_vector(31 downto 0); --value received from server
	MSS_set								            : in std_logic_vector(15 downto 0); --value set by the controller software 
	-- value receive in a packet
	rcv_pckt								        : in std_logic;
	rd_val								            : out std_logic;	
	rcv_seg_SEQ_i      					            : in std_logic_vector(31 downto 0);
	rcv_seg_ACK_i      					            : in std_logic_vector(31 downto 0);
	rcv_WIN_i							            : in std_logic_vector(31 downto 0);-- value WINDOW shift by option 2 (if used)
	rcv_FLAG_i 	       						        : in std_logic_vector(7 downto 0);
	rcv_len_i							            : in std_logic_vector(15 downto 0); -- len in case of we receive data with ack ?? normally not allow
	-- value send to build TCP_IP headers for ACK packet
	Acpt_rq_ack_pkt					                : in std_logic; -- accept parameters for TCP packet
	wen_rq_ack_pkt						            : out std_logic;-- send a TCP packet parameters
	FLAG_out								        : out std_logic_vector(7 downto 0);
	SEQ_out								            : out std_logic_vector(31 downto 0);
	ACK_out								            : out std_logic_vector(31 downto 0);
	option_out							            : out std_logic_vector(2 downto 0);

	-- value send to build TCP_IP headers for DATA/SYNC packet
	Acpt_rq_Send_Pkt					            : in std_logic; -- accept parameters for TCP packet
	wen_rq_Send_pkt					                : out std_logic;-- send a TCP packet parameters
	FLAG_Send_out						            : out std_logic_vector(7 downto 0);
	SEQ_Send_out						            : out std_logic_vector(31 downto 0);
	ACK_Send_out						            : out std_logic_vector(31 downto 0);
	option_Send_out					                : out std_logic_vector(2 downto 0);
	Length_Send_out					                : out std_logic_vector(31 downto 0);
	pckt_wo_dt							            : out std_logic;


	-- signal between SEND and RECEIVE
	Get_TIMESTAMP						            : in std_logic_vector(31 downto 0);
	-- value from/to the SENDER part
	Pkt_done								        : in std_logic;
	snd_new								            : in std_logic_vector(31 downto 0);
	Pointer_read						            : out std_logic_vector(31 downto 0);
	valid_new_p							            : out std_logic;
	no_delay								        : in std_logic;
	state_o								            : out std_logic;
	statistic							            : out std_logic_vector(18 downto 0);
	Stat_flag							            : out std_logic_vector(31 downto 0);
	f_fastretransmit_disabled		                : in std_logic;
	snd_cwnd								        : in std_logic_vector(31 downto 0);
	load_new_snd_cwnd					            : in std_logic;
	timer_rtt_init						            : in std_logic_vector(31 downto 0); 
	timer_rtt_init_sync 				            : in std_logic_vector(31 downto 0);
	timer_persist_init  				            : in std_logic_vector(31 downto 0);
	tcprexmtthresh						            : in std_logic_vector(31 downto 0);
	timer_stop_disable				                : in std_logic;
	TCP_REXMT_CWND_SHIFT				            : in std_logic_vector(31 downto 0);
	measure_RTT_max					                : out std_logic_vector(31 downto 0);
	measure_RTT_min					                : out std_logic_vector(31 downto 0);
	Measure_RTT_cnt					                : out std_logic_vector(31 downto 0);
	Measure_RTT_Accu					            : out std_logic_vector(63 downto 0);
	stat_wnd_max						            : out std_logic_vector(31 downto 0);
	stat_wnd_min						            : out std_logic_vector(31 downto 0);
	Stat_snd_byte						            : out std_logic_vector(63 downto 0);
	stat_snd_rexmit_byte				            : out std_logic_vector(63 downto 0);
	stat_dupack_max				                    : out std_logic_vector(31 downto 0);
	rtt_shifts_MAX						            : out std_logic_vector(31 downto 0);
	rtt_v									        : out std_logic_vector(31 downto 0);
	TCP_state_o							            : out std_logic_vector( 2 downto 0); -- (0) closed  -- (1) sync sent -- (2)  Established
	Persist_state						            : out std_logic
	);
end component;

signal rd_TCP_val_rcvo_cell						: std_logic;
signal rd_TCP_val_rcvo_regdel					: std_logic;

signal Eth_req_return_reg						: TCP_req_record;
signal TCP_scale_HOME							: std_logic_vector( 7 downto 0);
signal TCP_win_HOME								: std_logic_vector(15 downto 0);
signal TCP_TS									: std_logic_vector(31 downto 0);
signal TCP_TS_rply								: std_logic_vector(31 downto 0);

signal TCP_Stat_cnt_ena 		            	: std_logic_vector(18 downto 0);
signal TCP_Stat_flags 		            		: std_logic_vector(31 downto 0);
signal snd_cwnd									: std_logic_vector(31 downto 0);
signal load_new_snd_cwnd_sync		    	    : std_logic;
signal timer_rtt_I							    : std_logic_vector(31 downto 0); 
signal timer_rtt_I_sync				    	    : std_logic_vector(31 downto 0);
signal timer_persist_I				    	    : std_logic_vector(31 downto 0);
signal thresold_retrans				    	    : std_logic_vector(31 downto 0); 
signal TCP_Rexmt_CWND_sh					    : std_logic_vector(31 downto 0);
signal TCP_measure_RTT_max				        : std_logic_vector(31 downto 0);
signal TCP_measure_RTT_min				        : std_logic_vector(31 downto 0);
signal TCP_Measure_RTT_cnt				        : std_logic_vector(31 downto 0);
signal TCP_Measure_RTT_Accu					    : std_logic_vector(63 downto 0);
signal TCP_stat_wnd_max						    : std_logic_vector(31 downto 0);
signal TCP_stat_wnd_min						    : std_logic_vector(31 downto 0);
signal TCP_Stat_snd_byte					    : std_logic_vector(63 downto 0);
signal TCP_stat_snd_rexmit_byte				    : std_logic_vector(63 downto 0);
signal TCP_stat_dupack_max				        : std_logic_vector(31 downto 0);
signal TCP_rtt_shifts_MAX					    : std_logic_vector(31 downto 0);
signal TCP_rtt_v								: std_logic_vector(31 downto 0);
signal TCP_state_o							    : std_logic_vector( 2 downto 0); -- (0) closed  -- (1) sync sent -- (2)  Established
signal TCP_Persist_state					    : std_logic;

signal TCP_Buf_resync_new_pointer              	: std_logic_vector(31 downto 0);
signal Rst_TCP_clock_n							: std_logic;
signal counter_rst								: std_logic;
signal counter_rst_resync						: std_logic;
signal request_sync_con							: std_logic_vector(1 downto 0); 
signal request_reset_con						: std_logic_vector(1 downto 0); 
signal TCP_P0_statistic_val						: std_logic_vector(63 downto 0); 
signal timestamp								: std_logic_vector(63 downto 0);
 
signal Req_read_HBM								: std_logic;
signal Req_read_HBM_pulse						: std_logic;
 
signal TCP_wen_rq_ack_pkt						: std_logic;
signal TCP_wen_rq_Send_pkt						: std_logic;
signal TCP_ack_ACK_requestn						: std_logic;
signal TCP_ack_SND_requestn						: std_logic;
signal TCP_FLAG_R_Ack_Pkt						: std_logic_vector(7 downto 0);
signal TCP_SEQ_R_Ack_Pkt						: std_logic_vector(31 downto 0);
signal TCP_ACK_R_Ack_Pkt 						: std_logic_vector(31 downto 0);
signal TCP_Opt_R_Ack_Pkt						: std_logic_vector(2 downto 0); 
signal TCP_FLAG_Send						    : std_logic_vector(7 downto 0);
signal TCP_SEQ_Send						        : std_logic_vector(31 downto 0);
signal TCP_ACK_Send						        : std_logic_vector(31 downto 0);
signal TCP_option_Send					        : std_logic_vector(2 downto 0);
signal TCP_Length_Send					        : std_logic_vector(31 downto 0);
signal TCP_pckt_wo_dt							: std_logic;
	 
signal TCP_ack_Send_req							: std_logic; 
signal TCP_ack_Ack_req							: std_logic; 
signal ETH_SND_access                           : std_logic;
signal ETH_ACK_access                           : std_logic;
signal ETH_request								: std_logic;
signal ETH_request_sent							: std_logic; 
signal TCP_ST_A_Seq_num							: std_logic_vector(31 downto 0);
signal TCP_ST_A_Ack_num							: std_logic_vector(31 downto 0);
signal TCP_ST_A_Length							: std_logic_vector(31 downto 0);
signal TCP_ST_A_Flag							: std_logic_vector(7 downto 0);
signal TCP_ST_A_Option							: std_logic_vector(2 downto 0);
signal TCP_ST_A_wo_data							: std_logic ;
signal TCP_ST_S_Seq_num							: std_logic_vector(31 downto 0);
signal TCP_ST_S_Ack_num							: std_logic_vector(31 downto 0);
signal TCP_ST_S_Length							: std_logic_vector(31 downto 0);
signal TCP_ST_S_Flag							: std_logic_vector(7 downto 0);
signal TCP_ST_S_Option							: std_logic_vector(2 downto 0);
signal TCP_ST_S_wo_data							: std_logic ; 


component Ctrl_reg_TCP is
 	generic (addr_offset		: integer := 0);
	port
	(CLOCK						: IN STD_LOGIC;
	 RESET						: IN STD_LOGIC;
			
	Ctrl_DT						: IN STD_LOGIC_VECTOR(63 downto 0);
	Ctrl_func					: IN STD_LOGIC_VECTOR(16383 downto 0); 
	Ctrl_wr						: IN STD_LOGIC; 
	Ctrl_func_rd				: IN STD_LOGIC_VECTOR(16383 downto 0);  								
	Ctrl_out					: OUT STD_LOGIC_VECTOR(63 downto 0);
		 
	TCP_scale					: OUT STD_LOGIC_VECTOR(7 downto 0);
	TCP_win_Home				: OUT STD_LOGIC_VECTOR(15 downto 0);
	TCP_win_Host				: OUT STD_LOGIC_VECTOR(31 downto 0); 
	TCP_MMS_Max					: OUT STD_LOGIC_VECTOR(15 downto 0);
	TCP_MSS_Host				: OUT STD_LOGIC_VECTOR(15 downto 0);
	TCP_TS						: OUT STD_LOGIC_VECTOR(31 downto 0);
	TCP_TS_rply					: OUT STD_LOGIC_VECTOR(31 downto 0);
	snd_cwnd					: OUT STD_LOGIC_VECTOR(31 downto 0); 
	load_new_snd_cwnd			: OUT STD_LOGIC;
	timer_rtt_I					: OUT STD_LOGIC_VECTOR(31 downto 0); 
	timer_rtt_I_sync			: OUT STD_LOGIC_VECTOR(31 downto 0);
	timer_persist_I				: OUT STD_LOGIC_VECTOR(31 downto 0);
	TCP_Rexmt_CWND_sh			: OUT STD_LOGIC_VECTOR(31 downto 0);
	Init_bit					: OUT STD_LOGIC_VECTOR(15 downto 0);
	thresold_retrans			: OUT STD_LOGIC_VECTOR(31 downto 0);
	
	resetn_TCP_clock            : in std_logic;
	TCP_clock                   : in  std_logic;
	Open_connection             : in  std_logic;
		
	Receive_ACK_pckt			: in std_logic;
	TCP_win_from_HOST			: in std_logic_vector(15 downto 0);
	new_Scale             		: in std_logic_vector(7 downto 0);
    update_Scale          		: in std_logic;
    new_MMS               		: in std_logic_vector(15 downto 0);
    update_MMS            		: in std_logic;

	TCP_Stream_number			: out std_logic_vector(7 downto 0) 
	);
end component;

signal load_new_snd_cwnd		: std_logic;
signal TCP_MSS_rcv				: STD_LOGIC_VECTOR(15 downto 0);
signal TCP_max_size				: STD_LOGIC_VECTOR(15 downto 0);
signal TCP_win_HOST				: STD_LOGIC_VECTOR(31 downto 0);
signal Init_bit					: STD_LOGIC_VECTOR(15 downto 0);
signal Ctrl_register_out		: STD_LOGIC_VECTOR(63 downto 0);
signal TCP_Stream_number		: STD_LOGIC_VECTOR(7 downto 0);

component statistic_TCP is
generic (addr_offset		: integer := 0);
port (
	reset						: in std_logic;
	local_resetn			    : in std_logic;
	TCP_clock				    : in std_logic;
	latch_rcv_ack				: in std_logic;
	 
	Ctrl_clock				    : in std_logic;
	latch_value_to_be_rd	    : in std_logic;
	ena_stat					: in std_logic_vector(18 downto 0);
	Stat_reg					: in std_logic_vector(31 downto 0);
	stat_send_byte			    : in std_logic_vector(63 downto 0):= (others => '0');
	stat_snd_rexmit_byte	    : in std_logic_vector(63 downto 0):= (others => '0');
	stat_dupack_max		        : in std_logic_vector(31 downto 0):= (others => '0');
	stat_win_max			    : in std_logic_vector(31 downto 0):= (others => '0');
	stat_win_min			    : in std_logic_vector(31 downto 0):= (others => '0');
	measure_RTT_max		        : in std_logic_vector(31 downto 0):= (others => '0');
	measure_RTT_min		        : in std_logic_vector(31 downto 0):= (others => '0');
	measure_RTT_sum		        : in std_logic_vector(63 downto 0):= (others => '0'); 
	RTT_Shift_MAX			    : in std_logic_vector(31 downto 0):= (others => '0');
	RTT_Val					    : in std_logic_vector(31 downto 0):= (others => '0');
	MSS_rcv					    : in std_logic_vector(31 downto 0):= (others => '0'); 
	ACK_rcv					    : in std_logic; 
	TCP_state					: in std_logic_vector(2 downto 0); 
	func						: in std_logic_vector(16383 downto 0);
	Ctrl_data_out			    : out std_logic_vector(63 downto 0)
	);
end component;

signal latch_for_mon                : std_logic;
 
component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic; 
	Resetn_sync		    : out std_logic;
	Resetp_sync		    : out std_logic
	);
end component;

component resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic; 
	Resetp_sync		    : out std_logic
	);
end component;

component resync_v4 is
port (
	aresetn				: in std_logic;
	clocki			    : in std_logic;	
	input				: in std_logic;
	clocko			    : in std_logic;
	output			    : out std_logic
	);
end component;

component resync_sig_gen is 
port ( 
	clocko				: in std_logic;
	input				: in std_logic;
	output				: out std_logic
	);
end component;

signal counter_ena_read_block_reg                           : std_logic;

attribute mark_debug                                        : string; 
--attribute mark_debug of ETH_request_sent	            : signal is "true";  
--attribute mark_debug of HBM_TCP_block			            : signal is "true";    
--attribute mark_debug of ETH_request			            : signal is "true";    
--attribute mark_debug of ETH_SND_access			            : signal is "true";    
--attribute mark_debug of ETH_ACK_access			            : signal is "true";    
--attribute mark_debug of TCP_ack_SEND_req			            : signal is "true";    
--attribute mark_debug of HBM_blk_ready			            : signal is "true";    
--attribute mark_debug of Req_read_HBM			            : signal is "true";    
--attribute mark_debug of Req_read_HBM_pulse			            : signal is "true";    
--attribute mark_debug of TCP_ST_S_Length			            : signal is "true";    
--attribute mark_debug of TCP_ST_S_Seq_num			            : signal is "true";    

--***************************************************************** 
--*************<     CODE START HERE    >**************************
--***************************************************************** 
 begin 
 
 
resync_reset_i1:resetn_resync  
port map(
	aresetn				=> usr_rst_n,
	clock				=> TCP_clock,
	Resetn_sync		    => Rst_TCP_clock_n 
	);
			
 
 --*******************************************************************************************
--   TCP logic for TCP_Buf 0 and 1

--process(TCP_clock)
--begin
--	if rising_edge(TCP_clock) then
TCP_Buf_resync_new_pointer	<= TCP_Buf_SND_NEW;
--	end if;
--end process;

process(usr_clk)
begin
	if rising_edge(usr_clk) then
		request_sync_con(0)			<= '0';
		if usr_wen = '1' then
			if usr_func_wr(TCP_100Gb_counter_reset + TCP_Address_Offset) = '1' then
				counter_rst 	<= usr_data_wr(0);
			end if;
			
			if usr_func_wr(TCP_100Gb_ETH_request_status + TCP_Address_Offset) = '1' then
				request_sync_con(0) 	<= usr_data_wr(4);
				request_reset_con(0) 	<= usr_data_wr(5);
				latch_for_mon       	<= usr_data_wr(30);
			end if;
			
		end if;
	end if;
end process;
 
 --************************************************
-- open con reset con ARP probe	ARP request...
 -- resync open_conn
Open_con_sync_i0_pci_xgmii_sync:resync_v4 
	port map(
		aresetn			=> usr_rst_n					,		 
		clocki			=> usr_clk						,		 
		input			=> request_sync_con(0)			,		 
		clocko			=> TCP_clock					,	 
		output			=> request_sync_con(1)		 	 
		);				
 
Open_con_reset_i0_pci_xgmii_sync:resync_v4 
	port map(
		aresetn			=> usr_rst_n					,		 
		clocki			=> usr_clk						,		 
		input			=> request_reset_con(0)			,		 
		clocko			=> TCP_clock					,	 
		output			=> request_reset_con(1)		 	 
		);	
 --************************************************
		
resync_reset_i2:resetp_resync  
port map(
	aresetp				=> counter_rst,
	clock				=> TCP_clock,
	Resetp_sync		    => counter_rst_resync 
	);
		
	
TCP_State_machine_i0:TCP_manage_0x  
port map(
	reset_n								    => Rst_TCP_clock_n				   		        , -- Active low
	rst_local							    => counter_rst_resync			                , -- Active high
	clock									=> TCP_clock						            ,
	ISS									    => x"FFFFFFFF"						            , -- Initial Segment Number
	f_Push								    => Init_bit(0)						            ,			
	Req_Sync								=> request_sync_con(1)			                ,			
	Req_Rst									=> request_reset_con(1)			            ,			
	MSS_srv(15 downto 0)				    => TCP_MSS_rcv						        ,			 --value received from server
	MSS_srv(31 downto 16)			        => x"0000" 							            ,			 --value received from server
	MSS_set								    => TCP_max_size					                ,			 --value set by the controller software 
	-- value receive in a packet   
	rcv_pckt								=> TCP_Ack_present				 	        ,			
	rd_val								    => rd_TCP_val_rcvo_cell					        ,			
	rcv_seg_SEQ_i						    => TCP_ack_rcv.TCP_Px_seq_num_rcv					        ,			
	rcv_seg_ACK_i							=> TCP_ack_rcv.TCP_Px_ack_num_rcv					        ,			
	rcv_WIN_i								=> TCP_win_HOST 					        ,			-- value WINDOW shift by option 2 (if used)
	rcv_FLAG_i								=> TCP_ack_rcv.TCP_Px_flag_num_rcv					        ,			
	rcv_len_i								    => TCP_ack_rcv.TCP_Px_dt_len_rcv					        ,			 -- len in case of we receive data with ack ?? normally not allow
	-- value send to build TCP_IP headers for ACK packet
	Acpt_rq_ack_pkt					        => TCP_ack_ACK_requestn		        				,			-- accept parameters for TCP packet
	wen_rq_ack_pkt						    => TCP_wen_rq_ack_pkt					    ,			-- send a TCP packet parameters
	FLAG_out								=> TCP_FLAG_R_Ack_Pkt					    ,			
	SEQ_out								    => TCP_SEQ_R_Ack_Pkt					        ,			
	ACK_out								    => TCP_ACK_R_Ack_Pkt 					    ,			
	option_out							    => TCP_Opt_R_Ack_Pkt					        ,			
	-- value send to build TCP_IP headers for DATA/SYNC packet
	Acpt_rq_Send_Pkt					    => TCP_ack_SND_requestn 			        				,			-- accept parameters for TCP packet
	wen_rq_Send_pkt					        => TCP_wen_rq_Send_pkt			    ,			-- send a TCP packet parameters
	FLAG_Send_out						    => TCP_FLAG_Send			        ,			
	SEQ_Send_out						    => TCP_SEQ_Send				    ,			
	ACK_Send_out						    => TCP_ACK_Send				    ,			
	option_Send_out					        => TCP_option_Send			        ,			
	Length_Send_out					        => TCP_Length_Send			        ,			
	pckt_wo_dt							    => TCP_pckt_wo_dt				    ,			
	-- signal between SEND and RECEIVE
	Get_TIMESTAMP						    => timestamp(31 downto 0)		                ,			
	-- value from/to the SENDER part
	Pkt_done								=> TCP_request_took				            ,			--: inform that the packet is sent
	snd_new								    => TCP_Buf_resync_new_pointer				    ,			
	Pointer_read						    => TCP_Buf_SND_UNA						    	,			
	valid_new_p							    => TCP_Buf_VALID_P						    	,			
	no_delay								=> Init_bit(2)						            ,			
	-- state_o								=> 			                                    ,				
	statistic							    => TCP_Stat_cnt_ena(18 downto 0)	 	        ,			
	Stat_flag							    => TCP_Stat_flags(31 downto 0)		        ,			
	f_fastretransmit_disabled		        => Init_bit(3)						            ,			
	snd_cwnd								=> snd_cwnd							            ,			
	load_new_snd_cwnd					    => load_new_snd_cwnd_sync		                ,			
	timer_rtt_init						    => timer_rtt_I						            ,			
	timer_rtt_init_sync 				    => timer_rtt_I_sync				                ,			
	timer_persist_init  				    => timer_persist_I				                ,			
	tcprexmtthresh						    => thresold_retrans				                ,			
	timer_stop_disable				        => Init_bit(4)						            ,			
	TCP_REXMT_CWND_SHIFT				    => TCP_Rexmt_CWND_sh				            ,			
	measure_RTT_max					        => TCP_measure_RTT_max				        ,			
	measure_RTT_min					        => TCP_measure_RTT_min				        ,			
	Measure_RTT_cnt					        => TCP_Measure_RTT_cnt				        ,			
	Measure_RTT_Accu					    => TCP_Measure_RTT_Accu				        ,			
	stat_wnd_max						    => TCP_stat_wnd_max					        ,			
	stat_wnd_min						    => TCP_stat_wnd_min					        ,			
	Stat_snd_byte						    => TCP_Stat_snd_byte					        ,		
	stat_snd_rexmit_byte				    => TCP_stat_snd_rexmit_byte			        ,			
	stat_dupack_max					        => TCP_stat_dupack_max				        ,			
	rtt_shifts_MAX						    => TCP_rtt_shifts_MAX				        ,			
	rtt_v									=> TCP_rtt_v							        ,		
	TCP_state_o							    => TCP_state_o						        ,		 -- (0) closed  -- (1) sync sent -- (2)  Established
	Persist_state						    => TCP_Persist_state						 
	);

-- use to reset registers when the TCP port is closed
close_fed 		<= TCP_state_o(0);
rd_TCP_val_rcvo	<= rd_TCP_val_rcvo_regdel;--rd_TCP_val_rcvo_cell;

process(TCP_clock)
begin
	if rising_edge(TCP_clock) then
		rd_TCP_val_rcvo_regdel	<= rd_TCP_val_rcvo_cell;
	end if;
end process;

-- Request to send Ethernet Packet with and without data
 
	
process(TCP_clock)
begin
	if rising_edge(TCP_clock)  then
		if 		TCP_wen_rq_Send_pkt = '1' then
			TCP_ST_S_Seq_num	     <= TCP_SEQ_Send;
			TCP_ST_S_Ack_num	     <= TCP_ACK_Send;
			TCP_ST_S_Length          <= TCP_Length_Send;
			TCP_ST_S_Flag            <= TCP_FLAG_Send;
			TCP_ST_S_Option          <= TCP_option_Send;
			TCP_ST_S_wo_data	     <= TCP_pckt_wo_dt;
			
		elsif 	TCP_wen_rq_ack_pkt = '1' then
			TCP_ST_A_Seq_num	     <= TCP_SEQ_R_Ack_Pkt;
			TCP_ST_A_Ack_num	     <= TCP_ACK_R_Ack_Pkt;
			TCP_ST_A_Length          <= (others => '0');
			TCP_ST_A_Flag            <= TCP_FLAG_R_Ack_Pkt;
			TCP_ST_A_Option          <= TCP_Opt_R_Ack_Pkt;
			TCP_ST_A_wo_data         <= '1';
		end if;
	end if;
end process;	

HBM_MEM_req_rd_blk					<= Req_read_HBM_pulse;
HBM_MEM_Size_request				<= TCP_ST_S_Length;
HBM_MEM_Add_request					<= TCP_ST_S_Seq_num;

process(Rst_TCP_clock_n,TCP_clock)
begin
	if Rst_TCP_clock_n = '0' then
		Req_read_HBM		<= '0'; 
		Req_read_HBM_pulse	<= '0'; 
		ETH_request			<= '0';
		TCP_ack_Send_req	<= '0';
		TCP_ack_Ack_req	    <= '0';
		ETH_SND_access	    <= '0';
		ETH_ACK_access	    <= '0';
	elsif rising_edge(TCP_clock) then
	    Req_read_HBM_pulse      <= '0';
		if Req_read_HBM = '1' and HBM_blk_ready = '1'	 and ETH_SND_access = '1'then  -- and TCP_Packet_Done = '1' 
			Req_read_HBM		<= '0';
		elsif TCP_wen_rq_Send_pkt = '1' and TCP_pckt_wo_dt = '0' and Req_read_HBM = '0' then--request a block transfer
			Req_read_HBM 		<= '1';
			Req_read_HBM_pulse  <= '1';
		end if;
	 
		if 		(ETH_SND_access = '1' and TCP_Packet_Done = '1' ) then
			TCP_ack_SEND_req		<= '0';
		elsif 	TCP_wen_rq_Send_pkt = '1' then-- request a TCP packet without data 
			TCP_ack_SEND_req		<= '1';
		end if;		
		 		
		if 		(ETH_ACK_access = '1' and TCP_Packet_Done = '1' ) then
			TCP_ack_ACK_req		<= '0';
		elsif 	(TCP_wen_rq_ack_pkt = '1') then												-- request a TCP Ack
			TCP_ack_ACK_req		<= '1';
		end if;		
		
		if TCP_Packet_Done = '1'     then
            ETH_ACK_access  <= '0';
            ETH_SND_access  <= '0'; 
		elsif TCP_ack_ACK_req = '1'                                                       and ETH_ACK_access = '0' and ETH_SND_access = '0' then
            ETH_ACK_access  <= '1'; 
		elsif TCP_ack_SEND_req = '1'  and (HBM_blk_ready = '1' or TCP_ST_S_wo_data = '1') and ETH_ACK_access = '0' and ETH_SND_access = '0' and TCP_ack_ACK_req = '0' then
            ETH_SND_access  <= '1'; 
        end if;     
            
            
		if TCP_Packet_Done = '1'	then
			ETH_request			<= '0';
		elsif 	(ETH_SND_access = '1' or ETH_ACK_access = '1' )  and ETH_request = '0'   then
			ETH_request 		<= '1';
		end if;

	end if;
end process;
 	
TCP_ack_SND_requestn				    <= not(TCP_ack_SEND_req);
TCP_ack_ACK_requestn				    <= not(TCP_ack_ACK_req);


-- mux from 5 TCP engines and store values in Registers


process(Rst_TCP_clock_n,TCP_clock)
begin
    if Rst_TCP_clock_n = '0' then
        ETH_request_sent                <= '0';
    elsif rising_edge(TCP_clock) then 
        if TCP_Packet_Done = '1'	then
            ETH_request_sent            <= '0';
        elsif     ETH_request = '1'   and (ETH_SND_access = '1' or  ETH_ACK_access = '1') then
            ETH_request_sent            <= '1';
        end if;
    end if;
end process;

process(TCP_clock)
begin
	if rising_edge(TCP_clock) then
		if     ETH_request = '1'   and ETH_SND_access = '1' then
			Eth_req_return_reg.TCP_SEQ_Send			<= TCP_ST_S_Seq_num;
			Eth_req_return_reg.TCP_ACK_Send			<= TCP_ST_S_Ack_num;
			Eth_req_return_reg.TCP_FLAG_Send 		<= TCP_ST_S_Flag;
			Eth_req_return_reg.TCP_option_Send 		<= TCP_ST_S_Option;
			Eth_req_return_reg.TCP_Length_Send		<= TCP_ST_S_Length;
			Eth_req_return_reg.TCP_pckt_wo_dt		<= TCP_ST_S_wo_data; 
		elsif   ETH_request = '1'   and ETH_ACK_access = '1' then
			Eth_req_return_reg.TCP_SEQ_Send			<= TCP_ST_A_Seq_num;
			Eth_req_return_reg.TCP_ACK_Send			<= TCP_ST_A_Ack_num;
			Eth_req_return_reg.TCP_FLAG_Send 		<= TCP_ST_A_Flag;
			Eth_req_return_reg.TCP_option_Send 		<= TCP_ST_A_Option;
			Eth_req_return_reg.TCP_Length_Send		<= x"00000000";
			Eth_req_return_reg.TCP_pckt_wo_dt		<= '1'; 
		end if;
	end if;
end process;

HBM_TCP_block.Request_TCP_packet	<= ETH_request_sent;
HBM_TCP_block.Sel_TCP_Stream		<= TCP_Stream_number;			
HBM_TCP_block.TCP_SEQ_Send			<= Eth_req_return_reg.TCP_SEQ_Send;		
HBM_TCP_block.TCP_ACK_Send			<= Eth_req_return_reg.TCP_ACK_Send;		
HBM_TCP_block.TCP_FLAG_Send 		<= Eth_req_return_reg.TCP_FLAG_Send ;	
HBM_TCP_block.TCP_option_Send 		<= Eth_req_return_reg.TCP_option_Send ;	
HBM_TCP_block.TCP_Length_Send		<= Eth_req_return_reg.TCP_Length_Send;	
HBM_TCP_block.TCP_pckt_wo_dt		<= Eth_req_return_reg.TCP_pckt_wo_dt;	
HBM_TCP_block.TCP_max_size			<= TCP_max_size;	
HBM_TCP_block.TCP_scale				<= TCP_scale_HOME;	
HBM_TCP_block.TCP_win				<= TCP_win_HOME;	
HBM_TCP_block.TCP_TS				<= TCP_TS;	
HBM_TCP_block.TCP_TS_rply			<= TCP_TS_rply;	
HBM_TCP_block.HBM_PL_data			<= (others => '0');
HBM_TCP_block.HBM_PL_ena			<= (others => '0');
HBM_TCP_block.HBM_PL_last			<= '0';
HBM_TCP_block.HBM_blk_CS			<= (others => '0');
HBM_TCP_block.HBM_blk_wen			<= '0'; 
 
 
process(Rst_TCP_clock_n,TCP_clock) 
begin
    if Rst_TCP_clock_n = '0' then
        counter_ena_read_block_reg  <= '0';
    elsif rising_edge(TCP_clock) then
        if Req_read_HBM = '1' then
            counter_ena_read_block_reg <= '1';
        elsif ETH_request_sent = '1' then
            counter_ena_read_block_reg <= '0';
        end if;
    end if;
end process;
  
counter_ena_read_block <= counter_ena_read_block_reg;
 
-- free running counter , used as a TIMESTAMP 
process(TCP_clock)	
begin
	if rising_edge(TCP_clock) then
		timestamp	<= timestamp + '1';
	end if;
end process;
	
Register_setting_i1:Ctrl_reg_TCP  
 	generic map(addr_offset		=> TCP_Address_Offset)
	port map
	(CLOCK						=> usr_clk,--: IN STD_LOGIC;
	 RESET						=> usr_rst_n,--: IN STD_LOGIC;
								 
	Ctrl_DT						=> usr_data_wr,--: IN STD_LOGIC_VECTOR(63 downto 0);
	Ctrl_func					=> usr_func_wr,--: IN STD_LOGIC_VECTOR(16383 downto 0); 
	Ctrl_wr						=> usr_wen,--: IN STD_LOGIC; 
	Ctrl_func_rd				=> usr_func_rd,--: IN STD_LOGIC_VECTOR(16383 downto 0);  								
	Ctrl_out					=> Ctrl_register_out,--: OUT STD_LOGIC_VECTOR(63 downto 0);
	  
	TCP_scale					=> TCP_scale_HOME			,--: OUT STD_LOGIC_VECTOR(7 downto 0);
	TCP_win_Home				=> TCP_win_HOME				,--: OUT STD_LOGIC_VECTOR(15 downto 0);
	TCP_win_Host				=> TCP_win_HOST, 
	TCP_MMS_Max					=> TCP_max_size,
	TCP_MSS_Host				=> TCP_MSS_rcv,
	TCP_TS						=> TCP_TS				,--: OUT STD_LOGIC_VECTOR(31 downto 0);
	TCP_TS_rply					=> TCP_TS_rply			,--: OUT STD_LOGIC_VECTOR(31 downto 0);
	snd_cwnd					=> snd_cwnd			,--: OUT STD_LOGIC_VECTOR(31 downto 0); 
	load_new_snd_cwnd			=> load_new_snd_cwnd	,--: OUT STD_LOGIC;
	timer_rtt_I					=> timer_rtt_I			,--: OUT STD_LOGIC_VECTOR(31 downto 0); 
	timer_rtt_I_sync			=> timer_rtt_I_sync	,--: OUT STD_LOGIC_VECTOR(31 downto 0);
	timer_persist_I				=> timer_persist_I		,--: OUT STD_LOGIC_VECTOR(31 downto 0);
	TCP_Rexmt_CWND_sh			=> TCP_Rexmt_CWND_sh	,--: OUT STD_LOGIC_VECTOR(31 downto 0);
	Init_bit					=> Init_bit			,--: OUT STD_LOGIC_VECTOR(15 downto 0);
	thresold_retrans			=> thresold_retrans	 ,
	
	resetn_TCP_clock            => Rst_TCP_clock_n,
	TCP_clock                   => TCP_clock,
	Open_connection             => request_sync_con(1),
	
	Receive_ACK_pckt			=>rd_TCP_val_rcvo_cell,-- rd_TCP_val_rcvo_regdel,--: in std_logic;
	TCP_win_from_HOST			=> TCP_ack_rcv.TCP_Px_win_num_rcv,--: in std_logic_vector(15 downto 0);
	new_Scale             		=> TCP_ack_rcv.TCP_Px_new_Scale_rcv, 
    update_Scale          		=> TCP_ack_rcv.TCP_Px_update_Scale_rcv, 
    new_MMS               		=> TCP_ack_rcv.TCP_Px_new_MMS_rcv, 
    update_MMS            		=> TCP_ack_rcv.TCP_Px_update_MMS_rcv, 

	TCP_Stream_number			=> TCP_Stream_number
	);	
	
 -- resync load pulse for load_new_snd_cwnd
 load_new_snd_cwnd_sync_i2_pci_xgmii:resync_v4 
	port map(
		aresetn			=> usr_rst_n				,		 
		clocki			=> usr_clk					,		 
		input			=> load_new_snd_cwnd		,		 
		clocko			=> TCP_clock				,	 
		output			=> load_new_snd_cwnd_sync		 	 
		);	


--************************************************
 -- status registers read from PCI_add
		
 stats_i0:statistic_TCP  
generic map(addr_offset						=>  TCP_Address_Offset)
 port map(
	reset									=> usr_rst_n					,				 
	local_resetn						    => counter_rst_resync           ,	--pos 	,					 
	TCP_clock							    => TCP_clock					,
	latch_rcv_ack							=> rd_TCP_val_rcvo_cell,--rd_TCP_val_rcvo_regdel, 
	 
	Ctrl_clock							    => usr_clk						,				 
	latch_value_to_be_rd				    => latch_for_mon				,				 
	ena_stat								=> TCP_Stat_cnt_ena			,		 
	Stat_reg								=> TCP_Stat_flags			,
	stat_send_byte						    => TCP_Stat_snd_byte			,		  
	stat_snd_rexmit_byte				    => TCP_stat_snd_rexmit_byte	,		  
	stat_dupack_max					        => TCP_stat_dupack_max		,		  
	stat_win_max						    => TCP_stat_wnd_max			,		  
	stat_win_min						    => TCP_stat_wnd_min			,		  
	measure_RTT_max					        => TCP_measure_RTT_max		,		  
	measure_RTT_min					        => TCP_measure_RTT_min		,		  
	measure_RTT_sum					        => TCP_Measure_RTT_Accu		,	 	  
	RTT_Shift_MAX						    => TCP_rtt_shifts_MAX		,
	RTT_Val								    => TCP_rtt_v					,
	MSS_rcv(15 downto 0)				    => TCP_MSS_rcv				,		  
	MSS_rcv(31 downto 16)			        => x"0000"						,	 				 
	ACK_rcv								    => rd_TCP_val_rcvo_cell			,-- read a ack parcket received
	TCP_state								=> TCP_state_o					,		 
	func									=> usr_func_rd					,				 
	Ctrl_data_out						    => TCP_P0_statistic_val(63 downto 0)					 	 
	);		

usr_data_rd	<= TCP_P0_statistic_val or Ctrl_register_out;--or 
end behavioral;