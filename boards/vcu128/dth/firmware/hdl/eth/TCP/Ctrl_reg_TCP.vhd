------------------------------------------------------------------------------
--                      PCIe register interface                                 --
------------------------------------------------------------------------------
--   	 
--   
--   Dominique Gigi 2021		 
--								 
--   ver 2.00							 
--  
-- 
-- 
--
--
--
------------------------------------------------------------------------------
LIBRARY ieee;
 

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
use work.address_table.all;
	
entity Ctrl_reg_TCP is
 	generic (addr_offset		: integer := 0);
	port
	(CLOCK						: IN STD_LOGIC;
	 RESET						: IN STD_LOGIC;
			
	Ctrl_DT						: IN STD_LOGIC_VECTOR(63 downto 0);
	Ctrl_func					: IN STD_LOGIC_VECTOR(16383 downto 0); 
	Ctrl_wr						: IN STD_LOGIC; 
	Ctrl_func_rd				: IN STD_LOGIC_VECTOR(16383 downto 0);  								
	Ctrl_out					: OUT STD_LOGIC_VECTOR(63 downto 0);
		
	TCP_scale					: OUT STD_LOGIC_VECTOR(7 downto 0);
	TCP_win_Home				: OUT STD_LOGIC_VECTOR(15 downto 0);
	TCP_win_Host				: OUT STD_LOGIC_VECTOR(31 downto 0);
	TCP_MMS_Max					: OUT STD_LOGIC_VECTOR(15 downto 0);
	TCP_MSS_Host				: OUT STD_LOGIC_VECTOR(15 downto 0);
	Scale_from_Host             : OUT STD_LOGIC_VECTOR(7 downto 0);
	TCP_TS						: OUT STD_LOGIC_VECTOR(31 downto 0);
	TCP_TS_rply					: OUT STD_LOGIC_VECTOR(31 downto 0);
	snd_cwnd					: OUT STD_LOGIC_VECTOR(31 downto 0); 
	load_new_snd_cwnd			: OUT STD_LOGIC;
	timer_rtt_I					: OUT STD_LOGIC_VECTOR(31 downto 0); 
	timer_rtt_I_sync			: OUT STD_LOGIC_VECTOR(31 downto 0);
	timer_persist_I				: OUT STD_LOGIC_VECTOR(31 downto 0);
	TCP_Rexmt_CWND_sh			: OUT STD_LOGIC_VECTOR(31 downto 0);
	Init_bit					: OUT STD_LOGIC_VECTOR(15 downto 0);
	thresold_retrans			: OUT STD_LOGIC_VECTOR(31 downto 0);
	
	resetn_TCP_clock            : in std_logic;
	TCP_clock                   : in  std_logic;
	Open_connection             : in  std_logic;
	
	Receive_ACK_pckt			: in std_logic;
	TCP_win_from_HOST			: in std_logic_vector(15 downto 0);
	new_Scale             		: in std_logic_vector(7 downto 0);
    update_Scale          		: in std_logic;
    new_MMS               		: in std_logic_vector(15 downto 0);
    update_MMS            		: in std_logic;

	TCP_Stream_number			: out std_logic_vector(7 downto 0)
		
	);
end Ctrl_reg_TCP;

architecture behavioral of Ctrl_reg_TCP is 

signal TCP_win_from_HOST_reg        : STD_LOGIC_VECTOR(15 downto 0);
signal TCP_max_size_reg			    : STD_LOGIC_VECTOR(15 downto 0);
signal TCP_scale_reg				: STD_LOGIC_VECTOR(7 downto 0);
signal TCP_win_reg					: STD_LOGIC_VECTOR(15 downto 0);
signal TCP_TS_reg					: STD_LOGIC_VECTOR(31 downto 0);
signal TCP_TS_rply_reg				: STD_LOGIC_VECTOR(31 downto 0);
signal Init_reg					    : STD_LOGIC_VECTOR(15 downto 0);
signal snd_cwnd_reg				    : STD_LOGIC_VECTOR(31 downto 0); 
signal timer_rtt_I_reg				: STD_LOGIC_VECTOR(31 downto 0); 
signal timer_rtt_I_sync_reg		    : STD_LOGIC_VECTOR(31 downto 0); 
signal timer_persist_I_reg			: STD_LOGIC_VECTOR(31 downto 0); 
signal TCP_Rexmt_CWND_sh_reg		: STD_LOGIC_VECTOR(31 downto 0); 
signal thresold_retrans_reg		    : STD_LOGIC_VECTOR(31 downto 0); 
signal load_new_snd_cwnd_reg		: std_logic;
signal option_SCALE				    : STD_LOGIC_VECTOR(7 downto 0); 
signal MSS_reg						: STD_LOGIC_VECTOR(15 downto 0); 
signal TCP_stream_number_reg		: std_logic_vector(7 downto 0) := x"FF";
signal HOST_TCP_win_reg             : std_logic_vector(31 downto 0);

signal Ctrl_out_rg					: STD_LOGIC_VECTOR(63 downto 0); 

attribute mark_debug                                            : string; 
--attribute mark_debug of option_SCALE	                        : signal is "true";  
--attribute mark_debug of TCP_win_from_HOST			            : signal is "true";    
--attribute mark_debug of HOST_TCP_win_reg			            : signal is "true";   

--attribute mark_debug of MSS_reg			                        : signal is "true";   
--attribute mark_debug of Open_connection			                : signal is "true";   
--attribute mark_debug of TCP_max_size_reg			            : signal is "true";   
--attribute mark_debug of new_MMS			                        : signal is "true";   
--attribute mark_debug of update_MMS			                    : signal is "true";   
--attribute mark_debug of update_Scale		                    : signal is "true";   
--attribute mark_debug of Receive_ACK_pckt			            : signal is "true"; 
  
--attribute mark_debug of snd_cwnd_reg			                        : signal is "true";   
--****************************************************************************
--*******************************  BEGINNING *********************************
--****************************************************************************
BEGIN

 
process(reset,clock)
begin
if reset = '0' then
 	TCP_max_size_reg					<= x"2300";
	TCP_scale_reg						<= x"01";
	TCP_win_reg							<= x"0400";
	TCP_TS_reg							<= (others => '0');
	TCP_TS_rply_reg						<= (others => '0');
	Init_reg							<= (others => '0');
	TCP_Rexmt_CWND_sh_reg				<= x"00000006";
	snd_cwnd_reg						<= x"00100000";
	timer_rtt_I_reg						<= x"0004c4b4";
	timer_rtt_I_sync_reg				<= x"12A05F20";
	timer_persist_I_reg					<= x"00098968";
	thresold_retrans_reg				<= x"00000003"; 
 
elsif rising_edge(clock) then
	load_new_snd_cwnd_reg				<= '0';	
	if Ctrl_wr = '1' then
		if Ctrl_func(TCP_100Gb_ctrl_Init_def + addr_offset)  = '1' then
			TCP_win_reg						<= Ctrl_dt(15 downto 0);
			Init_reg						<= Ctrl_dt(31 downto 16);
		end if;
		if Ctrl_func(TCP_100Gb_ctrl_Max_size_Scale_def + addr_offset)  = '1' then
			TCP_max_size_reg				<= Ctrl_dt(15 downto 0);
			TCP_scale_reg					<= Ctrl_dt(23 downto 16);
		end if;
		if Ctrl_func(TCP_100Gb_ctrl_timestamp_def + addr_offset)  = '1' then
			TCP_TS_reg						<= Ctrl_dt(31 downto 0);		
		end if;
		if Ctrl_func(TCP_100Gb_ctrl_timestamp_reply_def + addr_offset)  = '1' then
			TCP_TS_rply_reg	 				<= Ctrl_dt(31 downto 0);
		end if;
		 	
		if Ctrl_func(TCP_100Gb_ctrl_CongWind_def + addr_offset)  = '1' then
			snd_cwnd_reg(31 downto 3)		<= Ctrl_dt(31 downto 3);
			snd_cwnd_reg(2 downto 0)		<= "000";
			load_new_snd_cwnd_reg			<= '1';
		end if;
		load_new_snd_cwnd 					<= load_new_snd_cwnd_reg;
		
		if Ctrl_func(TCP_100Gb_ctrl_timer_RTT_def + addr_offset)  = '1' then
			timer_rtt_I_reg	 				<= Ctrl_dt(31 downto 0);
		end if;
		if Ctrl_func(TCP_100Gb_ctrl_timer_RTT_sync_def + addr_offset)  = '1' then
			timer_rtt_I_sync_reg			<= Ctrl_dt(31 downto 0);
		end if;
		if Ctrl_func(TCP_100Gb_ctrl_timer_persist_def + addr_offset)  = '1' then
			timer_persist_I_reg				<= Ctrl_dt(31 downto 0);
		end if;
		if Ctrl_func(TCP_100Gb_ctrl_thresold_retrans_def + addr_offset)  = '1' then
			thresold_retrans_reg			<= Ctrl_dt(31 downto 0);
		end if;
		if Ctrl_func(TCP_100Gb_ctrl_Rexmt_CWND_Sh_def + addr_offset)  = '1' then
			TCP_Rexmt_CWND_sh_reg			<= Ctrl_dt(31 downto 0);
		end if;
		if Ctrl_func(TCP_100Gb_ctrl_TCP_stream + addr_offset)  = '1' then
			TCP_stream_number_reg			<= Ctrl_dt(7 downto 0);
		end if;
	end if;	
end if;
end process;

process(clock)
begin
	if rising_edge(clock) then	
		Ctrl_out_rg <= (others => '0');
		 
		if Ctrl_func_rd(TCP_100Gb_ctrl_Init_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto  0) 					<= TCP_win_reg;
			Ctrl_out_rg(31 downto 16) 					<= Init_reg;
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_Max_size_Scale_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto  0) 					<= MSS_reg;
			Ctrl_out_rg(23 downto 16) 					<= TCP_scale_reg;
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_timestamp_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= TCP_TS_reg;
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_timestamp_reply_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= TCP_TS_rply_reg;
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_CongWind_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= snd_cwnd_reg;	
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_timer_RTT_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= timer_rtt_I_reg;	
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_timer_RTT_sync_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= timer_rtt_I_sync_reg;	
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_timer_persist_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= timer_persist_I_reg;		
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_thresold_retrans_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= thresold_retrans_reg;
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_Rexmt_CWND_Sh_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= TCP_Rexmt_CWND_sh_reg;	
		elsif Ctrl_func_rd(TCP_100Gb_ctrl_TCP_stream + addr_offset)  = '1' then
			Ctrl_out_rg(07 downto  0) 					<= TCP_stream_number_reg;	
		elsif Ctrl_func_rd(TCP_100Gb_status_Current_wind + addr_offset) = '1' then
			Ctrl_out_rg(15 downto 00) 	                <= TCP_win_from_HOST_reg;	
			Ctrl_out_rg(23 downto 16) 	                <= option_SCALE;	
--		else	
--			Ctrl_out_rg <= (others => '0');
		end if;

	end if;
end process;
 
-- MMS register
process(TCP_clock)
begin
	if rising_edge(TCP_clock) then
		if Open_connection = '1' then
			MSS_reg			<= TCP_max_size_reg;
		elsif update_MMS = '1' and Receive_ACK_pckt = '1' then
			MSS_reg			<= new_MMS;
		end if;
	end if;
end process;

-- windows and scale
process(resetn_TCP_clock,TCP_clock)
begin
	if resetn_TCP_clock = '0' then
		option_SCALE	      <= (others => '0');
		TCP_win_from_HOST_reg <= (others => '0');
	elsif rising_edge(TCP_clock) then
		if update_Scale = '1' and Receive_ACK_pckt = '1' then
			option_SCALE	<= new_Scale;
		end if;
		
		if Receive_ACK_pckt = '1' then
			TCP_win_from_HOST_reg	<= TCP_win_from_HOST;
		end if;
		 
	end if;
end process;

Scale_from_Host <= option_SCALE;

process(TCP_clock)
begin
	if rising_edge(TCP_clock) then
		 
        HOST_TCP_win_reg		<= (others => '0');
        if 	option_SCALE = x"00" then
            HOST_TCP_win_reg(15 downto 4) <= TCP_win_from_HOST_reg(15 downto 4);  --a ligned on 128-bit data;
        elsif option_SCALE = x"01" then	
            HOST_TCP_win_reg(16 downto 4) <= TCP_win_from_HOST_reg(15 downto 3);   
        elsif option_SCALE = x"02" then	
            HOST_TCP_win_reg(17 downto 4) <= TCP_win_from_HOST_reg(15 downto 2);   
        elsif option_SCALE = x"03" then	
            HOST_TCP_win_reg(18 downto 4) <= TCP_win_from_HOST_reg(15 downto 1);  
        elsif option_SCALE = x"04" then
            HOST_TCP_win_reg(19 downto 4) <= TCP_win_from_HOST_reg(15 downto 0);  
        elsif option_SCALE = x"05" then
            HOST_TCP_win_reg(20 downto 5) <= TCP_win_from_HOST_reg(15 downto 0);  
        elsif option_SCALE = x"06" then
            HOST_TCP_win_reg(21 downto 6) <= TCP_win_from_HOST_reg(15 downto 0);  
        elsif option_SCALE = x"07" then
            HOST_TCP_win_reg(22 downto 7) <= TCP_win_from_HOST_reg(15 downto 0);  
        elsif option_SCALE = x"08" then
            HOST_TCP_win_reg(23 downto 8) <= TCP_win_from_HOST_reg(15 downto 0);  
        elsif option_SCALE = x"09" then
            HOST_TCP_win_reg(24 downto 9) <= TCP_win_from_HOST_reg(15 downto 0);  
        elsif option_SCALE = x"0a" then
            HOST_TCP_win_reg(25 downto 10) <= TCP_win_from_HOST_reg(15 downto 0);  
        elsif option_SCALE = x"0b" then
            HOST_TCP_win_reg(26 downto 11) <= TCP_win_from_HOST_reg(15 downto 0);  
        elsif option_SCALE = x"0c" then
            HOST_TCP_win_reg(27 downto 12) <= TCP_win_from_HOST_reg(15 downto 0);  
        elsif option_SCALE = x"0d" then
            HOST_TCP_win_reg(28 downto 13) <= TCP_win_from_HOST_reg(15 downto 0);  
        else 
            HOST_TCP_win_reg(29 downto 14) <= TCP_win_from_HOST_reg(15 downto 0);  -- if scale is higher than 14 => 14 is used
        end if;			
		 
	end if;
end process;

Ctrl_out					<= Ctrl_out_rg;
 
TCP_scale					<= TCP_scale_reg;	-- Scale HOME TCP
TCP_win_home				<= TCP_win_reg;		-- WIN HOME TCP
TCP_win_Host				<= HOST_TCP_win_reg;-- WIN From HOST
TCP_MMS_Max					<= TCP_max_size_reg;
TCP_MSS_Host				<= MSS_reg;
TCP_TS						<= TCP_TS_reg;
TCP_TS_rply					<= TCP_TS_rply_reg;
snd_cwnd					<= snd_cwnd_reg;
timer_rtt_I					<= timer_rtt_I_reg;
timer_rtt_I_sync			<= timer_rtt_I_sync_reg;
timer_persist_I				<= timer_persist_I_reg;
Init_bit					<= Init_reg;
thresold_retrans			<= thresold_retrans_reg;
TCP_Rexmt_CWND_sh			<= TCP_Rexmt_CWND_sh_reg;
		
TCP_stream_number			<= TCP_stream_number_reg;

end behavioral;
