------------------------------------------------------
-- TCP receiver 
-- 
-- Dominique Gigi
--
-- July 2012 ver 2.00
--
------------------------------------------------------
--  version with timer clock 156.25 Mhz
--  Correct the LT LEQ GT GEQ
-- Timeout value anc CWND value varaible set by PCI
-- Move to version Petr SM 1.1
-- Move to version Petr SM1.2.0 (congestion control)
-- L_snd_cwnd default value will be MSS not shifted
-- remove the idle state on TCP_snd
-- move Stat(16) et create stat(17)
-- pipe the SND_NXT  addition  (tmp_snd_NXT)
-- see file MODIF_ON_TCP_CORE.txt
-- 11/2014  version 1.2.7  modif on flags and stat.register page "TCP_eceiver (1/5)
-- 03/03/2015 add a flag for terminate with data received
-- 27/04/2015 fixe the bug on line "elsif TCP_rcv_FC = Update_UNA and f_persist = '0' and EQ_ACK_MAX = '0' then " add and EQ_ACK_MAX = '0' in the equation
-- 28/04/2015  add _data_to_send > 0 in bottom TCP_SENDER 1/3 (see state "Prep_Send_data_S20")
-- 06/12/2021 pipe Seq_Nxt_i1 
--                 ACK_UNA_i2  
--                 ACK_UNA_i3       
--                 Seq_Nxt_i2       
--                 wl2_ACK_i2       
--                 LT_nxt_una_i1       
--                 GT_ACK_RTTseq_i1       
--                              (clocked)

------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


ENTITY TCP_manage_0x IS
 generic (	constant TCP_THRESHOLD_RTTSHIFT	        : std_logic_vector(7 downto 0):= x"06";
				constant TCP_MAX_RTTSHIFT			: std_logic_vector(7 downto 0):= x"0C";
				constant WINDOW						: std_logic_vector(7 downto 0):= x"01"
			-- constant snd_cwnd			: std_logic_vector(31 downto 0):= x"00020000";
			-- constant timer_rtt_init		: std_logic_vector(31 downto 0):= x"0002625A"; 
			-- constant timer_rtt_init_sync : std_logic_vector(31 downto 0):= x"12A05F20";
			-- constant timer_persist_init  : std_logic_vector(31 downto 0):= x"0004C4B4"
	 );
port (
	reset_n								: in std_logic;  -- Active low
	rst_local							: in std_logic;  -- Active high
	clock								: in std_logic;
 

	ISS									: in std_logic_vector(31 downto 0); -- Initial Segment Number
	f_Push								: in std_logic;
	Req_Sync							: in std_logic;
	Req_Rst								: in std_logic;
	MSS_srv								: in std_logic_vector(31 downto 0); --value received from server
	MSS_set								: in std_logic_vector(15 downto 0); --value set by the controller software 
	
	-- value receive in a packet
	rcv_pckt							: in std_logic;
	rd_val								: out std_logic;	

	rcv_seg_SEQ_i						: in std_logic_vector(31 downto 0);
	rcv_seg_ACK_i						: in std_logic_vector(31 downto 0);
	rcv_WIN_i						    : in std_logic_vector(31 downto 0);-- value WINDOW shift by option 2 (if used)
	rcv_FLAG_i						    : in std_logic_vector(7 downto 0);
	rcv_len_i							: in std_logic_vector(15 downto 0); -- len in case of we receive data with ack ?? normally not allow

	-- value send to build TCP_IP headers for ACK packet
	Acpt_rq_ack_pkt						: in std_logic; -- accept parameters for TCP packet
	wen_rq_ack_pkt						: out std_logic;-- send a TCP packet parameters
	FLAG_out							: out std_logic_vector(7 downto 0);
	SEQ_out								: out std_logic_vector(31 downto 0);
	ACK_out								: out std_logic_vector(31 downto 0);
	option_out							: out std_logic_vector(2 downto 0);


	-- value send to build TCP_IP headers for DATA/SYNC packet
	Acpt_rq_Send_Pkt					: in std_logic; -- accept parameters for TCP packet
	wen_rq_Send_pkt						: out std_logic;-- send a TCP packet parameters
	FLAG_Send_out						: out std_logic_vector(7 downto 0);
	SEQ_Send_out						: out std_logic_vector(31 downto 0);
	ACK_Send_out						: out std_logic_vector(31 downto 0);
	option_Send_out						: out std_logic_vector(2 downto 0);
	Length_Send_out						: out std_logic_vector(31 downto 0);
	pckt_wo_dt							: out std_logic := '0';



	-- signal between SEND and RECEIVE
 
	Get_TIMESTAMP						: in std_logic_vector(31 downto 0);

	-- value from/to the SENDER part
	Pkt_done							: in std_logic; --update Send_UNA
	snd_new								: in std_logic_vector(31 downto 0);
	
	Pointer_read						: out std_logic_vector(31 downto 0);
	valid_new_p							: out std_logic;

	no_delay							: in std_logic;
	state_o								: out std_logic;
	statistic							: out std_logic_vector(18 downto 0);
	Stat_flag							: out std_logic_vector(31 downto 0);
	
	f_fastretransmit_disabled			: in std_logic;
	snd_cwnd							: in std_logic_vector(31 downto 0);
	load_new_snd_cwnd					: in std_logic;
	timer_rtt_init						: in std_logic_vector(31 downto 0); 
	timer_rtt_init_sync 				: in std_logic_vector(31 downto 0);
	timer_persist_init  				: in std_logic_vector(31 downto 0);
	tcprexmtthresh						: in std_logic_vector(31 downto 0);
	timer_stop_disable					: in std_logic;
	TCP_REXMT_CWND_SHIFT				: in std_logic_vector(31 downto 0);
	measure_RTT_max						: out std_logic_vector(31 downto 0);
	measure_RTT_min						: out std_logic_vector(31 downto 0);
	Measure_RTT_cnt						: out std_logic_vector(31 downto 0);
	Measure_RTT_Accu					: out std_logic_vector(63 downto 0);
	stat_wnd_max						: out std_logic_vector(31 downto 0);
	stat_wnd_min						: out std_logic_vector(31 downto 0);
	Stat_snd_byte						: out std_logic_vector(63 downto 0);
	stat_snd_rexmit_byte				: out std_logic_vector(63 downto 0);
	stat_dupack_max					   	: out std_logic_vector(31 downto 0);
	rtt_shifts_MAX						: out std_logic_vector(31 downto 0);
	rtt_v								: out std_logic_vector(31 downto 0);
	TCP_state_o							: out std_logic_vector( 2 downto 0); -- (0) closed  -- (1) sync sent -- (2)  Established
	Persist_state						: out std_logic
	);
END TCP_manage_0x;

---------------------------------------------------------------------------------
--########################     ARCHITECTURE     ###############################--
---------------------------------------------------------------------------------

ARCHITECTURE behavioral of TCP_manage_0x IS

attribute FSM_ENCODING :string;

 type TCP_rcv_fc_type is (	idle,
									Rd_rcv_Packet_ACK,
									Check_TCP_state,
									Syn_sent,
									Ack_rcv,
									DROP,
									DROP_aft_ACK,
									DROP_w_RST,
									TERM_w_RST,
									TERMINATE,
									update_UNA,
									update_win,
									update_registers,
									send_ACK,
									send_FRAG,
									Already_ACK,
									send_req,
									retrans,
									mem_big_wind
							);
signal TCP_rcv_FC:TCP_rcv_fc_type;
--attribute FSM_ENCODING of TCP_rcv_FC: signal is "sequential";

type TCP_snd_fc_type is (	State_S0,
									Prep_Send_data_S10,
									Prep_Send_data_S11 ,
									Prep_Send_data_S12,
									Prep_Send_data_S13,
									Prep_Send_data_S14,
									Prep_Send_data_S15,
									Prep_Send_data_S16,
									Prep_Send_data_S20,
									SEND_DATA_S50,	
									SEND_DATA_S51,		
									SEND_DATA_S52,
									SEND_DATA_S53,
									SEND_DATA_S54,
									SEND_DATA_S55,
									SEND_WIN_PROBE,
									SEND_DATA_SEG,
									Close_state,
									SEND_SYNC,
									Send_Segment,
									update_UNA_OK
						);
signal TCP_snd_FC:TCP_snd_fc_type;
--attribute FSM_ENCODING of TCP_snd_FC: signal is "sequential"; 

component comparator is
	generic( val_signed 		: boolean := true;
				comp_null		: boolean := true;
				pipe			: boolean := true; -- 1 clock
				wd_width		: natural := 32
		);
	port (
		reset			: in std_logic :='1';
		clock			: in std_logic :='1';
		dataa			: in std_logic_vector(wd_width-1 downto 0);
		datab			: in std_logic_vector(wd_width-1 downto 0):= (others => '0');--x"00000000";
		
		aeb				: out std_logic;
		agb				: out std_logic;
		ageb			: out std_logic;
		alb				: out std_logic;
		aleb			: out std_logic;
		aneb			: out std_logic		
	);  
end component;

signal read_Ack_val						: std_logic;
signal read_Ack_val_ext 				: std_logic;
signal read_Ack_val_del 				: std_logic_vector(2 downto 0);
signal rcv_seg_SEQ					    : std_logic_vector(31 downto 0);
signal rcv_seg_ACK  					: std_logic_vector(31 downto 0);
signal rcv_WIN 						    : std_logic_vector(31 downto 0);-- value WINDOW shift by option 2 (if used)
signal rcv_FLAG 						: std_logic_vector(7 downto 0);
signal rcv_len 						    : std_logic_vector(15 downto 0);

SIGNAL not_rst_local					: std_logic;
SIGNAL rst_timer						: std_logic;
SIGNAL ld_timer							: std_logic;
SIGNAL dt_timer							: std_logic_vector(31 downto 0);
SIGNAL out_timer						: std_logic_vector(31 downto 0);
SIGNAL ena_timer						: std_logic;
			
SIGNAL TCP_state						: std_logic_vector(2 downto 0); --1 Closed --2 Syn_Sent --4 Established
SIGNAL mem_req_Sync						: std_logic;
SIGNAL mem_req_reset					: std_logic;
SIGNAL Req_ESTABLISH					: std_logic;

signal LEQ_ACK_UNA						: std_logic;
signal GT_ACK_MAX						: std_logic;
signal EQ_ACK_MAX						: std_logic;
signal LT_Seq_Nxt						: std_logic;
signal GT_Seq_Nxt						: std_logic;
signal GT_Seq_NxtpW						: std_logic;
signal LT_wl2_ACK						: std_logic;
signal EQ_wl2_ACK						: std_logic;
signal GT_win_swin						: std_logic;
signal NE_win_swin						: std_logic;
signal NE_ACK_NXT						: std_logic;
signal LT_NXT_UNA						: std_logic;
signal LT_win_cwin						: std_logic;
signal LTsoLenWIN						: std_logic;
signal GE_DR_SoLen						: std_logic;
signal Neg_DT_t_S						: std_logic;
signal Pos_DT_t_S						: std_logic;
signal GE_DT_t_S_MSS					: std_logic;
signal LT_NXT_MAX						: std_logic;
signal Null_snd_wnd						: std_logic;
signal Null_send_win					: std_logic;
signal Pos_Lso_len						: std_logic;
signal N_Ltimer							: std_logic;
signal EQ_PTS_SoLen						: std_logic;
signal GT_LSndX_SndM					: std_logic;
signal GT_statWind_win_max				: std_logic;
signal LT_statWind_win_min				: std_logic;
signal Diff_una_reg						: std_logic;
signal Diff_luna_reg					: std_logic;
signal NE_ack_Snd_UNA					: std_logic;
signal dupack_retrans					: std_logic;
signal GT_ACK_RTTseq					: std_logic;
signal RTT_MAX_CHECK					: std_logic;
signal RTT_MIN_CHECK					: std_logic;
signal length_Not_Null					: std_logic;
			
signal SNDNXT_UNA						: std_logic_vector(31 downto 0);
signal SNDNXT_MAX						: std_logic_vector(31 downto 0);
signal SEQ_RCVNXT						: std_logic_vector(31 downto 0);
signal SEQ_RCVNXTpW						: std_logic_vector(31 downto 0);
signal ACK_UNA							: std_logic_vector(31 downto 0);
signal ACK_MAX							: std_logic_vector(31 downto 0);
signal WL2_ACK							: std_logic_vector(31 downto 0);
signal ACK_RTTseq						: std_logic_vector(31 downto 0);
		
signal inc_length						: std_logic;
signal snd_ACK_frg						: std_logic;
signal snd_RST_frg0						: std_logic;
signal snd_RST_frg1						: std_logic;
signal f_allow_to_send					: std_logic;
		
signal new_seq_num						: std_logic_vector(31 downto 0);
signal add_ena							: std_logic; 
signal send_data						: std_logic; 
			
signal rcv_Nxt							: std_logic_vector(31 downto 0); -- next Seq expected
signal Snd_UNA							: std_logic_vector(31 downto 0); -- keep the lastest Unacknowlede SEQ#
signal snd_NXT							: std_logic_vector(31 downto 0); 
signal tmp_snd_NXT						: std_logic_vector(31 downto 0); 
signal snd_MAX							: std_logic_vector(31 downto 0);
signal snd_wl2							: std_logic_vector(31 downto 0); -- last higher ACK
signal snd_wnd							: std_logic_vector(31 downto 0);

signal MSS								: std_logic_vector(31 downto 0); -- Maximum Segment Size


--local variable for  Flow-Chart
signal L_snd_una						: std_logic_vector(31 downto 0);
signal Snd_una_OK						: std_logic_vector(31 downto 0); 
signal L_snd_MAX						: std_logic_vector(31 downto 0);
signal L_snd_wnd						: std_logic_vector(31 downto 0);
signal L_timer							: std_logic_vector(31 downto 0);
signal L_so_length						: std_logic_vector(31 downto 0);
signal lcell_so_length					: std_logic_vector(31 downto 0);
signal f_Idle							: std_logic;
signal L_data_already_sent				: std_logic_vector(31 downto 0);
signal L_send_win						: std_logic_vector(31 downto 0);
signal L_data_to_send					: std_logic_vector(31 downto 0); -- total lenght Already sent + ready to send
signal DT_tobe_send						: std_logic_vector(31 downto 0);	-- total lenght  already sent + Actu packet
signal L_segment_seq					: std_logic_vector(31 downto 0);
signal L_Length							: std_logic_vector(31 downto 0);
		
signal data_ready						: std_logic_vector(31 downto 0);
		
signal TIMER_START_RTT					: std_logic;
signal TIMER_START_RTT_L				: std_logic;
signal TIMER_START_PERSIST				: std_logic;
signal TIMER_STOP						: std_logic;
signal f_timer_expired 					: std_logic;
signal f_persist						: std_logic;
signal f_measure_rtt					: std_logic;
		                            	
signal MEASURE_RTT						: std_logic_vector(5 downto 0);
signal measure_RTT_seq					: std_logic_vector(31 downto 0);
signal measure_RTT_timestamp			: std_logic_vector(31 downto 0);
signal Measure_RTT_accu_rg				: std_logic_vector(63 downto 0);
signal local_RTT						: std_logic_vector(31 downto 0);
signal Measure_RTT_cnt_rg				: std_logic_vector(31 downto 0);
signal measure_RTT_max_reg				: std_logic_vector(31 downto 0);
signal measure_RTT_min_reg				: std_logic_vector(31 downto 0);
		
signal dupack							: std_logic_vector(31 downto 0);
signal check_dupack						: std_logic;
signal stat_dupack_max_reg				: std_logic_vector(31 downto 0);
		
signal f_fastretransmit					: std_logic;
signal ff_fastretransmit				: std_logic;
signal fastretransmit_saved_seq 		: std_logic_vector(31 downto 0);
signal fastretransmit_seq				: std_logic_vector(31 downto 0);
		
--debug signals		
signal stat_wnd_max_rg					: std_logic_vector(31 downto 0);
signal stat_wnd_min_rg					: std_logic_vector(31 downto 0);
signal stat_snd_rexmit_byte_reg 		: std_logic_vector(63 downto 0);
signal Stat_snd_byte_reg				: std_logic_vector(63 downto 0);
		
signal L_snd_cwnd						: std_logic_vector(31 downto 0);
		
signal rtt_shifts						: std_logic_vector(7 downto 0);
signal rtt_shifts_MAX_reg				: std_logic_vector(7 downto 0);
signal ld_counter						: std_logic;
signal flag_reg							: std_logic_vector(31 downto 0);

signal statistic_cell					: std_logic_vector(18 downto 0);




attribute mark_debug : string;
--attribute mark_debug of TCP_rcv_FC: signal is "true"; 
--attribute mark_debug of TCP_snd_FC: signal is "true";
--attribute mark_debug of Snd_una_OK					: signal is "true";
--attribute mark_debug of snd_new					: signal is "true";
--attribute mark_debug of L_snd_una				: signal is "true";
--attribute mark_debug of L_data_to_send			: signal is "true";
--attribute mark_debug of L_data_already_sent		: signal is "true";
--attribute mark_debug of f_allow_to_send			: signal is "true";
--attribute mark_debug of L_so_length				: signal is "true";
--attribute mark_debug of L_send_win				: signal is "true"; 

--attribute mark_debug of Stat_snd_byte_reg			: signal is "true";
--attribute mark_debug of stat_snd_rexmit_byte_reg	: signal is "true";
--attribute mark_debug of TCP_state					: signal is "true"; 
--attribute mark_debug of statistic_cell				: signal is "true"; 
--###############################################################################--
--##########################<< CODE  START  HERE   >>############################--
--###############################################################################--
BEGIN

not_rst_local	<= not(rst_local);

-- latch incoming new values
process(clock)
begin
	if rising_edge(clock) then
		-- if rcv_pckt = '1' then
		if read_Ack_val = '1' then
			rcv_seg_SEQ		<= rcv_seg_SEQ_i;	
			rcv_seg_ACK		<= rcv_seg_ACK_i; 	
			rcv_FLAG		<= rcv_FLAG_i	;	
			rcv_len			<= rcv_len_i	;	
		end if;
		
		if read_Ack_val_del(2) = '1' then 	
			rcv_WIN			<= rcv_WIN_i	;	 
		end if;		
		
		read_Ack_val_del(2 downto 1)  <= read_Ack_val_del(1 downto 0);
		read_Ack_val_del(0)           <= read_Ack_val;
	end if;
end process;


ACK_UNA_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 32		)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> rcv_seg_ACK,
		datab		=> Snd_UNA,
		aneb		=> NE_ack_Snd_UNA
	);


ACK_UNA_i2:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
--		reset		=> '1',
		clock		=> clock,
		dataa		=> ACK_UNA,
--		datab		=> x"00000000",
		aleb		=> LEQ_ACK_UNA
	);


ACK_UNA_i3:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
--		reset		=> '1',
		clock		=> clock,
		dataa		=> ACK_MAX,
--		datab		=> x"00000000",
		agb			=> GT_ACK_MAX,
		aeb			=> EQ_ACK_MAX
	);


Seq_Nxt_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
--		reset		=> '1',
		clock		=> clock,
		dataa		=> SEQ_RCVNXT,
--		datab		=> x"00000000",
		agb		    => GT_Seq_Nxt,
		alb		    => LT_Seq_Nxt
	);

Seq_Nxt_i2:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
--		reset		=> '1',
		clock		=> clock,
		dataa		=> SEQ_RCVNXTpW,
--		datab		=> x"00000000",
		agb		    => GT_Seq_NxtpW
	);
	
wl2_ACK_i2:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
--		reset		=> '1',
		clock		=> clock,
		dataa		=> WL2_ACK,
--		datab		=> x"00000000",
		aeb		    => EQ_wl2_ACK,
		alb		    => LT_wl2_ACK
	); 

 
Win_chk_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> rcv_WIN,
		datab		=> snd_wnd,
		agb		    => GT_win_swin,
		aneb		=> NE_win_swin
	);


GT_statWind_win_i1:comparator 
	generic map(val_signed 		=> false,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> rcv_WIN,
		datab		=> stat_wnd_max_rg,
		agb		 => GT_statWind_win_max
	);


GT_statWind_win_i2:comparator 
	generic map(val_signed 		=> false,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> rcv_WIN,
		datab		=> stat_wnd_min_rg,
		alb		=> LT_statWind_win_min
	);


NE_ACK_NXT_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> rcv_seg_ACK,
		datab		=> snd_NXT,
		aneb		=> NE_ACK_NXT
	);


LT_nxt_una_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null   => true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		--reset		=> '1',
		clock		=> clock,
		dataa		=> SNDNXT_UNA,
		--datab		=> x"00000000",
		alb			=> LT_NXT_UNA
	);


LT_win_cwin_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> snd_wnd, --we use the snd_wnd and not the L_SND_wnd because the test spends 1 clock (done in S10 to be used in S11)
		datab		=> L_snd_cwnd,
		alb		=> LT_win_cwin
	);


LTsoLenWIN_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> L_so_length,
		datab		=> L_send_win,
		alb		=> LTsoLenWIN
	);

GE_DR_SoLen_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null   => false,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> data_ready,
		datab		=> L_so_length,
		ageb		=> GE_DR_SoLen
	);


NP_DT_t_S_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null   => true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> L_data_to_send,
--		datab		=> x"00000000",
		agb		    => Pos_DT_t_S,
		alb		    => Neg_DT_t_S
	);


Null_snd_wnd_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> snd_wnd,
--		datab		=> x"00000000",
		aeb		=> Null_snd_wnd
	);

GE_DT_t_S_MSS_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> L_data_to_send,
		datab		=> MSS,
		ageb		=> GE_DT_t_S_MSS
	);

--this comparator is done on values changed 2 clocks before the result is used
LT_NXT_MAX_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> true,
					pipe		=> false,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
--		clock		=> '1',
		dataa		=> SNDNXT_MAX,
--		datab		=> x"00000000",
		agb		    => GT_LSndX_SndM,
		alb		    => LT_NXT_MAX
	);
 

Null_send_win_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null   => true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> l_send_win,
--		datab		=> x"00000000",
		aeb		    => Null_send_win
	);


Pos_Lso_len_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> L_so_length,
--		datab		=> x"00000000",
		agb		=> Pos_Lso_len
	);
 

N_Ltimern_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> true,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> L_timer,
--		datab		=> x"00000000",
		aeb		=> N_Ltimer 
	);
 
EQ_PTS_SoLen_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port  map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> DT_tobe_send,
		datab		=> L_so_length,
		aeb		=> EQ_PTS_SoLen 
	);
 

Diff_Luna_reg_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> Snd_una_OK,
		datab		=> L_snd_una,
		aneb		=> Diff_luna_reg
	);
 
Diff_una_reg_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null   => false,
					pipe		=> true,
					wd_width	=> 	32	)
	port map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> Snd_una_OK,
		datab		=> snd_una,
		aneb		=> Diff_una_reg
	); 
	
DUPACK_retrans_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null	=> false,
					pipe		=> true,
					wd_width	=> 	32	)
	port map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> dupack,
		datab		=> tcprexmtthresh,
		aeb		    => DUPACK_retrans
	);
 
GT_ACK_RTTseq_i1:comparator 
	generic map(val_signed 		=> true,
					comp_null   => true,
					pipe		=> true,
					wd_width	=> 	32	)
	port map(
--		reset		=> '1',
		clock		=> clock,
		dataa		=> ACK_RTTseq,
--		datab		=> x"00000000",
		agb		    => GT_ACK_RTTseq 
	);
	
	
Rcv_length_i1:comparator 
	generic map(val_signed 		=> false,
					comp_null	=> true,
					pipe		=> true,
					wd_width	=> 16	)
	port map(
		reset		=> '1',
		clock		=> clock,
		dataa		=> rcv_len,
--		datab		=> x"00000000",
		aneb		=> length_Not_Null 
	); 	
	
process(clock)
begin
if rising_edge(clock) then
	SNDNXT_UNA		<= snd_Nxt 		- snd_UNA;
	SNDNXT_MAX		<= snd_Nxt 		- snd_MAX;
	SEQ_RCVNXT		<= rcv_seg_SEQ - rcv_Nxt;
	SEQ_RCVNXTpW	<= rcv_seg_SEQ - rcv_Nxt - WINDOW;
	ACK_UNA			<= rcv_seg_ACK - snd_UNA;
	ACK_MAX			<= rcv_seg_ACK - snd_MAX;
	WL2_ACK			<= snd_wl2 		- rcv_seg_ACK;
	ACK_RTTseq		<= rcv_seg_ACK - measure_RTT_seq;
end if;
end process;

--############################################
-- Memorize the INPUT signals
--############################################
process(reset_n,clock)
begin
if reset_n = '0' then
	mem_req_Sync  		<= '0';
	mem_req_reset 		<= '0';
elsif rising_edge(clock) then
	if Req_Sync = '1' and TCP_state(2) = '0' then
		mem_req_Sync 	<= '1';
	elsif TCP_snd_FC = SEND_SYNC or Req_Rst = '1' then
		mem_req_Sync 	<= '0';
	end if;
	if Req_Rst = '1' and TCP_state(2) = '1' then
		mem_req_reset 	<= '1';
	elsif TCP_state(0) = '1' then
		mem_req_reset 	<= '0';
	end if;
end if;
end process;

--####################################################################################################################################
--############################################ GLOBAL state machine for the receiver part ############################################
--####################################################################################################################################
 
TCP_rcv_FC_state:process(clock)
begin 

if rising_edge(clock) then
	if reset_n = '0' then
		TCP_rcv_FC 			<= idle;
		inc_length 			<= '0';
		statistic_cell(0) 	<= '0';	-- not used anymore
		statistic_cell(2) 	<= '0'; 	-- not used anymore
		statistic_cell(10)	<= '0';
		statistic_cell(11)	<= '0';
		statistic_cell(12)	<= '0';
		statistic_cell(13)	<= '0';	
		
	else
		inc_length 			<= '0';
		statistic_cell(0) 	<= '0';	-- not used anymore
		statistic_cell(2) 	<= '0'; 	-- not used anymore
		statistic_cell(10)	<= '0';
		statistic_cell(11)	<= '0';
		statistic_cell(12)	<= '0';
		statistic_cell(13)	<= '0';
		Case TCP_rcv_FC is
			when idle =>
				if rcv_pckt = '1' then -- something is arrived
					TCP_rcv_FC 	<= Rd_rcv_Packet_ACK;
				end if;
			when Rd_rcv_Packet_ACK => -- Read ACK_Rcv FIFO
			     if read_Ack_val_del(2) = '1' then 
				    TCP_rcv_FC 			<= Check_TCP_state;
				 end if;
			when Check_TCP_state =>
				if 		TCP_state(0) = '1' then 						--CLOSED
					TCP_rcv_FC 		<= DROP_w_RST;
				elsif rcv_FLAG(0) = '1' or rcv_FLAG(5) = '1' then 	-- finish or Urgent
					TCP_rcv_FC	 	<= TERM_w_RST;
				elsif 	TCP_state(1) = '1' then 						--SYN_Sent
					TCP_rcv_FC 		<= Syn_sent;
				else	
					TCP_rcv_FC 		<= Ack_rcv;
				end if;
				
			when Ack_rcv => --------------------------manage a ACK packet
				-- The link is established	  
				if LT_Seq_Nxt = '1' or GT_Seq_NxtpW = '1' then			-- Seq# is < from the previous or  Seq > rcv_nxt + WINDOW
					TCP_rcv_FC 		<= DROP_aft_ACK;	
				elsif length_Not_Null = '1' then
					TCP_rcv_FC 		<= TERM_w_RST;
				elsif GT_Seq_Nxt = '1' then			-- Seq# is > from the previous OR  SYN flag set
					TCP_rcv_FC 		<= TERM_w_RST;
				elsif rcv_FLAG(2) = '1' then 		-- RST flag set
					TCP_rcv_FC 		<= TERMINATE;
				elsif rcv_FLAG(1) = '1' then		-- SYNC flag set
					TCP_rcv_FC 		<= TERM_w_RST;
				elsif rcv_FLAG(4) = '0' then		-- ACK flag reset
					TCP_rcv_FC 		<= DROP;
				elsif LEQ_ACK_UNA = '1' then		-- ACKrcv =< UNA
					TCP_rcv_FC 		<= Already_ACK;
				elsif GT_ACK_MAX = '1' then			-- ACK > MAX
					statistic_cell(10)	<= '1';
					TCP_rcv_FC 		<= DROP_aft_ACK;
				else								-- Measure_RTT(activated) AND ACK > MeasureRTTSEQ
					TCP_rcv_FC		<= update_UNA;
				end if;
			
			when update_UNA =>
				if LT_wl2_ACK = '1' OR (EQ_wl2_ACK = '1' and GT_win_swin = '1' ) then
					TCP_rcv_FC 		<= update_win;
				else
					TCP_rcv_FC 		<= idle;
				end if;
				
			when Syn_sent =>  ------------------------------ manage the ACK from a SYN
				--The link wait for ACK for SYN
				--	RESET  
				if    rcv_FLAG(2) = '1' then 	-- flag reset set
					if rcv_FLAG(4) = '1' then	--  CHECK ACK
						TCP_rcv_FC 	<= TERMINATE;
					else
						TCP_rcv_FC	<= DROP;
					end if;
				elsif rcv_FLAG(1) = '0' then 	-- SYN reset
					if rcv_FLAG(4) = '1' then	--  CHECK ACK
						TCP_rcv_FC 	<= DROP_w_RST;
					else
						TCP_rcv_FC 	<= DROP;
					end if;
				elsif rcv_FLAG(4) = '0' OR NE_ACK_NXT = '1' then-- check flag and ACK-Snd_NXT
					TCP_rcv_FC 		<= DROP_w_RST;
				else
					TCP_rcv_FC 		<= update_registers;
				end if;
			
			when update_registers =>
				TCP_rcv_FC 			<= send_ACK;
	
			when update_win =>		------------------------------------- update window reg
				TCP_rcv_FC 			<= idle;
	
		 
			when Already_ACK => 
				if NE_win_swin = '1' then
					if LT_wl2_ACK = '1' OR (EQ_wl2_ACK = '1' and GT_win_swin = '1' ) then
						TCP_rcv_FC 	<= update_win;
					else
						TCP_rcv_FC 	<= idle;
					end if;
				else
					TCP_rcv_FC 		<= retrans;
				end if;
				
			when retrans =>
				TCP_rcv_FC 			<= DROP;
				
			when DROP => 
				statistic_cell(12)	<= '1';
				TCP_rcv_FC 			<= idle;
				
			when DROP_aft_ACK =>
				statistic_cell(11)	<= '1';
				if rcv_FLAG(2) = '0' then
					TCP_rcv_FC 		<= send_ACK;
				else
					TCP_rcv_FC 		<= idle;
				end if;			
				
			when DROP_w_RST => 
				statistic_cell(13)	<= '1';
				if rcv_FLAG(2) = '1' then	-- if reset flag is set
					TCP_rcv_FC 		<= idle;
				elsif rcv_FLAG(4) = '1' then-- if ACK flag is set
					TCP_rcv_FC		<= send_FRAG;
				elsif rcv_FLAG(1) = '1' then
					inc_length 		<= '1';
					TCP_rcv_FC 		<= send_FRAG;
				else
					TCP_rcv_FC 		<= send_FRAG;
				end if;
			
			when TERM_w_RST => 
				TCP_rcv_FC 			<= DROP_w_RST;
				
			when TERMINATE => 
				TCP_rcv_FC 			<= idle;
				
			when send_ACK =>		 
				TCP_rcv_FC 			<= send_req;
				
			when send_FRAG =>		
				TCP_rcv_FC 			<= send_req;
				
			when send_req =>		------------------------------------ Send a packet
				if Acpt_rq_ack_pkt = '1' then
					TCP_rcv_FC 		<= idle;
				end if;
				
			when others =>
				TCP_rcv_FC 			<= idle;
		  end case;
	end if;
end if;
end process;

--=========================================================
-- Set the MSS value at SYNC receive
--=========================================================
process(reset_n,clock)
begin
	if reset_n = '0' then
		MSS	<= x"00000200";
	elsif rising_edge(clock) then
		if 	mem_req_Sync ='1' then
				MSS(31 downto 16)	<= (others => '0');
				MSS(15 downto  0)	<= MSS_set;
		elsif TCP_rcv_FC = Syn_sent then
			if MSS > MSS_srv then
				MSS					<= MSS_srv;
			end if;
		end if;
	end if;
end process;

--=========================================================
-- variable that change with ACK states					
--=========================================================
-- Compute length for the fragement send 
add_ena <= '1' when TCP_rcv_FC = DROP_w_RST else '0';
--length_calcul
process(clock)
begin
	if rising_edge(clock) then
		if add_ena = '1' then
			new_seq_num		<= rcv_seg_SEQ + (x"0000" & rcv_len) + ("000000000000000" & inc_length);
		end if;
	end if;
end process;

process(reset_n,clock)
begin
if reset_n = '0' then  
	snd_wnd					<= (others => '0');
	rcv_Nxt					<= (others => '0');
	Snd_UNA					<= (others => '0');
	snd_wl2					<= (others => '0');
	FLAG_out				<= (others => '0');
	SEQ_out					<= (others => '0');
	ACK_out					<= (others => '0');
	option_out				<= (others => '0');
	
elsif rising_edge(clock) then
	-- prepare variables for send a packet
	if TCP_rcv_FC = send_ACK then	-- OK	
		FLAG_out			<= x"10";		-- Ack flag
		SEQ_out				<= snd_NXT;
		ACK_out				<= rcv_Nxt;
		option_out			<= "000";
	elsif TCP_rcv_FC = send_FRAG then 
		if rcv_FLAG(4) = '1' then			-- ACK flag
			FLAG_out			<= x"04";		-- reset flag
			SEQ_out			<= rcv_seg_ACK;
			ACK_out			<= (others => '0');
			option_out		<= "000";
		else
			FLAG_out			<= x"04";		-- reset flag
			SEQ_out			<= (others => '0');
			ACK_out			<= new_seq_num;
			option_out		<= "000";
		end if;
	end if;

	-- update WIN  and ACK keep
	if 		((TCP_rcv_FC = update_UNA or (TCP_rcv_FC = Already_ACK and  NE_win_swin = '1')) and (LT_wl2_ACK = '1' OR (EQ_wl2_ACK = '1' and GT_win_swin = '1' ))) 	
		OR    TCP_rcv_FC = update_registers 																							then 
		snd_wl2			<= rcv_seg_ACK;
		snd_wnd			<= rcv_WIN;
	end if;
	-- Update UnAcknoledge Var
	if 		TCP_rcv_FC = update_UNA 			 
		or 	TCP_rcv_FC = update_registers  then 
		Snd_UNA(31 downto 4) 		<= rcv_seg_ACK(31 downto 4);
		Snd_UNA(3 downto 0) 		<= "0000"; -- this 4 bit should be at '0'  to avoid a miss alignment of the data (modulo 128-bit)
	elsif TCP_snd_FC = State_S0 and mem_req_Sync = '1' then
		Snd_UNA						<= ISS;
	end if;	
 	
 	if TCP_rcv_FC = update_registers then  
		rcv_Nxt 			<= rcv_seg_SEQ + '1'; 
	end if;
end if;
end process;


process(clock)
begin
if rising_edge(clock) then 
	--Update local variables
	Req_ESTABLISH 			<= '0';
	if TCP_rcv_FC = update_registers then   
		Req_ESTABLISH 		<= '1';
	end if;
		
	read_Ack_val       <= '0';
	read_Ack_val_ext   <= '0';
	if rcv_pckt = '1' and TCP_rcv_FC = idle then
		read_Ack_val	 <= '1';
		read_Ack_val_ext <= '1';
	end if;

end if;
end process;

rd_val	<= read_Ack_val_ext;

process(reset_n,rst_local,clock)
begin
if reset_n = '0' or rst_local = '1' then
	stat_wnd_max_rg		<= (others => '0');
	stat_wnd_min_rg		<= (others => '1');
	statistic_cell(16) 	<= '0';
elsif rising_edge(clock) then
	if TCP_state(0) = '1' then
		stat_wnd_min_rg 	<= (others => '1');
	elsif TCP_rcv_FC =  update_registers OR (TCP_rcv_FC =  update_win and LT_statWind_win_min = '1') then
		stat_wnd_min_rg 	<= snd_wnd;
	end if;
	
	if TCP_state(0) = '1' then
		stat_wnd_max_rg 	<= (others => '0');
	elsif TCP_rcv_FC =  update_registers OR (TCP_rcv_FC =  update_win and GT_statWind_win_max = '1') then
		stat_wnd_max_rg 	<= snd_wnd;
	end if;
	
	statistic_cell(16)		<= '0';
	if TCP_rcv_FC =  update_win and snd_wnd = x"00000000" then
		statistic_cell(16) 	<= '1';
	end if;
end if;
end process;

wen_rq_ack_pkt <= '1' when TCP_rcv_FC = send_req and Acpt_rq_ack_pkt = '1' else '0';--

stat_wnd_max	<= stat_wnd_max_rg;
stat_wnd_min	<= stat_wnd_min_rg;

process(reset_n,clock)
begin
if reset_n = '0' then
	dupack 				<= (others => '0');
	check_dupack		<= '0';
	statistic_cell(6)	<= '0';
elsif rising_edge(clock) then
	check_dupack				<= '0';
	statistic_cell(6)			<= '0';
	if TCP_rcv_FC = Ack_rcv AND LT_Seq_Nxt = '0' AND GT_Seq_NxtpW = '0' AND GT_Seq_Nxt = '0' AND rcv_FLAG(2) = '0' and rcv_FLAG(1) = '0' AND rcv_FLAG(4) = '1' AND LEQ_ACK_UNA = '0' then --OK
				dupack 			<= (others => '0');
	elsif TCP_rcv_FC = retrans then
		statistic_cell(6)		<= '1';
		if f_fastretransmit_disabled = '0' then  
			if (f_persist = '1' or NE_ack_Snd_UNA = '1') then 
				dupack 			<= (others => '0');
			else
				dupack 			<= dupack + '1';
				check_dupack	<= '1';
			end if;
		end if;
	elsif TCP_state(0) = '1' then	
		dupack 					<= (others => '0');
	end if;
end if;
end process;

process(reset_n,rst_local,clock)
begin
if reset_n = '0' or rst_local = '1' then
	stat_dupack_max_reg 		<= (others => '0');
elsif rising_edge(clock) then
	if dupack > stat_dupack_max_reg and check_dupack = '1' then
		stat_dupack_max_reg 	<= dupack;
	end if;
end if;
end process; 

stat_dupack_max <= stat_dupack_max_reg;

-- congestion control mecanism

process(reset_n,clock)
begin
if reset_n = '0' then
	L_snd_cwnd 								<= (others => '0');
elsif rising_edge(clock) then
	if mem_req_Sync = '1' or load_new_snd_cwnd = '1' then	
		L_snd_cwnd 							<=  snd_cwnd;		
	elsif TCP_rcv_FC = ACk_rcv and LEQ_ACK_UNA = '0' then
		L_snd_cwnd 							<= snd_cwnd;
	elsif TCP_rcv_FC = retrans and f_fastretransmit_disabled = '0' and (f_persist = '1' or NE_ack_Snd_UNA = '1') then
		L_snd_cwnd 							<= snd_cwnd;
	elsif TCP_rcv_FC = retrans  AND f_fastretransmit_disabled = '0' AND f_persist = '0' AND NE_ack_Snd_UNA = '0' and dupack_retrans = '1' then  
		L_snd_cwnd(31 downto 0) 		<= MSS(31 downto 0);
		if 	TCP_REXMT_CWND_SHIFT(7 downto 0) = x"01" then
			L_snd_cwnd(31 downto 1) 	<= MSS(30 downto 0);
		elsif 	TCP_REXMT_CWND_SHIFT(7 downto 0) = x"02" then
			L_snd_cwnd(31 downto 2) 	<= MSS(29 downto 0);
		elsif 	TCP_REXMT_CWND_SHIFT(7 downto 0) = x"03" then	
			L_snd_cwnd(31 downto 3) 	<= MSS(28 downto 0);
		elsif 	TCP_REXMT_CWND_SHIFT(7 downto 0) = x"04" then
			L_snd_cwnd(31 downto 4) 	<= MSS(27 downto 0);
		elsif 	TCP_REXMT_CWND_SHIFT(7 downto 0) = x"05" then
			L_snd_cwnd(31 downto 5) 	<= MSS(26 downto 0);
		elsif 	TCP_REXMT_CWND_SHIFT(7 downto 0) = x"06" then	
			L_snd_cwnd(31 downto 6)		<= MSS(25 downto 0);
		elsif 	TCP_REXMT_CWND_SHIFT(7 downto 0) = x"07" then
			L_snd_cwnd(31 downto 7) 	<= MSS(24 downto 0);
		elsif 	TCP_REXMT_CWND_SHIFT(7 downto 0) = x"08" then
			L_snd_cwnd(31 downto 8) 	<= MSS(23 downto 0);
		end if;
	end if;
end if;
end process;

--####################################################################################################################################
--############################################  GLOBAL state machine for the Sender part  ############################################
--####################################################################################################################################
 
TCP_snd_FC_state:process(clock)
begin  
    if rising_edge(clock) then
        if reset_n = '0' then
	       TCP_snd_FC <= State_S0; 
        else
            statistic_cell(5) 				<= '0';
            Case TCP_snd_FC is
                when State_S0 =>
                    if TCP_state(0) = '1' then -- if closed
                        if mem_req_Sync = '1' then
                            TCP_snd_FC		<=  SEND_SYNC;
                        end if;
                    elsif f_timer_expired = '1' and f_persist = '0' and TCP_state(1) = '1' then
                        TCP_snd_FC 			<=  SEND_SYNC;
                    elsif TCP_state(2) = '1' then -- if established
                        if mem_req_reset = '1' then
                            TCP_snd_FC		<=  Close_state;
                        else
                            TCP_snd_FC		<=  Prep_Send_data_S10;
                        end if;
                    end if;
                    
                when Prep_Send_data_S10 =>
                    TCP_snd_FC 				<=  Prep_Send_data_S11;
                when Prep_Send_data_S11 =>
                    TCP_snd_FC 				<=  Prep_Send_data_S12;
                when Prep_Send_data_S12 =>
                    TCP_snd_FC 				<=  Prep_Send_data_S13;	
                when Prep_Send_data_S13 =>
                    TCP_snd_FC 				<=  Prep_Send_data_S14;
                when Prep_Send_data_S14 =>
                    TCP_snd_FC 				<=  Prep_Send_data_S15;
                when Prep_Send_data_S15 =>
                    TCP_snd_FC 				<=  Prep_Send_data_S16;
                when Prep_Send_data_S16 =>
                    TCP_snd_FC 				<=  Prep_Send_data_S20;	
                    
                when Prep_Send_data_S20 =>
                    if f_persist = '1' and f_timer_expired = '1' and Null_snd_wnd = '1' then		
                        TCP_snd_FC 			<=  SEND_WIN_PROBE;	
                    elsif f_persist = '1' and f_timer_expired = '1' and Null_snd_wnd = '0' and Pos_DT_t_S = '1' then
                        TCP_snd_FC 			<=  SEND_DATA_S50;
                    elsif f_allow_to_send = '1' or (Pos_DT_t_S = '1' and LT_NXT_MAX = '1') then
                        TCP_snd_FC 			<=  SEND_DATA_S50;
                    else
                        TCP_snd_FC 			<=  State_S0; 
                    end if;
                    
                when SEND_DATA_S50 =>	
                    TCP_snd_FC				<= SEND_DATA_S51;
                when SEND_DATA_S51 =>		
                    TCP_snd_FC				<= SEND_DATA_S52;
                when SEND_DATA_S52 =>
                    TCP_snd_FC				<= SEND_DATA_S53;
                    
                when SEND_DATA_S53 =>
                    TCP_snd_FC				<= SEND_DATA_S54;
                when SEND_DATA_S54 =>
                    TCP_snd_FC				<= SEND_DATA_S55;
                when SEND_DATA_S55 =>
                    TCP_snd_FC				<= SEND_DATA_SEG;
        
                when Close_state =>
                    TCP_snd_FC 				<=  Send_Segment;
                when SEND_WIN_PROBE =>
                    statistic_cell(5) 		<= '1';
                    TCP_snd_FC 				<=  Send_Segment;
                when SEND_DATA_SEG =>
                    TCP_snd_FC 				<=  Send_Segment;
                when SEND_SYNC =>
                    TCP_snd_FC 				<=  Send_Segment;
                    
                when Send_Segment =>
                    if Acpt_rq_Send_Pkt = '1' then
                        if send_data = '1' then
                            TCP_snd_FC 		<= update_UNA_OK;
                        else
                            TCP_snd_FC 		<= State_S0; 
                        end if;
                    end if;
                
                when update_UNA_OK =>
                    if Pkt_done = '1' then
                        TCP_snd_FC 			<= State_S0; 
                    end if;
                    
        --		when others =>
        --			TCP_snd_FC 				<= State_S0; 
           end case;
        end if;
    end if;
end process;
state_o <= '1' when TCP_snd_FC = State_S0 else '0';

--*** variable to bypass Update_UNA_OK state if not a send segment of data
process(reset_n,clock)
begin
if reset_n = '0' then
	send_data		<= '0';
elsif rising_edge(clock) then
	if TCP_snd_FC = SEND_DATA_SEG then
		send_data 	<= '1';
	elsif TCP_snd_FC = update_UNA_OK then
		send_data 	<= '0';
	end if;
end if;
end process;

--############################################
-- variable that change with SENDER states
--############################################
process(reset_n,clock)
begin
if reset_n = '0' then
	TCP_state 			<= "001";
elsif rising_edge(clock) then
	if TCP_snd_FC = SEND_SYNC then	 
		TCP_state 		<= "010";
	elsif TCP_rcv_FC = TERM_w_RST or TCP_rcv_FC = TERMINATE or TCP_snd_FC = Close_state then  
		TCP_state 		<= "001";
	elsif Req_ESTABLISH = '1' then  
		TCP_state 		<= "100";
	end if;
end if;
end process;	

TCP_state_o <= TCP_state;

process(reset_n,clock)
begin
if reset_n = '0' then
	pckt_wo_dt 		<= '0';
elsif rising_edge(clock) then
	if TCP_snd_FC = SEND_WIN_PROBE OR TCP_snd_FC = Close_state OR TCP_snd_FC = SEND_SYNC then
		pckt_wo_dt 	<= '1';
	elsif TCP_snd_FC = State_S0 then
		pckt_wo_dt 	<= '0';
	end if;
end if;
end process;

--****************************************************************************************************
----******************************* Manage   SND_NXT   and  SND_MAX
process(reset_n,clock) 
begin
if reset_n = '0' then
	snd_NXT					<= (others => '1');
	snd_MAX 				<= (others => '1');
	Snd_una_OK 				<= (others => '0');
	valid_new_p				<= '0';
	statistic_cell(1) 		<= '0';
	statistic_cell(17) 		<= '0';
elsif rising_edge(clock) then 
	statistic_cell(1) 		<= '0';
	statistic_cell(17) 		<= '0';
	if TCP_snd_FC = SEND_SYNC then  
			snd_NXT 			<= (others => '0'); --ISS + 1
			statistic_cell(1)	<= '1';
	elsif TCP_snd_FC = State_S0	and TCP_state(0) = '0' and f_timer_expired = '1' and f_persist = '0' then 
			snd_NXT 			<= snd_una;	
	elsif TCP_snd_FC = Prep_Send_data_S10 then  
		if LT_NXT_UNA = '1' then  
			snd_NXT 			<= snd_una;
		elsif f_fastretransmit_disabled = '0' and f_fastretransmit = '1' then
			snd_NXT 			<= fastretransmit_seq;
		end if;
	elsif TCP_snd_FC = Prep_Send_data_S20 	and (f_persist = '0' or f_timer_expired = '0') and (f_allow_to_send = '0' and (Pos_DT_t_S = '0' or LT_NXT_MAX = '0')) and (Neg_DT_t_S = '1' and Null_send_win = '1') then  
			snd_NXT 			<= snd_una;
			statistic_cell(17) 	<= '1';
	elsif TCP_snd_FC = SEND_DATA_S53 then  
			snd_NXT 			<= tmp_snd_NXT;
	elsif  TCP_snd_FC = SEND_DATA_S55 then  
		if ff_fastretransmit = '1' then  
			snd_NXT 			<= fastretransmit_saved_seq;
		end if;
	end if;
	
	valid_new_p				<= '0';
	if TCP_snd_FC = Prep_Send_data_S20 	and (f_persist = '0' or f_timer_expired = '0') and (f_allow_to_send = '0' and (Pos_DT_t_S = '0' or LT_NXT_MAX = '0')) and Diff_luna_reg = '1' then   
		Snd_una_OK 			<= L_snd_una;
		valid_new_p			<= '1';
	elsif  TCP_snd_FC = update_UNA_OK and Pkt_done = '1' and Diff_una_reg = '1' then  
		Snd_una_OK 			<= snd_una;
		valid_new_p			<= '1';
	elsif TCP_rcv_FC = update_registers then  
		Snd_una_OK 			<= rcv_seg_ACK;
		valid_new_p			<= '1';
	elsif TCP_snd_FC = State_S0 and mem_req_Sync = '1' then
		Snd_una_OK			<= ISS;
		valid_new_p			<= '1';
	end if;
	
	if TCP_snd_FC = SEND_SYNC then  
		snd_MAX 				<= (others => '0');-- snd_NXT @ sync req
	elsif TCP_snd_FC = SEND_DATA_S55 and GT_LSndX_SndM = '1' then  
		snd_MAX 				<= snd_NXT ;
	end if;
end if;
end process;


process(clock) 
begin
	if rising_edge(clock) then 
		tmp_snd_NXT				<= snd_NXT + L_Length;
	end if;
end process;
---*****************************************  RTT min Max

RTT_MAX_CHECK_i1:comparator 
	generic map(val_signed 	=> false,
				comp_null		=> false,
				pipe				=> true,  
				wd_width			=> 32
		)
	port map(
		reset		=> not_rst_local,
		clock		=> clock,
		dataa		=> Local_RTT,
		datab		=> measure_RTT_max_reg,
		agb		=> RTT_MAX_CHECK 
	);
 

RTT_MIN_CHECK_i1:comparator 
	generic map(val_signed 	=> false,
				comp_null		=> false,
				pipe				=> true,  
				wd_width			=> 32
		)
	port map(
		reset		=> not_rst_local,
		clock		=> clock,
		dataa		=> Local_RTT,
		datab		=> measure_RTT_min_reg,
		alb		=> RTT_MIN_CHECK
		);


process(rst_local,clock)
begin
	if rst_local = '1' then
		measure_RTT_cnt_rg 	<= (others => '0');
	elsif rising_edge(clock) then
		if MEASURE_RTT(2) = '1' then
			measure_RTT_cnt_rg <= measure_RTT_cnt_rg + '1';
		end if;
	end if;
end process;

process(rst_local,clock)
begin
if rst_local = '1' then
	measure_RTT_max_reg			<= (others => '0');
elsif rising_edge(clock) then

	if MEASURE_RTT(2) = '1' then
		if RTT_MAX_CHECK = '1' then
			measure_RTT_max_reg 	<= Local_RTT;
		end if;
	end if;
end if;
end process;

process(rst_local,clock)
begin
if rst_local = '1' then
	measure_RTT_min_reg			<= (others => '1');
elsif rising_edge(clock) then
	if MEASURE_RTT(2) = '1' then
		if RTT_MIN_CHECK = '1' then
			measure_RTT_min_reg 	<= Local_RTT;
		end if;
	end if;
end if;
end process;

process(rst_local,clock)
begin
if rst_local = '1' then
	Measure_RTT_accu_rg			 <= (others => '0');
elsif rising_edge(clock) then
	if MEASURE_RTT(1) = '1' then 
		Measure_RTT_accu_rg 		<= Measure_RTT_accu_rg + Local_RTT;
	elsif TCP_state(0) = '1' then --my implementation
		Measure_RTT_accu_rg 		<= (others => '0');
	end if;
end if;
end process;

measure_RTT_max 	<= measure_RTT_max_reg;
measure_RTT_min 	<= measure_RTT_min_reg;
measure_RTT_cnt 	<= measure_RTT_cnt_rg;
Measure_RTT_accu	<= Measure_RTT_accu_rg;


process(clock)--------------------------------------------------------------  Manage RTT seq
begin
	if rising_edge(clock) then
		if TCP_snd_FC = SEND_DATA_S55 and GT_LSndX_SndM = '1' and f_measure_RTT = '0' then 
			measure_RTT_seq 			<= L_segment_seq;
			measure_RTT_timestamp	<= Get_TIMESTAMP;
		elsif TCP_snd_FC = SEND_SYNC and f_measure_RTT = '0' then   
			measure_RTT_seq 			<= snd_nxt;
			measure_RTT_timestamp	<= Get_TIMESTAMP;
		end if;
	end if;
end process;


process(reset_n,clock)--------------------------------------------------------------  Manage RTT seq
begin
	if reset_n = '0' then
		f_measure_RTT				<= '0';
		MEASURE_RTT					<= (others => '0');
		Local_RTT					<= (others => '0');
		statistic_cell(9)			<= '0';
	elsif rising_edge(clock) then
		statistic_cell(9)			<= '0';
		MEASURE_RTT(5 downto 1) 	<= MEASURE_RTT(4 downto 0);
		MEASURE_RTT(0) 				<= '0';
		
		if 		(TCP_rcv_FC = update_UNA AND f_measure_RTT = '1' AND GT_ACK_RTTseq = '1')        
			OR 	(TCP_rcv_FC = update_registers AND f_measure_RTT = '1') 					then  
			MEASURE_RTT(0) 			<= '1';
		end if;
		
		if MEASURE_RTT(0) = '1' then
			Local_RTT 					<= Get_TIMESTAMP - measure_RTT_timestamp;
			statistic_cell(9)			<= '1';
		end if;
			
		if  TCP_snd_FC = State_S0 AND TCP_state(0) = '0' AND f_timer_expired = '1' AND f_persist = '0' then  
			f_measure_RTT				<= '0';
		elsif TCP_rcv_FC = retrans AND f_fastretransmit_disabled = '0' AND f_persist = '0' AND NE_ack_Snd_UNA = '0' and dupack_retrans = '1' then 
			f_measure_RTT				<= '0';
		elsif MEASURE_RTT(0) = '1' then  
			f_measure_RTT				<= '0';
		elsif out_timer = x"00000001"  and ld_counter = '0' then 
			f_measure_RTT				<= '0';
		elsif TCP_snd_FC = SEND_DATA_S55 and GT_LSndX_SndM = '1' and f_measure_RTT = '0' then  
			f_measure_RTT				<= '1';
		elsif TCP_snd_FC = SEND_SYNC then 
			f_measure_RTT				<= '1';
		end if;
		
	end if;
end process;

rtt_v				<= Local_RTT;

process(snd_new,snd_una)
begin
	lcell_so_length <= SIGNED(snd_new) - SIGNED(snd_una);
end process;
	
	
process(reset_n,clock)		-- prepapre local variable to send a packet
begin
if reset_n = '0' then
	f_Idle					<= '0';
	f_allow_to_send 		<= '0';
	L_timer					<= (others => '0');
	L_data_to_send			<= (others => '0');
	L_snd_una				<= (others => '0'); 
	L_snd_wnd				<= (others => '0');
	L_send_win				<= (others => '0');
	L_data_already_sent	<= (others => '0');
	L_so_length				<= (others => '0');
elsif rising_edge(clock) then
	
	if TCP_snd_FC = Prep_Send_data_S10 then  
		L_snd_una			<= snd_una; 
		L_snd_wnd			<= snd_wnd;
		L_timer				<= out_timer;
		L_so_length			<= lcell_so_length;
		if snd_una = snd_MAX then
			f_Idle			<= '1';
		else 
			f_Idle			<= '0';
		end if;	
	end if;
	
	if  TCP_snd_FC = Prep_Send_data_S11 then 
		L_data_already_sent	<= snd_nxt - L_snd_una;
		if LT_win_cwin = '1' then
			L_send_win			<= L_snd_wnd;
		else
			L_send_win			<= L_snd_cwnd;
		end if;
	end if;
	if  TCP_snd_FC = Prep_Send_data_S13 then  
		if LTsoLenWIN = '1' then
			L_data_to_send		<= L_so_length - L_data_already_sent;
		else
			L_data_to_send		<= L_send_win  - L_data_already_sent;
		end if;
	end if;
	  
	
	if TCP_snd_FC = Prep_Send_data_S16 then   
		if  Pos_DT_t_S = '1' and (  ((f_Idle = '1' or no_delay = '1') and GE_DR_SoLen = '1') or  (GE_DT_t_S_MSS = '1')) then	
			f_allow_to_send 	<= '1';
		else
			f_allow_to_send 	<= '0';
		end if;
	end if;
end if;
end process;

process(clock)		
begin
	if rising_edge(clock) then
		data_ready 					<= L_data_to_send + L_data_already_sent; --Prep_Send_data_S14 
	end if;
end process;
	
process(reset_n,clock) --Persist flag
begin
if reset_n = '0' then
	f_persist 			<= '0';
elsif rising_edge(clock) then --OKOKOK
	if TCP_state(0) = '1' then	
		f_persist 		<= '0';
	elsif TIMER_START_RTT_L = '1' then
		f_persist 		<= '0';
	elsif TIMER_START_PERSIST = '1' then
		f_persist 		<= '1';
	elsif TIMER_STOP = '1' then
		f_persist 		<= '0';
	end if;
end if;
end process;

Persist_state <= f_persist;

process(clock)
begin
if rising_edge(clock) then
	statistic_cell(14) 		<= '0';
	if (TIMER_START_RTT_L = '1' or TIMER_STOP = '1') AND f_persist = '1' then
		statistic_cell(14) 	<= '1';
	end if;
	statistic_cell(15)		<= '0';
	if TIMER_START_PERSIST = '1' and f_persist = '0' then
		statistic_cell(15) 	<= '1';
	end if;
end if;
end process;

process(reset_n,clock) 
begin
if reset_n = '0' then
	DT_tobe_send 			<= (others => '0');
elsif rising_edge(clock) then
	if TCP_snd_FC = SEND_DATA_S51 then  
		DT_tobe_send 		<= L_data_already_sent + L_Length;
	end if;
end if;
end process;


process(reset_n, clock)
begin
if reset_n = '0' then
	FLAG_Send_out 						<= (others => '0');
	L_Length							<= (others => '0');
	L_segment_seq						<= (others => '0');
	ACK_Send_out						<= (others => '0');
	option_Send_out						<= "011";
	statistic_cell(18)					<= '0';
elsif rising_edge(clock) then
	if TCP_snd_FC = SEND_DATA_S50 then 
		if GE_DT_t_S_MSS = '1' then
			L_Length 						<= MSS;
		else
			L_Length 						<= L_data_to_send;
		end if;		
		option_Send_out					<= "000";
	elsif TCP_snd_FC = SEND_SYNC then  
		L_Length 							<= (others => '0');
		L_segment_seq 						<= ISS;
		ACK_Send_out 						<= rcv_nxt;
		option_Send_out					<= "011";
	elsif TCP_snd_FC = SEND_DATA_S53 then -- OK
		L_segment_seq(31 downto 3) 	<= snd_nxt(31 downto 3);
		L_segment_seq(2 downto 0) 		<= "000";
		ACK_Send_out 					<= rcv_nxt;	
		statistic_cell(18)				<= snd_nxt(2) or snd_nxt(1) or snd_nxt(0); -- this 3 bits should be all the time "000" if not there is a problem
	elsif TCP_snd_FC = SEND_WIN_PROBE then 
		L_Length 							<= (others => '0');
		L_segment_seq 						<= snd_una - '1';
		ACK_Send_out 						<= rcv_nxt;	
		option_Send_out					<= "000";
	elsif TCP_snd_FC = Close_state then  
		L_Length 							<= (others => '0');
		L_segment_seq 						<= snd_Nxt;
		ACK_Send_out 						<= (others => '0');	
		option_Send_out					<= "000";
	end if;
	
	if TCP_snd_FC = SEND_SYNC then  
		FLAG_Send_out		<= x"02";-- Sync bit
	elsif TCP_snd_FC = SEND_WIN_PROBE then  
		FLAG_Send_out		<= x"10";-- ACK bit
	elsif TCP_snd_FC = SEND_DATA_S53 then 
		if (EQ_PTS_SoLen = '1' or f_Push = '1') then
			FLAG_Send_out 	<= x"18";-- ACK bit + Push
		else
			FLAG_Send_out	<= x"10";-- ACK bit
		end if;
	elsif TCP_snd_FC = Close_state then  
		FLAG_Send_out 		<= x"04";-- Reset bit 
	end if;
end if;
end process;
SEQ_Send_out		<= L_segment_seq;
Length_Send_out 	<= L_Length;

process(reset_n,clock)-------------------------------fast retransmit
begin
if reset_n = '0' then
	fastretransmit_saved_seq 		<= (others => '0');
	fastretransmit_seq		 		<= (others => '0');
	ff_fastretransmit				<= '0';
	f_fastretransmit 				<= '0';
	statistic_cell(7)				<= '0';
elsif rising_edge(clock) then
	statistic_cell(7)				<= '0';
	if  TCP_snd_FC = Prep_Send_data_S10 and f_fastretransmit_disabled = '0' and f_fastretransmit = '1' then
		f_fastretransmit 				<= '0';
		ff_fastretransmit				<= '1';
	elsif  TCP_snd_FC = SEND_DATA_S55 and ff_fastretransmit = '1' then 
		ff_fastretransmit 			<= '0';
	elsif TCP_rcv_FC = retrans  AND f_fastretransmit_disabled = '0' AND f_persist = '0' AND NE_ack_Snd_UNA = '0' and dupack_retrans = '1' then  
		f_fastretransmit 			<= '1'; 
		statistic_cell(7)			<= '1';
	end if;
	
	if  TCP_snd_FC = Prep_Send_data_S10 and f_fastretransmit_disabled = '0' and f_fastretransmit = '1' then
		fastretransmit_saved_seq 	<= snd_nxt;
	elsif TCP_rcv_FC = retrans  AND f_fastretransmit_disabled = '0' AND f_persist = '0' AND NE_ack_Snd_UNA = '0' and dupack_retrans = '1' then  
		fastretransmit_seq 			<= rcv_seg_ack;
	end if;
end if;
end process;

process(reset_n,rst_local,clock)
begin
if reset_n = '0' or rst_local = '1' then
	Stat_snd_byte_reg					<= (others => '0');
	stat_snd_rexmit_byte_reg			<= (others => '0');
	statistic_cell(3) 					<= '0';
	statistic_cell(4) 					<= '0';
elsif rising_edge(clock) then
	statistic_cell(3) 					<= '0';
	statistic_cell(4) 					<= '0';
	if TCP_snd_FC = SEND_DATA_S51 then
		statistic_cell(3) 				<= '0';
		statistic_cell(4) 				<= '0';
		if LT_NXT_MAX = '1' then
			stat_snd_rexmit_byte_reg	<= stat_snd_rexmit_byte_reg + L_length; 
			statistic_cell(4) 			<= '1';
		else
			Stat_snd_byte_reg			<= Stat_snd_byte_reg + L_length; 
			statistic_cell(3) 			<= '1';
		end if;
	elsif TCP_State(0) = '1' then
		stat_snd_rexmit_byte_reg 		<= (others => '0');
		Stat_snd_byte_reg					<= (others => '0');
	end if;
end if;
end process;

stat_snd_rexmit_byte	<= stat_snd_rexmit_byte_reg;
Stat_snd_byte	  		<= Stat_snd_byte_reg;

--############################################################################################################
--#########################################     TIMER     ####################################################
--############################################################################################################
--##############################  timer 32 bit ## 6.4 ns ##  max timer = 27.4 sec #############################


----i_timer
process(clock,rst_timer)
begin
	if rst_timer = '1' then
		out_timer		<= (others => '0');
	elsif rising_edge(clock) then
		if ld_timer = '1' then
			out_timer	<= dt_timer;
		elsif ena_timer = '1' then
			out_timer	<= out_timer - '1' ;
		end if;
	end if;
end process;
	
process(reset_n,clock)
begin
if reset_n = '0' then
	rst_timer 								<= '1';
	ld_timer									<= '0';
	dt_timer									<= (others => '0');
elsif rising_edge(clock) then
	rst_timer 								<= '0';
	ld_timer									<= '0';
	
	if TCP_state(0) = '1' then
		rst_timer 							<= '1';
	elsif TIMER_START_RTT_L = '1' then  
		if TCP_state(1) = '1' then
			dt_timer 						<= timer_rtt_init_sync;
		else
			dt_timer 						<= (others => '0');
			if rtt_shifts >= TCP_THRESHOLD_RTTSHIFT then
				dt_timer(31 downto 6) 	<= timer_rtt_init(25 downto 0);
			elsif 		rtt_shifts = x"00" then
				dt_timer(31 downto 0) 	<= timer_rtt_init;
			elsif 	rtt_shifts = x"01" then
				dt_timer(31 downto 1) 	<= timer_rtt_init(30 downto 0);
			elsif 	rtt_shifts = x"02" then
				dt_timer(31 downto 2) 	<= timer_rtt_init(29 downto 0);
			elsif 	rtt_shifts = x"03" then
				dt_timer(31 downto 3) 	<= timer_rtt_init(28 downto 0);
			elsif 	rtt_shifts = x"04" then
				dt_timer(31 downto 4) 	<= timer_rtt_init(27 downto 0);
			elsif 	rtt_shifts = x"05" then
				dt_timer(31 downto 5) 	<= timer_rtt_init(26 downto 0);
			end if;
		end if;
		ld_timer	 							<= '1';
	elsif TIMER_START_PERSIST = '1' then  
		dt_timer 							<= timer_persist_init;
		ld_timer 							<= '1';
	elsif TIMER_STOP = '1' then  
		rst_timer 							<= '1';
	end if;
end if;
end process;

process(TCP_state(0),clock)
begin
if TCP_state(0) = '1' then
	ena_timer 		<= '0';
elsif rising_edge(clock) then
	if out_timer > x"00000002" then
		ena_timer 	<= '1';
	else	
		ena_timer 	<= '0';
	end if;
end if;
end process;

process(reset_n,clock) --expired timer
begin
if reset_n = '0' then
	f_timer_expired 				<= '0';
	statistic_cell(8) 				<= '0';
elsif rising_edge(clock) then
	statistic_cell(8) 				<= '0';
	if TCP_state(0) = '1' then
			f_timer_expired 		<= '0'; 
	elsif  TCP_snd_FC = State_S0 AND TCP_state(0) = '0' AND f_timer_expired = '1' AND f_persist = '0' then  
			f_timer_expired 		<= '0'; 
			statistic_cell(8) 		<= '1';
	elsif TCP_snd_FC = Prep_Send_data_S20 then 
		if f_persist = '1' and f_timer_expired = '1' then  
			f_timer_expired 		<= '0'; 
		end if;
	elsif TIMER_STOP = '1' then  
			f_timer_expired 		<= '0'; 	
	elsif out_timer = x"00000001"  and ld_counter = '0' then  
			f_timer_expired 		<= '1';
	end if;
end if;
end process;

process(reset_n,clock) -- rtt shifts  Congestion control
begin
if reset_n = '0' then
	rtt_shifts 			<= (others => '0');
elsif rising_edge(clock) then
	if TIMER_START_PERSIST = '1' or TIMER_STOP = '1' then  
		rtt_shifts 		<= (others => '0');
	elsif out_timer = x"00000001" and f_persist = '0' and rtt_shifts < TCP_MAX_RTTSHIFT and ld_counter = '0' then  
		rtt_shifts 		<= rtt_shifts + '1';
	end if;	
end if;
end process;

process(reset_n,rst_local,clock) --  
begin
if reset_n = '0' or rst_local = '1' then
	rtt_shifts_MAX_reg 			<= (others => '0');
elsif rising_edge(clock) then
	if TCP_state(0) = '1' then
		rtt_shifts_MAX_reg 		<= (others => '0');
	elsif out_timer = x"00000001" and f_persist = '0' and rtt_shifts < TCP_MAX_RTTSHIFT and rtt_shifts > rtt_shifts_MAX_reg and ld_counter = '0' then
		rtt_shifts_MAX_reg 		<= rtt_shifts;
	end if;
end if;
end process;

rtt_shifts_MAX(31 downto 8) 	<= (others => '0');
rtt_shifts_MAX(7 downto 0) 	<= rtt_shifts_MAX_reg;

process(reset_n,clock)
begin
if reset_n = '0' then
	TIMER_START_RTT 			<= '0';
	TIMER_START_PERSIST 		<= '0';
	ld_counter					<= '0';
elsif rising_edge(clock) then
	TIMER_START_RTT 			<= '0';
	TIMER_START_PERSIST 		<= '0';
	
	if 	out_timer = x"00000001" and f_persist = '0' and ld_counter = '0' then
		TIMER_START_RTT 		<= '1';
		ld_counter				<= '1';
	elsif TCP_rcv_FC = Update_UNA and f_persist = '0' and EQ_ACK_MAX = '0' then 
		TIMER_START_RTT 		<= '1';
		ld_counter				<= '1';
	elsif TCP_snd_FC = SEND_DATA_S55 and (out_timer = x"00000000" or f_persist = '1') then 
		TIMER_START_RTT 		<= '1';
		ld_counter				<= '1';
	elsif TCP_snd_FC = SEND_SYNC and out_timer = x"00000000" then 
		TIMER_START_RTT 		<= '1';
		ld_counter				<= '1';
	end if;
	
	if out_timer = x"00000001" and f_persist = '1' and ld_counter = '0' then --OK
		TIMER_START_PERSIST <= '1';
		ld_counter				<= '1';
	elsif  TCP_snd_FC = Prep_Send_data_S20 and ((f_persist = '0' or f_timer_expired = '0')  and (f_allow_to_send = '0' and (Pos_DT_t_S = '0' or LT_NXT_MAX = '0')) and ((Neg_DT_t_S = '1' and Null_send_win = '1') or (Pos_Lso_len ='1' and N_Ltimer = '1'))) then  --OK
		TIMER_START_PERSIST <= '1';
		ld_counter				<= '1';
	end if;
	
	if ld_timer	= '1' then
		ld_counter				<= '0';
	end if;
end if;
end process;	


process(TIMER_START_RTT,TCP_rcv_FC,f_persist,EQ_ACK_MAX)
begin
	if TIMER_START_RTT = '1' OR (TCP_rcv_FC = update_UNA  AND f_persist = '0' AND  EQ_ACK_MAX = '0') then
		TIMER_START_RTT_L <= '1';
	else
		TIMER_START_RTT_L <= '0';
	end if;
end process;
	
process(TCP_rcv_FC,EQ_ACK_MAX,f_fastretransmit_disabled,f_persist,NE_ack_Snd_UNA,dupack_retrans,timer_stop_disable)
begin	
	TIMER_STOP 		<= '0';
	if TCP_rcv_FC = update_UNA  and EQ_ACK_MAX = '1' then 
		TIMER_STOP 	<= '1';
	elsif TCP_rcv_FC = retrans  AND f_fastretransmit_disabled = '0' AND f_persist = '0' AND NE_ack_Snd_UNA = '0' and dupack_retrans = '1' AND timer_stop_disable = '0' then --timer_stop_disable is a config flag
		TIMER_STOP 	<= '1';
	elsif TCP_rcv_FC = update_registers then  
		TIMER_STOP	<= '1';
	end if;
end process;

-- Write a request to send a ACK, PAcket ,.....
wen_rq_Send_pkt <= '1' when TCP_snd_FC = Send_Segment and Acpt_rq_Send_Pkt = '1' else '0';-- 

Pointer_read <= SND_UNA_OK;--when TCP_state(2) = '1'  else (others => '0'); -- latest unacknoledge pointer (can be write bellow)


--##################
-- Flag  signals
process(reset_n,clock)
begin
	if reset_n = '0' then
		flag_reg				<= (others => '0');
	elsif rising_edge(clock) then
		if Req_Sync = '1' then	
			flag_reg(1)		<= '0';
			flag_reg(3)		<= '0';
		elsif TCP_rcv_FC = Check_TCP_state AND  rcv_FLAG(5) = '1' AND TCP_state(0) = '0' then  ---Check_TCP_state and TCP Urgent flag
			flag_reg(3) 	<= '1';
		elsif TCP_rcv_FC = Check_TCP_state AND  rcv_FLAG(0) = '1' AND TCP_state(0) = '0' then ---Check_TCP_state and TCP FINISH flag
			flag_reg(1) 	<= '1';
		end if;

		if Req_Sync = '1' then	
			flag_reg(0)		<= '0';
			flag_reg(4)		<= '0';
			flag_reg(5)		<= '0';
			flag_reg(6)		<= '0';
		elsif TCP_rcv_FC = Ack_Rcv AND  length_Not_Null = '1' AND LT_Seq_Nxt = '0' AND GT_Seq_NxtpW = '0' then	---rcv Length not null
			flag_reg(6) 	<= '1';
		elsif TCP_rcv_FC = Ack_Rcv AND  GT_Seq_Nxt = '1' 		AND LT_Seq_Nxt = '0' AND GT_Seq_NxtpW = '0' then	---Ack_Rcv and SEQ > RCV_NXT
			flag_reg(4) 	<= '1';
		elsif TCP_rcv_FC = Ack_Rcv AND  rcv_FLAG(2) = '1' 		AND LT_Seq_Nxt = '0' AND GT_Seq_NxtpW = '0'  then	---Ack_Rcv and TCP RESET flag
			flag_reg(0) 	<= '1';
		elsif TCP_rcv_FC = Ack_Rcv AND  rcv_FLAG(1) = '1' 		AND LT_Seq_Nxt = '0' AND GT_Seq_NxtpW = '0' then	---Ack_Rcv and TCP SYN flag
			flag_reg(5) 	<= '1';
		end if;	 		
		
		if Req_Sync = '1' then	
			flag_reg(2)		<= '0';
		elsif TCP_rcv_FC = Syn_sent AND  rcv_FLAG(2) = '1' AND rcv_FLAG(4) = '1' then	 --connection refused
			flag_reg(2) 	<= '1';
		end if;	 				
	end if;	
end process;

Stat_flag		<= flag_reg;
statistic		<= statistic_cell;
				
END behavioral;