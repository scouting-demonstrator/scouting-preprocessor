----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 21.04.2017 14:23:45
-- Design Name:
-- Module Name: top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
--use ieee.std_logic_arith.all;

use work.interface.all;
use work.address_table.all;


entity ethernet_block is
    generic (
        addr_offset_100G_eth    : integer := 0;
        QSFP_port_num           : std_logic_vector(7 downto 0)
    );
    port (
        clock_low               : in  std_logic; -- low clock ~1MHz for ARP probe
        MAC_Card                : in  std_logic_vector(47 downto 0);
        MAC_Card_P0             : in  std_logic_vector(47 downto 0);
        Ld_MAC_add              : in  std_logic;
        MAC_itf_ready           : in  std_logic;

        Usr_resetp_TCP_clock    : in  std_logic;
        TCP_clock               : in  std_logic;
        TCP_ack_rcvo            : out TCP_ack_rcvo_record;  -- request to read UR
        TCP_Px_Ack_present      : out std_logic;
        rd_TCP_Px_val_rcvo      : in  std_logic;

        TCP_BLOCK_TO_SEND       : in  HBM_to_TCP_block_record;
        HBM_ack_packet          : out std_logic ;
        TCP_packet_done         : out std_logic;
        Backpressure            : in  std_logic;

        usr_clk                 : in  std_logic;
        usr_rst_n               : in  std_logic;
        usr_func_wr             : in  std_logic_vector(16383 downto 0);
        usr_wen                 : in  std_logic;
        usr_data_wr             : in  std_logic_vector(63 downto 0);

        usr_func_rd             : in  std_logic_vector(16383 downto 0);
        usr_rden                : in  std_logic;
        usr_data_rd             : out std_logic_vector(63 downto 0);
        usr_rd_val              : out std_logic;

        --sync to TCP clock
        TX_lbus_in              : out tx_user_ift;
        TX_lbus_wr              : out std_logic;
        TX_pack_ready           : out std_logic;

        -- signals sync to MAC rx clock
        rx_lbus                 : in  rx_user_ift;
        rx_lbus_clk             : in  std_logic;

        resetp_counter_out      : out std_logic
   );
end ethernet_block;





architecture Behavioral of ethernet_block is

signal req_LLDP					: STD_LOGIC;
signal LLDP_data				: STD_LOGIC_VECTOR(511 downto 0);
signal LLDP_last_data			: STD_LOGIC;
signal rd_LLDP_data				: STD_LOGIC;

signal usr_LLDP_out				: std_logic_vector(63 downto 0);

signal Payload_block_A			: tx_usr_record;
signal Payload_block_B			: tx_usr_record;
signal Payload_block_C			: tx_usr_record;
signal Payload_block_D			: tx_usr_record;
signal Payload_last_data		: std_logic ;
signal Payload_rd_blk 			: std_logic ;

signal TX_lbus_cell						            : tx_user_ift;
signal TX_lbus_wr_cell							    : std_logic;

signal no_transfer_ongoing							: std_logic;
signal end_of_eth_transfert							: std_logic;

signal TCP_Px_request_took		   					: std_logic;

signal req_pause_frame								: std_logic;
signal wait_on_eth_transfert						: std_logic;
signal counter_pause_frame							: std_logic_vector(31 downto 0);
signal delay_pause_frame							: std_logic_vector(15 downto 0);

COMPONENT tcp_ack_fifo
	PORT (
		srst         		: IN STD_LOGIC;
		clk           		: IN STD_LOGIC;
		din         		: IN STD_LOGIC_VECTOR(141 DOWNTO 0);
		wr_en       		: IN STD_LOGIC;
		full        		: OUT STD_LOGIC;
		wr_rst_busy 		: OUT STD_LOGIC;
		rd_en       		: IN STD_LOGIC;
		dout        		: OUT STD_LOGIC_VECTOR(141 DOWNTO 0);
		empty       		: OUT STD_LOGIC;
		rd_rst_busy 		: OUT STD_LOGIC
	);
END COMPONENT;

signal TCP_Px_ack_num_rcv							: std_logic_vector(31 downto 0);
signal TCP_Px_seq_num_rcv							: std_logic_vector(31 downto 0);
signal TCP_Px_win_num_rcv							: std_logic_vector(15 downto 0);
signal TCP_Px_flag_num_rcv							: std_logic_vector(7 downto 0);
signal TCP_Px_dt_len_rcv  							: std_logic_vector(15 downto 0);
signal TCP_Px_new_Scale_rcv							: std_logic_vector(7 downto 0);
signal TCP_Px_update_Scale_rcv						: std_logic;
signal TCP_Px_new_MMS_rcv							: std_logic_vector(15 downto 0);
signal TCP_Px_update_MMS_rcv						: std_logic;
signal TCP_Px_Port_sel_rcv							: std_logic_vector(11 downto 0);

signal TCP_Px_ack_num_rcvo							: std_logic_vector(31 downto 0);
signal TCP_Px_seq_num_rcvo							: std_logic_vector(31 downto 0);
signal TCP_Px_win_num_rcvo							: std_logic_vector(15 downto 0);
signal TCP_Px_flag_num_rcvo							: std_logic_vector(7 downto 0);
signal TCP_Px_dt_len_rcvo							: std_logic_vector(15 downto 0);
signal TCP_Px_new_Scale_rcvo						: std_logic_vector(7 downto 0);
signal TCP_Px_update_Scale_rcvo						: std_logic;
signal TCP_Px_new_MMS_rcvo							: std_logic_vector(15 downto 0);
signal TCP_Px_update_MMS_rcvo						: std_logic;
signal TCP_Px_Port_sel_rcvo							: std_logic_vector(11 downto 0);


signal TCP_Px_Ack_present_n						    : std_logic;
signal TCP_Px_latch_ack								: std_logic;

signal PORT_D										: TCP_port_number;
signal PORT_S										: TCP_port_number;
signal PORTx_D										: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal PORTx_S										: STD_LOGIC_VECTOR(15 DOWNTO 0);

signal MAC_S										: STD_LOGIC_VECTOR(47 downto 0);
signal IP_D											: STD_LOGIC_VECTOR(31 downto 0);
signal IP_S											: STD_LOGIC_VECTOR(31 downto 0);
signal IP_GATEWAY									: STD_LOGIC_VECTOR(31 downto 0);
signal IP_NETWORK									: STD_LOGIC_VECTOR(31 downto 0);
signal NETWORK										: STD_LOGIC_VECTOR(31 downto 0);
signal TCP_max_size								    : STD_LOGIC_VECTOR(15 downto 0);
signal TCP_scale									: STD_LOGIC_VECTOR(7 downto 0);
signal TCP_win										: STD_LOGIC_VECTOR(15 downto 0);
signal TCP_TS										: STD_LOGIC_VECTOR(31 downto 0);
signal TCP_TS_rply								    : STD_LOGIC_VECTOR(31 downto 0);
signal ETH_reg_out								    : STD_LOGIC_VECTOR(63 downto 0);

signal reset_low_clock                          : std_logic;
signal request_probe_arp                        : std_logic_vector(1 downto 0);
signal ARP_acc_done 				            : std_logic;
signal request_arp_rg				            : std_logic_vector(3 downto 0);
signal ARP_Req_done					            : std_logic;
signal probe_ARP_DONE				            : std_logic;
signal IP_dst_and_IP_nwk_eq_Net_Dst	            : std_logic;
signal Req_acc_ARP              	            : std_logic;
signal Req_acc_ARP_sync         	            : std_logic;
signal ARP_probe								: std_logic;
signal ARP_dst  								: std_logic;
signal ARP_gw   								: std_logic;
signal ARP_reply_rcv                            : std_logic;
signal ARP_reply_done                           : std_logic_vector(1 downto 0);

signal ARP_MAC_Dest      						: std_logic_vector(47 downto 0);
signal ARP_MAC_GateWay  						: std_logic_vector(47 downto 0);
signal ARP_check_MAC_src 						: std_logic;
signal ARP_check_IP_src  						: std_logic;
signal ARP_MAC_error    						: std_logic_vector(47 downto 0);
signal ARP_MAC_rcvOK 							: std_logic;
signal ARP_MAC_gwOK  							: std_logic;
signal ARP_IP									: std_logic_vector(31 downto 0);
signal req_ARP_reply 							: std_logic;
signal ARP_reply     							: std_logic;
signal valid_ARP_packet							: std_logic;
signal req_icmp_prb								: std_logic;
signal PING_HD_empty							: std_logic;
signal ping_last_dt								: std_logic;
signal wr_PING_data								: std_logic;
signal data_ping								: std_logic_vector(63 downto 0);
signal Reset_PING_FIFO							: std_logic;
signal PING_MAC_dst								: std_logic_vector(47 downto 0);
signal PING_IP_dst								: std_logic_vector(31 downto 0);

signal Rst_TCP_clock_p							: std_logic;
signal Rst_tx_usr_clk_n							: std_logic;

signal PING_req									: std_logic;
signal PING_req_resync							: std_logic;

signal Ping_time								: std_logic_vector(31 downto 0);
signal Ping_time_latched						: std_logic_vector(31 downto 0);
signal Ping_time_ena							: std_logic ;
signal received_Ping_reply						: std_logic ;
signal destination_MAC							: std_logic_vector(47 downto 0);
signal ping_req_counter							: std_logic_vector(31 downto 0);
signal ping_rec_counter							: std_logic_vector(31 downto 0);
signal counter_rst								: std_logic;
signal counter_rst_resync						: std_logic;

signal request_sync_con0 						: std_logic_vector(1 downto 0);
signal request_sync_con1 						: std_logic_vector(1 downto 0);
signal request_RESET_con0						: std_logic_vector(1 downto 0);
signal request_RESET_con1						: std_logic_vector(1 downto 0);

signal Data_rd_reg								: std_logic_vector(63 downto 0);

signal nb_retrans_val							: std_logic_vector(31 downto 0);
signal nb_retrans_cnt							: std_logic_vector(31 downto 0);
signal stop_transfert_TCP						: std_logic;


-- Conflict IP & MAC
signal conflict_MAC_counter				        : std_logic_vector(31 downto 0);
signal conflict_IP_counter					    : std_logic_vector(31 downto 0);
signal conflict_MAC_value					    : std_logic_vector(47 downto 0);

signal check_oups                               : std_logic;

attribute mark_debug : string;
--attribute mark_debug of xxxxxxx	: signal is "true";


--#############################################################################
-- Code start here
--#############################################################################
begin

---*****************************************************************************************************
--Interface Write  and Read command
process(usr_rst_n,usr_clk)
begin
	if usr_rst_n = '0' then
		counter_rst				<= '0';
		nb_retrans_val			<= (others => '1');
	elsif rising_edge(usr_clk) then
		counter_rst				<= '0';

		if usr_wen = '1'   then
			if usr_func_wr(eth_100Gb_counter_reset 			+  addr_offset_100G_eth) = '1' then
				counter_rst				<= usr_data_wr(0);
			end if;

			if usr_func_wr(eth_100Gb_nb_retrans_before_stop +  addr_offset_100G_eth) = '1' then
				nb_retrans_val			<= usr_data_wr(31 downto 0);
			end if;
		end if;
	end if;
end process;

resetp_counter_out   <= counter_rst;

process(usr_clk)
begin
	if rising_edge(usr_clk) then
 		request_arp_rg(0)				<= '0';
		request_probe_arp(0)			<= '0';
		PING_req						<= '0';
		if usr_wen = '1' then
			if usr_func_wr(eth_100Gb_ETH_request_status 	+ addr_offset_100G_eth) = '1' then
				request_arp_rg(0)   	<= usr_data_wr(0);
				request_probe_arp(0)    <= usr_data_wr(1);
				PING_req				<= usr_data_wr(2);
			end if;
		end if;
	end if;
end process;

process(usr_clk)
begin
	if rising_edge(usr_clk) then
		Data_rd_reg 						    <=	ETH_reg_out ;

		if usr_func_rd(eth_100Gb_counter_reset 				+ addr_offset_100G_eth) = '1' then
			Data_rd_reg(28)    			        <= check_oups;
			Data_rd_reg(0) 			            <= counter_rst;

		elsif usr_func_rd(eth_100Gb_ping_counter_latched 	+  addr_offset_100G_eth) = '1' then
			Data_rd_reg(31 downto 0)		   	<= Ping_time_latched;

		elsif usr_func_rd(eth_100Gb_ping_req_cnt 			+  addr_offset_100G_eth) = '1' then
			Data_rd_reg(31 downto 0)			<= ping_req_counter;
		elsif usr_func_rd(eth_100Gb_ping_rcv_cnt 			+  addr_offset_100G_eth) = '1' then
			Data_rd_reg(31 downto 0)			<= ping_rec_counter;
		elsif  usr_func_rd(eth_100Gb_nb_retrans_before_stop +  addr_offset_100G_eth) = '1' then
			Data_rd_reg(30 downto 0)			<= nb_retrans_val(30 downto 0);
			-- Data_rd_reg(31)						<= stop_transfert_TCP;
		elsif  usr_func_rd(eth_100Gb_pause_frame_rcv 		+  addr_offset_100G_eth) = '1' then
			Data_rd_reg(31 downto 0)			<= counter_pause_frame;
			Data_rd_reg(47 downto 32)			<= delay_pause_frame;

        elsif  usr_func_rd(eth_100Gb_ETH_request_status 	+  addr_offset_100G_eth) = '1' then
			Data_rd_reg(16)			            <= ARP_reply_rcv;
			Data_rd_reg(17)			            <= probe_ARP_DONE;

        elsif  usr_func_rd(eth_100Gb_Dest_MAC_address 	+  addr_offset_100G_eth) = '1' then
			Data_rd_reg(47 downto 0)		    <= destination_MAC;
		end if;

	end if;
end process;

usr_data_rd  	<= Data_rd_reg or usr_LLDP_out;

--******************************************************************************************************
-- resync command request ARP PING ....

eth_cmd_resync_pulsec_i3:entity work.resync_pulse
   port map(
   	aresetn				=> usr_rst_n,
   	clocki			    => usr_clk,
   	in_s				=> ping_req,
   	clocko			    => TCP_clock,
   	out_s			    => ping_req_resync
   	);


--******************************************************************************************************
-- resync reset to MAC 100G tx clock
reset_resync_i4:entity work.resetp_resync
	port map(
		aresetp				=> counter_rst	,
		clock				=> TCP_clock	,
		Resetp_sync			=> counter_rst_resync
		);

Rst_TCP_clock_p		<= Usr_resetp_TCP_clock;
Rst_tx_usr_clk_n 	<= not(Usr_resetp_TCP_clock);

--**************************************************************************************
-- Small buffer to record the data coming from the selected TCP stream (SlinkRocket)
Payload_block_i1:entity work.Block_Buffer_in_eth
 Port Map(
	Resetp				=> Rst_TCP_clock_p,
	Clock				=> TCP_clock,

	PL_from_TCP_Stream	=> TCP_BLOCK_TO_SEND,--: in HBM_to_TCP_block_record;

	PL_TX_lbus_A		=> Payload_block_A,--: out tx_usr_record;
	PL_TX_lbus_B		=> Payload_block_B,--: out tx_usr_record;
	PL_TX_lbus_C		=> Payload_block_C,--: out tx_usr_record;
	PL_TX_lbus_D		=> Payload_block_D,--: out tx_usr_record;
	PL_last_data        => Payload_last_data,
	rd_payload_block	=>  Payload_rd_blk--: in std_logic
 );

--**************************************************************************************
-- LLDP packet  for network discover
LLDP_i1:entity work.LLDP_interface
    generic map (
        port_num                       => QSFP_port_num,
        addr_offset_100G_eth           => addr_offset_100G_eth,
        Simulation                     =>  false
    )
	port map(
		resetn						=> usr_rst_n   ,
		usr_clk						=> usr_clk     ,
		usr_DT						=> usr_data_wr ,
		usr_func_wr					=> usr_func_wr ,
		usr_func_rd					=> usr_func_rd ,
		usr_wr						=> usr_wen     ,
		usr_rden					=> usr_rden    ,
		usr_out						=> usr_LLDP_out,

		MAC_itf_ready               => MAC_itf_ready,
		Low_clock					=> clock_low   ,
		Clock_eth					=> TCP_clock   ,
		MAC_src						=> MAC_Card    ,
		MAC_src_p0					=> MAC_Card_P0  ,
		wr_MAC_to_LLDP_mem			=> Ld_MAC_add   ,
		req_LLDP					=> req_LLDP		,
		LLDP_data					=> LLDP_data	,
		LLDP_last_data				=> LLDP_last_data,
		rd_LLDP_data				=> rd_LLDP_data
	 );


--**************************************************************************************
-- generate ethernet packet (PING ARP TCP)

process(TCP_clock)
begin
    if rising_edge(TCP_clock) then
            -- PORTx_S  <= PORT_S(TO_INTEGER(UNSIGNED(TCP_BLOCK_TO_SEND.Sel_TCP_Stream))-1);
            -- PORTx_D  <= PORT_D(TO_INTEGER(UNSIGNED(TCP_BLOCK_TO_SEND.Sel_TCP_Stream))-1);
		if		 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"01" then
			PORTx_S  <= PORT_S(0);
			PORTx_D  <= PORT_D(0);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"02" then
			PORTx_S  <= PORT_S(1);
			PORTx_D  <= PORT_D(1);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"03" then
			PORTx_S  <= PORT_S(2);
			PORTx_D  <= PORT_D(2);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"04" then
			PORTx_S  <= PORT_S(3);
			PORTx_D  <= PORT_D(3);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"05" then
			PORTx_S  <= PORT_S(4);
			PORTx_D  <= PORT_D(4);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"06" then
			PORTx_S  <= PORT_S(5);
			PORTx_D  <= PORT_D(5);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"07" then
			PORTx_S  <= PORT_S(6);
			PORTx_D  <= PORT_D(6);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"08" then
			PORTx_S  <= PORT_S(7);
			PORTx_D  <= PORT_D(7);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"09" then
			PORTx_S  <= PORT_S(8);
			PORTx_D  <= PORT_D(8);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"0A" then
			PORTx_S  <= PORT_S(9);
			PORTx_D  <= PORT_D(9);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"0B" then
			PORTx_S  <= PORT_S(10);
			PORTx_D  <= PORT_D(10);
		elsif 	 TCP_BLOCK_TO_SEND.Sel_TCP_Stream = x"0C" then
			PORTx_S  <= PORT_S(11);
			PORTx_D  <= PORT_D(11);
		end if;


    end if;
end process;

Gen_eth_packt_i1:entity work.transf_pkt
	port	map(
	CLOCK									=> TCP_clock					    ,
	RESET_p								    => Rst_TCP_clock_p			    	,
	PORT_D     							    => PORTx_D						    ,
	PORT_S 					   		        => PORTx_S						    ,
	MAC_Gateway							    => x"000000000000"			        ,
	MAC_D									=> destination_MAC			        ,
	MAC_S									=> MAC_card					 		,
	IP_D									=> IP_D								,
	IP_S									=> IP_S 							,
	IP_Server							    => x"00000000"					    ,
	IP_GATEWAY							    => IP_GATEWAY					    ,
	IP_NETWORK							    => IP_NETWORK   				    ,
	NETWORK								    => NETWORK				    		,
	IP_dst_and_IP_nwk_eq_Net_Dst	        => IP_dst_and_IP_nwk_eq_Net_Dst     ,
	SEQ_N									=> TCP_BLOCK_TO_SEND.TCP_SEQ_Send		    ,
	ACK_N									=> TCP_BLOCK_TO_SEND.TCP_ACK_Send		    ,
																				-- Data signals for TCP packet
	DATA_pckt_ready					        => TCP_BLOCK_TO_SEND.Request_TCP_packet    ,-- a packet is ready to send (fifo Length and CS is filled)
	PL_TX_lbus_A	   						=> Payload_block_A	    			,
	PL_TX_lbus_B	   						=> Payload_block_B	    			,
	PL_TX_lbus_C	   						=> Payload_block_C	    			,
	PL_TX_lbus_D	 						=> Payload_block_D	    			,
	DATA_pckt_end						    => Payload_last_data				,-- last data of the packet
	RD_DATA_pckt						    => Payload_rd_blk					,-- Read of the packet
	DATA_pckt_CS						    => TCP_BLOCK_TO_SEND.HBM_blk_CS		,-- CheckSum + carry of the packet to send
	RD_CS_LEN							    => HBM_ack_packet 					,

	TCP_req								    => TCP_BLOCK_TO_SEND.Request_TCP_packet 	,-- request a TCP packet --if DATA DATA_pckt_ready should be at '1'
 	TCP_wodt								=> TCP_BLOCK_TO_SEND.TCP_pckt_wo_dt	,-- TCP without data
	TCP_flags							    => TCP_BLOCK_TO_SEND.TCP_FLAG_Send	,-- TCP flags
	TCP_opt								    => TCP_BLOCK_TO_SEND.TCP_option_Send,
	TCP_len								    => TCP_BLOCK_TO_SEND.TCP_Length_Send,-- Number of bytes request to transfert
	TCP_max_size 						    => TCP_BLOCK_TO_SEND.TCP_max_size	,
	TCP_scale							    => TCP_BLOCK_TO_SEND.TCP_scale		,
	TCP_win								    => TCP_BLOCK_TO_SEND.TCP_win		,
	TCP_TS								    => TCP_BLOCK_TO_SEND.TCP_TS			,
	TCP_TS_rply							    => TCP_BLOCK_TO_SEND.TCP_TS_rply	,
	TCP_request_took					    => TCP_Px_request_took              ,

	ARP_req								    => Req_acc_ARP				        ,-- request a ARP packet
	ARP_rply								=> req_ARP_reply				    ,-- request a ARP reply if at '1' a ARP request if at '0'
	ARP_probe							    => ARP_probe       		 	    	,-- probe_ARP looking for a same IP addess
	ARP_Dest								=> ARP_dst						    ,
	ARP_Gateway							    => ARP_gw						    ,
	MAC_D_ARP_reply					        => destination_MAC					, -- From the ARP ETH request , we should reply to
	IP_D_ARP_reply						    => ARP_IP						    ,
	Idle_ARP                                => ARP_acc_done                    ,
	PING_req								=> PING_req_resync			        ,
	ICMP_prbl_req						    => req_icmp_prb				    	,-- request q ICMP problem packet (PING with payload not aligned
	--PING_HD_emp							=> 							    	,-- inform that the previous PING reply is sent out
	WR_DT_PING							    => wr_PING_data				    	,-- write & last_dt do a Ping reply request
	Last_Ping_DT						    => ping_last_dt				    	,
	Data_ping							    => data_ping				 	    ,
	Reset_PING_FIFO					        => Reset_PING_FIFO			        ,
	MAC_D_PING_reply					    => PING_MAC_dst				    	, -- for Ping and ARP (same origin)
	no_transfer_ongoing						=> no_transfer_ongoing				,
	IP_D_PING_reply					        => PING_IP_dst					    ,
	req_LLDP								=> req_LLDP							,
	LLDP_data								=> LLDP_data						,
	LLDP_last_data							=> LLDP_last_data					,
	rd_LLDP_data							=> rd_LLDP_data						,

	TX_lbus_a							    => TX_lbus_cell(0)				    ,
	TX_lbus_b							    => TX_lbus_cell(1)				    ,
	TX_lbus_c							    => TX_lbus_cell(2)				    ,
	TX_lbus_d							    => TX_lbus_cell(3)				    ,
	TX_lbus_wr							    => TX_lbus_wr_cell					,
	TX_pack_ready						    => end_of_eth_transfert            ,
	Backpressure                            => Backpressure    -- the Buffer in front of MAC is almost full stop to prepare the packet
	--no_request_peding				        =>									,--: out std_logic
	);

TX_lbus_wr   <= TX_lbus_wr_cell;
TX_lbus_in	 <= TX_lbus_cell;

TX_pack_ready	<= end_of_eth_transfert;
TCP_packet_done <= TCP_Px_request_took;
---**************************************************************
--  PING monitoring
process(Rst_TCP_clock_p,TCP_clock)
begin
	if Rst_TCP_clock_p = '1' then
		Ping_time_ena				<= '0';
		Ping_time_latched <= (others => '0');
	elsif rising_edge(TCP_clock) then
		if PING_req_resync = '1' then
			Ping_time_ena			<= '1';
		elsif received_Ping_reply = '1' then
			Ping_time_ena			<= '0';
			Ping_time_latched 		<= Ping_time;
		end if;
	end if;
end process;

process(TCP_clock)
begin
	if rising_edge(TCP_clock) then
		if received_Ping_reply = '1' or PING_req_resync = '1' then
			Ping_time		<= (others => '0');
		elsif Ping_time_ena = '1' then
			Ping_time		<= Ping_time + '1';
		end if;
	end if;
end process;


process(counter_rst_resync,TCP_clock)
begin
	if counter_rst_resync = '1' then
		ping_req_counter 		<= (others => '0');
		ping_rec_counter 		<= (others => '0');
	elsif rising_edge(TCP_clock) then
		if ping_req_resync = '1' then
			ping_req_counter 	<= ping_req_counter + '1';
		end if;
		if received_Ping_reply = '1' then
			ping_rec_counter 	<= ping_rec_counter + '1';
		end if;
	end if;
end process;

--***************************************************************************
--**********************    Receiver decoder    *******************************
eth_decoder_i1:entity work.Eth_rcv_pckt
port map
	(
	reset_in_p							    => Rst_TCP_clock_p			    , -- sync to tx_usr_clk
	RX_lbus_in						        => rx_lbus						,
	rx_lbus_clk								=> rx_lbus_clk					,

	MAC_home								=> MAC_card					    ,
	IP_home								    => IP_S						    ,
	IP_Dest								    => IP_D						    ,	-- IP for the ARP request   SERVER IP (used for ARP request  IP Destination)
	PORT_Home						        => PORT_S   					,
	PORT_Host   							=> PORT_D   					,
	IP_GateWay							    => x"00000000"					,	-- IP for the ARP request   GATEWAY IP (used for ARP request  IP gateway)
	--
	request_ARP							    => Req_acc_ARP					,	-- The request will reset the counter for ARP probe
	ARP_MAC_Dest      				        => ARP_MAC_Dest      			,
	ARP_MAC_GateWay  					    => ARP_MAC_GateWay  			,
	ARP_check_MAC_src 				        => ARP_check_MAC_src 			,
	ARP_check_IP_src  				        => ARP_check_IP_src  			,
	ARP_MAC_error    					    => ARP_MAC_error    			,
	ARP_MAC_rcvOK 						    => ARP_MAC_rcvOK 				,
	ARP_MAC_gwOK  						    => ARP_MAC_gwOK  				,
	ARP_IP								    => ARP_IP						,
	ARP_MAC								    => destination_MAC				,
	--
	req_ARP_reply 						    => req_ARP_reply 				,
	ARP_reply     						    => ARP_reply     				,
	valid_ARP_packet					    => valid_ARP_packet			    ,
	--
	req_icmp_prb						    => req_icmp_prb				    ,
	PING_HD_empty						    => '0'							,
	ping_last_dt						    => ping_last_dt				    ,
	wr_PING_data						    => wr_PING_data				    ,
	data_ping							    => data_ping					,
	Reset_PING_FIFO					        => Reset_PING_FIFO				,
	PING_MAC_dst						    => PING_MAC_dst				    ,
	PING_IP_dst							    => PING_IP_dst					,
	received_Ping_reply				        => received_Ping_reply			,
	clock_out							    => TCP_clock					,
	--
	PORTx_TCP_MSS_default					=> TCP_max_size				    ,
	PORTx_PORT_rcv							=> TCP_Px_Port_sel_rcv		    ,
	PORTx_ACK_number						=> TCP_Px_ack_num_rcv			,
	PORTx_SEQ_number						=> TCP_Px_seq_num_rcv			,
	PORTx_TCP_flag							=> TCP_Px_flag_num_rcv			,
	PORTx_TCP_win							=> TCP_Px_win_num_rcv			,
	PORTx_TCP_len							=> TCP_Px_dt_len_rcv			,
	PORTx_latch_ack							=> TCP_Px_latch_ack             ,--: out std_logic;
	PORTx_new_Scale                     	=> TCP_Px_new_Scale_rcv         ,--: out std_logic_vector(7 downto 0);
    PORTx_update_Scale                  	=> TCP_Px_update_Scale_rcv      ,--: out std_logic;
    PORTx_new_MMS                       	=> TCP_Px_new_MMS_rcv           ,--: out std_logic_vector(15 downto 0);
    PORTx_update_MMS                    	=> TCP_Px_update_MMS_rcv        ,--: out std_logic;

	-- Conflict IP & MAC
	conflict_MAC_counter				    => conflict_MAC_counter,
	conflict_IP_counter					    => conflict_IP_counter,
	conflict_MAC_value					    => conflict_MAC_value,

	req_pause_frame							=> req_pause_frame				,
	counter_pause_frame						=> counter_pause_frame			,
	delay_pause_frame						=> delay_pause_frame
	 );

--****************************************
-- Pause Frame

pause_i1:entity work.pause_frame
	port map(
		resetp								=> Rst_TCP_clock_p,
		clock								=> TCP_clock,
		req_pause_frame						=> req_pause_frame,
		pause_quanta						=> delay_pause_frame,
		no_transfer_ongoing					=> no_transfer_ongoing,
		pause_transmition					=> wait_on_eth_transfert
	 );
--****************************************
 -- FIFO to record the ACk received
 FIFO_rcv_i0:tcp_ack_FIFO
	PORT map	(
		srst						=> Rst_TCP_clock_p		            ,
		clk							=> TCP_clock				        ,
		wr_en						=> TCP_Px_latch_ack				    ,
		din(31 downto 0)			=> TCP_Px_ack_num_rcv				,
		din(63 downto 32)			=> TCP_Px_seq_num_rcv				,
		din(79 downto 64)			=> TCP_Px_win_num_rcv				,
		din(87 downto 80)			=> TCP_Px_flag_num_rcv			    ,
		din(103 downto 88)			=> TCP_Px_dt_len_rcv  			    ,
		din(111 downto 104)			=> TCP_Px_new_Scale_rcv	        	,
		din(112)					=> TCP_Px_update_Scale_rcv			,
		din(128 downto 113)			=> TCP_Px_new_MMS_rcv		        ,
		din(129)				    => TCP_Px_update_MMS_rcv		    ,
		din(141 downto 130)			=> TCP_Px_Port_sel_rcv   		    ,

		--full						=>
		rd_en						=> rd_TCP_Px_val_rcvo					,
		empty						=> TCP_Px_Ack_present_n		        , -- '1' = empty
		dout(31 downto 0)			=> TCP_ack_rcvo.TCP_Px_ack_num_rcv		 ,
		dout(63 downto 32)			=> TCP_ack_rcvo.TCP_Px_seq_num_rcv		 ,
		dout(79 downto 64)			=> TCP_ack_rcvo.TCP_Px_win_num_rcv		 ,
		dout(87 downto 80)			=> TCP_ack_rcvo.TCP_Px_flag_num_rcv		 ,
		dout(103 downto 88)		    => TCP_ack_rcvo.TCP_Px_dt_len_rcv		 ,
		dout(111 downto 104)		=> TCP_ack_rcvo.TCP_Px_new_Scale_rcv	 ,
		dout(112)					=> TCP_ack_rcvo.TCP_Px_update_Scale_rcv	 ,
		dout(128 downto 113)		=> TCP_ack_rcvo.TCP_Px_new_MMS_rcv		 ,
		dout(129)					=> TCP_ack_rcvo.TCP_Px_update_MMS_rcv	 ,
		dout(141 downto 130)		=> TCP_ack_rcvo.TCP_Px_Port_sel_rcv
	);


 TCP_Px_Ack_present	<= not(TCP_Px_Ack_present_n);


--************************************************
 --settting of parameters
Ctrl_setup_register_i1:entity work.Ctrl_reg_eth
 	generic map(addr_offset					=>  addr_offset_100G_eth)
	port map
	(
	CLOCK									=> usr_clk						,
	RESET									=> usr_rst_n					,

	Ctrl_DT								    => usr_data_wr					,
	Ctrl_func							    => usr_func_wr              	,
	Ctrl_wr								    => usr_wen						,
	Ctrl_func_rd						    => usr_func_rd					,
	Ctrl_out								=> ETH_reg_out					,

	PORT_D						            => PORT_D                       ,
	PORT_S						            => PORT_S                       ,
	MAC_S									=> MAC_S						,
	IP_D									=> IP_D							,
	IP_S									=> IP_S							,
	IP_GATEWAY							    => IP_GATEWAY					,
	IP_NETWORK							    => IP_NETWORK					,
	NETWORK								    => NETWORK						,

    -- Conflict IP & MAC
    conflict_MAC_counter				    => conflict_MAC_counter         ,
    conflict_IP_counter					    => conflict_IP_counter          ,
    conflict_MAC_value					    => conflict_MAC_value           ,

	MAC									    => MAC_card
	);

-- ****************************************************
-- ARP probe logic


 -- resync ARP probe
 Arp_probe_resync_pulse_pci_clklow:entity work.resync_pulse
	port map(
		aresetn		=> usr_rst_n					,
		clocki		=> usr_clk					,
		in_s		=> request_probe_arp(0)		,
		clocko		=> clock_low					,
		out_s		=> request_probe_arp(1)
		);
 -- resync ARP request
Arp_req_resync_pulse_pci_clklow:entity work.resync_pulse
	port map(
		aresetn		=> usr_rst_n					,
		clocki		=> usr_clk					,
		in_s		=> request_arp_rg(0)				,
		clocko		=> clock_low					,
		out_s		=> request_arp_rg(1)
		);


process(reset_low_clock,clock_low)
begin
	if reset_low_clock = '0' then
		request_arp_rg(2) 		<= '0';
		request_arp_rg(3) 		<= '0';

	elsif rising_edge(clock_low) then
		if ARP_Req_done = '1' then
			request_arp_rg(3) 		<= '0';
		elsif request_arp_rg(2) = '1' and Req_acc_ARP_sync = '0' then
			request_arp_rg(3) 		<= '1';
		end if;

		if request_arp_rg(2) = '1' and Req_acc_ARP_sync = '0' then
			request_arp_rg(2) 		<= '0';
		elsif request_arp_rg(1) = '1' then
			request_arp_rg(2) 		<= '1';
		end if;
	end if;
end process;

ARP_probe_i1:entity work.probe_ARP
	port map (
	 Low_clk								   => clock_low,
	 CLOCK_tx								   => TCP_clock,--  MAC 100Gb clock
	 RESET									   => Rst_tx_usr_clk_n,--: in std_LOGIC;
	 probe_ARP_req							   => request_probe_arp(1),--: in std_logic;
	 req_ARP								   => request_arp_rg(3),--: in std_logic;
	 MAC_S									   => MAC_Card,--: in std_logic_vector(47 downto 0);
	 IP_Gateway								   => IP_GATEWAY ,--: in std_logic_vector(31 downto 0);
	 ARP_acc_done							   => ARP_acc_done,--: in std_logic;  -- ACTIVE LOW
	 IP_dst_and_IP_nwk_eq_Net_Dst		       => IP_dst_and_IP_nwk_eq_Net_Dst,--: in std_logic;
	 Req_acc_ARP							   => Req_acc_ARP,--: out std_logic;
	 ARP_gw									   => ARP_gw,--: out std_logic;
	 ARP_dst								   => ARP_dst,--: out std_logic;
	 ARP_probe								   => ARP_probe,--: out std_logic;
	 probe_DONE								   => probe_ARP_DONE ,--: out std_logic;
	 ARP_req_Done							   => ARP_Req_done,   -- ARP done
	 reset_low_clock						   => reset_low_clock -- resync reset internally
	 );

resync_sig_i1:entity work.resync_sig_gen
port map(
	clocko				=> clock_low,
	input				=> Req_acc_ARP,
	output				=> Req_acc_ARP_sync
	);

--************************************************
-- control if the ARP request received a reply

ARP_reply_done(0)   <= '1' when ARP_reply = '1' and valid_ARP_packet = '1' else '0';

 -- resync ARP reply on our request
Arp_reply_resync_pulse:entity work.resync_pulse
	port map(
		aresetn		=> '1'					,
		clocki		=> TCP_clock			,
		in_s		=> ARP_reply_done(0)	,
		clocko		=> usr_clk				,
		out_s		=> ARP_reply_done(1)
		);


process(usr_rst_n,usr_clk)
begin
    if usr_rst_n = '0' then
        ARP_reply_rcv   <= '1';
    elsif rising_edge(usr_clk) then
        if  request_arp_rg(0) = '1' then
            ARP_reply_rcv <= '0';
        elsif ARP_reply_done(1) = '1' then
            ARP_reply_rcv <= '1';
        end if;
    end if;
end process;


end Behavioral;