----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.01.2018 11:39:48
-- Design Name: 
-- Module Name: SERDES_common_userclk_rx - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.Numeric_std.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity MAC100gb_common_userclk_tx is
	Generic( 	constant P_CONTENTS         			: integer:= 0;
				constant P_FREQ_RATIO_SOURCE_TO_USRCLK  : integer:= 1;
				constant P_FREQ_RATIO_USRCLK_TO_USRCLK2 : integer:= 2
				);
	Port ( 
		gtwiz_userclk_tx_srcclk_in		: in std_logic;
		gtwiz_userclk_tx_reset_in		: in std_logic;
 		gtwiz_userclk_tx_usrclk_out		: out std_logic;
		gtwiz_userclk_tx_usrclk2_out	: out std_logic;
		gtwiz_userclk_tx_active_out		: out std_logic 
 );
end MAC100gb_common_userclk_tx;

architecture Behavioral of MAC100gb_common_userclk_tx is

 -- -------------------------------------------------------------------------------------------------------------------
 -- Local parameters
  -------------------------------------------------------------------------------------------------------------------

 -- Convert integer parameters with known, limited legal range to a 3-bit local parameter values
--
signal P_USRCLK_INT_DIV  				: integer := P_FREQ_RATIO_SOURCE_TO_USRCLK - 1;
signal P_USRCLK_DIV    					: std_logic_vector(2 downto 0) := STD_LOGIC_VECTOR(TO_UNSIGNED(P_USRCLK_INT_DIV,3));
signal P_USRCLK2_INT_DIV 				: integer := (P_FREQ_RATIO_SOURCE_TO_USRCLK * P_FREQ_RATIO_USRCLK_TO_USRCLK2) - 1;
signal P_USRCLK2_DIV     				: std_logic_vector(2 downto 0) := STD_LOGIC_VECTOR(TO_UNSIGNED(P_USRCLK2_INT_DIV,3));

signal gtwiz_userclk_tx_usrclk_cell		: std_logic;
signal gtwiz_userclk_tx_usrclk2_cell	: std_logic;

signal gtwiz_userclk_tx_active_meta 	: std_logic;
signal gtwiz_userclk_tx_active_sync 	: std_logic;

begin


 -- -------------------------------------------------------------------------------------------------------------------
 -- Transmitter user clocking network conditional generation, based on parameter values in module instantiation
 -- -------------------------------------------------------------------------------------------------------------------

   -- Use BUFG_GT instance(s) to drive TXUSRCLK and TXUSRCLK2, inferred for integral source to TXUSRCLK frequency ratio
gen_0:if (P_CONTENTS = 0) generate
     -- Drive TXUSRCLK with BUFG_GT-buffered source clock, dividing the input by the integral source clock to TXUSRCLK
     -- frequency ratio
     BUFG_GT_inst : BUFG_GT
     port map (
        O 		=> gtwiz_userclk_tx_usrclk_cell,             -- 1-bit output: Buffer
        CE 		=> '1',                                      -- 1-bit input: Buffer enable
        CEMASK 	=> '0',                                      -- 1-bit input: CE Mask
        CLR 	=> gtwiz_userclk_tx_reset_in,                -- 1-bit input: Asynchronous clear
        CLRMASK => '0',                                      -- 1-bit input: CLR Mask
        DIV 	=> P_USRCLK_DIV,                             -- 3-bit input: Dynamic divide Value
        I 		=> gtwiz_userclk_tx_srcclk_in                -- 1-bit input: Buffer
     );
end generate;

	gtwiz_userclk_tx_usrclk_out	<= gtwiz_userclk_tx_usrclk_cell;
	
     -- If TXUSRCLK and TXUSRCLK2 frequencies are identical, drive both from the same BUFG_GT. Otherwise, drive
     -- TXUSRCLK2 from a second BUFG_GT instance, dividing the source clock down to the TXUSRCLK2 frequency.

		 gen1:if (P_FREQ_RATIO_USRCLK_TO_USRCLK2 = 1) generate
		 	gtwiz_userclk_tx_usrclk2_cell <= gtwiz_userclk_tx_usrclk_cell;
		 end generate;
		 
		 gen_2:if (P_FREQ_RATIO_USRCLK_TO_USRCLK2 /= 1) generate
		   bufg_gt_usrclk2_inst : BUFG_GT
		   port map (
		      O 		=> gtwiz_userclk_tx_usrclk2_cell,             -- 1-bit output: Buffer
		      CE 		=> '1',                                       -- 1-bit input: Buffer enable
		      CEMASK 	=> '0',                                       -- 1-bit input: CE Mask
		      CLR 		=> gtwiz_userclk_tx_reset_in,                 -- 1-bit input: Asynchronous clear
		      CLRMASK 	=> '0',                                       -- 1-bit input: CLR Mask
		      DIV 		=> P_USRCLK2_DIV,                             -- 3-bit input: Dynamic divide Value
		      I 		=> gtwiz_userclk_tx_srcclk_in                 -- 1-bit input: Buffer
		   );
		 end generate;
		 
	 gtwiz_userclk_tx_usrclk2_out	<= gtwiz_userclk_tx_usrclk2_cell;
 
     -- Indicate active helper block functionality when the BUFG_GT divider is not held in reset

 	process(gtwiz_userclk_tx_usrclk2_cell,gtwiz_userclk_tx_reset_in)
 	begin
 		if gtwiz_userclk_tx_reset_in = '1' then
 			gtwiz_userclk_tx_active_meta <= '0';
			gtwiz_userclk_tx_active_sync <= '0';
 		elsif rising_edge(gtwiz_userclk_tx_usrclk2_cell) then
			gtwiz_userclk_tx_active_meta <= '1';
 			gtwiz_userclk_tx_active_sync <= gtwiz_userclk_tx_active_meta;
 		end if; 	
 	end process;
 	
 	gtwiz_userclk_tx_active_out <= gtwiz_userclk_tx_active_sync;

end Behavioral;
