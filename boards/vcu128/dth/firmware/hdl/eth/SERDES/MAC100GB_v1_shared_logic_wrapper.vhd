----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.09.2019 09:34:36
-- Design Name: 
-- Module Name: MAC100GB_v1_shared_logic_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MAC100GB_v1_shared_logic_wrapper is
 Port ( 
	gt_ref_clk_p           	: in std_logic;
	gt_ref_clk_n           	: in std_logic;
	gt_txusrclk2           	: in std_logic;
	gt_rxusrclk2           	: in std_logic;
	gt_ref_clk_out         	: out std_logic;
	rx_clk                 	: in std_logic;
	gt_powergood           	: in std_logic;
	gt_tx_reset_in         	: in std_logic;
	gt_rx_reset_in         	: in std_logic;
	core_drp_reset         	: in std_logic;  
	core_tx_reset          	: in std_logic;
	tx_reset_out           	: out std_logic;
	core_rx_reset          	: in std_logic;
	rx_reset_out           	: out std_logic;
	rx_serdes_reset_out    	: out std_logic_vector(9 downto 0); 
	qpll0reset             	: in std_logic_vector(0 downto 0);
	qpll0lock              	: out std_logic_vector(0 downto 0);
	qpll0outclk            	: out std_logic_vector(0 downto 0);
	qpll0outrefclk         	: out std_logic_vector(0 downto 0);
	drpclk_common_in       	: in std_logic;  
	common0_drpaddr        	: in std_logic_vector(15 downto 0);
	common0_drpdi          	: in std_logic_vector(15 downto 0);
	common0_drpwe          	: in std_logic;
	common0_drpen          	: in std_logic;
	common0_drprdy         	: out std_logic;
	common0_drpdo          	: out std_logic_vector(15 downto 0);
	qpll1reset             	: in std_logic_vector(0 downto 0);
	qpll1lock              	: out std_logic_vector(0 downto 0);
	qpll1outclk            	: out std_logic_vector(0 downto 0);
	qpll1outrefclk         	: out std_logic_vector(0 downto 0);
	usr_tx_reset           	: out std_logic;
	usr_rx_reset			: out std_logic
	);
end MAC100GB_v1_shared_logic_wrapper;

architecture Behavioral of MAC100GB_v1_shared_logic_wrapper is

component MAC100GBv1_common_wrapper is
port (
	refclk              : in std_logic;
	qpll0reset      	: in std_logic_vector(0 downto 0);
	qpll0lock       	: out std_logic_vector(0 downto 0);
	qpll0outclk     	: out std_logic_vector(0 downto 0);
	qpll0outrefclk  	: out std_logic_vector(0 downto 0);
	drpclk_common_in	: in std_logic;  
	common0_drpaddr 	: in std_logic_vector(15 downto 0);
	common0_drpdi   	: in std_logic_vector(15 downto 0);
	common0_drpwe   	: in std_logic;
	common0_drpen   	: in std_logic;
	common0_drprdy  	: out std_logic;
	common0_drpdo   	: out std_logic_vector(15 downto 0);
	qpll1reset      	: in std_logic_vector(0 downto 0);
	qpll1lock       	: out std_logic_vector(0 downto 0);
	qpll1outclk     	: out std_logic_vector(0 downto 0);
	qpll1outrefclk		: out std_logic_vector(0 downto 0)
    );
end component;

component clk_wrapper is
	port
	(
		gt_ref_CLKi_p		: in std_logic;
		gt_ref_CLKi_n		: in std_logic;
		gt_powergood		: in std_logic;
		
		gt_ref_clk			: out std_logic;
		gt_ref_CLKo			: out std_logic
	 );
end component;

component MAC100Gbv1_reset_wrapper is
  Port ( 
	gt_txusrclk2           	: in std_logic;
	gt_rxusrclk2           	: in std_logic;
	rx_clk					: in std_logic;
	gt_tx_reset_in         	: in std_logic;
	gt_rx_reset_in         	: in std_logic;
	core_drp_reset			: in std_logic;  
	core_tx_reset          	: in std_logic;
	tx_reset_out           	: out std_logic;
	core_rx_reset          	: in std_logic;
	rx_reset_out           	: out std_logic;
	rx_serdes_reset_out    	: out std_logic_vector(9 downto 0); 
	usr_tx_reset			: out std_logic;
	usr_rx_reset			: out std_logic 
  );
end component;	

signal gt_ref_clk : std_logic;

--******************************************************************
--**********    CODE    START    HERE         **********************
--******************************************************************
begin

i_MAC100GBv1_clocking_wrapper:clk_wrapper 
	port map
	(
	gt_ref_CLKi_p			=> gt_ref_clk_p,
	gt_ref_CLKi_n			=> gt_ref_clk_n,
	gt_powergood			=> gt_powergood,
	gt_ref_clk				=> gt_ref_clk,
	gt_ref_CLKo				=> gt_ref_clk_out 
	 ); 

i_MAC100GBv1_reset_wrapper:MAC100Gbv1_reset_wrapper 
  Port map( 
	gt_txusrclk2           	=> gt_txusrclk2           	,
	gt_rxusrclk2           	=> gt_rxusrclk2           	,
	rx_clk                 	=> rx_clk                 	,
	gt_tx_reset_in         	=> gt_tx_reset_in         	,
	gt_rx_reset_in         	=> gt_rx_reset_in         	,
	core_drp_reset         	=> core_drp_reset         	,  
	core_tx_reset          	=> core_tx_reset          	,
	tx_reset_out           	=> tx_reset_out           	,
	core_rx_reset          	=> core_rx_reset          	,
	rx_reset_out           	=> rx_reset_out           	,
	rx_serdes_reset_out    	=> rx_serdes_reset_out    	, 
	usr_tx_reset           	=> usr_tx_reset           	,
	usr_rx_reset			=> usr_rx_reset			 
  ); 
 
i_MAC100GBv1_common_wrapper: MAC100GBv1_common_wrapper
port map(
	refclk              	=> gt_ref_clk,
	qpll0reset      		=> qpll0reset      ,
	qpll0lock       		=> qpll0lock       ,
	qpll0outclk     		=> qpll0outclk     ,
	qpll0outrefclk  		=> qpll0outrefclk  ,
	drpclk_common_in		=> drpclk_common_in,
	common0_drpaddr 		=> common0_drpaddr ,
	common0_drpdi   		=> common0_drpdi   ,
	common0_drpwe   		=> common0_drpwe   ,
	common0_drpen   		=> common0_drpen   ,
	common0_drprdy  		=> common0_drprdy  ,
	common0_drpdo   		=> common0_drpdo   ,
	qpll1reset      		=> qpll1reset      ,
	qpll1lock       		=> qpll1lock       ,
	qpll1outclk     		=> qpll1outclk     ,
	qpll1outrefclk			=> qpll1outrefclk	 
    ); 

end Behavioral;
