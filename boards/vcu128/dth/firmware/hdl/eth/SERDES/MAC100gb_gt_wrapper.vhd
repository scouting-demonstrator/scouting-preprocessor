----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 02.09.2019 11:35:01
-- Design Name:
-- Module Name: MAC100gb_gt_wrapper - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity MAC100gb_gt_wrapper is
    generic (
        QSFP_port_num               : std_logic_vector(7 downto 0)
    );
    port (
        gt_rxp_in                 	: in std_logic_vector(3 downto 0);
        gt_rxn_in                	: in std_logic_vector(3 downto 0);
        gt_txn_out               	: out std_logic_vector(3 downto 0);
        gt_txp_out               	: out std_logic_vector(3 downto 0);

        ctrl_gt_loopback_in        	: in std_logic_vector(2 downto 0);
        gt_rxrecclkout            	: out std_logic_vector(3 downto 0);
        gt_powergoodout           	: out std_logic_vector(3 downto 0);

        -- GT Transceiver debug interface ports
        gt_eyescanreset           	: in std_logic_vector(3 downto 0);
        gt_eyescantrigger         	: in std_logic_vector(3 downto 0);
        gt_rxcdrhold                : in std_logic_vector(3 downto 0);
        rx_polarity             	: in std_logic;
        gt_rxrate                   : in std_logic_vector(11 downto 0);
        tx_diff_ctrl             	: in std_logic_vector(19 downto 0);
        tx_polarity             	: in std_logic;
        gt_txinhibit                : in std_logic_vector(3 downto 0);
        gt_txpippmen                : in std_logic_vector(3 downto 0);
        gt_txpippmsel               : in std_logic_vector(3 downto 0);
        txpostcursor           		: in std_logic_vector(19 downto 0);
        gt_txprbsforceerr_sync      : in std_logic;
        txprecursor            		: in std_logic_vector(19 downto 0);
        gt_eyescandataerror         : out std_logic_vector(3 downto 0);
        gt_txbufstatus             	: out std_logic_vector(7 downto 0);

        gt_rxdfelpmreset            : in std_logic_vector(3 downto 0);
        gt_rxlpmen					: in std_logic_vector(3 downto 0);
        gt_rxprbscntreset_async    	: in std_logic;
        gt_rxprbserr_out           	: out std_logic_vector(3 downto 0);
        gt_rxprbssel_async          : in std_logic_vector(3 downto 0);
        gt_rxresetdone              : out std_logic_vector(3 downto 0);
        gt_txprbssel_async          : in std_logic_vector(3 downto 0);
        gt_txresetdone              : out std_logic_vector(3 downto 0);
        gt_rxbufstatus            	: out std_logic_vector(11 downto 0);
        gtwiz_reset_tx_datapath   	: in std_logic;
        gtwiz_reset_rx_datapath     : in std_logic;
        ctl_gt_loopback             : in std_logic;
        gt_drprstp                  : in std_logic;
        gt_drpclk                 	: in std_logic;
        gt_drpaddr               	: in std_logic_vector(9 downto 0);
        gt_drpen                 	: in std_logic;
        gt_drpdi                 	: in std_logic_vector(63 downto 0);
        gt_drpdo                 	: out std_logic_vector(63 downto 0);
        gt_drprdy                	: out std_logic_vector(3 downto 0);
        gt_drpwe                 	: in std_logic;

        gt_txusrclk2              	: out std_logic;
        gt_rxusrclk2 	            : out std_logic;
        gt_reset_tx_done_out     	: out std_logic;
        gt_reset_rx_done_out     	: out std_logic;
        rx_serdes_clk             	: out std_logic_vector(9 downto 0);

        qpll0clk_in               	: in std_logic_vector(3 downto 0);
        qpll0refclk_in            	: in std_logic_vector(3 downto 0);
        qpll1clk_in               	: in std_logic_vector(3 downto 0);
        qpll1refclk_in            	: in std_logic_vector(3 downto 0);
        gtwiz_reset_qpll0lock_in  	: in std_logic;
        gtwiz_reset_qpll0reset_out	: out std_logic;

        txdata_in                  	: in std_logic_vector(511 downto 0);
        txctrl0_in                 	: in std_logic_vector(63 downto 0);
        txctrl1_in                 	: in std_logic_vector(63 downto 0);

        rxdata_out                 	: out std_logic_vector(511 downto 0);
        rxctrl0_out                	: out std_logic_vector(63 downto 0);
        rxctrl1_out                	: out std_logic_vector(63 downto 0);

        sys_reset                 	: in std_logic;
        --usr_clk                 	: in std_logic;
        init_clk					: in std_logic;

        gt_rxprbslocked_out        	: out std_logic_vector(3 downto 0);

        --gt_rxpcsreset				: in std_logic;
        txpcsreset_in				: in std_logic;

        txoutclksel_in              : in std_logic_vector(2 downto 0)
        --rxoutclksel_async      		: in std_logic_vector(2 downto 0)

    );
end MAC100gb_gt_wrapper;

    architecture Behavioral of MAC100gb_gt_wrapper is

attribute ASYNC_REG			: string;

component MAC100gb_common_userclk_tx is
	Generic( 	constant P_CONTENTS         			: integer:= 0;
				constant P_FREQ_RATIO_SOURCE_TO_USRCLK  : integer:= 1;
				constant P_FREQ_RATIO_USRCLK_TO_USRCLK2 : integer:= 1
				);
	Port (
		gtwiz_userclk_tx_srcclk_in		: in std_logic;
		gtwiz_userclk_tx_reset_in		: in std_logic;
 		gtwiz_userclk_tx_usrclk_out		: out std_logic;
		gtwiz_userclk_tx_usrclk2_out	: out std_logic;
		gtwiz_userclk_tx_active_out		: out std_logic
 );
end component;

component MAC100gb_common_userclk_rx is
	Generic( 	constant P_CONTENTS         			: integer:= 0;
				constant P_FREQ_RATIO_SOURCE_TO_USRCLK  : integer:= 1;
				constant P_FREQ_RATIO_USRCLK_TO_USRCLK2 : integer:= 1
				);
	Port (
		gtwiz_userclk_rx_srcclk_in		: in std_logic;
		gtwiz_userclk_rx_reset_in		: in std_logic;
 		gtwiz_userclk_rx_usrclk_out		: out std_logic;
		gtwiz_userclk_rx_usrclk2_out	: out std_logic;
		gtwiz_userclk_rx_active_out		: out std_logic
 );
end component;

signal cmac_gtwiz_userclk_tx_reset_in						: std_logic;
signal cmac_gtwiz_userclk_rx_reset_in						: std_logic;
signal cmac_gtwiz_userclk_tx_active_out						: std_logic;
signal cmac_gtwiz_userclk_rx_active_out						: std_logic;


signal gtwiz_userclk_tx_srcclk_out						: std_logic_vector(3 downto 0);
signal gtwiz_userclk_tx_usrclk_out							: std_logic;
signal gtwiz_userclk_tx_usrclk2_out                      	: std_logic;
signal txusrclk_in                       	: std_logic_vector(3 downto 0);
signal txusrclk2_in                      	: std_logic_vector(3 downto 0);

signal rxusrclk_out_int						: std_logic_vector(3 downto 0);
signal gtwiz_userclk_rx_usrclk_out							: std_logic;
signal gtwiz_userclk_rx_usrclk2_out                      	: std_logic;
signal rxusrclk_in                       	: std_logic_vector(3 downto 0);
signal rxusrclk2_in                      	: std_logic_vector(3 downto 0);

COMPONENT mac100gb_gt_p00
  PORT (
   gtwiz_userclk_tx_active_in          : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in                 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_qpll0lock_in           : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_qpll0reset_out         : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    drpaddr_in                         : IN STD_LOGIC_VECTOR(39 DOWNTO 0);
    drpclk_in                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    drpdi_in                           : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    drpen_in                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    drpwe_in                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    eyescanreset_in                    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    eyescantrigger_in                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtyrxn_in                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtyrxp_in                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    loopback_in                        : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    qpll0clk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    qpll0refclk_in                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    qpll1clk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    qpll1refclk_in                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxcdrhold_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxdfelfhold_in                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxdfelpmreset_in                   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxlpmen_in                         : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpolarity_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbscntreset_in                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbssel_in                       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    rxrate_in                          : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxusrclk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxusrclk2_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txctrl0_in                         : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl1_in                         : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txdata_in                          : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    txdiffctrl_in                      : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    txinhibit_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpippmen_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpippmsel_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpolarity_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpostcursor_in                    : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    txprbsforceerr_in                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txprbssel_in                       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    txprecursor_in                     : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    txusrclk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txusrclk2_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    drpdo_out                          : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    drprdy_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    eyescandataerror_out               : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtpowergood_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtytxn_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtytxp_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxbufstatus_out                    : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxctrl0_out                        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxctrl1_out                        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxdata_out                         : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    rxoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbserr_out                      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbslocked_out                   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxrecclkout_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxresetdone_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txbufstatus_out                    : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    txoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txprgdivresetdone_out              : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txresetdone_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
     txoutclksel_in                    : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    txpcsreset_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END COMPONENT;



COMPONENT mac100gb_gt_p01
  PORT (
   gtwiz_userclk_tx_active_in          : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in                 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_qpll0lock_in           : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_qpll0reset_out         : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    drpaddr_in                         : IN STD_LOGIC_VECTOR(39 DOWNTO 0);
    drpclk_in                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    drpdi_in                           : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    drpen_in                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    drpwe_in                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    eyescanreset_in                    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    eyescantrigger_in                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtyrxn_in                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtyrxp_in                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    loopback_in                        : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    qpll0clk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    qpll0refclk_in                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    qpll1clk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    qpll1refclk_in                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxcdrhold_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxdfelfhold_in                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxdfelpmreset_in                   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxlpmen_in                         : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpolarity_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbscntreset_in                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbssel_in                       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    rxrate_in                          : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxusrclk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxusrclk2_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txctrl0_in                         : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl1_in                         : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txdata_in                          : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    txdiffctrl_in                      : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    txinhibit_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpippmen_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpippmsel_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpolarity_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpostcursor_in                    : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    txprbsforceerr_in                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txprbssel_in                       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    txprecursor_in                     : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    txusrclk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txusrclk2_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    drpdo_out                          : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    drprdy_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    eyescandataerror_out               : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtpowergood_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtytxn_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtytxp_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxbufstatus_out                    : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxctrl0_out                        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxctrl1_out                        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxdata_out                         : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    rxoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbserr_out                      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbslocked_out                   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxrecclkout_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxresetdone_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txbufstatus_out                    : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    txoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txprgdivresetdone_out              : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txresetdone_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
     txoutclksel_in                    : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    txpcsreset_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END COMPONENT;



COMPONENT mac100gb_gt_p02
  PORT (
   gtwiz_userclk_tx_active_in          : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in                 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_qpll0lock_in           : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_qpll0reset_out         : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    drpaddr_in                         : IN STD_LOGIC_VECTOR(39 DOWNTO 0);
    drpclk_in                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    drpdi_in                           : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    drpen_in                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    drpwe_in                           : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    eyescanreset_in                    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    eyescantrigger_in                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtyrxn_in                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtyrxp_in                          : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    loopback_in                        : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    qpll0clk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    qpll0refclk_in                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    qpll1clk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    qpll1refclk_in                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxcdrhold_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxdfelfhold_in                     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxdfelpmreset_in                   : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxlpmen_in                         : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpolarity_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbscntreset_in                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbssel_in                       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    rxrate_in                          : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxusrclk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxusrclk2_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txctrl0_in                         : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl1_in                         : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txdata_in                          : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    txdiffctrl_in                      : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    txinhibit_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpippmen_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpippmsel_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpolarity_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpostcursor_in                    : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    txprbsforceerr_in                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txprbssel_in                       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    txprecursor_in                     : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    txusrclk_in                        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txusrclk2_in                       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    drpdo_out                          : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    drprdy_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    eyescandataerror_out               : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtpowergood_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtytxn_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtytxp_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxbufstatus_out                    : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxctrl0_out                        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxctrl1_out                        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxdata_out                         : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    rxoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbserr_out                      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxprbslocked_out                   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxrecclkout_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxresetdone_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txbufstatus_out                    : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    txoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txprgdivresetdone_out              : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txresetdone_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
     txoutclksel_in                    : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    txpcsreset_in                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END COMPONENT;

signal reset_all_in                        : std_logic;

signal gtwiz_reset_rx_cdr_stable_out      : STD_LOGIC_VECTOR(0 DOWNTO 0);

COMPONENT resync_v4 is
	port (
		aresetn				: in std_logic;
		clocki			    : in std_logic;
		input				: in std_logic;
		clocko			    : in std_logic;
		output			    : out std_logic
		);
end COMPONENT;

COMPONENT resync_sig_gen is
port (
	clocko				: in std_logic;
	input				: in std_logic;
	output				: out std_logic
	);
end COMPONENT;

component resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic;
	Resetp_sync			: out std_logic
	);
end component;

signal gthrxn_int						                        : std_logic_vector(3 downto 0);
signal gthrxp_int						                        : std_logic_vector(3 downto 0);
signal gthtxn_int						                        : std_logic_vector(3 downto 0);
signal gthtxp_int						                        : std_logic_vector(3 downto 0);

signal gtwiz_userclk_tx_active_out                             : std_logic;
signal gtwiz_userclk_rx_active_out                             : std_logic;

signal gtwiz_reset_all_in                	                    : std_logic_vector(0 downto 0);
signal gtwiz_reset_tx_pll_and_datapath_in	                    : std_logic_vector(0 downto 0);
signal gtwiz_reset_tx_datapath_in        	                    : std_logic_vector(0 downto 0);
signal gtwiz_reset_rx_pll_and_datapath_in	                    : std_logic_vector(0 downto 0);
signal gtwiz_reset_rx_datapath_in        	                    : std_logic_vector(0 downto 0);

signal rxrecclkout_out                                          : std_logic_vector(3 downto 0);
  -- wire [319                                                  :0]   gtwiz_userdata_tx_in

signal gtpowergood_out                                          : std_logic_vector(3 downto 0);
  -- wire [319                                                  :0]   gtwiz_userdata_rx_out
signal rxpmaresetdone_out                                       : std_logic_vector(3 downto 0);
signal txprgdivresetdone_out                                    : std_logic_vector(3 downto 0);
signal txpmaresetdone_out                                       : std_logic_vector(3 downto 0);

signal gt_rxrate_in		                                        : std_logic_vector(11 downto 0);

signal gt_loopback_in                		                    : std_logic_vector(11 downto 0);
signal gt_rxpolarity_in                		                    : std_logic_vector(3 downto 0);
signal rx_polarity_resync              		                    : std_logic;
signal gt_txpolarity_in              		                    : std_logic_vector(3 downto 0);
signal tx_polarity_resync              		                    : std_logic;

signal gt_rxprbscntreset_sync				                    : std_logic;
signal gt_rxprbscntreset_sync_inter                             : std_logic;
attribute ASYNC_REG of  gt_rxprbscntreset_sync                  : signal is "TRUE";
attribute ASYNC_REG of  gt_rxprbscntreset_sync_inter            : signal is "TRUE";
signal gt_rxprbscntreset                                        : std_logic_vector(3 downto 0);

signal gt_txprbsforceerr            							: std_logic_vector(3 downto 0);

signal gt_txprbssel_sync                                        : std_logic_vector(3 downto 0);
signal gt_txprbssel_sync_inter                                  : std_logic_vector(3 downto 0);
attribute ASYNC_REG of  gt_txprbssel_sync                       : signal is "TRUE";
attribute ASYNC_REG of  gt_txprbssel_sync_inter                       : signal is "TRUE";
signal gt_txprbssel                                             : std_logic_vector(15 downto 0);

signal gt_rxprbserr                                             :  std_logic_vector(3 downto 0);

signal gt_rxprbssel_sync                                        : std_logic_vector(3 downto 0);
signal gt_rxprbssel_sync_inter                                  : std_logic_vector(3 downto 0);
attribute ASYNC_REG of  gt_rxprbssel_sync                       : signal is "TRUE";
attribute ASYNC_REG of  gt_rxprbssel_sync_inter                 : signal is "TRUE";
signal gt_rxprbssel                                             : std_logic_vector(15 downto 0);

signal gt_txdiffctrl                            				: std_logic_vector(19 downto 0);
signal gt_txpostcursor                          				: std_logic_vector(19 downto 0);
signal gt_txprecursor 											: std_logic_vector(19 downto 0);
signal gt_txoutclksel_in	 									: std_logic_vector(11 downto 0);
--signal rxoutclksel_in	 										: std_logic_vector(11 downto 0);
signal gt_txpcsreset_in                                         : std_logic_vector(3 downto 0);

signal drpaddr_in 							                    : STD_LOGIC_VECTOR(39 DOWNTO 0);
signal drpclk_in  							                    : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal drpdi_in   							                    : STD_LOGIC_VECTOR(63 DOWNTO 0);
signal drpen_in   							                    : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal drprst_in  							                    : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal drpwe_in   							                    : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal drpdo_out							                    : STD_LOGIC_VECTOR(63 DOWNTO 0);
signal drprdy_out							                    : STD_LOGIC_VECTOR(3 DOWNTO 0);

-- ===================================================================================================================
-- TX/RX USER CLOCKING Helper block integration
-- ===================================================================================================================


--**************************************************************
--************   CODE   START   HERE         *******************
--**************************************************************
begin

gt_txusrclk2        <=  gtwiz_userclk_tx_usrclk2_out;
gt_rxusrclk2 	    <=  gtwiz_userclk_rx_usrclk2_out;

cmac_gtwiz_userclk_tx_reset_in	<= '0' when  txprgdivresetdone_out = "1111"  and txpmaresetdone_out = "1111" else '1';

TX_user_clock:MAC100gb_common_userclk_tx
Generic map( 	P_CONTENTS         			   => 0,
				P_FREQ_RATIO_SOURCE_TO_USRCLK  => 1,
				P_FREQ_RATIO_USRCLK_TO_USRCLK2 => 1)
	Port map(
		gtwiz_userclk_tx_srcclk_in		=> gtwiz_userclk_tx_srcclk_out(0),--: in std_logic;
		gtwiz_userclk_tx_reset_in		=> cmac_gtwiz_userclk_tx_reset_in,--: in std_logic;
 		gtwiz_userclk_tx_usrclk_out		=> gtwiz_userclk_tx_usrclk_out,--: out std_logic;
		gtwiz_userclk_tx_usrclk2_out	=> gtwiz_userclk_tx_usrclk2_out,--: out std_logic;
		gtwiz_userclk_tx_active_out		=> cmac_gtwiz_userclk_tx_active_out --: out std_logic
 );

cmac_gtwiz_userclk_rx_reset_in	<= '0' when rxpmaresetdone_out = "1111" else '1';

RX_user_clock:MAC100gb_common_userclk_rx
Generic map( 	P_CONTENTS         			   => 0,
				P_FREQ_RATIO_SOURCE_TO_USRCLK  => 1,
				P_FREQ_RATIO_USRCLK_TO_USRCLK2 => 1)
	Port map(
		gtwiz_userclk_rx_srcclk_in		=> rxusrclk_out_int(0),--: in std_logic;
		gtwiz_userclk_rx_reset_in		=> cmac_gtwiz_userclk_rx_reset_in,--: in std_logic;
 		gtwiz_userclk_rx_usrclk_out		=> gtwiz_userclk_rx_usrclk_out,--: out std_logic;
		gtwiz_userclk_rx_usrclk2_out	=> gtwiz_userclk_rx_usrclk2_out,--: out std_logic;
		gtwiz_userclk_rx_active_out		=> cmac_gtwiz_userclk_rx_active_out --: out std_logic
 );

rx_serdes_clk(0)    <=  gtwiz_userclk_rx_usrclk2_out;
rx_serdes_clk(1)    <=  gtwiz_userclk_rx_usrclk2_out;
rx_serdes_clk(2)    <=  gtwiz_userclk_rx_usrclk2_out;
rx_serdes_clk(3)    <=  gtwiz_userclk_rx_usrclk2_out;
rx_serdes_clk(4)    <=  '0';
rx_serdes_clk(5)    <=  '0';
rx_serdes_clk(6)    <=  '0';
rx_serdes_clk(7)    <=  '0';
rx_serdes_clk(8)    <=  '0';
rx_serdes_clk(9)    <=  '0';

-- /////////////////////////////////////////////////
--  resync analog control og the GT
--
--resync_sig_i1:resync_sig_gen
--port map(
--	clocko			=> gtwiz_userclk_tx_usrclk2_out,
--	input			=> tx_polarity,
--	output			=> tx_polarity_resync
--	);
 tx_polarity_resync  <= 	tx_polarity;


--resync_sig_i2:resync_sig_gen
--port map(
----	clocko			=> gtwiz_userclk_rx_usrclk2_out,
--	clocko			=> gtwiz_userclk_tx_usrclk2_out,
--	input			=> rx_polarity,
--	output			=> rx_polarity_resync
--	);
rx_polarity_resync <= rx_polarity;

gt_txdiffctrl       <= tx_diff_ctrl ;	-- Async signal(s)
gt_txpostcursor     <= txpostcursor ;	-- Async signal(s)
gt_txprecursor      <= txprecursor ;	-- Async signal(s)

gt_txpolarity_in	<= tx_polarity_resync & tx_polarity_resync & tx_polarity_resync & tx_polarity_resync;
gt_rxpolarity_in	<= rx_polarity_resync & rx_polarity_resync & rx_polarity_resync & rx_polarity_resync;
gt_loopback_in		<= x"FFF" when ctl_gt_loopback = '1' or ctrl_gt_loopback_in = "111" else x"000" ; -- Async signal(s)

-- ///////////////////////////////////////////
-- Control the PRBS signal for TEST PRBS
--


process(gtwiz_userclk_rx_usrclk2_out)
begin
    if rising_edge(gtwiz_userclk_rx_usrclk2_out) then
        gt_rxprbscntreset_sync          <= gt_rxprbscntreset_sync_inter;
        gt_rxprbscntreset_sync_inter    <= gt_rxprbscntreset_async;

        gt_rxprbssel_sync               <= gt_rxprbssel_sync_inter;
        gt_rxprbssel_sync_inter         <= gt_rxprbssel_async;
    end if;
end process;

process(gtwiz_userclk_tx_usrclk2_out)
begin
    if rising_edge(gtwiz_userclk_tx_usrclk2_out) then
        gt_txprbssel_sync               <= gt_txprbssel_sync_inter;
        gt_txprbssel_sync_inter         <= gt_txprbssel_async;

    end if;
end process;


gt_txpcsreset_in                            <= txpcsreset_in & txpcsreset_in & txpcsreset_in & txpcsreset_in;

gt_txoutclksel_in							<= txoutclksel_in & txoutclksel_in & txoutclksel_in & txoutclksel_in; -- Async signal(s)
--rxoutclksel_in								<= rxoutclksel_async & rxoutclksel_async & rxoutclksel_async & rxoutclksel_async;-- Async signal(s)

gt_txprbssel								<= gt_txprbssel_sync & gt_txprbssel_sync & gt_txprbssel_sync & gt_txprbssel_sync;
gt_txprbsforceerr							<= gt_txprbsforceerr_sync & gt_txprbsforceerr_sync & gt_txprbsforceerr_sync & gt_txprbsforceerr_sync;
gt_rxprbssel								<= gt_rxprbssel_sync & gt_rxprbssel_sync & gt_rxprbssel_sync & gt_rxprbssel_sync;
gt_rxprbscntreset                        	<= gt_rxprbscntreset_sync & gt_rxprbscntreset_sync & gt_rxprbscntreset_sync & gt_rxprbscntreset_sync;

gt_rxrate_in								<= "000000000000";

--///////////////////
--Global reset of  the SERDES
--
process(init_clk)
begin
    if rising_edge(init_clk) then
         reset_all_in    <= gtwiz_reset_all_in(0);
    end if;
end process;

gtwiz_reset_all_in(0)                  		<= gt_drprstp;

--/////////////////////
-- GT Cock assignment

rxusrclk_in(0)								<= gtwiz_userclk_rx_usrclk_out;
rxusrclk_in(1)								<= gtwiz_userclk_rx_usrclk_out;
rxusrclk_in(2)								<= gtwiz_userclk_rx_usrclk_out;
rxusrclk_in(3)								<= gtwiz_userclk_rx_usrclk_out;

rxusrclk2_in(0)								<= gtwiz_userclk_rx_usrclk2_out;
rxusrclk2_in(1)								<= gtwiz_userclk_rx_usrclk2_out;
rxusrclk2_in(2)								<= gtwiz_userclk_rx_usrclk2_out;
rxusrclk2_in(3)								<= gtwiz_userclk_rx_usrclk2_out;

txusrclk_in(0)								<= gtwiz_userclk_tx_usrclk_out;
txusrclk_in(1)								<= gtwiz_userclk_tx_usrclk_out;
txusrclk_in(2)								<= gtwiz_userclk_tx_usrclk_out;
txusrclk_in(3)								<= gtwiz_userclk_tx_usrclk_out;

txusrclk2_in(0)								<= gtwiz_userclk_tx_usrclk2_out;
txusrclk2_in(1)								<= gtwiz_userclk_tx_usrclk2_out;
txusrclk2_in(2)								<= gtwiz_userclk_tx_usrclk2_out;
txusrclk2_in(3)								<= gtwiz_userclk_tx_usrclk2_out;
--/////////////////////
-- GT Reset assignment
gtwiz_reset_tx_pll_and_datapath_in(0)		<= '0';
gtwiz_reset_rx_pll_and_datapath_in(0)  		<= '0';
gtwiz_reset_tx_datapath_in(0)          		<= gtwiz_reset_tx_datapath;
gtwiz_reset_rx_datapath_in(0)				<= gtwiz_reset_rx_datapath;


--** Transceiver IO
gthrxn_int(0)								<= gt_rxn_in(0);
gthrxn_int(1)								<= gt_rxn_in(1);
gthrxn_int(2)								<= gt_rxn_in(2);
gthrxn_int(3)								<= gt_rxn_in(3);

gthrxp_int(0)								<= gt_rxp_in(0);
gthrxp_int(1)								<= gt_rxp_in(1);
gthrxp_int(2)								<= gt_rxp_in(2);
gthrxp_int(3)								<= gt_rxp_in(3);

gt_txn_out(0)               					<= gthtxn_int(0);
gt_txn_out(1)               					<= gthtxn_int(1);
gt_txn_out(2)               					<= gthtxn_int(2);
gt_txn_out(3)               					<= gthtxn_int(3);

gt_txp_out(0)               					<= gthtxp_int(0);
gt_txp_out(1)               					<= gthtxp_int(1);
gt_txp_out(2)               					<= gthtxp_int(2);
gt_txp_out(3)               					<= gthtxp_int(3);


gen_mac100gb_gt_p00 : if QSFP_port_num = x"30" generate
    mac_p00 : mac100gb_gt_p00
    PORT MAP(
        drpaddr_in                         => drpaddr_in 			                        , ---------
        drpclk_in                          => drpclk_in  			                        , ---------
        drpdi_in                           => drpdi_in   			                        , ---------
        drpen_in                           => drpen_in   			                        , ---------
        drpwe_in                           => drpwe_in   			                        , ---------
        drpdo_out                          => drpdo_out			                            , ---------
        drprdy_out                         => drprdy_out			                        , ---------

    -- eyescandataerror_out               : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        eyescanreset_in                    => gt_eyescanreset			                    , ---------
        eyescantrigger_in                  => gt_eyescantrigger		                        , ---------
        gtpowergood_out                    => gtpowergood_out			                    , ---------

        gtwiz_reset_all_in(0)              => reset_all_in		                    ,
        gtwiz_reset_clk_freerun_in(0)      => init_clk				        				,
        gtwiz_reset_qpll0lock_in(0)        => gtwiz_reset_qpll0lock_in						,
        gtwiz_reset_qpll0reset_out(0)      => gtwiz_reset_qpll0reset_out					,
        gtwiz_reset_rx_cdr_stable_out      => gtwiz_reset_rx_cdr_stable_out			        ,
        gtwiz_reset_rx_datapath_in         => gtwiz_reset_rx_datapath_in			        ,
        gtwiz_reset_rx_done_out(0)         => gt_reset_rx_done_out				            ,
        gtwiz_reset_rx_pll_and_datapath_in => gtwiz_reset_rx_pll_and_datapath_in			,
        gtwiz_reset_tx_datapath_in         => gtwiz_reset_tx_datapath_in			        ,
        gtwiz_reset_tx_done_out(0)         => gt_reset_tx_done_out			            ,
        gtwiz_reset_tx_pll_and_datapath_in => gtwiz_reset_tx_pll_and_datapath_in			,
        gtwiz_userclk_rx_active_in(0)      => cmac_gtwiz_userclk_rx_active_out,
        gtwiz_userclk_tx_active_in(0)      => cmac_gtwiz_userclk_tx_active_out,
        rxctrl0_out                        => rxctrl0_out			                        , ---------
        rxctrl1_out                        => rxctrl1_out			                        , ---------
        rxdata_out                         => rxdata_out 			                        ,---------

        txctrl0_in                         => txctrl0_in			                        , ---------
        txctrl1_in                         => txctrl1_in			                        , ---------
        txdata_in                          => txdata_in			                            , ---------
        gtyrxn_in                          => gthrxn_int			                        , ---------
        gtyrxp_in                          => gthrxp_int			                        , ---------
        gtytxn_out                         => gthtxn_int			                        , ---------
        gtytxp_out                         => gthtxp_int			                        , ---------
        loopback_in                        => gt_loopback_in			                    , ---------
        qpll0clk_in                        => qpll0clk_in,
        qpll0refclk_in                     => qpll0refclk_in,
        qpll1clk_in                        => qpll1clk_in,
        qpll1refclk_in                     => qpll1refclk_in,
        rxbufstatus_out					   => gt_rxbufstatus								,
        rxcdrhold_in                       => gt_rxcdrhold			                            , ---------
        rxdfelfhold_in                     => "0000"			                            , ---------
        rxdfelpmreset_in                   => gt_rxdfelpmreset			                            , ---------
        rxlpmen_in                         => gt_rxlpmen		                            ,
        rxoutclk_out                       => rxusrclk_out_int								,
        rxpmaresetdone_out                 => rxpmaresetdone_out			                ,---------
        rxpolarity_in                      => gt_rxpolarity_in,
        rxprbscntreset_in                  => gt_rxprbscntreset			                    , ---------
        rxprbserr_out                      => gt_rxprbserr_out			                    , ---------
        rxprbssel_in                       => gt_rxprbssel			                        , ---------
        rxrate_in                          => gt_rxrate_in			                        ,
        rxrecclkout_out                    => rxrecclkout_out			                    ,---------
        rxresetdone_out                    => gt_rxresetdone			                    ,---------
        rxusrclk_in                        => rxusrclk_in									,
        rxusrclk2_in 					   => rxusrclk2_in									,
        txbufstatus_out                    => gt_txbufstatus			                    , ---------
        txdiffctrl_in                      => gt_txdiffctrl			                        , ---------
        txoutclk_out                       => gtwiz_userclk_tx_srcclk_out								,
        txpmaresetdone_out                 => txpmaresetdone_out			                ,---------
        txpolarity_in                      => gt_txpolarity_in			                    , ---------
        txinhibit_in                       => gt_txinhibit			                            , ---------
        txpippmen_in                       => gt_txpippmen			                            , ---------
        txpippmsel_in                      => gt_txpippmsel			                            , ---------
        txpostcursor_in                    => gt_txpostcursor			                    , ---------
        txprbsforceerr_in                  => gt_txprbsforceerr			                    , ---------
        txprbssel_in                       => gt_txprbssel 			                        , ---------
        txprecursor_in                     => gt_txprecursor			                    , ---------
        txprgdivresetdone_out              => txprgdivresetdone_out			                ,---------
        txresetdone_out                    => gt_txresetdone	 							,
        txusrclk_in                        => txusrclk_in									,
        txusrclk2_in                       => txusrclk2_in									,


        txoutclksel_in                     => gt_txoutclksel_in,
        txpcsreset_in                      => gt_txpcsreset_in,
        -- rxoutclksel_in					   => rxoutclksel_in								, ---------
        --rxpcsreset_in                      => gt_rxpcsreset,
            -- rxcdrphdone_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        rxprbslocked_out                   => gt_rxprbslocked_out			                	 ---------
    );
end generate;



gen_mac100gb_gt_p01 : if QSFP_port_num = x"31" generate
    mac_p01 : mac100gb_gt_p01
    PORT MAP(
        drpaddr_in                         => drpaddr_in 			                        , ---------
        drpclk_in                          => drpclk_in  			                        , ---------
        drpdi_in                           => drpdi_in   			                        , ---------
        drpen_in                           => drpen_in   			                        , ---------
        drpwe_in                           => drpwe_in   			                        , ---------
        drpdo_out                          => drpdo_out			                            , ---------
        drprdy_out                         => drprdy_out			                        , ---------

    -- eyescandataerror_out               : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        eyescanreset_in                    => gt_eyescanreset			                    , ---------
        eyescantrigger_in                  => gt_eyescantrigger		                        , ---------
        gtpowergood_out                    => gtpowergood_out			                    , ---------

        gtwiz_reset_all_in(0)              => reset_all_in		                    ,
        gtwiz_reset_clk_freerun_in(0)      => init_clk				        				,
        gtwiz_reset_qpll0lock_in(0)        => gtwiz_reset_qpll0lock_in						,
        gtwiz_reset_qpll0reset_out(0)      => gtwiz_reset_qpll0reset_out					,
        gtwiz_reset_rx_cdr_stable_out      => gtwiz_reset_rx_cdr_stable_out			        ,
        gtwiz_reset_rx_datapath_in         => gtwiz_reset_rx_datapath_in			        ,
        gtwiz_reset_rx_done_out(0)         => gt_reset_rx_done_out				            ,
        gtwiz_reset_rx_pll_and_datapath_in => gtwiz_reset_rx_pll_and_datapath_in			,
        gtwiz_reset_tx_datapath_in         => gtwiz_reset_tx_datapath_in			        ,
        gtwiz_reset_tx_done_out(0)         => gt_reset_tx_done_out			            ,
        gtwiz_reset_tx_pll_and_datapath_in => gtwiz_reset_tx_pll_and_datapath_in			,
        gtwiz_userclk_rx_active_in(0)      => cmac_gtwiz_userclk_rx_active_out,
        gtwiz_userclk_tx_active_in(0)      => cmac_gtwiz_userclk_tx_active_out,
        rxctrl0_out                        => rxctrl0_out			                        , ---------
        rxctrl1_out                        => rxctrl1_out			                        , ---------
        rxdata_out                         => rxdata_out 			                        ,---------

        txctrl0_in                         => txctrl0_in			                        , ---------
        txctrl1_in                         => txctrl1_in			                        , ---------
        txdata_in                          => txdata_in			                            , ---------
        gtyrxn_in                          => gthrxn_int			                        , ---------
        gtyrxp_in                          => gthrxp_int			                        , ---------
        gtytxn_out                         => gthtxn_int			                        , ---------
        gtytxp_out                         => gthtxp_int			                        , ---------
        loopback_in                        => gt_loopback_in			                    , ---------
        qpll0clk_in                        => qpll0clk_in,
        qpll0refclk_in                     => qpll0refclk_in,
        qpll1clk_in                        => qpll1clk_in,
        qpll1refclk_in                     => qpll1refclk_in,
        rxbufstatus_out					   => gt_rxbufstatus								,
        rxcdrhold_in                       => gt_rxcdrhold			                            , ---------
        rxdfelfhold_in                     => "0000"			                            , ---------
        rxdfelpmreset_in                   => gt_rxdfelpmreset			                            , ---------
        rxlpmen_in                         => gt_rxlpmen		                            ,
        rxoutclk_out                       => rxusrclk_out_int								,
        rxpmaresetdone_out                 => rxpmaresetdone_out			                ,---------
        rxpolarity_in                      => gt_rxpolarity_in,
        rxprbscntreset_in                  => gt_rxprbscntreset			                    , ---------
        rxprbserr_out                      => gt_rxprbserr_out			                    , ---------
        rxprbssel_in                       => gt_rxprbssel			                        , ---------
        rxrate_in                          => gt_rxrate_in			                        ,
        rxrecclkout_out                    => rxrecclkout_out			                    ,---------
        rxresetdone_out                    => gt_rxresetdone			                    ,---------
        rxusrclk_in                        => rxusrclk_in									,
        rxusrclk2_in 					   => rxusrclk2_in									,
        txbufstatus_out                    => gt_txbufstatus			                    , ---------
        txdiffctrl_in                      => gt_txdiffctrl			                        , ---------
        txoutclk_out                       => gtwiz_userclk_tx_srcclk_out								,
        txpmaresetdone_out                 => txpmaresetdone_out			                ,---------
        txpolarity_in                      => gt_txpolarity_in			                    , ---------
        txinhibit_in                       => gt_txinhibit			                            , ---------
        txpippmen_in                       => gt_txpippmen			                            , ---------
        txpippmsel_in                      => gt_txpippmsel			                            , ---------
        txpostcursor_in                    => gt_txpostcursor			                    , ---------
        txprbsforceerr_in                  => gt_txprbsforceerr			                    , ---------
        txprbssel_in                       => gt_txprbssel 			                        , ---------
        txprecursor_in                     => gt_txprecursor			                    , ---------
        txprgdivresetdone_out              => txprgdivresetdone_out			                ,---------
        txresetdone_out                    => gt_txresetdone	 							,
        txusrclk_in                        => txusrclk_in									,
        txusrclk2_in                       => txusrclk2_in									,


        txoutclksel_in                     => gt_txoutclksel_in,
        txpcsreset_in                      => gt_txpcsreset_in,
        -- rxoutclksel_in					   => rxoutclksel_in								, ---------
        --rxpcsreset_in                      => gt_rxpcsreset,
            -- rxcdrphdone_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        rxprbslocked_out                   => gt_rxprbslocked_out			                	 ---------
    );
end generate;



gen_mac100gb_gt_p02 : if QSFP_port_num = x"32" generate
    mac_p02 : mac100gb_gt_p02
    PORT MAP(
        drpaddr_in                         => drpaddr_in 			                        , ---------
        drpclk_in                          => drpclk_in  			                        , ---------
        drpdi_in                           => drpdi_in   			                        , ---------
        drpen_in                           => drpen_in   			                        , ---------
        drpwe_in                           => drpwe_in   			                        , ---------
        drpdo_out                          => drpdo_out			                            , ---------
        drprdy_out                         => drprdy_out			                        , ---------

    -- eyescandataerror_out               : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        eyescanreset_in                    => gt_eyescanreset			                    , ---------
        eyescantrigger_in                  => gt_eyescantrigger		                        , ---------
        gtpowergood_out                    => gtpowergood_out			                    , ---------

        gtwiz_reset_all_in(0)              => reset_all_in		                    ,
        gtwiz_reset_clk_freerun_in(0)      => init_clk				        				,
        gtwiz_reset_qpll0lock_in(0)        => gtwiz_reset_qpll0lock_in						,
        gtwiz_reset_qpll0reset_out(0)      => gtwiz_reset_qpll0reset_out					,
        gtwiz_reset_rx_cdr_stable_out      => gtwiz_reset_rx_cdr_stable_out			        ,
        gtwiz_reset_rx_datapath_in         => gtwiz_reset_rx_datapath_in			        ,
        gtwiz_reset_rx_done_out(0)         => gt_reset_rx_done_out				            ,
        gtwiz_reset_rx_pll_and_datapath_in => gtwiz_reset_rx_pll_and_datapath_in			,
        gtwiz_reset_tx_datapath_in         => gtwiz_reset_tx_datapath_in			        ,
        gtwiz_reset_tx_done_out(0)         => gt_reset_tx_done_out			            ,
        gtwiz_reset_tx_pll_and_datapath_in => gtwiz_reset_tx_pll_and_datapath_in			,
        gtwiz_userclk_rx_active_in(0)      => cmac_gtwiz_userclk_rx_active_out,
        gtwiz_userclk_tx_active_in(0)      => cmac_gtwiz_userclk_tx_active_out,
        rxctrl0_out                        => rxctrl0_out			                        , ---------
        rxctrl1_out                        => rxctrl1_out			                        , ---------
        rxdata_out                         => rxdata_out 			                        ,---------

        txctrl0_in                         => txctrl0_in			                        , ---------
        txctrl1_in                         => txctrl1_in			                        , ---------
        txdata_in                          => txdata_in			                            , ---------
        gtyrxn_in                          => gthrxn_int			                        , ---------
        gtyrxp_in                          => gthrxp_int			                        , ---------
        gtytxn_out                         => gthtxn_int			                        , ---------
        gtytxp_out                         => gthtxp_int			                        , ---------
        loopback_in                        => gt_loopback_in			                    , ---------
        qpll0clk_in                        => qpll0clk_in,
        qpll0refclk_in                     => qpll0refclk_in,
        qpll1clk_in                        => qpll1clk_in,
        qpll1refclk_in                     => qpll1refclk_in,
        rxbufstatus_out					   => gt_rxbufstatus								,
        rxcdrhold_in                       => gt_rxcdrhold			                            , ---------
        rxdfelfhold_in                     => "0000"			                            , ---------
        rxdfelpmreset_in                   => gt_rxdfelpmreset			                            , ---------
        rxlpmen_in                         => gt_rxlpmen		                            ,
        rxoutclk_out                       => rxusrclk_out_int								,
        rxpmaresetdone_out                 => rxpmaresetdone_out			                ,---------
        rxpolarity_in                      => gt_rxpolarity_in,
        rxprbscntreset_in                  => gt_rxprbscntreset			                    , ---------
        rxprbserr_out                      => gt_rxprbserr_out			                    , ---------
        rxprbssel_in                       => gt_rxprbssel			                        , ---------
        rxrate_in                          => gt_rxrate_in			                        ,
        rxrecclkout_out                    => rxrecclkout_out			                    ,---------
        rxresetdone_out                    => gt_rxresetdone			                    ,---------
        rxusrclk_in                        => rxusrclk_in									,
        rxusrclk2_in 					   => rxusrclk2_in									,
        txbufstatus_out                    => gt_txbufstatus			                    , ---------
        txdiffctrl_in                      => gt_txdiffctrl			                        , ---------
        txoutclk_out                       => gtwiz_userclk_tx_srcclk_out								,
        txpmaresetdone_out                 => txpmaresetdone_out			                ,---------
        txpolarity_in                      => gt_txpolarity_in			                    , ---------
        txinhibit_in                       => gt_txinhibit			                            , ---------
        txpippmen_in                       => gt_txpippmen			                            , ---------
        txpippmsel_in                      => gt_txpippmsel			                            , ---------
        txpostcursor_in                    => gt_txpostcursor			                    , ---------
        txprbsforceerr_in                  => gt_txprbsforceerr			                    , ---------
        txprbssel_in                       => gt_txprbssel 			                        , ---------
        txprecursor_in                     => gt_txprecursor			                    , ---------
        txprgdivresetdone_out              => txprgdivresetdone_out			                ,---------
        txresetdone_out                    => gt_txresetdone	 							,
        txusrclk_in                        => txusrclk_in									,
        txusrclk2_in                       => txusrclk2_in									,


        txoutclksel_in                     => gt_txoutclksel_in,
        txpcsreset_in                      => gt_txpcsreset_in,
        -- rxoutclksel_in					   => rxoutclksel_in								, ---------
        --rxpcsreset_in                      => gt_rxpcsreset,
            -- rxcdrphdone_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        rxprbslocked_out                   => gt_rxprbslocked_out			                	 ---------
    );
end generate;

gt_powergoodout							<= gtpowergood_out ;
gt_rxrecclkout							<= rxrecclkout_out;

--******************************
--  DRP interface for Channel
drpaddr_in(09 downto 00)				<= gt_drpaddr;
drpaddr_in(19 downto 10)				<= gt_drpaddr;
drpaddr_in(29 downto 20)				<= gt_drpaddr;
drpaddr_in(39 downto 30)				<= gt_drpaddr;
drpclk_in(0)							<= gt_drpclk;
drpclk_in(1)							<= gt_drpclk;
drpclk_in(2)							<= gt_drpclk;
drpclk_in(3)							<= gt_drpclk;
drpdi_in            					<= gt_drpdi;
drpen_in(0)								<= gt_drpen;
drpen_in(1)								<= gt_drpen;
drpen_in(2)								<= gt_drpen;
drpen_in(3)								<= gt_drpen;
drprst_in								<= "0000";
drpwe_in(0)								<= gt_drpwe;
drpwe_in(1)								<= gt_drpwe;
drpwe_in(2)								<= gt_drpwe;
drpwe_in(3)								<= gt_drpwe;
gt_drpdo								<= drpdo_out;
gt_drprdy								<= drprdy_out;
--*******************************

end Behavioral;
