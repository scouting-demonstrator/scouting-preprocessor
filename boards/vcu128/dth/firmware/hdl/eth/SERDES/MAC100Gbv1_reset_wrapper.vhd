----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.09.2019 08:58:15
-- Design Name: 
-- Module Name: MAC100Gbv1_reset_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MAC100Gbv1_reset_wrapper is
  Port ( 
	gt_txusrclk2           	: in std_logic;
	gt_rxusrclk2           	: in std_logic;
	rx_clk					: in std_logic;
	gt_tx_reset_in         	: in std_logic;
	gt_rx_reset_in         	: in std_logic;
	core_drp_reset			: in std_logic;  
	core_tx_reset          	: in std_logic;
	tx_reset_out           	: out std_logic;
	core_rx_reset          	: in std_logic;
	rx_reset_out           	: out std_logic;
	rx_serdes_reset_out    	: out std_logic_vector(9 downto 0); 
	usr_tx_reset			: out std_logic;
	usr_rx_reset			: out std_logic 
  );
end MAC100Gbv1_reset_wrapper;

architecture Behavioral of MAC100Gbv1_reset_wrapper is

component resync_sig_gen is
port (
	clocko				: in std_logic;
	input				: in std_logic;
	output				: out std_logic
	);
end component;
 
signal tx_reset_done                			: std_logic;
signal tx_reset_done_sync_txusrclk2   			: std_logic;
signal rx_reset_done_sync_rx_clk	   			: std_logic;

signal gt_txresetdone_int           			: std_logic;
signal gt_txresetdone_int_sync      			: std_logic;
signal gt_tx_reset_done_inv         			: std_logic;
 
signal rx_reset_done                			: std_logic; 
signal gt_rxresetdone_int           			: std_logic;
signal gt_rxresetdone_int_sync      			: std_logic;
signal gt_rx_reset_done_inv         			: std_logic;
signal gt_rx_reset_done_inv_reg     			: std_logic;
signal gt_rx_reset_done_inv_reg_sync			: std_logic;
signal reset_done_async             			: std_logic;
signal rx_serdes_reset_done         			: std_logic_vector(9 downto 0);
   
signal core_drp_reset_tx_clk_sync				: std_logic;
signal core_tx_reset_sync						: std_logic;  

signal core_drp_reset_rx_clk_sync				: std_logic;
signal core_rx_reset_sync						: std_logic;
signal core_drp_reset_serdes_clk_sync			: std_logic;
  
--********************************************************************
--****************    CODE    START     HERE   ***********************
--********************************************************************
begin

gt_txresetdone_int <= gt_tx_reset_in;

i_MAC100GBv1_exdes_cmac_cdc_sync_gt_txresetdone_int:resync_sig_gen  
port map(
	clocko				=> gt_txusrclk2,
	input				=> gt_txresetdone_int,
	output				=> gt_txresetdone_int_sync 
	);

i_MAC100GBv1_exdes_cmac_cdc_sync_core_drp_reset_tx_clk:resync_sig_gen  
port map(
	clocko				=> gt_txusrclk2,
	input				=> core_drp_reset,
	output				=> core_drp_reset_tx_clk_sync 
	);

i_MAC100GBv1_exdes_cmac_cdc_sync_core_tx_reset:resync_sig_gen  
port map(
	clocko				=> gt_txusrclk2,
	input				=> core_tx_reset,
	output				=> core_tx_reset_sync 
	);

gt_tx_reset_done_inv     <=  not(gt_txresetdone_int_sync);
tx_reset_done            <=  gt_tx_reset_done_inv or core_drp_reset_tx_clk_sync or core_tx_reset_sync;
 
i_MAC100GBv1_0_exdes_cmac_cdc_sync_tx_reset_done_txusrclk2:resync_sig_gen  
port map(
	clocko				=> gt_txusrclk2,
	input				=> tx_reset_done,
	output				=> tx_reset_done_sync_txusrclk2 
	); 
  
usr_tx_reset			 <= tx_reset_done_sync_txusrclk2;
tx_reset_out             <= tx_reset_done_sync_txusrclk2;


gt_rxresetdone_int       <=  gt_rx_reset_in;
rx_serdes_reset_out      <=  rx_serdes_reset_done;

i_MAC100GBv1_0_exdes_cmac_cdc_sync_gt_rxresetdone_int:resync_sig_gen  
port map(
	clocko				=> rx_clk,
	input				=> gt_rxresetdone_int,
	output				=> gt_rxresetdone_int_sync 
	);

i_MAC100GBv1_0_exdes_cmac_cdc_sync_core_drp_reset_rx_clk:resync_sig_gen  
port map(
	clocko				=> rx_clk,
	input				=> core_drp_reset,
	output				=> core_drp_reset_rx_clk_sync 
	);

i_MAC100GBv1_0_exdes_cmac_cdc_sync_core_rx_reset:resync_sig_gen  
port map(
	clocko				=> rx_clk,
	input				=> core_rx_reset,
	output				=> core_rx_reset_sync 
	);

gt_rx_reset_done_inv     <=  not(gt_rxresetdone_int_sync);
rx_reset_done            <=  gt_rx_reset_done_inv or core_drp_reset_rx_clk_sync or core_rx_reset_sync ;


i_MAC100GBv1_0_exdes_cmac_cdc_sync_rx_reset_done_rx_clk:resync_sig_gen  
port map(
	clocko				=> rx_clk,
	input				=> rx_reset_done,
	output				=> rx_reset_done_sync_rx_clk 
	);

usr_rx_reset	<= rx_reset_done_sync_rx_clk;
rx_reset_out	<= rx_reset_done_sync_rx_clk;
 

process(rx_clk)
begin
	if rising_edge(rx_clk) then
		gt_rx_reset_done_inv_reg    <= gt_rx_reset_done_inv;
	end if;
end process;

i_MAC100GBv1_exdes_cmac_cdc_sync_gt_rxresetdone_reg_rxusrclk2:resync_sig_gen  
port map(
	clocko				=> gt_rxusrclk2,
	input				=> gt_rx_reset_done_inv_reg,
	output				=> gt_rx_reset_done_inv_reg_sync 
	);

i_MAC100GBv1_exdes_cmac_cdc_sync_gt_txresetdone_int3:resync_sig_gen  
port map(
	clocko				=> gt_rxusrclk2,
	input				=> core_drp_reset,
	output				=> core_drp_reset_serdes_clk_sync 
	);
	
reset_done_async         <=  gt_rx_reset_done_inv_reg_sync or core_drp_reset_serdes_clk_sync;	

  

rx_serdes_reset_done(0)  <=  reset_done_async;
rx_serdes_reset_done(1)  <=  reset_done_async;
rx_serdes_reset_done(2)  <=  reset_done_async;
rx_serdes_reset_done(3)  <=  reset_done_async;
rx_serdes_reset_done(4)  <=  '1';
rx_serdes_reset_done(5)  <=  '1';
rx_serdes_reset_done(6)  <=  '1';
rx_serdes_reset_done(7)  <=  '1';
rx_serdes_reset_done(8)  <=  '1';
rx_serdes_reset_done(9)  <=  '1';
 
  
end Behavioral;
