
LIBRARY ieee;
LIBRARY UNISIM;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use UNISIM.vcomponents.all;
 
--ENTITY-----------------------------------------------------------
entity clk_wrapper is
	port
	(
		gt_ref_CLKi_p		: in std_logic;
		gt_ref_CLKi_n		: in std_logic;
		gt_powergood		: in std_logic;
		
		gt_ref_clk			: out std_logic;
		gt_ref_CLKo			: out std_logic
	 );
end clk_wrapper;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of clk_wrapper is

signal gt_ref_clk_int : std_logic;
 	
--CODE-------------------------------------------------------------
 begin 
 
IBUFDS_GTE4_inst:IBUFDS_GTE4
port map(
	I		=> gt_ref_CLKi_p,		--1-bitinput:RefertoTransceiverUserGuide
	IB		=> gt_ref_CLKi_n,		--1-bitinput:RefertoTransceiverUserGuide
	CEB		=> '0',	--1-bitinput:RefertoTransceiverUserGuide
	O		=> gt_ref_clk,		--1-bitoutput:RefertoTransceiverUserGuide
	ODIV2	=> gt_ref_clk_int 	--1-bitoutput:RefertoTransceiverUserGuide
	
);

BUFG_GT_REFCLK_INST:BUFG_GT  
port map(
	CE      => gt_powergood,
	CEMASK  => '1',
	CLR     => '0',
	CLRMASK => '1',
	DIV     => "000",
	I       => gt_ref_clk_int,
	O       => gt_ref_CLKo
);



 
 
end behavioral;