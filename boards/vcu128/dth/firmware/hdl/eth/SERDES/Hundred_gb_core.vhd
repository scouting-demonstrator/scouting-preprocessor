----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 18.08.2017 15:39:25
-- Design Name:
-- Module Name: Hundred_gb_core - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
--use IEEE.std_logic_unsigned.all;
use work.interface.all;
use work.address_table.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library xpm;
use xpm.vcomponents.all;

entity Hundred_gb_core is
    generic(
        QSFP_port_num           : std_logic_vector(7 downto 0);
        addr_offset_100G_eth    : integer := 0
    );
    port (
        init_clk                : in std_logic;
        Serdes_Reset_p          : in std_logic;

        rx_lbus                 : out rx_user_ift;
        mac_rx_clk              : out std_logic;
        tx_lbus                 : in tx_user_ift;
        tx_lbus_ready           : out std_logic;
        Usr_resetp_MAC_TX_clock : in std_logic;
        mac_tx_clk              : out std_logic;

        usr_clk                 : in std_logic;
        usr_rst_n               : in std_logic;
        usr_func_wr             : in std_logic_vector(16383 downto 0);
        usr_wen                 : in std_logic;
        usr_data_wr             : in std_logic_vector(63 downto 0);

        usr_func_rd             : in std_logic_vector(16383 downto 0);
        usr_rden                : in std_logic;
        usr_data_rd             : out std_logic_vector(63 downto 0);

        QSFP_TX4_N              : out std_logic;
        QSFP_TX4_P              : out std_logic;
        QSFP_TX3_N              : out std_logic;
        QSFP_TX3_P              : out std_logic;
        QSFP_TX2_N              : out std_logic;
        QSFP_TX2_P              : out std_logic;
        QSFP_TX1_N              : out std_logic;
        QSFP_TX1_P              : out std_logic;

        QSFP_RX4_P              : in std_logic;
        QSFP_RX4_N              : in std_logic;
        QSFP_RX3_P              : in std_logic;
        QSFP_RX3_N              : in std_logic;
        QSFP_RX2_P              : in std_logic;
        QSFP_RX2_N              : in std_logic;
        QSFP_RX1_P              : in std_logic;
        QSFP_RX1_N              : in std_logic;

        GT_ref_clock_p          : in std_logic;
        GT_ref_clock_n          : in std_logic;

        gt_ref_clk_out          : out std_logic;     -- bring mgt clock out

        MAC_ready               : out std_logic;
        status_out              : out std_logic_vector(31 downto 0):= x"00000000"
   );
end Hundred_gb_core;

--*************************************************************************************
architecture Behavioral of Hundred_gb_core is

attribute ASYNC_REG			: string;


COMPONENT cmac100gb_p00
  PORT (
    txdata_in                      : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    txctrl0_in                     : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl1_in                     : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxdata_out                     : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    rxctrl0_out                    : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxctrl1_out                    : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
--    ctl_tx_rsfec_enable            : IN STD_LOGIC;
--    ctl_rx_rsfec_enable            : IN STD_LOGIC;
--    ctl_rsfec_ieee_error_indication_mode : IN STD_LOGIC;
--    ctl_rx_rsfec_enable_correction : IN STD_LOGIC;
--    ctl_rx_rsfec_enable_indication : IN STD_LOGIC;
--    stat_rx_rsfec_am_lock0         : OUT STD_LOGIC;
--    stat_rx_rsfec_am_lock1         : OUT STD_LOGIC;
--    stat_rx_rsfec_am_lock2         : OUT STD_LOGIC;
--    stat_rx_rsfec_am_lock3         : OUT STD_LOGIC;
--    stat_rx_rsfec_corrected_cw_inc : OUT STD_LOGIC;
--    stat_rx_rsfec_cw_inc           : OUT STD_LOGIC;
--    stat_rx_rsfec_err_count0_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_err_count1_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_err_count2_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_err_count3_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_hi_ser           : OUT STD_LOGIC;
--    stat_rx_rsfec_lane_alignment_status : OUT STD_LOGIC;
--    stat_rx_rsfec_lane_fill_0      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_fill_1      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_fill_2      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_fill_3      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_mapping     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--    stat_rx_rsfec_uncorrected_cw_inc : OUT STD_LOGIC;
    rx_dataout0                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout1                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout2                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout3                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_enaout0                     : OUT STD_LOGIC;
    rx_enaout1                     : OUT STD_LOGIC;
    rx_enaout2                     : OUT STD_LOGIC;
    rx_enaout3                     : OUT STD_LOGIC;
    rx_eopout0                     : OUT STD_LOGIC;
    rx_eopout1                     : OUT STD_LOGIC;
    rx_eopout2                     : OUT STD_LOGIC;
    rx_eopout3                     : OUT STD_LOGIC;
    rx_errout0                     : OUT STD_LOGIC;
    rx_errout1                     : OUT STD_LOGIC;
    rx_errout2                     : OUT STD_LOGIC;
    rx_errout3                     : OUT STD_LOGIC;
    rx_mtyout0                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout1                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout2                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout3                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_sopout0                     : OUT STD_LOGIC;
    rx_sopout1                     : OUT STD_LOGIC;
    rx_sopout2                     : OUT STD_LOGIC;
    rx_sopout3                     : OUT STD_LOGIC;
    rx_otn_bip8_0                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_1                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_2                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_3                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_4                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_data_0                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_1                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_2                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_3                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_4                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_ena                     : OUT STD_LOGIC;
    rx_otn_lane0                   : OUT STD_LOGIC;
    rx_otn_vlmarker                : OUT STD_LOGIC;
    rx_preambleout                 : OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
    stat_rx_aligned                : OUT STD_LOGIC;
    stat_rx_aligned_err            : OUT STD_LOGIC;
    stat_rx_bad_code               : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_bad_fcs                : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_bad_preamble           : OUT STD_LOGIC;
    stat_rx_bad_sfd                : OUT STD_LOGIC;
    stat_rx_bip_err_0              : OUT STD_LOGIC;
    stat_rx_bip_err_1              : OUT STD_LOGIC;
    stat_rx_bip_err_10             : OUT STD_LOGIC;
    stat_rx_bip_err_11             : OUT STD_LOGIC;
    stat_rx_bip_err_12             : OUT STD_LOGIC;
    stat_rx_bip_err_13             : OUT STD_LOGIC;
    stat_rx_bip_err_14             : OUT STD_LOGIC;
    stat_rx_bip_err_15             : OUT STD_LOGIC;
    stat_rx_bip_err_16             : OUT STD_LOGIC;
    stat_rx_bip_err_17             : OUT STD_LOGIC;
    stat_rx_bip_err_18             : OUT STD_LOGIC;
    stat_rx_bip_err_19             : OUT STD_LOGIC;
    stat_rx_bip_err_2              : OUT STD_LOGIC;
    stat_rx_bip_err_3              : OUT STD_LOGIC;
    stat_rx_bip_err_4              : OUT STD_LOGIC;
    stat_rx_bip_err_5              : OUT STD_LOGIC;
    stat_rx_bip_err_6              : OUT STD_LOGIC;
    stat_rx_bip_err_7              : OUT STD_LOGIC;
    stat_rx_bip_err_8              : OUT STD_LOGIC;
    stat_rx_bip_err_9              : OUT STD_LOGIC;
    stat_rx_block_lock             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_broadcast              : OUT STD_LOGIC;
    stat_rx_fragment               : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_framing_err_0          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_1          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_10         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_11         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_12         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_13         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_14         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_15         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_16         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_17         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_18         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_19         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_2          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_3          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_4          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_5          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_6          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_7          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_8          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_9          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_valid_0    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_1    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_10   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_11   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_12   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_13   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_14   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_15   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_16   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_17   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_18   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_19   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_2    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_3    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_4    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_5    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_6    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_7    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_8    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_9    : OUT STD_LOGIC;
    stat_rx_got_signal_os          : OUT STD_LOGIC;
    stat_rx_hi_ber                 : OUT STD_LOGIC;
    stat_rx_inrangeerr             : OUT STD_LOGIC;
    stat_rx_internal_local_fault   : OUT STD_LOGIC;
    stat_rx_jabber                 : OUT STD_LOGIC;
    stat_rx_local_fault            : OUT STD_LOGIC;
    stat_rx_mf_err                 : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_mf_len_err             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_mf_repeat_err          : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_misaligned             : OUT STD_LOGIC;
    stat_rx_multicast              : OUT STD_LOGIC;
    stat_rx_oversize               : OUT STD_LOGIC;
    stat_rx_packet_1024_1518_bytes : OUT STD_LOGIC;
    stat_rx_packet_128_255_bytes   : OUT STD_LOGIC;
    stat_rx_packet_1519_1522_bytes : OUT STD_LOGIC;
    stat_rx_packet_1523_1548_bytes : OUT STD_LOGIC;
    stat_rx_packet_1549_2047_bytes : OUT STD_LOGIC;
    stat_rx_packet_2048_4095_bytes : OUT STD_LOGIC;
    stat_rx_packet_256_511_bytes   : OUT STD_LOGIC;
    stat_rx_packet_4096_8191_bytes : OUT STD_LOGIC;
    stat_rx_packet_512_1023_bytes  : OUT STD_LOGIC;
    stat_rx_packet_64_bytes        : OUT STD_LOGIC;
    stat_rx_packet_65_127_bytes    : OUT STD_LOGIC;
    stat_rx_packet_8192_9215_bytes : OUT STD_LOGIC;
    stat_rx_packet_bad_fcs         : OUT STD_LOGIC;
    stat_rx_packet_large           : OUT STD_LOGIC;
    stat_rx_packet_small           : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ctl_rx_enable                  : IN STD_LOGIC;
    ctl_rx_force_resync            : IN STD_LOGIC;
    ctl_rx_test_pattern            : IN STD_LOGIC;
    rx_clk                         : IN STD_LOGIC;
    stat_rx_received_local_fault   : OUT STD_LOGIC;
    stat_rx_remote_fault           : OUT STD_LOGIC;
    stat_rx_status                 : OUT STD_LOGIC;
    stat_rx_stomped_fcs            : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_synced                 : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_synced_err             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_test_pattern_mismatch  : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_toolong                : OUT STD_LOGIC;
    stat_rx_total_bytes            : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    stat_rx_total_good_bytes       : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_rx_total_good_packets     : OUT STD_LOGIC;
    stat_rx_total_packets          : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_truncated              : OUT STD_LOGIC;
    stat_rx_undersize              : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_unicast                : OUT STD_LOGIC;
    stat_rx_vlan                   : OUT STD_LOGIC;
    stat_rx_pcsl_demuxed           : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_pcsl_number_0          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_1          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_10         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_11         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_12         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_13         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_14         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_15         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_16         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_17         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_18         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_19         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_2          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_3          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_4          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_5          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_6          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_7          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_8          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_9          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_tx_bad_fcs                : OUT STD_LOGIC;
    stat_tx_broadcast              : OUT STD_LOGIC;
    stat_tx_frame_error            : OUT STD_LOGIC;
    stat_tx_local_fault            : OUT STD_LOGIC;
    stat_tx_multicast              : OUT STD_LOGIC;
    stat_tx_packet_1024_1518_bytes : OUT STD_LOGIC;
    stat_tx_packet_128_255_bytes   : OUT STD_LOGIC;
    stat_tx_packet_1519_1522_bytes : OUT STD_LOGIC;
    stat_tx_packet_1523_1548_bytes : OUT STD_LOGIC;
    stat_tx_packet_1549_2047_bytes : OUT STD_LOGIC;
    stat_tx_packet_2048_4095_bytes : OUT STD_LOGIC;
    stat_tx_packet_256_511_bytes   : OUT STD_LOGIC;
    stat_tx_packet_4096_8191_bytes : OUT STD_LOGIC;
    stat_tx_packet_512_1023_bytes  : OUT STD_LOGIC;
    stat_tx_packet_64_bytes        : OUT STD_LOGIC;
    stat_tx_packet_65_127_bytes    : OUT STD_LOGIC;
    stat_tx_packet_8192_9215_bytes : OUT STD_LOGIC;
    stat_tx_packet_large           : OUT STD_LOGIC;
    stat_tx_packet_small           : OUT STD_LOGIC;
    stat_tx_total_bytes            : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    stat_tx_total_good_bytes       : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_tx_total_good_packets     : OUT STD_LOGIC;
    stat_tx_total_packets          : OUT STD_LOGIC;
    stat_tx_unicast                : OUT STD_LOGIC;
    stat_tx_vlan                   : OUT STD_LOGIC;
    ctl_tx_enable                  : IN STD_LOGIC;
    ctl_tx_send_idle               : IN STD_LOGIC;
    ctl_tx_send_rfi                : IN STD_LOGIC;
    ctl_tx_send_lfi                : IN STD_LOGIC;
    ctl_tx_test_pattern            : IN STD_LOGIC;
    tx_clk                         : IN STD_LOGIC;
    tx_ovfout                      : OUT STD_LOGIC;
    tx_rdyout                      : OUT STD_LOGIC;
    tx_unfout                      : OUT STD_LOGIC;
    tx_datain0                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain1                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain2                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain3                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_enain0                      : IN STD_LOGIC;
    tx_enain1                      : IN STD_LOGIC;
    tx_enain2                      : IN STD_LOGIC;
    tx_enain3                      : IN STD_LOGIC;
    tx_eopin0                      : IN STD_LOGIC;
    tx_eopin1                      : IN STD_LOGIC;
    tx_eopin2                      : IN STD_LOGIC;
    tx_eopin3                      : IN STD_LOGIC;
    tx_errin0                      : IN STD_LOGIC;
    tx_errin1                      : IN STD_LOGIC;
    tx_errin2                      : IN STD_LOGIC;
    tx_errin3                      : IN STD_LOGIC;
    tx_mtyin0                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin1                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin2                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin3                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_sopin0                      : IN STD_LOGIC;
    tx_sopin1                      : IN STD_LOGIC;
    tx_sopin2                      : IN STD_LOGIC;
    tx_sopin3                      : IN STD_LOGIC;
    tx_preamblein                  : IN STD_LOGIC_VECTOR(55 DOWNTO 0);
    tx_reset_done                  : IN STD_LOGIC;
    rx_reset_done                  : IN STD_LOGIC;
    rx_serdes_reset_done           : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    rx_serdes_clk_in               : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    drp_clk                        : IN STD_LOGIC;
    drp_addr                       : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    drp_di                         : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    drp_en                         : IN STD_LOGIC;
    drp_do                         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    drp_rdy                        : OUT STD_LOGIC;
    drp_we                         : IN STD_LOGIC
  );
END COMPONENT;



COMPONENT cmac100gb_p01
  PORT (
    txdata_in                      : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    txctrl0_in                     : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl1_in                     : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxdata_out                     : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    rxctrl0_out                    : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxctrl1_out                    : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
--    ctl_tx_rsfec_enable            : IN STD_LOGIC;
--    ctl_rx_rsfec_enable            : IN STD_LOGIC;
--    ctl_rsfec_ieee_error_indication_mode : IN STD_LOGIC;
--    ctl_rx_rsfec_enable_correction : IN STD_LOGIC;
--    ctl_rx_rsfec_enable_indication : IN STD_LOGIC;
--    stat_rx_rsfec_am_lock0         : OUT STD_LOGIC;
--    stat_rx_rsfec_am_lock1         : OUT STD_LOGIC;
--    stat_rx_rsfec_am_lock2         : OUT STD_LOGIC;
--    stat_rx_rsfec_am_lock3         : OUT STD_LOGIC;
--    stat_rx_rsfec_corrected_cw_inc : OUT STD_LOGIC;
--    stat_rx_rsfec_cw_inc           : OUT STD_LOGIC;
--    stat_rx_rsfec_err_count0_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_err_count1_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_err_count2_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_err_count3_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_hi_ser           : OUT STD_LOGIC;
--    stat_rx_rsfec_lane_alignment_status : OUT STD_LOGIC;
--    stat_rx_rsfec_lane_fill_0      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_fill_1      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_fill_2      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_fill_3      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_mapping     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--    stat_rx_rsfec_uncorrected_cw_inc : OUT STD_LOGIC;
    rx_dataout0                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout1                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout2                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout3                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_enaout0                     : OUT STD_LOGIC;
    rx_enaout1                     : OUT STD_LOGIC;
    rx_enaout2                     : OUT STD_LOGIC;
    rx_enaout3                     : OUT STD_LOGIC;
    rx_eopout0                     : OUT STD_LOGIC;
    rx_eopout1                     : OUT STD_LOGIC;
    rx_eopout2                     : OUT STD_LOGIC;
    rx_eopout3                     : OUT STD_LOGIC;
    rx_errout0                     : OUT STD_LOGIC;
    rx_errout1                     : OUT STD_LOGIC;
    rx_errout2                     : OUT STD_LOGIC;
    rx_errout3                     : OUT STD_LOGIC;
    rx_mtyout0                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout1                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout2                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout3                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_sopout0                     : OUT STD_LOGIC;
    rx_sopout1                     : OUT STD_LOGIC;
    rx_sopout2                     : OUT STD_LOGIC;
    rx_sopout3                     : OUT STD_LOGIC;
    rx_otn_bip8_0                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_1                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_2                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_3                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_4                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_data_0                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_1                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_2                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_3                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_4                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_ena                     : OUT STD_LOGIC;
    rx_otn_lane0                   : OUT STD_LOGIC;
    rx_otn_vlmarker                : OUT STD_LOGIC;
    rx_preambleout                 : OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
    stat_rx_aligned                : OUT STD_LOGIC;
    stat_rx_aligned_err            : OUT STD_LOGIC;
    stat_rx_bad_code               : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_bad_fcs                : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_bad_preamble           : OUT STD_LOGIC;
    stat_rx_bad_sfd                : OUT STD_LOGIC;
    stat_rx_bip_err_0              : OUT STD_LOGIC;
    stat_rx_bip_err_1              : OUT STD_LOGIC;
    stat_rx_bip_err_10             : OUT STD_LOGIC;
    stat_rx_bip_err_11             : OUT STD_LOGIC;
    stat_rx_bip_err_12             : OUT STD_LOGIC;
    stat_rx_bip_err_13             : OUT STD_LOGIC;
    stat_rx_bip_err_14             : OUT STD_LOGIC;
    stat_rx_bip_err_15             : OUT STD_LOGIC;
    stat_rx_bip_err_16             : OUT STD_LOGIC;
    stat_rx_bip_err_17             : OUT STD_LOGIC;
    stat_rx_bip_err_18             : OUT STD_LOGIC;
    stat_rx_bip_err_19             : OUT STD_LOGIC;
    stat_rx_bip_err_2              : OUT STD_LOGIC;
    stat_rx_bip_err_3              : OUT STD_LOGIC;
    stat_rx_bip_err_4              : OUT STD_LOGIC;
    stat_rx_bip_err_5              : OUT STD_LOGIC;
    stat_rx_bip_err_6              : OUT STD_LOGIC;
    stat_rx_bip_err_7              : OUT STD_LOGIC;
    stat_rx_bip_err_8              : OUT STD_LOGIC;
    stat_rx_bip_err_9              : OUT STD_LOGIC;
    stat_rx_block_lock             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_broadcast              : OUT STD_LOGIC;
    stat_rx_fragment               : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_framing_err_0          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_1          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_10         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_11         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_12         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_13         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_14         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_15         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_16         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_17         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_18         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_19         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_2          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_3          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_4          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_5          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_6          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_7          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_8          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_9          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_valid_0    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_1    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_10   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_11   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_12   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_13   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_14   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_15   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_16   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_17   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_18   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_19   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_2    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_3    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_4    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_5    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_6    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_7    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_8    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_9    : OUT STD_LOGIC;
    stat_rx_got_signal_os          : OUT STD_LOGIC;
    stat_rx_hi_ber                 : OUT STD_LOGIC;
    stat_rx_inrangeerr             : OUT STD_LOGIC;
    stat_rx_internal_local_fault   : OUT STD_LOGIC;
    stat_rx_jabber                 : OUT STD_LOGIC;
    stat_rx_local_fault            : OUT STD_LOGIC;
    stat_rx_mf_err                 : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_mf_len_err             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_mf_repeat_err          : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_misaligned             : OUT STD_LOGIC;
    stat_rx_multicast              : OUT STD_LOGIC;
    stat_rx_oversize               : OUT STD_LOGIC;
    stat_rx_packet_1024_1518_bytes : OUT STD_LOGIC;
    stat_rx_packet_128_255_bytes   : OUT STD_LOGIC;
    stat_rx_packet_1519_1522_bytes : OUT STD_LOGIC;
    stat_rx_packet_1523_1548_bytes : OUT STD_LOGIC;
    stat_rx_packet_1549_2047_bytes : OUT STD_LOGIC;
    stat_rx_packet_2048_4095_bytes : OUT STD_LOGIC;
    stat_rx_packet_256_511_bytes   : OUT STD_LOGIC;
    stat_rx_packet_4096_8191_bytes : OUT STD_LOGIC;
    stat_rx_packet_512_1023_bytes  : OUT STD_LOGIC;
    stat_rx_packet_64_bytes        : OUT STD_LOGIC;
    stat_rx_packet_65_127_bytes    : OUT STD_LOGIC;
    stat_rx_packet_8192_9215_bytes : OUT STD_LOGIC;
    stat_rx_packet_bad_fcs         : OUT STD_LOGIC;
    stat_rx_packet_large           : OUT STD_LOGIC;
    stat_rx_packet_small           : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ctl_rx_enable                  : IN STD_LOGIC;
    ctl_rx_force_resync            : IN STD_LOGIC;
    ctl_rx_test_pattern            : IN STD_LOGIC;
    rx_clk                         : IN STD_LOGIC;
    stat_rx_received_local_fault   : OUT STD_LOGIC;
    stat_rx_remote_fault           : OUT STD_LOGIC;
    stat_rx_status                 : OUT STD_LOGIC;
    stat_rx_stomped_fcs            : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_synced                 : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_synced_err             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_test_pattern_mismatch  : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_toolong                : OUT STD_LOGIC;
    stat_rx_total_bytes            : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    stat_rx_total_good_bytes       : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_rx_total_good_packets     : OUT STD_LOGIC;
    stat_rx_total_packets          : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_truncated              : OUT STD_LOGIC;
    stat_rx_undersize              : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_unicast                : OUT STD_LOGIC;
    stat_rx_vlan                   : OUT STD_LOGIC;
    stat_rx_pcsl_demuxed           : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_pcsl_number_0          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_1          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_10         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_11         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_12         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_13         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_14         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_15         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_16         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_17         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_18         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_19         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_2          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_3          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_4          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_5          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_6          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_7          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_8          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_9          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_tx_bad_fcs                : OUT STD_LOGIC;
    stat_tx_broadcast              : OUT STD_LOGIC;
    stat_tx_frame_error            : OUT STD_LOGIC;
    stat_tx_local_fault            : OUT STD_LOGIC;
    stat_tx_multicast              : OUT STD_LOGIC;
    stat_tx_packet_1024_1518_bytes : OUT STD_LOGIC;
    stat_tx_packet_128_255_bytes   : OUT STD_LOGIC;
    stat_tx_packet_1519_1522_bytes : OUT STD_LOGIC;
    stat_tx_packet_1523_1548_bytes : OUT STD_LOGIC;
    stat_tx_packet_1549_2047_bytes : OUT STD_LOGIC;
    stat_tx_packet_2048_4095_bytes : OUT STD_LOGIC;
    stat_tx_packet_256_511_bytes   : OUT STD_LOGIC;
    stat_tx_packet_4096_8191_bytes : OUT STD_LOGIC;
    stat_tx_packet_512_1023_bytes  : OUT STD_LOGIC;
    stat_tx_packet_64_bytes        : OUT STD_LOGIC;
    stat_tx_packet_65_127_bytes    : OUT STD_LOGIC;
    stat_tx_packet_8192_9215_bytes : OUT STD_LOGIC;
    stat_tx_packet_large           : OUT STD_LOGIC;
    stat_tx_packet_small           : OUT STD_LOGIC;
    stat_tx_total_bytes            : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    stat_tx_total_good_bytes       : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_tx_total_good_packets     : OUT STD_LOGIC;
    stat_tx_total_packets          : OUT STD_LOGIC;
    stat_tx_unicast                : OUT STD_LOGIC;
    stat_tx_vlan                   : OUT STD_LOGIC;
    ctl_tx_enable                  : IN STD_LOGIC;
    ctl_tx_send_idle               : IN STD_LOGIC;
    ctl_tx_send_rfi                : IN STD_LOGIC;
    ctl_tx_send_lfi                : IN STD_LOGIC;
    ctl_tx_test_pattern            : IN STD_LOGIC;
    tx_clk                         : IN STD_LOGIC;
    tx_ovfout                      : OUT STD_LOGIC;
    tx_rdyout                      : OUT STD_LOGIC;
    tx_unfout                      : OUT STD_LOGIC;
    tx_datain0                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain1                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain2                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain3                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_enain0                      : IN STD_LOGIC;
    tx_enain1                      : IN STD_LOGIC;
    tx_enain2                      : IN STD_LOGIC;
    tx_enain3                      : IN STD_LOGIC;
    tx_eopin0                      : IN STD_LOGIC;
    tx_eopin1                      : IN STD_LOGIC;
    tx_eopin2                      : IN STD_LOGIC;
    tx_eopin3                      : IN STD_LOGIC;
    tx_errin0                      : IN STD_LOGIC;
    tx_errin1                      : IN STD_LOGIC;
    tx_errin2                      : IN STD_LOGIC;
    tx_errin3                      : IN STD_LOGIC;
    tx_mtyin0                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin1                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin2                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin3                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_sopin0                      : IN STD_LOGIC;
    tx_sopin1                      : IN STD_LOGIC;
    tx_sopin2                      : IN STD_LOGIC;
    tx_sopin3                      : IN STD_LOGIC;
    tx_preamblein                  : IN STD_LOGIC_VECTOR(55 DOWNTO 0);
    tx_reset_done                  : IN STD_LOGIC;
    rx_reset_done                  : IN STD_LOGIC;
    rx_serdes_reset_done           : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    rx_serdes_clk_in               : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    drp_clk                        : IN STD_LOGIC;
    drp_addr                       : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    drp_di                         : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    drp_en                         : IN STD_LOGIC;
    drp_do                         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    drp_rdy                        : OUT STD_LOGIC;
    drp_we                         : IN STD_LOGIC
  );
END COMPONENT;



COMPONENT cmac100gb_p02
  PORT (
    txdata_in                      : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    txctrl0_in                     : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl1_in                     : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxdata_out                     : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    rxctrl0_out                    : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxctrl1_out                    : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
--    ctl_tx_rsfec_enable            : IN STD_LOGIC;
--    ctl_rx_rsfec_enable            : IN STD_LOGIC;
--    ctl_rsfec_ieee_error_indication_mode : IN STD_LOGIC;
--    ctl_rx_rsfec_enable_correction : IN STD_LOGIC;
--    ctl_rx_rsfec_enable_indication : IN STD_LOGIC;
--    stat_rx_rsfec_am_lock0         : OUT STD_LOGIC;
--    stat_rx_rsfec_am_lock1         : OUT STD_LOGIC;
--    stat_rx_rsfec_am_lock2         : OUT STD_LOGIC;
--    stat_rx_rsfec_am_lock3         : OUT STD_LOGIC;
--    stat_rx_rsfec_corrected_cw_inc : OUT STD_LOGIC;
--    stat_rx_rsfec_cw_inc           : OUT STD_LOGIC;
--    stat_rx_rsfec_err_count0_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_err_count1_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_err_count2_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_err_count3_inc   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--    stat_rx_rsfec_hi_ser           : OUT STD_LOGIC;
--    stat_rx_rsfec_lane_alignment_status : OUT STD_LOGIC;
--    stat_rx_rsfec_lane_fill_0      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_fill_1      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_fill_2      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_fill_3      : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--    stat_rx_rsfec_lane_mapping     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--    stat_rx_rsfec_uncorrected_cw_inc : OUT STD_LOGIC;
    rx_dataout0                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout1                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout2                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_dataout3                    : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    rx_enaout0                     : OUT STD_LOGIC;
    rx_enaout1                     : OUT STD_LOGIC;
    rx_enaout2                     : OUT STD_LOGIC;
    rx_enaout3                     : OUT STD_LOGIC;
    rx_eopout0                     : OUT STD_LOGIC;
    rx_eopout1                     : OUT STD_LOGIC;
    rx_eopout2                     : OUT STD_LOGIC;
    rx_eopout3                     : OUT STD_LOGIC;
    rx_errout0                     : OUT STD_LOGIC;
    rx_errout1                     : OUT STD_LOGIC;
    rx_errout2                     : OUT STD_LOGIC;
    rx_errout3                     : OUT STD_LOGIC;
    rx_mtyout0                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout1                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout2                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_mtyout3                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rx_sopout0                     : OUT STD_LOGIC;
    rx_sopout1                     : OUT STD_LOGIC;
    rx_sopout2                     : OUT STD_LOGIC;
    rx_sopout3                     : OUT STD_LOGIC;
    rx_otn_bip8_0                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_1                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_2                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_3                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_bip8_4                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rx_otn_data_0                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_1                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_2                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_3                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_data_4                  : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    rx_otn_ena                     : OUT STD_LOGIC;
    rx_otn_lane0                   : OUT STD_LOGIC;
    rx_otn_vlmarker                : OUT STD_LOGIC;
    rx_preambleout                 : OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
    stat_rx_aligned                : OUT STD_LOGIC;
    stat_rx_aligned_err            : OUT STD_LOGIC;
    stat_rx_bad_code               : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_bad_fcs                : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_bad_preamble           : OUT STD_LOGIC;
    stat_rx_bad_sfd                : OUT STD_LOGIC;
    stat_rx_bip_err_0              : OUT STD_LOGIC;
    stat_rx_bip_err_1              : OUT STD_LOGIC;
    stat_rx_bip_err_10             : OUT STD_LOGIC;
    stat_rx_bip_err_11             : OUT STD_LOGIC;
    stat_rx_bip_err_12             : OUT STD_LOGIC;
    stat_rx_bip_err_13             : OUT STD_LOGIC;
    stat_rx_bip_err_14             : OUT STD_LOGIC;
    stat_rx_bip_err_15             : OUT STD_LOGIC;
    stat_rx_bip_err_16             : OUT STD_LOGIC;
    stat_rx_bip_err_17             : OUT STD_LOGIC;
    stat_rx_bip_err_18             : OUT STD_LOGIC;
    stat_rx_bip_err_19             : OUT STD_LOGIC;
    stat_rx_bip_err_2              : OUT STD_LOGIC;
    stat_rx_bip_err_3              : OUT STD_LOGIC;
    stat_rx_bip_err_4              : OUT STD_LOGIC;
    stat_rx_bip_err_5              : OUT STD_LOGIC;
    stat_rx_bip_err_6              : OUT STD_LOGIC;
    stat_rx_bip_err_7              : OUT STD_LOGIC;
    stat_rx_bip_err_8              : OUT STD_LOGIC;
    stat_rx_bip_err_9              : OUT STD_LOGIC;
    stat_rx_block_lock             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_broadcast              : OUT STD_LOGIC;
    stat_rx_fragment               : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_framing_err_0          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_1          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_10         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_11         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_12         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_13         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_14         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_15         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_16         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_17         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_18         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_19         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_2          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_3          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_4          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_5          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_6          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_7          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_8          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_9          : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    stat_rx_framing_err_valid_0    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_1    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_10   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_11   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_12   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_13   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_14   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_15   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_16   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_17   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_18   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_19   : OUT STD_LOGIC;
    stat_rx_framing_err_valid_2    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_3    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_4    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_5    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_6    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_7    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_8    : OUT STD_LOGIC;
    stat_rx_framing_err_valid_9    : OUT STD_LOGIC;
    stat_rx_got_signal_os          : OUT STD_LOGIC;
    stat_rx_hi_ber                 : OUT STD_LOGIC;
    stat_rx_inrangeerr             : OUT STD_LOGIC;
    stat_rx_internal_local_fault   : OUT STD_LOGIC;
    stat_rx_jabber                 : OUT STD_LOGIC;
    stat_rx_local_fault            : OUT STD_LOGIC;
    stat_rx_mf_err                 : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_mf_len_err             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_mf_repeat_err          : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_misaligned             : OUT STD_LOGIC;
    stat_rx_multicast              : OUT STD_LOGIC;
    stat_rx_oversize               : OUT STD_LOGIC;
    stat_rx_packet_1024_1518_bytes : OUT STD_LOGIC;
    stat_rx_packet_128_255_bytes   : OUT STD_LOGIC;
    stat_rx_packet_1519_1522_bytes : OUT STD_LOGIC;
    stat_rx_packet_1523_1548_bytes : OUT STD_LOGIC;
    stat_rx_packet_1549_2047_bytes : OUT STD_LOGIC;
    stat_rx_packet_2048_4095_bytes : OUT STD_LOGIC;
    stat_rx_packet_256_511_bytes   : OUT STD_LOGIC;
    stat_rx_packet_4096_8191_bytes : OUT STD_LOGIC;
    stat_rx_packet_512_1023_bytes  : OUT STD_LOGIC;
    stat_rx_packet_64_bytes        : OUT STD_LOGIC;
    stat_rx_packet_65_127_bytes    : OUT STD_LOGIC;
    stat_rx_packet_8192_9215_bytes : OUT STD_LOGIC;
    stat_rx_packet_bad_fcs         : OUT STD_LOGIC;
    stat_rx_packet_large           : OUT STD_LOGIC;
    stat_rx_packet_small           : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ctl_rx_enable                  : IN STD_LOGIC;
    ctl_rx_force_resync            : IN STD_LOGIC;
    ctl_rx_test_pattern            : IN STD_LOGIC;
    rx_clk                         : IN STD_LOGIC;
    stat_rx_received_local_fault   : OUT STD_LOGIC;
    stat_rx_remote_fault           : OUT STD_LOGIC;
    stat_rx_status                 : OUT STD_LOGIC;
    stat_rx_stomped_fcs            : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_synced                 : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_synced_err             : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_test_pattern_mismatch  : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_toolong                : OUT STD_LOGIC;
    stat_rx_total_bytes            : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    stat_rx_total_good_bytes       : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_rx_total_good_packets     : OUT STD_LOGIC;
    stat_rx_total_packets          : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_truncated              : OUT STD_LOGIC;
    stat_rx_undersize              : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    stat_rx_unicast                : OUT STD_LOGIC;
    stat_rx_vlan                   : OUT STD_LOGIC;
    stat_rx_pcsl_demuxed           : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    stat_rx_pcsl_number_0          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_1          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_10         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_11         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_12         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_13         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_14         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_15         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_16         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_17         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_18         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_19         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_2          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_3          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_4          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_5          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_6          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_7          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_8          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_rx_pcsl_number_9          : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    stat_tx_bad_fcs                : OUT STD_LOGIC;
    stat_tx_broadcast              : OUT STD_LOGIC;
    stat_tx_frame_error            : OUT STD_LOGIC;
    stat_tx_local_fault            : OUT STD_LOGIC;
    stat_tx_multicast              : OUT STD_LOGIC;
    stat_tx_packet_1024_1518_bytes : OUT STD_LOGIC;
    stat_tx_packet_128_255_bytes   : OUT STD_LOGIC;
    stat_tx_packet_1519_1522_bytes : OUT STD_LOGIC;
    stat_tx_packet_1523_1548_bytes : OUT STD_LOGIC;
    stat_tx_packet_1549_2047_bytes : OUT STD_LOGIC;
    stat_tx_packet_2048_4095_bytes : OUT STD_LOGIC;
    stat_tx_packet_256_511_bytes   : OUT STD_LOGIC;
    stat_tx_packet_4096_8191_bytes : OUT STD_LOGIC;
    stat_tx_packet_512_1023_bytes  : OUT STD_LOGIC;
    stat_tx_packet_64_bytes        : OUT STD_LOGIC;
    stat_tx_packet_65_127_bytes    : OUT STD_LOGIC;
    stat_tx_packet_8192_9215_bytes : OUT STD_LOGIC;
    stat_tx_packet_large           : OUT STD_LOGIC;
    stat_tx_packet_small           : OUT STD_LOGIC;
    stat_tx_total_bytes            : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    stat_tx_total_good_bytes       : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    stat_tx_total_good_packets     : OUT STD_LOGIC;
    stat_tx_total_packets          : OUT STD_LOGIC;
    stat_tx_unicast                : OUT STD_LOGIC;
    stat_tx_vlan                   : OUT STD_LOGIC;
    ctl_tx_enable                  : IN STD_LOGIC;
    ctl_tx_send_idle               : IN STD_LOGIC;
    ctl_tx_send_rfi                : IN STD_LOGIC;
    ctl_tx_send_lfi                : IN STD_LOGIC;
    ctl_tx_test_pattern            : IN STD_LOGIC;
    tx_clk                         : IN STD_LOGIC;
    tx_ovfout                      : OUT STD_LOGIC;
    tx_rdyout                      : OUT STD_LOGIC;
    tx_unfout                      : OUT STD_LOGIC;
    tx_datain0                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain1                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain2                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_datain3                     : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    tx_enain0                      : IN STD_LOGIC;
    tx_enain1                      : IN STD_LOGIC;
    tx_enain2                      : IN STD_LOGIC;
    tx_enain3                      : IN STD_LOGIC;
    tx_eopin0                      : IN STD_LOGIC;
    tx_eopin1                      : IN STD_LOGIC;
    tx_eopin2                      : IN STD_LOGIC;
    tx_eopin3                      : IN STD_LOGIC;
    tx_errin0                      : IN STD_LOGIC;
    tx_errin1                      : IN STD_LOGIC;
    tx_errin2                      : IN STD_LOGIC;
    tx_errin3                      : IN STD_LOGIC;
    tx_mtyin0                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin1                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin2                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_mtyin3                      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx_sopin0                      : IN STD_LOGIC;
    tx_sopin1                      : IN STD_LOGIC;
    tx_sopin2                      : IN STD_LOGIC;
    tx_sopin3                      : IN STD_LOGIC;
    tx_preamblein                  : IN STD_LOGIC_VECTOR(55 DOWNTO 0);
    tx_reset_done                  : IN STD_LOGIC;
    rx_reset_done                  : IN STD_LOGIC;
    rx_serdes_reset_done           : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    rx_serdes_clk_in               : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    drp_clk                        : IN STD_LOGIC;
    drp_addr                       : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    drp_di                         : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    drp_en                         : IN STD_LOGIC;
    drp_do                         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    drp_rdy                        : OUT STD_LOGIC;
    drp_we                         : IN STD_LOGIC
  );
END COMPONENT;

signal ctl_gt_loopback                  : std_logic;
signal txdata_in                      	: STD_LOGIC_VECTOR(511 DOWNTO 0);
signal txctrl0_in                     	: STD_LOGIC_VECTOR(63 DOWNTO 0);
signal txctrl1_in                     	: STD_LOGIC_VECTOR(63 DOWNTO 0);
signal rxdata_out                     	: STD_LOGIC_VECTOR(511 DOWNTO 0);
signal rxctrl0_out                    	: STD_LOGIC_VECTOR(63 DOWNTO 0);
signal rxctrl1_out                    	: STD_LOGIC_VECTOR(63 DOWNTO 0);

signal tx_reset_done					: std_logic;
signal rx_reset_done					: std_logic;
signal rx_serdes_reset_done				: std_logic_vector(9 downto 0);
signal rx_serdes_clk					: std_logic_vector(9 downto 0);

component MAC100gb_gt_wrapper is
    generic (
        QSFP_port_num              	: std_logic_vector(7 downto 0)
    );
    port (
        gt_rxp_in                 	: in std_logic_vector(3 downto 0);
        gt_rxn_in                	: in std_logic_vector(3 downto 0);
        gt_txn_out               	: out std_logic_vector(3 downto 0);
        gt_txp_out               	: out std_logic_vector(3 downto 0);

        ctrl_gt_loopback_in        	: in std_logic_vector(2 downto 0);
        gt_rxrecclkout            	: out std_logic_vector(3 downto 0);
        gt_powergoodout           	: out std_logic_vector(3 downto 0);

        -- GT Transceiver debug interface ports
        gt_eyescanreset           	: in std_logic_vector(3 downto 0);
        gt_eyescantrigger         	: in std_logic_vector(3 downto 0);
        gt_rxcdrhold                : in std_logic_vector(3 downto 0);
        rx_polarity             	: in std_logic;
        gt_rxrate                   : in std_logic_vector(11 downto 0);
        tx_diff_ctrl             	: in std_logic_vector(19 downto 0);
        tx_polarity             	: in std_logic;
        gt_txinhibit                : in std_logic_vector(3 downto 0);
        gt_txpippmen                : in std_logic_vector(3 downto 0);
        gt_txpippmsel               : in std_logic_vector(3 downto 0);
        txpostcursor           		: in std_logic_vector(19 downto 0);
        gt_txprbsforceerr_sync      : in std_logic;
        txprecursor            		: in std_logic_vector(19 downto 0);
        gt_eyescandataerror         : out std_logic_vector(3 downto 0);
        gt_txbufstatus             	: out std_logic_vector(7 downto 0);

        gt_rxdfelpmreset            : in std_logic_vector(3 downto 0);
        gt_rxlpmen					: in std_logic_vector(3 downto 0);
        gt_rxprbscntreset_async    	: in std_logic;
        gt_rxprbserr_out           	: out std_logic_vector(3 downto 0);
        gt_rxprbssel_async          : in std_logic_vector(3 downto 0);
        gt_rxresetdone              : out std_logic_vector(3 downto 0);
        gt_txprbssel_async          : in std_logic_vector(3 downto 0);
        gt_txresetdone              : out std_logic_vector(3 downto 0);
        gt_rxbufstatus            	: out std_logic_vector(11 downto 0);
        gtwiz_reset_tx_datapath   	: in std_logic;
        gtwiz_reset_rx_datapath     : in std_logic;
        ctl_gt_loopback             : in std_logic;

        gt_drprstp                  : in std_logic;
        gt_drpclk                 	: in std_logic;
        gt_drpaddr               	: in std_logic_vector(9 downto 0);
        gt_drpen                 	: in std_logic;
        gt_drpdi                 	: in std_logic_vector(63 downto 0);
        gt_drpdo                 	: out std_logic_vector(63 downto 0);
        gt_drprdy                	: out std_logic_vector(3 downto 0);
        gt_drpwe                 	: in std_logic;

        gt_txusrclk2              	: out std_logic;
        gt_rxusrclk2 	            : out std_logic;
        gt_reset_tx_done_out     	: out std_logic;
        gt_reset_rx_done_out     	: out std_logic;
        rx_serdes_clk             	: out std_logic_vector(9 downto 0);

        qpll0clk_in               	: in std_logic_vector(3 downto 0);
        qpll0refclk_in            	: in std_logic_vector(3 downto 0);
        qpll1clk_in               	: in std_logic_vector(3 downto 0);
        qpll1refclk_in            	: in std_logic_vector(3 downto 0);
        gtwiz_reset_qpll0lock_in  	: in std_logic;
        gtwiz_reset_qpll0reset_out	: out std_logic;

        txdata_in                  	: in std_logic_vector(511 downto 0);
        txctrl0_in                 	: in std_logic_vector(63 downto 0);
        txctrl1_in                 	: in std_logic_vector(63 downto 0);

        rxdata_out                 	: out std_logic_vector(511 downto 0);
        rxctrl0_out                	: out std_logic_vector(63 downto 0);
        rxctrl1_out                	: out std_logic_vector(63 downto 0);

        sys_reset                 	: in std_logic;
        --usr_clk                 	: in std_logic;
        init_clk					: in std_logic;

        gt_rxprbslocked_out        	: out std_logic_vector(3 downto 0) ;

        txpcsreset_in				: in std_logic;
        txoutclksel_in              : in std_logic_vector(2 downto 0)
    );
end component;

signal qpll0clk_in               								: std_logic_vector(3 downto 0);
signal qpll0clk 	               								: std_logic;
signal qpll0refclk_in            								: std_logic_vector(3 downto 0);
signal qpll0refclk	            								: std_logic;
signal qpll1clk_in               								: std_logic_vector(3 downto 0);
signal qpll1refclk_in            								: std_logic_vector(3 downto 0);
signal gtwiz_qpll0lock   										: std_logic;
signal gtwiz_qpll0reset 										: std_logic;

signal gt_rxrecclkout            								: std_logic_vector(3 downto 0);
-- signal gt_ref_clk_out            								: std_logic;

signal init_done_out                                            : std_logic;
signal SM_link_out                                              : std_logic;

signal gt_loopback_in	                                        : STD_LOGIC_VECTOR(11 DOWNTO 0);
signal gt_rxlpmen												: std_logic_vector(3 downto 0);
signal gt_rxpcsreset_async 										: STD_LOGIC;
signal gt_txpcsreset_async 										: STD_LOGIC;
signal gt_txbufstatus											: std_logic_vector(7 downto 0);
signal gt_txresetdone							 	            : std_logic_vector(3 downto 0);

signal gtwiz_reset_qpll0lock  									: std_logic_vector(0 downto 0);
signal gtwiz_reset_qpll0reset									: std_logic_vector(0 downto 0);
signal gt_powergoodout											: std_logic_vector(3 downto 0);
signal powergood												: std_logic;

component MAC100GB_v1_shared_logic_wrapper is
 Port (
	gt_ref_clk_p           	: in std_logic;
	gt_ref_clk_n           	: in std_logic;
	gt_txusrclk2           	: in std_logic;
	gt_rxusrclk2           	: in std_logic;
	gt_ref_clk_out         	: out std_logic;
	rx_clk                 	: in std_logic;
	gt_powergood           	: in std_logic;
	gt_tx_reset_in         	: in std_logic;
	gt_rx_reset_in         	: in std_logic;
	core_drp_reset         	: in std_logic;
	core_tx_reset          	: in std_logic;
	tx_reset_out           	: out std_logic;
	core_rx_reset          	: in std_logic;
	rx_reset_out           	: out std_logic;
	rx_serdes_reset_out    	: out std_logic_vector(9 downto 0);
	qpll0reset             	: in std_logic_vector(0 downto 0);
	qpll0lock              	: out std_logic_vector(0 downto 0);
	qpll0outclk            	: out std_logic_vector(0 downto 0);
	qpll0outrefclk         	: out std_logic_vector(0 downto 0);
	drpclk_common_in       	: in std_logic;
	common0_drpaddr        	: in std_logic_vector(15 downto 0);
	common0_drpdi          	: in std_logic_vector(15 downto 0);
	common0_drpwe          	: in std_logic;
	common0_drpen          	: in std_logic;
	common0_drprdy         	: out std_logic;
	common0_drpdo          	: out std_logic_vector(15 downto 0);
	qpll1reset             	: in std_logic_vector(0 downto 0);
	qpll1lock              	: out std_logic_vector(0 downto 0);
	qpll1outclk            	: out std_logic_vector(0 downto 0);
	qpll1outrefclk         	: out std_logic_vector(0 downto 0);
	usr_tx_reset           	: out std_logic;
	usr_rx_reset			: out std_logic
	);
end component;

signal usr_tx_reset           	: std_logic;
signal usr_rx_reset				: std_logic;

component statistic_15bcounter is
    Port (
    	reset : in STD_LOGIC;
		clock : in STD_LOGIC;
  		ena : in STD_LOGIC;
 		values : out STD_LOGIC_VECTOR (15 downto 0)
 		);
end component;

component clk_wrapper is
	port
	(
		gt_ref_CLKi_p		: in std_logic;
		gt_ref_CLKi_n		: in std_logic;
		gt_ref_CLKo			: out std_logic
	 );
end component;

component resync_v4 is
port (
	aresetn				: in std_logic;
	clocki				: in std_logic;
	input				: in std_logic;
	clocko				: in std_logic;
	output				: out std_logic
	);
end component;

component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic;
	Resetn_sync			: out std_logic;
	Resetp_sync			: out std_logic
	);
end component;

component resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic;
	Resetp_sync			: out std_logic
	);
end component;

COMPONENT resync_sig_gen is
port (
	clocko				: in std_logic;
	input				: in std_logic;
	output				: out std_logic
	);
end COMPONENT;

-- global signal Reset Clock
signal gt_txusrclk2                                             : std_logic;
signal gt_rxusrclk2                                             : std_logic;

signal rx_clk                                                   : std_logic;
signal tx_clk                                                   : std_logic;
signal Serdes_rst_T_Clk                                         : std_logic;
signal counter_reset                                          	: std_logic;
signal counter_reset_resync                                    	: std_logic;

signal sys_reset                                                : std_logic;
signal rx_lbus_cell                    							: rx_user_ift;

-- statistics signals
signal stat_rx_local_fault                                      : std_logic;
signal stat_rx_misaligned                                       : std_logic;
signal stat_rx_status                                           : std_logic;
signal stat_rx_synced                                           : std_logic_vector(19 downto 0);
signal stat_rx_synced_err                                       : std_logic_vector(19 downto 0);
signal ctl_tx_enable											: std_logic;
signal ctl_rx_enable                  							: STD_LOGIC;
signal ctl_rx_force_resync            							: STD_LOGIC;
signal ctl_rx_test_pattern           							: STD_LOGIC;

signal counter_reset_sync_txclk                                 : std_logic;

type stat_rx_bip_err_type	is array (0 to 3) of std_logic;
signal stat_rx_bip_err                                          : stat_rx_bip_err_type;
type stat_rx_bip_err_cnt_type	is array (0 to 3) of std_logic_vector(63 downto 0);
signal stat_rx_bip_err_cnt                                      : stat_rx_bip_err_cnt_type;

signal stat_rx_total_bytes				                        : STD_LOGIC_vector(6 downto 0);
signal stat_rx_total_bytes_cnt		                            : std_logic_vector(31 downto 0);

signal stat_rx_packet					                        : std_logic_vector(11 downto 0);
signal stat_rx_packet_size				                        : std_logic_vector(11 downto 0);
signal stat_rx_packet_cnt				                        : std_logic_vector(31 downto 12);
signal stat_rx_packet_small			                            : std_logic_vector(2 downto 0);
signal stat_rx_packet_small_cnt		                            : std_logic_vector(31 downto 0);

signal stat_rx_aligned 						                    : STD_LOGIC;
signal stat_rx_aligned_sync_TX				                    : STD_LOGIC;
signal stat_rx_aligned_not					                    : STD_LOGIC;
signal stat_rx_aligned_err 				                        : STD_LOGIC;
signal stat_rx_bad_code 					                    : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_bad_fcs 						                    : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_bad_preamble 				                    : STD_LOGIC;
signal stat_rx_bad_sfd 						                    : STD_LOGIC;
signal stat_rx_block_lock 					                    : STD_LOGIC_VECTOR(19 DOWNTO 0);
signal stat_rx_fragment 					                    : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal stat_rx_internal_local_fault                             : std_logic;
signal stat_rx_received_local_fault                             : std_logic;
signal inc_tx_total_good_packets			                    : std_logic_vector(31 downto 0);
signal inc_rx_total_good_packets_ena	                        : std_logic;
signal tx_ena_lbus				                                : std_logic;
signal tx_ena_resync				                            : std_logic;
signal tx_ena_lbus_resync		                                : std_logic_vector(3 downto 0);
signal tx_ovfout 			                                    : STD_LOGIC;
signal tx_ovfout_mem		                                    : STD_LOGIC;
signal tx_rdyout 			                                    : STD_LOGIC;
signal tx_unfout 			                                    : STD_LOGIC;
signal tx_unfout_mem		                                    : STD_LOGIC;
signal stat_tx_local_fault                                      : std_logic;
signal stat_tx_bad_fcs                							: STD_LOGIC;
signal stat_tx_broadcast              							: STD_LOGIC;
signal stat_tx_frame_error            							: STD_LOGIC;
signal stat_tx_local_fault_cnt                                  : std_logic_vector(15 downto 0);
signal stat_tx_bad_fcs_cnt                						: STD_LOGIC_vector(15 downto 0);
signal stat_tx_broadcast_cnt              						: STD_LOGIC_vector(15 downto 0);
signal stat_tx_frame_error_cnt            						: STD_LOGIC_vector(15 downto 0);

signal rx_frame_l0_err											: std_logic_vector(1 downto 0);
signal rx_frame_l1_err											: std_logic_vector(1 downto 0);
signal rx_frame_l2_err											: std_logic_vector(1 downto 0);
signal rx_frame_l3_err											: std_logic_vector(1 downto 0);

signal rx_frame_l0_err_val										: std_logic;
signal rx_frame_l1_err_val										: std_logic;
signal rx_frame_l2_err_val										: std_logic;
signal rx_frame_l3_err_val										: std_logic;

signal rx_frame_l0_err_cnt										: std_logic_vector(63 downto 0);
signal rx_frame_l1_err_cnt										: std_logic_vector(63 downto 0);
signal rx_frame_l2_err_cnt										: std_logic_vector(63 downto 0);
signal rx_frame_l3_err_cnt										: std_logic_vector(63 downto 0);

-- control DRP bus accesses
signal DRP_clock											    : std_logic;
signal DRP_rst												    : std_logic;
signal DRP_rstp												    : std_logic;
signal drp_addr 							                    : STD_LOGIC_VECTOR(9 DOWNTO 0) := "0000000000";
signal drp_di 								                    : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";
signal drp_en 								                    : STD_LOGIC_vector(1 downto 0) :="00";
signal drp_en_cell							                    : STD_LOGIC  := '0';
signal drp_do 								                    : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";
signal drp_data							                        : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";
signal drp_rdy 							                        : STD_LOGIC  := '0';
signal drp_we 								                    : STD_LOGIC_vector(1 downto 0);
signal drp_done							                        : std_logic  := '0';
signal drp_reset							                    : std_logic  := '0';
signal gt_reset                                                 : std_logic  := '0';
signal data_in_rg                                               : std_logic_vector(63 downto 0);


signal gtx_ch_drp_addr 							                : STD_LOGIC_VECTOR(9 DOWNTO 0):= "0000000000";
signal gtx_ch_drp_di 								            : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"0000";
signal gtx_ch_drp_do 								            : STD_LOGIC_VECTOR(63 DOWNTO 0) := x"0000000000000000";
signal gtx_ch_drp_we 								            : STD_LOGIC   := '0';
signal gtx_ch_drp_we_sync							            : STD_LOGIC   := '0';
signal gtx_ch_drp_rd								            : STD_LOGIC   := '0';
signal gtx_ch_drp_rd_sync							            : STD_LOGIC   := '0';
signal gtx_ch_drp_en								            : STD_LOGIC_vector(3 downto 0) := x"0" ;
signal gtx_ch_drp_en_sync								        : STD_LOGIC_vector(3 downto 0) := x"0" ;
signal gtx_ch_drp_done								            : STD_LOGIC_vector(3 downto 0) := x"0" ;

signal gt_ch_drp_en 								            : STD_LOGIC   := '0';
signal gt_ch_drp_do  								            : STD_LOGIC_VECTOR(63 DOWNTO 0) := x"0000000000000000";
signal gt_ch_drp_rdy 							                : STD_LOGIC_VECTOR(3 DOWNTO 0)   := "0000";
signal gt_ch_drp_rdy_sync						                : STD_LOGIC_VECTOR(3 DOWNTO 0)   := "0000";
signal gt_ch_drp_we 								            : STD_LOGIC   := '0';

 -- Signals for monitoring   next implemetnations
signal gt_reset_rx_done                                      :  std_logic;
signal gt_reset_tx_done                                      :  std_logic;
signal gt_rxbufstatus                                           :  std_logic_vector(11 downto 0);

-- sognals for Pause Frame control
signal stat_rx_pause                  							: STD_LOGIC;
signal stat_rx_pause_quanta0          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta1          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta2          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta3          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta4          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta5          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta6          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta7          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_quanta8          							: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal stat_rx_pause_req              							: STD_LOGIC_VECTOR(8 DOWNTO 0);
signal stat_rx_pause_valid            							: STD_LOGIC_VECTOR(8 DOWNTO 0);
signal stat_rx_user_pause             							: STD_LOGIC;

signal ctl_tx_pause_req               							: STD_LOGIC_VECTOR(8 DOWNTO 0);
signal ctl_tx_pause_req_resync         							: STD_LOGIC_VECTOR(8 DOWNTO 0);
signal ctl_tx_resend_pause            							: STD_LOGIC;
signal ctl_tx_resend_pause_resync      							: STD_LOGIC;

signal stat_rx_pause_counter         							: STD_LOGIC_VECTOR(15 DOWNTO 0);

-- signal to set "analog" parameters
signal tx_diff_ctrl											    : std_logic_vector(19 downto 0):= "11000110001100011000";
signal txpostcursor											    : std_logic_vector(19 downto 0):= "10100101001010010100";
signal txprecursor											    : std_logic_vector(19 downto 0):= "00000000000000000000";
signal tx_polarity											    : std_logic:= '0';
signal rx_polarity											    : std_logic:= '0';
signal gt_loopback_in_rg									    : std_logic_vector(2 downto 0);
signal txoutclksel_async									    : std_logic_vector(2 downto 0) := "101";
signal rxoutclksel_async									    : std_logic_vector(2 downto 0) := "010";
signal gt_txprbssel_async									    : std_logic_vector(3 downto 0);
signal gt_rxprbssel_async									    : std_logic_vector(3 downto 0);
signal gt_rxprbserr											    : std_logic_vector(3 downto 0);
signal rxprbslocked_out										    : std_logic_vector(3 downto 0);
signal gt_rxprbscntreset_async									: std_logic ;
signal gt_txprbsforceerr_async									: std_logic ;
signal gt_txprbsforceerr_sync									: std_logic ;

signal rx_crc_error												: std_logic_vector(15 downto 0);
signal measure_MGT_clock    									: std_logic_vector(31 downto 0);

signal ctl_tx_send_rfi											: std_logic;
signal ctl_tx_send_lfi											: std_logic;
signal ctl_tx_send_idle											: std_logic;

signal ctl_tx_send_lfi_reg											: std_logic;
signal ctl_tx_send_idle_reg											: std_logic;

signal MAC_ready_reg                                            : std_logic;

attribute mark_debug : string;
--attribute mark_debug of  tx_reset_done                    : signal is "true";
--attribute mark_debug of  rx_reset_done                    : signal is "true";
--attribute mark_debug of  rx_serdes_reset_done             : signal is "true";

--****************************************************************************************************
--   Code start HERE
--****************************************************************************************************
begin

status_out(0)		<= rx_clk;
status_out(1)		<= tx_clk;

resync_reset_i1:resetn_resync
port map(
	aresetn				=> usr_rst_n,
	clock				=> DRP_clock,
	Resetn_sync			=> DRP_rst
	);
DRP_rstp	<= not(DRP_rst);

-- DRP channel access
DRP_clock		<= init_clk;

process(DRP_clock)
begin
	if rising_edge(DRP_clock) then
		if 		gtx_ch_drp_en_sync(0) = '1' and gt_ch_drp_rdy(0) = '1' then
			gtx_ch_drp_do(15 downto 00)	<= gt_ch_drp_do(15 downto 00);
		end if;
		if 	gtx_ch_drp_en_sync(1) = '1' and gt_ch_drp_rdy(1) = '1' then
			gtx_ch_drp_do(31 downto 16)	<= gt_ch_drp_do(31 downto 16);
		end if;
		if 	gtx_ch_drp_en_sync(2) = '1' and gt_ch_drp_rdy(2) = '1' then
			gtx_ch_drp_do(47 downto 32)	<= gt_ch_drp_do(47 downto 32);
		end if;
		if 	gtx_ch_drp_en_sync(3) = '1' and gt_ch_drp_rdy(3) = '1' then
			gtx_ch_drp_do(63 downto 48)	<= gt_ch_drp_do(63 downto 48);
		end if;

		gtx_ch_drp_en_sync    <= gtx_ch_drp_en;
	end if;
end process;

drp_resync_i11:resync_v4
port map(
	aresetn	=> '1',
	clocki	=> usr_clk,
	input	=> gtx_ch_drp_we,
	clocko	=> DRP_clock,
	output	=> gtx_ch_drp_we_sync
	);


drp_resync_i12:resync_v4
port map(
	aresetn	=> '1',
	clocki	=> usr_clk,
	input	=> gtx_ch_drp_rd,
	clocko	=> DRP_clock,
	output	=> gtx_ch_drp_rd_sync
	);

drp_resync_i20:resync_v4
port map(
	aresetn	=> '1',
	clocki	=> DRP_clock,
	input	=> gt_ch_drp_rdy(0),
	clocko	=> usr_clk,
	output	=> gt_ch_drp_rdy_sync(0)
	);

drp_resync_i21:resync_v4
port map(
	aresetn	=> '1',
	clocki	=> DRP_clock,
	input	=> gt_ch_drp_rdy(1),
	clocko	=> usr_clk,
	output	=> gt_ch_drp_rdy_sync(1)
	);

drp_resync_i22:resync_v4
port map(
	aresetn	=> '1',
	clocki	=> DRP_clock,
	input	=> gt_ch_drp_rdy(2),
	clocko	=> usr_clk,
	output	=> gt_ch_drp_rdy_sync(2)
	);

drp_resync_i23:resync_v4
port map(
	aresetn	=> '1',
	clocki	=> DRP_clock,
	input	=> gt_ch_drp_rdy(3),
	clocko	=> usr_clk,
	output	=> gt_ch_drp_rdy_sync(3)
	);

Pause_resync_i0:resync_v4
port map(
	aresetn	=> '1',
	clocki	=> usr_clk,
	input	=> ctl_tx_resend_pause,
	clocko	=> gt_txusrclk2,
	output	=> ctl_tx_resend_pause_resync
	);

For_i1:for I in 0 to 8 generate
	Pause_resync_i0:resync_v4
	port map(
		aresetn	=> '1',
		clocki	=> usr_clk,
		input	=> ctl_tx_pause_req(I),
		clocko	=> gt_txusrclk2,
		output	=> ctl_tx_pause_req_resync(I)
		);
end generate;



process(usr_rst_n,usr_clk)
begin
	if usr_rst_n = '0' then
		gtx_ch_drp_done	<= (others => '0');
	elsif rising_edge(usr_clk) then
		if (gtx_ch_drp_we = '1' or gtx_ch_drp_rd = '1') and gtx_ch_drp_en(0) = '1' then
			gtx_ch_drp_done(0)	<= '0';
		elsif gt_ch_drp_rdy_sync(0) = '1' then
			gtx_ch_drp_done(0)	<= '1';
		end if;

		if (gtx_ch_drp_we = '1' or gtx_ch_drp_rd = '1') and gtx_ch_drp_en(1) = '1' then
			gtx_ch_drp_done(1)	<= '0';
		elsif gt_ch_drp_rdy_sync(1) = '1' then
			gtx_ch_drp_done(1)	<= '1';
		end if;

		if (gtx_ch_drp_we = '1' or gtx_ch_drp_rd = '1') and gtx_ch_drp_en(2) = '1' then
			gtx_ch_drp_done(2)	<= '0';
		elsif gt_ch_drp_rdy_sync(2) = '1' then
			gtx_ch_drp_done(2)	<= '1';
		end if;

		if (gtx_ch_drp_we = '1' or gtx_ch_drp_rd = '1') and gtx_ch_drp_en(3) = '1' then
			gtx_ch_drp_done(3)	<= '0';
		elsif gt_ch_drp_rdy_sync(3) = '1' then
			gtx_ch_drp_done(3)	<= '1';
		end if;

	end if;
end process;

gt_ch_drp_en	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en_sync(0) = '1' ) or (gtx_ch_drp_rd_sync = '1' and gtx_ch_drp_en_sync(0) = '1' ) else '0';

gt_ch_drp_we	<= '1' when (gtx_ch_drp_we_sync = '1' and gtx_ch_drp_en_sync(0) = '1' ) else '0';

LFI_resync:resync_sig_gen
port map (
	clocko				=> tx_clk,
	input				=> ctl_tx_send_lfi_reg,
	output				=> ctl_tx_send_lfi
	);

stat_rx_aligned_not <= not(stat_rx_aligned);


Idle_resync:resync_sig_gen
port map (
	clocko				=> tx_clk,
	input				=> ctl_tx_send_idle_reg,
	output				=> ctl_tx_send_idle
	);

-- SERDES  MAC100Gb instantiation
gen_cmac100gb_p00 : if QSFP_port_num = x"30" generate
	caui4_p00 : cmac100gb_p00
	PORT MAP(
		txdata_in                    	=> txdata_in                    ,
		txctrl0_in                     	=> txctrl0_in                   ,
		txctrl1_in                     	=> txctrl1_in                   ,

		rxdata_out                     	=> rxdata_out                   ,
		rxctrl0_out                    	=> rxctrl0_out                  ,
		rxctrl1_out                    	=> rxctrl1_out                  ,

--		-- test FEC
--		ctl_tx_rsfec_enable => '1',
--        ctl_rx_rsfec_enable => '1',
--        ctl_rsfec_ieee_error_indication_mode => '1',
--        ctl_rx_rsfec_enable_correction => '1',
--        ctl_rx_rsfec_enable_indication => '1',

		rx_dataout0                    	=> rx_lbus_cell(0).data_bus     ,
		rx_dataout1                    	=> rx_lbus_cell(1).data_bus     ,
		rx_dataout2                    	=> rx_lbus_cell(2).data_bus     ,
		rx_dataout3                    	=> rx_lbus_cell(3).data_bus     ,
		rx_enaout0                     	=> rx_lbus_cell(0).data_ena     ,
		rx_enaout1                     	=> rx_lbus_cell(1).data_ena     ,
		rx_enaout2                     	=> rx_lbus_cell(2).data_ena     ,
		rx_enaout3                     	=> rx_lbus_cell(3).data_ena     ,
		rx_eopout0                     	=> rx_lbus_cell(0).eopckt       ,
		rx_eopout1                     	=> rx_lbus_cell(1).eopckt       ,
		rx_eopout2                     	=> rx_lbus_cell(2).eopckt       ,
		rx_eopout3                     	=> rx_lbus_cell(3).eopckt       ,
		rx_errout0                     	=> rx_lbus_cell(0).err_pckt     ,
		rx_errout1                     	=> rx_lbus_cell(1).err_pckt     ,
		rx_errout2                     	=> rx_lbus_cell(2).err_pckt     ,
		rx_errout3                     	=> rx_lbus_cell(3).err_pckt     ,
		rx_mtyout0                     	=> rx_lbus_cell(0).empty_pckt   ,
		rx_mtyout1                     	=> rx_lbus_cell(1).empty_pckt   ,
		rx_mtyout2                     	=> rx_lbus_cell(2).empty_pckt   ,
		rx_mtyout3                     	=> rx_lbus_cell(3).empty_pckt   ,
		rx_sopout0                     	=> rx_lbus_cell(0).sopckt       ,
		rx_sopout1                     	=> rx_lbus_cell(1).sopckt       ,
		rx_sopout2                     	=> rx_lbus_cell(2).sopckt       ,
		rx_sopout3                     	=> rx_lbus_cell(3).sopckt       ,
	--    rx_otn_bip8_0                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_1                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_2                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_3                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_4                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_data_0                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_1                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_2                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_3                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_4                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_ena                     	=>                          ,--: OUT STD_LOGIC;
	--    rx_otn_lane0                   	=>                          ,--: OUT STD_LOGIC;
	--    rx_otn_vlmarker                	=>                          ,--: OUT STD_LOGIC;
	--    rx_preambleout                 	=>                          ,--: OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
		stat_rx_aligned                	=> stat_rx_aligned              ,
		stat_rx_aligned_err            	=> stat_rx_aligned_err          ,
		stat_rx_bad_code               	=> stat_rx_bad_code             ,
		stat_rx_bad_fcs                	=> stat_rx_bad_fcs              ,
		stat_rx_bad_preamble           	=> stat_rx_bad_preamble         ,
		stat_rx_bad_sfd                	=> stat_rx_bad_sfd              ,
		stat_rx_bip_err_0              	=> stat_rx_bip_err(0)           ,
		stat_rx_bip_err_1              	=> stat_rx_bip_err(1)           ,
		stat_rx_bip_err_2              	=> stat_rx_bip_err(2)           ,
		stat_rx_bip_err_3              	=> stat_rx_bip_err(3)           ,
		stat_rx_block_lock             	=> stat_rx_block_lock           ,
		-- stat_rx_broadcast              	=>                          ,--: OUT STD_LOGIC;
		stat_rx_fragment               	=> stat_rx_fragment             ,
		stat_rx_framing_err_0          	=> rx_frame_l0_err	            ,
		stat_rx_framing_err_1          	=> rx_frame_l1_err	            ,
		stat_rx_framing_err_2          	=> rx_frame_l2_err              ,
		stat_rx_framing_err_3          	=> rx_frame_l3_err              ,

		stat_rx_framing_err_valid_0    	=> rx_frame_l0_err_val          ,
		stat_rx_framing_err_valid_1    	=> rx_frame_l1_err_val          ,
		stat_rx_framing_err_valid_2    	=> rx_frame_l2_err_val          ,
		stat_rx_framing_err_valid_3    	=> rx_frame_l3_err_val          ,
		-- stat_rx_got_signal_os          	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_hi_ber                 	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_inrangeerr             	=>                          ,--: OUT STD_LOGIC;
		stat_rx_internal_local_fault   	=> stat_rx_internal_local_fault ,
		-- stat_rx_jabber                 	=>                          ,--: OUT STD_LOGIC;
		stat_rx_local_fault            	=> stat_rx_local_fault          ,
		-- stat_rx_mf_err                 	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		-- stat_rx_mf_len_err             	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		-- stat_rx_mf_repeat_err          	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		stat_rx_misaligned             	=> stat_rx_misaligned           ,
		-- stat_rx_multicast              	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_oversize               	=>                          ,--: OUT STD_LOGIC;
		stat_rx_packet_1024_1518_bytes 	=> stat_rx_packet(5)            ,
		stat_rx_packet_128_255_bytes   	=> stat_rx_packet(2)            ,
		stat_rx_packet_1519_1522_bytes 	=> stat_rx_packet(6)            ,
		stat_rx_packet_1523_1548_bytes 	=> stat_rx_packet(7)            ,
		stat_rx_packet_1549_2047_bytes 	=> stat_rx_packet(8)            ,
		stat_rx_packet_2048_4095_bytes 	=> stat_rx_packet(9)            ,
		stat_rx_packet_256_511_bytes   	=> stat_rx_packet(3)            ,
		stat_rx_packet_4096_8191_bytes 	=> stat_rx_packet(10)           ,
		stat_rx_packet_512_1023_bytes  	=> stat_rx_packet(4)            ,
		stat_rx_packet_64_bytes        	=> stat_rx_packet(0)            ,
		stat_rx_packet_65_127_bytes    	=> stat_rx_packet(1)            ,
		stat_rx_packet_8192_9215_bytes 	=> stat_rx_packet(11)           ,
		-- stat_rx_packet_bad_fcs         	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_packet_large           	=>                          ,--: OUT STD_LOGIC;
		stat_rx_packet_small           	=> stat_rx_packet_small         ,
		ctl_rx_enable                   => '1',
		ctl_rx_force_resync             => '0',
		ctl_rx_test_pattern             => '0',
		rx_clk                         	=> rx_clk                       ,
		stat_rx_received_local_fault   	=> stat_rx_received_local_fault ,
		-- stat_rx_remote_fault           	=>                          ,--: OUT STD_LOGIC;
		stat_rx_status                 	=> stat_rx_status               ,
		-- stat_rx_stomped_fcs            	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		stat_rx_synced                 	=> stat_rx_synced               ,
		stat_rx_synced_err             	=> stat_rx_synced_err           ,
		-- stat_rx_test_pattern_mismatch  	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		-- stat_rx_toolong                	=>                          ,--: OUT STD_LOGIC;
		stat_rx_total_bytes            	=> stat_rx_total_bytes          ,
		-- stat_rx_total_good_bytes       	=>                          ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
		stat_rx_total_good_packets     	=> inc_rx_total_good_packets_ena,
		-- stat_rx_total_packets          	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		-- stat_rx_truncated              	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_undersize              	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		-- stat_rx_unicast                	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_vlan                   	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_pcsl_demuxed           	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		-- stat_rx_pcsl_number_0          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_1          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_10         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_11         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_12         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_13         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_14         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_15         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_16         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_17         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_18         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_19         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_2          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_3          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_4          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_5          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_6          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_7          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_8          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_9          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		stat_tx_bad_fcs                	=> stat_tx_bad_fcs              ,
		stat_tx_broadcast              	=> stat_tx_broadcast            ,
		stat_tx_frame_error            	=> stat_tx_frame_error          ,
		stat_tx_local_fault            	=> stat_tx_local_fault          ,
		-- stat_tx_multicast              	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1024_1518_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_128_255_bytes   	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1519_1522_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1523_1548_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1549_2047_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_2048_4095_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_256_511_bytes   	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_4096_8191_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_512_1023_bytes  	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_64_bytes        	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_65_127_bytes    	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_8192_9215_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_large           	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_small           	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_total_bytes            	=>                          ,--: OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
		-- stat_tx_total_good_bytes       	=>                          ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
		-- stat_tx_total_good_packets     	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_total_packets          	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_unicast                	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_vlan                   	=>                          ,--: OUT STD_LOGIC;
		ctl_tx_enable                   => ctl_tx_enable,
		ctl_tx_send_idle               	=> '0',--ctl_tx_send_idle             ,
		ctl_tx_send_rfi                	=> ctl_tx_send_rfi              ,
		ctl_tx_send_lfi                	=> '0',--ctl_tx_send_lfi              ,
		ctl_tx_test_pattern             => '0',
		tx_clk                         	=> tx_clk                       ,
		-- stat_tx_pause_valid            	=>                          ,--: OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
		-- stat_tx_pause                  	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_user_pause             	=>                          ,--: OUT STD_LOGIC;
	--    ctl_tx_pause_req               	=> ctl_tx_pause_req_resync      ,
	--    ctl_tx_resend_pause            	=> ctl_tx_resend_pause_resync   ,
		tx_ovfout                      	=> tx_ovfout                    ,
		tx_rdyout                      	=> tx_rdyout                    ,
		tx_unfout                      	=> tx_unfout                    ,
		tx_datain0                     	=> tx_lbus(0).data_bus          ,
		tx_datain1                     	=> tx_lbus(1).data_bus          ,
		tx_datain2                     	=> tx_lbus(2).data_bus          ,
		tx_datain3                     	=> tx_lbus(3).data_bus          ,
		tx_enain0                      	=> tx_lbus(0).data_ena          ,
		tx_enain1                      	=> tx_lbus(1).data_ena          ,
		tx_enain2                      	=> tx_lbus(2).data_ena          ,
		tx_enain3                      	=> tx_lbus(3).data_ena          ,
		tx_eopin0                      	=> tx_lbus(0).eopckt            ,
		tx_eopin1                      	=> tx_lbus(1).eopckt            ,
		tx_eopin2                      	=> tx_lbus(2).eopckt            ,
		tx_eopin3                      	=> tx_lbus(3).eopckt            ,
		tx_errin0                      	=> '0'                          ,
		tx_errin1                      	=> '0'                          ,
		tx_errin2                      	=> '0'                          ,
		tx_errin3                      	=> '0'                          ,
		tx_mtyin0                      	=> tx_lbus(0).empty_pckt        ,
		tx_mtyin1                      	=> tx_lbus(1).empty_pckt        ,
		tx_mtyin2                      	=> tx_lbus(2).empty_pckt        ,
		tx_mtyin3                      	=> tx_lbus(3).empty_pckt        ,
		tx_sopin0                      	=> tx_lbus(0).sopckt            ,
		tx_sopin1                      	=> tx_lbus(1).sopckt            ,
		tx_sopin2                      	=> tx_lbus(2).sopckt            ,
		tx_sopin3                      	=> tx_lbus(3).sopckt            ,
		tx_preamblein                  	=> x"00000000000000"            ,
		tx_reset_done                  	=> tx_reset_done                ,
		rx_reset_done                  	=> rx_reset_done                ,
		rx_serdes_reset_done           	=> rx_serdes_reset_done         ,

		rx_serdes_clk_in               	=> rx_serdes_clk                ,
		drp_clk                        	=> DRP_clock                    ,
		drp_addr                       	=> drp_addr                     ,
		drp_di                         	=> drp_di                       ,
		drp_en                         	=> drp_en_cell                  ,
		drp_do                         	=> drp_do                       ,
		drp_rdy                        	=> drp_rdy                      ,
		drp_we                         	=> drp_we(1)
	);
end generate;


gen_cmac100gb_p01 : if QSFP_port_num = x"31" generate
	caui4_p01 : cmac100gb_p01
	PORT MAP(
		txdata_in                    	=> txdata_in                    ,
		txctrl0_in                     	=> txctrl0_in                   ,
		txctrl1_in                     	=> txctrl1_in                   ,

		rxdata_out                     	=> rxdata_out                   ,
		rxctrl0_out                    	=> rxctrl0_out                  ,
		rxctrl1_out                    	=> rxctrl1_out                  ,

--		-- test FEC
--		ctl_tx_rsfec_enable => '1',
--        ctl_rx_rsfec_enable => '1',
--        ctl_rsfec_ieee_error_indication_mode => '1',
--        ctl_rx_rsfec_enable_correction => '1',
--        ctl_rx_rsfec_enable_indication => '1',

		rx_dataout0                    	=> rx_lbus_cell(0).data_bus     ,
		rx_dataout1                    	=> rx_lbus_cell(1).data_bus     ,
		rx_dataout2                    	=> rx_lbus_cell(2).data_bus     ,
		rx_dataout3                    	=> rx_lbus_cell(3).data_bus     ,
		rx_enaout0                     	=> rx_lbus_cell(0).data_ena     ,
		rx_enaout1                     	=> rx_lbus_cell(1).data_ena     ,
		rx_enaout2                     	=> rx_lbus_cell(2).data_ena     ,
		rx_enaout3                     	=> rx_lbus_cell(3).data_ena     ,
		rx_eopout0                     	=> rx_lbus_cell(0).eopckt       ,
		rx_eopout1                     	=> rx_lbus_cell(1).eopckt       ,
		rx_eopout2                     	=> rx_lbus_cell(2).eopckt       ,
		rx_eopout3                     	=> rx_lbus_cell(3).eopckt       ,
		rx_errout0                     	=> rx_lbus_cell(0).err_pckt     ,
		rx_errout1                     	=> rx_lbus_cell(1).err_pckt     ,
		rx_errout2                     	=> rx_lbus_cell(2).err_pckt     ,
		rx_errout3                     	=> rx_lbus_cell(3).err_pckt     ,
		rx_mtyout0                     	=> rx_lbus_cell(0).empty_pckt   ,
		rx_mtyout1                     	=> rx_lbus_cell(1).empty_pckt   ,
		rx_mtyout2                     	=> rx_lbus_cell(2).empty_pckt   ,
		rx_mtyout3                     	=> rx_lbus_cell(3).empty_pckt   ,
		rx_sopout0                     	=> rx_lbus_cell(0).sopckt       ,
		rx_sopout1                     	=> rx_lbus_cell(1).sopckt       ,
		rx_sopout2                     	=> rx_lbus_cell(2).sopckt       ,
		rx_sopout3                     	=> rx_lbus_cell(3).sopckt       ,
	--    rx_otn_bip8_0                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_1                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_2                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_3                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_4                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_data_0                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_1                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_2                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_3                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_4                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_ena                     	=>                          ,--: OUT STD_LOGIC;
	--    rx_otn_lane0                   	=>                          ,--: OUT STD_LOGIC;
	--    rx_otn_vlmarker                	=>                          ,--: OUT STD_LOGIC;
	--    rx_preambleout                 	=>                          ,--: OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
		stat_rx_aligned                	=> stat_rx_aligned              ,
		stat_rx_aligned_err            	=> stat_rx_aligned_err          ,
		stat_rx_bad_code               	=> stat_rx_bad_code             ,
		stat_rx_bad_fcs                	=> stat_rx_bad_fcs              ,
		stat_rx_bad_preamble           	=> stat_rx_bad_preamble         ,
		stat_rx_bad_sfd                	=> stat_rx_bad_sfd              ,
		stat_rx_bip_err_0              	=> stat_rx_bip_err(0)           ,
		stat_rx_bip_err_1              	=> stat_rx_bip_err(1)           ,
		stat_rx_bip_err_2              	=> stat_rx_bip_err(2)           ,
		stat_rx_bip_err_3              	=> stat_rx_bip_err(3)           ,
		stat_rx_block_lock             	=> stat_rx_block_lock           ,
		-- stat_rx_broadcast              	=>                          ,--: OUT STD_LOGIC;
		stat_rx_fragment               	=> stat_rx_fragment             ,
		stat_rx_framing_err_0          	=> rx_frame_l0_err	            ,
		stat_rx_framing_err_1          	=> rx_frame_l1_err	            ,
		stat_rx_framing_err_2          	=> rx_frame_l2_err              ,
		stat_rx_framing_err_3          	=> rx_frame_l3_err              ,

		stat_rx_framing_err_valid_0    	=> rx_frame_l0_err_val          ,
		stat_rx_framing_err_valid_1    	=> rx_frame_l1_err_val          ,
		stat_rx_framing_err_valid_2    	=> rx_frame_l2_err_val          ,
		stat_rx_framing_err_valid_3    	=> rx_frame_l3_err_val          ,
		-- stat_rx_got_signal_os          	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_hi_ber                 	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_inrangeerr             	=>                          ,--: OUT STD_LOGIC;
		stat_rx_internal_local_fault   	=> stat_rx_internal_local_fault ,
		-- stat_rx_jabber                 	=>                          ,--: OUT STD_LOGIC;
		stat_rx_local_fault            	=> stat_rx_local_fault          ,
		-- stat_rx_mf_err                 	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		-- stat_rx_mf_len_err             	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		-- stat_rx_mf_repeat_err          	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		stat_rx_misaligned             	=> stat_rx_misaligned           ,
		-- stat_rx_multicast              	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_oversize               	=>                          ,--: OUT STD_LOGIC;
		stat_rx_packet_1024_1518_bytes 	=> stat_rx_packet(5)            ,
		stat_rx_packet_128_255_bytes   	=> stat_rx_packet(2)            ,
		stat_rx_packet_1519_1522_bytes 	=> stat_rx_packet(6)            ,
		stat_rx_packet_1523_1548_bytes 	=> stat_rx_packet(7)            ,
		stat_rx_packet_1549_2047_bytes 	=> stat_rx_packet(8)            ,
		stat_rx_packet_2048_4095_bytes 	=> stat_rx_packet(9)            ,
		stat_rx_packet_256_511_bytes   	=> stat_rx_packet(3)            ,
		stat_rx_packet_4096_8191_bytes 	=> stat_rx_packet(10)           ,
		stat_rx_packet_512_1023_bytes  	=> stat_rx_packet(4)            ,
		stat_rx_packet_64_bytes        	=> stat_rx_packet(0)            ,
		stat_rx_packet_65_127_bytes    	=> stat_rx_packet(1)            ,
		stat_rx_packet_8192_9215_bytes 	=> stat_rx_packet(11)           ,
		-- stat_rx_packet_bad_fcs         	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_packet_large           	=>                          ,--: OUT STD_LOGIC;
		stat_rx_packet_small           	=> stat_rx_packet_small         ,
		ctl_rx_enable                   => '1',
		ctl_rx_force_resync             => '0',
		ctl_rx_test_pattern             => '0',
		rx_clk                         	=> rx_clk                       ,
		stat_rx_received_local_fault   	=> stat_rx_received_local_fault ,
		-- stat_rx_remote_fault           	=>                          ,--: OUT STD_LOGIC;
		stat_rx_status                 	=> stat_rx_status               ,
		-- stat_rx_stomped_fcs            	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		stat_rx_synced                 	=> stat_rx_synced               ,
		stat_rx_synced_err             	=> stat_rx_synced_err           ,
		-- stat_rx_test_pattern_mismatch  	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		-- stat_rx_toolong                	=>                          ,--: OUT STD_LOGIC;
		stat_rx_total_bytes            	=> stat_rx_total_bytes          ,
		-- stat_rx_total_good_bytes       	=>                          ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
		stat_rx_total_good_packets     	=> inc_rx_total_good_packets_ena,
		-- stat_rx_total_packets          	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		-- stat_rx_truncated              	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_undersize              	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		-- stat_rx_unicast                	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_vlan                   	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_pcsl_demuxed           	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		-- stat_rx_pcsl_number_0          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_1          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_10         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_11         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_12         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_13         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_14         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_15         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_16         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_17         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_18         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_19         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_2          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_3          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_4          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_5          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_6          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_7          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_8          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_9          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		stat_tx_bad_fcs                	=> stat_tx_bad_fcs              ,
		stat_tx_broadcast              	=> stat_tx_broadcast            ,
		stat_tx_frame_error            	=> stat_tx_frame_error          ,
		stat_tx_local_fault            	=> stat_tx_local_fault          ,
		-- stat_tx_multicast              	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1024_1518_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_128_255_bytes   	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1519_1522_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1523_1548_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1549_2047_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_2048_4095_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_256_511_bytes   	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_4096_8191_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_512_1023_bytes  	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_64_bytes        	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_65_127_bytes    	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_8192_9215_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_large           	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_small           	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_total_bytes            	=>                          ,--: OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
		-- stat_tx_total_good_bytes       	=>                          ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
		-- stat_tx_total_good_packets     	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_total_packets          	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_unicast                	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_vlan                   	=>                          ,--: OUT STD_LOGIC;
		ctl_tx_enable                   => ctl_tx_enable,
		ctl_tx_send_idle               	=> '0',--ctl_tx_send_idle             ,
		ctl_tx_send_rfi                	=> ctl_tx_send_rfi              ,
		ctl_tx_send_lfi                	=> '0',--ctl_tx_send_lfi              ,
		ctl_tx_test_pattern             => '0',
		tx_clk                         	=> tx_clk                       ,
		-- stat_tx_pause_valid            	=>                          ,--: OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
		-- stat_tx_pause                  	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_user_pause             	=>                          ,--: OUT STD_LOGIC;
	--    ctl_tx_pause_req               	=> ctl_tx_pause_req_resync      ,
	--    ctl_tx_resend_pause            	=> ctl_tx_resend_pause_resync   ,
		tx_ovfout                      	=> tx_ovfout                    ,
		tx_rdyout                      	=> tx_rdyout                    ,
		tx_unfout                      	=> tx_unfout                    ,
		tx_datain0                     	=> tx_lbus(0).data_bus          ,
		tx_datain1                     	=> tx_lbus(1).data_bus          ,
		tx_datain2                     	=> tx_lbus(2).data_bus          ,
		tx_datain3                     	=> tx_lbus(3).data_bus          ,
		tx_enain0                      	=> tx_lbus(0).data_ena          ,
		tx_enain1                      	=> tx_lbus(1).data_ena          ,
		tx_enain2                      	=> tx_lbus(2).data_ena          ,
		tx_enain3                      	=> tx_lbus(3).data_ena          ,
		tx_eopin0                      	=> tx_lbus(0).eopckt            ,
		tx_eopin1                      	=> tx_lbus(1).eopckt            ,
		tx_eopin2                      	=> tx_lbus(2).eopckt            ,
		tx_eopin3                      	=> tx_lbus(3).eopckt            ,
		tx_errin0                      	=> '0'                          ,
		tx_errin1                      	=> '0'                          ,
		tx_errin2                      	=> '0'                          ,
		tx_errin3                      	=> '0'                          ,
		tx_mtyin0                      	=> tx_lbus(0).empty_pckt        ,
		tx_mtyin1                      	=> tx_lbus(1).empty_pckt        ,
		tx_mtyin2                      	=> tx_lbus(2).empty_pckt        ,
		tx_mtyin3                      	=> tx_lbus(3).empty_pckt        ,
		tx_sopin0                      	=> tx_lbus(0).sopckt            ,
		tx_sopin1                      	=> tx_lbus(1).sopckt            ,
		tx_sopin2                      	=> tx_lbus(2).sopckt            ,
		tx_sopin3                      	=> tx_lbus(3).sopckt            ,
		tx_preamblein                  	=> x"00000000000000"            ,
		tx_reset_done                  	=> tx_reset_done                ,
		rx_reset_done                  	=> rx_reset_done                ,
		rx_serdes_reset_done           	=> rx_serdes_reset_done         ,

		rx_serdes_clk_in               	=> rx_serdes_clk                ,
		drp_clk                        	=> DRP_clock                    ,
		drp_addr                       	=> drp_addr                     ,
		drp_di                         	=> drp_di                       ,
		drp_en                         	=> drp_en_cell                  ,
		drp_do                         	=> drp_do                       ,
		drp_rdy                        	=> drp_rdy                      ,
		drp_we                         	=> drp_we(1)
	);
end generate;

gen_cmac100gb_p02 : if QSFP_port_num = x"32" generate
	caui4_p02 : cmac100gb_p02
	PORT MAP(
		txdata_in                    	=> txdata_in                    ,
		txctrl0_in                     	=> txctrl0_in                   ,
		txctrl1_in                     	=> txctrl1_in                   ,

		rxdata_out                     	=> rxdata_out                   ,
		rxctrl0_out                    	=> rxctrl0_out                  ,
		rxctrl1_out                    	=> rxctrl1_out                  ,

--		-- test FEC
--		ctl_tx_rsfec_enable => '1',
--        ctl_rx_rsfec_enable => '1',
--        ctl_rsfec_ieee_error_indication_mode => '1',
--        ctl_rx_rsfec_enable_correction => '1',
--        ctl_rx_rsfec_enable_indication => '1',

		rx_dataout0                    	=> rx_lbus_cell(0).data_bus     ,
		rx_dataout1                    	=> rx_lbus_cell(1).data_bus     ,
		rx_dataout2                    	=> rx_lbus_cell(2).data_bus     ,
		rx_dataout3                    	=> rx_lbus_cell(3).data_bus     ,
		rx_enaout0                     	=> rx_lbus_cell(0).data_ena     ,
		rx_enaout1                     	=> rx_lbus_cell(1).data_ena     ,
		rx_enaout2                     	=> rx_lbus_cell(2).data_ena     ,
		rx_enaout3                     	=> rx_lbus_cell(3).data_ena     ,
		rx_eopout0                     	=> rx_lbus_cell(0).eopckt       ,
		rx_eopout1                     	=> rx_lbus_cell(1).eopckt       ,
		rx_eopout2                     	=> rx_lbus_cell(2).eopckt       ,
		rx_eopout3                     	=> rx_lbus_cell(3).eopckt       ,
		rx_errout0                     	=> rx_lbus_cell(0).err_pckt     ,
		rx_errout1                     	=> rx_lbus_cell(1).err_pckt     ,
		rx_errout2                     	=> rx_lbus_cell(2).err_pckt     ,
		rx_errout3                     	=> rx_lbus_cell(3).err_pckt     ,
		rx_mtyout0                     	=> rx_lbus_cell(0).empty_pckt   ,
		rx_mtyout1                     	=> rx_lbus_cell(1).empty_pckt   ,
		rx_mtyout2                     	=> rx_lbus_cell(2).empty_pckt   ,
		rx_mtyout3                     	=> rx_lbus_cell(3).empty_pckt   ,
		rx_sopout0                     	=> rx_lbus_cell(0).sopckt       ,
		rx_sopout1                     	=> rx_lbus_cell(1).sopckt       ,
		rx_sopout2                     	=> rx_lbus_cell(2).sopckt       ,
		rx_sopout3                     	=> rx_lbus_cell(3).sopckt       ,
	--    rx_otn_bip8_0                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_1                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_2                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_3                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_bip8_4                  	=>                          ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	--    rx_otn_data_0                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_1                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_2                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_3                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_data_4                  	=>                          ,--: OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
	--    rx_otn_ena                     	=>                          ,--: OUT STD_LOGIC;
	--    rx_otn_lane0                   	=>                          ,--: OUT STD_LOGIC;
	--    rx_otn_vlmarker                	=>                          ,--: OUT STD_LOGIC;
	--    rx_preambleout                 	=>                          ,--: OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
		stat_rx_aligned                	=> stat_rx_aligned              ,
		stat_rx_aligned_err            	=> stat_rx_aligned_err          ,
		stat_rx_bad_code               	=> stat_rx_bad_code             ,
		stat_rx_bad_fcs                	=> stat_rx_bad_fcs              ,
		stat_rx_bad_preamble           	=> stat_rx_bad_preamble         ,
		stat_rx_bad_sfd                	=> stat_rx_bad_sfd              ,
		stat_rx_bip_err_0              	=> stat_rx_bip_err(0)           ,
		stat_rx_bip_err_1              	=> stat_rx_bip_err(1)           ,
		stat_rx_bip_err_2              	=> stat_rx_bip_err(2)           ,
		stat_rx_bip_err_3              	=> stat_rx_bip_err(3)           ,
		stat_rx_block_lock             	=> stat_rx_block_lock           ,
		-- stat_rx_broadcast              	=>                          ,--: OUT STD_LOGIC;
		stat_rx_fragment               	=> stat_rx_fragment             ,
		stat_rx_framing_err_0          	=> rx_frame_l0_err	            ,
		stat_rx_framing_err_1          	=> rx_frame_l1_err	            ,
		stat_rx_framing_err_2          	=> rx_frame_l2_err              ,
		stat_rx_framing_err_3          	=> rx_frame_l3_err              ,

		stat_rx_framing_err_valid_0    	=> rx_frame_l0_err_val          ,
		stat_rx_framing_err_valid_1    	=> rx_frame_l1_err_val          ,
		stat_rx_framing_err_valid_2    	=> rx_frame_l2_err_val          ,
		stat_rx_framing_err_valid_3    	=> rx_frame_l3_err_val          ,
		-- stat_rx_got_signal_os          	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_hi_ber                 	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_inrangeerr             	=>                          ,--: OUT STD_LOGIC;
		stat_rx_internal_local_fault   	=> stat_rx_internal_local_fault ,
		-- stat_rx_jabber                 	=>                          ,--: OUT STD_LOGIC;
		stat_rx_local_fault            	=> stat_rx_local_fault          ,
		-- stat_rx_mf_err                 	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		-- stat_rx_mf_len_err             	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		-- stat_rx_mf_repeat_err          	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		stat_rx_misaligned             	=> stat_rx_misaligned           ,
		-- stat_rx_multicast              	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_oversize               	=>                          ,--: OUT STD_LOGIC;
		stat_rx_packet_1024_1518_bytes 	=> stat_rx_packet(5)            ,
		stat_rx_packet_128_255_bytes   	=> stat_rx_packet(2)            ,
		stat_rx_packet_1519_1522_bytes 	=> stat_rx_packet(6)            ,
		stat_rx_packet_1523_1548_bytes 	=> stat_rx_packet(7)            ,
		stat_rx_packet_1549_2047_bytes 	=> stat_rx_packet(8)            ,
		stat_rx_packet_2048_4095_bytes 	=> stat_rx_packet(9)            ,
		stat_rx_packet_256_511_bytes   	=> stat_rx_packet(3)            ,
		stat_rx_packet_4096_8191_bytes 	=> stat_rx_packet(10)           ,
		stat_rx_packet_512_1023_bytes  	=> stat_rx_packet(4)            ,
		stat_rx_packet_64_bytes        	=> stat_rx_packet(0)            ,
		stat_rx_packet_65_127_bytes    	=> stat_rx_packet(1)            ,
		stat_rx_packet_8192_9215_bytes 	=> stat_rx_packet(11)           ,
		-- stat_rx_packet_bad_fcs         	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_packet_large           	=>                          ,--: OUT STD_LOGIC;
		stat_rx_packet_small           	=> stat_rx_packet_small         ,
		ctl_rx_enable                   => '1',
		ctl_rx_force_resync             => '0',
		ctl_rx_test_pattern             => '0',
		rx_clk                         	=> rx_clk                       ,
		stat_rx_received_local_fault   	=> stat_rx_received_local_fault ,
		-- stat_rx_remote_fault           	=>                          ,--: OUT STD_LOGIC;
		stat_rx_status                 	=> stat_rx_status               ,
		-- stat_rx_stomped_fcs            	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		stat_rx_synced                 	=> stat_rx_synced               ,
		stat_rx_synced_err             	=> stat_rx_synced_err           ,
		-- stat_rx_test_pattern_mismatch  	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		-- stat_rx_toolong                	=>                          ,--: OUT STD_LOGIC;
		stat_rx_total_bytes            	=> stat_rx_total_bytes          ,
		-- stat_rx_total_good_bytes       	=>                          ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
		stat_rx_total_good_packets     	=> inc_rx_total_good_packets_ena,
		-- stat_rx_total_packets          	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		-- stat_rx_truncated              	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_undersize              	=>                          ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		-- stat_rx_unicast                	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_vlan                   	=>                          ,--: OUT STD_LOGIC;
		-- stat_rx_pcsl_demuxed           	=>                          ,--: OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		-- stat_rx_pcsl_number_0          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_1          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_10         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_11         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_12         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_13         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_14         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_15         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_16         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_17         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_18         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_19         	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_2          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_3          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_4          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_5          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_6          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_7          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_8          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		-- stat_rx_pcsl_number_9          	=>                          ,--: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		stat_tx_bad_fcs                	=> stat_tx_bad_fcs              ,
		stat_tx_broadcast              	=> stat_tx_broadcast            ,
		stat_tx_frame_error            	=> stat_tx_frame_error          ,
		stat_tx_local_fault            	=> stat_tx_local_fault          ,
		-- stat_tx_multicast              	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1024_1518_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_128_255_bytes   	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1519_1522_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1523_1548_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_1549_2047_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_2048_4095_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_256_511_bytes   	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_4096_8191_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_512_1023_bytes  	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_64_bytes        	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_65_127_bytes    	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_8192_9215_bytes 	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_large           	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_packet_small           	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_total_bytes            	=>                          ,--: OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
		-- stat_tx_total_good_bytes       	=>                          ,--: OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
		-- stat_tx_total_good_packets     	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_total_packets          	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_unicast                	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_vlan                   	=>                          ,--: OUT STD_LOGIC;
		ctl_tx_enable                   => ctl_tx_enable,
		ctl_tx_send_idle               	=> '0',--ctl_tx_send_idle             ,
		ctl_tx_send_rfi                	=> ctl_tx_send_rfi              ,
		ctl_tx_send_lfi                	=> '0',--ctl_tx_send_lfi              ,
		ctl_tx_test_pattern             => '0',
		tx_clk                         	=> tx_clk                       ,
		-- stat_tx_pause_valid            	=>                          ,--: OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
		-- stat_tx_pause                  	=>                          ,--: OUT STD_LOGIC;
		-- stat_tx_user_pause             	=>                          ,--: OUT STD_LOGIC;
	--    ctl_tx_pause_req               	=> ctl_tx_pause_req_resync      ,
	--    ctl_tx_resend_pause            	=> ctl_tx_resend_pause_resync   ,
		tx_ovfout                      	=> tx_ovfout                    ,
		tx_rdyout                      	=> tx_rdyout                    ,
		tx_unfout                      	=> tx_unfout                    ,
		tx_datain0                     	=> tx_lbus(0).data_bus          ,
		tx_datain1                     	=> tx_lbus(1).data_bus          ,
		tx_datain2                     	=> tx_lbus(2).data_bus          ,
		tx_datain3                     	=> tx_lbus(3).data_bus          ,
		tx_enain0                      	=> tx_lbus(0).data_ena          ,
		tx_enain1                      	=> tx_lbus(1).data_ena          ,
		tx_enain2                      	=> tx_lbus(2).data_ena          ,
		tx_enain3                      	=> tx_lbus(3).data_ena          ,
		tx_eopin0                      	=> tx_lbus(0).eopckt            ,
		tx_eopin1                      	=> tx_lbus(1).eopckt            ,
		tx_eopin2                      	=> tx_lbus(2).eopckt            ,
		tx_eopin3                      	=> tx_lbus(3).eopckt            ,
		tx_errin0                      	=> '0'                          ,
		tx_errin1                      	=> '0'                          ,
		tx_errin2                      	=> '0'                          ,
		tx_errin3                      	=> '0'                          ,
		tx_mtyin0                      	=> tx_lbus(0).empty_pckt        ,
		tx_mtyin1                      	=> tx_lbus(1).empty_pckt        ,
		tx_mtyin2                      	=> tx_lbus(2).empty_pckt        ,
		tx_mtyin3                      	=> tx_lbus(3).empty_pckt        ,
		tx_sopin0                      	=> tx_lbus(0).sopckt            ,
		tx_sopin1                      	=> tx_lbus(1).sopckt            ,
		tx_sopin2                      	=> tx_lbus(2).sopckt            ,
		tx_sopin3                      	=> tx_lbus(3).sopckt            ,
		tx_preamblein                  	=> x"00000000000000"            ,
		tx_reset_done                  	=> tx_reset_done                ,
		rx_reset_done                  	=> rx_reset_done                ,
		rx_serdes_reset_done           	=> rx_serdes_reset_done         ,

		rx_serdes_clk_in               	=> rx_serdes_clk                ,
		drp_clk                        	=> DRP_clock                    ,
		drp_addr                       	=> drp_addr                     ,
		drp_di                         	=> drp_di                       ,
		drp_en                         	=> drp_en_cell                  ,
		drp_do                         	=> drp_do                       ,
		drp_rdy                        	=> drp_rdy                      ,
		drp_we                         	=> drp_we(1)
	);
end generate;


resync_signal_i10:resync_sig_gen
port map(
	clocko				=> tx_clk ,
	input				=> stat_rx_aligned,
	output				=> stat_rx_aligned_sync_TX
	);


resync_reset_i4:resetp_resync
port map(
	aresetp			=> Serdes_Reset_p,
	clock			=> tx_clk,
	Resetp_sync		=> Serdes_rst_T_Clk
	);

process(Serdes_rst_T_Clk,tx_clk)
begin
	if Serdes_rst_T_Clk = '1' then
		ctl_tx_send_rfi <= '1';
		ctl_tx_enable	<= '0';
	elsif rising_edge(tx_clk) then
		if stat_rx_aligned_sync_TX = '1' then
			ctl_tx_send_rfi <= '0';
			ctl_tx_enable	<= '1';
		end if;
	end if;
end process;


gt_rxlpmen					<= "1111" ;

MAC100_GT_i1:MAC100gb_gt_wrapper
    generic map (
        QSFP_port_num => QSFP_port_num
    )
    port map (
        gt_rxp_in(0)               	=> QSFP_RX1_P,--: in std_logic_vector(3 downto 0);
        gt_rxp_in(1)               	=> QSFP_RX2_P,--: in std_logic_vector(3 downto 0);
        gt_rxp_in(2)               	=> QSFP_RX3_P,--: in std_logic_vector(3 downto 0);
        gt_rxp_in(3)               	=> QSFP_RX4_P,--: in std_logic_vector(3 downto 0);
        gt_rxn_in(0)               	=> QSFP_RX1_N,--: in std_logic_vector(3 downto 0);
        gt_rxn_in(1)               	=> QSFP_RX2_N,--: in std_logic_vector(3 downto 0);
        gt_rxn_in(2)               	=> QSFP_RX3_N,--: in std_logic_vector(3 downto 0);
        gt_rxn_in(3)               	=> QSFP_RX4_N,--: in std_logic_vector(3 downto 0);
        gt_txn_out(0)             	=> QSFP_TX1_N,--: out std_logic_vector(3 downto 0);
        gt_txn_out(1)             	=> QSFP_TX2_N,--: out std_logic_vector(3 downto 0);
        gt_txn_out(2)             	=> QSFP_TX3_N,--: out std_logic_vector(3 downto 0);
        gt_txn_out(3)             	=> QSFP_TX4_N,--: out std_logic_vector(3 downto 0);
        gt_txp_out(0)              	=> QSFP_TX1_P,--: out std_logic_vector(3 downto 0);
        gt_txp_out(1)              	=> QSFP_TX2_P,--: out std_logic_vector(3 downto 0);
        gt_txp_out(2)              	=> QSFP_TX3_P,--: out std_logic_vector(3 downto 0);
        gt_txp_out(3)              	=> QSFP_TX4_P,--: out std_logic_vector(3 downto 0);

        ctrl_gt_loopback_in        	=> "000",--: in std_logic_vector(2 downto 0);
        gt_rxrecclkout            	=> gt_rxrecclkout,--: out std_logic_vector(3 downto 0);
        gt_powergoodout           	=> gt_powergoodout,--: out std_logic_vector(3 downto 0);

        -- GT Transceiver debug interface ports
        gt_eyescanreset           	=> "0000",--: in std_logic_vector(3 downto 0);
        gt_eyescantrigger         	=> "0000",--: in std_logic_vector(3 downto 0);
        gt_rxcdrhold                => "0000",
        gt_rxrate                   => "000000000000",
        rx_polarity             	=> rx_polarity,--: in std_logic;

        tx_diff_ctrl             	=> tx_diff_ctrl,--: in std_logic_vector(4 downto 0);

        tx_polarity             	=> tx_polarity,--: in std_logic;

        gt_txinhibit              	=> "0000",--: in std_logic_vector(3 downto 0);
        gt_txpippmen              	=> "0000",--: in std_logic_vector(3 downto 0);
        gt_txpippmsel             	=> "1111",--: in std_logic_vector(3 downto 0);
        txpostcursor           		=> txpostcursor,--: in std_logic_vector(4 downto 0);

        gt_txprbsforceerr_sync      => gt_txprbsforceerr_sync,--: in std_logic;
        txprecursor            		=> txprecursor,--: in std_logic_vector(4 downto 0);
        gt_txbufstatus             	=> gt_txbufstatus,--: out std_logic_vector(7 downto 0);

        gt_rxdfelpmreset			=> "0000",
        gt_rxlpmen					=> gt_rxlpmen,--: in std_logic_vector(3 downto 0);
        gt_rxprbscntreset_async    	=> gt_rxprbscntreset_async,--: in std_logic;
        gt_rxprbserr_out           	=> gt_rxprbserr,--: out std_logic_vector(3 downto 0);
        gt_rxprbssel_async          => gt_rxprbssel_async,--: in std_logic_vector(3 downto 0);
        -- gt_rxresetdone              => ,--: out std_logic_vector(3 downto 0);
        gt_txprbssel_async          => gt_txprbssel_async,--: in std_logic_vector(3 downto 0);
        gt_txresetdone              => gt_txresetdone,--: out std_logic_vector(3 downto 0);
        gt_rxbufstatus            	=> gt_rxbufstatus,--: out std_logic_vector(11 downto 0);
        gtwiz_reset_tx_datapath   	=> '0',--: in std_logic;
        gtwiz_reset_rx_datapath		=> '0',

        ctl_gt_loopback				=> ctl_gt_loopback,

        gt_drprstp                  => DRP_rstp              ,
        gt_drpclk                 	=> DRP_clock ,--: in std_logic;
        gt_drpaddr               	=> gtx_ch_drp_addr,
        gt_drpen                 	=> gt_ch_drp_en,
        gt_drpdi(15 downto 00)      => gtx_ch_drp_di,
        gt_drpdi(31 downto 16)      => gtx_ch_drp_di,
        gt_drpdi(47 downto 32)      => gtx_ch_drp_di,
        gt_drpdi(63 downto 48)      => gtx_ch_drp_di,
        gt_drpdo                 	=> gt_ch_drp_do,
        gt_drprdy               	=> gt_ch_drp_rdy,
        gt_drpwe                 	=> gt_ch_drp_we,

        gt_txusrclk2              	=> gt_txusrclk2,--: out std_logic;
        gt_rxusrclk2 	            => gt_rxusrclk2,--: out std_logic;
        gt_reset_tx_done_out        => gt_reset_tx_done,--: out std_logic;
        gt_reset_rx_done_out        => gt_reset_rx_done,--: out std_logic;
        rx_serdes_clk             	=> rx_serdes_clk,--: out std_logic_vector(9 downto 0);

        qpll0clk_in               	=> qpll0clk_in     ,--: in std_logic_vector(3 downto 0);
        qpll0refclk_in            	=> qpll0refclk_in  ,--: in std_logic_vector(3 downto 0);
        qpll1clk_in               	=> "0000"          ,--: in std_logic_vector(3 downto 0);
        qpll1refclk_in            	=> "0000"          ,--: in std_logic_vector(3 downto 0);
        gtwiz_reset_qpll0lock_in  	=> gtwiz_qpll0lock ,--: in std_logic;
        gtwiz_reset_qpll0reset_out	=> gtwiz_qpll0reset,--: out std_logic;

        txdata_in                  	=> txdata_in  ,--: in std_logic_vector(511 downto 0);
        txctrl0_in                 	=> txctrl0_in ,--: in std_logic_vector(63 downto 0);
        txctrl1_in                 	=> txctrl1_in ,--: in std_logic_vector(63 downto 0);

        rxdata_out                 	=> rxdata_out   ,--: out std_logic_vector(511 downto 0);
        rxctrl0_out                	=> rxctrl0_out  ,--: out std_logic_vector(63 downto 0);
        rxctrl1_out                	=> rxctrl1_out  ,--: out std_logic_vector(63 downto 0);

        sys_reset                 	=> sys_reset  ,--: in std_logic;
        --usr_clk                 	=> usr_clk    ,--: in std_logic;
        init_clk					=> init_clk  ,  --: in std_logic;

        gt_rxprbslocked_out        	=> rxprbslocked_out ,--: out std_logic_vector(3 downto 0);

        txpcsreset_in				=> gt_txpcsreset_async,
        txoutclksel_in              => txoutclksel_async
    );

resync_sig_i4:resync_v4
port map(
	aresetn				=> '1',
	clocki				=> usr_clk,
	input				=> gt_txprbsforceerr_async,
	clocko				=> gt_txusrclk2,
	output				=> gt_txprbsforceerr_sync
	);


process(Usr_resetp_MAC_TX_clock,gt_txusrclk2)
begin
    if Usr_resetp_MAC_TX_clock = '1' then
        MAC_ready_reg   <= '0';
    elsif rising_edge(gt_txusrclk2) then
        if stat_rx_aligned = '1' then
            MAC_ready_reg   <= '1';
        end if;
    end if;
end process;

MAC_ready           <= MAC_ready_reg;

rx_lbus				<= rx_lbus_cell;
mac_rx_clk 			<= gt_txusrclk2;
rx_clk      		<= gt_txusrclk2;

tx_clk      		<= gt_txusrclk2;
mac_tx_clk 			<= gt_txusrclk2;

tx_lbus_ready		<= tx_rdyout;

drp_en_cell			<= '1' when drp_we(1) = '1' or drp_en(1) = '1' else '0';

powergood			<= '1' when (gt_powergoodout  = "1111" ) else  '0';

qpll0clk_in(0)		<= qpll0clk;
qpll0clk_in(1)		<= qpll0clk;
qpll0clk_in(2)		<= qpll0clk;
qpll0clk_in(3)		<= qpll0clk;
qpll0refclk_in(0)	<= qpll0refclk;
qpll0refclk_in(1)	<= qpll0refclk;
qpll0refclk_in(2)	<= qpll0refclk;
qpll0refclk_in(3)	<= qpll0refclk;

MAC_GT_share_logic_i1: MAC100GB_v1_shared_logic_wrapper
 Port map(
	gt_ref_clk_p           		=> GT_ref_clock_p,--: in std_logic;
	gt_ref_clk_n           		=> GT_ref_clock_n,--: in std_logic;
	gt_txusrclk2           		=> gt_txusrclk2,--: in std_logic;
	gt_rxusrclk2           		=> gt_rxusrclk2,--: in std_logic;
	gt_ref_clk_out         		=> gt_ref_clk_out,--: out std_logic;
	rx_clk                 		=> tx_clk,--: in std_logic;
	gt_powergood           		=> powergood,--: in std_logic;
	gt_tx_reset_in         		=> gt_reset_tx_done,--: in std_logic;
	gt_rx_reset_in         		=> gt_reset_rx_done,--: in std_logic;
	core_drp_reset         		=> '0',--: in std_logic;
	core_tx_reset          		=> '0',--: in std_logic;
	tx_reset_out           		=> tx_reset_done,--: out std_logic;
	core_rx_reset          		=> '0',--: in std_logic;
	rx_reset_out           		=> rx_reset_done,--: out std_logic;
	rx_serdes_reset_out    		=> rx_serdes_reset_done,--: out std_logic_vector(9 downto 0);
	qpll0reset(0)          		=> gtwiz_qpll0reset,--: in std_logic_vector(0 downto 0);
	qpll0lock(0)           		=> gtwiz_qpll0lock,--: out std_logic_vector(0 downto 0);
	qpll0outclk(0)         		=> qpll0clk,--: out std_logic_vector(0 downto 0);
	qpll0outrefclk(0)      		=> qpll0refclk,--: out std_logic_vector(0 downto 0);
	drpclk_common_in       		=> init_clk,--: in std_logic;
	common0_drpaddr        		=> x"0000",--: in std_logic_vector(15 downto 0);
	common0_drpdi          		=> x"0000",--: in std_logic_vector(15 downto 0);
	common0_drpwe          		=> '0',--: in std_logic;
	common0_drpen          		=> '0',--: in std_logic;
	-- common0_drprdy         		=> ,--: out std_logic;
	-- common0_drpdo          		=> ,--: out std_logic_vector(15 downto 0);
	qpll1reset(0)          		=> '0',--: in std_logic_vector(0 downto 0);
	-- qpll1lock              		=> ,--: out std_logic_vector(0 downto 0);
	-- qpll1outclk            		=> ,--: out std_logic_vector(0 downto 0);
	-- qpll1outrefclk         		=> ,--: out std_logic_vector(0 downto 0);
	usr_tx_reset           		=> usr_tx_reset,--: out std_logic;
	usr_rx_reset				=> usr_rx_reset --: out std_logic
	);



--**************************************************************************************************
--		PCIe write data

-- register which should be reset
process(usr_rst_n,usr_clk)
  begin
      if usr_rst_n = '0' then
      	gt_rxpcsreset_async			<= '0';
      	gt_txpcsreset_async			<= '0';
        gt_reset              		<= '0';
        sys_reset             		<= '0';
        txoutclksel_async			<= "101";
        rxoutclksel_async			<= "010";
     	gt_loopback_in_rg     		<= (others => '0');
		tx_diff_ctrl				<= "11000110001100011000";
		txpostcursor				<= "00000000000000000000";
		txprecursor					<= "00000000000000000000";
		gt_rxprbscntreset_async		<= '0';
		gt_txprbssel_async			<= "0000";
		gt_rxprbssel_async			<= "0000";
		gt_txprbsforceerr_async		<= '0';
		ctl_tx_pause_req        	<= (others => '0');
		ctl_tx_resend_pause     	<= '0';
        ctl_tx_send_lfi_reg			<= '0' ;
        ctl_tx_send_idle_reg		<= '0' ;
      elsif rising_edge(usr_clk) then
--      	gt_reset              		<= '0';
--      	sys_reset             		<= '0';
      		gt_txprbsforceerr_async		<= '0';
        if usr_wen = '1'  then
				if usr_func_wr(MAC100G_SERDES_reset 			+ addr_offset_100G_eth) = '1' then
					gt_reset               	 	<= usr_data_wr(0);
					sys_reset               	<= usr_data_wr(1);
					gt_rxpcsreset_async        	<= usr_data_wr(2);
					gt_txpcsreset_async        	<= usr_data_wr(3);
					counter_reset				<= usr_data_wr(31);
				end if;

				if usr_func_wr(MAC100G_SERDES_voltage 			+ addr_offset_100G_eth) = '1' then
					--lane 0
					tx_diff_ctrl(4 downto 0)	<= usr_data_wr(4 downto 0);
					txpostcursor(4 downto 0)	<= usr_data_wr(9 downto 5);
					txprecursor(4 downto 0)		<= usr_data_wr(14 downto 10);
					--lane 1
					tx_diff_ctrl(9 downto 5)	<= usr_data_wr(20 downto 16);
					txpostcursor(9 downto 5)	<= usr_data_wr(25 downto 21);
					txprecursor(9 downto 5)		<= usr_data_wr(30 downto 26);
					--lane 2
					tx_diff_ctrl(14 downto 10)	<= usr_data_wr(36 downto 32);
					txpostcursor(14 downto 10)	<= usr_data_wr(41 downto 37);
					txprecursor(14 downto 10)	<= usr_data_wr(46 downto 42);
					--lane 3
					tx_diff_ctrl(19 downto 15)	<= usr_data_wr(52 downto 48);
					txpostcursor(19 downto 15)	<= usr_data_wr(57 downto 53);
					txprecursor(19 downto 15)	<= usr_data_wr(62 downto 58);

				end if;

				if usr_func_wr(MAC100G_SERDES_loopback 			+ addr_offset_100G_eth) = '1' then
					gt_loopback_in_rg        	<= usr_data_wr(2 downto 0);
					txoutclksel_async			<= usr_data_wr(6 downto 4);
					rxoutclksel_async			<= usr_data_wr(10 downto 8);

                    ctl_tx_send_lfi_reg			<= usr_data_wr(29);
                    ctl_tx_send_idle_reg		<= usr_data_wr(30);
				end if;

				if usr_func_wr(MAC100G_SERDES_PRBS_TEST 		+ addr_offset_100G_eth) = '1' then
					gt_txprbssel_async        	<= usr_data_wr(3 downto 0);
					gt_rxprbssel_async			<= usr_data_wr(7 downto 4);
					gt_rxprbscntreset_async		<= usr_data_wr(28);
					gt_txprbsforceerr_async		<= usr_data_wr(31);

				end if;

				if usr_func_wr(MAC100G_SERDES_TX_pause_frame_req + addr_offset_100G_eth) = '1' then
					ctl_tx_pause_req       	 	<= usr_data_wr(8 downto 0);
					ctl_tx_resend_pause     	<= usr_data_wr(16);
				end if;

			end if;
      end if;
end process;

-- register which are not reset
process(usr_clk)
  begin
      if rising_edge(usr_clk) then
          gtx_ch_drp_rd			<= '0';
          gtx_ch_drp_we			<= '0';

       	 if usr_wen = '1'  then

				if usr_func_wr(MAC100G_SERDES_DRP_cmd			 	+ addr_offset_100G_eth) = '1' then
					drp_en(0)              	<= usr_data_wr(31);
					drp_we(0)              	<= usr_data_wr(30);
					drp_reset              	<= usr_data_wr(28);
					drp_addr               	<= usr_data_wr(25 downto 16);
					drp_di                 	<= usr_data_wr(15 downto 0);
				end if;

				if usr_func_wr(MAC100G_SERDES_ChDRP_cmd 			+ addr_offset_100G_eth) = '1' then
					gtx_ch_drp_en      		<= usr_data_wr(31 downto 28);--- Ena access DRP
					gtx_ch_drp_we    		<= usr_data_wr(27);          --- Write Ena DRP
					gtx_ch_drp_rd    		<= usr_data_wr(26);          --- Read Ena DRP
					gtx_ch_drp_addr    		<= usr_data_wr(25 downto 16);
					gtx_ch_drp_di      		<= usr_data_wr(15 downto 0);
				end if;

			end if;
      end if;
end process;

-- xpm_cdc_array_single_inst:xpm_cdc_array_single
-- generic map (
-- DEST_SYNC_FF 	=> 2, -- DECIMAL; range: 2-10
-- INIT_SYNC_FF 	=> 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages SRC_INPUT_REG => 1, -- DECIMAL; 0=do not register input, 1=register input
-- WIDTH 			=> 4 -- DECIMAL; range: 1-1024
-- )
-- port map (
-- dest_out => gtx_ch_drp_en_sync, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
-- dest_clk => DRP_clock, -- 1-bit input: Clock signal for the destination clock domain.
-- src_clk => usr_clk, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
-- src_in 	=> gtx_ch_drp_en 	-- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
					-- domain. It is assumed that each bit of the array is unrelated to the others.
					-- This is reflected in the constraints applied to this macro. To transfer a binary -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro instead.
-- );

--******************************************************************************************************
--		PCIe read data

process(usr_clk)
begin
	if rising_edge(usr_clk) then
	   data_in_rg                                  	         <= (others => '0');

--			if    usr_func_rd(100) = '1' then
--				data_in_rg(31 downto 0)                      <= inc_tx_total_good_packets;
--			elsif usr_func_rd(101) = '1' then
--				data_in_rg(31 downto 0)                      <= stat_rx_total_bytes_cnt;
			if usr_func_rd(MAC100G_SERDES_status 		+ addr_offset_100G_eth) = '1' then
				data_in_rg(0)                                <= stat_rx_aligned 	;
				data_in_rg(1)                                <= stat_rx_aligned_err;
				data_in_rg(2)                                <= init_done_out;
				data_in_rg(3)                                <= SM_link_out;

				data_in_rg(4 downto 2)                       <= stat_rx_bad_code ;
				data_in_rg(8 downto 6)                       <= stat_rx_bad_fcs;
				data_in_rg(9)                                <= powergood ;
				data_in_rg(10)                               <= stat_rx_bad_preamble ;
				data_in_rg(11)                               <= stat_rx_bad_sfd 		;

				data_in_rg(12)                               <= stat_rx_internal_local_fault;
				data_in_rg(13)                               <= stat_rx_received_local_fault;
				data_in_rg(14)                               <= stat_tx_local_fault;

				data_in_rg(16)			                     <= stat_rx_local_fault   ;
				data_in_rg(17)			                     <= stat_rx_misaligned    ;
				data_in_rg(18)			                     <= stat_rx_status        ;

				data_in_rg(20)                               <= gt_reset_tx_done;
				data_in_rg(24)                               <= gt_reset_rx_done;



			elsif usr_func_rd(MAC100G_SERDES_statusa 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(0)		             			<= powergood ;

			elsif usr_func_rd(MAC100G_SERDES_reset 		+ addr_offset_100G_eth) = '1' then
				data_in_rg(0) 			                     <= gt_reset;
				data_in_rg(1)                                <= sys_reset;
				data_in_rg(2)                                <= gt_rxpcsreset_async;
				data_in_rg(3)                                <= gt_txpcsreset_async;
				data_in_rg(31)                     			 <= counter_reset;

			elsif usr_func_rd(MAC100G_SERDES_loopback 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(2 downto 0)				         <= gt_loopback_in_rg;
				data_in_rg(6 downto 4)				         <= txoutclksel_async;
				data_in_rg(10 downto 8)				         <= rxoutclksel_async;

                data_in_rg(28)                               <= ctl_tx_send_rfi;
                data_in_rg(29)                               <= ctl_tx_send_lfi_reg;
                data_in_rg(30)                               <= ctl_tx_send_idle_reg;

			elsif usr_func_rd(MAC100G_SERDES_voltage 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(4 downto 0)				         <= tx_diff_ctrl(4 downto 0);
				data_in_rg(9 downto 5)				         <= txpostcursor(4 downto 0);
				data_in_rg(14 downto 10)			         <= txprecursor(4 downto 0);

				data_in_rg(20 downto 16)			         <= tx_diff_ctrl(9 downto 5);
				data_in_rg(25 downto 21)			         <= txpostcursor(9 downto 5);
				data_in_rg(30 downto 26)			         <= txprecursor(9 downto 5);

				data_in_rg(36 downto 32)			         <= tx_diff_ctrl(14 downto 10);
				data_in_rg(41 downto 37)			         <= txpostcursor(14 downto 10);
				data_in_rg(46 downto 42)			         <= txprecursor(14 downto 10);

				data_in_rg(52 downto 48)			         <= tx_diff_ctrl(19 downto 15);
				data_in_rg(57 downto 53)			         <= txpostcursor(19 downto 15);
				data_in_rg(62 downto 58)			         <= txprecursor(19 downto 15);


			elsif usr_func_rd(MAC100G_SERDES_PRBS_TEST 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(3 downto 0)				         <= gt_txprbssel_async;
				data_in_rg(7 downto 4)				         <= gt_rxprbssel_async;
				data_in_rg(11 downto 8)				         <= gt_txresetdone;
				data_in_rg(19 downto 16)		       		 <= gt_rxprbserr;
				data_in_rg(27 downto 24)			         <= rxprbslocked_out;
				data_in_rg(28)							     <= gt_rxprbscntreset_async;


			elsif usr_func_rd(MAC100G_SERDES_DRP_cmd 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(25 downto 16)			         <= drp_addr;
				data_in_rg(15 downto 0)				         <= drp_di;

			elsif usr_func_rd(MAC100G_SERDES_DRP_dt 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(16)							     <= drp_done;
				data_in_rg(15 downto 0)				         <= drp_data;

			elsif usr_func_rd(MAC100G_SERDES_ChDRP_cmd 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(25 downto 16)                     <= gtx_ch_drp_addr;
				data_in_rg(15 downto 0)                      <= gtx_ch_drp_di;

			elsif usr_func_rd(MAC100G_SERDES_ChDRP_dt 	+ addr_offset_100G_eth) = '1' then
				data_in_rg 				                     <= gtx_ch_drp_do;

			elsif usr_func_rd(MAC100G_SERDES_ChDRP_st 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(3 downto 0)                     <= gtx_ch_drp_done;

			elsif usr_func_rd(MAC100G_SERDES_TX_status 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(0)					             <= tx_ovfout_mem ;
				data_in_rg(1)					             <= tx_rdyout;
				data_in_rg(2)					             <= tx_unfout_mem;
				data_in_rg(15 downto 8)					 	 <= gt_txbufstatus;

			elsif usr_func_rd(MAC100G_SERDES_RX_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg(15 downto 0)			             <= rx_crc_error;
				data_in_rg(31 downto 16)			         <= stat_rx_pause_counter;

			elsif usr_func_rd(MAC100G_SERDES_TX_error_1 + addr_offset_100G_eth) = '1' then
				data_in_rg(15 downto 0)			             <= stat_tx_local_fault_cnt;
				data_in_rg(31 downto 16)			         <= stat_tx_bad_fcs_cnt;

			elsif usr_func_rd(MAC100G_SERDES_TX_error_2 + addr_offset_100G_eth) = '1' then
				data_in_rg(15 downto 0)			             <= stat_tx_broadcast_cnt;
				data_in_rg(31 downto 16)			         <= stat_tx_frame_error_cnt;

			elsif usr_func_rd(MAC100G_rx_frame_l0_error + addr_offset_100G_eth) = '1' then
				data_in_rg						             <= rx_frame_l0_err_cnt;
			elsif usr_func_rd(MAC100G_rx_frame_l1_error + addr_offset_100G_eth) = '1' then
				data_in_rg						             <= rx_frame_l1_err_cnt;
			elsif usr_func_rd(MAC100G_rx_frame_l2_error + addr_offset_100G_eth) = '1' then
				data_in_rg						             <= rx_frame_l2_err_cnt;
			elsif usr_func_rd(MAC100G_rx_frame_l3_error + addr_offset_100G_eth) = '1' then
				data_in_rg						             <= rx_frame_l3_err_cnt;

			elsif usr_func_rd(MAC100G_rx_l0_bip_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg 				                     <= stat_rx_bip_err_cnt(0);
			elsif usr_func_rd(MAC100G_rx_l1_bip_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg									 <= stat_rx_bip_err_cnt(1);
			elsif usr_func_rd(MAC100G_rx_l2_bip_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg 				                   <= stat_rx_bip_err_cnt(2);
			elsif usr_func_rd(MAC100G_rx_l3_bip_error 	+ addr_offset_100G_eth) = '1' then
				data_in_rg 				                     <= stat_rx_bip_err_cnt(3);
			elsif usr_func_rd(MAC100G_Clock_rec_measure	+ addr_offset_100G_eth) = '1' then
				data_in_rg(31 downto 0)                      <= measure_MGT_clock;
			elsif usr_func_rd(MAC100G_test 				+ addr_offset_100G_eth) = '1' then
				data_in_rg 				                     <= x"7878787812121212";

			end if;

	end if;
end process;

usr_data_rd           <=  data_in_rg;


resync_reset_i5:resetp_resync
port map(
	aresetp				=>	counter_reset,
	clock				=>	rx_clk,
	Resetp_sync			=>	counter_reset_resync
	);

resync_reset_i6:resetp_resync
port map(
	aresetp				=>	counter_reset,
	clock				=>	gt_txusrclk2,
	Resetp_sync			=>	counter_reset_sync_txclk
	);

process(counter_reset_resync,rx_clk)
begin
	if counter_reset_resync = '1' then
		stat_rx_packet_size	<= (others => '0');
		stat_rx_packet_cnt	<= (others => '0');
	elsif rising_edge(rx_clk) then
		if stat_rx_packet /= x"000" then
			stat_rx_packet_size <= stat_rx_packet;
			stat_rx_packet_cnt <= stat_rx_packet_cnt + '1';
		end if;
	end if;
end process;

process(counter_reset_resync,rx_clk)
begin
	if counter_reset_resync = '1' then
		stat_rx_packet_small_cnt	<= (others => '0');
	elsif rising_edge(rx_clk) then
		if stat_rx_packet_small /= "000" then
			stat_rx_packet_small_cnt <= stat_rx_packet_small_cnt + '1';
		end if;

	end if;
end process;

process(counter_reset_resync,rx_clk)
begin
	if counter_reset_resync = '1' then
		inc_tx_total_good_packets	<= (others => '0');
	elsif rising_edge(rx_clk) then
		if inc_rx_total_good_packets_ena = '1' then
			inc_tx_total_good_packets	<= inc_tx_total_good_packets + '1';
		end if;
	end if;
end process;

process(counter_reset_resync,rx_clk)
begin
	if counter_reset_resync = '1' then
		stat_rx_total_bytes_cnt	<= (others => '0');
	elsif rising_edge(rx_clk) then
		if stat_rx_total_bytes /= "0000000" then
			stat_rx_total_bytes_cnt	<= stat_rx_total_bytes_cnt + '1';
		end if;

	end if;
end process;


-- Statistic counter
process(counter_reset_resync,rx_clk)
begin
	if counter_reset_resync = '1' then
		rx_crc_error			<= (others => '0');
		stat_rx_pause_counter	<= (others => '0');
		rx_frame_l0_err_cnt		<= (others => '0');
		rx_frame_l1_err_cnt		<= (others => '0');
		rx_frame_l2_err_cnt		<= (others => '0');
		rx_frame_l3_err_cnt		<= (others => '0');

		stat_rx_bip_err_cnt(0)	<= (others => '0');
		stat_rx_bip_err_cnt(1)	<= (others => '0');
		stat_rx_bip_err_cnt(2)	<= (others => '0');
		stat_rx_bip_err_cnt(3)	<= (others => '0');

	elsif rising_edge(rx_clk) then
		if stat_rx_bad_fcs  /= "000" then
			rx_crc_error	<= rx_crc_error + '1';
		end if;

		if rx_frame_l0_err /= "00" and rx_frame_l0_err_val = '1' then
			rx_frame_l0_err_cnt <= rx_frame_l0_err_cnt + '1';
		end if;

		if rx_frame_l1_err /= "00" and rx_frame_l1_err_val = '1' then
			rx_frame_l1_err_cnt <= rx_frame_l1_err_cnt + '1';
		end if;

		if rx_frame_l2_err /= "00" and rx_frame_l2_err_val = '1' then
			rx_frame_l2_err_cnt <= rx_frame_l2_err_cnt + '1';
		end if;

		if rx_frame_l3_err /= "00" and rx_frame_l3_err_val = '1' then
			rx_frame_l3_err_cnt <= rx_frame_l3_err_cnt + '1';
		end if;

		if stat_rx_bip_err(0) = '1' then
			stat_rx_bip_err_cnt(0) <= stat_rx_bip_err_cnt(0) + '1';
		end if;

		if stat_rx_bip_err(1) = '1' then
			stat_rx_bip_err_cnt(1) <= stat_rx_bip_err_cnt(1) + '1';
		end if;

		if stat_rx_bip_err(2) = '1' then
			stat_rx_bip_err_cnt(2) <= stat_rx_bip_err_cnt(2) + '1';
		end if;

		if stat_rx_bip_err(3) = '1' then
			stat_rx_bip_err_cnt(3) <= stat_rx_bip_err_cnt(3) + '1';
		end if;

		if stat_rx_pause_req /= "000000000" then
			stat_rx_pause_counter <= stat_rx_pause_counter + '1';
		end if;
	end if;
end process;

process(counter_reset_sync_txclk,gt_txusrclk2)
begin
	if counter_reset_sync_txclk = '1' then
		stat_tx_local_fault_cnt       <= (others => '0');
		stat_tx_bad_fcs_cnt           <= (others => '0');
		stat_tx_broadcast_cnt         <= (others => '0');
		stat_tx_frame_error_cnt       <= (others => '0');
		tx_ovfout_mem			      <= '0';
		tx_unfout_mem			      <= '0';
	elsif rising_edge(gt_txusrclk2) then
		if stat_tx_local_fault = '1'then
			stat_tx_local_fault_cnt	<= stat_tx_local_fault_cnt + '1';
		end if;

		if stat_tx_bad_fcs = '1'then
			stat_tx_bad_fcs_cnt		<= stat_tx_bad_fcs_cnt + '1';
		end if;

		if stat_tx_broadcast = '1'then
			stat_tx_broadcast_cnt	<= stat_tx_broadcast_cnt + '1';
		end if;

		if stat_tx_frame_error = '1'then
			stat_tx_frame_error_cnt	<= stat_tx_frame_error_cnt + '1';
		end if;

		if tx_ovfout = '1' then
			tx_ovfout_mem <= '1';
		end if;

		if tx_unfout = '1' then
			tx_unfout_mem <= '1';
		end if;

	end if;
end process;

--------------------------------------------------------------
-- Monitoring Clock TX (RX is not tested)
--------------------------------------------------------------

freq_measure_clock_tx : entity work.freq_measure_base
	generic map (
		freq_used   => x"000186A0"	-- set for 100 Mhz ref clock (base_clk)
	)
	port map (
		resetn      => '1',
		sysclk      => gt_rxusrclk2,
		base_clk    => init_clk,
		frequency   => measure_MGT_clock
	);
end Behavioral;
