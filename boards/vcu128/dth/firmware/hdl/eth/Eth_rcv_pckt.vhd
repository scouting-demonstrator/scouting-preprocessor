--*****************************************************************
-- Dominique Gigi
--
-- Decoder V1
-- This first version decode only one start per 512 bits
--
--
--*****************************************************************
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.interface.all;


--ENTITY-----------------------------------------------------------
entity Eth_rcv_pckt is
	port
	(
		reset_in_p							: in std_logic;
		RX_lbus_in							: in rx_user_ift;
		rx_lbus_clk							: in std_logic;
		-- General information
		MAC_home							: in std_logic_vector(47 downto 0);
		IP_home								: in std_logic_vector(31 downto 0);
		IP_Dest								: in std_logic_vector(31 downto 0); 	-- IP for the ARP request   SERVER IP (used for ARP request  IP Destination)
		PORT_Home							: in TCP_port_number;
		PORT_Host							: in TCP_port_number;
		IP_GateWay							: in std_logic_vector(31 downto 0);		-- IP for the ARP request   GATEWAY IP (used for ARP request  IP gateway)

		-- ARP parameters
		request_ARP							: in std_logic;								-- The request will reset the counter for ARP probe
		ARP_MAC_Dest      					: out std_logic_vector(47 downto 0);
		ARP_MAC_GateWay  					: out std_logic_vector(47 downto 0);
		ARP_check_MAC_src 					: out std_logic;
		ARP_check_IP_src  					: out std_logic;
		ARP_MAC_error    					: out std_logic_vector(47 downto 0);
		ARP_MAC_rcvOK 						: out std_logic;
		ARP_MAC_gwOK  						: out std_logic;
		ARP_IP								: out std_logic_vector(31 downto 0);
		ARP_MAC								: out std_logic_vector(47 downto 0);
		req_ARP_reply 						: out std_logic;
		ARP_reply     						: out std_logic;	-- reply on our ARP request
		valid_ARP_packet					: out std_logic;
		--PING parameters
		req_icmp_prb						: out std_logic;
		PING_HD_empty						: in std_logic;
		ping_last_dt						: out std_logic;
		wr_PING_data						: out std_logic;
		data_ping							: out std_logic_vector(63 downto 0);
		Reset_PING_FIFO						: out std_logic;
		PING_MAC_dst						: out std_logic_vector(47 downto 0);
		PING_IP_dst							: out std_logic_vector(31 downto 0);
		received_Ping_reply					: out std_logic;

		--TCP ACK parameters
		PORTx_TCP_MSS_default				: in std_logic_vector(15 downto 0);
		PORTx_PORT_rcv						: out std_logic_vector(11 downto 0);
		PORTx_ACK_number					: out std_logic_vector(31 downto 0);
		PORTx_SEQ_number					: out std_logic_vector(31 downto 0);
		PORTx_TCP_flag						: out std_logic_vector( 7 downto 0);
		PORTx_TCP_win						: out std_logic_vector(15 downto 0);
		PORTx_TCP_len						: out std_logic_vector(15 downto 0);
		PORTx_new_Scale                     : out std_logic_vector(7 downto 0);
        PORTx_update_Scale                  : out std_logic;
        PORTx_new_MMS                       : out std_logic_vector(15 downto 0);
        PORTx_update_MMS                    : out std_logic;
		PORTx_latch_ack						: out std_logic;


		counter_pause_frame					: out std_logic_vector(31 downto 0);
		delay_pause_frame					: out std_logic_vector(15 downto 0);
		req_pause_frame						: out std_logic;

		-- Conflict IP & MAC
		conflict_MAC_counter				: out std_logic_vector(31 downto 0);
		conflict_IP_counter					: out std_logic_vector(31 downto 0);
		conflict_MAC_value					: out std_logic_vector(47 downto 0);

		--100G  transmit clock
		clock_out							: in std_logic

	 );
end Eth_rcv_pckt;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of Eth_rcv_pckt is

-- registers pipe for packet received from the MAC ethernet
signal RX_lbus_rg								: rx_user_ift;

COMPONENT ETH_rcv_buffer
PORT (
		srst 				: IN STD_LOGIC;
		wr_clk 				: IN STD_LOGIC;
		rd_clk				: IN STD_LOGIC;
		din 				: IN STD_LOGIC_VECTOR(135 DOWNTO 0);
		wr_en 				: IN STD_LOGIC;
		rd_en 				: IN STD_LOGIC;
		dout 				: OUT STD_LOGIC_VECTOR(135 DOWNTO 0);
		full 				: OUT STD_LOGIC;
		empty 				: OUT STD_LOGIC;
		prog_full 			: OUT STD_LOGIC;
		wr_rst_busy 		: OUT STD_LOGIC;
		rd_rst_busy 		: OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic;
	Resetp_sync			: out std_logic
	);
end COMPONENT;

signal reset_clkout_p							: std_logic;
signal reset_clkin_p							: std_logic;

-- signal resync to MAC TX_clock
signal rx_lbus_cell								: rx_user_ift;
signal rx_lbus_pipe_A							: rx_user_ift;
signal Full_indiv								: std_logic_vector(3 downto 0);
signal Empty_indiv								: std_logic_vector(3 downto 0);
signal read_buffer_cell							: std_logic_vector(3 downto 0);

-- state machine to decode the Ethernet packet received
type packet_decode_type is (
							Idle,
							ARP_decode,
							TCP_decode,
							ICMP_decode,
							PING_word0,
							PING_word1,
							read_word,
							wait_end
							);
signal packet_decode:packet_decode_type;

-- specify the type of packet recveived
signal ARP_store_val 							: std_logic;
signal TCP_store_val 							: std_logic;
signal ICMP_store_val							: std_logic;

signal packet_valid 							: std_logic;
signal packet_error								: std_logic;

-- registers for ARP packets
signal MAC_Dest_rg     							: std_logic_vector(47 downto 0);
signal MAC_GateWay_rg  							: std_logic_vector(47 downto 0);
signal check_MAC_src_rg							: std_logic;
signal check_IP_src_rg 							: std_logic;
signal MAC_error_rg    							: std_logic_vector(47 downto 0);
signal ARP_MAC_src_rg							: std_logic_vector(47 downto 0);
signal ARP_IP_src_rg							: std_logic_vector(31 downto 0);
signal MAC_rcvOK_reg							: std_logic;
signal MAC_gwOK_reg 							: std_logic;
signal req_ARP_reply_rg							: std_logic;
signal ARP_reply_rg    							: std_logic;
signal valid_ARP_packet_rg						: std_logic;

--Registers for PING packets
signal l_bus_word								: std_logic_vector(3 downto 0);
signal PING_tmp_data							: std_logic_vector(47 downto 0);
signal PING_data_rg								: std_logic_vector(63 downto 0);
signal PING_Last_dt_rg							: std_logic;
signal req_icmp_prb_rg							: std_logic;
signal wr_data_ping								: std_logic;
signal Reset_PING_FIFO_rg						: std_logic;
signal PING_MAC_dst_rg							: std_logic_vector(47 downto 0);
signal PING_IP_dst_rg							: std_logic_vector(31 downto 0);
signal identifier_rg							: std_logic_vector(15 downto 0);
signal first_word_change_CheckSum				: std_logic;
signal received_Ping_reply_cell					: std_logic;

-- Registers for TCP Ack packets
signal Px_PORT_Host_reg							: std_logic_vector(15 downto 0);
signal Px_PORT_Home_reg							: std_logic_vector(15 downto 0);
signal TCP_port_number_reg						: std_logic_vector(11 downto 0);
signal Px_ACK_number_reg					   	: std_logic_vector(31 downto 0);
signal Px_SEQ_number_reg					   	: std_logic_vector(31 downto 0);
signal Px_TCP_flag_reg							: std_logic_vector( 7 downto 0);
signal TCP_win_tmp						   		: std_logic_vector(15 downto 0);
signal Px_TCP_len_reg						   	: std_logic_vector(15 downto 0);
signal Px_option_SCALE							: std_logic_vector( 7 downto 0);
signal Px_option_MMS							: std_logic_vector(15 downto 0);
signal update_MSS								: std_logic;
signal update_Scale								: std_logic;
signal no_options 	 							: std_logic;
signal latch_ack_reg						   	: std_logic_vector( 1 downto 0);


signal latch_MAC_Dest_rg						: std_logic;
signal latch_MAC_GateWay_rg						: std_logic;
signal latch_MAC_error_rg						: std_logic;
signal latch_TCP_val_rg							: std_logic;
signal latch_ICMP_decode						: std_logic;
signal latch_ARP_decode							: std_logic;
signal latch_Px_option_MMS_rg					: std_logic_vector(2 downto 0);
signal latch_Px_option_SCALE_rg					: std_logic_vector(2 downto 0);

-- enable 1 clock on 2 (decrease time of decoding)
signal ena_clock_half							: std_logic := '0';

signal latch_pause_frame						: std_logic;
signal counter_pause_frame_reg					: std_logic_vector(31 downto 0);
signal delay_pause_frame_reg					: std_logic_vector(15 downto 0);

signal conflict_MAC_counter_reg					: std_logic_vector(31 downto 0);
signal conflict_IP_counter_reg					: std_logic_vector(31 downto 0);
signal conflict_MAC_value_reg					: std_logic_vector(47 downto 0);


attribute mark_debug : string;
--attribute mark_debug of Px_option_SCALE: signal is "true";
--attribute mark_debug of update_Scale   : signal is "true";
--attribute mark_debug of Px_option_MMS  : signal is "true";
--attribute mark_debug of update_MSS     : signal is "true";
--attribute mark_debug of TCP_win_tmp    : signal is "true";
--attribute mark_debug of latch_ack_reg  : signal is "true";
--attribute mark_debug of rx_lbus_pipe_A  : signal is "true";
--attribute mark_debug of packet_decode  : signal is "true";
--attribute mark_debug of latch_Px_option_SCALE_rg  : signal is "true";
--attribute mark_debug of latch_Px_option_MMS_rg  : signal is "true";


--*****************************************************************
--      Code start HERE
--*****************************************************************
 begin

--resynchronize General reset to MAC 100G rx_clock
reset_resync_i1:resetp_resync
port map(
	aresetp				=> reset_in_p,
	clock				=> rx_lbus_clk,
	Resetp_sync			=> reset_clkin_p
	);

reset_clkout_p			<= reset_in_p;

--register RX bus
process(rx_lbus_clk)
begin
	if rising_edge(rx_lbus_clk) then
		RX_lbus_rg <= RX_lbus_in;
	end if;
end process;

 -- record the data packet
 -- will be change later with sible clock domain
 -- First Word Fall Through
rcv_buffer:FOR J in 0 to 3 generate
	i0:ETH_rcv_buffer
	  PORT map(
		 srst 						=>	reset_clkin_p				,
		 wr_clk 					=>	rx_lbus_clk					,
		 wr_en 						=>	RX_lbus_rg(0).data_ena	  	,-- record all 4 128-bit bus together
		 din(127 downto 0) 			=>	RX_lbus_rg(J).data_bus	  	,
		 din(128)              	 	=>	RX_lbus_rg(J).sopckt  	  	,
		 din(129)               	=>	RX_lbus_rg(J).data_ena	  	,
		 din(130)               	=>	RX_lbus_rg(J).eopckt		,
		 din(131)               	=>	RX_lbus_rg(J).err_pckt	  	,
		 din(135 downto 132)    	=>	RX_lbus_rg(J).empty_pckt  	,
		 prog_full					=>	Full_indiv(J)				,

		 rd_clk 					=>	clock_out				  	,
		 rd_en						=>	read_buffer_cell(J)		  	,
		 dout(127 downto 0) 		=>	rx_lbus_cell(J).data_bus  	,
		 dout(128)              	=>	rx_lbus_cell(J).sopckt    	,
		 dout(129)              	=>	rx_lbus_cell(J).data_ena  	,
		 dout(130)              	=>	rx_lbus_cell(J).eopckt	  	,
		 dout(131)              	=>	rx_lbus_cell(J).err_pckt  	,
		 dout(135 downto 132)   	=>	rx_lbus_cell(J).empty_pckt	,
		 empty 						=>	Empty_indiv(J)
	);
end generate;


read_buffer_cell(0) 		<= '1' when ((packet_decode = read_word) or (packet_decode = PING_word0 and l_bus_word(3) = '1')) and ena_clock_half = '1' else '0';
read_buffer_cell(1) 		<= '1' when ((packet_decode = read_word) or (packet_decode = PING_word0 and l_bus_word(3) = '1')) and ena_clock_half = '1' else '0';
read_buffer_cell(2) 		<= '1' when ((packet_decode = read_word) or (packet_decode = PING_word0 and l_bus_word(3) = '1')) and ena_clock_half = '1' else '0';
read_buffer_cell(3) 		<= '1' when ((packet_decode = read_word) or (packet_decode = PING_word0 and l_bus_word(3) = '1')) and ena_clock_half = '1' else '0';


process(clock_out)
begin
	if rising_edge(clock_out) then
		rx_lbus_pipe_A	<= rx_lbus_cell;
		--this signal is used to decode the packet receiver using a "half clock period"
		ena_clock_half  <= not(ena_clock_half);
	end if;
end process;


--////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
--||||||||||| STATE MACHINE TO DECODE ETHERNET PACKET |||||||||||||||
--\\\\\\\\\\\\\\\\\\\\\\\\\\\\//////////////////////////////////////
work_structure:process(clock_out,reset_clkout_p)
begin
	if reset_clkout_p = '1'  then
		packet_decode 		<= Idle;

	elsif rising_edge(clock_out) then
		latch_pause_frame			<= '0';
		received_Ping_reply_cell	<= '0';
		if ena_clock_half = '1'then
			Case packet_decode is
				when Idle =>
					if Empty_indiv = "0000"  then
						-- check the broadcast packet or our own MAC address
						if (rx_lbus_cell(0).data_bus(127 downto 80) = x"FFFFFFFFFFFF" or  rx_lbus_cell(0).data_bus(127 downto 80) = MAC_home) then
						--  IP packet
							if 	rx_lbus_cell(0).data_bus(31 downto 16) = x"0800" then
						--- we received a ICMP/Ping packet   (protocol = x01)
								if 	rx_lbus_cell(1).data_bus(71 downto 64) = x"01" then
						-- echo request
									if  rx_lbus_cell(2).data_bus(111 downto 96) = x"0800" then
										packet_decode 					<= ICMP_decode;
						-- receive a echo on our PING request
									else
										received_Ping_reply_cell		<= '1';
										packet_decode 					<= read_word;
									end	if;
						-- we received a TCP packet    (protocol = x06)
								elsif rx_lbus_cell(1).data_bus(71 downto 64) = x"06" and rx_lbus_cell(1).data_bus(15 downto 0) = IP_Home(31 downto 16) and rx_lbus_cell(2).data_bus(127 downto 112) = IP_Home(15 downto 0) then	--- we received a TCP packet
									packet_decode <= TCP_decode;
						-- we don't decode other kind of packet (read what we received)
								else
									packet_decode <= read_word;
								end if;
						-- ARP packet
							elsif rx_lbus_cell(0).data_bus(31 downto 16) = x"0806" and rx_lbus_cell(2).data_bus(79 downto 48) = IP_Home then		--- we received a ARP packet
								packet_decode 	<= ARP_decode;
						-- we don't decode other kind of packet (read what is received)
							else
								packet_decode 	<= read_word;
							end if;
						-- check if we receive PAUSE FRAME
						elsif rx_lbus_cell(0).data_bus(127 downto 80) = x"0180C2000001" then
							if 	rx_lbus_cell(0).data_bus(31 downto 00) = x"88080001" then
								latch_pause_frame	<= '1';
								packet_decode	 	<= read_word;
							else
								packet_decode 		<= read_word;
							end if;
						-- we receive something not for US, read the data (trash them)
						else
							packet_decode 		<= read_word;
						end if;
					end if;

				when ARP_decode =>
					packet_decode 	<= read_word;

				when TCP_decode =>
					packet_decode 	<= read_word;

				when ICMP_decode =>
					packet_decode 	<= PING_word0;

				when PING_word0 =>
					if  (l_bus_word(0) = '1' and rx_lbus_cell(0).data_ena = '1' and rx_lbus_cell(0).eopckt = '1' and rx_lbus_cell(0).empty_pckt = x"6") or
						(l_bus_word(1) = '1' and rx_lbus_cell(1).data_ena = '1' and rx_lbus_cell(1).eopckt = '1' and rx_lbus_cell(1).empty_pckt = x"6") or
						(l_bus_word(2) = '1' and rx_lbus_cell(2).data_ena = '1' and rx_lbus_cell(2).eopckt = '1' and rx_lbus_cell(2).empty_pckt = x"6") or
						(l_bus_word(3) = '1' and rx_lbus_cell(3).data_ena = '1' and rx_lbus_cell(3).eopckt = '1' and rx_lbus_cell(3).empty_pckt = x"6") then
						packet_decode 	<= read_word;
					else
						packet_decode 	<= PING_word1;
					end if;

				when PING_word1 =>
					if  (l_bus_word(0) = '1' and rx_lbus_cell(0).data_ena = '1' and rx_lbus_cell(0).eopckt = '1' and rx_lbus_cell(0).empty_pckt = x"E") or
						(l_bus_word(1) = '1' and rx_lbus_cell(1).data_ena = '1' and rx_lbus_cell(1).eopckt = '1' and rx_lbus_cell(1).empty_pckt = x"E") or
						(l_bus_word(2) = '1' and rx_lbus_cell(2).data_ena = '1' and rx_lbus_cell(2).eopckt = '1' and rx_lbus_cell(2).empty_pckt = x"E") or
						(l_bus_word(3) = '1' and rx_lbus_cell(3).data_ena = '1' and rx_lbus_cell(3).eopckt = '1' and rx_lbus_cell(3).empty_pckt = x"E") then
						packet_decode 	<= read_word;
					else
						packet_decode 	<= PING_word0;
					end if;

				when read_word =>
					if  (rx_lbus_cell(0).data_ena = '1' and rx_lbus_cell(0).eopckt = '1') or
						(rx_lbus_cell(1).data_ena = '1' and rx_lbus_cell(1).eopckt = '1') or
						(rx_lbus_cell(2).data_ena = '1' and rx_lbus_cell(2).eopckt = '1') or
						(rx_lbus_cell(3).data_ena = '1' and rx_lbus_cell(3).eopckt = '1') then
						packet_decode 	<= Idle;
					end if;

				when others =>
					packet_decode 		<= Idle;

			end case;
		end if;
	end if;
end process;

process(clock_out,reset_clkout_p)
begin
	if reset_clkout_p = '1'  then
		ARP_store_val 		<= '0';
		TCP_store_val 		<= '0';
		ICMP_store_val 		<= '0';
	elsif rising_edge(clock_out) then

		if packet_decode = ARP_decode then
			ARP_store_val 	<= '1';
		elsif packet_decode =Idle  then
			ARP_store_val 	<= '0';
		end if;

		if packet_decode = TCP_decode then
			TCP_store_val 	<= '1';
		elsif  packet_decode =Idle  then
			TCP_store_val 	<= '0';
		end if;

		if packet_decode = ICMP_decode then
			ICMP_store_val 	<= '1';
		elsif  packet_decode =Idle  then
			ICMP_store_val 	<= '0';
		end if;
	end if;
end process;

--***********************************************************************
-- validate the packet
process(clock_out)
begin
	if rising_edge(clock_out) then
		packet_valid	<= '0';
		packet_error	<= '0';
		if packet_decode = read_word  and  ena_clock_half = '1' then
			if  (rx_lbus_cell(0).data_ena = '1' and rx_lbus_cell(0).eopckt = '1' and rx_lbus_cell(0).err_pckt = '0') or
				(rx_lbus_cell(1).data_ena = '1' and rx_lbus_cell(1).eopckt = '1' and rx_lbus_cell(1).err_pckt = '0') or
				(rx_lbus_cell(2).data_ena = '1' and rx_lbus_cell(2).eopckt = '1' and rx_lbus_cell(2).err_pckt = '0') or
				(rx_lbus_cell(3).data_ena = '1' and rx_lbus_cell(3).eopckt = '1' and rx_lbus_cell(3).err_pckt = '0') then
				packet_valid	<= '1';
			else
				packet_error	<= '1';
			end if;
		end if;
	end if;
end process;


--**#########################################################################################################################################***
-- ARP check
process(reset_clkout_p,request_ARP,clock_out)
begin
	if reset_clkout_p = '1' or request_ARP = '1' then
		MAC_rcvOK_reg				<= '0';
		MAC_gwOK_reg				<= '0';
		req_ARP_reply_rg			<= '0';
		ARP_reply_rg				<= '0';
	elsif rising_edge(clock_out) then
		if packet_valid = '1' then
			req_ARP_reply_rg		<= '0';

		elsif ena_clock_half = '1' then
			if packet_decode = ARP_decode then
				req_ARP_reply_rg		<= rx_lbus_cell(1).data_bus(80); -- request a REPLY on ARP request
				ARP_reply_rg			<= rx_lbus_cell(1).data_bus(81); -- reply on our ARP request
				-- The network is direct
				if rx_lbus_cell(1).data_bus(31 downto 0) = IP_Dest and rx_lbus_cell(1).data_bus(81) = '1' then
					MAC_rcvOK_reg 		<= '1';
				end if;

				-- The network pass through a GateWay
				if rx_lbus_cell(1).data_bus(31 downto 0) = IP_GateWay then
					MAC_gwOK_reg		<= '1';
				end if;
 			end if;
		end if;
	end if;
end process;

--////////////////////////////////////////////////////
process(clock_out)
begin
	if rising_edge(clock_out) then

		latch_MAC_Dest_rg 		<= '0';
		latch_MAC_GateWay_rg	<= '0';
		latch_MAC_error_rg		<= '0';
		latch_ARP_decode		<= '0';
		if packet_decode = ARP_decode then
			latch_ARP_decode				<= '1';
			if  ena_clock_half = '1' then
				-- The network is direct
				if rx_lbus_cell(1).data_bus(31 downto 0) = IP_Dest  then
					latch_MAC_Dest_rg		<= '1';
				end if;

				-- The network pass through a GateWay
				if rx_lbus_cell(1).data_bus(31 downto 0) = IP_GateWay   then
					latch_MAC_GateWay_rg	<= '1';
				end if;
				--############################################################################
				-- check if the MAC source is somebody else that me
				if rx_lbus_cell(1).data_bus(79 downto 32) = MAC_home   then
					check_MAC_src_rg	<= '1';
				end if;
				--############################################################################
				-- check if the IP source is somebody else that me
				if rx_lbus_cell(1).data_bus(31 downto 0) = IP_home   then
					check_IP_src_rg		<= '1';
					latch_MAC_error_rg	<= '1';
				end if;
			end if;
		end if;

		if packet_valid = '1' then
			check_MAC_src_rg	<= '0';
			check_IP_src_rg		<= '0';
		end if;
	end if;
end process;

process(clock_out)
begin
	if rising_edge(clock_out) then
		if latch_ARP_decode = '1' then
			ARP_MAC_src_rg		<= rx_lbus_pipe_A(1).data_bus(79 downto 32);
			ARP_IP_src_rg		<= rx_lbus_pipe_A(1).data_bus(31 downto 0);
		end if;

		if latch_MAC_Dest_rg = '1' then
			MAC_Dest_rg			<= rx_lbus_pipe_A(1).data_bus(79 downto 32);
		end if;
		if latch_MAC_GateWay_rg = '1' then
			MAC_GateWay_rg		<= rx_lbus_pipe_A(1).data_bus(79 downto 32);
		end if;
		if latch_MAC_error_rg = '1' then
			MAC_error_rg		<= rx_lbus_pipe_A(1).data_bus(79 downto 32);
		end if;

	end if;
end process;

ARP_MAC_Dest 			<= MAC_Dest_rg;
ARP_MAC_GateWay 		<= MAC_GateWay_rg;
ARP_check_MAC_src		<= '1' when check_MAC_src_rg = '1' and packet_valid = '1' else '0';
ARP_check_IP_src		<= '1' when check_IP_src_rg = '1' and packet_valid = '1' else '0';
ARP_MAC_error			<= MAC_error_rg;
ARP_MAC_rcvOK 			<= MAC_rcvOK_reg;
ARP_MAC_gwOK 	  		<= MAC_gwOK_reg;
req_ARP_reply			<= '1' when packet_valid = '1' and req_ARP_reply_rg = '1'  else '0';
ARP_reply				<= ARP_reply_rg;
ARP_IP					<= ARP_IP_src_rg;
ARP_MAC					<= ARP_MAC_src_rg;
valid_ARP_packet  		<= '1' when packet_valid = '1' and ARP_store_val = '1'  else '0'; -- not used

--**#########################################################################################################################################***
-- TCP ACK check

process(clock_out)
begin
	if rising_edge(clock_out) then
		if latch_TCP_val_rg = '1' then

			Px_PORT_Host_reg		<= rx_lbus_pipe_A(2).data_bus(111 downto 96); --Port Host received
			Px_PORT_Home_reg		<= rx_lbus_pipe_A(2).data_bus( 95 downto 80); --Port local (DTH)
			Px_SEQ_number_reg		<= rx_lbus_pipe_A(2).data_bus( 79 downto 48);
			Px_ACK_number_reg		<= rx_lbus_pipe_A(2).data_bus( 47 downto 16);
			Px_TCP_flag_reg			<= rx_lbus_pipe_A(2).data_bus(  7 downto  0);

			Px_TCP_len_reg			<= rx_lbus_pipe_A(1).data_bus(127 downto 112) - ("0000000000" & rx_lbus_pipe_A(0).data_bus(11 downto 8) & "00")  - ("0000000000" & rx_lbus_pipe_A(2).data_bus(15 downto 12) & "00");
			TCP_win_tmp				<= rx_lbus_pipe_A(3).data_bus(127 downto 112);


            if    latch_Px_option_MMS_rg(0) = '1' then
                Px_option_MMS			<= rx_lbus_pipe_A(3).data_bus(63 downto 48);
            elsif latch_Px_option_MMS_rg(1) = '1' then
                Px_option_MMS			<= rx_lbus_pipe_A(3).data_bus(39 downto 24);
            elsif latch_Px_option_MMS_rg(2) = '1' then
                Px_option_MMS			<= rx_lbus_pipe_A(3).data_bus(31 downto 16);
            elsif no_options			 = '1' then
                Px_option_MMS			<= (others => '0');
            end if;

            if    latch_Px_option_SCALE_rg(0) = '1' then
                Px_option_SCALE			<= rx_lbus_pipe_A(3).data_bus(63 downto 56);
            elsif latch_Px_option_SCALE_rg(1) = '1' then
                Px_option_SCALE			<= rx_lbus_pipe_A(3).data_bus(31 downto 24);
            elsif latch_Px_option_SCALE_rg(2) = '1' then
                Px_option_SCALE			<= rx_lbus_pipe_A(3).data_bus(23 downto 16);
            elsif no_options			 = '1' then
                Px_option_SCALE			<= (others => '0');
            end if;


		end if;
	end if;
end process;

process(clock_out)
begin
	if rising_edge(clock_out) then

		latch_TCP_val_rg				<= '0';
		latch_Px_option_MMS_rg			<= (others => '0');
		latch_Px_option_SCALE_rg		<= (others => '0');

		if latch_ack_reg(1) = '1' then
				update_MSS				<= '0';
				update_Scale			<= '0';

		elsif ena_clock_half = '1' then
			-- TCP decode  corret port number
			if packet_decode = TCP_decode then
				latch_TCP_val_rg		<= '1';
				update_MSS				<= '0';
 				no_options 				<= '0';

 				if rx_lbus_cell(2).data_bus(1) = '1' then
					if 	rx_lbus_cell(2).data_bus(15 downto 12) = x"6" then				-- only one option  (check length)

						if rx_lbus_cell(3).data_bus(79 downto 72) = x"02" then					-- Option 2 MSS
							latch_Px_option_MMS_rg(0) 		<= '1';
							update_MSS						<= '1';
						elsif rx_lbus_cell(3).data_bus(79 downto 72) = x"03" then				-- Option 3  SCALE
							latch_Px_option_SCALE_rg(0) 	<= '1';
							update_Scale			        <= '1';
						end if;

					elsif rx_lbus_cell(2).data_bus(15 downto 12) > x"6" then         	-- at least our 2 options (check length) -- limit the possible positions on 32 bit alignement or following bytes
						if	   rx_lbus_cell(3).data_bus(79 downto 72) = x"02" then				-- start with option 2
							latch_Px_option_MMS_rg(0) 		<= '1';
							update_MSS						<= '1';
							if 	rx_lbus_cell(3).data_bus(47 downto 40) = x"03" then
								latch_Px_option_SCALE_rg(1)	<= '1';
							    update_Scale			    <= '1';
							elsif rx_lbus_cell(3).data_bus(39 downto 32) = x"03" then
								latch_Px_option_SCALE_rg(2)	<= '1';
							    update_Scale			    <= '1';
							end if;
						elsif rx_lbus_cell(3).data_bus(79 downto 72) = x"03" then				-- start with option 3
							latch_Px_option_SCALE_rg(0) 	<= '1';
							update_Scale			        <= '1';
							if 	rx_lbus_cell(3).data_bus(55 downto 48) = x"02" then
								latch_Px_option_MMS_rg(1)	<= '1';
								update_MSS 					<= '1';
							elsif rx_lbus_cell(3).data_bus(47 downto 40) = x"02" then
								latch_Px_option_MMS_rg(2)	<= '1';
								update_MSS					<= '1';
							end if;
						end if;
					else
						no_options 	<= '1';
					end if;
				end if;
			end if;
		end if;


	end if;
end process;


process(reset_clkout_p,clock_out)
begin
	if reset_clkout_p = '1' then
		latch_ack_reg				<= (others => '0');
	elsif rising_edge(clock_out) then
		latch_ack_reg(1)			<= latch_ack_reg(0);
		latch_ack_reg(0)			<= '0';
		if packet_valid = '1' and TCP_store_val = '1' then
			latch_ack_reg(0)		<= '1';
		end if;

		for I in 0 to number_of_port-1 loop
			if Px_PORT_Home_reg = PORT_Home(I) and Px_PORT_Host_reg = PORT_Host(I) then
				TCP_port_number_reg			<= std_logic_vector(TO_UNSIGNED(2**I,12));
			end if;
		end loop;
	end if;
end process;



PORTx_new_Scale        			<= Px_option_SCALE;		--8b
PORTx_update_Scale     			<= update_Scale;   		--1b
PORTx_new_MMS          			<= Px_option_MMS;  		--16b
PORTx_update_MMS       			<= update_MSS;     		--1b

PORTx_PORT_rcv					<= TCP_port_number_reg;	--12b
PORTx_ACK_number				<= Px_ACK_number_reg;   --32b
PORTx_SEQ_number				<= Px_SEQ_number_reg;   --32b
PORTx_TCP_flag					<= Px_TCP_flag_reg;	    --8b
PORTx_TCP_win					<= TCP_win_tmp;         --16b
PORTx_TCP_len					<= Px_TCP_len_reg;      --16b
PORTx_latch_ack					<= latch_ack_reg(1);

--**#########################################################################################################################################***
-- PING check

process(clock_out)
begin
	if rising_edge(clock_out) then
		if packet_decode = PING_word0 and  ena_clock_half = '1' then
			l_bus_word(3 downto 1) 		<= l_bus_word(2 downto 0);
			l_bus_word(0)			  	<= l_bus_word(3);
		elsif  packet_decode = ICMP_decode then
			l_bus_word 					<= "0100";  -- start ping on word 2 (first word is 0 Eth, 1 IP ,....)
			PING_Last_dt_rg				<= '0';
		end if;

		latch_ICMP_decode					<= '0';
		if ena_clock_half = '1' then
			if  packet_decode = ICMP_decode then
				latch_ICMP_decode			<= '1';
			end if;
		end if;

		wr_data_ping	<= '0';
		if ena_clock_half = '1' then
			if 	packet_decode = PING_word0 then
				wr_data_ping	<= '1';
				if 	l_bus_word(0) = '1' then
					PING_data_rg					<= rx_lbus_cell(0).data_bus(111 downto 48);
					PING_tmp_data					<= rx_lbus_cell(0).data_bus(47 downto 0);
					if rx_lbus_cell(0).empty_pckt = x"6" and rx_lbus_cell(0).eopckt = '1' then
						PING_Last_dt_rg			<= '1';
					end if;
				elsif l_bus_word(1) = '1' then
					PING_data_rg					<= rx_lbus_cell(1).data_bus(111 downto 48);
					PING_tmp_data					<= rx_lbus_cell(1).data_bus(47 downto 0);
					if rx_lbus_cell(1).empty_pckt = x"6" and rx_lbus_cell(1).eopckt = '1' then
						PING_Last_dt_rg			<= '1';
					end if;
				elsif l_bus_word(2) = '1' then
					PING_data_rg					<= rx_lbus_cell(2).data_bus(111 downto 48);
					PING_tmp_data					<= rx_lbus_cell(2).data_bus(47 downto 0);
					if first_word_change_CheckSum = '1' then----------------------------------change the PING Checksum
						PING_data_rg(63 downto 48) <= x"0000";
						PING_data_rg(47 downto 32) <= rx_lbus_cell(2).data_bus(95 downto 80) + x"0800";
					end if;
					if rx_lbus_cell(2).empty_pckt = x"6" and rx_lbus_cell(2).eopckt = '1' then
						PING_Last_dt_rg			<= '1';
					else
						wr_data_ping	<= '1';
					end if;
				elsif l_bus_word(3) = '1' then
					PING_data_rg					<= rx_lbus_cell(3).data_bus(111 downto 48);
					PING_tmp_data					<= rx_lbus_cell(3).data_bus(47 downto 0);
					if rx_lbus_cell(3).empty_pckt = x"6" and rx_lbus_cell(3).eopckt = '1' then
						PING_Last_dt_rg			<= '1';
					end if;
				end if;

			elsif packet_decode = PING_word1 then
				wr_data_ping	<= '1';
					PING_data_rg(63 downto 16)	<= PING_tmp_data;
				if 	l_bus_word(0) = '1' then
					PING_data_rg(15 downto 0)	<= rx_lbus_cell(0).data_bus(127 downto 112);
					if rx_lbus_cell(0).empty_pckt = x"E" and rx_lbus_cell(0).eopckt = '1' then
						PING_Last_dt_rg			<= '1';
					end if;
				elsif l_bus_word(1) = '1' then
					PING_data_rg(15 downto 0)	<= rx_lbus_cell(1).data_bus(127 downto 112);
					if rx_lbus_cell(1).empty_pckt = x"E" and rx_lbus_cell(1).eopckt = '1' then
						PING_Last_dt_rg			<= '1';
					end if;
				elsif l_bus_word(2) = '1' then
					PING_data_rg(15 downto 0)	<= rx_lbus_cell(2).data_bus(127 downto 112);
					if rx_lbus_cell(2).empty_pckt = x"E" and rx_lbus_cell(2).eopckt = '1' then
						PING_Last_dt_rg			<= '1';
					end if;
				elsif l_bus_word(3) = '1' then
					PING_data_rg(15 downto 0)	<= rx_lbus_cell(3).data_bus(127 downto 112);
					if rx_lbus_cell(3).empty_pckt = x"E" and rx_lbus_cell(3).eopckt = '1' then
						PING_Last_dt_rg			<= '1';
					end if;
				end if;
			end if;
		end if;

		req_icmp_prb_rg	<= '0';
		if ICMP_store_val = '1' and packet_error = '1' then
			req_icmp_prb_rg		<= '1';
		end if;

		Reset_PING_FIFO_rg	<= '0';
		if (ICMP_store_val = '1' and packet_error = '1')  then--or (packet_decode = ICMP_decode )
			Reset_PING_FIFO_rg	<= '1';
		end if;
	end if;
end process;


process(reset_clkout_p,clock_out)
begin
	if reset_clkout_p = '1'then
		first_word_change_CheckSum	<= '0';
		counter_pause_frame_reg		<= (others => '0');
	elsif rising_edge(clock_out) then
		if  packet_decode = ICMP_decode then
			first_word_change_CheckSum <= '1';
		end if;

		if 	packet_decode = PING_word0 then
			if l_bus_word(2) = '1' then
				if first_word_change_CheckSum = '1' then----------------------------------change the PING Checksum
					first_word_change_CheckSum	<= '0';
				end if;
			end if;
		end if;

		if latch_pause_frame = '1' then
			counter_pause_frame_reg <= counter_pause_frame_reg + '1';
			delay_pause_frame_reg	<= rx_lbus_cell(1).data_bus(127 downto 112);
		end if;

	end if;
end process;

process(clock_out)
begin
	if rising_edge(clock_out) then
		if latch_ICMP_decode = '1' then
			PING_MAC_dst_rg		<= rx_lbus_pipe_A(0).data_bus(79 downto 32);
			PING_IP_dst_rg		<= rx_lbus_pipe_A(1).data_bus(47 downto 16);
		end if;
	end if;
end process;

req_pause_frame			<= latch_pause_frame;
counter_pause_frame 	<= counter_pause_frame_reg;
delay_pause_frame		<= delay_pause_frame_reg;

req_icmp_prb			<= req_icmp_prb_rg;
ping_last_dt			<= PING_Last_dt_rg;		-- write PING data , the last data will generate a PING reply request
wr_PING_data			<= wr_data_ping;
data_ping				<= PING_data_rg;
Reset_PING_FIFO			<= Reset_PING_FIFO_rg;

PING_MAC_dst			<= PING_MAC_dst_rg;
PING_IP_dst				<= PING_IP_dst_rg;
received_Ping_reply		<= received_Ping_reply_cell; --we receive a PING reply (on our PING Request)PING_req


-- -----------------------------------------
-- check MAC and IP Conflict
process(reset_in_p,clock_out)
begin
	if reset_in_p = '1'  then
		conflict_MAC_counter_reg	<= (others => '0');
		conflict_IP_counter_reg 	<= (others => '0');
		conflict_MAC_value_reg   	<= (others => '0');
	elsif rising_edge(clock_out) then
		if packet_decode = Idle and Empty_indiv = "0000"  then
			-- Check is MAC source is our MAC address !!!
			if rx_lbus_cell(0).data_bus(79 downto 32) = MAC_home then
				conflict_MAC_counter_reg	<= conflict_MAC_counter_reg + '1';
			end	if;

			-- Check is IP source is our MAC address !!!  Check On ARP packet the first packet uses in the 100Gb link
			if rx_lbus_cell(0).data_bus(31 downto 16) = x"0806" and rx_lbus_cell(1).data_bus(31 downto 0) = IP_Home then
				conflict_IP_counter_reg		<= conflict_IP_counter_reg + '1';
				-- memoryse the MAC of the IP identical
				conflict_MAC_value_reg		<= rx_lbus_cell(0).data_bus(79 downto 32);
			end	if;

		end if;


	end if;
end process;

conflict_MAC_counter			<= conflict_MAC_counter_reg;
conflict_IP_counter				<= conflict_IP_counter_reg;
conflict_MAC_value				<= conflict_MAC_value_reg;
end behavioral;