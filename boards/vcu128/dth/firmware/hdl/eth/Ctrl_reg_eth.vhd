------------------------------------------------------------------------------
--                      PCIe register interface                                 --
------------------------------------------------------------------------------
--
--
--   Dominique Gigi 2012
--
--   ver 2.00
--
--
--
--
--
--
------------------------------------------------------------------------------
LIBRARY ieee;


USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.address_table.all;
use work.interface.all;

entity Ctrl_reg_eth is
 	generic (addr_offset		: integer := 0);
	port (
	CLOCK						: IN STD_LOGIC;
	RESET						: IN STD_LOGIC;

	Ctrl_DT						: IN STD_LOGIC_VECTOR(63 downto 0);
	Ctrl_func					: IN STD_LOGIC_VECTOR(16383 downto 0);
	Ctrl_wr						: IN STD_LOGIC;
	Ctrl_func_rd				: IN STD_LOGIC_VECTOR(16383 downto 0);
	Ctrl_out					: OUT STD_LOGIC_VECTOR(63 downto 0);

	PORT_D						: OUT TCP_port_number;
	PORT_S						: OUT TCP_port_number;

	MAC_S						: OUT STD_LOGIC_VECTOR(47 downto 0);
	IP_D						: OUT STD_LOGIC_VECTOR(31 downto 0);
	IP_S						: OUT STD_LOGIC_VECTOR(31 downto 0);
	IP_GATEWAY					: OUT STD_LOGIC_VECTOR(31 downto 0);
	IP_NETWORK					: OUT STD_LOGIC_VECTOR(31 downto 0);
	NETWORK						: OUT STD_LOGIC_VECTOR(31 downto 0);

    -- Conflict IP & MAC
    conflict_MAC_counter		: in std_logic_vector(31 downto 0);
    conflict_IP_counter			: in std_logic_vector(31 downto 0);
    conflict_MAC_value			: in std_logic_vector(47 downto 0);

	MAC							: IN STD_LOGIC_VECTOR(47 downto 0)
	);
end Ctrl_reg_eth;

architecture behavioral of Ctrl_reg_eth is

signal	PORT_D_reg					: TCP_port_number;
signal	PORT_S_reg					: TCP_port_number;
signal	MAC_S_reg					: STD_LOGIC_VECTOR(47 downto 0);
signal	IP_D_reg					: STD_LOGIC_VECTOR(31 downto 0);
signal	IP_S_reg					: STD_LOGIC_VECTOR(31 downto 0);

signal	IP_GATEWAY_reg				: STD_LOGIC_VECTOR(31 downto 0);
signal	IP_NETWORK_reg				: STD_LOGIC_VECTOR(31 downto 0);
signal	NETWORK_reg					: STD_LOGIC_VECTOR(31 downto 0);


signal  Ctrl_out_rg					: STD_LOGIC_VECTOR(63 downto 0);

--****************************************************************************
--*******************************  BEGINNING *********************************
--****************************************************************************
BEGIN

process(clock)
begin
	if rising_edge(clock) then
		if Ctrl_wr = '1' then
			if Ctrl_func(eth_100Gb_ctrl_IPS_def + addr_offset)  = '1' then
				IP_S_reg(31 downto 0) 		<= Ctrl_dt(31 downto 0);
			end if;
		end if;
	end if;
end process;

process(reset,clock)
begin
if reset = '0' then
	PORT_D_reg(0)						<= (others => '0');
	PORT_S_reg(0)						<= (others => '0');
	PORT_D_reg(1)						<= (others => '0');
	PORT_S_reg(1)						<= (others => '0');
	PORT_D_reg(2)						<= (others => '0');
	PORT_S_reg(2)						<= (others => '0');
	PORT_D_reg(3)						<= (others => '0');
	PORT_S_reg(3)						<= (others => '0');
	PORT_D_reg(4)						<= (others => '0');
	PORT_S_reg(4)						<= (others => '0');
	PORT_D_reg(5)						<= (others => '0');
	PORT_S_reg(5)						<= (others => '0');
	PORT_D_reg(6)						<= (others => '0');
	PORT_S_reg(6)						<= (others => '0');
	PORT_D_reg(7)						<= (others => '0');
	PORT_S_reg(7)						<= (others => '0');
	PORT_D_reg(8)						<= (others => '0');
	PORT_S_reg(8)						<= (others => '0');
	PORT_D_reg(9)						<= (others => '0');
	PORT_S_reg(9)						<= (others => '0');
	PORT_D_reg(10)						<= (others => '0');
	PORT_S_reg(10)						<= (others => '0');
	PORT_D_reg(11)						<= (others => '0');
	PORT_S_reg(11)						<= (others => '0');
	IP_D_reg							<= (others => '0');
	IP_GATEWAY_reg						<= (others => '0');
	IP_NETWORK_reg						<= (others => '0');

elsif rising_edge(clock) then
	if Ctrl_wr = '1' then
		if Ctrl_func(eth_100Gb_ctrl_port0_def + addr_offset)  = '1' then
			PORT_D_reg(0)					<= Ctrl_dt(15 downto 0);
			PORT_S_reg(0)					<= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port1_def + addr_offset)  = '1' then
			PORT_D_reg(1)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(1)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port2_def + addr_offset)  = '1' then
			PORT_D_reg(2)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(2)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port3_def + addr_offset)  = '1' then
			PORT_D_reg(3)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(3)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port4_def + addr_offset)  = '1' then
			PORT_D_reg(4)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(4)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port5_def + addr_offset)  = '1' then
			PORT_D_reg(5)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(5)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port6_def + addr_offset)  = '1' then
			PORT_D_reg(6)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(6)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port7_def + addr_offset)  = '1' then
			PORT_D_reg(7)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(7)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port8_def + addr_offset)  = '1' then
			PORT_D_reg(8)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(8)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port9_def + addr_offset)  = '1' then
			PORT_D_reg(9)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(9)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port10_def + addr_offset)  = '1' then
			PORT_D_reg(10)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(10)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_port11_def + addr_offset)  = '1' then
			PORT_D_reg(11)				    <= Ctrl_dt(15 downto 0);
			PORT_S_reg(11)				    <= Ctrl_dt(31 downto 16);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_IPD_def + addr_offset)  = '1' then
			IP_D_reg(31 downto 0) 			<= Ctrl_dt(31 downto 0);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_IP_network_def + addr_offset)  = '1' then
			IP_NETWORK_reg					<= Ctrl_dt(31 downto 0);
		end if;

		if Ctrl_func(eth_100Gb_ctrl_IP_gateway_def + addr_offset)  = '1' then
			IP_GATEWAY_reg		 			<= Ctrl_dt(31 downto 0);
		end if;

	end if;
end if;
end process;

process(clock)
begin
	if rising_edge(clock) then
		Ctrl_out_rg <= (others => '0');

		if 	  Ctrl_func_rd(eth_100Gb_ctrl_port0_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(0);
			Ctrl_out_rg(31 downto 16) 					<= PORT_S_reg(0);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port1_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(1);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(1);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port2_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(2);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(2);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port3_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(3);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(3);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port4_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(4);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(4);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port5_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(5);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(5);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port6_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(6);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(6);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port7_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(7);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(7);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port8_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(8);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(8);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port9_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(9);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(9);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port10_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(10);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(10);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_port11_def + addr_offset)  = '1' then
			Ctrl_out_rg(15 downto 0) 					<= PORT_D_reg(11);
			Ctrl_out_rg(31 downto 16)			 		<= PORT_S_reg(11);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_MAC_def + addr_offset)  = '1' then
			Ctrl_out_rg(47 downto  0) 					<= MAC(47 downto 0);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_IPS_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0)					<= IP_S_reg(31 downto 0);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_IPD_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= IP_D_reg(31 downto 0);
		elsif Ctrl_func_rd(eth_100Gb_ctrl_IP_network_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= IP_NETWORK_reg;
		elsif Ctrl_func_rd(eth_100Gb_ctrl_IP_gateway_def + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<= IP_GATEWAY_reg;
		elsif Ctrl_func_rd(eth_100gb_MAC_Conflict + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<=  conflict_MAC_counter;
		elsif Ctrl_func_rd(eth_100gb_IP_Conflict + addr_offset)  = '1' then
			Ctrl_out_rg(31 downto  0) 					<=  conflict_IP_counter;
		elsif Ctrl_func_rd(eth_100gb_MAC_WITH_IP_CONFLICT + addr_offset)  = '1' then
			Ctrl_out_rg(47 downto  0) 					<= conflict_MAC_value;
--		else
--			Ctrl_out_rg <= (others => '0');
		end if;

	end if;
end process;

Ctrl_out		<= Ctrl_out_rg;

process(clock )
begin
 if rising_edge(clock) then
	NETWORK_reg	<= IP_S_reg and IP_NETWORK_reg;
end if;
end process;

PORT_D						<= PORT_D_reg;
PORT_S						<= PORT_S_reg;
MAC_S						<= MAC;
IP_D						<= IP_D_reg;
IP_S						<= IP_S_reg;
IP_NETWORK					<= IP_NETWORK_reg;
IP_GATEWAY					<= IP_GATEWAY_reg;
NETWORK						<= NETWORK_reg;

end behavioral;
