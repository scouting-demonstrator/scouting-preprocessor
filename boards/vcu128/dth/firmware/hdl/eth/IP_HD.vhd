----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.08.2017 13:58:46
-- Design Name: 
-- Module Name: IP_HD - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.interface.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IP_HD is
  Port (
		-- INPUT
        RESET_n                                     : IN STD_LOGIC;
        CLOCK                                       : IN STD_LOGIC;                       
                        
        IDENTIF                                     : IN STD_LOGIC_VECTOR(15 DOWNTO 0); -- Identification value
        PL_LENGTH                                   : IN STD_LOGIC_VECTOR(15 DOWNTO 0);    -- Playload  length in bytes
        IP_S                                        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        IP_D                                        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        ICMP                                        : IN STD_LOGIC;
       -- DHCP                                      : IN STD_LOGIC;    
        TCP_opt                                     : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        TCP_wodt                                    : IN STD_LOGIC;
        PREP                                        : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
                        
        -- OUTPUT     
		IP_TX_lbus_a								: OUT tx_usr_record;                 
        IP_TX_lbus_b								: OUT tx_usr_record;          
        IP_TX_lbus_c								: OUT tx_usr_record           
    );
end IP_HD;

--******************************************************************
architecture Behavioral of IP_HD is

COMPONENT CS_IP_64b
  PORT (
    A       : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    B       : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    CLK     : IN STD_LOGIC;
    C_IN    : IN STD_LOGIC;
    CE      : IN STD_LOGIC;
    SCLR    : IN STD_LOGIC;
    C_OUT   : OUT STD_LOGIC;
    S       : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
  );
END COMPONENT;

COMPONENT CS_IP_32b
  PORT (
    A       : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    B       : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    CLK     : IN STD_LOGIC;
    C_IN    : IN STD_LOGIC;
    CE      : IN STD_LOGIC;
    SCLR    : IN STD_LOGIC;
    C_OUT   : OUT STD_LOGIC;
    S       : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;

COMPONENT CS_IP_16b
  PORT (
    A       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    B       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    CLK     : IN STD_LOGIC;
    C_IN    : IN STD_LOGIC;
    CE      : IN STD_LOGIC;
    SCLR    : IN STD_LOGIC;
    C_OUT   : OUT STD_LOGIC;
    S       : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

COMPONENT ip_length
  PORT (
    A       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    B       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    CLK     : IN STD_LOGIC;
    CE      : IN STD_LOGIC;
    S       : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;
 
signal ip_hd_l				       : std_logic_vector(15 downto 0);
signal ip_pckt_l			       : std_logic_vector(15 downto 0);

signal LEN_mux  			       : std_logic_vector(15 downto 0);
signal checksum_n 		           : std_logic_vector(15 downto 0);
signal checksum 			       : std_logic_vector(15 downto 0);
 
signal protocol			           : std_logic_vector(7 downto 0);
	
signal IP_Dest				       : std_logic_vector(31 downto 0);

signal CS_S01                      : std_logic_vector(63 downto 0);
signal CS_S02                      : std_logic_vector(63 downto 0);
signal CS_S10                      : std_logic_vector(63 downto 0);
signal CS_S20                      : std_logic_vector(31 downto 0);
signal CS_S30                      : std_logic_vector(15 downto 0);
signal CS_S40                      : std_logic_vector(15 downto 0);

signal CS_carry                    : std_logic_vector(31 downto 0);


attribute mark_debug : string;
--attribute mark_debug of ip_pckt_l: signal is "true";
--******************************************************************
--              Code start HERE
--******************************************************************
begin

--IP_Dest <= x"FFFFFFFF" when DHCP = '1' else IP_D;
IP_Dest <= IP_D;

process(clock)
begin
if rising_edge(clock) then
	if ICMP = '1' then
		ip_hd_l <= x"0014";
--	elsif DHCP = '1' then
--		ip_hd_l <= x"011A";
	elsif TCP_opt(2) = '0' and  TCP_opt(1 downto 0) /= "00" then
		ip_hd_l <= x"0030";	
	-- elsif TCP_opt(2) = '1' and  TCP_opt(1 downto 0)  = "00" then --not used
		-- ip_hd_l <= x"0038";
	-- elsif TCP_opt(2) = '1' and  TCP_opt(1 downto 0) /= "00" then -- not used
		-- ip_hd_l <= x"0040";
	else
		ip_hd_l <= x"0028";
	end if;	
end if;
end process;

process(ICMP)--,DHCP)
begin
--	if DHCP = '1' then
--		protocol <= x"11";
--	els
	if ICMP = '1' then
		protocol <= x"01";
	else
		protocol <= x"06";
	end if;
end process;
 

LEN_mux <= x"0000" when  TCP_wodt = '1' else  PL_LENGTH; -- DHCP = '1' or"0000000000000" when TCP_flags /= x"00"    elseif open or reset connection, no data

LENGTH_IP:ip_length
  PORT map(
    A       => LEN_mux		,--VECTOR(15 DOWNTO 0);
    B       => ip_hd_l		,--VECTOR(15 DOWNTO 0);
    CLK     => CLOCK		,--
    CE      => PREP(0)		,--
    S       => ip_pckt_l     --VECTOR(15 DOWNTO 0)
  ); 
 

-----------------------------------------------------			
-- MUX to compute the Checksum : DATA and ENA
-----------------------------------------------------
 -- identifier all the time 00000
------------------------------------------------level I   (prep(0))
CS_i10:CS_IP_64b
PORT map(
    A(63 downto 32) => IP_S					,
    A(31 downto 0)  => IP_Dest				,
	B(63 downto 48) => ip_pckt_l			,
	B(47 downto 40) => x"FF"				,
    B(39 downto 32) => protocol          	,
    B(31 downto 0)  => x"45004000"			,
    CLK             => CLOCK				,
    C_IN            => '0'					,
    CE              => '1'					,
    SCLR            => '0'					,
    C_OUT           => CS_carry(0)			,
    S               => CS_S01               
  ); 
 
CS_S02(15 downto 0)    <= IDENTIF;    
CS_S02(31 downto 16)  <= x"0000";       
CS_S02(63 downto 32)  <= (others => '0');      

------------------------------------------------Level II  (prep(1))
-- it this implementation the third 64-b word is 0, removed from the CS calcule
-- CS_i20:CS_IP_64b
-- PORT map(
    -- A               => CS_S01              , 
    -- B               => CS_S02              , 
    -- CLK             => CLOCK               , 
    -- C_IN            => CS_carry(0)         , 
    -- CE              => '1'                 , 
    -- SCLR            => '0'                 , 
    -- C_OUT           => CS_carry(1)         , 
    -- S               => CS_S10                
-- ); 
 
------------------------------------------------Level III (prep(1))
CS_i30:CS_IP_32b
PORT map(
    A               => CS_S01(31 downto 0), 
    B               => CS_S01(63 downto 32), 
    CLK             => CLOCK               , 
    C_IN            => CS_carry(0)         , 
    CE              => '1'                 , 
    SCLR            => '0'                 , 
    C_OUT           => CS_carry(2)         , 
    S               => CS_S20                
); 
 
--------------------------------------------------Level IV (prep(2))
CS_i40:CS_IP_16b
 PORT map(
   A                => CS_S20(15 downto 0) ,
   B                => CS_S20(31 downto 16),
   CLK              => CLOCK               ,
   C_IN             => CS_carry(2)         ,
   CE               => '1'                ,
   SCLR             => '0'                ,
   C_OUT            => CS_carry(3)        ,
   S                => CS_S30                   
 );
 
---------------------------------------------------Level V (prep(3))
CS_i50:CS_IP_16b
 PORT map(
   A                => CS_S30(15 downto 0) ,
   B                => x"0000"             ,
   CLK              => CLOCK               ,
   C_IN             => CS_carry(3)         ,
   CE               => '1'                 ,
   SCLR             => '0'                 ,
--   C_OUT            => CS_carry(3)       ,
   S                => checksum_n                    
 ); 
 
checksum <= not(checksum_n); 
 

---------------------------------------------
-- DATA to write to FIFO	: DATA and ENA
---------------------------------------------
IP_TX_lbus_a.data_bus(127 downto 16)	    <= (others => '0');
IP_TX_lbus_a.data_bus(15 downto 0)		    <= x"4500";  
IP_TX_lbus_a.data_ena						<= '1';
IP_TX_lbus_a.sopckt							<= '1';
IP_TX_lbus_a.eopckt							<= '0';
IP_TX_lbus_a.err_pckt						<= '0';
IP_TX_lbus_a.empty_pckt						<= x"0";

IP_TX_lbus_b.data_bus(127 downto 112)	    <= ip_pckt_l;
IP_TX_lbus_b.data_bus(111 downto 96)	    <= IDENTIF;
IP_TX_lbus_b.data_bus(95 downto 72)		    <= x"4000FF"; -- don't fragment the datagram
IP_TX_lbus_b.data_bus(71 downto 64)		    <= protocol; -- protocol x01 for PING(ICMP);  x06 for TCP
IP_TX_lbus_b.data_bus(63 downto 48)		    <= checksum;
IP_TX_lbus_b.data_bus(47 downto 16)		    <= IP_S(31 downto  0);
IP_TX_lbus_b.data_bus(15 downto 00)		    <= IP_Dest(31 downto 16);
IP_TX_lbus_b.data_ena						<= '1';
IP_TX_lbus_b.sopckt							<= '0';
IP_TX_lbus_b.eopckt							<= '0';
IP_TX_lbus_b.err_pckt						<= '0';
IP_TX_lbus_b.empty_pckt						<= x"0";

IP_TX_lbus_c.data_bus(127 downto 112)	    <= IP_Dest(15 downto  0);
IP_TX_lbus_c.data_bus(111 downto 0	)	    <= (others => '0');
IP_TX_lbus_c.data_ena						<= '1';
IP_TX_lbus_c.sopckt							<= '0';
IP_TX_lbus_c.eopckt							<= '0';
IP_TX_lbus_c.err_pckt						<= '0';
IP_TX_lbus_c.empty_pckt						<= x"0";

end Behavioral;
