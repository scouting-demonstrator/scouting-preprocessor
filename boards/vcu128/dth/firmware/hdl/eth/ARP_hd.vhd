------------------------------------------------------
-- this boxe prepare the ARP header
--
-- Dominique Gigi
--
-- Oct 2010 ver 1.00
--
------------------------------------------------------
 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.interface.all;
entity ARP_HD is
	port (
		-- INPUT
		RESET_n						: IN STD_LOGIC;
		CLOCK						: IN STD_LOGIC;
							
		MAC_D						: IN STD_LOGIC_VECTOR(47 DOWNTO 0);
		MAC_S						: IN STD_LOGIC_VECTOR(47 DOWNTO 0);
		IP_D						: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		IP_S						: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		Rq_Rply					: IN STD_LOGIC;-- request reply
		-- OUTPUT
		
		TX_lbus_a			: OUT tx_usr_record;
		TX_lbus_b			: OUT tx_usr_record;
		TX_lbus_c			: OUT tx_usr_record;
		TX_lbus_d			: OUT tx_usr_record 
		);
end ARP_HD;

architecture core of ARP_HD is
 

 

--**********************************************************************************
--************************** beginning  ********************************************
--**********************************************************************************
begin
 
TX_lbus_a.data_bus(127 downto 16)		<= (others => '0');
TX_lbus_a.data_bus(15 downto 0)			<= x"0001";  
TX_lbus_a.data_ena						<= '1';
TX_lbus_a.sopckt						<= '1';
TX_lbus_a.eopckt						<= '0';
TX_lbus_a.err_pckt						<= '0';
TX_lbus_a.empty_pckt					<= x"0";

TX_lbus_b.data_bus(127 downto 96)		<= x"08000604"; --TYPE + Hardware len + Protocol len
TX_lbus_b.data_bus(95 downto 82)		<= "00000000000000";
TX_lbus_b.data_bus(81)					<= Rq_Rply;
TX_lbus_b.data_bus(80)					<= not(Rq_Rply);
TX_lbus_b.data_bus(79 downto 32)		<= MAC_S;
TX_lbus_b.data_bus(31 downto 0)			<= IP_S;
TX_lbus_b.data_ena						<= '1';
TX_lbus_b.sopckt						<= '0';
TX_lbus_b.eopckt						<= '0';
TX_lbus_b.err_pckt						<= '0';
TX_lbus_b.empty_pckt					<= x"0";

TX_lbus_c.data_bus(127 downto 80)		<= (others => '0');--MAC_D;
TX_lbus_c.data_bus(79 downto 48)		<= IP_D;
TX_lbus_c.data_bus(47 downto 0	)		<= (others => '0');
TX_lbus_c.data_ena						<= '1';
TX_lbus_c.sopckt						<= '0';
TX_lbus_c.eopckt						<= '0';
TX_lbus_c.err_pckt						<= '0';
TX_lbus_c.empty_pckt					<= x"0";


--TX_lbus_d.data_bus(127 downto 112)		<= IP_D(15 downto 0);
TX_lbus_d.data_bus(127 downto 0	)		<= (others => '0');
TX_lbus_d.data_ena						<= '1';
TX_lbus_d.sopckt						<= '0';
TX_lbus_d.eopckt						<= '1';
TX_lbus_d.err_pckt						<= '0';
TX_lbus_d.empty_pckt					<= x"4"; 	-- PAD packet with 000
end core;