----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.01.2021 11:23:45
-- Design Name: 
-- Module Name: Block_Buffer_in_eth - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.interface.all;
use work.address_table.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Block_Buffer_in_eth is
 Port (
	
	Resetp				: in std_logic;
	Clock				: in std_logic;
	
	PL_from_TCP_Stream	: in HBM_to_TCP_block_record;
	
	PL_TX_lbus_A		: out tx_usr_record;
	PL_TX_lbus_B		: out tx_usr_record;
	PL_TX_lbus_C		: out tx_usr_record;
	PL_TX_lbus_D		: out tx_usr_record;
	PL_last_data        : out std_logic;
	
	rd_payload_block	: in std_logic
 );
end Block_Buffer_in_eth;

--ARCHITECTURE------------------------------------------------------
architecture Behavioral of Block_Buffer_in_eth is

COMPONENT Buffer_in_eth
  PORT (
    clk         : IN STD_LOGIC;
    srst        : IN STD_LOGIC;
    din         : IN STD_LOGIC_VECTOR(516 DOWNTO 0);
    wr_en       : IN STD_LOGIC;
    rd_en       : IN STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(516 DOWNTO 0);
    full        : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;

signal eth_data_o							: std_logic_vector(511 downto 0);
signal eth_data_ena_o						: std_logic_vector(3 downto 0);
signal eth_last_dt_o						: std_logic;
 
signal eth_rd_data_cell						: std_logic;
signal last_data_reg						: std_logic;

signal Fifo_Empty   						: std_logic;

signal PL_TX_lbus_A_reg						: tx_usr_record;
signal PL_TX_lbus_B_reg						: tx_usr_record;
signal PL_TX_lbus_C_reg						: tx_usr_record;
signal PL_TX_lbus_D_reg						: tx_usr_record;

--***************************************************************** 
--*************<     CODE START HERE    >**************************
--***************************************************************** 
begin

Bufer_i1:Buffer_in_eth
  PORT MAP(
    srst         		=> Resetp, 
    clk        		    => Clock, 
    wr_en       		=> PL_from_TCP_Stream.HBM_blk_wen, 
    din(511 downto 000)	=> PL_from_TCP_Stream.HBM_PL_data , 
    din(515 downto 512)	=> PL_from_TCP_Stream.HBM_PL_ena  , 
    din(516)			=> PL_from_TCP_Stream.HBM_PL_last  , 
    -- full        		=> ,--: OUT STD_LOGIC;
    -- wr_rst_busy 		=> ,--: OUT STD_LOGIC;
    rd_en       		=> eth_rd_data_cell, 
    dout(511 downto 000)=> eth_data_o          , 
    dout(515 downto 512)=> eth_data_ena_o      , 
    dout(516)			=> eth_last_dt_o       ,
    empty       		=> Fifo_Empty
    -- rd_rst_busy 		=>  --: OUT STD_LOGIC
);


    
eth_rd_data_cell			<= '1' when rd_payload_block = '1'  else '0'; -- and last_data_reg = '0'


 --@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--  Send out the packet


 
--  Format to 100G MAC Xilinx
--  for TCP packet the payload start to be sent on PL_TX_lbus_D (bit 79..0) 
process(Clock) 
begin
	if rising_edge(Clock) then
    -- Ethernet word 000..127
        PL_TX_lbus_A_reg.data_bus		<= eth_data_o(127 downto 000);
        PL_TX_lbus_A_reg.data_ena		<= eth_data_ena_o(0);
        PL_TX_lbus_A_reg.eopckt			<= '0';
        if eth_data_ena_o(1) = '0' and eth_data_ena_o(0)  = '1' then
            PL_TX_lbus_A_reg.eopckt		<= eth_last_dt_o;
        end if;
        PL_TX_lbus_A_reg.sopckt			<= '0';
        PL_TX_lbus_A_reg.err_pckt		<= '0';
    
        if 		eth_data_ena_o(1)  = '0'  then
            PL_TX_lbus_A_reg.empty_pckt	<= x"A";
        else
            PL_TX_lbus_A_reg.empty_pckt	<= x"0";
        end if;

    -- Ethernet word 128..255	
        PL_TX_lbus_B_reg.data_bus		<= eth_data_o(255 downto 128);
        PL_TX_lbus_B_reg.data_ena		<= eth_data_ena_o(1);
        PL_TX_lbus_B_reg.eopckt			<= '0';
        if eth_data_ena_o(2) = '0' and eth_data_ena_o(1)  = '1' then
            PL_TX_lbus_B_reg.eopckt		<= eth_last_dt_o;
        end if;
        PL_TX_lbus_B_reg.sopckt			<= '0';
        PL_TX_lbus_B_reg.err_pckt		<= '0';
    
        if 		eth_data_ena_o(2)  = '0' then
            PL_TX_lbus_B_reg.empty_pckt		<= x"A";
        else
            PL_TX_lbus_B_reg.empty_pckt		<= x"0";
        end if;
    
    -- Ethernet word 256..383		
        PL_TX_lbus_C_reg.data_bus		<= eth_data_o(383 downto 256);
        PL_TX_lbus_C_reg.data_ena		<= eth_data_ena_o(2);
        PL_TX_lbus_C_reg.eopckt			<= '0';
        if eth_data_ena_o(3) = '0' and eth_data_ena_o(2)  = '1' then
            PL_TX_lbus_C_reg.eopckt		<= eth_last_dt_o;
        end if;PL_TX_lbus_C_reg.sopckt			<= '0';
        PL_TX_lbus_C_reg.err_pckt		<= '0';
    
        if 		eth_data_ena_o(3)  = '0' then
            PL_TX_lbus_C_reg.empty_pckt		<= x"A";
        else
            PL_TX_lbus_C_reg.empty_pckt		<= x"0";
        end if;

        -- Ethernet word 384..511
        PL_TX_lbus_D_reg.data_bus		<= eth_data_o(511 downto 384);
        PL_TX_lbus_D_reg.data_ena		<= eth_data_ena_o(3);
        PL_TX_lbus_D_reg.eopckt			<= '0';
        if   eth_data_ena_o(3)  = '1' then
            PL_TX_lbus_D_reg.eopckt		<= eth_last_dt_o;
        end if;PL_TX_lbus_D_reg.sopckt			<= '0';
        PL_TX_lbus_D_reg.err_pckt		<= '0';
    
        if 		eth_data_ena_o(3)  = '1' and eth_last_dt_o = '1' then
            PL_TX_lbus_D_reg.empty_pckt		<= x"A";
        else
            PL_TX_lbus_D_reg.empty_pckt		<= x"0";
        end if;
        
        last_data_reg		<= eth_last_dt_o;
		 
    
	end if;
end process;

PL_last_data  <= last_data_reg;
PL_TX_lbus_A  <= PL_TX_lbus_A_reg;
PL_TX_lbus_B  <= PL_TX_lbus_B_reg;
PL_TX_lbus_C  <= PL_TX_lbus_C_reg;
PL_TX_lbus_D  <= PL_TX_lbus_D_reg;

end Behavioral;
