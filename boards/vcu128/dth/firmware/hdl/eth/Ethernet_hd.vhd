 ------------------------------------------------------
-- this boxe prepare the ETHERNET header
--
-- Dominique Gigi
--
-- Oct 2010 ver 1.00
--
------------------------------------------------------
 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.interface.all;

entity ethernet_HD is
	port (
		-- INPUT
		RESET_n				    : IN STD_LOGIC;
		CLOCK					: IN STD_LOGIC;
					
		MAC_D					: IN STD_LOGIC_VECTOR(47 DOWNTO 0);
		MAC_S					: IN STD_LOGIC_VECTOR(47 DOWNTO 0);
		TCP					    : IN STD_LOGIC;
		ARP					    : IN STD_LOGIC;
		PING					: IN STD_LOGIC;
		DHCP					: IN STD_LOGIC;
		LLDP					: IN STD_LOGIC;
		Brdcast				    : IN STD_LOGIC;
 	
		-- OUTPUT
		PACKET_TYPE			    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		ETH_TX_lbus			    : OUT tx_usr_record
	);
end ethernet_HD;

architecture core of ethernet_HD is

signal wr_ff			: std_logic;
signal mux				: std_logic_vector(127 downto 0);
signal packet_type_rg	: std_logic_vector(2 downto 0);

--*****************************************************************************************
--
--*****************************************************************************************
begin

---------------------------------------------
-- mux to write to FIFO	: DATA and ENA
---------------------------------------------
process(clock)
begin
	if rising_edge(clock) then

		if Brdcast = '1' then
			mux(127 downto 80) 	<= x"FFFFFFFFFFFF"; -- broadcast MAC address
		elsif LLDP = '1' then
			mux(127 downto 80) 	<= x"0180c200000E"; -- LLDP MAC address
		else
			mux(127 downto 80)	<= MAC_D(47 downto 0); -- MAC Destination
		end if;
		
		    mux(79 downto 32)   <= MAC_S(47 downto 0);-- MAC Source
			
		if TCP = '1' or PING = '1' or DHCP = '1' then
			mux(31 downto 16) 	<= x"0800"; -- Ethernet TYPE packet IP
		elsif ARP = '1' then
			mux(31 downto 16) 	<= x"0806"; -- Ethernet TYPE packet ARP
		elsif LLDP = '1' then
			mux(31 downto 16) 	<= x"88CC"; -- Ethernet TYPE packet LLDP
		end if;
		mux(15 downto 0) 		<= (others => '0');
	end if;
end process;

PACKET_TYPE 				<= packet_type_rg;
ETH_TX_lbus.data_bus		<= mux;
ETH_TX_lbus.sopckt			<= '1';
ETH_TX_lbus.eopckt			<= '0';
ETH_TX_lbus.err_pckt		<= '0';
ETH_TX_lbus.empty_pckt		<= x"0";
ETH_TX_lbus.data_ena		<= '1'; 


end core;