
LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
 
--ENTITY-----------------------------------------------------------
entity pause_frame is

	port
	(
		resetp								: in std_logic;
		clock								: in std_logic;
		
		req_pause_frame						: in std_logic;
		pause_quanta						: in std_logic_vector(15 downto 0);
		no_transfer_ongoing					: in std_logic;
		
		pause_transmition					: out std_logic
	 );
end pause_frame;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of pause_frame is
	 
signal start_decount						: std_logic;
signal end_quanta							: std_logic;
signal pause_transmition_cell				: std_logic;

signal quanta_counter						: std_logic_vector(15 downto 0);

attribute mark_debug : string;
--attribute mark_debug of start_decount: signal is "true";
--attribute mark_debug of end_quanta: signal is "true";
--attribute mark_debug of pause_transmition_cell: signal is "true";
--attribute mark_debug of quanta_counter: signal is "true";
--attribute mark_debug of req_pause_frame: signal is "true"; 

--***************************************************************** 
--*************<     CODE START HERE    >**************************
--***************************************************************** 
 begin 
 
 process(resetp,clock)
 begin
	if resetp = '1' then 
		start_decount			<= '0'; 
		pause_transmition_cell	<= '0';
	elsif rising_edge(clock) then
		if req_pause_frame = '1' then	
			pause_transmition_cell	<= '1';
		elsif end_quanta = '1' then
			pause_transmition_cell	<= '0';
		end if;
		
		if no_transfer_ongoing = '1' and pause_transmition_cell = '1' then
			start_decount	<= '1';
		elsif end_quanta = '1' then
			start_decount	<= '0';
		end if;
	end if;
 end process;
 
pause_transmition 	<= pause_transmition_cell;

process(resetp,clock)
begin	
	if resetp = '1' then
		quanta_counter	<= (others => '0');
	elsif rising_edge(clock) then
		if req_pause_frame = '1' then
			if pause_quanta = x"0000" then
				quanta_counter	<= x"0000";
			elsif pause_quanta < x"018A" then   -- < 394
				quanta_counter	<= x"018A";
			else
				quanta_counter	<= pause_quanta;
			end if;
		elsif start_decount = '1' then
			quanta_counter <= quanta_counter - '1';
		end if;
		
		end_quanta 		<= '0';
		if quanta_counter = x"0001" then
			end_quanta 	<= '1';
		end if;
	end if;
end process;



 
end behavioral;