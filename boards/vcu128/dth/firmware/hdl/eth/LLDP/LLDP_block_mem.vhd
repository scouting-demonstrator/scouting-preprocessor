LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

--ENTITY-----------------------------------------------------------
entity LLDP_block_mem is
    generic (port_num           : std_logic_vector(7 downto 0));
	port
	(
	    Reset_n         : in STD_LOGIC;
		address_a		: IN STD_LOGIC_VECTOR (0 DOWNTO 0);
		address_b		: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
		byteena_a		: IN STD_LOGIC_VECTOR (63 DOWNTO 0) :=  (OTHERS => '1');
		byteena_b		: IN STD_LOGIC_VECTOR (7 DOWNTO 0)  :=  (OTHERS => '1');
		clock_a			: IN STD_LOGIC  := '1';
		clock_b			: IN STD_LOGIC ;
		data_a			: IN STD_LOGIC_VECTOR (511 DOWNTO 0);
		data_b			: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		wren_a			: IN STD_LOGIC  := '0';
		wren_b			: IN STD_LOGIC  := '0';
		q_a				: OUT STD_LOGIC_VECTOR (511 DOWNTO 0);
		q_b				: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	 );
end LLDP_block_mem;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of LLDP_block_mem is

type work64bit			is array (0 to 15) of std_logic_vector(63 downto 0);
type work512bit		is array (0 to 1) of std_logic_vector(511 downto 0);
signal word_LLDP : work512bit := (x"0000000000000000000000000000020704FFFFFFFFFFFF040703FFFFFFFFFFFF060200780A144454483430302020556E636F6E666967757265640E0400800080",
                                  x"0C2D434D53204441512044544834303020206E6F742079657420636F6E666967757265642062792044544850433430080270FF00000000000000000000000000");

--0000  01 80 C2 00 00 0E 00 08 03 04 05 16 88 CC 02 07  ................ MAC dest  MAC src
--0010  04 08 00 30 F4 8E 1E 04 07 03 08 00 30 F4 8E 1E  ................
--0020  06 02 00 78 0A 14 44 54 48 34 30 30 20 20 55 6E  ...x..DTH Unco
--0030  63 6F 6E 66 69 67 75 72 65 64 0E 04 00 80 00 80  nfigured.......)
--0040  0C 2D 43 4D 53 20 44 41 51 20 44 54 48 34 30 30   CMS DAQ DTH no
--0050  20 20 6E 6F 74 20 79 65 74 20 63 6F 6E 66 69 67   yet configured
--0060  75 72 65 64 20 62 79 20 44 54 48 50 43 34 30 08      by DTHPC..p0..

signal we_b_clk_a_resync			: std_logic;
signal WE_B_wait_for_free			: std_logic;

signal reg_out_LLDP					: std_logic_vector(511 downto 0);
signal reg_out_ctrl					: std_logic_vector(63 downto 0);

signal vcc							: std_logic := '1';

--*****************************************************************
--*************<     CODE START HERE    >**************************
--*****************************************************************
 begin
-- resynbc we from clock B
we_resync_i1:entity work.resync_pulse
port map(
	aresetn		=> vcc,
	clocki		=> clock_b,
	in_s		=> wren_b,
	clocko		=> clock_a,
	out_s		=> we_b_clk_a_resync
	);

 process(clock_a)
 begin
	if rising_edge(clock_a) then
		if we_b_clk_a_resync = '1' then
			WE_B_wait_for_free <= '1';
		elsif wren_a  = '0' then
			WE_B_wait_for_free <= '0';
		end if;
	end if;
 end process;

 -- write to teh register to change the values
 process(Reset_n,clock_a)
 variable wrd64b    : work64bit;
 variable wrd512b    : work512bit;

 begin
    if Reset_n = '0' then
        word_LLDP(1)(111 downto 104) <= port_num;
    elsif rising_edge(clock_a) then
        -- move registers to local variable
        for I in 0 to 1 loop
            wrd512b(I) := word_LLDP(I);
        end loop;

        for I in 0 to 7 loop
            wrd64b(I)   := word_LLDP(0)(511-(I*64) downto 448-(I*64));
            wrd64b(8+I) := word_LLDP(1)(511-(I*64) downto 448-(I*64));
        end loop;

        -- Change the local variable
        for I in 63 downto 0 loop
           -- if wren_a = '1' then
                if byteena_a(I) = '1' then
                    if address_a = "0" then
                        wrd512b(0)((8*I)+7 downto (8*I)) := data_a((8*I)+7 downto (8*I));
                    else
                        wrd512b(1)((8*I)+7 downto (8*I)) := data_a((8*I)+7 downto (8*I));
                    end if;
                end if;
           -- end if;
        end loop;

        for J in 0 to 15 loop
            for I in 0 to 7 loop
               -- if (WE_B_wait_for_free = '1' and wren_a = '0')  then
                    if address_b = std_logic_vector(TO_UNSIGNED(J,4))  then
                        if byteena_b(I) = '1' then
                            wrd64b(J)((I*8)+7 downto I*8) := data_b((I*8)+7 downto I*8);
                        end if;
                    end if;
              --  end if;
            end loop;
        end loop;

        -- update register with local variable if we write
       if wren_a = '1' then
           for I in 0 to 1 loop
                word_LLDP(I) <=  wrd512b(I);
           end loop;
        elsif (WE_B_wait_for_free = '1' and wren_a = '0')  then
            for I in 0 to 7 loop
                word_LLDP(0)(511-(I*64) downto 448-(I*64)) <= wrd64b(I);
                word_LLDP(1)(511-(I*64) downto 448-(I*64)) <= wrd64b(8+I);
            end loop;
        end if;
	end if;
 end process;

 -- read out the registers to LLDP
 process(word_LLDP,address_a)
 begin
   --if rising_edge(clock_a) then
        if address_a = "0" then
            reg_out_LLDP <= word_LLDP(0);
        else
            reg_out_LLDP <= word_LLDP(1);
        end if;
 -- end if;
 end process;

 q_a    <= reg_out_LLDP;

 -- read out the registers to control
 process(clock_b)
 begin
    if rising_edge(clock_b) then
        for I in 0 to 7 loop
            if address_b(3) = '0' then
                if address_b(2 downto 0) = std_logic_vector(TO_UNSIGNED(I,3)) then
                    reg_out_ctrl <=  word_LLDP(0)(511-(I*64) downto 448-(I*64));
                end if;
            else
                if address_b(2 downto 0) = std_logic_vector(TO_UNSIGNED(I,3)) then
                    reg_out_ctrl <=  word_LLDP(1)(511-(I*64) downto 448-(I*64));
                end if;
            end if;
        end loop;
    end if;
 end process;

 q_b    <= reg_out_ctrl;

end behavioral;
