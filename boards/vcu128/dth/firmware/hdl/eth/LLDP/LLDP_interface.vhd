LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;

--ENTITY-----------------------------------------------------------
entity LLDP_interface is
    generic (
     port_num                       : std_logic_vector(7 downto 0) ;
     addr_offset_100G_eth           : integer;
     Simulation                     : boolean := false
    );
	port
	(
		resetn						: IN STD_LOGIC;
		usr_clk						: IN STD_LOGIC;
		usr_DT						: IN STD_LOGIC_VECTOR(63 downto 0);
		usr_func_wr					: IN STD_LOGIC_VECTOR(16383 downto 0);
		usr_wr						: IN STD_LOGIC;
		usr_func_rd					: IN STD_LOGIC_VECTOR(16383 downto 0);
		usr_rden					: in std_logic;
		usr_out						: OUT STD_LOGIC_VECTOR(63 downto 0);

		Low_clock					: IN STD_LOGIC;
		Clock_eth					: IN STD_LOGIC;
		MAC_itf_ready               : IN STD_LOGIC;
		MAC_src						: IN STD_LOGIC_VECTOR(47 downto 0);
		MAC_src_p0					: IN STD_LOGIC_VECTOR(47 downto 0);
		wr_MAC_to_LLDP_mem			: IN STD_LOGIC;	-- pulse sync on ETH clock MAC value ready

		req_LLDP					: OUT STD_LOGIC;
		LLDP_data					: OUT STD_LOGIC_VECTOR(511 downto 0);
		LLDP_last_data				: OUT STD_LOGIC;
		rd_LLDP_data				: IN STD_LOGIC

	 );
end LLDP_interface;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of LLDP_interface is
--	signal Q : std_logic;

signal LLDP_counter_Zero	: std_logic;
signal LLDP_stop_to_send	: std_logic;
signal LLDP_stop_to_send_ethclk	: std_logic;

signal word_counter			: std_logic_vector(7 downto 0);
signal word_counter_setup	: std_logic_vector(7 downto 0) := x"0E";
signal Mem_PCKT_LLDP		: std_logic_vector(7 downto 0) ;
signal LLDP_mem_add			: std_logic_vector(6 downto 0);
signal LLDP_mem_wr_dt		: std_logic_vector(63 downto 0);
signal LLDP_mem_rd_dt		: std_logic_vector(63 downto 0);
signal LLDP_mem_ena			: std_logic_vector(7 downto 0);
signal LLDP_wr_pulse		: std_logic;

signal Eth_LLDP_add			: std_logic_vector(0 downto 0);
signal LLDP_timeout_cnt		: std_logic_vector(31 downto 0);
signal LLDP_timeout_val		: std_logic_vector(31 downto 0);

signal usr_out_reg			: std_logic_vector(63 downto 0);
signal req_timeout_counter	: std_logic_vector(31 downto 0) := x"00000000";
signal req_LLDP_org			: std_logic:= '0';
signal req_LLDP_org_resync	: std_logic:= '0';
signal req_LLDP_cell		: std_logic := '0';
signal req_delay			: std_logic;

signal LLDP_data_write_MAC	: std_logic_vector(511 downto 0) ;
signal LLDP_wr_MAC			: std_logic;
signal LLDP_ena_wr_MAC		: std_logic_vector(63 downto 0);

signal Start_LLDP_pckt		: std_logic := '0';
signal Start_LLDP_pckt_resync: std_logic_vector(1 downto 0) := "00";
signal MAC_ready_sync        : std_logic_vector(1 downto 0) := "00";

signal resetn_clketh_resync   		: std_logic;
signal last_word_LLDP		         : std_logic;
signal wr_MAC_to_LLDP_mem_sync		: std_logic;

attribute mark_debug : string;
attribute mark_debug of LLDP_wr_MAC             : signal is "true";
attribute mark_debug of MAC_src_p0              : signal is "true";
attribute mark_debug of MAC_src            	    : signal is "true";
attribute mark_debug of Eth_LLDP_add          	: signal is "true";
attribute mark_debug of LLDP_data          	: signal is "true";
attribute mark_debug of LLDP_last_data          	: signal is "true";
attribute mark_debug of req_LLDP          	 : signal is "true";
attribute mark_debug of rd_LLDP_data          	: signal is "true";
--*****************************************************************
--*************<     CODE START HERE    >**************************
--*****************************************************************
 begin

--******************************************************************************
-- Enable Request on Timeout only when MAC has been updateed in the LLDP memory

resync_req_LLDP:entity work.resync_pulse
	port map(
		aresetn				=> '1',
		clocki			    => usr_clk,
		in_s				=> wr_MAC_to_LLDP_mem,
		clocko			    => Clock_eth,
		out_s			    => wr_MAC_to_LLDP_mem_sync
		);


process(Clock_eth)
begin
	if rising_edge(Clock_eth) then
		if wr_MAC_to_LLDP_mem_sync = '1'then
			Start_LLDP_pckt	<= '1';

		end if;
	end if;
end process;

--******************************************************************************
-- READ/WRITE Control Access for function

process(resetn,usr_clk)
begin
	if rising_edge(usr_clk) then
		if  usr_wr = '1' then
			if usr_func_wr(LLDP_word_count) = '1' then
				word_counter_setup				<= usr_DT(7 downto 0); -- initialisew by FPGA load to x"0E";
			end if;
		end if;
	end if;
end process;

LLDP_timeout_val <=  x"00000050"  when Simulation else x"003B9ACA";

process(resetn,usr_clk)
begin
	if resetn = '0' then
		LLDP_mem_add		<= (others => '0');
		LLDP_mem_wr_dt		<= (others => '0');
		LLDP_wr_pulse		<= '0' ;
		LLDP_timeout_cnt	<= LLDP_timeout_val;
	elsif rising_edge(usr_clk) then
		LLDP_wr_pulse		<= '0';

		if  usr_wr = '1' then


			if usr_func_wr(LLDP_packet_add + addr_offset_100G_eth) = '1' then
				LLDP_mem_add					<= usr_DT(6 downto 0);
			end if;

			if usr_func_wr(LLDP_packet_data + addr_offset_100G_eth) = '1' then
				LLDP_mem_wr_dt(63 downto 56)	<= usr_DT(07 downto 00);
				LLDP_mem_wr_dt(55 downto 48)	<= usr_DT(15 downto 08);
				LLDP_mem_wr_dt(47 downto 40)	<= usr_DT(23 downto 16);
				LLDP_mem_wr_dt(39 downto 32)	<= usr_DT(31 downto 24);
				LLDP_mem_wr_dt(31 downto 24)	<= usr_DT(39 downto 32);
				LLDP_mem_wr_dt(23 downto 16)	<= usr_DT(47 downto 40);
				LLDP_mem_wr_dt(15 downto 08)	<= usr_DT(55 downto 48);
				LLDP_mem_wr_dt(07 downto 00)	<= usr_DT(63 downto 56);
				LLDP_wr_pulse					<= '1';

				LLDP_mem_ena(7 downto 0) 	<= x"FF";
			end if;

			if usr_func_wr(LLDP_timeout + addr_offset_100G_eth) = '1' then
				LLDP_timeout_cnt 		<= usr_DT(31 downto 0);
			end if;

		end if;
	end if;
end process;

process(usr_clk)
begin
	if rising_edge(usr_clk) then
		usr_out_reg		<= (others => '0');
		if   usr_func_rd(LLDP_word_count + addr_offset_100G_eth) = '1' then
			usr_out_reg(7 downto 0) 		<= word_counter_setup;
		elsif usr_func_rd(LLDP_packet_add + addr_offset_100G_eth) = '1' then
			usr_out_reg(6 downto 0)		<= LLDP_mem_add;
		elsif usr_func_rd(LLDP_packet_data + addr_offset_100G_eth) = '1' then
				usr_out_reg(07 downto 00)	<= LLDP_mem_rd_dt(63 downto 56);
				usr_out_reg(15 downto 08)	<= LLDP_mem_rd_dt(55 downto 48);
				usr_out_reg(23 downto 16)	<= LLDP_mem_rd_dt(47 downto 40);
				usr_out_reg(31 downto 24)	<= LLDP_mem_rd_dt(39 downto 32);
				usr_out_reg(39 downto 32)	<= LLDP_mem_rd_dt(31 downto 24);
				usr_out_reg(47 downto 40)	<= LLDP_mem_rd_dt(23 downto 16);
				usr_out_reg(55 downto 48)	<= LLDP_mem_rd_dt(15 downto 08);
				usr_out_reg(63 downto 56)	<= LLDP_mem_rd_dt(07 downto 00);

		elsif usr_func_rd(LLDP_timeout + addr_offset_100G_eth) = '1' then
			usr_out_reg(31 downto 0) 		<= LLDP_timeout_cnt;
		end if;
	end if;
end process;

usr_out	<= usr_out_reg;

--******************************************************************************
-- LLDP memory
reset_resync_i1:entity work.resetn_resync
port map(
	aresetn			=> resetn,
	clock			=> Clock_eth,
	Resetn_sync		=> resetn_clketh_resync
	);

LLDP_memory_i1:entity work.LLDP_block_mem
    GENERIC MAP (port_num => port_num)
	PORT MAP
	(
	    reset_n         => resetn_clketh_resync,
		clock_a			=> Clock_eth,
		address_a		=> Eth_LLDP_add,
		byteena_a		=> LLDP_ena_wr_MAC,
		data_a			=> LLDP_data_write_MAC,
		wren_a			=> LLDP_wr_MAC,
		q_a				=> LLDP_data,

		clock_b			=> usr_clk,
		address_b		=> LLDP_mem_add(3 downto 0),
		data_b			=> LLDP_mem_wr_dt ,
		byteena_b		=> LLDP_mem_ena,
		wren_b			=> LLDP_wr_pulse,
		q_b				=> LLDP_mem_rd_dt
	);

--address LLDP read to send data on Ethernet
process(Clock_eth)
begin
	if rising_edge(Clock_eth) then
		if		wr_MAC_to_LLDP_mem_sync	= '1' then
			Eth_LLDP_add	<= "0";

		elsif req_LLDP_cell = '1' and req_delay = '0' then
			Eth_LLDP_add	<= (others => '0');
		elsif rd_LLDP_data = '1' then
			Eth_LLDP_add	<= Eth_LLDP_add + '1';
		end if;

		req_delay	<= req_LLDP_cell;
	end if;
end process;

process(Low_clock)
begin
	if rising_edge(Low_clock) then
		if req_LLDP_org = '1'  then
			Mem_PCKT_LLDP	<= word_counter_setup;
		end if;
	end if;
end process;

last_word_LLDP	<= '1' when Eth_LLDP_add = "1"  else '0';
LLDP_last_data	<= last_word_LLDP;


--******************************************************************************
-- Logic to write local MAC in the LLDP memory
process(Clock_eth)
begin
	if rising_edge(Clock_eth) then
		LLDP_wr_MAC			 		<= '0';
		LLDP_ena_wr_MAC				<= (others => '0');
		LLDP_data_write_MAC			<= (others => '0');

		if wr_MAC_to_LLDP_mem_sync = '1'  then--and Start_LLDP_pckt = '0'
			LLDP_ena_wr_MAC						<= x"00007E3F00000000";
			LLDP_wr_MAC		 					<= '1';
			LLDP_data_write_MAC(375 downto 328)	<= MAC_src_p0;
			LLDP_data_write_MAC(303 downto 256)	<= MAC_src;
		end if;
	end if;
end process;


--******************************************************************************
-- Counter to send in timeout time the LLDP packet (request)
process(Low_clock)
begin
    if rising_edge(Low_clock) then
        LLDP_counter_Zero	<= '0';
        if word_counter_setup /= x"00" then -- stop to send LLDP packet if length is set to 0
             LLDP_counter_Zero	<= '1';
        end if;
    end if;
end process;

resync_reset_i1:entity work.resetn_resync
port map(
	aresetn			=> LLDP_counter_Zero,
	clock			=> Low_clock,
	Resetn_sync		=> LLDP_stop_to_send
	);

resync_reset_i2:entity work.resetn_resync
port map(
	aresetn			=> LLDP_counter_Zero,
	clock			=> Clock_eth,
	Resetn_sync		=> LLDP_stop_to_send_ethclk
	);

sig_resync_i1:entity work.resync_pulse
port map(
	aresetn			=> LLDP_stop_to_send_ethclk,
	clocki			=> Low_clock,
	in_s			=> req_LLDP_org,
	clocko			=> Clock_eth,
	out_s			=> req_LLDP_org_resync
	);


process(LLDP_stop_to_send,Low_clock)
begin
	if LLDP_stop_to_send = '0' then
		req_timeout_counter				<= (others => '0');
		req_LLDP_org					<= '0';
		Start_LLDP_pckt_resync          <= "00";
	elsif rising_edge(Low_clock) then
		if Start_LLDP_pckt_resync(1) = '1' and MAC_ready_sync(1) = '1' then
			if req_LLDP_org = '1' then
				req_timeout_counter		<= (others => '0');
			else
				req_timeout_counter 	<= req_timeout_counter + '1';
			end if;

			req_LLDP_org				<= '0';
			if req_timeout_counter = LLDP_timeout_cnt then -- do a request on timeout
				req_LLDP_org			<= '1';
			end if;

		end if;

		MAC_ready_sync(1)	        <= MAC_ready_sync(0);
		MAC_ready_sync(0)	        <= MAC_itf_ready;
		Start_LLDP_pckt_resync(1)	<= Start_LLDP_pckt_resync(0);
		Start_LLDP_pckt_resync(0)	<= Start_LLDP_pckt;
	end if;
end process;

process(Clock_eth)
begin
	if rising_edge(Clock_eth) then
		if req_LLDP_org_resync = '1' then --maintains the request until the ETH logic read the LLDP memory
			req_LLDP_cell	<= '1';
		elsif  rd_LLDP_data = '1'  then
			req_LLDP_cell	<= '0';
		end if;
	end if;
end process;

req_LLDP	<= req_LLDP_cell;

end behavioral;
