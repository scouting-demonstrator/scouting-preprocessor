------------------------------------------------------------------------------
--                     Probe ARP                                            --
------------------------------------------------------------------------------
--  Transfer a data block to SERDES		 
--   
--   Dominique Gigi 2014		 
--								 
--   ver 1.00							 
--  Probe ARP state machine
-- 
-- 
--
--
--
------------------------------------------------------------------------------
LIBRARY ieee; 

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all; 
	
entity probe_ARP is
 
	port
	(reset									    : in std_LOGIC;
	 CLOCK_tx									: in std_LOGIC;--xgmii 
	 Low_clk									: in std_logic;
	 					
	 probe_ARP_req							    : in std_logic;
	 req_ARP									: in std_logic;
	 MAC_S									    : in std_logic_vector(47 downto 0);
	 IP_Gateway								    : in std_logic_vector(31 downto 0);
	 ARP_acc_done							    : in std_logic;  -- ACTIVE LOW
	 IP_dst_and_IP_nwk_eq_Net_Dst		        : in std_logic;
	 
	 
	 Req_acc_ARP							    : out std_logic;
	 ARP_gw									    : out std_logic;
	 ARP_dst									: out std_logic;
	 ARP_probe								    : out std_logic;
	 probe_DONE								    : out std_logic;
	 ARP_req_Done							    : out std_logic;
	 reset_low_clock						    : out std_LOGIC
				
	 );
end probe_ARP;

architecture behavioral of probe_ARP is

type probe_SM_type is ( idle,
						wait_0,
						first_req,
						wait_10,
						wait_11,
						second_req,
						wait_20,
						wait_21,
						third_req,
						wait_30,
						wait_31,
						fourth_req,
						done,
						ARP_GateWay,
						fifth,
						ARP_Dest,
						ARP_Done
					);
signal probe_SM,probe_SMNext:probe_SM_type;

component resync_v4 is
port (
	aresetn				: in std_logic;
	clocki			: in std_logic;	
	input				: in std_logic;
	clocko			: in std_logic;
	output			: out std_logic
	);
end component;

component resync_sig_gen is 
port ( 
	clocko				: in std_logic;
	input				: in std_logic;
	output				: out std_logic
	);
end component;

component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic; 
	Resetn_sync		   : out std_logic
	);
end component;	
	
signal timer						: std_logic_vector(15 downto 0);
signal end_time					: std_logic;
signal probe_ARP_reg				: std_logic;
signal probe_DONE_reg			: std_logic;
signal probe_ARP_cell			: std_logic;
signal mem_req_ARP				: std_logic;
signal ARP_rst_set_values		: std_logic;
signal ARP_gw_reg					: std_logic;
signal ARP_dst_reg				: std_logic;
signal ARP_probe_reg				: std_logic;
signal check_IP_GW				: std_logic;
signal GRst_low_clk				: std_logic; 
signal reset_timer				: std_logic;
signal IP_dst_and_IP_nwk_eq_Net_Dst_cell	: std_logic;
--###########################################
--######  BEGIN   CODE 
--###########################################
begin

resync_reset_i1:resetn_resync 
port map(
	aresetn				=> reset,
	clock				=> low_clk, 
	Resetn_sync		    => GRst_low_clk 
	);
	
reset_low_clock	<= GRst_low_clk;	

--***** Check if gateway is set ?
process(GRst_low_clk,low_clk)
begin
if GRst_low_clk = '0' then
	check_IP_GW			 	<= '0';
elsif rising_edge(low_clk) then
	check_IP_GW 			<= '0';
	if IP_Gateway /= x"00000000" then  -- I don't check if only one byte is null
		check_IP_GW 		<= '1';
	end if;
end if;
end process;

resync_sig_i1:resync_sig_gen
	port map(
		input		  => IP_dst_and_IP_nwk_eq_Net_Dst,		
		clocko		  => low_clk,
		output		  => IP_dst_and_IP_nwk_eq_Net_Dst_cell
		);

--##########################################
-- state machine for ARP probe sequence
probe_SM0:process(low_clk,GRst_low_clk)
begin
	if GRst_low_clk = '0' then
		probe_SM 		<= idle;
	elsif rising_edge(low_clk) then
		probe_SM 		<= probe_SMNext;
	end if;
end process;



probe_p:process(probe_SM,probe_ARP_req,end_time,req_ARP,check_IP_GW,IP_dst_and_IP_nwk_eq_Net_Dst_cell) 
begin 
probe_SMNext <= probe_SM;
Case probe_SM is
	when idle =>
		if probe_ARP_req = '1' then
			probe_SMNext <= wait_0;
		elsif req_ARP = '1' then
			if  check_IP_GW = '1' then
				probe_SMNext <= ARP_GateWay;
			elsif IP_dst_and_IP_nwk_eq_Net_Dst_cell = '1' then
				probe_SMNext <= ARP_Dest;
			else
				probe_SMNext <= ARP_Done;
			end if;
		end if;
	when wait_0 =>
		if end_time = '1' then
			probe_SMNext 	<= first_req;
		end if;
		
	when first_req =>
		 probe_SMNext 		<= wait_10;
		 
	when wait_10 =>
		if end_time = '1' then
			probe_SMNext 	<= wait_11;
		end if;
		
	when wait_11 =>
		if end_time = '1' then
			probe_SMNext 	<= second_req;
		end if;
		
	when second_req =>
		probe_SMNext 		<= wait_20;
		
	when wait_20 =>
		if end_time = '1' then
			probe_SMNext 	<= wait_21;
		end if;
		
	when wait_21 =>
		if end_time = '1' then
			probe_SMNext 	<= third_req;
		end if;
		
	when third_req =>
		probe_SMNext 		<= wait_30;

	when wait_30 =>
		if end_time = '1' then
			probe_SMNext	<= wait_31;
		end if;
	when wait_31 =>
		if end_time = '1' then
			probe_SMNext 	<= fourth_req;
		end if;
		
	when fourth_req =>
		probe_SMNext 		<= done;
	
	when done  =>		
		probe_SMNext 		<= Idle;
		
	when ARP_GateWay =>
		probe_SMNext		<= fifth;
		
	when fifth =>
		if end_time = '1' then
			if IP_dst_and_IP_nwk_eq_Net_Dst_cell = '1' then
				probe_SMNext	<= ARP_Dest;
			else
				probe_SMNext	<= ARP_Done;
			end if;
		end if;
	when ARP_Dest =>
		probe_SMNext		<= ARP_Done;		 
	
	when ARP_Done =>
		probe_SMNext		<= Idle;	
	
	when others =>
		probe_SMNext 		<= Idle;
	end case;
end process;


--##################################################
-- timer  50 ms  (low_clk = 2,5 uS)  => load x4E20 (20000 unit of 2,5 uS)
process( low_clk,MAC_S)
begin
-- remove reset from the logic because state_machine is Idle on reset
	if rising_edge(low_clk) then
		end_time 			<= '0';
		if reset_timer = '1' then
			timer( 2 downto  0)	<= (others => '0');
			timer(14 downto  3)	<= MAC_S(11 downto 0);
			timer(15)			<= '0';
			end_time 			<= '0';
		elsif timer = "0000" then
			timer 			<=  x"4E20";
			end_time 		<= '1';
		else	
			timer 			<= timer - '1';
		end if;
	end if;
end process;

reset_timer	<= '1'  when probe_SM = Idle else '0';

--#################################################
-- request  ARP Access
process(GRst_low_clk,low_clk)
begin
if GRst_low_clk = '0' then
	probe_ARP_reg		<= '0';
	probe_DONE_reg		<= '0';
elsif rising_edge(low_clk) then
	probe_ARP_reg		<= '0';
	if  probe_SM = first_req or probe_SM = second_req or probe_SM = third_req or probe_SM = fourth_req or probe_SM = ARP_GateWay or probe_SM = ARP_Dest then
		probe_ARP_reg 	<= '1';
	end if;
	
	if probe_SM = done then
		probe_DONE_reg 	<= '1';
	elsif  probe_ARP_req = '1' then
		probe_DONE_reg 	<= '0';
	end if;
	
end if;
end process;

--#################################################
-- specify the ARP request (probe, ARP Dest ARP gateway?
	
resync_reset_i2:resetn_resync 
port map(
	aresetn				=> ARP_acc_done,
	clock				=> low_clk, 
	Resetn_sync		    => ARP_rst_set_values 
	);
	
process(GRst_low_clk,ARP_rst_set_values,low_clk)
begin
if ARP_rst_set_values = '0' or GRst_low_clk = '0' then
	ARP_gw_reg			<= '0';
	ARP_dst_reg			<= '0';
	ARP_probe_reg		<= '0';
elsif rising_edge(low_clk) then
	if probe_SM = ARP_GateWay then
		ARP_gw_reg		<= '1';
	end if;
	
	if probe_SM = ARP_Dest then
		ARP_dst_reg		<= '1';
	end if;
	
	if  probe_SM = first_req or probe_SM = second_req or probe_SM = third_req or probe_SM = fourth_req	then
		ARP_probe_reg	<= '1';
	end if;
end if;
end process;

-- resync signal to 10Gb clock
i1_clklow_xgmii:resync_v4 
port map(
	aresetn				=> GRst_low_clk,
	clocki				=> low_clk,
	input				=> probe_ARP_reg,
	clocko				=> CLOCK_tx,
	output				=> probe_ARP_cell
	);	
	
process(reset,CLOCK_tx)
begin
if reset = '0' then
	mem_req_ARP 		<= '0';
elsif rising_edge(CLOCK_tx) then
	if probe_ARP_cell = '1' then
		mem_req_ARP 	<= '1';
	elsif ARP_acc_done = '0'  then
		mem_req_ARP 	<= '0';
	end if;
end if;
end process;

Req_acc_ARP				<= mem_req_ARP;
ARP_gw					<= ARP_gw_reg;
ARP_dst					<= ARP_dst_reg;
ARP_probe				<= ARP_probe_reg;
probe_DONE 				<= probe_DONE_reg;

ARP_req_Done			<= '1' when probe_SM = ARP_Done else '0';


end behavioral;
 
 
 