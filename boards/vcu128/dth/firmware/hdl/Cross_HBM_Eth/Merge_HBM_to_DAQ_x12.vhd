----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 05.02.2021 11:28:33
-- Design Name:
-- Module Name: Merge_HBM_to_DAQ - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.address_table.all;
use work.interface.all;





entity Merge_HBM_to_DAQ_x12 is
    generic (
        addr_offset_100g_eth        : integer := 0;
        addr_offset_hbm_item        : integer := 0
    );
    port (
        usr_clk                     : in  std_logic;
        usr_rst_n                   : in  std_logic;
        usr_func_wr                 : in  std_logic_vector(16383 downto 0);
        usr_wen                     : in  std_logic;
        usr_data_wr                 : in  std_logic_vector(63 downto 0);

        usr_func_rd                 : in  std_logic_vector(16383 downto 0);
        usr_rden                    : in  std_logic;
        usr_data_rd                 : out std_logic_vector(63 downto 0);

        TCP_clock_resetn            : in  std_logic;
        TCP_Clock                   : in  std_logic;    -- Clock used from HBM to FIFO in front of the MAC 100gbs
        resetp_counter_timer_resync : out std_logic;

        -- Bus merging data source from TCP/HBM block
        TCPxx_BLOCK_TO_SEND         : out HBM_to_TCP_block_record;
        ETH_TCP_x_packet_done       : in  std_logic;

        -- Input record (coming from TCP/HBM logic  when a block is ready to be sent
        TCP00_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP00_gnt_access            : out std_logic;
        TCP00_packet_done           : out std_logic;

        TCP01_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP01_gnt_access            : out std_logic;
        TCP01_packet_done           : out std_logic;

        TCP02_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP02_gnt_access            : out std_logic;
        TCP02_packet_done           : out std_logic;

        TCP03_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP03_gnt_access            : out std_logic;
        TCP03_packet_done           : out std_logic;

        TCP04_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP04_gnt_access            : out std_logic;
        TCP04_packet_done           : out std_logic;

        TCP05_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP05_gnt_access            : out std_logic;
        TCP05_packet_done           : out std_logic;

        TCP06_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP06_gnt_access            : out std_logic;
        TCP06_packet_done           : out std_logic;

        TCP07_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP07_gnt_access            : out std_logic;
        TCP07_packet_done           : out std_logic;

        TCP08_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP08_gnt_access            : out std_logic;
        TCP08_packet_done           : out std_logic;

        TCP09_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP09_gnt_access            : out std_logic;
        TCP09_packet_done           : out std_logic;

        TCP10_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP10_gnt_access            : out std_logic;
        TCP10_packet_done           : out std_logic;

        TCP11_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP11_gnt_access            : out std_logic;
        TCP11_packet_done           : out std_logic
    );
end Merge_HBM_to_DAQ_x12;





architecture Behavioral of Merge_HBM_to_DAQ_x12 is

    component resetp_resync is
        port (
            aresetp     : in  std_logic;
            clock       : in  std_logic;
            Resetp_sync : out std_logic
        );
    end component;

    component resync_v4
        port (
            aresetn : in  std_logic;
            clocki  : in  std_logic;
            clocko  : in  std_logic;
            input   : in  std_logic;
            output  : out std_logic
        );
    end component;

    signal round_robin                      : std_logic_vector(5 downto 0);
    signal req_stream                       : std_logic_vector(11 downto 0);
    signal gnt_stream                       : std_logic_vector(11 downto 0);
    attribute dont_touch                    : string;
    attribute dont_touch of gnt_stream : signal is "true";
    attribute dont_touch of round_robin : signal is "true";

    signal counter_A                        : std_logic_vector(63 downto 0);
    signal counter_B                        : std_logic_vector(63 downto 0);
    signal counter_C                        : std_logic_vector(63 downto 0);
    signal counter_D                        : std_logic_vector(63 downto 0);
    signal counter_E                        : std_logic_vector(63 downto 0);
    signal counter_F                        : std_logic_vector(63 downto 0);
    signal counter_G                        : std_logic_vector(63 downto 0);
    signal counter_H                        : std_logic_vector(63 downto 0);
    signal counter_I                        : std_logic_vector(63 downto 0);
    signal counter_J                        : std_logic_vector(63 downto 0);
    signal counter_K                        : std_logic_vector(63 downto 0);
    signal counter_L                        : std_logic_vector(63 downto 0);
    signal counter_free                     : std_logic_vector(63 downto 0);

    signal resetp_counter_timer             : std_logic;
    signal resetp_counter_timer_resync_cell : std_logic;

    signal latch_counter_A                  : std_logic_vector(63 downto 0);
    signal latch_counter_B                  : std_logic_vector(63 downto 0);
    signal latch_counter_C                  : std_logic_vector(63 downto 0);
    signal latch_counter_D                  : std_logic_vector(63 downto 0);
    signal latch_counter_E                  : std_logic_vector(63 downto 0);
    signal latch_counter_F                  : std_logic_vector(63 downto 0);
    signal latch_counter_G                  : std_logic_vector(63 downto 0);
    signal latch_counter_H                  : std_logic_vector(63 downto 0);
    signal latch_counter_I                  : std_logic_vector(63 downto 0);
    signal latch_counter_J                  : std_logic_vector(63 downto 0);
    signal latch_counter_K                  : std_logic_vector(63 downto 0);
    signal latch_counter_L                  : std_logic_vector(63 downto 0);
    signal latch_counter_free               : std_logic_vector(63 downto 0);

    signal latch_counter_timer              : std_logic;
    signal latch_counter_timer_resync       : std_logic;

    signal data_in_rg                       : std_logic_vector(63 downto 0);
    signal internal_register                : HBM_to_TCP_block_record;
    attribute dont_touch of internal_register : signal is "true";


    signal TCPxx_packet_done_reg            : std_logic_vector(11 downto 0);
    attribute dont_touch of TCPxx_packet_done_reg : signal is "true";

begin

    req_stream(00) <= '1' when TCP00_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(01) <= '1' when TCP01_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(02) <= '1' when TCP02_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(03) <= '1' when TCP03_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(04) <= '1' when TCP04_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(05) <= '1' when TCP05_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(06) <= '1' when TCP06_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(07) <= '1' when TCP07_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(08) <= '1' when TCP08_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(09) <= '1' when TCP09_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(10) <= '1' when TCP10_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';
    req_stream(11) <= '1' when TCP11_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';



    process(TCP_clock_resetn,TCP_clock)
    begin
        if TCP_clock_resetn        = '0' then
            round_robin    <= (others => '0');
            gnt_stream    <= (others => '0');
        elsif rising_edge(TCP_Clock) then
            if    req_stream(00) = '1'  and round_robin = "000000"  then gnt_stream(00) <= '1';
            elsif req_stream(01) = '1'  and round_robin = "000100"  then gnt_stream(01) <= '1';
            elsif req_stream(02) = '1'  and round_robin = "001000"  then gnt_stream(02) <= '1';
            elsif req_stream(03) = '1'  and round_robin = "001100"  then gnt_stream(03) <= '1';
            elsif req_stream(04) = '1'  and round_robin = "010000"  then gnt_stream(04) <= '1';
            elsif req_stream(05) = '1'  and round_robin = "010100"  then gnt_stream(05) <= '1';
            elsif req_stream(06) = '1'  and round_robin = "011000"  then gnt_stream(06) <= '1';
            elsif req_stream(07) = '1'  and round_robin = "011100"  then gnt_stream(07) <= '1';
            elsif req_stream(08) = '1'  and round_robin = "100000"  then gnt_stream(08) <= '1';
            elsif req_stream(09) = '1'  and round_robin = "100100"  then gnt_stream(09) <= '1';
            elsif req_stream(10) = '1'  and round_robin = "101000"  then gnt_stream(10) <= '1';
            elsif req_stream(11) = '1'  and round_robin = "101100"  then gnt_stream(11) <= '1';
            elsif ETH_TCP_x_packet_done = '1'                       then gnt_stream    <= (others => '0');
            end if;

            if gnt_stream = "000000000000" then
                round_robin <= round_robin + '1';
            end if;
        end if;
    end process;



    process(TCP_clock_resetn, TCP_clock)
    begin
        if TCP_clock_resetn = '0' then
            TCPxx_packet_done_reg <= (others => '0');
        elsif rising_edge(TCP_Clock) then
            TCPxx_packet_done_reg <= (others => '0');
            if ETH_TCP_x_packet_done = '1' then
                if gnt_stream(00) = '1' then TCPxx_packet_done_reg(00) <= '1'; end if;
                if gnt_stream(01) = '1' then TCPxx_packet_done_reg(01) <= '1'; end if;
                if gnt_stream(02) = '1' then TCPxx_packet_done_reg(02) <= '1'; end if;
                if gnt_stream(03) = '1' then TCPxx_packet_done_reg(03) <= '1'; end if;
                if gnt_stream(04) = '1' then TCPxx_packet_done_reg(04) <= '1'; end if;
                if gnt_stream(05) = '1' then TCPxx_packet_done_reg(05) <= '1'; end if;
                if gnt_stream(06) = '1' then TCPxx_packet_done_reg(06) <= '1'; end if;
                if gnt_stream(07) = '1' then TCPxx_packet_done_reg(07) <= '1'; end if;
                if gnt_stream(08) = '1' then TCPxx_packet_done_reg(08) <= '1'; end if;
                if gnt_stream(09) = '1' then TCPxx_packet_done_reg(09) <= '1'; end if;
                if gnt_stream(10) = '1' then TCPxx_packet_done_reg(10) <= '1'; end if;
                if gnt_stream(11) = '1' then TCPxx_packet_done_reg(11) <= '1'; end if;
            end if;
        end if;
    end process;

    TCP00_packet_done <= TCPxx_packet_done_reg(00);
    TCP01_packet_done <= TCPxx_packet_done_reg(01);
    TCP02_packet_done <= TCPxx_packet_done_reg(02);
    TCP03_packet_done <= TCPxx_packet_done_reg(03);
    TCP04_packet_done <= TCPxx_packet_done_reg(04);
    TCP05_packet_done <= TCPxx_packet_done_reg(05);
    TCP06_packet_done <= TCPxx_packet_done_reg(06);
    TCP07_packet_done <= TCPxx_packet_done_reg(07);
    TCP08_packet_done <= TCPxx_packet_done_reg(08);
    TCP09_packet_done <= TCPxx_packet_done_reg(09);
    TCP10_packet_done <= TCPxx_packet_done_reg(10);
    TCP11_packet_done <= TCPxx_packet_done_reg(11);



    process(TCP_clock)
    begin
        if rising_edge(TCP_clock) then
            if ETH_TCP_x_packet_done = '1' then
                internal_register.Request_TCP_packet <= '0';

            elsif gnt_stream(00) = '1' then internal_register    <= TCP00_BLOCK_TO_SEND;
            elsif gnt_stream(01) = '1' then internal_register    <= TCP01_BLOCK_TO_SEND;
            elsif gnt_stream(02) = '1' then internal_register    <= TCP02_BLOCK_TO_SEND;
            elsif gnt_stream(03) = '1' then internal_register    <= TCP03_BLOCK_TO_SEND;
            elsif gnt_stream(04) = '1' then internal_register    <= TCP04_BLOCK_TO_SEND;
            elsif gnt_stream(05) = '1' then internal_register    <= TCP05_BLOCK_TO_SEND;
            elsif gnt_stream(06) = '1' then internal_register    <= TCP06_BLOCK_TO_SEND;
            elsif gnt_stream(07) = '1' then internal_register    <= TCP07_BLOCK_TO_SEND;
            elsif gnt_stream(08) = '1' then internal_register    <= TCP08_BLOCK_TO_SEND;
            elsif gnt_stream(09) = '1' then internal_register    <= TCP09_BLOCK_TO_SEND;
            elsif gnt_stream(10) = '1' then internal_register    <= TCP10_BLOCK_TO_SEND;
            elsif gnt_stream(11) = '1' then internal_register    <= TCP11_BLOCK_TO_SEND;
            end if;
        end if;
    end process;

    TCP00_gnt_access <= gnt_stream(00);
    TCP01_gnt_access <= gnt_stream(01);
    TCP02_gnt_access <= gnt_stream(02);
    TCP03_gnt_access <= gnt_stream(03);
    TCP04_gnt_access <= gnt_stream(04);
    TCP05_gnt_access <= gnt_stream(05);
    TCP06_gnt_access <= gnt_stream(06);
    TCP07_gnt_access <= gnt_stream(07);
    TCP08_gnt_access <= gnt_stream(08);
    TCP09_gnt_access <= gnt_stream(09);
    TCP10_gnt_access <= gnt_stream(10);
    TCP11_gnt_access <= gnt_stream(11);

    TCPxx_BLOCK_TO_SEND <= internal_register;



    --##############################################################
    -- Control

    process(usr_clk)
    begin
        if rising_edge(usr_clk) then
            latch_counter_timer <= '0';
            resetp_counter_timer <= '0';

            if usr_wen = '1' then
                if usr_func_wr(HBM_status + addr_offset_hbm_item) = '1' then
                    latch_counter_timer  <= usr_data_wr(30);
                    resetp_counter_timer <= usr_data_wr(31);
                end if;
            end if;
        end if;
    end process;

    process(usr_clk)
    begin
        if rising_edge(usr_clk) then
            data_in_rg <= (others => '0');

            if    usr_func_rd(measure_counter_merger_A + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_A;
            elsif usr_func_rd(measure_counter_merger_B + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_B;
            elsif usr_func_rd(measure_counter_merger_C + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_C;
            elsif usr_func_rd(measure_counter_merger_D + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_D;
            elsif usr_func_rd(measure_counter_merger_E + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_E;
            elsif usr_func_rd(measure_counter_merger_F + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_F;
            elsif usr_func_rd(measure_counter_merger_G + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_G;
            elsif usr_func_rd(measure_counter_merger_H + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_H;
            elsif usr_func_rd(measure_counter_merger_I + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_I;
            elsif usr_func_rd(measure_counter_merger_J + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_J;
            elsif usr_func_rd(measure_counter_merger_K + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_K;
            elsif usr_func_rd(measure_counter_merger_L + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_L;
            elsif usr_func_rd(measure_counter_merger_free + addr_offset_100G_eth) = '1' then data_in_rg <= latch_counter_free;
            end if;
        end if;
    end process;

    usr_data_rd    <= data_in_rg;



    --##############################################################
    -- Stat time

    resync_reset : resetp_resync
        port map (
            aresetp     => resetp_counter_timer,
            clock       => TCP_clock,
            Resetp_sync => resetp_counter_timer_resync_cell
        );

    resetp_counter_timer_resync <= resetp_counter_timer_resync_cell;

    process(resetp_counter_timer_resync_cell, TCP_clock)
    begin
        if resetp_counter_timer_resync_cell = '1' then
            counter_A    <= (others => '0');
            counter_B    <= (others => '0');
            counter_C    <= (others => '0');
            counter_D    <= (others => '0');
            counter_E    <= (others => '0');
            counter_F    <= (others => '0');
            counter_G    <= (others => '0');
            counter_H    <= (others => '0');
            counter_I    <= (others => '0');
            counter_J    <= (others => '0');
            counter_K    <= (others => '0');
            counter_L    <= (others => '0');
            counter_free <= (others => '0');
        elsif rising_edge(TCP_clock) then
            counter_free <= counter_free + '1';

            if gnt_stream(00) = '1'  then counter_A <= counter_A + '1'; end if;
            if gnt_stream(01) = '1'  then counter_B <= counter_B + '1'; end if;
            if gnt_stream(02) = '1'  then counter_C <= counter_C + '1'; end if;
            if gnt_stream(03) = '1'  then counter_D <= counter_D + '1'; end if;
            if gnt_stream(04) = '1'  then counter_E <= counter_E + '1'; end if;
            if gnt_stream(05) = '1'  then counter_F <= counter_F + '1'; end if;
            if gnt_stream(06) = '1'  then counter_G <= counter_G + '1'; end if;
            if gnt_stream(07) = '1'  then counter_H <= counter_H + '1'; end if;
            if gnt_stream(08) = '1'  then counter_I <= counter_I + '1'; end if;
            if gnt_stream(09) = '1'  then counter_J <= counter_J + '1'; end if;
            if gnt_stream(10) = '1'  then counter_K <= counter_K + '1'; end if;
            if gnt_stream(11) = '1'  then counter_L <= counter_L + '1'; end if;
        end if;
    end process;

    latch_resync : resync_v4
        port map (
            aresetn => '1',
            clocki  => usr_clk,
            input   => latch_counter_timer,
            clocko  => TCP_Clock,
            output  => latch_counter_timer_resync
        );


    process(TCP_Clock)
    begin
        if rising_edge(TCP_Clock) then
            if latch_counter_timer_resync = '1' then
                latch_counter_A    <= counter_A;
                latch_counter_B    <= counter_B;
                latch_counter_C    <= counter_C;
                latch_counter_D    <= counter_D;
                latch_counter_E    <= counter_E;
                latch_counter_F    <= counter_F;
                latch_counter_G    <= counter_G;
                latch_counter_H    <= counter_H;
                latch_counter_I    <= counter_I;
                latch_counter_J    <= counter_J;
                latch_counter_K    <= counter_K;
                latch_counter_L    <= counter_L;
                latch_counter_free <= counter_free;
            end if;
        end if;
    end process;

end Behavioral;
