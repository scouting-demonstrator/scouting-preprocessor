----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 05.02.2021 11:25:57
-- Design Name:
-- Module Name: Split_ETH_ack - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.interface.all;





entity Split_ETH_ack_x4 is
    port (
        reset_n                 : in  std_logic;
        TCP_clock               : in  std_logic;

        --informations from DAQ ethernet
        ETH_TCP_ack_rcvo        : in  TCP_ack_rcvo_record;      -- receive ACK packet
        ETH_TCP_Px_Ack_present  : in  std_logic;
        ETH_rd_TCP_Px_val_rcvo  : out std_logic;

        --Send the DAQ ethernet packet infos to the coresponding TCP logic (selected by the port#)
        TCP00_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP00_Px_Ack_present    : out std_logic;
        rd_TCP00_val_rcvo       : in  std_logic;

        TCP01_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP01_Px_Ack_present    : out std_logic;
        rd_TCP01_val_rcvo       : in  std_logic;

        TCP02_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP02_Px_Ack_present    : out std_logic;
        rd_TCP02_val_rcvo       : in  std_logic;

        TCP03_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP03_Px_Ack_present    : out std_logic;
        rd_TCP03_val_rcvo       : in  std_logic
    );
end Split_ETH_ack_x4;





architecture Behavioral of Split_ETH_ack_x4 is

    signal ack_present_reg  : std_logic_vector(11 downto 0);-- assuming only 12 Streams max per DAQ unit
    attribute dont_touch    : string;
    attribute dont_touch of ack_present_reg : signal is "true";

begin

    process(reset_n, TCP_clock)
    begin
        if reset_n = '0' then
            ack_present_reg <= (others => '0');
        elsif rising_edge(TCP_clock) then
            for I in 0 to 3 loop
                ack_present_reg(I) <= '0';
                if ETH_TCP_Px_Ack_present = '1' and ETH_TCP_ack_rcvo.TCP_Px_Port_sel_rcv(I) = '1' then
                    ack_present_reg(I) <= '1';
                end if;
            end loop;
        end if;
    end process;



    TCP00_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP00_Px_Ack_present <= ack_present_reg(0);
    TCP01_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP01_Px_Ack_present <= ack_present_reg(1);
    TCP02_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP02_Px_Ack_present <= ack_present_reg(2);
    TCP03_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP03_Px_Ack_present <= ack_present_reg(3);



    process(ack_present_reg, rd_TCP00_val_rcvo, rd_TCP01_val_rcvo, rd_TCP02_val_rcvo, rd_TCP03_val_rcvo)
    begin
        ETH_rd_TCP_Px_val_rcvo <= '0';
        if (ack_present_reg(0) = '1' and rd_TCP00_val_rcvo = '1') or
           (ack_present_reg(1) = '1' and rd_TCP01_val_rcvo = '1') or
           (ack_present_reg(2) = '1' and rd_TCP02_val_rcvo = '1') or
           (ack_present_reg(3) = '1' and rd_TCP03_val_rcvo = '1') then
            ETH_rd_TCP_Px_val_rcvo <= '1';
        end if;
    end process;

end Behavioral;
