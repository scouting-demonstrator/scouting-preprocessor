----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 05.02.2021 11:28:33
-- Design Name:
-- Module Name: Merge_HBM_to_DAQ - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.address_table.all;
use work.interface.all;





entity Merge_HBM_to_DAQ_x1 is
    generic (
        addr_offset_100g_eth        : integer := 0;
        addr_offset_hbm_item        : integer := 0
    );
    port (
        usr_clk                     : in  std_logic;
        usr_rst_n                   : in  std_logic;
        usr_func_wr                 : in  std_logic_vector(16383 downto 0);
        usr_wen                     : in  std_logic;
        usr_data_wr                 : in  std_logic_vector(63 downto 0);

        usr_func_rd                 : in  std_logic_vector(16383 downto 0);
        usr_rden                    : in  std_logic;
        usr_data_rd                 : out std_logic_vector(63 downto 0);

        TCP_clock_resetn            : in  std_logic;
        TCP_Clock                   : in  std_logic;    -- Clock used from HBM to FIFO in front of the MAC 100gbs
        resetp_counter_timer_resync : out std_logic;

        -- Bus merging data source from TCP/HBM block
        TCPxx_BLOCK_TO_SEND         : out HBM_to_TCP_block_record;
        ETH_TCP_x_packet_done       : in  std_logic;

        -- Input record (coming from TCP/HBM logic  when a block is ready to be sent
        TCP00_BLOCK_TO_SEND         : in  HBM_to_TCP_block_record;
        TCP00_gnt_access            : out std_logic;
        TCP00_packet_done           : out std_logic
    );
end Merge_HBM_to_DAQ_x1;





architecture Behavioral of Merge_HBM_to_DAQ_x1 is

    component resetp_resync is
        port (
            aresetp     : in  std_logic;
            clock       : in  std_logic;
            Resetp_sync : out std_logic
        );
    end component;

    component resync_v4
        port (
            aresetn : in  std_logic;
            clocki  : in  std_logic;
            clocko  : in  std_logic;
            input   : in  std_logic;
            output  : out std_logic
        );
    end component;

    signal round_robin                      : std_logic_vector(2 downto 0);
    signal req_stream                       : std_logic_vector(11 downto 0);
    signal gnt_stream                       : std_logic_vector(11 downto 0);
    attribute dont_touch                    : string;
    attribute dont_touch of gnt_stream : signal is "true";
    attribute dont_touch of round_robin : signal is "true";

    signal counter_A                        : std_logic_vector(63 downto 0);
    signal counter_B                        : std_logic_vector(63 downto 0);
    signal counter_C                        : std_logic_vector(63 downto 0);
    signal counter_D                        : std_logic_vector(63 downto 0);
    signal counter_free                     : std_logic_vector(63 downto 0);

    signal resetp_counter_timer             : std_logic;
    signal resetp_counter_timer_resync_cell : std_logic;

    signal latch_counter_A                  : std_logic_vector(63 downto 0);
    signal latch_counter_B                  : std_logic_vector(63 downto 0);
    signal latch_counter_C                  : std_logic_vector(63 downto 0);
    signal latch_counter_D                  : std_logic_vector(63 downto 0);
    signal latch_counter_free               : std_logic_vector(63 downto 0);

    signal latch_counter_timer              : std_logic;
    signal latch_counter_timer_resync       : std_logic;

    signal data_in_rg                       : std_logic_vector(63 downto 0);
    signal internal_register                : HBM_to_TCP_block_record;
    attribute dont_touch of internal_register : signal is "true";


    signal TCPxx_packet_done_reg            : std_logic_vector(11 downto 0);
    attribute dont_touch of TCPxx_packet_done_reg : signal is "true";

begin

    req_stream(0) <= '1' when TCP00_BLOCK_TO_SEND.Request_TCP_packet = '1' else '0';



    process(TCP_clock_resetn,TCP_clock)
    begin
        if TCP_clock_resetn        = '0' then
            round_robin    <= (others => '0');
            gnt_stream    <= (others => '0');
        elsif rising_edge(TCP_Clock) then
            if    req_stream(0) = '1'  and round_robin = "000"  then gnt_stream(0) <= '1';
            elsif ETH_TCP_x_packet_done = '1'                   then gnt_stream    <= (others => '0');
            end if;

            if gnt_stream = "000000000000" then
                round_robin <= round_robin + '1';
            end if;
        end if;
    end process;



    process(TCP_clock_resetn, TCP_clock)
    begin
        if TCP_clock_resetn = '0' then
            TCPxx_packet_done_reg <= (others => '0');
        elsif rising_edge(TCP_Clock) then
            TCPxx_packet_done_reg <= (others => '0');
            if ETH_TCP_x_packet_done = '1' then
                if gnt_stream(00) = '1' then TCPxx_packet_done_reg(00) <= '1'; end if;
            end if;
        end if;
    end process;

    TCP00_packet_done <= TCPxx_packet_done_reg(0);



    process(TCP_clock)
    begin
        if rising_edge(TCP_clock) then
            if ETH_TCP_x_packet_done = '1' then
                internal_register.Request_TCP_packet <= '0';

            elsif gnt_stream(0) = '1' then internal_register    <= TCP00_BLOCK_TO_SEND;
            end if;
        end if;
    end process;

    TCP00_gnt_access <= gnt_stream(0);

    TCPxx_BLOCK_TO_SEND <= internal_register;



    --##############################################################
    -- Control

    process(usr_clk)
    begin
        if rising_edge(usr_clk) then
            latch_counter_timer <= '0';
            resetp_counter_timer <= '0';

            if usr_wen = '1' then
                if usr_func_wr(HBM_status + addr_offset_hbm_item) = '1' then
                    latch_counter_timer  <= usr_data_wr(30);
                    resetp_counter_timer <= usr_data_wr(31);
                end if;
            end if;
        end if;
    end process;

    process(usr_clk)
    begin
        if rising_edge(usr_clk) then
            data_in_rg <= (others => '0');

            if    usr_func_rd(measure_counter_merger_A + addr_offset_100G_eth)    = '1' then data_in_rg <= latch_counter_A;
            elsif usr_func_rd(measure_counter_merger_free + addr_offset_100G_eth) = '1' then data_in_rg <= latch_counter_free;
            end if;
        end if;
    end process;

    usr_data_rd    <= data_in_rg;



    --##############################################################
    -- Stat time

    resync_reset : resetp_resync
        port map (
            aresetp     => resetp_counter_timer,
            clock       => TCP_clock,
            Resetp_sync => resetp_counter_timer_resync_cell
        );

    resetp_counter_timer_resync <= resetp_counter_timer_resync_cell;

    process(resetp_counter_timer_resync_cell, TCP_clock)
    begin
        if resetp_counter_timer_resync_cell = '1' then
            counter_A    <= (others => '0');
            counter_free <= (others => '0');
        elsif rising_edge(TCP_clock) then
            counter_free <= counter_free + '1';

            if gnt_stream(0) = '1'  then counter_A <= counter_A + '1'; end if;
        end if;
    end process;

    latch_resync : resync_v4
        port map (
            aresetn => '1',
            clocki  => usr_clk,
            input   => latch_counter_timer,
            clocko  => TCP_Clock,
            output  => latch_counter_timer_resync
        );


    process(TCP_Clock)
    begin
        if rising_edge(TCP_Clock) then
            if latch_counter_timer_resync = '1' then
                latch_counter_A    <= counter_A;
                latch_counter_free <= counter_free;
            end if;
        end if;
    end process;

end Behavioral;
