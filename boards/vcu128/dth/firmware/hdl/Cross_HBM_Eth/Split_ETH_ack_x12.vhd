----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 05.02.2021 11:25:57
-- Design Name:
-- Module Name: Split_ETH_ack - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.interface.all;





entity Split_ETH_ack_x12 is
    port (
        reset_n                 : in  std_logic;
        TCP_clock               : in  std_logic;

        --informations from DAQ ethernet
        ETH_TCP_ack_rcvo        : in  TCP_ack_rcvo_record;      -- receive ACK packet
        ETH_TCP_Px_Ack_present  : in  std_logic;
        ETH_rd_TCP_Px_val_rcvo  : out std_logic;

        --Send the DAQ ethernet packet infos to the coresponding TCP logic (selected by the port#)
        TCP00_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP00_Px_Ack_present    : out std_logic;
        rd_TCP00_val_rcvo       : in  std_logic;

        TCP01_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP01_Px_Ack_present    : out std_logic;
        rd_TCP01_val_rcvo       : in  std_logic;

        TCP02_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP02_Px_Ack_present    : out std_logic;
        rd_TCP02_val_rcvo       : in  std_logic;

        TCP03_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP03_Px_Ack_present    : out std_logic;
        rd_TCP03_val_rcvo       : in  std_logic;

        TCP04_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP04_Px_Ack_present    : out std_logic;
        rd_TCP04_val_rcvo       : in  std_logic;

        TCP05_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP05_Px_Ack_present    : out std_logic;
        rd_TCP05_val_rcvo       : in  std_logic;

        TCP06_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP06_Px_Ack_present    : out std_logic;
        rd_TCP06_val_rcvo       : in  std_logic;

        TCP07_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP07_Px_Ack_present    : out std_logic;
        rd_TCP07_val_rcvo       : in  std_logic;

        TCP08_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP08_Px_Ack_present    : out std_logic;
        rd_TCP08_val_rcvo       : in  std_logic;

        TCP09_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP09_Px_Ack_present    : out std_logic;
        rd_TCP09_val_rcvo       : in  std_logic;

        TCP10_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP10_Px_Ack_present    : out std_logic;
        rd_TCP10_val_rcvo       : in  std_logic;

        TCP11_ack_rcvo          : out TCP_ack_rcvo_record;      -- receive ACK packet
        TCP11_Px_Ack_present    : out std_logic;
        rd_TCP11_val_rcvo       : in  std_logic
    );
end Split_ETH_ack_x12;





architecture Behavioral of Split_ETH_ack_x12 is

    signal ack_present_reg  : std_logic_vector(11 downto 0);-- assuming only 12 Streams max per DAQ unit
    attribute dont_touch    : string;
    attribute dont_touch of ack_present_reg : signal is "true";

begin

    process(reset_n, TCP_clock)
    begin
        if reset_n = '0' then
            ack_present_reg <= (others => '0');
        elsif rising_edge(TCP_clock) then
            for I in 0 to 11 loop
                ack_present_reg(I) <= '0';
                if ETH_TCP_Px_Ack_present = '1' and ETH_TCP_ack_rcvo.TCP_Px_Port_sel_rcv(I) = '1' then
                    ack_present_reg(I) <= '1';
                end if;
            end loop;
        end if;
    end process;



    TCP00_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP00_Px_Ack_present <= ack_present_reg(00);
    TCP01_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP01_Px_Ack_present <= ack_present_reg(01);
    TCP02_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP02_Px_Ack_present <= ack_present_reg(02);
    TCP03_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP03_Px_Ack_present <= ack_present_reg(03);
    TCP04_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP04_Px_Ack_present <= ack_present_reg(04);
    TCP05_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP05_Px_Ack_present <= ack_present_reg(05);
    TCP06_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP06_Px_Ack_present <= ack_present_reg(06);
    TCP07_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP07_Px_Ack_present <= ack_present_reg(07);
    TCP08_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP08_Px_Ack_present <= ack_present_reg(08);
    TCP09_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP09_Px_Ack_present <= ack_present_reg(09);
    TCP10_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP10_Px_Ack_present <= ack_present_reg(10);
    TCP11_ack_rcvo       <= ETH_TCP_ack_rcvo;
    TCP11_Px_Ack_present <= ack_present_reg(11);



    process(ack_present_reg, rd_TCP00_val_rcvo,
                             rd_TCP01_val_rcvo,
                             rd_TCP02_val_rcvo,
                             rd_TCP03_val_rcvo,
                             rd_TCP04_val_rcvo,
                             rd_TCP05_val_rcvo,
                             rd_TCP06_val_rcvo,
                             rd_TCP07_val_rcvo,
                             rd_TCP08_val_rcvo,
                             rd_TCP09_val_rcvo,
                             rd_TCP10_val_rcvo,
                             rd_TCP11_val_rcvo)
    begin
        ETH_rd_TCP_Px_val_rcvo <= '0';
        if (ack_present_reg(00) = '1' and rd_TCP00_val_rcvo = '1') or
           (ack_present_reg(01) = '1' and rd_TCP01_val_rcvo = '1') or
           (ack_present_reg(02) = '1' and rd_TCP02_val_rcvo = '1') or
           (ack_present_reg(03) = '1' and rd_TCP03_val_rcvo = '1') or
           (ack_present_reg(04) = '1' and rd_TCP04_val_rcvo = '1') or
           (ack_present_reg(05) = '1' and rd_TCP05_val_rcvo = '1') or
           (ack_present_reg(06) = '1' and rd_TCP06_val_rcvo = '1') or
           (ack_present_reg(07) = '1' and rd_TCP07_val_rcvo = '1') or
           (ack_present_reg(08) = '1' and rd_TCP08_val_rcvo = '1') or
           (ack_present_reg(09) = '1' and rd_TCP09_val_rcvo = '1') or
           (ack_present_reg(10) = '1' and rd_TCP10_val_rcvo = '1') or
           (ack_present_reg(11) = '1' and rd_TCP11_val_rcvo = '1') then
            ETH_rd_TCP_Px_val_rcvo <= '1';
        end if;
    end process;

end Behavioral;
