----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.10.2018 13:23:55
-- Design Name: 
-- Module Name: add_16x16 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
-- accumulate the intermediate sums (8*16)
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Accu_4x31 is
	Port ( 
		aclrp		: IN STD_LOGIC  := '0';
		clken		: IN STD_LOGIC  := '1';
		clock		: IN STD_LOGIC  := '0';
		data0x		: IN STD_LOGIC_VECTOR (18 DOWNTO 0);
		data1x		: IN STD_LOGIC_VECTOR (18 DOWNTO 0);
		data2x		: IN STD_LOGIC_VECTOR (18 DOWNTO 0);
		data3x		: IN STD_LOGIC_VECTOR (18 DOWNTO 0);

		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
);
end Accu_4x31;

architecture Behavioral of Accu_4x31 is

signal data0_reg			: std_logic_vector(31 downto 0) := (others => '0');
signal data1_reg			: std_logic_vector(31 downto 0) := (others => '0');
signal data2_reg			: std_logic_vector(31 downto 0) := (others => '0');
signal data3_reg			: std_logic_vector(31 downto 0) := (others => '0');
signal data4_reg			: std_logic_vector(31 downto 0) := (others => '0');
signal data5_reg			: std_logic_vector(31 downto 0) := (others => '0');
 

signal result_reg			: std_logic_vector(31 downto 0);

--*********************************************************************
--******************     CODE    START     HERE        ****************
--*********************************************************************
begin

data0_reg(18 downto 0)	<= data0x;
data1_reg(18 downto 0)	<= data1x;
data2_reg(18 downto 0)	<= data2x;
data3_reg(18 downto 0)	<= data3x;

data4_reg(15 downto 0)	<= result_reg(15 downto 00); 
data5_reg(15 downto 0)	<= result_reg(31 downto 16);

 



process(aclrp,clock)
begin
	if aclrp = '1' then
		result_reg		<= (others => '0');
	elsif rising_edge(clock) then
		if clken = '1' then
			result_reg <= data0_reg + data1_reg + data2_reg + data3_reg + data4_reg + data5_reg;
		end if;
	end if;
end process;

result		<= result_reg;

end Behavioral;
