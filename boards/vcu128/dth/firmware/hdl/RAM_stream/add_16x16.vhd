----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.10.2018 13:23:55
-- Design Name: 
-- Module Name: add_16x16 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity add_16x16 is
	Port ( 
		aclr		: IN STD_LOGIC  := '0';
		clken		: IN STD_LOGIC  := '1';
		clock		: IN STD_LOGIC  := '0';
		data0x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data10x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data11x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data12x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data13x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data14x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data15x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data1x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data2x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data3x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data4x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data5x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data6x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data7x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data8x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data9x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (19 DOWNTO 0)
);
end add_16x16;

architecture Behavioral of add_16x16 is

signal data0_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data1_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data2_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data3_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data4_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data5_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data6_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data7_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data8_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data9_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data10_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data11_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data12_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data13_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data14_reg			: std_logic_vector(19 downto 0):= x"00000";
signal data15_reg			: std_logic_vector(19 downto 0):= x"00000";

signal result_reg			: std_logic_vector(19 downto 0);

--*********************************************************************
--******************     CODE    START     HERE        ****************
--*********************************************************************
begin

data0_reg(15 downto 0)	<= data0x;
data1_reg(15 downto 0)	<= data1x;
data2_reg(15 downto 0)	<= data2x;
data3_reg(15 downto 0)	<= data3x;
data4_reg(15 downto 0)	<= data4x;
data5_reg(15 downto 0)	<= data5x;
data6_reg(15 downto 0)	<= data6x;
data7_reg(15 downto 0)	<= data7x;
data8_reg(15 downto 0)	<= data8x;
data9_reg(15 downto 0)	<= data9x;
data10_reg(15 downto 0)	<= data10x;
data11_reg(15 downto 0)	<= data11x;
data12_reg(15 downto 0)	<= data12x;
data13_reg(15 downto 0)	<= data13x;
data14_reg(15 downto 0)	<= data14x;
data15_reg(15 downto 0)	<= data15x;



process(aclr,clock)
begin
	if aclr = '0' then
		result_reg		<= (others => '0');
	elsif rising_edge(clock) then
		if clken = '1' then
			result_reg <= data0_reg + data1_reg + data2_reg + data3_reg + data4_reg + data5_reg + data6_reg + data7_reg + data8_reg + data9_reg + data10_reg + data11_reg + data12_reg + data13_reg + data14_reg + data15_reg;
		end if;
	end if;
end process;

result		<= result_reg;

end Behavioral;
