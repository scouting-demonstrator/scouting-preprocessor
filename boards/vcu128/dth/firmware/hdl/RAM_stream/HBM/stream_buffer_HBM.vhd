----------------------------------------------------------------------------------
-- Company:
-- Engineer: D. Gigi
--
-- Create Date: 17.10.2018 11:50:12
-- Design Name:
-- Module Name: stream_buffer - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- HBM IP changed to add more HBM slots (x4 + 1 for PCIe readout for debugging) (R. Ardino, January 2023)
--
----------------------------------------------------------------------------------


library IEEE;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;
use work.interface.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;


entity stream_buffer_HBM is
    generic (
        MMCM_CLKFBOUT_MULT_F        : real := 10.0;
        MMCM_CLKOUT0_DIVIDE_F       : integer := 4;
        MMCM_CLKOUT0_DIVIDE_F_real  : real := 4.0;
        MMCM_DIVCLK_DIVIDE          : integer := 1;
        MMCM_CLKIN1_PERIOD          : real := 10.0
    );
    Port (
		usr_rst_n							: in std_logic;
  		usr_clk								: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0);
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0);
		usr_func_rd					      	: in std_logic_vector(16383 downto 0);
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0);

		HBM_RESETn						    : in std_logic;
		HBM_clk_ref						    : in std_logic; -- 100 MHz
  		Data_clk						    : in std_logic; -- 100 MHz
  		ctrl_clk						    : in std_logic; -- 100 MHz
  		-- write access bus
		HBM_write_in_mem					: in HBM_write_req_type;
		HBM_write_out_mem					: out HBM_write_ack_type;
		-- read access bus
		HBM_read_in_mem						: in HBM_read_req_type;
		HBM_read_out_mem					: out HBM_read_ack_type;

		clock_interface					    : out std_logic;
		-- control/monitoring access bus

		HBM_memory_ready				    : out std_logic;
		Catastrophic_temp         		    : OUT STD_LOGIC;
		HBM_temperature                	    : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);

		HBM_ctrl_awaddr  				    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		HBM_ctrl_awprot  				    : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		HBM_ctrl_awvalid 				    : IN STD_LOGIC;
		HBM_ctrl_awready 				    : OUT STD_LOGIC;
		HBM_ctrl_wdata   				    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		HBM_ctrl_wstrb   				    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		HBM_ctrl_wvalid  				    : IN STD_LOGIC;
		HBM_ctrl_wready  				    : OUT STD_LOGIC;
		HBM_ctrl_bresp   				    : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
		HBM_ctrl_bvalid  				    : OUT STD_LOGIC;
		HBM_ctrl_bready  				    : IN STD_LOGIC;
		HBM_ctrl_araddr  				    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		HBM_ctrl_arprot  				    : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		HBM_ctrl_arvalid 				    : IN STD_LOGIC;
		HBM_ctrl_arready 				    : OUT STD_LOGIC;
		HBM_ctrl_rdata   				    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		HBM_ctrl_rresp   				    : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
		HBM_ctrl_rvalid  				    : OUT STD_LOGIC;
		HBM_ctrl_rready  				    : IN STD_LOGIC
  	);
end stream_buffer_HBM;



architecture Behavioral of stream_buffer_HBM is



COMPONENT TCP_Stream_HBM
  PORT (
    HBM_REF_CLK_0       : IN STD_LOGIC;
    AXI_00_ACLK         : IN STD_LOGIC;
    AXI_00_ARESET_N     : IN STD_LOGIC;
    AXI_00_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_00_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_00_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_00_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_00_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_00_ARVALID      : IN STD_LOGIC;
    AXI_00_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_00_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_00_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_00_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_00_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_00_AWVALID      : IN STD_LOGIC;
    AXI_00_RREADY       : IN STD_LOGIC;
    AXI_00_BREADY       : IN STD_LOGIC;
    AXI_00_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_00_WLAST        : IN STD_LOGIC;
    AXI_00_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_00_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_00_WVALID       : IN STD_LOGIC;

    AXI_01_ACLK         : IN STD_LOGIC;
    AXI_01_ARESET_N     : IN STD_LOGIC;
    AXI_01_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_01_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_01_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_01_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_01_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_01_ARVALID      : IN STD_LOGIC;
    AXI_01_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_01_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_01_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_01_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_01_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_01_AWVALID      : IN STD_LOGIC;
    AXI_01_RREADY       : IN STD_LOGIC;
    AXI_01_BREADY       : IN STD_LOGIC;
    AXI_01_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_01_WLAST        : IN STD_LOGIC;
    AXI_01_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_01_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_01_WVALID       : IN STD_LOGIC;

    AXI_02_ACLK         : IN STD_LOGIC;
    AXI_02_ARESET_N     : IN STD_LOGIC;
    AXI_02_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_02_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_02_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_02_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_02_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_02_ARVALID      : IN STD_LOGIC;
    AXI_02_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_02_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_02_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_02_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_02_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_02_AWVALID      : IN STD_LOGIC;
    AXI_02_RREADY       : IN STD_LOGIC;
    AXI_02_BREADY       : IN STD_LOGIC;
    AXI_02_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_02_WLAST        : IN STD_LOGIC;
    AXI_02_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_02_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_02_WVALID       : IN STD_LOGIC;

    AXI_03_ACLK         : IN STD_LOGIC;
    AXI_03_ARESET_N     : IN STD_LOGIC;
    AXI_03_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_03_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_03_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_03_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_03_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_03_ARVALID      : IN STD_LOGIC;
    AXI_03_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_03_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_03_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_03_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_03_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_03_AWVALID      : IN STD_LOGIC;
    AXI_03_RREADY       : IN STD_LOGIC;
    AXI_03_BREADY       : IN STD_LOGIC;
    AXI_03_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_03_WLAST        : IN STD_LOGIC;
    AXI_03_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_03_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_03_WVALID       : IN STD_LOGIC;

    AXI_04_ACLK         : IN STD_LOGIC;
    AXI_04_ARESET_N     : IN STD_LOGIC;
    AXI_04_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_04_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_04_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_04_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_04_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_04_ARVALID      : IN STD_LOGIC;
    AXI_04_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_04_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_04_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_04_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_04_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_04_AWVALID      : IN STD_LOGIC;
    AXI_04_RREADY       : IN STD_LOGIC;
    AXI_04_BREADY       : IN STD_LOGIC;
    AXI_04_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_04_WLAST        : IN STD_LOGIC;
    AXI_04_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_04_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_04_WVALID       : IN STD_LOGIC;

    AXI_05_ACLK         : IN STD_LOGIC;
    AXI_05_ARESET_N     : IN STD_LOGIC;
    AXI_05_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_05_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_05_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_05_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_05_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_05_ARVALID      : IN STD_LOGIC;
    AXI_05_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_05_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_05_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_05_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_05_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_05_AWVALID      : IN STD_LOGIC;
    AXI_05_RREADY       : IN STD_LOGIC;
    AXI_05_BREADY       : IN STD_LOGIC;
    AXI_05_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_05_WLAST        : IN STD_LOGIC;
    AXI_05_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_05_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_05_WVALID       : IN STD_LOGIC;

    AXI_06_ACLK         : IN STD_LOGIC;
    AXI_06_ARESET_N     : IN STD_LOGIC;
    AXI_06_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_06_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_06_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_06_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_06_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_06_ARVALID      : IN STD_LOGIC;
    AXI_06_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_06_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_06_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_06_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_06_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_06_AWVALID      : IN STD_LOGIC;
    AXI_06_RREADY       : IN STD_LOGIC;
    AXI_06_BREADY       : IN STD_LOGIC;
    AXI_06_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_06_WLAST        : IN STD_LOGIC;
    AXI_06_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_06_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_06_WVALID       : IN STD_LOGIC;

    AXI_07_ACLK         : IN STD_LOGIC;
    AXI_07_ARESET_N     : IN STD_LOGIC;
    AXI_07_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_07_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_07_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_07_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_07_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_07_ARVALID      : IN STD_LOGIC;
    AXI_07_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_07_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_07_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_07_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_07_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_07_AWVALID      : IN STD_LOGIC;
    AXI_07_RREADY       : IN STD_LOGIC;
    AXI_07_BREADY       : IN STD_LOGIC;
    AXI_07_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_07_WLAST        : IN STD_LOGIC;
    AXI_07_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_07_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_07_WVALID       : IN STD_LOGIC;

    AXI_08_ACLK         : IN STD_LOGIC;
    AXI_08_ARESET_N     : IN STD_LOGIC;
    AXI_08_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_08_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_08_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_08_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_08_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_08_ARVALID      : IN STD_LOGIC;
    AXI_08_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_08_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_08_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_08_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_08_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_08_AWVALID      : IN STD_LOGIC;
    AXI_08_RREADY       : IN STD_LOGIC;
    AXI_08_BREADY       : IN STD_LOGIC;
    AXI_08_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_08_WLAST        : IN STD_LOGIC;
    AXI_08_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_08_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_08_WVALID       : IN STD_LOGIC;

    AXI_09_ACLK         : IN STD_LOGIC;
    AXI_09_ARESET_N     : IN STD_LOGIC;
    AXI_09_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_09_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_09_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_09_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_09_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_09_ARVALID      : IN STD_LOGIC;
    AXI_09_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_09_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_09_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_09_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_09_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_09_AWVALID      : IN STD_LOGIC;
    AXI_09_RREADY       : IN STD_LOGIC;
    AXI_09_BREADY       : IN STD_LOGIC;
    AXI_09_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_09_WLAST        : IN STD_LOGIC;
    AXI_09_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_09_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_09_WVALID       : IN STD_LOGIC;

    AXI_10_ACLK         : IN STD_LOGIC;
    AXI_10_ARESET_N     : IN STD_LOGIC;
    AXI_10_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_10_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_10_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_10_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_10_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_10_ARVALID      : IN STD_LOGIC;
    AXI_10_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_10_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_10_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_10_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_10_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_10_AWVALID      : IN STD_LOGIC;
    AXI_10_RREADY       : IN STD_LOGIC;
    AXI_10_BREADY       : IN STD_LOGIC;
    AXI_10_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_10_WLAST        : IN STD_LOGIC;
    AXI_10_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_10_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_10_WVALID       : IN STD_LOGIC;

    AXI_11_ACLK         : IN STD_LOGIC;
    AXI_11_ARESET_N     : IN STD_LOGIC;
    AXI_11_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_11_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_11_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_11_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_11_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_11_ARVALID      : IN STD_LOGIC;
    AXI_11_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_11_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_11_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_11_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_11_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_11_AWVALID      : IN STD_LOGIC;
    AXI_11_RREADY       : IN STD_LOGIC;
    AXI_11_BREADY       : IN STD_LOGIC;
    AXI_11_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_11_WLAST        : IN STD_LOGIC;
    AXI_11_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_11_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_11_WVALID       : IN STD_LOGIC;

    AXI_12_ACLK         : IN STD_LOGIC;
    AXI_12_ARESET_N     : IN STD_LOGIC;
    AXI_12_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_12_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_12_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_12_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_12_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_12_ARVALID      : IN STD_LOGIC;
    AXI_12_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_12_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_12_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_12_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_12_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_12_AWVALID      : IN STD_LOGIC;
    AXI_12_RREADY       : IN STD_LOGIC;
    AXI_12_BREADY       : IN STD_LOGIC;
    AXI_12_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_12_WLAST        : IN STD_LOGIC;
    AXI_12_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_12_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_12_WVALID       : IN STD_LOGIC;

    AXI_13_ACLK         : IN STD_LOGIC;
    AXI_13_ARESET_N     : IN STD_LOGIC;
    AXI_13_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_13_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_13_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_13_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_13_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_13_ARVALID      : IN STD_LOGIC;
    AXI_13_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_13_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_13_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_13_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_13_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_13_AWVALID      : IN STD_LOGIC;
    AXI_13_RREADY       : IN STD_LOGIC;
    AXI_13_BREADY       : IN STD_LOGIC;
    AXI_13_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_13_WLAST        : IN STD_LOGIC;
    AXI_13_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_13_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_13_WVALID       : IN STD_LOGIC;

    AXI_14_ACLK         : IN STD_LOGIC;
    AXI_14_ARESET_N     : IN STD_LOGIC;
    AXI_14_ARADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_14_ARBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_14_ARID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_14_ARLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_14_ARSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_14_ARVALID      : IN STD_LOGIC;
    AXI_14_AWADDR       : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_14_AWBURST      : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_14_AWID         : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_14_AWLEN        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_14_AWSIZE       : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_14_AWVALID      : IN STD_LOGIC;
    AXI_14_RREADY       : IN STD_LOGIC;
    AXI_14_BREADY       : IN STD_LOGIC;
    AXI_14_WDATA        : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_14_WLAST        : IN STD_LOGIC;
    AXI_14_WSTRB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_14_WDATA_PARITY : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_14_WVALID       : IN STD_LOGIC;


    APB_0_PWDATA        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    APB_0_PADDR         : IN STD_LOGIC_VECTOR(21 DOWNTO 0);
    APB_0_PCLK          : IN STD_LOGIC;
    APB_0_PENABLE       : IN STD_LOGIC;
    APB_0_PRESET_N      : IN STD_LOGIC;
    APB_0_PSEL          : IN STD_LOGIC;
    APB_0_PWRITE        : IN STD_LOGIC;


    AXI_00_ARREADY      : OUT STD_LOGIC;
    AXI_00_AWREADY      : OUT STD_LOGIC;
    AXI_00_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_00_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_00_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_00_RLAST        : OUT STD_LOGIC;
    AXI_00_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_00_RVALID       : OUT STD_LOGIC;
    AXI_00_WREADY       : OUT STD_LOGIC;
    AXI_00_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_00_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_00_BVALID       : OUT STD_LOGIC;

    AXI_01_ARREADY      : OUT STD_LOGIC;
    AXI_01_AWREADY      : OUT STD_LOGIC;
    AXI_01_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_01_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_01_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_01_RLAST        : OUT STD_LOGIC;
    AXI_01_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_01_RVALID       : OUT STD_LOGIC;
    AXI_01_WREADY       : OUT STD_LOGIC;
    AXI_01_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_01_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_01_BVALID       : OUT STD_LOGIC;

    AXI_02_ARREADY      : OUT STD_LOGIC;
    AXI_02_AWREADY      : OUT STD_LOGIC;
    AXI_02_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_02_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_02_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_02_RLAST        : OUT STD_LOGIC;
    AXI_02_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_02_RVALID       : OUT STD_LOGIC;
    AXI_02_WREADY       : OUT STD_LOGIC;
    AXI_02_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_02_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_02_BVALID       : OUT STD_LOGIC;

    AXI_03_ARREADY      : OUT STD_LOGIC;
    AXI_03_AWREADY      : OUT STD_LOGIC;
    AXI_03_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_03_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_03_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_03_RLAST        : OUT STD_LOGIC;
    AXI_03_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_03_RVALID       : OUT STD_LOGIC;
    AXI_03_WREADY       : OUT STD_LOGIC;
    AXI_03_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_03_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_03_BVALID       : OUT STD_LOGIC;

    AXI_04_ARREADY      : OUT STD_LOGIC;
    AXI_04_AWREADY      : OUT STD_LOGIC;
    AXI_04_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_04_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_04_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_04_RLAST        : OUT STD_LOGIC;
    AXI_04_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_04_RVALID       : OUT STD_LOGIC;
    AXI_04_WREADY       : OUT STD_LOGIC;
    AXI_04_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_04_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_04_BVALID       : OUT STD_LOGIC;

    AXI_05_ARREADY      : OUT STD_LOGIC;
    AXI_05_AWREADY      : OUT STD_LOGIC;
    AXI_05_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_05_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_05_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_05_RLAST        : OUT STD_LOGIC;
    AXI_05_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_05_RVALID       : OUT STD_LOGIC;
    AXI_05_WREADY       : OUT STD_LOGIC;
    AXI_05_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_05_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_05_BVALID       : OUT STD_LOGIC;

    AXI_06_ARREADY      : OUT STD_LOGIC;
    AXI_06_AWREADY      : OUT STD_LOGIC;
    AXI_06_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_06_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_06_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_06_RLAST        : OUT STD_LOGIC;
    AXI_06_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_06_RVALID       : OUT STD_LOGIC;
    AXI_06_WREADY       : OUT STD_LOGIC;
    AXI_06_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_06_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_06_BVALID       : OUT STD_LOGIC;

    AXI_07_ARREADY      : OUT STD_LOGIC;
    AXI_07_AWREADY      : OUT STD_LOGIC;
    AXI_07_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_07_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_07_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_07_RLAST        : OUT STD_LOGIC;
    AXI_07_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_07_RVALID       : OUT STD_LOGIC;
    AXI_07_WREADY       : OUT STD_LOGIC;
    AXI_07_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_07_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_07_BVALID       : OUT STD_LOGIC;

    AXI_08_ARREADY      : OUT STD_LOGIC;
    AXI_08_AWREADY      : OUT STD_LOGIC;
    AXI_08_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_08_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_08_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_08_RLAST        : OUT STD_LOGIC;
    AXI_08_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_08_RVALID       : OUT STD_LOGIC;
    AXI_08_WREADY       : OUT STD_LOGIC;
    AXI_08_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_08_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_08_BVALID       : OUT STD_LOGIC;

    AXI_09_ARREADY      : OUT STD_LOGIC;
    AXI_09_AWREADY      : OUT STD_LOGIC;
    AXI_09_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_09_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_09_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_09_RLAST        : OUT STD_LOGIC;
    AXI_09_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_09_RVALID       : OUT STD_LOGIC;
    AXI_09_WREADY       : OUT STD_LOGIC;
    AXI_09_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_09_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_09_BVALID       : OUT STD_LOGIC;

    AXI_10_ARREADY      : OUT STD_LOGIC;
    AXI_10_AWREADY      : OUT STD_LOGIC;
    AXI_10_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_10_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_10_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_10_RLAST        : OUT STD_LOGIC;
    AXI_10_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_10_RVALID       : OUT STD_LOGIC;
    AXI_10_WREADY       : OUT STD_LOGIC;
    AXI_10_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_10_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_10_BVALID       : OUT STD_LOGIC;

    AXI_11_ARREADY      : OUT STD_LOGIC;
    AXI_11_AWREADY      : OUT STD_LOGIC;
    AXI_11_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_11_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_11_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_11_RLAST        : OUT STD_LOGIC;
    AXI_11_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_11_RVALID       : OUT STD_LOGIC;
    AXI_11_WREADY       : OUT STD_LOGIC;
    AXI_11_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_11_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_11_BVALID       : OUT STD_LOGIC;

    AXI_12_ARREADY      : OUT STD_LOGIC;
    AXI_12_AWREADY      : OUT STD_LOGIC;
    AXI_12_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_12_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_12_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_12_RLAST        : OUT STD_LOGIC;
    AXI_12_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_12_RVALID       : OUT STD_LOGIC;
    AXI_12_WREADY       : OUT STD_LOGIC;
    AXI_12_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_12_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_12_BVALID       : OUT STD_LOGIC;

    AXI_13_ARREADY      : OUT STD_LOGIC;
    AXI_13_AWREADY      : OUT STD_LOGIC;
    AXI_13_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_13_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_13_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_13_RLAST        : OUT STD_LOGIC;
    AXI_13_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_13_RVALID       : OUT STD_LOGIC;
    AXI_13_WREADY       : OUT STD_LOGIC;
    AXI_13_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_13_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_13_BVALID       : OUT STD_LOGIC;

    AXI_14_ARREADY      : OUT STD_LOGIC;
    AXI_14_AWREADY      : OUT STD_LOGIC;
    AXI_14_RDATA_PARITY : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_14_RDATA        : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_14_RID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_14_RLAST        : OUT STD_LOGIC;
    AXI_14_RRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_14_RVALID       : OUT STD_LOGIC;
    AXI_14_WREADY       : OUT STD_LOGIC;
    AXI_14_BID          : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    AXI_14_BRESP        : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_14_BVALID       : OUT STD_LOGIC;

    APB_0_PRDATA        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    APB_0_PREADY        : OUT STD_LOGIC;
    APB_0_PSLVERR       : OUT STD_LOGIC;
    apb_complete_0      : OUT STD_LOGIC;
    DRAM_0_STAT_CATTRIP : OUT STD_LOGIC;
    DRAM_0_STAT_TEMP    : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
  );
END COMPONENT;


signal HBM_data_o_cell			 		: HBM_bus_256_bit;
signal HBM_rd_data_last_cell			: HBM_bus_1_bit;
signal HBM_rd_valid_cell				: HBM_bus_1_bit;
signal HBM_read_ack_cell				: HBM_bus_1_bit;
signal HBM_read_ready_cell				: HBM_bus_1_bit;

signal pipe_data_o						: HBM_bus_256_bit;
signal pipe_rd_data_last				: HBM_bus_1_bit;
signal pipe_rd_valid_data				: HBM_bus_1_bit;

signal HBM_write_enable_cell			: HBM_bus_32_bit;
signal HBM_write_ack_cell				: HBM_bus_1_bit;
signal HBM_write_ready_cell				: HBM_bus_1_bit;
signal HBM_write_burst_done_cell 		: HBM_bus_1_bit;
signal HBM_write_burst_ID_return_cell 	: HBM_bus_6_bit;

COMPONENT HBM_AXI_to_APB
  PORT (
    s_axi_aclk    			: IN STD_LOGIC;
    s_axi_aresetn 			: IN STD_LOGIC;
    s_axi_awaddr  			: IN STD_LOGIC_VECTOR(21 DOWNTO 0);
    s_axi_awprot  			: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_awvalid 			: IN STD_LOGIC;
    s_axi_awready 			: OUT STD_LOGIC;
    s_axi_wdata   			: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_wstrb   			: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_wvalid  			: IN STD_LOGIC;
    s_axi_wready  			: OUT STD_LOGIC;
    s_axi_bresp   			: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_bvalid  			: OUT STD_LOGIC;
    s_axi_bready  			: IN STD_LOGIC;
    s_axi_araddr  			: IN STD_LOGIC_VECTOR(21 DOWNTO 0);
    s_axi_arprot  			: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_arvalid 			: IN STD_LOGIC;
    s_axi_arready 			: OUT STD_LOGIC;
    s_axi_rdata   			: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_rresp   			: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_rvalid  			: OUT STD_LOGIC;
    s_axi_rready  			: IN STD_LOGIC;
    m_apb_paddr   			: OUT STD_LOGIC_VECTOR(21 DOWNTO 0);
    m_apb_psel    			: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_apb_penable 			: OUT STD_LOGIC;
    m_apb_pwrite  			: OUT STD_LOGIC;
    m_apb_pwdata  			: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_apb_pready  			: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_apb_prdata  			: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_apb_pslverr 			: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_apb_pprot   			: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_apb_pstrb   			: OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END COMPONENT;

COMPONENT resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic;

	Resetn_sync		: out std_logic;
	Resetp_sync		: out std_logic
	);
end COMPONENT;

COMPONENT resync_v4 is
	port (
		aresetn				: in std_logic;
		clocki			    : in std_logic;
		input				: in std_logic;
		clocko			    : in std_logic;
		output			    : out std_logic
		);
end COMPONENT;

signal APB_PCLK_bufO				: std_logic;
signal APB_PCLK_bufI				: std_logic;

signal AXI_ACLK0_st0_buf			: std_logic;


signal resync_resetn_AXI                		: std_logic;
signal resync_resetn_APB                		: std_logic;
signal APB_reset_cnt                            : std_logic_vector(7 downto 0);
signal APB_reset_n                              : std_logic;

signal MMC_RSTp									: std_logic;
signal MMC_RSTn									: std_logic;
signal MMC_Data_clk								: std_logic;
signal MMC_Reset_clk							: std_logic;
signal MMC_LOCKED								: std_logic;
signal MMC_CLKFBOUT								: std_logic;

signal AXI_reg0_resetn							: std_logic := '0' ;
signal AXI_reg1_resetn							: std_logic := '0' ;
signal AXI_reg2_resetn							: std_logic := '0' ;
attribute ASYNC_REG : string;
attribute ASYNC_REG of  AXI_reg1_resetn 		: signal is "true";
attribute ASYNC_REG of  AXI_reg2_resetn 		: signal is "true";

signal m_apb_paddr   							: STD_LOGIC_VECTOR(21 DOWNTO 0);
signal m_apb_psel    							: STD_LOGIC_VECTOR(0 DOWNTO 0);
signal m_apb_penable 							: STD_LOGIC;
signal m_apb_pwrite  							: STD_LOGIC;
signal m_apb_pwdata  							: STD_LOGIC_VECTOR(31 DOWNTO 0);
signal m_apb_pready  							: STD_LOGIC_VECTOR(0 DOWNTO 0);
signal m_apb_prdata  							: STD_LOGIC_VECTOR(31 DOWNTO 0);
signal m_apb_pslverr 							: STD_LOGIC_VECTOR(0 DOWNTO 0);
signal m_apb_pprot   							: STD_LOGIC_VECTOR(2 DOWNTO 0);
signal m_apb_pstrb   							: STD_LOGIC_VECTOR(3 DOWNTO 0);

signal Memory_ready								: std_logic;

signal delay_reset_counter						: std_logic_vector(3 downto 0);
signal AXI_delay_reset							: std_logic;
signal MMC_ref_clk								: std_logic;
signal MMC_clk_reset							: std_logic;

signal AXIACLK0_st0_buf_reset_n					: std_logic;


signal MMC_delay_reset_counter					: std_logic_vector(7 downto 0);

COMPONENT HBM_Read_random
  PORT (
    rst           : IN STD_LOGIC;
    wr_clk        : IN STD_LOGIC;
    rd_clk        : IN STD_LOGIC;
    din           : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    wr_en         : IN STD_LOGIC;
    rd_en         : IN STD_LOGIC;
    dout          : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    full          : OUT STD_LOGIC;
    empty         : OUT STD_LOGIC;
    wr_data_count : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
    wr_rst_busy   : OUT STD_LOGIC;
    rd_rst_busy   : OUT STD_LOGIC
  );
END COMPONENT;

signal Number_of_words_in_FIFO						: std_logic_vector(8 downto 0);
signal FIFO_resetn									: std_logic;
signal FIFO_resetp_resync							: std_logic;

signal Control_data_out								: std_logic_vector(63 downto 0);

signal User_HBM_add_ready							: std_logic;
signal HBM_User_address								: std_logic_vector(32 downto 0);
signal HBM_User_read_req							: std_logic_vector(2 downto 0);
signal HBM_User_data								: std_logic_vector(255 downto 0);
signal HBM_User_Lastdata_val						: std_logic;
signal HBM_User_data_val							: std_logic;

signal User_data_FIFO_ready							: std_logic;
signal User_Data_FIFO								: std_logic_vector(255 downto 0);
signal USER_readu_FIFO								: std_logic;

signal Fifo_read_full                               : std_logic;

attribute mark_debug                                        : string;
attribute mark_debug of HBM_User_data_val			            : signal is "true";
attribute mark_debug of HBM_User_data			            : signal is "true";

--***************************************************************************************
--*********************    CODE    START   HERE                 *************************
--***************************************************************************************

begin

--reset the APB   it is direct
MMC_resync_reset_i1:resetn_resync
port map(
	aresetn				=> HBM_RESETn,
	clock				=> MMC_ref_clk ,
	Resetn_sync			=> MMC_clk_reset
	);

APB_resync_reset_i1:resetn_resync
port map(
	aresetn				=> HBM_RESETn,
	clock				=> APB_PCLK_bufO ,
	Resetn_sync			=> resync_resetn_APB
	);


CLK0_st0_buf_resync_reset_i1:resetn_resync
port map(
	aresetn				=> HBM_RESETn,
	clock				=> AXI_ACLK0_st0_buf ,
	Resetn_sync			=> AXIACLK0_st0_buf_reset_n
	);

--**************************************************************
--reset the AXI
process(MMC_ref_clk)
begin
	if rising_edge(MMC_ref_clk) then
		if MMC_RSTn = '0' then
			delay_reset_counter	<= x"A";
		elsif delay_reset_counter /= "0000" then
			delay_reset_counter <= delay_reset_counter - '1';
		else
			delay_reset_counter	<= delay_reset_counter;
		end if;
	end if;
end process;

process(MMC_ref_clk)
begin
	if rising_edge(MMC_ref_clk) then
		if delay_reset_counter /= "0000" then
			AXI_delay_reset	<= '0';
		else
			AXI_delay_reset	<= '1';
		end if;

	end if;
end process;

process(MMC_clk_reset,MMC_ref_clk)
begin
	if MMC_clk_reset = '0' then
		AXI_reg0_resetn	<= '0';
	elsif rising_edge(MMC_ref_clk) then
		AXI_reg0_resetn	<= MMC_LOCKED and AXI_delay_reset;
	end if;
end process;

process(AXI_ACLK0_st0_buf)
begin
if rising_edge(AXI_ACLK0_st0_buf) then
		AXI_reg2_resetn	<= AXI_reg1_resetn;
		AXI_reg1_resetn	<= AXI_reg0_resetn;

	end if;
end process;

process(AXIACLK0_st0_buf_reset_n,AXI_ACLK0_st0_buf)
begin
    if AXIACLK0_st0_buf_reset_n = '0' then
        resync_resetn_AXI   <= '0';
	elsif rising_edge(AXI_ACLK0_st0_buf) then
		resync_resetn_AXI	<= AXI_reg2_resetn;
	end if;
end process;

--*********************************************************************************
--  APB clock & reset sequence

IBUF_inst : IBUF
port map (
    I => ctrl_clk ,
    O => APB_PCLK_bufI
);

BUFG_inst : BUFG
port map (
    I => APB_PCLK_bufI,
    O => APB_PCLK_bufO
);

process(resync_resetn_APB,APB_PCLK_bufO)
begin
    if resync_resetn_APB = '0' then
		APB_reset_cnt	<= (others => '0');
		APB_reset_n		<= '0';
    elsif rising_edge(APB_PCLK_bufO) then
		if APB_reset_cnt >= x"C8" then
			APB_reset_cnt	<= APB_reset_cnt;
			APB_reset_n		<= '1';
		else
			APB_reset_cnt	<= APB_reset_cnt + '1';
			APB_reset_n		<= '0';
		end if;
    end if;
end process;

--*********************************************************************************
WRITE_for_i1:process(HBM_write_in_mem,HBM_write_ack_cell,HBM_write_ready_cell,HBM_write_burst_ID_return_cell,HBM_write_burst_done_cell)
begin
    for I in  0 to number_of_HBM_port-1 loop
--    for I in  0 to number_of_HBM_port_hlf-1 loop
        HBM_write_enable_cell(I)(15 downto 00)	<= x"FFFF" when HBM_write_in_mem(I).word_enable(0) = '1' else x"0000";
        HBM_write_enable_cell(I)(31 downto 16)	<= x"FFFF" when HBM_write_in_mem(I).word_enable(1) = '1' else x"0000";

        HBM_write_out_mem(I).write_ack			<= '1' when HBM_write_ack_cell(I) 	= '1' else '0';
        HBM_write_out_mem(I).write_ready		<= '1' when HBM_write_ready_cell(I) = '1' else '0';

		HBM_write_out_mem(I).WrID_return        <= HBM_write_burst_ID_return_cell(I);
		HBM_write_out_mem(I).Wr_burst_done      <= HBM_write_burst_done_cell(I);

    end loop ;
end process;


HBM_inst0:TCP_stream_HBM
  PORT map(
-- first part for write
    HBM_REF_CLK_0       		=> HBM_clk_ref,

    AXI_00_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_00_ARESET_N     		=> resync_resetn_AXI,
    AXI_00_ARADDR(32 downto 28)	=> HBM_read_in_mem(0).H_ADD_rd,
    AXI_00_ARADDR(27 downto 00)	=> HBM_read_in_mem(0).address_rd,
    AXI_00_ARBURST      		=> "00",
    AXI_00_ARID         		=> HBM_read_in_mem(0).RdID,
    AXI_00_ARLEN        		=> HBM_read_in_mem(0).rdwd_length,
    AXI_00_ARSIZE       		=> "101",
    AXI_00_ARVALID      		=> HBM_read_in_mem(0).read_req	,
	AXI_00_ARREADY      		=> HBM_read_ack_cell(0),
    AXI_00_RDATA        		=> HBM_data_o_cell(0),
    AXI_00_RLAST        		=> HBM_rd_data_last_cell(0),
    AXI_00_RRESP        		=> HBM_read_out_mem(0).Rd_error(1 downto 0),
    AXI_00_RVALID       		=> HBM_rd_valid_cell(0),
    AXI_00_RREADY       		=> HBM_read_ready_cell(0),

    AXI_00_AWADDR(32 downto 28)	=> HBM_write_in_mem(0).H_ADD_wr,
    AXI_00_AWADDR(27 downto 00)	=> HBM_write_in_mem(0).address_wr,
    AXI_00_AWBURST      		=> "01",
    AXI_00_AWID         		=> HBM_write_in_mem(0).WrID,
    AXI_00_AWLEN        		=> HBM_write_in_mem(0).wrwd_length,
    AXI_00_AWSIZE       		=> "101",
    AXI_00_AWVALID      		=> HBM_write_in_mem(0).write_req,
    AXI_00_AWREADY      		=> HBM_write_ack_cell(0),
    AXI_00_WDATA        		=> HBM_write_in_mem(0).data_i,
    AXI_00_WLAST        		=> HBM_write_in_mem(0).last_data_i,
    AXI_00_WVALID       		=> HBM_write_in_mem(0).write_val,
    AXI_00_WREADY       		=> HBM_write_ready_cell(0),
    AXI_00_WSTRB        		=> HBM_write_enable_cell(0),
    AXI_00_WDATA_PARITY 		=> x"00000000",
    AXI_00_BID                  => HBM_write_burst_ID_return_cell(0),   -- ID of the burst Done
    AXI_00_BVALID       		=> HBM_write_burst_done_cell(0),     --Indicates write burst is done
	AXI_00_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_01_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_01_ARESET_N     		=> resync_resetn_AXI,
    AXI_01_ARADDR(32 downto 28)	=> HBM_read_in_mem(1).H_ADD_rd,
    AXI_01_ARADDR(27 downto 00)	=> HBM_read_in_mem(1).address_rd,
    AXI_01_ARBURST      		=> "00",
    AXI_01_ARID         		=> HBM_read_in_mem(1).RdID,
    AXI_01_ARLEN        		=> HBM_read_in_mem(1).rdwd_length,
    AXI_01_ARSIZE       		=> "101",
    AXI_01_ARVALID      		=> HBM_read_in_mem(1).read_req	,
	AXI_01_ARREADY      		=> HBM_read_ack_cell(1),
    AXI_01_RDATA        		=> HBM_data_o_cell(1),
    AXI_01_RLAST        		=> HBM_rd_data_last_cell(1),
    AXI_01_RRESP        		=> HBM_read_out_mem(1).Rd_error(1 downto 0),
    AXI_01_RVALID       		=> HBM_rd_valid_cell(1),
    AXI_01_RREADY       		=> HBM_read_ready_cell(1),

    AXI_01_AWADDR(32 downto 28)	=> HBM_write_in_mem(1).H_ADD_wr,
    AXI_01_AWADDR(27 downto 00)	=> HBM_write_in_mem(1).address_wr,
    AXI_01_AWBURST      		=> "01",
    AXI_01_AWID         		=> HBM_write_in_mem(1).WrID,
    AXI_01_AWLEN        		=> HBM_write_in_mem(1).wrwd_length,
    AXI_01_AWSIZE       		=> "101",
    AXI_01_AWVALID      		=> HBM_write_in_mem(1).write_req,
    AXI_01_AWREADY      		=> HBM_write_ack_cell(1),
    AXI_01_WDATA        		=> HBM_write_in_mem(1).data_i,
    AXI_01_WLAST        		=> HBM_write_in_mem(1).last_data_i,
    AXI_01_WVALID       		=> HBM_write_in_mem(1).write_val,
    AXI_01_WREADY       		=> HBM_write_ready_cell(1),
    AXI_01_WSTRB        		=> HBM_write_enable_cell(1),
    AXI_01_WDATA_PARITY 		=> x"00000000",
    AXI_01_BID                  => HBM_write_burst_ID_return_cell(1),   -- ID of the burst Done
    AXI_01_BVALID       		=> HBM_write_burst_done_cell(1),     --Indicates write burst is done
	AXI_01_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_02_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_02_ARESET_N     		=> resync_resetn_AXI,
    AXI_02_ARADDR(32 downto 28)	=> HBM_read_in_mem(2).H_ADD_rd,
    AXI_02_ARADDR(27 downto 00)	=> HBM_read_in_mem(2).address_rd,
    AXI_02_ARBURST      		=> "00",
    AXI_02_ARID         		=> HBM_read_in_mem(2).RdID,
    AXI_02_ARLEN        		=> HBM_read_in_mem(2).rdwd_length,
    AXI_02_ARSIZE       		=> "101",
    AXI_02_ARVALID      		=> HBM_read_in_mem(2).read_req	,
	AXI_02_ARREADY      		=> HBM_read_ack_cell(2),
    AXI_02_RDATA        		=> HBM_data_o_cell(2),
    AXI_02_RLAST        		=> HBM_rd_data_last_cell(2),
    AXI_02_RRESP        		=> HBM_read_out_mem(2).Rd_error(1 downto 0),
    AXI_02_RVALID       		=> HBM_rd_valid_cell(2),
    AXI_02_RREADY       		=> HBM_read_ready_cell(2),

    AXI_02_AWADDR(32 downto 28)	=> HBM_write_in_mem(2).H_ADD_wr,
    AXI_02_AWADDR(27 downto 00)	=> HBM_write_in_mem(2).address_wr,
    AXI_02_AWBURST      		=> "01",
    AXI_02_AWID         		=> HBM_write_in_mem(2).WrID,
    AXI_02_AWLEN        		=> HBM_write_in_mem(2).wrwd_length,
    AXI_02_AWSIZE       		=> "101",
    AXI_02_AWVALID      		=> HBM_write_in_mem(2).write_req,
    AXI_02_AWREADY      		=> HBM_write_ack_cell(2),
    AXI_02_WDATA        		=> HBM_write_in_mem(2).data_i,
    AXI_02_WLAST        		=> HBM_write_in_mem(2).last_data_i,
    AXI_02_WVALID       		=> HBM_write_in_mem(2).write_val,
    AXI_02_WREADY       		=> HBM_write_ready_cell(2),
    AXI_02_WSTRB        		=> HBM_write_enable_cell(2),
    AXI_02_WDATA_PARITY 		=> x"00000000",
    AXI_02_BID                  => HBM_write_burst_ID_return_cell(2),   -- ID of the burst Done
    AXI_02_BVALID       		=> HBM_write_burst_done_cell(2),     --Indicates write burst is done
	AXI_02_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_03_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_03_ARESET_N     		=> resync_resetn_AXI,
    AXI_03_ARADDR(32 downto 28)	=> HBM_read_in_mem(3).H_ADD_rd,
    AXI_03_ARADDR(27 downto 00)	=> HBM_read_in_mem(3).address_rd,
    AXI_03_ARBURST      		=> "00",
    AXI_03_ARID         		=> HBM_read_in_mem(3).RdID,
    AXI_03_ARLEN        		=> HBM_read_in_mem(3).rdwd_length,
    AXI_03_ARSIZE       		=> "101",
    AXI_03_ARVALID      		=> HBM_read_in_mem(3).read_req	,
	AXI_03_ARREADY      		=> HBM_read_ack_cell(3),
    AXI_03_RDATA        		=> HBM_data_o_cell(3),
    AXI_03_RLAST        		=> HBM_rd_data_last_cell(3),
    AXI_03_RRESP        		=> HBM_read_out_mem(3).Rd_error(1 downto 0),
    AXI_03_RVALID       		=> HBM_rd_valid_cell(3),
    AXI_03_RREADY       		=> HBM_read_ready_cell(3),

    AXI_03_AWADDR(32 downto 28)	=> HBM_write_in_mem(3).H_ADD_wr,
    AXI_03_AWADDR(27 downto 00)	=> HBM_write_in_mem(3).address_wr,
    AXI_03_AWBURST      		=> "01",
    AXI_03_AWID         		=> HBM_write_in_mem(3).WrID,
    AXI_03_AWLEN        		=> HBM_write_in_mem(3).wrwd_length,
    AXI_03_AWSIZE       		=> "101",
    AXI_03_AWVALID      		=> HBM_write_in_mem(3).write_req,
    AXI_03_AWREADY      		=> HBM_write_ack_cell(3),
    AXI_03_WDATA        		=> HBM_write_in_mem(3).data_i,
    AXI_03_WLAST        		=> HBM_write_in_mem(3).last_data_i,
    AXI_03_WVALID       		=> HBM_write_in_mem(3).write_val,
    AXI_03_WREADY       		=> HBM_write_ready_cell(3),
    AXI_03_WSTRB        		=> HBM_write_enable_cell(3),
    AXI_03_WDATA_PARITY 		=> x"00000000",
    AXI_03_BID                  => HBM_write_burst_ID_return_cell(3),   -- ID of the burst Done
    AXI_03_BVALID       		=> HBM_write_burst_done_cell(3),     --Indicates write burst is done
	AXI_03_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_04_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_04_ARESET_N     		=> resync_resetn_AXI,
    AXI_04_ARADDR(32 downto 28)	=>                HBM_read_in_mem(4).H_ADD_rd,
    AXI_04_ARADDR(27 downto 00)	=>                HBM_read_in_mem(4).address_rd,
    AXI_04_ARBURST      		=> "00",
    AXI_04_ARID         		=>                HBM_read_in_mem(4).RdID,
    AXI_04_ARLEN        		=>                HBM_read_in_mem(4).rdwd_length,
    AXI_04_ARSIZE       		=> "101",
    AXI_04_ARVALID      		=>                HBM_read_in_mem(4).read_req	,
	AXI_04_ARREADY      		=>              HBM_read_ack_cell(4),
    AXI_04_RDATA        		=>                HBM_data_o_cell(4),
    AXI_04_RLAST        		=>          HBM_rd_data_last_cell(4),
    AXI_04_RRESP        		=>               HBM_read_out_mem(4).Rd_error(1 downto 0),
    AXI_04_RVALID       		=>              HBM_rd_valid_cell(4),
    AXI_04_RREADY       		=>            HBM_read_ready_cell(4),

    AXI_04_AWADDR(32 downto 28)	=>               HBM_write_in_mem(4).H_ADD_wr,
    AXI_04_AWADDR(27 downto 00)	=>               HBM_write_in_mem(4).address_wr,
    AXI_04_AWBURST      		=> "01",
    AXI_04_AWID         		=>               HBM_write_in_mem(4).WrID,
    AXI_04_AWLEN        		=>               HBM_write_in_mem(4).wrwd_length,
    AXI_04_AWSIZE       		=> "101",
    AXI_04_AWVALID      		=>               HBM_write_in_mem(4).write_req,
    AXI_04_AWREADY      		=>             HBM_write_ack_cell(4),
    AXI_04_WDATA        		=>               HBM_write_in_mem(4).data_i,
    AXI_04_WLAST        		=>               HBM_write_in_mem(4).last_data_i,
    AXI_04_WVALID       		=>               HBM_write_in_mem(4).write_val,
    AXI_04_WREADY       		=>           HBM_write_ready_cell(4),
    AXI_04_WSTRB        		=>          HBM_write_enable_cell(4),
    AXI_04_WDATA_PARITY 		=> x"00000000",
    AXI_04_BID                  => HBM_write_burst_ID_return_cell(4),   -- ID of the burst Done
    AXI_04_BVALID       		=>      HBM_write_burst_done_cell(4),     --Indicates write burst is done
	AXI_04_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_05_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_05_ARESET_N     		=> resync_resetn_AXI,
    AXI_05_ARADDR(32 downto 28)	=>                HBM_read_in_mem(5).H_ADD_rd,
    AXI_05_ARADDR(27 downto 00)	=>                HBM_read_in_mem(5).address_rd,
    AXI_05_ARBURST      		=> "00",
    AXI_05_ARID         		=>                HBM_read_in_mem(5).RdID,
    AXI_05_ARLEN        		=>                HBM_read_in_mem(5).rdwd_length,
    AXI_05_ARSIZE       		=> "101",
    AXI_05_ARVALID      		=>                HBM_read_in_mem(5).read_req	,
	AXI_05_ARREADY      		=>              HBM_read_ack_cell(5),
    AXI_05_RDATA        		=>                HBM_data_o_cell(5),
    AXI_05_RLAST        		=>          HBM_rd_data_last_cell(5),
    AXI_05_RRESP        		=>               HBM_read_out_mem(5).Rd_error(1 downto 0),
    AXI_05_RVALID       		=>              HBM_rd_valid_cell(5),
    AXI_05_RREADY       		=>            HBM_read_ready_cell(5),

    AXI_05_AWADDR(32 downto 28)	=>               HBM_write_in_mem(5).H_ADD_wr,
    AXI_05_AWADDR(27 downto 00)	=>               HBM_write_in_mem(5).address_wr,
    AXI_05_AWBURST      		=> "01",
    AXI_05_AWID         		=>               HBM_write_in_mem(5).WrID,
    AXI_05_AWLEN        		=>               HBM_write_in_mem(5).wrwd_length,
    AXI_05_AWSIZE       		=> "101",
    AXI_05_AWVALID      		=>               HBM_write_in_mem(5).write_req,
    AXI_05_AWREADY      		=>             HBM_write_ack_cell(5),
    AXI_05_WDATA        		=>               HBM_write_in_mem(5).data_i,
    AXI_05_WLAST        		=>               HBM_write_in_mem(5).last_data_i,
    AXI_05_WVALID       		=>               HBM_write_in_mem(5).write_val,
    AXI_05_WREADY       		=>           HBM_write_ready_cell(5),
    AXI_05_WSTRB        		=>          HBM_write_enable_cell(5),
    AXI_05_WDATA_PARITY 		=> x"00000000",
    AXI_05_BID                  => HBM_write_burst_ID_return_cell(5),   -- ID of the burst Done
    AXI_05_BVALID       		=>      HBM_write_burst_done_cell(5),     --Indicates write burst is done
	AXI_05_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_06_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_06_ARESET_N     		=> resync_resetn_AXI,
    AXI_06_ARADDR(32 downto 28)	=>                HBM_read_in_mem(6).H_ADD_rd,
    AXI_06_ARADDR(27 downto 00)	=>                HBM_read_in_mem(6).address_rd,
    AXI_06_ARBURST      		=> "00",
    AXI_06_ARID         		=>                HBM_read_in_mem(6).RdID,
    AXI_06_ARLEN        		=>                HBM_read_in_mem(6).rdwd_length,
    AXI_06_ARSIZE       		=> "101",
    AXI_06_ARVALID      		=>                HBM_read_in_mem(6).read_req	,
	AXI_06_ARREADY      		=>              HBM_read_ack_cell(6),
    AXI_06_RDATA        		=>                HBM_data_o_cell(6),
    AXI_06_RLAST        		=>          HBM_rd_data_last_cell(6),
    AXI_06_RRESP        		=>               HBM_read_out_mem(6).Rd_error(1 downto 0),
    AXI_06_RVALID       		=>              HBM_rd_valid_cell(6),
    AXI_06_RREADY       		=>            HBM_read_ready_cell(6),

    AXI_06_AWADDR(32 downto 28)	=>               HBM_write_in_mem(6).H_ADD_wr,
    AXI_06_AWADDR(27 downto 00)	=>               HBM_write_in_mem(6).address_wr,
    AXI_06_AWBURST      		=> "01",
    AXI_06_AWID         		=>               HBM_write_in_mem(6).WrID,
    AXI_06_AWLEN        		=>               HBM_write_in_mem(6).wrwd_length,
    AXI_06_AWSIZE       		=> "101",
    AXI_06_AWVALID      		=>               HBM_write_in_mem(6).write_req,
    AXI_06_AWREADY      		=>             HBM_write_ack_cell(6),
    AXI_06_WDATA        		=>               HBM_write_in_mem(6).data_i,
    AXI_06_WLAST        		=>               HBM_write_in_mem(6).last_data_i,
    AXI_06_WVALID       		=>               HBM_write_in_mem(6).write_val,
    AXI_06_WREADY       		=>           HBM_write_ready_cell(6),
    AXI_06_WSTRB        		=>          HBM_write_enable_cell(6),
    AXI_06_WDATA_PARITY 		=> x"00000000",
    AXI_06_BID                  => HBM_write_burst_ID_return_cell(6),   -- ID of the burst Done
    AXI_06_BVALID       		=>      HBM_write_burst_done_cell(6),     --Indicates write burst is done
	AXI_06_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_07_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_07_ARESET_N     		=> resync_resetn_AXI,
    AXI_07_ARADDR(32 downto 28)	=>                HBM_read_in_mem(7).H_ADD_rd,
    AXI_07_ARADDR(27 downto 00)	=>                HBM_read_in_mem(7).address_rd,
    AXI_07_ARBURST      		=> "00",
    AXI_07_ARID         		=>                HBM_read_in_mem(7).RdID,
    AXI_07_ARLEN        		=>                HBM_read_in_mem(7).rdwd_length,
    AXI_07_ARSIZE       		=> "101",
    AXI_07_ARVALID      		=>                HBM_read_in_mem(7).read_req	,
	AXI_07_ARREADY      		=>              HBM_read_ack_cell(7),
    AXI_07_RDATA        		=>                HBM_data_o_cell(7),
    AXI_07_RLAST        		=>          HBM_rd_data_last_cell(7),
    AXI_07_RRESP        		=>               HBM_read_out_mem(7).Rd_error(1 downto 0),
    AXI_07_RVALID       		=>              HBM_rd_valid_cell(7),
    AXI_07_RREADY       		=>            HBM_read_ready_cell(7),

    AXI_07_AWADDR(32 downto 28)	=>               HBM_write_in_mem(7).H_ADD_wr,
    AXI_07_AWADDR(27 downto 00)	=>               HBM_write_in_mem(7).address_wr,
    AXI_07_AWBURST      		=> "01",
    AXI_07_AWID         		=>               HBM_write_in_mem(7).WrID,
    AXI_07_AWLEN        		=>               HBM_write_in_mem(7).wrwd_length,
    AXI_07_AWSIZE       		=> "101",
    AXI_07_AWVALID      		=>               HBM_write_in_mem(7).write_req,
    AXI_07_AWREADY      		=>             HBM_write_ack_cell(7),
    AXI_07_WDATA        		=>               HBM_write_in_mem(7).data_i,
    AXI_07_WLAST        		=>               HBM_write_in_mem(7).last_data_i,
    AXI_07_WVALID       		=>               HBM_write_in_mem(7).write_val,
    AXI_07_WREADY       		=>           HBM_write_ready_cell(7),
    AXI_07_WSTRB        		=>          HBM_write_enable_cell(7),
    AXI_07_WDATA_PARITY 		=> x"00000000",
    AXI_07_BID                  => HBM_write_burst_ID_return_cell(7),   -- ID of the burst Done
    AXI_07_BVALID       		=>      HBM_write_burst_done_cell(7),     --Indicates write burst is done
	AXI_07_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_08_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_08_ARESET_N     		=> resync_resetn_AXI,
    AXI_08_ARADDR(32 downto 28)	=>                HBM_read_in_mem(8).H_ADD_rd,
    AXI_08_ARADDR(27 downto 00)	=>                HBM_read_in_mem(8).address_rd,
    AXI_08_ARBURST      		=> "00",
    AXI_08_ARID         		=>                HBM_read_in_mem(8).RdID,
    AXI_08_ARLEN        		=>                HBM_read_in_mem(8).rdwd_length,
    AXI_08_ARSIZE       		=> "101",
    AXI_08_ARVALID      		=>                HBM_read_in_mem(8).read_req	,
	AXI_08_ARREADY      		=>              HBM_read_ack_cell(8),
    AXI_08_RDATA        		=>                HBM_data_o_cell(8),
    AXI_08_RLAST        		=>          HBM_rd_data_last_cell(8),
    AXI_08_RRESP        		=>               HBM_read_out_mem(8).Rd_error(1 downto 0),
    AXI_08_RVALID       		=>              HBM_rd_valid_cell(8),
    AXI_08_RREADY       		=>            HBM_read_ready_cell(8),

    AXI_08_AWADDR(32 downto 28)	=>               HBM_write_in_mem(8).H_ADD_wr,
    AXI_08_AWADDR(27 downto 00)	=>               HBM_write_in_mem(8).address_wr,
    AXI_08_AWBURST      		=> "01",
    AXI_08_AWID         		=>               HBM_write_in_mem(8).WrID,
    AXI_08_AWLEN        		=>               HBM_write_in_mem(8).wrwd_length,
    AXI_08_AWSIZE       		=> "101",
    AXI_08_AWVALID      		=>               HBM_write_in_mem(8).write_req,
    AXI_08_AWREADY      		=>             HBM_write_ack_cell(8),
    AXI_08_WDATA        		=>               HBM_write_in_mem(8).data_i,
    AXI_08_WLAST        		=>               HBM_write_in_mem(8).last_data_i,
    AXI_08_WVALID       		=>               HBM_write_in_mem(8).write_val,
    AXI_08_WREADY       		=>           HBM_write_ready_cell(8),
    AXI_08_WSTRB        		=>          HBM_write_enable_cell(8),
    AXI_08_WDATA_PARITY 		=> x"00000000",
    AXI_08_BID                  => HBM_write_burst_ID_return_cell(8),   -- ID of the burst Done
    AXI_08_BVALID       		=>      HBM_write_burst_done_cell(8),     --Indicates write burst is done
	AXI_08_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_09_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_09_ARESET_N     		=> resync_resetn_AXI,
    AXI_09_ARADDR(32 downto 28)	=>                HBM_read_in_mem(9).H_ADD_rd,
    AXI_09_ARADDR(27 downto 00)	=>                HBM_read_in_mem(9).address_rd,
    AXI_09_ARBURST      		=> "00",
    AXI_09_ARID         		=>                HBM_read_in_mem(9).RdID,
    AXI_09_ARLEN        		=>                HBM_read_in_mem(9).rdwd_length,
    AXI_09_ARSIZE       		=> "101",
    AXI_09_ARVALID      		=>                HBM_read_in_mem(9).read_req	,
	AXI_09_ARREADY      		=>              HBM_read_ack_cell(9),
    AXI_09_RDATA        		=>                HBM_data_o_cell(9),
    AXI_09_RLAST        		=>          HBM_rd_data_last_cell(9),
    AXI_09_RRESP        		=>               HBM_read_out_mem(9).Rd_error(1 downto 0),
    AXI_09_RVALID       		=>              HBM_rd_valid_cell(9),
    AXI_09_RREADY       		=>            HBM_read_ready_cell(9),

    AXI_09_AWADDR(32 downto 28)	=>               HBM_write_in_mem(9).H_ADD_wr,
    AXI_09_AWADDR(27 downto 00)	=>               HBM_write_in_mem(9).address_wr,
    AXI_09_AWBURST      		=> "01",
    AXI_09_AWID         		=>               HBM_write_in_mem(9).WrID,
    AXI_09_AWLEN        		=>               HBM_write_in_mem(9).wrwd_length,
    AXI_09_AWSIZE       		=> "101",
    AXI_09_AWVALID      		=>               HBM_write_in_mem(9).write_req,
    AXI_09_AWREADY      		=>             HBM_write_ack_cell(9),
    AXI_09_WDATA        		=>               HBM_write_in_mem(9).data_i,
    AXI_09_WLAST        		=>               HBM_write_in_mem(9).last_data_i,
    AXI_09_WVALID       		=>               HBM_write_in_mem(9).write_val,
    AXI_09_WREADY       		=>           HBM_write_ready_cell(9),
    AXI_09_WSTRB        		=>          HBM_write_enable_cell(9),
    AXI_09_WDATA_PARITY 		=> x"00000000",
    AXI_09_BID                  => HBM_write_burst_ID_return_cell(9),   -- ID of the burst Done
    AXI_09_BVALID       		=>      HBM_write_burst_done_cell(9),     --Indicates write burst is done
	AXI_09_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_10_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_10_ARESET_N     		=> resync_resetn_AXI,
    AXI_10_ARADDR(32 downto 28)	=>                HBM_read_in_mem(10).H_ADD_rd,
    AXI_10_ARADDR(27 downto 00)	=>                HBM_read_in_mem(10).address_rd,
    AXI_10_ARBURST      		=> "00",
    AXI_10_ARID         		=>                HBM_read_in_mem(10).RdID,
    AXI_10_ARLEN        		=>                HBM_read_in_mem(10).rdwd_length,
    AXI_10_ARSIZE       		=> "101",
    AXI_10_ARVALID      		=>                HBM_read_in_mem(10).read_req	,
	AXI_10_ARREADY      		=>              HBM_read_ack_cell(10),
    AXI_10_RDATA        		=>                HBM_data_o_cell(10),
    AXI_10_RLAST        		=>          HBM_rd_data_last_cell(10),
    AXI_10_RRESP        		=>               HBM_read_out_mem(10).Rd_error(1 downto 0),
    AXI_10_RVALID       		=>              HBM_rd_valid_cell(10),
    AXI_10_RREADY       		=>            HBM_read_ready_cell(10),

    AXI_10_AWADDR(32 downto 28)	=>               HBM_write_in_mem(10).H_ADD_wr,
    AXI_10_AWADDR(27 downto 00)	=>               HBM_write_in_mem(10).address_wr,
    AXI_10_AWBURST      		=> "01",
    AXI_10_AWID         		=>               HBM_write_in_mem(10).WrID,
    AXI_10_AWLEN        		=>               HBM_write_in_mem(10).wrwd_length,
    AXI_10_AWSIZE       		=> "101",
    AXI_10_AWVALID      		=>               HBM_write_in_mem(10).write_req,
    AXI_10_AWREADY      		=>             HBM_write_ack_cell(10),
    AXI_10_WDATA        		=>               HBM_write_in_mem(10).data_i,
    AXI_10_WLAST        		=>               HBM_write_in_mem(10).last_data_i,
    AXI_10_WVALID       		=>               HBM_write_in_mem(10).write_val,
    AXI_10_WREADY       		=>           HBM_write_ready_cell(10),
    AXI_10_WSTRB        		=>          HBM_write_enable_cell(10),
    AXI_10_WDATA_PARITY 		=> x"00000000",
    AXI_10_BID                  => HBM_write_burst_ID_return_cell(10),   -- ID of the burst Done
    AXI_10_BVALID       		=>      HBM_write_burst_done_cell(10),     --Indicates write burst is done
	AXI_10_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_11_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_11_ARESET_N     		=> resync_resetn_AXI,
    AXI_11_ARADDR(32 downto 28)	=>                HBM_read_in_mem(11).H_ADD_rd,
    AXI_11_ARADDR(27 downto 00)	=>                HBM_read_in_mem(11).address_rd,
    AXI_11_ARBURST      		=> "00",
    AXI_11_ARID         		=>                HBM_read_in_mem(11).RdID,
    AXI_11_ARLEN        		=>                HBM_read_in_mem(11).rdwd_length,
    AXI_11_ARSIZE       		=> "101",
    AXI_11_ARVALID      		=>                HBM_read_in_mem(11).read_req	,
	AXI_11_ARREADY      		=>              HBM_read_ack_cell(11),
    AXI_11_RDATA        		=>                HBM_data_o_cell(11),
    AXI_11_RLAST        		=>          HBM_rd_data_last_cell(11),
    AXI_11_RRESP        		=>               HBM_read_out_mem(11).Rd_error(1 downto 0),
    AXI_11_RVALID       		=>              HBM_rd_valid_cell(11),
    AXI_11_RREADY       		=>            HBM_read_ready_cell(11),

    AXI_11_AWADDR(32 downto 28)	=>               HBM_write_in_mem(11).H_ADD_wr,
    AXI_11_AWADDR(27 downto 00)	=>               HBM_write_in_mem(11).address_wr,
    AXI_11_AWBURST      		=> "01",
    AXI_11_AWID         		=>               HBM_write_in_mem(11).WrID,
    AXI_11_AWLEN        		=>               HBM_write_in_mem(11).wrwd_length,
    AXI_11_AWSIZE       		=> "101",
    AXI_11_AWVALID      		=>               HBM_write_in_mem(11).write_req,
    AXI_11_AWREADY      		=>             HBM_write_ack_cell(11),
    AXI_11_WDATA        		=>               HBM_write_in_mem(11).data_i,
    AXI_11_WLAST        		=>               HBM_write_in_mem(11).last_data_i,
    AXI_11_WVALID       		=>               HBM_write_in_mem(11).write_val,
    AXI_11_WREADY       		=>           HBM_write_ready_cell(11),
    AXI_11_WSTRB        		=>          HBM_write_enable_cell(11),
    AXI_11_WDATA_PARITY 		=> x"00000000",
    AXI_11_BID                  => HBM_write_burst_ID_return_cell(11),   -- ID of the burst Done
    AXI_11_BVALID       		=>      HBM_write_burst_done_cell(11),     --Indicates write burst is done
	AXI_11_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_12_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_12_ARESET_N     		=> resync_resetn_AXI,
    AXI_12_ARADDR(32 downto 28)	=>                HBM_read_in_mem(12).H_ADD_rd,
    AXI_12_ARADDR(27 downto 00)	=>                HBM_read_in_mem(12).address_rd,
    AXI_12_ARBURST      		=> "00",
    AXI_12_ARID         		=>                HBM_read_in_mem(12).RdID,
    AXI_12_ARLEN        		=>                HBM_read_in_mem(12).rdwd_length,
    AXI_12_ARSIZE       		=> "101",
    AXI_12_ARVALID      		=>                HBM_read_in_mem(12).read_req	,
	AXI_12_ARREADY      		=>              HBM_read_ack_cell(12),
    AXI_12_RDATA        		=>                HBM_data_o_cell(12),
    AXI_12_RLAST        		=>          HBM_rd_data_last_cell(12),
    AXI_12_RRESP        		=>               HBM_read_out_mem(12).Rd_error(1 downto 0),
    AXI_12_RVALID       		=>              HBM_rd_valid_cell(12),
    AXI_12_RREADY       		=>            HBM_read_ready_cell(12),

    AXI_12_AWADDR(32 downto 28)	=>               HBM_write_in_mem(12).H_ADD_wr,
    AXI_12_AWADDR(27 downto 00)	=>               HBM_write_in_mem(12).address_wr,
    AXI_12_AWBURST      		=> "01",
    AXI_12_AWID         		=>               HBM_write_in_mem(12).WrID,
    AXI_12_AWLEN        		=>               HBM_write_in_mem(12).wrwd_length,
    AXI_12_AWSIZE       		=> "101",
    AXI_12_AWVALID      		=>               HBM_write_in_mem(12).write_req,
    AXI_12_AWREADY      		=>             HBM_write_ack_cell(12),
    AXI_12_WDATA        		=>               HBM_write_in_mem(12).data_i,
    AXI_12_WLAST        		=>               HBM_write_in_mem(12).last_data_i,
    AXI_12_WVALID       		=>               HBM_write_in_mem(12).write_val,
    AXI_12_WREADY       		=>           HBM_write_ready_cell(12),
    AXI_12_WSTRB        		=>          HBM_write_enable_cell(12),
    AXI_12_WDATA_PARITY 		=> x"00000000",
    AXI_12_BID                  => HBM_write_burst_ID_return_cell(12),   -- ID of the burst Done
    AXI_12_BVALID       		=>      HBM_write_burst_done_cell(12),     --Indicates write burst is done
	AXI_12_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_13_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_13_ARESET_N     		=> resync_resetn_AXI,
    AXI_13_ARADDR(32 downto 28)	=>                HBM_read_in_mem(13).H_ADD_rd,
    AXI_13_ARADDR(27 downto 00)	=>                HBM_read_in_mem(13).address_rd,
    AXI_13_ARBURST      		=> "00",
    AXI_13_ARID         		=>                HBM_read_in_mem(13).RdID,
    AXI_13_ARLEN        		=>                HBM_read_in_mem(13).rdwd_length,
    AXI_13_ARSIZE       		=> "101",
    AXI_13_ARVALID      		=>                HBM_read_in_mem(13).read_req	,
	AXI_13_ARREADY      		=>              HBM_read_ack_cell(13),
    AXI_13_RDATA        		=>                HBM_data_o_cell(13),
    AXI_13_RLAST        		=>          HBM_rd_data_last_cell(13),
    AXI_13_RRESP        		=>               HBM_read_out_mem(13).Rd_error(1 downto 0),
    AXI_13_RVALID       		=>              HBM_rd_valid_cell(13),
    AXI_13_RREADY       		=>            HBM_read_ready_cell(13),

    AXI_13_AWADDR(32 downto 28)	=>               HBM_write_in_mem(13).H_ADD_wr,
    AXI_13_AWADDR(27 downto 00)	=>               HBM_write_in_mem(13).address_wr,
    AXI_13_AWBURST      		=> "01",
    AXI_13_AWID         		=>               HBM_write_in_mem(13).WrID,
    AXI_13_AWLEN        		=>               HBM_write_in_mem(13).wrwd_length,
    AXI_13_AWSIZE       		=> "101",
    AXI_13_AWVALID      		=>               HBM_write_in_mem(13).write_req,
    AXI_13_AWREADY      		=>             HBM_write_ack_cell(13),
    AXI_13_WDATA        		=>               HBM_write_in_mem(13).data_i,
    AXI_13_WLAST        		=>               HBM_write_in_mem(13).last_data_i,
    AXI_13_WVALID       		=>               HBM_write_in_mem(13).write_val,
    AXI_13_WREADY       		=>           HBM_write_ready_cell(13),
    AXI_13_WSTRB        		=>          HBM_write_enable_cell(13),
    AXI_13_WDATA_PARITY 		=> x"00000000",
    AXI_13_BID                  => HBM_write_burst_ID_return_cell(13),   -- ID of the burst Done
    AXI_13_BVALID       		=>      HBM_write_burst_done_cell(13),     --Indicates write burst is done
	AXI_13_BREADY       		=> '1',--resync_resetn_AXI,


    AXI_14_ACLK         		=> AXI_ACLK0_st0_buf,
    AXI_14_ARESET_N     		=> resync_resetn_AXI,

	AXI_14_ARREADY      		=> User_HBM_add_ready, -- read address ready
    AXI_14_ARADDR       		=> HBM_User_address,
    AXI_14_ARBURST      		=> "00",
    AXI_14_ARID         		=> "000000", -- read address ID
    AXI_14_ARLEN        		=> "1111",
    AXI_14_ARSIZE       		=> "101",
    AXI_14_ARVALID      		=> HBM_User_read_req(2),
    -- AXI_14_RDATA_PARITY 		=> ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_14_RDATA        		=> HBM_User_data,--: OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    -- AXI_14_RID          		=> ,--: OUT STD_LOGIC_VECTOR(5 DOWNTO 0); -- Read ID tag
    AXI_14_RLAST        		=> HBM_User_Lastdata_val,--: OUT STD_LOGIC;
    -- AXI_14_RRESP        		=> ,--: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);-- read response
    AXI_14_RVALID       		=> HBM_User_data_val,--: OUT STD_LOGIC;
    AXI_14_RREADY       		=> '1',--: IN STD_LOGIC;-- read ready

    -- AXI_14_AWREADY      		=> ,--: OUT STD_LOGIC;				-- write address ready
    AXI_14_AWADDR       		=> "000000000000000000000000000000000",--: IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    AXI_14_AWBURST      		=> "00",--: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI_14_AWID         		=> "000000",--: IN STD_LOGIC_VECTOR(5 DOWNTO 0);-- write address ID
    AXI_14_AWLEN        		=> "0000",--: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI_14_AWSIZE       		=> "000",--: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    AXI_14_AWVALID      		=> '0',--: IN STD_LOGIC;
    AXI_14_BREADY       		=> '0',--: IN STD_LOGIC;-- Response Ready (write)
    AXI_14_WDATA        		=> x"0000000000000000000000000000000000000000000000000000000000000000",--: IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    AXI_14_WLAST        		=> '0',--: IN STD_LOGIC;
    AXI_14_WSTRB        		=> x"00000000",--: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_14_WDATA_PARITY 		=> x"00000000",--: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI_14_WVALID       		=> '0',--: IN STD_LOGIC;
    -- AXI_14_WREADY       		=> ,--: OUT STD_LOGIC;
    -- AXI_14_BID          		=> ,--: OUT STD_LOGIC_VECTOR(5 DOWNTO 0);--response ID tag (write)
    -- AXI_14_BRESP        		=> ,--: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);-- write response (write transaction)
    -- AXI_14_BVALID       		=> ,--: OUT STD_LOGIC;	-- write response valid

    APB_0_PRESET_N      		=> APB_reset_n,
    APB_0_PCLK          		=> APB_PCLK_bufO,--ctrl_clk,
    APB_0_PADDR         		=> "0000000000000000000000" ,--m_apb_paddr,
    APB_0_PSEL          		=> '0',--m_apb_psel(0),
    APB_0_PWRITE        		=> '0',--m_apb_pwrite,
    APB_0_PWDATA        		=> m_apb_pwdata,
    APB_0_PENABLE       		=> '0',--m_apb_penable,
    APB_0_PRDATA        		=> m_apb_prdata,
    APB_0_PREADY        		=> m_apb_pready(0),
    APB_0_PSLVERR       		=> m_apb_pslverr(0),

    apb_complete_0      		=> Memory_ready,

    DRAM_0_STAT_CATTRIP 		=> Catastrophic_temp ,
    DRAM_0_STAT_TEMP    		=> HBM_temperature
  );


READ_for_i1:process(HBM_read_ack_cell,AXI_ACLK0_st0_buf,pipe_rd_data_last,pipe_data_o,pipe_rd_valid_data)
begin
    for I in  0 to number_of_HBM_port-1 loop

		HBM_read_ready_cell(I)			<= '1';

		HBM_read_out_mem(I).read_ack		<= '1' when HBM_read_ack_cell(I) = '1' else '0';
		HBM_read_out_mem(I).rd_data_last	<= pipe_rd_data_last(I);
		HBM_read_out_mem(I).data_o			<= pipe_data_o(I);
		HBM_read_out_mem(I).rd_valid_data	<= pipe_rd_valid_data(I);

		 if rising_edge(AXI_ACLK0_st0_buf) then
			pipe_data_o(I)			<= HBM_data_o_cell(I);
			pipe_rd_data_last(I)	<= HBM_rd_data_last_cell(I);
			pipe_rd_valid_data(I)	<= HBM_rd_valid_cell(I);
		end if;
    end loop ;
end process;


HBM_memory_ready  	<= Memory_ready;
clock_interface		<= AXI_ACLK0_st0_buf;

-- interface to access the HBM registers
AXI_to_APB:HBM_AXI_to_APB
 PORT map(
   s_axi_aclk    				=> APB_PCLK_bufO,
   s_axi_aresetn 				=> APB_reset_n,
   s_axi_awaddr  				=> HBM_ctrl_awaddr(21 downto 0)  ,
   s_axi_awprot  				=> HBM_ctrl_awprot  ,
   s_axi_awvalid 				=> HBM_ctrl_awvalid ,
   s_axi_awready 				=> HBM_ctrl_awready ,
   s_axi_wdata   				=> HBM_ctrl_wdata   ,
   s_axi_wstrb   				=> HBM_ctrl_wstrb   ,
   s_axi_wvalid  				=> HBM_ctrl_wvalid  ,
   s_axi_wready  				=> HBM_ctrl_wready  ,
   s_axi_bresp   				=> HBM_ctrl_bresp   ,
   s_axi_bvalid  				=> HBM_ctrl_bvalid  ,
   s_axi_bready  				=> HBM_ctrl_bready  ,
   s_axi_araddr  				=> HBM_ctrl_araddr(21 downto 0)  ,
   s_axi_arprot  				=> HBM_ctrl_arprot  ,
   s_axi_arvalid 				=> HBM_ctrl_arvalid ,
   s_axi_arready 				=> HBM_ctrl_arready ,
   s_axi_rdata   				=> HBM_ctrl_rdata   ,
   s_axi_rresp   				=> HBM_ctrl_rresp   ,
   s_axi_rvalid  				=> HBM_ctrl_rvalid  ,
   s_axi_rready  				=> HBM_ctrl_rready  ,

   m_apb_paddr   				=> m_apb_paddr,
   m_apb_psel    				=> m_apb_psel,
   m_apb_penable 				=> m_apb_penable,
   m_apb_pwrite  				=> m_apb_pwrite,
   m_apb_pwdata  				=> m_apb_pwdata,
   m_apb_pready  				=> m_apb_pready,
   m_apb_prdata  				=> m_apb_prdata,
   m_apb_pslverr 				=> m_apb_pslverr,
   m_apb_pprot   				=> m_apb_pprot,
   m_apb_pstrb   				=> m_apb_pstrb
);


--I keep the bufg and the MMC like inthe example design
-- I simulate without them, it works
BUFG_inst1 : BUFG
port map (
    O => AXI_ACLK0_st0_buf, -- 1-bit output: Clock output
    I => MMC_Data_clk -- 1-bit input: Clock input
);

--AXI_ACLK0_st0_buf <= MMC_Data_clk;

BUFG_inst11 : BUFG
port map (
    O => MMC_ref_clk, -- 1-bit output: Clock output
    I => Data_clk -- 1-bit input: Clock input
);


--reset the MMC
process(MMC_ref_clk)
begin
	if  MMC_clk_reset = '0'  then
		MMC_RSTn				<= '0';
		MMC_delay_reset_counter	<= (others => '0');
	elsif rising_edge(MMC_ref_clk) then

		if MMC_delay_reset_counter <= x"64" then
			MMC_delay_reset_counter <= MMC_delay_reset_counter + '1';
			MMC_RSTn                <= '0';

		else
			MMC_RSTn				<= '1';
			MMC_delay_reset_counter	<= MMC_delay_reset_counter;
		end if;
	end if;
end process;


MMC_RSTp	<= not(MMC_RSTn);

MMCME4_ADV_inst : MMCME4_ADV
generic map (
	BANDWIDTH            		=> "OPTIMIZED", -- Jitter programming
	CLKOUT4_CASCADE      		=> "FALSE", -- Divide amount for CLKOUT (1-128)
	COMPENSATION         		=> "INTERNAL", -- Clock input compensation
	STARTUP_WAIT         		=> "FALSE", -- Delays DONE until MMCM is locked
	DIVCLK_DIVIDE        		=> MMCM_DIVCLK_DIVIDE, -- Master division value
	CLKFBOUT_MULT_F      		=> MMCM_CLKFBOUT_MULT_F, -- Multiply value for all CLKOUT
	CLKFBOUT_PHASE       		=> 0.0, -- Phase offset in degrees of CLKFB
	CLKFBOUT_USE_FINE_PS 		=> "FALSE", -- Fine phase shift enable (TRUE/FALSE)
	CLKOUT0_DIVIDE_F			=> MMCM_CLKOUT0_DIVIDE_F_real,
	CLKOUT0_PHASE        		=> 0.0, -- Phase offset for CLKOUT0
	CLKOUT0_DUTY_CYCLE   		=> 0.5, -- Duty cycle for CLKOUT0
	CLKOUT0_USE_FINE_PS  		=> "FALSE", -- Fine phase shift enable (TRUE/FALSE)

	CLKOUT1_DIVIDE       		=> MMCM_CLKOUT0_DIVIDE_F, -- Divide amount for CLKOUT (1-128)
	CLKOUT2_DIVIDE       		=> MMCM_CLKOUT0_DIVIDE_F, -- Divide amount for CLKOUT (1-128)
	CLKOUT3_DIVIDE       		=> MMCM_CLKOUT0_DIVIDE_F, -- Divide amount for CLKOUT (1-128)
	CLKOUT4_DIVIDE       		=> MMCM_CLKOUT0_DIVIDE_F, -- Divide amount for CLKOUT (1-128)
	CLKOUT5_DIVIDE       		=> MMCM_CLKOUT0_DIVIDE_F, -- Divide amount for CLKOUT (1-128)
	CLKOUT6_DIVIDE       		=> MMCM_CLKOUT0_DIVIDE_F, -- Divide amount for CLKOUT (1-128)

	CLKIN1_PERIOD        		=> MMCM_CLKIN1_PERIOD, -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz). CLKIN2_PERIOD => 0.0, -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz). CLKOUT0_DIVIDE_F => 1.0, -- Divide amount for CLKOUT0
	REF_JITTER1          		=> 0.010  -- Reference input jitter in UI (0.000-0.999).
)
port map (
	RST                  		=> MMC_RSTp, -- 1-bit input: Reset

	CLKOUT0              		=> MMC_Data_clk, -- 1-bit output: CLKOUT0
	CLKOUT0B             		=> open, -- 1-bit output: CLKOUT0
	CLKOUT1              		=> MMC_Reset_clk, -- 1-bit output: CLKOUT1
	CLKOUT1B             		=> open, -- 1-bit output: CLKOUT1
	-- CLKOUT2              		=> CLKOUT2, -- 1-bit output: CLKOUT2
	-- CLKOUT3              		=> CLKOUT3, -- 1-bit output: CLKOUT3
	-- CLKOUT4              		=> CLKOUT4, -- 1-bit output: CLKOUT4
	-- CLKOUT5              		=> CLKOUT5, -- 1-bit output: CLKOUT5
	-- CLKOUT6              		=> CLKOUT6, -- 1-bit output: CLKOUT6

	CLKIN1               		=> MMC_ref_clk, -- 1-bit input: Primary clock
	CLKIN2               		=> '0', -- 1-bit input: Secondary clock
    CLKFBIN                     => '0',--MMC_CLKFBOUT,
	LOCKED               		=> MMC_LOCKED, -- 1-bit output: LOCK

    PWRDWN                      => '0',
	-- CDDCDONE             		=> CDDCDONE, -- 1-bit output: Clock dynamic divide done
	CLKFBOUT             		=> open,--MMC_CLKFBOUT, -- 1-bit output: Feedback clock
	CLKFBOUTB            		=> open,--MMC_CLKFBOUT, -- 1-bit output: Feedback clock
	-- CLKFBOUTB            		=> CLKFBOUTB, -- 1-bit output: Inverted CLKFBOUT
	-- CLKFBSTOPPED         		=> CLKFBSTOPPED, -- 1-bit output: Feedback clock stopped
	-- CLKINSTOPPED         		=> CLKINSTOPPED, -- 1-bit output: Input clock stopped
	-- CLKOUT0B             		=> CLKOUT0B, -- 1-bit output: Inverted CLKOUT0
	-- CLKOUT1B             		=> CLKOUT1B, -- 1-bit output: Inverted CLKOUT1
	-- CLKOUT2B             		=> CLKOUT2B, -- 1-bit output: Inverted CLKOUT2
	-- CLKOUT3B             		=> CLKOUT3B, -- 1-bit output: Inverted CLKOUT3
	-- DO                   		=> DO, -- 16-bit output: DRP data output
	-- DRDY                 		=> DRDY, -- 1-bit output: DRP ready
	-- PSDONE               		=> PSDONE, -- 1-bit output: Phase shift done
	CDDCREQ              		=> '0', -- 1-bit input: Request to dynamic divide clock CLKFBIN => CLKFBIN, -- 1-bit input: Feedback clock
	CLKINSEL             		=> '1', -- 1-bit input: Clock select, High=CLKIN1 Low=CLKIN2 DADDR => DADDR, -- 7-bit input: DRP address
	DCLK                 		=> '0', -- 1-bit input: DRP clock
	DEN                  		=> '0', -- 1-bit input: DRP enable
	DADDR						=> "0000000",
	DI                   		=> x"0000", -- 16-bit input: DRP data input
	DWE                  		=> '0', -- 1-bit input: DRP write enable
	PSCLK                		=> '0', -- 1-bit input: Phase shift clock
	PSEN                 		=> '0', -- 1-bit input: Phase shift enable
	PSINCDEC             		=> '0' -- 1-bit input: Phase shift increment/decrement PWRDWN => PWRDWN, -- 1-bit input: Power-down
);

--###############################################
--  HBM read random

resync_reset_i10:resetn_resync
port map(
	aresetn			=> FIFO_resetn ,
	clock			=> AXI_ACLK0_st0_buf,

--	Resetn_sync		: out std_logic;
	Resetp_sync		=> FIFO_resetp_resync
	);
--usr_rst_p	<= not(resync_resetn_AXI);

FIFO_read_i1:HBM_Read_random
  PORT MAP(
    rst        		    => FIFO_resetp_resync  ,
    wr_clk      		=> AXI_ACLK0_st0_buf   ,
    din         		=> HBM_User_data       ,
    wr_en       		=> HBM_User_data_val   ,

    rd_clk      		=> usr_clk             ,
    rd_en       		=> USER_readu_FIFO     ,
    dout        		=> User_Data_FIFO      ,
    empty       		=> User_data_FIFO_ready,
    full                => Fifo_read_full       ,
    wr_data_count		=> Number_of_words_in_FIFO
  );



---*****************************************************************************************************
--Interface  Read command


process(usr_clk)
begin
	if rising_edge(usr_clk) then

	   if usr_rden = '1' then
            Control_data_out 							<= (others => '0');

            if 		usr_func_rd(hbm_RW_control) = '1' then
                Control_data_out(16)         			<= not(User_data_FIFO_ready);
                Control_data_out(32 downto 24)			<= Number_of_words_in_FIFO;

            elsif usr_func_rd(hbm_read_data_A) = '1'  then
                Control_data_out         				<= User_Data_FIFO(063 downto 000);

            elsif usr_func_rd(hbm_read_data_B) = '1'  then
                Control_data_out         				<= User_Data_FIFO(127 downto 064);

            elsif usr_func_rd(hbm_read_data_C) = '1'  then
                Control_data_out         				<= User_Data_FIFO(191 downto 128);

            elsif usr_func_rd(hbm_read_data_D) = '1'  then
                Control_data_out         				<= User_Data_FIFO(255 downto 192);
            end if;
        end if;

		USER_readu_FIFO							<= '0';
		if usr_func_rd(hbm_read_data_D) = '1' and  usr_rden = '1' then
			USER_readu_FIFO							<= '1';
		end if;

	end if;
end process;

usr_data_rd	<= Control_data_out;
---*****************************************************************************************************
--Interface Write  command
process(usr_rst_n,usr_clk)
begin
	if usr_rst_n = '0' then
		HBM_User_address		<= (others => '0');
		HBM_User_read_req(0)	<= '0';
		FIFO_resetn            	<= '1';
	elsif rising_edge(usr_clk) then
	    HBM_User_read_req(0)				<= '0';
	    FIFO_resetn            	            <= '1';
		if usr_wen = '1'   then
			if usr_func_wr(hbm_write_address) = '1' then
				HBM_User_address				<= usr_data_wr(32 downto 0);
			end if;

			if usr_func_wr(hbm_RW_control) = '1' then
				HBM_User_read_req(0)			<= usr_data_wr(0);
				FIFO_resetn                     <= not(usr_data_wr(1));
			end if;

		end if;
	end if;
end process;

-- request request from Control_clock domain to HBM AI clock domain
sync_write_req_i1:resync_v4
	port map(
		aresetn				=> '1',
		clocki			    => usr_clk,
		input				=> HBM_User_read_req(0),
		clocko			    => AXI_ACLK0_st0_buf,
		output			    => HBM_User_read_req(1)
		);

-- maintains the request if the HBM is not ready
process(resync_resetn_AXI,AXI_ACLK0_st0_buf)
begin
	if resync_resetn_AXI = '0' then
		HBM_User_read_req(2) <= '0';
	elsif rising_edge(AXI_ACLK0_st0_buf) then
		if HBM_User_read_req(1) = '1' then
			HBM_User_read_req(2) <= '1';
		elsif User_HBM_add_ready = '1' then
			HBM_User_read_req(2) <= '0';
		end if;
	end if;
end process;

end Behavioral;

