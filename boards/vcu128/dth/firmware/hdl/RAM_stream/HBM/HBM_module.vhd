----------------------------------------------------------------------------------
-- Company: 
-- Engineer: D. Gigi
-- 
-- Create Date: 21.04.2017 14:23:45
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- updated to send out HBM almost full signal (R. Ardino, January 2023)
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.address_table.all;
use work.interface.all;
 
--ENTITY-----------------------------------------------------------
entity HBM_module is
	port
	(  
	-- Control local interface
		usr_clk								: in std_logic;
		rst_usr_clk_n						: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
			
		usr_func_rd							: in std_logic_vector(16383 downto 0); 
		usr_rden							: in std_logic;
		usr_dto								: out std_logic_vector(63 downto 0);
		
		HBM_almost_full                     : out HBM_bus_1_bit; --R

        HBM_RESETn                           : in std_logic;
        HBM_clk_ref                         : in std_logic;
        Clock_HBM                           : in std_logic;
		resetp_counter_i					: in std_logic; 
		
	-- DATA to HBM TCP stream
	    User_rstn_clock_buffer              : in std_logic;
		HBM_clock_buffer					: out std_logic;

		SRx_rcv_in							: in SR_rcv_data_hlf;
		SRx_rcv_out							: out SR_rcv_ctrl_hlf;
		 
	--Ethernet Connections
	    TCP_clock_resetn                    : in std_logic;
		TCP_clock							: in std_logic;
		resetp_counter_timer_resync         : in std_logic;
		
		DAQx_TCP_ack_rcv				 	: in TCP_ack_rcvo_type;
		DAQx_TCP_Ack_present				: in std_logic_vector(number_of_HBM_port-1 downto 0);
		DAQx_rd_TCP_val_rcvo				: out std_logic_vector(number_of_HBM_port-1 downto 0);
		
	--Packet ready to be read from the Ethernet logic
        DAQx_eth_packet_to_be_sent          : out HBM_to_TCP_block_pipe;
		DAQx_eth_gnt_access					: in std_logic_vector(number_of_HBM_port-1 downto 0); -- Give the access to DAQ unit
		DAQx_eth_ack_packet					: in std_logic_vector(number_of_HBM_port-1 downto 0); -- pulse to read CS and release the place
			
		HBM_memory_ready                    : out std_logic 
	 );
end HBM_module;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of HBM_module is
  
						 
COMPONENT data_manager_v2 is 
  Port ( 
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0); 
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0); 
		
		usr_func_rd					      	: in std_logic_vector(16383 downto 0); 
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0); 
		
		HBM_almost_full_data_manager_v2     : out HBM_bus_1_bit; --R

		HBM_RESETn                          : in std_logic;
		HBM_clk_ref							: in std_logic; -- 100 MHz
  		Data_clk							: in std_logic; -- 200-250 MHz
  		ctrl_clk							: in std_logic; -- 100 MHz	
		HBM_memory_ready					: out std_logic;
		resetp_counter_i                    : in std_logic;   

		--Fragment from the SlinkRocket
	    User_rstn_clock_buffer              : in std_logic;
		clock_HBM_AXI						: out std_logic;
		
		SRx_rcv_in							: in SR_rcv_data_hlf;
		SRx_rcv_out							: out SR_rcv_ctrl_hlf;
		
	    TCP_clock_resetn                    : in std_logic;
		TCP_clock							: in std_logic;
		resetp_counter_timer_resync         : in std_logic;

		DAQx_TCP_ack_rcv				 	: in TCP_ack_rcvo_type;
		DAQx_TCP_Ack_present				: in std_logic_vector(number_of_HBM_port-1 downto 0);
		DAQx_rd_TCP_val_rcvo				: out std_logic_vector(number_of_HBM_port-1 downto 0);
		
		--Packet ready to be read from the Ethernet logic
        DAQx_eth_packet_to_be_sent          : out HBM_to_TCP_block_pipe;
		DAQx_eth_gnt_access					: in std_logic_vector(number_of_HBM_port-1 downto 0); -- Give the access to DAQ unit
		DAQx_eth_ack_packet					: in std_logic_vector(number_of_HBM_port-1 downto 0) -- pulse to read CS and release the place
			
  );
end COMPONENT;

signal TCP_Buf_0_SND_NEW						: std_logic_vector(31 downto 0);
signal TCP_Buf_0_SND_UNA						: std_logic_vector(31 downto 0);
signal TCP_Buf_0_VALID_P						: std_logic; 

signal clock_HBM_AXI							: std_logic;
 
signal HBM_usr_dto								: STD_LOGIC_VECTOR(63 DOWNTO 0);

signal Req_read_HBM								: std_logic; 
  
signal TCP_ack_Send_req							: std_logic;
signal TCP_ack_Ack_req							: std_logic; 
signal ETH_SND_access                           : std_logic;
signal ETH_ACK_access                           : std_logic;
signal ETH_request								: std_logic;
signal ETH_request_sent							: std_logic;
signal HBM_blk_ready							: std_logic;
signal TCP_ST_A_Seq_num							: std_logic_vector(31 downto 0);
signal TCP_ST_A_Ack_num							: std_logic_vector(31 downto 0);
signal TCP_ST_A_Length							: std_logic_vector(31 downto 0);
signal TCP_ST_A_Flag							: std_logic_vector(7 downto 0);
signal TCP_ST_A_Option							: std_logic_vector(2 downto 0);
signal TCP_ST_A_wo_data							: std_logic ; 
signal TCP_ST_S_Ack_num							: std_logic_vector(31 downto 0); 
signal TCP_ST_S_Flag							: std_logic_vector(7 downto 0);
signal TCP_ST_S_Option							: std_logic_vector(2 downto 0);
signal TCP_ST_S_wo_data							: std_logic ; 


signal Eth_req_return_reg						: HBM_to_TCP_block_record;
 
COMPONENT resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic; 
	Resetn_sync			: out std_logic;
	Resetp_sync			: out std_logic
	);
end COMPONENT;		

COMPONENT resync_v4 is
	port (
		aresetn				: in std_logic;
		clocki			    : in std_logic;	
		input				: in std_logic;
		clocko			    : in std_logic;
		output			    : out std_logic
		);
end COMPONENT;
 		
signal resetn_CLK_Buf_resync					: std_logic; 
signal resetn_CLK_eth_resync					: std_logic;  

--attribute mark_debug : string;

--attribute mark_debug of xxxxx			: signal is "true";	

--***************************************************************** 
--*************<     CODE START HERE    >**************************
--***************************************************************** 
 begin 
 

HBM_data_i1:data_manager_v2 
  Port map( 
		usr_clk								=> usr_clk			 		,
		usr_rst_n							=> rst_usr_clk_n			,
		usr_func_wr							=> usr_func_wr				,
		usr_wen								=> usr_wen					,
		usr_data_wr							=> usr_data_wr				,
 
		usr_func_rd					      	=> usr_func_rd				,
		usr_rden					      	=> usr_rden					,
		usr_data_rd					      	=> HBM_usr_dto				,

        HBM_almost_full_data_manager_v2     => HBM_almost_full          ,

        HBM_RESETn                          => HBM_RESETn               ,               
		HBM_clk_ref							=> HBM_clk_ref				,-- 100 MHz
  		Data_clk							=> Clock_HBM				,-- 100 MHz
  		ctrl_clk							=> Clock_HBM				,-- 100 MHz	
		HBM_memory_ready					=> HBM_memory_ready			,
		resetp_counter_i                    => resetp_counter_i			, 

		--Fragment from the SlinkRocket
	    User_rstn_clock_buffer              => User_rstn_clock_buffer,
		clock_HBM_AXI						=> clock_HBM_AXI,
		
		SRx_rcv_in							=> SRx_rcv_in				,
		SRx_rcv_out							=> SRx_rcv_out			,
		
	    TCP_clock_resetn                    => TCP_clock_resetn,
		TCP_clock							=> TCP_clock,
		resetp_counter_timer_resync         => resetp_counter_timer_resync,

		DAQx_TCP_ack_rcv				 	=> DAQx_TCP_ack_rcv	,
		DAQx_TCP_Ack_present				=> DAQx_TCP_Ack_present,
		DAQx_rd_TCP_val_rcvo				=> DAQx_rd_TCP_val_rcvo,
		
		--Packet ready to be read from the Ethernet logic
        DAQx_eth_packet_to_be_sent          => DAQx_eth_packet_to_be_sent  ,
		DAQx_eth_gnt_access					=> DAQx_eth_gnt_access,
		DAQx_eth_ack_packet					=> DAQx_eth_ack_packet			 -- pulse to read CS and release the place
			
					 
  );	
	 
HBM_clock_buffer				<= clock_HBM_AXI;

	
usr_dto			<= HBM_usr_dto;--or 
	
end behavioral;
