------------------------------------------------------
-- BIFI interface (DDR2  BIgFIfo)
--
--  Ver 1.00
--
-- Dominique Gigi September 2015
------------------------------------------------------
--  DDR 3 data burst 
--  Add access PCI + arbiter
-- 
--  we use a 28 Bit addressing (ROW:15 ; Column:10 ; Bank:3)
--
--  Modify the burst request to MEM to 2 request if the request is a burst going to the TCP stream bundary
--
--
--
------------------------------------------------------
LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.interface.all;


entity read_MEM_for_eth is 
generic (simulation                    : boolean := false);-- in simulation we do not shift bit to be aligned for the ETH packet format
 port (
		resetn_eth						: in std_logic;
		clock_eth						: in std_logic;
		--request from TCp block--------------------	Works on Clock_ETH
		req_rd_blk						: in std_logic; -- a pulse
		Add_HBM_High					: in std_logic_vector(4 downto 0); -- HBM high address to select the MCx
		Add_request						: in std_logic_vector(27 downto 0); -- is in Byte count
		Size_request					: in std_logic_vector(15 downto 0); -- is in 128-bit word count (only 9 to 0 are used  MAX 9Kbytes for Ethernet)
		Packet_ready					: out std_logic; -- request only started (not yet finish)
	
		-- return values to TCP logic--------------------	Works on Clock_ETH
		eth_data_CS16					: out std_logic_vector(15 downto 0);
		eth_ack_packet					: in std_logic;	-- pulse to read CS and release the place
		eth_rd_data						: in std_logic;
		eth_data_o						: out std_logic_vector(511 downto 0);
		eth_data_ena_o					: out std_logic_vector(3 downto 0);
		eth_last_dt_o					: out std_logic; 
		
		resetn_axi						: in std_logic;
		clock_axi						: in std_logic;
		resetn_counters					: in std_logic;	
		-- interface to HBM--------------------	 Works on CLOCK_AXI
		-- output block to HBM
  		read_req						: out std_logic;
  		read_ack						: in std_logic;
		RdID							: out std_logic_vector(5 downto 0);
		H_ADD_rd						: out std_logic_vector(4 downto 0);
  		address_rd						: out std_logic_vector(27 downto 0);
  		rdwd_length						: out std_logic_vector(3 downto 0); -- indicates the number of words (256 bits) -1
		readout_ready					: out std_logic;
  		data_0							: in std_logic_vector(255 downto 0);
  		last_data_0						: in std_logic;
		rd_valid_data					: in std_logic;
		RdID_return						: in std_logic_vector(5 downto 0);
  		Rd_error						: in std_logic_vector(3 downto 0);
  		
  		trigger_o                       : out std_logic
 );
 end read_MEM_for_eth;


--******************************** ARCHITECTURE **************************************************
architecture behavioral of read_MEM_for_eth is

component read_block_HBM is
-- This file works on Clock_AXI
	port
	(
		resetn							: in std_logic;
		clock_axi						: in std_logic;
		
		resetn_counters					: in std_logic;
		-- input/output signals from/to ETH 
		blk_len							: in std_logic_vector(15 downto 0);-- number of 128 bit words
		-- bit 4  selects the Stack
		-- b 3..0 selects the peusdo mem  
		req_H_add_rd					: in std_logic_vector(4 downto 0);
		req_address_rd					: in std_logic_vector(27 downto 0);
		req_rd_blk						: in std_logic;
		data_rd							: out std_logic_vector(511 downto 0);
		data_ena						: out std_logic_vector(3 downto 0);
		last_data_rd					: out std_logic;
		val_data_block					: out std_logic;
		access_read_idle				: out std_logic;
		-- interface to HBM 
  		read_req						: out std_logic;
  		read_ack						: in std_logic;
		RdID							: out std_logic_vector(5 downto 0);
		H_ADD_rd						: out std_logic_vector(4 downto 0);
  		address_rd						: out std_logic_vector(27 downto 0);
  		rdwd_length						: out std_logic_vector(3 downto 0); -- indicates the number of words (256 bits) -1
		readout_ready					: out std_logic;
  		data_0							: in std_logic_vector(255 downto 0);
  		last_data_0						: in std_logic;
		rd_valid_data					: in std_logic;
		RdID_return						: in std_logic_vector(5 downto 0);
  		Rd_error						: in std_logic_vector(3 downto 0)
	 );
end component;

signal HBM_data						: std_logic_vector(511 downto 0);
signal HBM_data_ena					: std_logic_vector(3 downto 0);
signal HBM_data_last				: std_logic;
signal HBM_data_val					: std_logic;
signal HBM_read_idle				: std_logic;

signal dt_to_Eth_FIFO				: std_logic_vector(511 downto 0); 
attribute DONT_TOUCH : string;
attribute DONT_TOUCH of dt_to_Eth_FIFO    : signal is "TRUE";
signal dt_to_Eth_FIFO_reg			: std_logic_vector(511 downto 0);
signal dt_ena_to_Eth_FIFO			: std_logic_vector(3 downto 0);
signal dt_ena_to_Eth_FIFO_reg		: std_logic_vector(3 downto 0);
signal wr_dt_to_fifo_out			: std_logic;
signal enable_first_word			: std_logic;

signal lst_wd_to_ETH_reg			: std_logic;
signal lst_wd_to_ETH				: std_logic;
signal wr_dt_to_fifo_out_reg		: std_logic;

attribute DONT_TOUCH of dt_to_Eth_FIFO_reg    : signal is "TRUE";
attribute DONT_TOUCH of dt_ena_to_Eth_FIFO_reg    : signal is "TRUE";
attribute DONT_TOUCH of lst_wd_to_ETH_reg    : signal is "TRUE";
attribute DONT_TOUCH of wr_dt_to_fifo_out_reg    : signal is "TRUE";
 
component CS_Compute is
	Port ( 
		aclrp		: IN std_logic;
		clock		: IN std_logic;
		clken		: IN std_logic  := '1';
		last_word	: in std_logic;
		data		: IN std_logic_vector (511 DOWNTO 0);
		data_ena	: in std_logic_vector(3 downto 0);
		
		result		: OUT std_logic_vector (15 DOWNTO 0);
		CS_done		: out std_logic
);
end component;

signal CS_result		: std_logic_vector(15 downto 0);
signal CS_done			: std_logic;

COMPONENT FIFO_DC_517x2048_FWFT
  PORT (
    wr_rst         	: IN STD_LOGIC;
    wr_clk        	: IN STD_LOGIC;
    rd_rst         	: IN STD_LOGIC;
    rd_clk        	: IN STD_LOGIC;
    din           	: IN STD_LOGIC_VECTOR(516 DOWNTO 0);
    wr_en         	: IN STD_LOGIC;
    rd_en         	: IN STD_LOGIC;
    dout          	: OUT STD_LOGIC_VECTOR(516 DOWNTO 0);
    full          	: OUT STD_LOGIC;
    empty         	: OUT STD_LOGIC;
    rd_data_count 	: OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
    wr_data_count 	: OUT STD_LOGIC_VECTOR(10 DOWNTO 0) 
  );
END COMPONENT;

COMPONENT FIFO_eth_CS
  PORT (
    wr_rst         	: IN STD_LOGIC;
    wr_clk      	: IN STD_LOGIC;
    rd_rst         	: IN STD_LOGIC;
    rd_clk      	: IN STD_LOGIC;
    din         	: IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    wr_en       	: IN STD_LOGIC;
    rd_en       	: IN STD_LOGIC;
    dout        	: OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    full        	: OUT STD_LOGIC;
    empty       	: OUT STD_LOGIC 
  );
END COMPONENT;

COMPONENT FIFO_dt_ready_UR
  PORT (
    rst         	: IN STD_LOGIC;
    wr_clk        	: IN STD_LOGIC;
    rd_clk        	: IN STD_LOGIC;
    din           	: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    wr_en         	: IN STD_LOGIC;
    rd_en         	: IN STD_LOGIC;
    dout          	: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    full          	: OUT STD_LOGIC;
    empty         	: OUT STD_LOGIC;
    wr_data_count 	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END COMPONENT;

signal Packet_ready_n					: std_logic;
signal resetp_axi    					: std_logic;
signal resetp_eth    					: std_logic;
signal CS_ready_full					: std_logic;
signal Packets_ready_full				: std_logic;
signal dummy							: std_logic;

COMPONENT resync_v4 is
	port (
		aresetn				: in std_logic;
		clocki			    : in std_logic;	
		input				: in std_logic;
		clocko			    : in std_logic;
		output			    : out std_logic
		);
end COMPONENT;
 
COMPONENT Check_read_HBM is
	port
	(
		resetn_axi        : in std_logic;
		clock_axi         : in std_logic;
						   
		req_read          : in std_logic;
		word_ena	      : in std_logic;
		word              : in std_logic_vector(255 downto 0);
		check_oups        : out std_logic
	 );
end COMPONENT;

signal req_rd_blk_resync			: std_logic; 
signal Add_request_Latch			: std_logic_vector(27 downto 0);  
signal Size_request_Latch			: std_logic_vector(15 downto 0);  
signal Packet_ready_sync			: std_logic;  

signal trigger                      : std_logic;
signal trigger_resync                : std_logic;

attribute mark_debug : string; 
--attribute mark_debug of HBM_data			            : signal is "true"; 
--attribute mark_debug of HBM_data_ena			        : signal is "true"; 
--attribute mark_debug of HBM_data_last			        : signal is "true"; 
--attribute mark_debug of HBM_data_val			        : signal is "true"; 
--attribute mark_debug of req_rd_blk_resync			    : signal is "true"; 

--attribute mark_debug of Packet_ready_n			        : signal is "true"; 
--attribute mark_debug of eth_rd_data			            : signal is "true"; 
--attribute mark_debug of eth_data_o			            : signal is "true"; 
--attribute mark_debug of eth_data_ena_o			        : signal is "true"; 
--attribute mark_debug of eth_last_dt_o			        : signal is "true"; 

--attribute mark_debug of trigger        			        : signal is "true"; 
 
--******************************************************************************
--******************************------------------******************************
--***************************<<<     BEGINNING    >>>***************************
--******************************------------------******************************
--******************************************************************************
begin

resetp_axi	<= not(resetn_axi);
resetp_eth	<= not(resetn_eth);

-- transfer  signal from clock_eth   to    clock_axi
resync_request_i1:resync_v4  
	port map(
		aresetn				=> resetn_eth,
		clocki			    => clock_eth,
		input				=> req_rd_blk,
		clocko			    => clock_axi,
		output			    => req_rd_blk_resync
		);
		
process(clock_axi)
begin
	if rising_edge(clock_axi) then
		-- values will stable when req_rd_blk_resync will use on the clock_axi domain
		if req_rd_blk_resync = '1' then
			Add_request_Latch    <= Add_request; 
			Size_request_Latch   <= Size_request; 
		end if;
	end if;
end process;
 
------------------------------------------------------

read_HBM_i1:read_block_HBM  
	port map
	(
		resetn							=> resetn_axi,
		clock_axi						=> clock_axi,
										 
		resetn_counters					=> resetn_counters,
		-- input/output signals from/to ETH 
		blk_len							=> Size_request_Latch,
		-- bit 4  selects the Stack
		-- b 3..0 selects the peusdo mem 
		req_H_add_rd					=> Add_HBM_High ,
		req_address_rd					=> Add_request_Latch(27 downto 0),
		req_rd_blk						=> req_rd_blk_resync,
		data_rd							=> HBM_data,
		data_ena						=> HBM_data_ena,
		last_data_rd					=> HBM_data_last,
		val_data_block					=> HBM_data_val,
		access_read_idle				=> HBM_read_idle,
		-- interface to HBM 
  		read_req						=> read_req		,
  		read_ack						=> read_ack		,
		RdID							=> RdID			,
		H_ADD_rd						=> H_ADD_rd		,
  		address_rd						=> address_rd		,
  		rdwd_length						=> rdwd_length		,-- indicates the number of words (256 bits) -1
		readout_ready					=> readout_ready	,
  		data_0							=> data_0			,
  		last_data_0						=> last_data_0		,
		rd_valid_data					=> rd_valid_data	,
		RdID_return						=> RdID_return		,
  		Rd_error						=> Rd_error		
	 );
	 
-- compute the Checksum for Ethernet Header	 
CS_compute_i1:CS_Compute  
	Port map( 
		aclrp							=> req_rd_blk_resync,--reset teh Checksum when we request block
		clock							=> clock_axi,
		clken							=> HBM_data_val, 
		last_word						=> HBM_data_last, 
		data							=> HBM_data,--128b word is reset when not enabled (for the last word
		data_ena						=> HBM_data_ena, 
							 
		result							=> CS_result,--: OUT std_logic_vector (15 DOWNTO 0);
		CS_done							=> CS_done --: out std_logic
);
 
debug_i1:Check_read_HBM 
	port map
	(
		resetn_axi        => resetn_axi,
		clock_axi         => clock_axi,
		req_read          => '0' ,
		word_ena	      => rd_valid_data,
		word              => data_0,
		check_oups        => trigger
	 ); 
 
--process(resetn_axi,clock_axi)
--begin
--    if resetn_axi = '0' then
--        trigger     <= '0';
--    elsif rising_edge(clock_axi) then 
--        if HBM_data(31 downto 0) = x"C1C1C1C1" and HBM_data(63 downto 32) = x"2B000000" then
--            trigger     <= '1';
--        end if;
--    end if;
--end process;


resync_sig_i2:resync_v4
port map(
	aresetn			 => '1',
	clocki			 => clock_axi, 
	input			 => trigger,
	clocko			 => clock_eth,  
	output			 => trigger_resync 
	);
	
trigger_o   <= trigger;
--*******************************************************************************************************************
--**********************  internal FIFO to resync HBM clock to ETH read clock

-- reorganize data for MAC IP
--
--		data to FIFO 		|  		DATA from HBM
--									Fisrt data					Following data
--    	127..000				xxxxxxxxxxxxx xxxxxxxxxxxx   ( 47..  0)' & (255..176)'
--	 	255..128				xxxxxxxxxxxxx xxxxxxxxxxxx   (175..128)' & (383..304)'
--	 	383..256				xxxxxxxxxxxxx xxxxxxxxxxxx   (303..256)' & (511..432)'
--	 	511..384				xxxxxxxxxxxxxxx (127.. 48)   (431..384)' & ...........
-- this is done in this order because when ETH/IP/TCP header are filled, is remained 80 bit on the top (127..48) and then we have to go to next word
--			()'are registered values


process(clock_axi)
begin
	if rising_edge(clock_axi) then

		wr_dt_to_fifo_out 					<= HBM_data_val;
		
		-- shift by 1 clock the data (UR is 1 clock latency when read)
		if HBM_data_val = '1' then
			dt_to_Eth_FIFO(511 downto 464)	<= HBM_data(431 downto 384);
			
			dt_to_Eth_FIFO(383 downto 336)	<= HBM_data(303 downto 256);
			dt_to_Eth_FIFO(335 downto 256)	<= HBM_data(511 downto 432);
			
			dt_to_Eth_FIFO(255 downto 208)	<= HBM_data(175 downto 128);
			dt_to_Eth_FIFO(207 downto 128)	<= HBM_data(383 downto 304);
			
			dt_to_Eth_FIFO(127 downto 080)	<= HBM_data(047 downto 000);
			dt_to_Eth_FIFO(079 downto 000)	<= HBM_data(255 downto 176);
		end if;	
		
		-- shift the enable by one clock  (data pipe by one clock in this process)
		if req_rd_blk_resync = '1' then
			dt_ena_to_Eth_FIFO			    <= "0000";
			lst_wd_to_ETH					<= '0';
		elsif HBM_data_val = '1' then
			dt_ena_to_Eth_FIFO			    <= HBM_data_ena;
			lst_wd_to_ETH					<= HBM_data_last;
		end if;
		
	end if;
end process;


--enable the first 80 bit for the MAC DATA alignment 
--(see Transfer packet, the first 80 bit finish the word after TCP header)
process(resetn_axi,clock_axi)
begin
	if resetn_axi = '0'  then
		enable_first_word <= '0';
	elsif rising_edge(clock_axi) then
		if req_rd_blk_resync = '1' then
			enable_first_word <= '1';
		elsif HBM_data_val = '1' then
			enable_first_word <= '0';
		end if;
	end if;
end process;

process(clock_axi)
begin
    if rising_edge(clock_axi) then
        --
        if not(simulation) then
            -- re-organize the bits to be directly or'ed  with the TCP bus in the transfer file (take in account ETH-IP -TCP headers
            -- It is why first word will used only the 80 first bit from the packet
            dt_to_Eth_FIFO_reg(511 downto 464)	<= dt_to_Eth_FIFO(511 downto 464); -- higher bit will go to D_register on MAC IP
            dt_to_Eth_FIFO_reg(463 downto 384)	<= HBM_data(127 downto 48); 
            dt_to_Eth_FIFO_reg(383 downto 000)	<= dt_to_Eth_FIFO(383 downto 000); -- lower bit will got to A_register on MAC IP
            
        ----------------------------------------------------------------simulation ---------------------------------------------------------------
        else -- for simulation we do not shift bits
             dt_to_Eth_FIFO_reg(511 downto 000)	<= HBM_data(511 downto 000);  
        
        end if;
        
        -- manage the enable of 128b words
        dt_ena_to_Eth_FIFO_reg  <= "0000"; 
        --the first word only upper 128b    other words according the data rad from HBM
        if not(simulation) then
            if  enable_first_word = '1'         or  dt_ena_to_Eth_FIFO(3) = '1' then
                dt_ena_to_Eth_FIFO_reg(3)	<= '1';
            end if;
            if                                      dt_ena_to_Eth_FIFO(2) = '1' then 
                dt_ena_to_Eth_FIFO_reg(2)	<= '1';
            end if;
            if                                      dt_ena_to_Eth_FIFO(1) = '1' then 
                dt_ena_to_Eth_FIFO_reg(1)	<= '1' ;
            end if;
            if                                      dt_ena_to_Eth_FIFO(0) = '1' then  
                dt_ena_to_Eth_FIFO_reg(0)	<= '1' ;
            end if;
         else ----------------------------------------------------------------simulation ----------------------------------------------------------------
            dt_ena_to_Eth_FIFO_reg          <= HBM_data_ena;
         end if;
         
        lst_wd_to_ETH_reg		<= '0';	
        if wr_dt_to_fifo_out = '1' and lst_wd_to_ETH = '1' then
            lst_wd_to_ETH_reg		<= '1';	
        end if;		
    end if;
end process;

process(resetn_axi,clock_axi)
begin
    if resetn_axi = '0' then
    
    elsif rising_edge(clock_axi) then
        wr_dt_to_fifo_out_reg	<= '0';
--			  All words                      Last word 	     
        if not(simulation) then
            if HBM_data_val = '1' or (wr_dt_to_fifo_out = '1' and lst_wd_to_ETH = '1') then
                wr_dt_to_fifo_out_reg	<= '1';
            end if;
        else----------------------------------------------------------------simulation ----------------------------------------------------------------
            if HBM_data_val = '1' then
                wr_dt_to_fifo_out_reg	<= '1';
            end if;
        end if;
    end if;
end process; 

--record the packet from TCP stream
-- 2048 words  max 14 packets
buffer_data_i1:FIFO_DC_517x2048_FWFT
  PORT MAP(
    wr_rst         		=> resetp_axi     , 
    wr_clk        		=> clock_axi           , 
    din(511 downto 000)	=> dt_to_Eth_FIFO_reg , 
    din(515 downto 512)	=> dt_ena_to_Eth_FIFO_reg  , 
    din(516)			=> lst_wd_to_ETH_reg  , 
    wr_en         		=> wr_dt_to_fifo_out_reg, 
--    full          		=>                 , 
	
	rd_rst			    => resetp_eth,								 
    rd_clk        		=> clock_eth           , 
    rd_en         		=> eth_rd_data         , 
    dout(511 downto 000)=> eth_data_o          , 
    dout(515 downto 512)=> eth_data_ena_o      , 
    dout(516)			=> eth_last_dt_o  
    -- empty         		=>  			   ,--: OUT STD_LOGIC;
	
    -- rd_data_count 		=>                 ,--: OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
    -- wr_data_count 		=>                 ,--: OUT STD_LOGIC_VECTOR(10 DOWNTO 0); 
  );
  
--record the Checksum when done
--  16 words  
buffer_checksum_i1:FIFO_eth_CS
  PORT MAP(
    wr_rst        		=> resetp_axi, 
    wr_clk      		=> clock_axi      , 
    din         		=> CS_result      , 
    wr_en       		=> CS_done        , 
    full        		=> CS_ready_full  , 
		
    rd_rst	            => resetp_eth,							    
    rd_clk      		=> clock_eth      , 
    rd_en       		=> eth_ack_packet , 
    dout        		=> eth_data_CS16  
    -- empty       		=> ,--: OUT STD_LOGIC; 
  );
 
-- record 1 bit per packet ready
-- 16 places
Buffer_packet_ready_i1:FIFO_dt_ready_UR
  PORT MAP(
    rst         		=> resetp_axi   , 
    wr_clk        		=> clock_axi         , 
    din(0)         		=> '1'               , 
    wr_en         		=> CS_done           , 
    full          		=> Packets_ready_full, 
						   
    rd_clk        		=> clock_eth         , 
    rd_en         		=> eth_ack_packet    , 
    dout(0)        		=> dummy             , 
    empty         		=> Packet_ready_n  
    -- wr_data_count 		=> ,--: OUT STD_LOGIC_VECTOR(9 DOWNTO 0); 
  );
 
Packet_ready	<= not(Packet_ready_n);

end behavioral;