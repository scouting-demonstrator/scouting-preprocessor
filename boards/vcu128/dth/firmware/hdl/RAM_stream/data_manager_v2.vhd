----------------------------------------------------------------------------------
-- Company:
-- Engineer: D. Gigi
--
-- Create Date: 19.10.2018 15:25:16
-- Design Name:
-- Module Name: data_manager_v2 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- modified to send out HBM almost signal (R. Ardino, January 2023)
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.interface.all;
use work.address_table.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_manager_v2 is
   Port (
		usr_clk								: in std_logic;
		usr_rst_n							: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0);
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0);

		usr_func_rd					      	: in std_logic_vector(16383 downto 0);
		usr_rden					      	: in std_logic;
		usr_data_rd					      	: out std_logic_vector(63 downto 0);

		HBM_almost_full_data_manager_v2     : out HBM_bus_1_bit; --R

        HBM_RESETn                          : in std_logic;
		HBM_clk_ref							: in std_logic; -- 100 MHz
  		Data_clk							: in std_logic; -- 100 MHz
  		ctrl_clk							: in std_logic; -- 100 MHz
		HBM_memory_ready					: out std_logic;
		resetp_counter_i                    : in std_logic;

		--Fragment from the SlinkRocket
	    User_rstn_clock_buffer              : in std_logic;
		clock_HBM_AXI						: out std_logic;

		SRx_rcv_in							: in SR_rcv_data_hlf;
		SRx_rcv_out							: out SR_rcv_ctrl_hlf;

	    TCP_clock_resetn                    : in std_logic;
		TCP_clock							: in std_logic;
		resetp_counter_timer_resync         : in std_logic;

		DAQx_TCP_ack_rcv				 	: in TCP_ack_rcvo_type;
		DAQx_TCP_Ack_present				: in std_logic_vector(number_of_HBM_port-1 downto 0);
		DAQx_rd_TCP_val_rcvo				: out std_logic_vector(number_of_HBM_port-1 downto 0);

		--Packet ready to be read from the Ethernet logic
        DAQx_eth_packet_to_be_sent          : out HBM_to_TCP_block_pipe;
		DAQx_eth_gnt_access					: in std_logic_vector(number_of_HBM_port-1 downto 0); -- Give the access to DAQ unit
		DAQx_eth_ack_packet					: in std_logic_vector(number_of_HBM_port-1 downto 0) -- pulse to read CS and release the place


  );
end data_manager_v2;

--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
architecture Behavioral of data_manager_v2 is

signal usr_data_rd_reg          : std_logic_vector(63 downto 0);
type TCP_read_out_type	is array (0 to number_of_HBM_port-1) of std_logic_vector(63 downto 0);
signal TCP_read_out 	         : TCP_read_out_type;


component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic;
	Resetn_sync			: out std_logic;
	Resetp_sync			: out std_logic
	);
end component;

component resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic;
	Resetp_sync			: out std_logic
	);
end component;

component resync_v4
port (
	aresetn			: in std_logic;
	clocki			: in std_logic;
	clocko			: in std_logic;
	input			: in std_logic;
	output			: out std_logic
	);
end component;

signal Clock_AXI						: std_logic;

component Write_block_HBM is
	generic (HBM_MC_High_address            : integer);
	port
	(
		resetn_axi_clk					: in std_logic;
		clock_axi						: in std_logic;

		resetn_counters					: in std_logic;
		-- input block
		blk_len							: in std_logic_vector(11 downto 0);-- number of 128 bit words
		HD_space						: in std_logic_vector(2 downto 0); -- 001   data words  010  Fragment Header  100 Orbit Header block
		req_wr_blk						: in std_logic;
		data_in							: in std_logic_vector(255 downto 0);
		rd_data_block					: out std_logic;
		read_block_done					: out std_logic;
		access_write_idle				: out std_logic;

		-- output block to HBM
  		HBM_write_req					: out HBM_write_req_record;
		HBM_write_ack					: in HBM_write_ack_record;

		HBM_almost_full                 : in std_logic;

		SND_NEW							: out std_logic_vector(31 downto 0);
		SND_NEW_update					: out std_logic
	 );
end component;

signal HBM_write_req					: HBM_write_req_type;
signal HBM_write_ack					: HBM_write_ack_type;

signal SND_NEW_reg						: HBM_bus_32_bit;
signal SND_NEW_cnt						: HBM_bus_32_bit;
signal SND_NEW_update					: HBM_bus_1_bit;
signal SND_NEW_update_reg				: HBM_bus_1_bit;
signal ltch_snd_new						: HBM_bus_1_bit;


component stream_buffer_HBM is
  Port (
		usr_rst_n						: in std_logic;
  		usr_clk							: in std_logic;
		usr_func_wr						: in std_logic_vector(16383 downto 0);
		usr_wen							: in std_logic;
		usr_data_wr						: in std_logic_vector(63 downto 0);
		usr_func_rd					    : in std_logic_vector(16383 downto 0);
		usr_rden					    : in std_logic;
		usr_data_rd					    : out std_logic_vector(63 downto 0);

		HBM_RESETn						: in std_logic;
		HBM_clk_ref						: in std_logic; -- 100 MHz
  		Data_clk						: in std_logic; -- 100 MHz
  		ctrl_clk						: in std_logic; -- 100 MHz
 		-- write access bus
		HBM_write_in_mem				: in HBM_write_req_type;
		HBM_write_out_mem				: out HBM_write_ack_type;
		-- read access bus
		HBM_read_in_mem					: in HBM_read_req_type;
		HBM_read_out_mem				: out HBM_read_ack_type;

		clock_interface					: out std_logic;
		-- control/monitoring access bus

		HBM_memory_ready				: out std_logic;
		Catastrophic_temp         		: OUT STD_LOGIC;
		HBM_temperature                	: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);

		HBM_ctrl_awaddr  				: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		HBM_ctrl_awprot  				: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		HBM_ctrl_awvalid 				: IN STD_LOGIC;
		HBM_ctrl_awready 				: OUT STD_LOGIC;
		HBM_ctrl_wdata   				: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		HBM_ctrl_wstrb   				: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		HBM_ctrl_wvalid  				: IN STD_LOGIC;
		HBM_ctrl_wready  				: OUT STD_LOGIC;
		HBM_ctrl_bresp   				: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
		HBM_ctrl_bvalid  				: OUT STD_LOGIC;
		HBM_ctrl_bready  				: IN STD_LOGIC;
		HBM_ctrl_araddr  				: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		HBM_ctrl_arprot  				: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		HBM_ctrl_arvalid 				: IN STD_LOGIC;
		HBM_ctrl_arready 				: OUT STD_LOGIC;
		HBM_ctrl_rdata   				: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		HBM_ctrl_rresp   				: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
		HBM_ctrl_rvalid  				: OUT STD_LOGIC;
		HBM_ctrl_rready  				: IN STD_LOGIC
  	);
end component;

signal HBM_memory_ready_cell			: std_logic;
signal Catastrophic_temp				: STD_LOGIC;
signal HBM_temperature  				: STD_LOGIC_VECTOR(6 DOWNTO 0);
signal HBM_User_data	  				: STD_LOGIC_VECTOR(63 DOWNTO 0);

component TCP_mem_block is
	generic (TCP_Address_Offset				: integer;
	         HBM_MC_High_address            : integer);
	Port (
	-- control local interface
		usr_clk								: in std_logic;
		rst_usr_clk_n						: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0);
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0);

		usr_func_rd							: in std_logic_vector(16383 downto 0);
		usr_rden							: in std_logic;
		usr_dto								: out std_logic_vector(63 downto 0);

		TCP_resetn							: in std_logic;
		TCP_clock							: in std_logic;

		TCP_ack_rcv				 			: in TCP_ack_rcvo_record;
		TCP_Ack_present						: in std_logic;
		rd_TCP_val_rcvo						: out std_logic;

		HBM_TCP_block						: out HBM_to_TCP_block_record;
		GNT_access_to_ETH					: in std_logic:= '1';
		TCP_request_took					: in std_logic;

		resetp_FED_sync_axi					: out std_logic;
		resetn_axi							: in std_logic;
		clock_axi							: in std_logic;
		resetn_counters						: in std_logic;
		resetp_Stat_rsync					: in std_logic;

		SND_NEW								: in std_logic_vector(31 downto 0);
		SND_NEW_update						: in std_logic;

		-- interface to HBM
  		HBM_read_req						: out HBM_read_req_record;
		HBM_read_ack						: in HBM_read_ack_record;

  		HBM_almost_full						: out std_logic;
  		HBM_pre_almost_full                 : out std_logic; --R

		counter_ena_read_block              : out std_logic
 );
end component;


signal HBM_read_req							: HBM_read_req_type;
signal HBM_read_ack							: HBM_read_ack_type;
signal HBM_stream_almost_full               : HBM_bus_1_bit;
signal HBM_stream_pre_almost_full           : HBM_bus_1_bit;

signal sync_valid_p									: std_logic;

--attribute mark_debug of read_memory_block				: signal is "true";

signal resetp_FED_rsync								: std_logic_vector(number_of_HBM_port-1 downto 0);
signal resetn_FED_rsync								: std_logic_vector(number_of_HBM_port-1 downto 0);
signal resetp_Stat_rsync							: std_logic_vector(number_of_HBM_port-1 downto 0);
signal resetn_counter_i 							: std_logic;
signal resetp_counter_resync                        : std_logic;
signal counter_ena_read_block                       : std_logic_vector(number_of_HBM_port-1 downto 0);


signal counter_A                                    : std_logic_vector(63 downto 0);
signal counter_B                                    : std_logic_vector(63 downto 0);
signal counter_C                                    : std_logic_vector(63 downto 0);
signal counter_D                                    : std_logic_vector(63 downto 0);
signal counter_free                                 : std_logic_vector(63 downto 0);

signal latch_counter_A                              : std_logic_vector(63 downto 0);
signal latch_counter_B                              : std_logic_vector(63 downto 0);
signal latch_counter_C                              : std_logic_vector(63 downto 0);
signal latch_counter_D                              : std_logic_vector(63 downto 0);
signal latch_counter_free                           : std_logic_vector(63 downto 0);

signal latch_counter_timer							: std_logic;
signal latch_counter_timer_resync					: std_logic;



attribute mark_debug                                        : string;
--attribute mark_debug of resetn_FED_rsync	            : signal is "true";
--attribute mark_debug of HBM_TCP_block			            : signal is "true";
--attribute mark_debug of ETH_request			            : signal is "true";

--#########################################################
--##############    CODE    START    HERE    ##############
--#########################################################
begin

resetn_counter_i    <= not(resetp_counter_i);

resync_reset_i4:resetp_resync
	port map(
		aresetp			=> resetp_counter_i,
		clock			=> Clock_AXI,
		Resetp_sync		=> resetp_counter_resync
		);

Reset_loop:for I in 0 to number_of_HBM_port-1 GENERATE
--Reset_loop:

	resetn_FED_rsync(I) <= not(resetp_FED_rsync(I));

	-- reset stat counters when link FED is closed  or with command
	process(Clock_AXI)
	begin
	   if rising_edge(Clock_AXI) then
	       resetp_Stat_rsync(I) <= '0';
	       if resetp_FED_rsync(I) = '1' or resetp_counter_resync = '1' then
	           resetp_Stat_rsync(I) <= '1';
	       end if;
	   end if;
	end process;

end generate Reset_loop;
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@         Interface for DATA IN                        @@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

 HBM_almost_full_data_manager_v2 <= HBM_stream_pre_almost_full; --R
 -- May lead to hbm write stop
 -- HBM_almost_full_data_manager_v2 <= HBM_stream_almost_full; --R

WRITE_TO_HBM_LOOP:for I in 0 to number_of_HBM_port-1 GENERATE
Write_data_to_HBM:Write_block_HBM
	generic map (HBM_MC_High_address   => HBM_S0(I))
	port map	(
		resetn_axi_clk					=> resetn_FED_rsync(I),
		clock_axi						=> Clock_AXI,

		resetn_counters					=> resetn_counter_i,--: in std_logic;
		-- input block
		blk_len(11 downto 00)			=> SRx_rcv_in(I).size_for_HBM_mem,      -- number of 128 bit words
		HD_space						=> SRx_rcv_in(I).HD_space_in_HBM, -- 001   data words  010  Fragment Header  100 Orbit Header block
		req_wr_blk						=> SRx_rcv_in(I).ready_for_HBM_mem,--: in std_logic;
		data_in							=> SRx_rcv_in(I).data_for_HBM_mem,
		rd_data_block					=> SRx_rcv_out(I).read_for_HBM_mem,
		read_block_done					=> SRx_rcv_out(I).Block_read_done_for_HBM,
		--access_write_idle				=> ,--: out std_logic;

		-- output block to HBM
		HBM_write_req					=> HBM_write_req(I),
		HBM_write_ack					=> HBM_write_ack(I),

		HBM_almost_full                 => HBM_stream_almost_full(I),

		SND_NEW							=> SND_NEW_cnt(I),
		SND_NEW_update					=> SND_NEW_update(I)
	 );


-- latch new counter to send to TCP logic STREAM A
	process(resetp_FED_rsync(I),Clock_AXI)
	begin
		if resetp_FED_rsync(I)  = '1' then
			SND_NEW_reg(I) <= (others => '0');
		elsif rising_edge(Clock_AXI) then
			SND_NEW_update_reg(I)		<= '0';
			if SND_NEW_update(I) = '1' then
				SND_NEW_reg(I) 			<= SND_NEW_cnt(I);
				SND_NEW_update_reg(I)	<= '1';
			 end if;
		end if;
	end process;
END GENERATE WRITE_TO_HBM_LOOP;

--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--@@@@@@@@@@@@@@@@@@@@@@@@@@					 HBM                             --@@@@@@@@@@@@@@@@@@@@@@@@@@				  256 MBytes                         --@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

HBM_STREAM_i1:stream_buffer_HBM
  Port map(
		usr_rst_n						=> usr_rst_n	,
  		usr_clk							=> usr_clk		,
		usr_func_wr						=> usr_func_wr	,
		usr_wen							=> usr_wen		,
		usr_data_wr						=> usr_data_wr	,
		usr_func_rd					    => usr_func_rd	,
		usr_rden					    => usr_rden	,
		usr_data_rd					    => HBM_User_data	,

		HBM_RESETn						=> HBM_RESETn,
		HBM_clk_ref						=> HBM_clk_ref	,  -- 100 MHz
  		Data_clk						=> Data_clk	,  -- 100 MHz
  		ctrl_clk						=> ctrl_clk	,  -- 100 MHz
  		-- write access bus
		HBM_write_in_mem				=> HBM_write_req,--	: in HBM_write_req_hlf;
		HBM_write_out_mem				=> HBM_write_ack,--	: out HBM_write_ack_hlf;
		-- read access bus
		HBM_read_in_mem					=> HBM_read_req,--	: in HBM_read_req_hlf;
		HBM_read_out_mem				=> HBM_read_ack,--	: out HBM_read_ack_hlf;

		clock_interface					=> Clock_AXI        ,
			-- control/monitoring access bus

		HBM_memory_ready				=> HBM_memory_ready_cell ,
		Catastrophic_temp         		=> Catastrophic_temp,
		HBM_temperature                	=> HBM_temperature  ,

		HBM_ctrl_awaddr  				=> x"00000000"      ,
		HBM_ctrl_awprot  				=> "000"            ,
		HBM_ctrl_awvalid 				=> '0'              ,
		-- HBM_ctrl_awready 				=>              ,
		HBM_ctrl_wdata   				=> x"00000000"      ,
		HBM_ctrl_wstrb   				=> "0000"           ,
		HBM_ctrl_wvalid  				=> '0'              ,
		-- HBM_ctrl_wready  				=>              ,
		-- HBM_ctrl_bresp   				=>              ,
		-- HBM_ctrl_bvalid  				=>              ,
		HBM_ctrl_bready  				=> '0'              ,
		HBM_ctrl_araddr  				=> x"00000000"      ,
		HBM_ctrl_arprot  				=> "000"            ,
		HBM_ctrl_arvalid 				=> '0'              ,
		-- HBM_ctrl_arready 				=>              ,
		-- HBM_ctrl_rdata   				=>              ,
		-- HBM_ctrl_rresp   				=>              ,
		-- HBM_ctrl_rvalid  				=>              ,
		HBM_ctrl_rready  				=> '1'
  	);

clock_HBM_AXI		<= Clock_AXI;
HBM_memory_ready	<= HBM_memory_ready_cell;

--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@ 				interface for DATA OUT                @@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--

READ_FROM_HBM_LOOP_0_4:for I in 0 to number_of_HBM_port-1 GENERATE
	Readout_HBM_for_ETH_i1:TCP_mem_block
		generic map(TCP_Address_Offset				=> Offset_TCP_Stream(I),
		            HBM_MC_High_address             => HBM_S0(I)      )
		Port map(
		-- control local interface
			usr_clk								=> usr_clk                 ,
			rst_usr_clk_n						=> usr_rst_n               ,
			usr_func_wr							=> usr_func_wr	           ,
			usr_wen								=> usr_wen		           ,
			usr_data_wr							=> usr_data_wr	           ,

			usr_func_rd							=> usr_func_rd             ,
			usr_rden							=> usr_rden                ,
			usr_dto								=> TCP_read_out(I)         ,

			TCP_resetn							=> TCP_clock_resetn    		,
			TCP_clock							=> TCP_clock          		,

			TCP_ack_rcv				 			=> DAQx_TCP_ack_rcv(I)		   ,
			TCP_Ack_present						=> DAQx_TCP_Ack_present(I)	   ,
			rd_TCP_val_rcvo						=> DAQx_rd_TCP_val_rcvo(I)	   ,

			HBM_TCP_block						=> DAQx_eth_packet_to_be_sent(I),
			GNT_access_to_ETH					=> DAQx_eth_gnt_access(I)       ,
			TCP_request_took					=> DAQx_eth_ack_packet(I)       ,

			resetp_FED_sync_axi          		=> resetp_FED_rsync(I)          ,

			resetn_axi							=> User_rstn_clock_buffer               ,
			clock_axi							=> Clock_AXI               ,
			resetn_counters						=> resetn_counter_i        ,
			resetp_Stat_rsync					=> resetp_Stat_rsync(I)       ,

			SND_NEW								=> SND_NEW_reg(I)          ,
			SND_NEW_update						=> SND_NEW_update_reg(I)   ,
			-- interface to HBM  	            =>                         ,
			HBM_read_req						=> HBM_read_req(I),--: out HBM_read_req_record;
			HBM_read_ack						=> HBM_read_ack(I),--: in HBM_read_ack_record;

			HBM_almost_full						=> HBM_stream_almost_full(I),
			HBM_pre_almost_full                 => HBM_stream_pre_almost_full(I), --R

		    counter_ena_read_block              => counter_ena_read_block(I)
	 );
END GENERATE READ_FROM_HBM_LOOP_0_4;


--******************************************************************************************
--  user access read for status


process(usr_clk)
begin
	if rising_edge(usr_clk) then
		latch_counter_timer			<= '0';
		if usr_wen = '1' then
			if usr_func_wr(HBM_status) = '1' then
				latch_counter_timer 	<= usr_data_wr(30);
			end if;

		end if;
	end if;
end process;


process(usr_clk)
begin
	if rising_edge(usr_clk) then
		usr_data_rd_reg 							       	<= (others => '0');
			if 		usr_func_rd(HBM_status) = '1' then
				usr_data_rd_reg(0)			<= HBM_memory_ready_cell;
				usr_data_rd_reg(1)			<= Catastrophic_temp;
				usr_data_rd_reg(14 downto 8)<= HBM_temperature;
--				usr_data_rd_reg(15)         <= Read_blk_error;
				usr_data_rd_reg(63 downto 40)<= x"48424D";

			elsif 	usr_func_rd(measure_counter_HBM_A) = '1' then
				usr_data_rd_reg				<= latch_counter_A;
			elsif	usr_func_rd(measure_counter_HBM_free) = '1' then
				usr_data_rd_reg				<= latch_counter_free;
			end if;
	end if;
end process;

process(resetp_counter_timer_resync ,TCP_clock)
begin
    if resetp_counter_timer_resync = '1'  then
     counter_A        <= (others => '0');
	 counter_free     <= (others => '0');

    elsif rising_edge(TCP_clock) then
		counter_free	<= counter_free + '1';

		if counter_ena_read_block(0) = '1' then
			counter_A	<= counter_A + '1';
		end if;


    end if;
end process;


latch_resync:resync_v4
port map(
	aresetn			=> '1',
	clocki			=> usr_clk,
	input			=> latch_counter_timer,
	clocko			=> TCP_Clock,
	output			=> latch_counter_timer_resync
	);


process(TCP_Clock)
begin
	if rising_edge(TCP_Clock) then
		if latch_counter_timer_resync = '1' then
			latch_counter_A		  <= counter_A	;
		    latch_counter_free    <= counter_free	;
		end if;
	end if;
end process;

usr_data_rd <= usr_data_rd_reg or
               HBM_User_data or
               TCP_read_out(00) or
               TCP_read_out(01) or
               TCP_read_out(02) or
               TCP_read_out(03) or
               TCP_read_out(04) or
               TCP_read_out(05) or
               TCP_read_out(06) or
               TCP_read_out(07) or
               TCP_read_out(08) or
               TCP_read_out(09) or
               TCP_read_out(10) or
               TCP_read_out(11) or
			   TCP_read_out(12) or
			   TCP_read_out(13);


end Behavioral;
