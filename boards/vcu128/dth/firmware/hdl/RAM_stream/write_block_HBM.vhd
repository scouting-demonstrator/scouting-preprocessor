LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.interface.all;

-------------------------------------------------------------------
-- At the write side (the HBM acts as a FIFO) fragments coming 
-- from SR is recorded one after the other .
 
 
 --NB: If blk_len doesn't change during the request, we can remove the case of prepare counters 
 --and use directly the blk_len instead of latch line 192
 
--ENTITY-----------------------------------------------------------
entity Write_block_HBM is
		-- bit 4  selects the Stack
		-- b 3..0 selects the peusdo mem 
	generic (HBM_MC_High_address            : integer);
	port
	(
		resetn_axi_clk					: in std_logic;
		clock_axi						: in std_logic;
		
		resetn_counters					: in std_logic;
		-- input block    --------------------	Works on Clock_AXI
		blk_len							: in std_logic_vector(11 downto 0);-- number of 128 bit words
		HD_space						: in std_logic_vector(2 downto 0); -- 001   data words  010  Fragment Header  100 Orbit Header block
		req_wr_blk						: in std_logic;
		data_in							: in std_logic_vector(255 downto 0);
		rd_data_block					: out std_logic;
		read_block_done					: out std_logic;
		access_write_idle				: out std_logic; 
		
		-- output block to HBM	--------------------	Works on Clock_AXI
  		HBM_write_req					: out HBM_write_req_record;
		HBM_write_ack					: in HBM_write_ack_record;
				
		HBM_almost_full                 : in std_logic;
		 
		SND_NEW							: out std_logic_vector(31 downto 0);
		SND_NEW_update					: out std_logic;
		BIFI_add						: out std_logic_vector(31 downto 0);
		BIFI_add_update					: out std_logic	
	 );
end Write_block_HBM;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of Write_block_HBM is

signal Wr_OrbitB_Header				: std_logic;
signal Wr_Fragment_header			: std_logic;
signal Wr_PayLoad					: std_logic;

signal write_ack					: std_logic;	
signal write_ready					: std_logic; 
signal WrID_return					: std_logic_vector(5 downto 0);
signal Wr_burst_done				: std_logic;
  
signal PL_address_write				: std_logic_vector(27 downto 0);	-- PayLoad address
signal end_PL_wth_hlf_wd			: std_logic; -- when we end to write only 128 bit on the first write 
signal OH_address_write				: std_logic_vector(27 downto 0);	-- Orbit Header address
signal end_OH_wth_hlf_wd			: std_logic; -- when we end to write only 128 bit on the first write 
signal FH_address_write				: std_logic_vector(27 downto 0);	-- Fragment Header address
signal end_FH_wth_hlf_wd			: std_logic; -- when we end to write only 128 bit on the first write 
signal address_req					: std_logic_vector(27 downto 0);
  
signal SND_NEW_write				: std_logic_vector(31 downto 0);
signal SND_NEW_update_reg			: std_logic;
signal BIFI_update_reg				: std_logic;
signal Update_add_reg_BIFI_SND		: std_logic;


signal rd_data_block_cell			: std_logic;
signal write_req_reg				: std_logic; 
signal incr_ID                      : std_logic;
signal WID_reg						: std_logic_vector(5 downto 0);
signal Last_wr_ID                   : std_logic_vector(5 downto 0);
 
signal counter_word					: std_logic_vector(15 downto 0);
signal counter_burst				: std_logic_vector(3 downto 0);

signal odd_nb_word					: std_logic;

signal word_enable_cell				: std_logic_vector(1 downto 0);
signal word_enable_reg				: std_logic_vector(1 downto 0);
signal write_val_cell				: std_logic;
signal write_val_reg				: std_logic;

signal first_word_of_blk			: std_logic;
signal swapped_delay_128b			: std_logic; 
signal end_burst_wrd				: std_logic;
signal last_burst_wrd_cell			: std_logic;
signal last_burst_wrd_reg			: std_logic;
signal tempo_128b_word				: std_logic_vector(127 downto 0);
signal data_wrd_reg					: std_logic_vector(255 downto 0);

signal read_wrd_from_ouside			: std_logic;
 
signal Add_HBM_High_cell			: std_logic_vector(4 downto 0); 
signal End_last_access              : std_logic;
 
type HBM_write_access_type is (	Idle,
								prepare_counters,
								First_Address_req,
								data_req,
								Next_Address_req,
								last_word,
								wait_write_burst_done,
								write_burst_done
							);
signal HBM_write_access:HBM_write_access_type;
 attribute fsm_encoding : string;
-- attribute fsm_encoding of HBM_write_access : signal is "sequential";

signal wait_write_burst_done_delay  : std_logic;
 
attribute mark_debug : string; 
 
attribute mark_debug of HBM_write_access			: signal is "true";   
 
--***************************************************************** 
--*************<     CODE START HERE    >**************************
--***************************************************************** 
 begin 
 
write_ack		<= HBM_write_ack.write_ack;
write_ready		<= HBM_write_ack.write_ready;		 
WrID_return		<= HBM_write_ack.WrID_return;	
Wr_burst_done   <= HBM_write_ack.Wr_burst_done ; 

process(clock_axi,resetn_axi_clk)
begin
	if resetn_axi_clk = '0' then
		Wr_OrbitB_Header		<= '0';
		Wr_Fragment_header	<= '0';
		Wr_PayLoad			<= '0';
	elsif rising_edge(clock_axi) then
		if HBM_write_access = prepare_counters then
			Wr_OrbitB_Header	<= HD_space(2);
			Wr_Fragment_header	<= HD_space(1);
			Wr_PayLoad			<= HD_space(0);
		end if;
	end if;
end process;
 
 	 
--**********************************************************
--  state machine to write to HBM memory

HBM_wr_i1:process(clock_axi)
begin  
    if rising_edge(clock_axi) then
        if resetn_axi_clk = '0' then
	       HBM_write_access <= idle;
        else
            Case HBM_write_access is
                when idle =>
                    if 		req_wr_blk = '1' and (HBM_almost_full = '0' or  HD_space(0) = '0') then
                        HBM_write_access <= prepare_counters;
                    end if;
                    
                when prepare_counters => 
                    HBM_write_access <= First_Address_req;
                    
                -- the fisrt address req is without data trasnfer
                when First_Address_req =>
					if counter_burst = "0000" and counter_word(15 downto 1) = "000000000000000" then	 
						HBM_write_access <= last_word;
					else
						HBM_write_access <= data_req;
					end if; 
            
                when data_req =>
                    if (counter_burst = "0001" and counter_word = x"000000") 	and write_ready = '1' then
                        HBM_write_access <= last_word;
                    elsif (counter_burst = "0001") 								and write_ready = '1' then --- to be adjust
                        HBM_write_access <= Next_Address_req;
                    end if;
                    
                -- next address request is shared with data trasnfer , for efficiency
                when Next_Address_req =>
					if counter_burst = "0000" and counter_word(15 downto 1) = "000000000000000" then	 
						HBM_write_access <= last_word;
					else
						HBM_write_access <= data_req;
					end if;

                when last_word => 
                    if write_ready = '1' then
                        HBM_write_access <= wait_write_burst_done;
                    end if;
                
                when wait_write_burst_done =>
                   if Wr_burst_done = '1' and (Last_wr_ID = WrID_return) then -- Check the validation of the burst with the last WrID => Block Write is DONE
                        HBM_write_access <= write_burst_done;
                   end if; 
                   
                when write_burst_done => 
                      HBM_write_access <= idle;
                       
                when others =>
                     HBM_write_access <= idle;
                    
            end case;
        end if;
    end if;
end process;
 
-- read (ACK) block from SlinkRocket
process(clock_axi)
begin
    if rising_edge(clock_axi) then
        wait_write_burst_done_delay     <= '0'; 
        if HBM_write_access = wait_write_burst_done then
            wait_write_burst_done_delay <= '1';
        end if;
    end if;
end process;

access_write_idle	<= '1' when   HBM_write_access = idle else '0';
-- Read Data or headers                                                                                                                                 VVVV changed 21/12/2021
rd_data_block_cell	<= '1' when write_ready = '1' and (HBM_write_access = data_req or ((end_burst_wrd = '1' or HBM_write_access = last_word)  and (not(End_last_access = '1' and odd_nb_word = '0') or (HBM_write_access = Next_Address_req) )) )else '0';
rd_data_block		<= rd_data_block_cell;

-- Registers this signal to optimize timing, end_xx_wth_hlf_wd is from the precedent access
process(clock_axi)
begin
    if rising_edge(clock_axi) then
        if      Wr_OrbitB_Header	= '1' then
		  End_last_access <= end_OH_wth_hlf_wd;
		elsif	Wr_Fragment_header	= '1' then
		  End_last_access <= end_FH_wth_hlf_wd;
		elsif 	Wr_PayLoad			= '1' then
		  End_last_access <= end_PL_wth_hlf_wd;
		end if;
    end if;
end process;


-- The block of DATA Header has been read completely 
read_block_done		<= '1' when   HBM_write_access = wait_write_burst_done and wait_write_burst_done_delay = '0' else '0'; -- ack the block to be written when we enter to "wait_write_burst_done" state

--**********************************************************
--    word counter 

process(clock_axi)
begin	
	if rising_edge(clock_axi) then
		end_burst_wrd		<= '0';
		if 	HBM_write_access = data_req	and counter_burst = "0001" and write_ready = '1'  then
			end_burst_wrd	<= '1';
			-- this signal is valid if we continue with a next burst or if we send the last data of the block transfer
		end if;
	end if;
end process;


process (resetn_axi_clk , clock_axi)
	variable temp_counter_word		: std_logic_vector(15 downto 0);
	variable temp_counter_burst		: std_logic_vector(3 downto 0);
begin
	if resetn_axi_clk = '0' then
		counter_word		<= (others => '0');-- number of 256 b words 
		counter_burst		<= "1111"; 
		odd_nb_word			<= '0';
	
	elsif rising_edge(clock_axi) then
		-- if we trasnfert a odd number of 128 bit, the burst should be + 1  
		-- If we already unaligned (end_PL_wth_hlf_wd = '1') we should add 2    // previous "Write"  wrote only 128b on the last data
		-- no influence if the blk_len correspond to a entier number of 256bit
		-- ex: if len = 7 (number of 128b)    should be 4 ... 3x256b + 1 x 128b
		if  (end_PL_wth_hlf_wd = '1' and HD_space(0) = '1') or 		-- Only PL and Frag Header can be 128b written
			(end_FH_wth_hlf_wd = '1' and HD_space(1) = '1') or
			(end_OH_wth_hlf_wd = '1' and HD_space(2) = '1') then
			temp_counter_word	:= ("0000" & blk_len) + x"0002";
		else
			temp_counter_word	:= ("0000" & blk_len) + x"0001";
		end if;
 
		--counter_word  manages the number of burst of 16 x 256b words  :::::   !! counter_word is blk)len / 2
		if req_wr_blk = '1' and HBM_write_access = idle then
			counter_word(15)			<= '0';
			counter_word(14 downto 0)	<= temp_counter_word(15 downto 1);
			odd_nb_word					<= blk_len(0); 
		elsif (HBM_write_access = prepare_counters or HBM_write_access = Next_Address_req) and write_ack = '1' then
			if counter_word > x"0010" then
				counter_word	<= counter_word - x"0010"; -- - 16 256b word 
			else 
				counter_word	<= (others => '0');
			end if;
		end if;

		--counter_burst  manages the number of 256b word write in a burst (a burst is max 16 words)
		if (HBM_write_access = prepare_counters or  HBM_write_access = Next_Address_req ) and write_ack = '1' then
			if counter_word(15 downto 4) = x"000" then
				temp_counter_burst	:= counter_word(3 downto 0) - '1';
				counter_burst 		<= temp_counter_burst(3 downto 0);
			else
				counter_burst		<= "1111"; 
			end if;
		elsif ( HBM_write_access = data_req or end_burst_wrd = '1' ) and write_ready = '1' then
				counter_burst	<= counter_burst - '1';  
		end if;
		
	end if;
end process;

--**********************************************************
--  address control for the Payload
--
 
Add_HBM_High_cell <=  std_logic_vector(TO_UNSIGNED(HBM_MC_High_address,5));

-- The Payload address maneage the HBM memory used for thePayload
-- The end_PL_wth_hlf_wd signal will indicates if we wrote only one 128b of 256b HBM word
process(resetn_axi_clk, clock_axi) 
begin
	if resetn_axi_clk = '0'  then 
	   ---- modified
		PL_address_write 		<= x"0000040"; -- First data to be writen will be a Playload and it should start at add 0x30  (
		-- PL_address_write 		<= x"0000020"; -- First data to be writen will be a Playload and it should start at add 0x30  (
		end_PL_wth_hlf_wd		<= '0';        -- First data to be written is a Payload and it should start after Orbit and Fragment Header add 0x30
		-- end_PL_wth_hlf_wd		<= '1';        -- First data to be written is a Payload and it should start after Orbit and Fragment Header add 0x30
        
	elsif rising_edge(clock_axi) then   
		
		-- the address is increased by 0x20 for each 256bit word
		-- should not increase the address if last word is 128b 
		if 	(HBM_write_access = data_req or HBM_write_access = Next_Address_req ) and Wr_PayLoad = '1' and write_ready = '1' then
			PL_address_write		  <= PL_address_write + x"0000020";  
			
		elsif ( HBM_write_access = last_word  ) 								  and Wr_PayLoad = '1'  and write_ready = '1' then 
			if (odd_nb_word = '0' and end_PL_wth_hlf_wd = '0') or 
			   (odd_nb_word = '1' and end_PL_wth_hlf_wd = '1')then
				PL_address_write	      	<= PL_address_write + x"0000020";  
			end if;
		-- update Payload address with Orbit Header and Fragment Header
		---- modified
		elsif HBM_write_access = write_burst_done and Wr_Fragment_header = '1' then 
		-- elsif HBM_write_access = write_burst_done and Wr_Fragment_header = '1' and end_PL_wth_hlf_wd = '1' then 
			PL_address_write				<= PL_address_write + x"0000020";	-- increase the address only if 128b of the 256b HBM word  was written by a Playload 
		elsif HBM_write_access = write_burst_done and Wr_OrbitB_Header = '1'  then 
			PL_address_write				<= PL_address_write + x"0000020";	-- increase by 256b word  address  0x20
		end if;	

		-- at the end of an odd number of 128b transfer  
		if 	  (odd_nb_word = '1' and HBM_write_access = last_word) and end_PL_wth_hlf_wd = '0'  and Wr_PayLoad = '1' and write_ready = '1' then
			end_PL_wth_hlf_wd		<= '1';
		-- one of previous  transfer terminated with write of 128b and this is odd number too  
		elsif (odd_nb_word = '1' and HBM_write_access = last_word) and end_PL_wth_hlf_wd = '1'  and Wr_PayLoad = '1' and write_ready = '1' then
			end_PL_wth_hlf_wd		<= '0';
		-- update Payload address with Orbit Header and Fragment Header
		elsif HBM_write_access = write_burst_done and Wr_Fragment_header = '1' then 
		    ---- modified
		    end_PL_wth_hlf_wd		<= '0';
			-- end_PL_wth_hlf_wd		<= not(end_PL_wth_hlf_wd);					-- increase the address only if 128b of the 256b HBM word  was written by a Playload 
		end if;		 
	end if;
end process;

--**********************************************************
--  address control for the Orbit Header 
--
process(resetn_axi_clk, clock_axi)
begin	
	if resetn_axi_clk = '0' then 
		OH_address_write	<= (others => '0');				-- First address of the Orbit Header is 0x00
		end_OH_wth_hlf_wd	<= '0';							-- First address of the Orbit Header is 0x00
	elsif rising_edge(clock_axi) then
	
		if  HBM_write_access = write_burst_done and Wr_OrbitB_Header = '1' then -- We updated the Orbit header, new Orbit Header is the last address of the Fragment Header
			OH_address_write	<= FH_address_write;
            -- OH_address_write	<= PL_address_write;
			end_OH_wth_hlf_wd	<= end_FH_wth_hlf_wd;
		end if;
	end if;
end process;
	
--**********************************************************
--  address control for the Fragment Header
--
process(resetn_axi_clk, clock_axi)
begin	
	if resetn_axi_clk = '0' then 
	    ---- modified
	    FH_address_write	<= x"0000020";	-- First address of the Fragment Header is 0x20
		-- FH_address_write	<= x"0000000";	-- First address of the Fragment Header is 0x20
		end_FH_wth_hlf_wd	<= '0';			-- First address of the Fragment Header is 0x20
	elsif rising_edge(clock_axi) then
	
		if  	HBM_write_access = write_burst_done and Wr_Fragment_header = '1' then -- We updated the Fragment header, new Fragment Header is the last address of the Payload
			FH_address_write	<= PL_address_write;
			end_FH_wth_hlf_wd	<= end_PL_wth_hlf_wd;
		elsif 	HBM_write_access = write_burst_done and Wr_OrbitB_Header = '1' then
			FH_address_write	<= FH_address_write + x"0000020"; -- finish the Orbit block move the next Fragment Header to leave place for the Orbit Header
		end if;
	end if;
end process;
		
--**********************************************************
-- manage the Space used in the BIFI memory TCP STREAM		
process(resetn_axi_clk, clock_axi) 
begin
	if resetn_axi_clk = '0'  then 
		SND_NEW_write 				<= (others => '0');
	elsif rising_edge(clock_axi) then		
	
		--SND_new is different of the address 
		if 		HBM_write_access = prepare_counters then
			SND_NEW_write		<= SND_NEW_write + ("0000000000000000" & blk_len & "0000");
--		elsif 	Wr_OrbitB_Header = '1' 	and HBM_write_access = write_burst_done then
--			-- Orbit Header is a 256bit word
--			SND_NEW_write		<= SND_NEW_write + x"0000020";
--		elsif 	Wr_Fragment_header = '1'and HBM_write_access = write_burst_done then
--			-- Fragment Header is a 128bit word
--			SND_NEW_write		<= SND_NEW_write + x"0000010";
		end if;
	end if;
end process;				 

process(resetn_axi_clk, clock_axi) 
begin
	if resetn_axi_clk = '0'  then 
		WID_reg						<= (others => '0');
		incr_ID						<= '0';
		write_req_reg				<= '0';
	elsif rising_edge(clock_axi) then
	
		incr_ID				<= '0';
		if write_req_reg = '1' and write_ack = '1' then
			incr_ID			<= '1';
		end if;
		
		if incr_ID = '1' then
			WID_reg	<= WID_reg + '1';
		end if;
		
		if ( HBM_write_access = First_Address_req or HBM_write_access = Next_Address_req ) and write_req_reg = '0'   then 
			write_req_reg	<= '1'; 
		elsif write_ack = '1' then
			write_req_reg	<= '0'; 
		end if;
	end if;
end process;

-- store the write access ID done to compare with the 
process(clock_axi)  
begin
    if rising_edge(clock_axi) then
        if write_req_reg = '1'  then
            Last_wr_ID  <= WID_reg;
        end if;
    end if;
end process;



--**********************************************************
-- HBM Address

-- Control the address used to access the HBM		
process(clock_axi) 
begin
	if rising_edge(clock_axi) then 		
		if 			HBM_write_access = First_Address_req  and Wr_PayLoad = '1' then
			address_req		<= PL_address_write; 
		elsif 		HBM_write_access = First_Address_req  and Wr_OrbitB_Header = '1' then
			address_req		<= OH_address_write; 
		elsif 		HBM_write_access = First_Address_req  and Wr_Fragment_header = '1' then
			address_req		<= FH_address_write; 
		elsif	HBM_write_access = Next_Address_req  then
			address_req		<= address_req + x"0000200";
		end if;
	end if;
end process;	
		
--**********************************************************
-- word ena

process(clock_axi)
begin
	if rising_edge(clock_axi) then
		first_word_of_blk		<= '0';
		if HBM_write_access = First_Address_req then
			first_word_of_blk	<= '1';
		end if;
	end if;
end process;
 
process(resetn_axi_clk, clock_axi)
begin	
	if resetn_axi_clk = '0' then
		word_enable_cell 	<= "11"; 
		write_val_cell		<= '0';
		last_burst_wrd_cell	<= '0'; 
	elsif rising_edge(clock_axi) then
	 
		--last word of the block transfer 
		-- if odd number of 128b
		if write_ready = '1'  then
            if 	  (odd_nb_word = '1' and HBM_write_access = last_word) and end_PL_wth_hlf_wd = '0' and Wr_PayLoad = '1' then
                word_enable_cell	<= "01";    ----  last word of the block write is 128b
            
            -- last word of the block transfer
            -- if even number of 128b but previous transfer was odd
            elsif (odd_nb_word = '0' and HBM_write_access = last_word) and end_PL_wth_hlf_wd = '1' and Wr_PayLoad = '1' then
                word_enable_cell	<= "01";    ----  last word of the block write is 128b			
                
            --the last word of previous write block was only 128b
            elsif first_word_of_blk = '1' 							   and end_PL_wth_hlf_wd = '1' and Wr_PayLoad = '1' then
                word_enable_cell 	<= "10";

			-- Fragment Header is only 128b word
            elsif first_word_of_blk = '1' 							   and end_FH_wth_hlf_wd = '1' and Wr_Fragment_header = '1' then
                word_enable_cell 	<= "10";
			
            elsif first_word_of_blk = '1' 							   and end_FH_wth_hlf_wd = '0' and Wr_Fragment_header = '1' then
                ---- modified (fragment header is now 256b long)
                word_enable_cell 	<= "11";
                -- word_enable_cell 	<= "01";
			
			-- Orbit Header is unaligned  2x 128b    or 1 x 256b
            elsif first_word_of_blk = '1' 							   and end_OH_wth_hlf_wd = '1' and Wr_OrbitB_Header = '1' then
                word_enable_cell 	<= "10";
			
            elsif HBM_write_access = last_word						   and end_OH_wth_hlf_wd = '1' and Wr_OrbitB_Header = '1' then
                word_enable_cell 	<= "01";
			
            else
                word_enable_cell	<= "11";
            end if;
        end if;

		--if ( HBM_write_access = data_req or HBM_write_access = Next_Address_req or HBM_write_access = last_word) then
		if ( HBM_write_access = data_req or end_burst_wrd = '1' or HBM_write_access = last_word) then
			write_val_cell	<= '1'; 
		elsif write_ready = '1' then
		    write_val_cell	<= '0';
		end if;
		
		if write_ready = '1'  then 
            if (end_burst_wrd = '1' or HBM_write_access = last_word) then
                last_burst_wrd_cell	<= '1';
            else
                last_burst_wrd_cell	<= '0';
            end if;
		end if;		
		
	end if;
end process;

--**********************************************************
-- Data word

swapped_delay_128b	<= '1' when	(end_PL_wth_hlf_wd = '1' and Wr_PayLoad = '1') 			or     
								(end_FH_wth_hlf_wd = '1' and Wr_Fragment_header = '1') 	or     
								(end_OH_wth_hlf_wd = '1' and Wr_OrbitB_Header = '1') 	else '0';

process(clock_axi)
begin	
	if rising_edge(clock_axi) then
		if write_ready = '1' then
			if swapped_delay_128b = '1' then
				data_wrd_reg(255 downto 128)	<= data_in(127 downto 000);
				data_wrd_reg(127 downto 000)	<= tempo_128b_word;
			else	
				data_wrd_reg					<= data_in;
			end if;
			
			tempo_128b_word	<= data_in(255 downto 128);
			
		end if;
	end if;
end process;

last_burst_wrd_reg	<= last_burst_wrd_cell;	-- to avoid this delay we can try to implement the Memory block like a FIFO read  in advance 
word_enable_reg		<= word_enable_cell;
write_val_reg		<= write_val_cell;


--**************************************************************
-- Signal going to HBM write AXI bus
HBM_write_req.write_req			<= write_req_reg;
HBM_write_req.H_ADD_wr			<= Add_HBM_High_cell;
HBM_write_req.address_wr		<= address_req;
HBM_write_req.WrID				<= WID_reg;
HBM_write_req.wrwd_length		<= counter_burst; 	

HBM_write_req.data_i			<= data_wrd_reg; 
HBM_write_req.word_enable		<= word_enable_reg;
HBM_write_req.write_val			<= write_val_reg;						 
HBM_write_req.last_data_i		<= last_burst_wrd_reg;

--**************************************************************
-- Address write value used to manage the backpressure
process(resetn_axi_clk,clock_axi)
begin 
    if resetn_axi_clk = '0' then 
        SND_NEW_update_reg 	 	<= '0'; 
        BIFI_update_reg 		<= '0'; 
		Update_add_reg_BIFI_SND	<= '0';
    elsif rising_edge(clock_axi) then
		Update_add_reg_BIFI_SND	<= '0';  --delay 1 clock cycle to give more time (can be remove )
		if HBM_write_access = write_burst_done then
			Update_add_reg_BIFI_SND	<= '1';
		end if;
		 
        SND_NEW_update_reg  <= '0';
        if Update_add_reg_BIFI_SND = '1' and Wr_OrbitB_Header = '1' then  -- Update the SND when the Orbit Block is done 
            SND_NEW_update_reg  <= '1';
        end if;
		
        BIFI_update_reg  <= '0';
        if Update_add_reg_BIFI_SND = '1' then  -- update the BIFI address and each end of block to check the almost full of the BIFI
            BIFI_update_reg  <= '1';
        end if;
    end if;
end process;


-- counter which mentiuonned to where valid adta is written in HBM (ready to be send over Ethernet
SND_NEW			                <= SND_NEW_write; 
SND_NEW_update	                <= SND_NEW_update_reg;

BIFI_add						<= SND_NEW_write;
BIFI_add_update					<= BIFI_update_reg;

 
end behavioral;
