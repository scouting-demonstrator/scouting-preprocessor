----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 04.01.2021 15:19:19
-- Design Name:
-- Module Name: TCP_mem_block - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.interface.all;
use work.address_table.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TCP_mem_block is
		-- bit 4  selects the Stack
		-- b 3..0 selects the peusdo mem
	generic (TCP_Address_Offset				: integer;
	         HBM_MC_High_address            : integer);
	Port (
	-- control local interface
		usr_clk								: in std_logic;
		rst_usr_clk_n						: in std_logic;
		usr_func_wr							: in std_logic_vector(16383 downto 0);
		usr_wen								: in std_logic;
		usr_data_wr							: in std_logic_vector(63 downto 0);

		usr_func_rd							: in std_logic_vector(16383 downto 0);
		usr_rden							: in std_logic;
		usr_dto								: out std_logic_vector(63 downto 0);

		TCP_resetn							: in std_logic;
		TCP_clock							: in std_logic;

		TCP_ack_rcv				 			: in TCP_ack_rcvo_record;
		TCP_Ack_present						: in std_logic;
		rd_TCP_val_rcvo						: out std_logic;

		HBM_TCP_block						: out HBM_to_TCP_block_record;
		GNT_access_to_ETH					: in std_logic:= '1';
		TCP_request_took					: in std_logic;

		resetp_FED_sync_axi					: out std_logic;
		resetn_axi							: in std_logic;
		clock_axi							: in std_logic;
		resetn_counters						: in std_logic;
		resetp_Stat_rsync					: in std_logic;

		SND_NEW								: in std_logic_vector(31 downto 0);
		SND_NEW_update						: in std_logic;

		-- interface to HBM
  		HBM_read_req						: out HBM_read_req_record;
		HBM_read_ack						: in HBM_read_ack_record;

  		HBM_almost_full						: out std_logic;
  		HBM_pre_almost_full                 : out std_logic; --R

		counter_ena_read_block              : out std_logic
 );
end TCP_mem_block;

--******************************** ARCHITECTURE **************************************************
architecture Behavioral of TCP_mem_block is


signal read_ack							: std_logic;
signal data_0							: std_logic_vector(255 downto 0);
signal last_data_0						: std_logic;
signal rd_valid_data					: std_logic;
signal RdID_return						: std_logic_vector(5 downto 0);
signal Rd_error							: std_logic_vector(3 downto 0);
signal Add_HBM_High_cell				: std_logic_vector(4 downto 0);


component read_MEM_for_eth is
 port (
		resetn_eth						: in std_logic;
		clock_eth						: in std_logic;
		--request from TCp block
		req_rd_blk						: in std_logic; -- a pulse
		Add_HBM_High					: in std_logic_vector(4 downto 0); -- HBM high address to select the MCx
		Add_request						: in std_logic_vector(27 downto 0); -- is in Byte count
		Size_request					: in std_logic_vector(15 downto 0); -- is in 128-bit word count (only 9 to 0 are used  MAX 9Kbytes for Ethernet)
		Packet_ready					: out std_logic; -- request only started (not yet finish)

		-- return values to TCP logic
		eth_data_CS16					: out std_logic_vector(15 downto 0);
		eth_ack_packet					: in std_logic;	-- pulse to read CS and release the place
		eth_rd_data						: in std_logic;
		eth_data_o						: out std_logic_vector(511 downto 0);
		eth_data_ena_o					: out std_logic_vector(3 downto 0);
		eth_last_dt_o					: out std_logic;

		resetn_axi						: in std_logic;
		clock_axi						: in std_logic;
		resetn_counters					: in std_logic;
		-- interface to HBM
		-- output block to HBM
  		read_req						: out std_logic;
  		read_ack						: in std_logic;
		RdID							: out std_logic_vector(5 downto 0);
		H_ADD_rd						: out std_logic_vector(4 downto 0);
  		address_rd						: out std_logic_vector(27 downto 0);
  		rdwd_length						: out std_logic_vector(3 downto 0); -- indicates the number of words (256 bits) -1
  		data_0							: in std_logic_vector(255 downto 0);
  		last_data_0						: in std_logic;
		rd_valid_data					: in std_logic;
		RdID_return						: in std_logic_vector(5 downto 0);
  		Rd_error						: in std_logic_vector(3 downto 0);

  		trigger_o                       : out std_logic
 );
 end component;

signal Packet_ready							: std_logic;
signal eth_data_o							: std_logic_vector(511 downto 0);
signal eth_data_ena_o						: std_logic_vector(3 downto 0);
signal eth_last_dt_o						: std_logic;
signal eth_data_CS16						: std_logic_vector(15 downto 0);

component TCP_block is
	generic (TCP_Address_Offset				: integer);
	port
	(
		usr_clk							    : in std_logic;
		usr_rst_n						    : in std_logic;
		usr_func_wr						    : in std_logic_vector(16383 downto 0);
		usr_wen							    : in std_logic;
		usr_data_wr					   		: in std_logic_vector(63 downto 0);

		usr_func_rd						    : in std_logic_vector(16383 downto 0);
		usr_rden							: in std_logic;
		usr_data_rd						    : out std_logic_vector(63 downto 0);
		usr_rd_val						    : out std_logic;

		TCP_clock							: in std_logic;
		TCP_ack_rcv				 			: in TCP_ack_rcvo_record;
		TCP_Ack_present						: in std_logic;
		rd_TCP_val_rcvo						: out std_logic;

		HBM_MEM_req_rd_blk					: out std_logic;
		HBM_MEM_Size_request				: out std_logic_vector(31 downto 0); -- number of bytes
		HBM_MEM_Add_request					: out std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)

		HBM_blk_ready						: in std_logic;
		HBM_TCP_block						: out HBM_to_TCP_block_record;
		TCP_request_took					: in std_logic;

		TCP_Buf_SND_NEW                   	: in std_logic_vector(31 downto 0);
		TCP_Buf_SND_UNA                   	: out std_logic_vector(31 downto 0);
		TCP_Buf_VALID_P						: out std_logic;

		TCP_Packet_Done						: in std_logic;

		counter_ena_read_block              : out std_logic;

		close_FED							: out std_logic
	 );
end component;

signal HBM_TCP_block_cell					: HBM_to_TCP_block_record;
signal TCP_usr_dto						    : std_logic_vector(63 downto 0);
signal SND_NEW_resync 						: std_logic_vector(31 downto 0);
signal SND_NEW_latched 						: std_logic_vector(31 downto 0);
signal SND_UNA 						        : std_logic_vector(31 downto 0);
signal VALID_P 						        : std_logic;

signal HBM_MEM_req_rd_blk					: std_logic;
signal HBM_MEM_Size_request				    : std_logic_vector(31 downto 0); -- number of bytes
signal HBM_MEM_Add_request					: std_logic_vector(31 downto 0); -- SND_SEQ# (in bytes)

signal close_FED					        : std_logic;

component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic;
	Resetn_sync		    : out std_logic;
	Resetp_sync		    : out std_logic
	);
end component;

COMPONENT resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic;
	Resetp_sync			: out std_logic
	);
end COMPONENT;

component resync_v4 is
port (
	aresetn				: in std_logic;
	clocki			    : in std_logic;
	input				: in std_logic;
	clocko			    : in std_logic;
	output			    : out std_logic
	);
end component;

signal resetp_FED_rsync					    : std_logic;
signal resetn_FED_rsync					    : std_logic;
signal ltch_snd_new						    : std_logic;
signal Rstp_FED_Clock_ETH				    : std_logic;
signal sync_valid_p						    : std_logic;

signal BIFI_full_cell						: std_logic;
signal BIFI_almost_full_cell				: std_logic;

signal BIFI_used_cell					    : std_logic_vector(31 downto 0);
signal BIFI_used						    : std_logic_vector(31 downto 0);
signal BIFI_max							    : std_logic_vector(31 downto 0);
signal sync_SND_UNA						    : std_logic_vector(31 downto 0);
signal BIFI_in_backpressure				    : std_logic_vector(63 downto 0);
signal usr_data_rd_reg					    : std_logic_vector(63 downto 0);
signal HBM_almost_full_reg                  : std_logic;
signal HBM_pre_almost_full_reg              : std_logic; --R

signal PL_TX_lbus_A_reg						: tx_usr_record;
signal PL_TX_lbus_B_reg						: tx_usr_record;
signal PL_TX_lbus_C_reg						: tx_usr_record;
signal PL_TX_lbus_D_reg						: tx_usr_record;
signal last_data_reg    					: std_logic;

signal eth_rd_data_cell						: std_logic;
signal Auto_rd_data							: std_logic;
signal request_access_to_Ethernet			: std_logic;
signal request_access_to_Ethernet_reg		: std_logic;
signal write_data_block						: std_logic;


attribute mark_debug                                        : string;
attribute mark_debug of SND_NEW_resync	                         : signal is "true";
attribute mark_debug of sync_SND_UNA	                         : signal is "true";
attribute mark_debug of BIFI_full_cell	                         : signal is "true";

--******************************************************************************
--******************************------------------******************************
--***************************<<<     BEGINNING    >>>***************************
--******************************------------------******************************
--******************************************************************************
begin

read_ack		 <= HBM_read_ack.read_ack;
data_0		     <= HBM_read_ack.data_o;
last_data_0	     <= HBM_read_ack.rd_data_last;
rd_valid_data    <= HBM_read_ack.rd_valid_data;
RdID_return	     <= HBM_read_ack.RdID_return;
Rd_error		 <= HBM_read_ack.Rd_error;

Add_HBM_High_cell <=  std_logic_vector(TO_UNSIGNED(HBM_MC_High_address,5));

-- read a block from HBM
read_HBM_i1:read_MEM_for_eth
 port map(
		resetn_eth						=> TCP_resetn,
		clock_eth						=> TCP_clock,
		--request from TCp block
		req_rd_blk						=> HBM_MEM_req_rd_blk,
		Add_HBM_High					=> Add_HBM_High_cell, -- HBM high address to select the MCx
		Add_request(27 downto 0)		=> HBM_MEM_Add_request(27 downto 0),
		Size_request					=> HBM_MEM_Size_request(19 downto 4),--number of 128 bit
		Packet_ready					=> Packet_ready, -- request only started (not yet finish) ==>

		-- return values to TCP logic
		eth_data_CS16					=> eth_data_CS16,
		eth_data_o						=> eth_data_o		,
		eth_data_ena_o					=> eth_data_ena_o	,
		eth_last_dt_o					=> eth_last_dt_o	,
		eth_rd_data						=> eth_rd_data_cell,
		eth_ack_packet					=> TCP_request_took,	-- pulse to read CS and release the place

		resetn_axi						=> resetn_axi		,
		clock_axi						=> clock_axi		,
		resetn_counters					=> resetn_counters	,
		-- interface to HBM
		-- output block to HBM
  		read_req						=> HBM_read_req.read_req			,
  		read_ack						=> read_ack			,
		RdID							=> HBM_read_req.RdID				,
		H_ADD_rd						=> HBM_read_req.H_ADD_rd			,
  		address_rd						=> HBM_read_req.address_rd		,
  		rdwd_length						=> HBM_read_req.rdwd_length		,-- indicates the number of words (256 bits) -1
  		data_0							=> data_0			,
  		last_data_0						=> last_data_0		,
		rd_valid_data					=> rd_valid_data	,
		RdID_return						=> RdID_return		,
  		Rd_error						=> Rd_error

  		-- trigger_o                       : out std_logic
 );

--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--  Send out the packet

HBM_TCP_block.HBM_PL_last			<= eth_last_dt_o;
HBM_TCP_block.HBM_PL_ena			<= eth_data_ena_o;
HBM_TCP_block.HBM_PL_data	   		<= eth_data_o;
HBM_TCP_block.HBM_blk_wen			<= eth_rd_data_cell;
HBM_TCP_block.HBM_blk_CS			<= eth_data_CS16;

process(TCP_resetn,TCP_clock)
begin
	if TCP_resetn = '0'  then
		Auto_rd_data	<= '0';
	elsif rising_edge(TCP_clock) then
		request_access_to_Ethernet_reg	<= request_access_to_Ethernet and GNT_access_to_ETH;

		if request_access_to_Ethernet = '1' and GNT_access_to_ETH = '1' and  request_access_to_Ethernet_reg = '0' and HBM_TCP_block_cell.TCP_pckt_wo_dt = '0'  then
			Auto_rd_data	<= '1';
		elsif eth_last_dt_o = '1' then
			Auto_rd_data	<= '0';
		end if;
	end if;
end process;

eth_rd_data_cell			<= '1' when Auto_rd_data = '1' else '0';-- and last_data_reg = '0'


 -- TCP logic whihc manage the data to be sent on ethernet
 TCP_logic_i1:TCP_block
	generic map(TCP_Address_Offset			=> TCP_Address_Offset)
	port map	(
		usr_clk							    => usr_clk ,
		usr_rst_n						    => rst_usr_clk_n ,
		usr_func_wr						    => usr_func_wr	 ,
		usr_wen							    => usr_wen		 ,
		usr_data_wr					   		=> usr_data_wr	 ,

		usr_func_rd						    => usr_func_rd	 ,
		usr_rden							=> usr_rden	 ,
		usr_data_rd						    => TCP_usr_dto	 ,
		-- usr_rd_val						    =>  ,

		TCP_clock							=> TCP_clock,
		TCP_ack_rcv				 			=> TCP_ack_rcv		,
		TCP_Ack_present						=> TCP_Ack_present	,
		rd_TCP_val_rcvo						=> rd_TCP_val_rcvo	,

		HBM_MEM_req_rd_blk					=> HBM_MEM_req_rd_blk	,    -- ==>>
		HBM_MEM_Size_request				=> HBM_MEM_Size_request ,
		HBM_MEM_Add_request					=> HBM_MEM_Add_request	,

		HBM_blk_ready						=> Packet_ready,           -- <<==
		HBM_TCP_block 						=> HBM_TCP_block_cell	,

		TCP_request_took					=> TCP_request_took		,
		TCP_Packet_Done						=> TCP_request_took,

		TCP_Buf_SND_NEW                   	=> SND_NEW_resync,
		TCP_Buf_SND_UNA                   	=> SND_UNA,
		TCP_Buf_VALID_P						=> VALID_P,

		counter_ena_read_block              => counter_ena_read_block,
		close_FED							=> close_FED
	 );

request_access_to_Ethernet			<= HBM_TCP_block_cell.Request_TCP_packet;
HBM_TCP_block.Request_TCP_packet	<= HBM_TCP_block_cell.Request_TCP_packet;
HBM_TCP_block.Sel_TCP_Stream		<= HBM_TCP_block_cell.Sel_TCP_Stream	;
HBM_TCP_block.TCP_SEQ_Send			<= HBM_TCP_block_cell.TCP_SEQ_Send		;
HBM_TCP_block.TCP_ACK_Send			<= HBM_TCP_block_cell.TCP_ACK_Send		;
HBM_TCP_block.TCP_FLAG_Send 		<= HBM_TCP_block_cell.TCP_FLAG_Send 	;
HBM_TCP_block.TCP_option_Send 		<= HBM_TCP_block_cell.TCP_option_Send 	;
HBM_TCP_block.TCP_Length_Send		<= HBM_TCP_block_cell.TCP_Length_Send	;
HBM_TCP_block.TCP_max_size 			<= HBM_TCP_block_cell.TCP_max_size 		;
HBM_TCP_block.TCP_scale				<= HBM_TCP_block_cell.TCP_scale			;
HBM_TCP_block.TCP_win				<= HBM_TCP_block_cell.TCP_win			;
HBM_TCP_block.TCP_TS				<= HBM_TCP_block_cell.TCP_TS			;
HBM_TCP_block.TCP_TS_rply			<= HBM_TCP_block_cell.TCP_TS_rply		;
HBM_TCP_block.TCP_pckt_wo_dt		<= HBM_TCP_block_cell.TCP_pckt_wo_dt	;

resetp_FED_sync_axi	                         <=  resetp_FED_rsync;

--******    Control the almost full (BIFI is like a FIFO  )      *******

--*********************************************************
--  resync SND_NEW value to send to SM TCP                *
--*********************************************************
resync_reset_i1:resetp_resync
port map(
	aresetp				=> close_FED,
	clock				=> Clock_AXI,
	Resetp_sync			=> resetp_FED_rsync
	);

resync_reset_i2:resetp_resync
port map(
	aresetp				=> close_FED,
	clock				=> TCP_clock,
	Resetp_sync			=> Rstp_FED_Clock_ETH
	);

resetn_FED_rsync    <= not(resetp_FED_rsync);
sync_new:resync_v4
PORT MAP(
	aresetn	=> resetn_FED_rsync,
	clocki	=> Clock_AXI,
	input	=> SND_NEW_update,
	clocko	=> TCP_clock,
	output	=> ltch_snd_new
	);

process(Rstp_FED_Clock_ETH,TCP_clock)
begin
	if Rstp_FED_Clock_ETH = '1' then
		SND_NEW_resync						<= (others => '0');
	elsif rising_edge(TCP_clock) then
		if ltch_snd_new = '1' then
			SND_NEW_resync(31 downto 0) 	<= SND_NEW;
		end if;
	end if;
end process;

Resync_valid_p_i1:resync_v4
port map(
	aresetn	=> '1',
	clocki	=> TCP_clock,
	input	=> VALID_P,
	clocko	=> Clock_AXI,
	output	=> sync_valid_p
	);

process(resetp_FED_rsync,Clock_AXI)
begin
	if resetp_FED_rsync = '1' then
		BIFI_used_cell    <= (others => '0');
		SND_NEW_latched   <= (others => '0');
		sync_SND_UNA      <= (others => '0');
	elsif rising_edge(Clock_AXI) then
--		BIFI_used_cell(27 downto 0) <= SND_NEW(27 downto 0) - sync_SND_UNA(27 downto 0);-- number of memory cells used   (not free)
		BIFI_used_cell(27 downto 0) <= SND_NEW_latched(27 downto 0) - sync_SND_UNA(27 downto 0);-- number of memory cells used   (not free)
--		BIFI_used_cell(27 downto 5) <= SND_NEW(27 downto 5) - sync_SND_UNA(27 downto 5);-- number of memory cells used   (not free)

		if SND_NEW_update = '1' then
			SND_NEW_latched(31 downto 0) 	<= SND_NEW;
		end if;

		if 	sync_valid_p = '1' then
			sync_SND_UNA(27 downto 0)	<= SND_UNA(27 downto 0) ;--and BIFI_size(13 downto 0);
		end if;
	end if;
end process;

--**********************************************************************
-- BIFI management
-- compute the number of data in inside the BIFI memory and create the almost full signal

process(resetp_FED_rsync,Clock_AXI)
begin
	if resetp_FED_rsync = '1' then
		BIFI_full_cell <= '0';
	elsif rising_edge(Clock_AXI) then
		-- if 		(BIFI_used_cell(27 downto 0) )  > x"FFF0000" then  -- almost FUll
		if 		(BIFI_used_cell(27 downto 0) )  > x"FA00000" then  -- almost FUll --R
			BIFI_full_cell <= '1';
		-- elsif 	(BIFI_used_cell(27 downto 0) )  < x"FFE0000" then  -- no almost Full --R
		elsif 	(BIFI_used_cell(27 downto 0) )  < x"F500000" then  -- no almost Full
			BIFI_full_cell <= '0';
		end if;
	end if;
end process;

--R (to avoid possible block truncation)
process(resetp_FED_rsync,Clock_AXI)
begin
	if resetp_FED_rsync = '1' then
		BIFI_almost_full_cell <= '0';
	elsif rising_edge(Clock_AXI) then
		-- if 		(BIFI_used_cell(27 downto 0) )  > x"FFF0000" then  -- almost FUll
		if 		(BIFI_used_cell(27 downto 0) )  > x"F000000" then  -- almost FUll --R
			BIFI_almost_full_cell <= '1';
		-- elsif 	(BIFI_used_cell(27 downto 0) )  < x"FFE0000" then  -- no almost Full --R
		elsif 	(BIFI_used_cell(27 downto 0) )  < x"E000000" then  -- no almost Full
			BIFI_almost_full_cell <= '0';
		end if;
	end if;
end process;
--R (to avoid possible block truncation)


process(Clock_AXI)
begin
	if rising_edge(Clock_AXI) then
		HBM_almost_full_reg	<= BIFI_full_cell;
		HBM_pre_almost_full_reg <= BIFI_almost_full_cell;
	end if;
end process;

HBM_almost_full <= HBM_almost_full_reg;
HBM_pre_almost_full <= HBM_pre_almost_full_reg;


BIFI_used	<= BIFI_used_cell ;

-- measure the time of BIFI in backpressure
process(resetp_Stat_rsync,resetp_FED_rsync,Clock_AXI)
begin
	if resetp_Stat_rsync = '1' or resetp_FED_rsync = '1' then
			BIFI_in_backpressure<= (others => '0');
	elsif rising_edge(Clock_AXI) then
		if BIFI_full_cell = '1' then
			BIFI_in_backpressure <= BIFI_in_backpressure + '1';
		end if;
	end if;
end process;

--******************************************************************************************
--  user access read for status

process(usr_clk)
begin
	if rising_edge(usr_clk) then
		usr_data_rd_reg 							       	<= (others => '0');
			if 		usr_func_rd(TCP_Address_Offset  + BIFI_BackPressure) = '1' then
				usr_data_rd_reg          	<= BIFI_in_backpressure;
			elsif usr_func_rd(TCP_Address_Offset  + BIFI_used_off) = '1' then
				usr_data_rd_reg(31 downto 0)<= BIFI_used;
			elsif usr_func_rd(TCP_Address_Offset  + BIFI_Max_used) = '1' then
				usr_data_rd_reg(31 downto 0)<= BIFI_max;

			end if;
	end if;
end process;

process(resetp_Stat_rsync,resetp_FED_rsync,Clock_AXI)
begin
    if resetp_Stat_rsync = '1' or resetp_FED_rsync = '1' then
        BIFI_max    <= (others => '0');
	elsif rising_edge(Clock_AXI) then
		if BIFI_max < BIFI_used then
		  BIFI_max <= BIFI_used;
		end if;
	end if;
end process;


usr_dto	 <= TCP_usr_dto or usr_data_rd_reg;--or ;-- or

end Behavioral;
