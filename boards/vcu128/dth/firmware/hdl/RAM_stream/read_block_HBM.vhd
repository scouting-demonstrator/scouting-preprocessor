
LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-------------------------------------------------------------------
-- At the write side (the HBM acts as a FIFO) fragments coming 
-- from SR is recorded one after the other .
 
 
--ENTITY-----------------------------------------------------------
entity read_block_HBM is
-- This file works on Clock_AXI
	port
	(
		resetn							: in std_logic;
		clock_axi						: in std_logic;
		
		resetn_counters					: in std_logic;
		-- input/output signals from/to ETH 
		req_rd_blk						: in std_logic;
		blk_len							: in std_logic_vector(15 downto 0);-- number of 128 bit words
		-- bit 4  selects the Stack
		-- b 3..0 selects the peusdo mem
		req_H_add_rd					: in std_logic_vector(4 downto 0);
		req_address_rd					: in std_logic_vector(27 downto 0);
		data_rd							: out std_logic_vector(511 downto 0);
		data_ena						: out std_logic_vector(3 downto 0);
		last_data_rd					: out std_logic;
		val_data_block					: out std_logic;
		access_read_idle				: out std_logic;
		  
		-- interface to HBM 
  		read_req						: out std_logic;
  		read_ack						: in std_logic;
		RdID							: out std_logic_vector(5 downto 0);
		H_ADD_rd						: out std_logic_vector(4 downto 0);
  		address_rd						: out std_logic_vector(27 downto 0);
  		rdwd_length						: out std_logic_vector(3 downto 0); -- indicates the number of words (256 bits) -1
		readout_ready					: out std_logic;
  		data_0							: in std_logic_vector(255 downto 0);
  		last_data_0						: in std_logic;
		rd_valid_data					: in std_logic;
		RdID_return						: in std_logic_vector(5 downto 0);
  		Rd_error						: in std_logic_vector(3 downto 0)
	 );
end read_block_HBM;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of read_block_HBM is

component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic; 

	Resetn_sync		: out std_logic;
	Resetp_sync		: out std_logic
	);
end component;

type HBM_read_access_type is (	Idle,
								--wait_HBM_readout_idle,
								load_new_add_len,
								First_Address_req,
								inter_req0,
								inter_req1,
								inter_req2,
								inter_req3,
								Next_Address_req,
								last_request 
							);
signal HBM_read_access:HBM_read_access_type;

signal load_new_counters		: std_logic;

signal counter_len				: std_logic_vector(15 downto 0);
signal address_rd_reg			: std_logic_vector(27 downto 0);
signal rdwd_length_reg			: std_logic_vector(3 downto 0);
signal H_ADD_rd_reg				: std_logic_vector(4 downto 0); 
signal read_req_reg				: std_logic; 

signal Haddress_memory			: std_logic_vector(4 downto 0);
signal address_memory			: std_logic_vector(27 downto 0);
signal length_memory			: std_logic_vector(15 downto 0);

signal Request_wait				: std_logic;
signal leave_HBM_readout_Idle	: std_logic;

type HBM_readout_type is (		Idle,
								latch_new_val,
								read_out_HBM,
								end_readout
							);
signal HBM_readout:HBM_readout_type;

signal load_val_for_readout		: std_logic; 
  
signal val_data_block_reg		: std_logic; 
signal data_reg					: std_logic_vector(511 downto 0);
attribute dont_touch : string;
attribute dont_touch of data_reg : signal is "true";

signal data_ena_reg				: std_logic_vector(3 downto 0);
signal data_ena_last_reg		: std_logic_vector(3 downto 0);
signal Burst_length_counter		: std_logic_vector(15 downto 0);
signal swap_128b_wrd			: std_logic; 
signal temp_128b_data			: std_logic_vector(127 downto 0);
signal latch_data_reg			: std_logic;
signal delay_latch_data_reg		: std_logic;
signal First_wrd_128b			: std_logic;
signal pass_the_first			: std_logic; 
signal delay_rd_valid_data		: std_logic;
signal last_data_rd_reg			: std_logic; 
signal Send_last_word			: std_logic; 

signal resetn_sync	             : std_logic;
 
attribute mark_debug : string;

--attribute mark_debug of data_reg			: signal is "true";	
--attribute mark_debug of latch_data_reg	 	: signal is "true";	
--attribute mark_debug of swap_128b_wrd		: signal is "true";	
--attribute mark_debug of rd_valid_data		: signal is "true";	
--attribute mark_debug of data_0				: signal is "true";	
--attribute mark_debug of data_ena_reg      	: signal is "true";	 
--***************************************************************** 
--*************<     CODE START HERE    >**************************
--***************************************************************** 
 begin 

resync_reset_i1:resetn_resync  
port map(
	aresetn			=> resetn,
	clock			=> clock_axi,
	Resetn_sync		=> resetn_sync
	);
--**********************************************************
--  state machine to write to HBM memory
--
HBM_wr_i1:process(resetn_sync,clock_axi)
begin 
if resetn_sync = '0' then
	HBM_read_access 	<= idle; 
elsif rising_edge(clock_axi) then 
Case HBM_read_access is
	when idle =>
		if req_rd_blk = '1' then
			HBM_read_access <= load_new_add_len;
		end if;
		
	-- when wait_HBM_readout_idle =>
		-- if HBM_readout = idle then
			-- HBM_read_access <= load_new_add_len;
		-- end if;
		
	when load_new_add_len =>
		if HBM_readout = idle then
			HBM_read_access <= First_Address_req;
		end if; 
		
	-- the fisrt address req is without data trasnfer
	when First_Address_req =>
		if counter_len <= x"0010" then
			HBM_read_access <= last_request;
		else
			HBM_read_access <= inter_req0;
		end if;

	-- insert 4 clocks cycles between each request
	when inter_req0 =>
		if read_ack = '1' then
			HBM_read_access <= inter_req1; 
		end if;
		
	when inter_req1 =>
		HBM_read_access <= inter_req2; 
		
	when inter_req2 =>
		HBM_read_access <= inter_req3; 
		
	when inter_req3 =>
		HBM_read_access <= Next_Address_req; 
		
	-- next address request is shared with data trasnfer , for efficiency
	when Next_Address_req =>
		if counter_len <= x"0010" then --- x"0010"
			HBM_read_access <= last_request;
		else
			HBM_read_access <= inter_req0;
		end if;
	 
	when last_request => 
		if  Request_wait = '0' then
			HBM_read_access <= idle;
		end if;
		
	when others =>
		HBM_read_access <= idle;
		
  end case;
 end if;
end process;

access_read_idle	<= '1' when HBM_read_access = idle else '0';
load_new_counters	<= '1' when HBM_read_access = load_new_add_len and HBM_readout = idle else '0';
--**********************************************************
-- Latch values
--
--process(clock_axi)
--begin
--	if rising_edge(clock_axi) then
--		if req_rd_blk = '1' then
			Haddress_memory	<= req_H_add_rd;
			address_memory	<= req_address_rd;
			length_memory   <= blk_len;
--		end if;
--	end if;
--end process;

--**********************************************************
--    word counter 
--
process(resetn_sync,clock_axi)
variable temp_len					: std_logic_vector(15 downto 0);
begin
	if resetn_sync = '0' then
		counter_len				<= (others => '0'); 
	elsif rising_edge(clock_axi) then
		--if add = x0   and Len x0 => Len number of 128b word
		--if add = x1   and Len x0 => Len + 2 number of 128b word
		--if add = x0   and Len x1 => Len + 1 number of 128b word
		--if add = x1   and Len x1 => Len + 2 number of 128b word

		if load_new_counters = '1' and address_memory(4) = '1' then
			temp_len		:= length_memory + "10";
		elsif load_new_counters = '1' then
			temp_len		:= length_memory + '1';
		end if;
				
		if load_new_counters = '1' then
			counter_len(15)				<= '0';
			counter_len(14 downto 0)	<= temp_len(15 downto 1);
		elsif HBM_read_access = First_Address_req or HBM_read_access = Next_Address_req then 
			counter_len					<= counter_len - x"10"; -- burst read of 16 words of 256 bytes
		end if;		

	end if;
end process;

--**********************************************************
--  address control
--
address_rd_reg(4 downto 0) <= (others => '0');-- HBM memory do not care of lower bit (the data bus is modulo 256b)

process(resetn_sync,clock_axi)
begin
	if resetn_sync = '0' then
		H_ADD_rd_reg	<= (others => '0');
		address_rd_reg	<= (others => '0');
	elsif rising_edge(clock_axi) then
		if load_new_counters = '1' then
			H_ADD_rd_reg	<= Haddress_memory;
		end if;
		
		if load_new_counters = '1' then
			address_rd_reg(27 downto 5)	<= address_memory(27 downto 5);
		elsif HBM_read_access = inter_req1 then
			address_rd_reg(27 downto 5)	<= address_rd_reg(27 downto 5) + "00000000000000000010000";
		end if;
	end if;
end process;


--**********************************************************
-- word ena
--
process(resetn_sync,clock_axi)
variable  len_temp					: std_logic_vector(4 downto 0);
begin
	if resetn_sync = '0' then	
		rdwd_length_reg	<= (others => '0');
		read_req_reg	<= '0';  
	elsif rising_edge(clock_axi) then
	   if HBM_read_access = First_Address_req or HBM_read_access = Next_Address_req then 
            if counter_len > x"0010" then
                rdwd_length_reg <= "1111";
            else
                len_temp 		:= counter_len(4 downto 0) - '1'; 
                rdwd_length_reg <= len_temp(3 downto 0);
            end if;
		end if;
		
		if HBM_read_access = First_Address_req or HBM_read_access = Next_Address_req then
			read_req_reg <= '1';
		elsif read_ack = '1' then
			read_req_reg <= '0';
		end if;
		
	end if;
end process;
 
	
--**************************************************************
-- signal request to HBM 
--
read_req		<= read_req_reg;
H_ADD_rd		<= H_ADD_rd_reg;
address_rd		<= address_rd_reg;
rdwd_length		<= rdwd_length_reg;
RdID			<= "000000";



process(resetn_sync,clock_axi)
begin
	if resetn_sync = '0' then
		Request_wait		<= '0';
	elsif rising_edge(clock_axi) then
		if req_rd_blk = '1' then
			Request_wait	<= '1';
		elsif leave_HBM_readout_Idle = '1' then
			Request_wait	<= '0';
		end if;
	end if;
end process;
	
--**********************************************************
--############################################################
--**********************************************************


--**********************************************************
--  state machine to write to HBM memory
--
HBM_readout_i1:process(resetn_sync,clock_axi)
begin 
if resetn_sync = '0' then
	HBM_readout 			<= idle;
	leave_HBM_readout_Idle	<= '0';
elsif rising_edge(clock_axi) then
leave_HBM_readout_Idle			<= '0';

Case HBM_readout is
	when idle =>
		if Request_wait = '1' and HBM_read_access /= idle then
			leave_HBM_readout_Idle	<= '1';
			HBM_readout 			<= latch_new_val;
		end if;
		
	when latch_new_val =>
		HBM_readout <= read_out_HBM;
	-- 
	when read_out_HBM =>
		if Send_last_word = '1' then
			HBM_readout <= end_readout;
		end if;
	
	when end_readout =>
		HBM_readout <= idle; 
		
	when others =>
		HBM_readout <= idle;
		
  end case;
 end if;
end process;

--*********************************************************
-- recuparate the memorised values
--
process(clock_axi)
begin
	if rising_edge(clock_axi) then 
		load_val_for_readout		<= '0';
		if HBM_readout = latch_new_val then
			load_val_for_readout	<= '1'; 
		end if;
	end if;
end process;
--*********************************************************
-- Burst length
--
process(resetn_sync,clock_axi)
variable temp_len					: std_logic_vector(15 downto 0);
begin
	if resetn_sync = '0' then
		swap_128b_wrd			<= '0';
		delay_rd_valid_data		<= '0';
		First_wrd_128b			<= '0';
		Burst_length_counter	<= (others => '0');
		data_ena_last_reg		<= (others => '0');
	elsif rising_edge(clock_axi) then

		if load_val_for_readout = '1' then
			-- odd_number_wrd				<= length_memory(0);
			swap_128b_wrd				<= address_memory(4); -- if we start to read at unaligned 256b address, we should swapp datas
			                                                  
			if 		length_memory(1 downto 0) = "00" then
				data_ena_last_reg	<= "1111";
			elsif 	length_memory(1 downto 0) = "01" then
				data_ena_last_reg	<= "0001";
			elsif 	length_memory(1 downto 0) = "10" then
				data_ena_last_reg	<= "0011";
			elsif 	length_memory(1 downto 0) = "11" then
				data_ena_last_reg	<= "0111";
			end if; 	
		end if;
		
		if	load_new_counters = '1' and address_memory(4) = '1' then -- is the address is not aligned on 256bit HBM word , the  first word has onle higher 128b availabled
			First_wrd_128b <= '1';
		elsif rd_valid_data = '1' then
			First_wrd_128b <= '0';
		end if;
	
		delay_rd_valid_data	<= rd_valid_data;
				
		if load_val_for_readout = '1' then
			Burst_length_counter		<= length_memory;	
		elsif rd_valid_data = '1' then 
			if First_wrd_128b = '1' then
				Burst_length_counter		<= Burst_length_counter - "001";  -- read a 128 word
			else
				Burst_length_counter		<= Burst_length_counter - "010";  -- read a 256 word
			end if;
			--Burst_length_counter		<= Burst_length_counter - "100";  -- read a 512b word
		end if;
		
	end if;
end process;

--**********************************************************
-- Data word
--
process(resetn_sync,clock_axi)
begin
	if resetn_sync = '0' then
		latch_data_reg			<= '0';
		pass_the_first			<= '0';
		delay_latch_data_reg	<= '0';
	elsif rising_edge(clock_axi) then
		if rd_valid_data = '1' then--or (latch_data_reg = '1' and Burst_length_counter <= x"0003") then---- mettre une condition de fin de burst!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			latch_data_reg		<= not(latch_data_reg);
			pass_the_first		<= '1';
		--elsif  HBM_read_access = idle then
		elsif  HBM_readout = idle then
			latch_data_reg		<= '0';
			pass_the_first 		<= '0';
		end if;
		
		delay_latch_data_reg	<= latch_data_reg;
	end if;
end process;

--##########################################################
--*********************************************************
-- Data words  (split the process in 128b words for Checksum compute
--
process(data_ena_reg(0),clock_axi)
begin	
	if data_ena_reg(0) = '0' then	
		data_reg(127 downto 000) <= (others => '0');
	elsif rising_edge(clock_axi) then 
		if 		swap_128b_wrd = '1' and (rd_valid_data = '1' ) then 
			if latch_data_reg = '0' then
				temp_128b_data			<= data_0(255 downto 128);
			end if;
		end if;
		
		if 		swap_128b_wrd = '1' and ((rd_valid_data = '1' ) 
		or (delay_rd_valid_data = '1' and Burst_length_counter = x"0000")) then --A150   L210	
			--if latch_data_reg = '0' then   for A150 L220
				data_reg(127 downto 000) <= temp_128b_data;
			--end if;	
		elsif	swap_128b_wrd = '0' and (rd_valid_data = '1'  ) then --or (delay_rd_valid_data = '1' and Burst_length_counter <= x"0004")
			if latch_data_reg = '0' then
				data_reg(127 downto 000) <= data_0(127 downto 000);
			end if;			
 
		end if;		
	end if;
end process;		
		
process(data_ena_reg(1),clock_axi)
begin	
	if data_ena_reg(1) = '0' then	
		data_reg(255 downto 128) <= (others => '0');
	elsif rising_edge(clock_axi) then 
		if 		swap_128b_wrd = '1' and (rd_valid_data = '1' ) then --or (delay_rd_valid_data = '1' and Burst_length_counter <= x"0003") or (delay_latch_data_reg = '1' and Burst_length_counter = x"0001")) then
			
			if latch_data_reg = '1' then
				data_reg(255 downto 128) <= data_0(127 downto 0);
			end if;	
			
		elsif	swap_128b_wrd = '0' and (rd_valid_data = '1' ) then --or (delay_rd_valid_data = '1' and Burst_length_counter <= x"0004")
			if latch_data_reg = '0' then
				data_reg(255 downto 128) <= data_0(255 downto 128);
			end if;			
 
		end if;		
	end if;
end process;		

process(data_ena_reg(2),clock_axi)
begin	
	if data_ena_reg(2) = '0' then	
		data_reg(383 downto 256) <= (others => '0');
	elsif rising_edge(clock_axi) then 
		if 		swap_128b_wrd = '1' and (rd_valid_data = '1' ) then --or (delay_rd_valid_data = '1' and Burst_length_counter <= x"0003") or (delay_latch_data_reg = '1' and Burst_length_counter = x"0001")) then
			
			if latch_data_reg = '1' then 
				data_reg(383 downto 256) <= data_0(255 downto 128);
			end if;	
			
		elsif	swap_128b_wrd = '0' and (rd_valid_data = '1'  ) then --or (delay_rd_valid_data = '1' and Burst_length_counter <= x"0004")
			if latch_data_reg = '1' then
				data_reg(383 downto 256) <= data_0(127 downto 0);
			end if;			
 
		end if;		
	end if;
end process;		
		
process(data_ena_reg(3),clock_axi)
begin	
	if data_ena_reg(3) = '0' then	
		data_reg(511 downto 384) <= (others => '0');
	elsif rising_edge(clock_axi) then 
		if 		swap_128b_wrd = '1' and (rd_valid_data = '1' ) then --or (delay_rd_valid_data = '1' and Burst_length_counter <= x"0003") or (delay_latch_data_reg = '1' and Burst_length_counter = x"0001")) then
			
			if latch_data_reg = '0' then
				data_reg(511 downto 384) <= data_0(127 downto 0);
			end if;	
			
		elsif	swap_128b_wrd = '0' and (rd_valid_data = '1' ) then --or (delay_rd_valid_data = '1' and Burst_length_counter <= x"0004")
			if latch_data_reg = '1' then
				data_reg(511 downto 384) <= data_0(255 downto 128);
			end if;			
 
		end if;		
	end if;
end process;		
		
--##########################################################		
		
process(clock_axi)
begin	
	if rising_edge(clock_axi) then 		
		val_data_block_reg				<= '0';
		data_ena_reg					<= "1111";
		last_data_rd_reg				<= '0';		
		Send_last_word					<= '0';
		
		if		swap_128b_wrd = '1' and ((rd_valid_data = '1'  ) 
		or (delay_rd_valid_data = '1' and Burst_length_counter = x"0000")) then --A150   L230
			-- or (delay_latch_data_reg = '1' and Burst_length_counter = x"0001")) then
			if 	latch_data_reg = '0' 
			or	Burst_length_counter <= x"0001" then					--= x"0001" A150  L220    <= x"0001"  A150   L210
				val_data_block_reg		 <= pass_the_first;
				if Burst_length_counter <= x"0001" then		--1
					last_data_rd_reg		<= '1'; 
					data_ena_reg			<= data_ena_last_reg;	
					Send_last_word			<= '1';	
				end if;
			end if;	
			
		elsif 	swap_128b_wrd = '0' and  (( rd_valid_data = '1' ) 
		or (delay_rd_valid_data = '1' and Burst_length_counter = x"0000")) then --A160  L220
			if latch_data_reg = '1' 
			or Burst_length_counter = x"0001" then  --A160  L210
				val_data_block_reg		 <= '1';
				if Burst_length_counter <= x"0002" then		--2
					last_data_rd_reg		<= '1'; 
					data_ena_reg			<= data_ena_last_reg;	
					Send_last_word			<= '1';			
				end if;
			end if;			
			
		end if;		
				 
	end if;
end process;
 
--**************************************************************
-- signal return from HBM 
--
last_data_rd	<= last_data_rd_reg;
data_rd			<= data_reg;
data_ena		<= data_ena_reg; 
val_data_block	<= val_data_block_reg;
readout_ready	<= '1'  when HBM_readout = read_out_HBM else '0';

end behavioral;