
LIBRARY ieee;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
 
--ENTITY-----------------------------------------------------------
entity Check_read_HBM is
	port
	(
		resetn_axi        : in std_logic;
		clock_axi         : in std_logic;
						   
		req_read          : in std_logic;
		word_ena	      : in std_logic;
		word              : in std_logic_vector(255 downto 0);
		check_oups        : out std_logic
	 );
end Check_read_HBM;

--ARCHITECTURE------------------------------------------------------
architecture behavioral of Check_read_HBM is
--	signal Q : std_logic;
 
signal swapped_data				: std_logic_vector(255 downto 0);
signal req_register				: std_logic_vector(1 downto 0);

 
signal trig_num                 : std_logic_vector(31 downto 0);

signal temp_A                   : std_logic_vector(127 downto 0);
signal temp_B                   : std_logic_vector(127 downto 0);
signal temp_C                   : std_logic_vector(127 downto 0);
signal temp_D                   : std_logic_vector(127 downto 0);

signal counter					: std_logic_vector(15 downto 0);
signal load_data				: std_logic;
signal incr_data				: std_logic;
signal reset_start_check		: std_logic;
signal check_data				: std_logic;
signal do_not_check				: std_logic;
signal oups_we_found_problem	: std_logic;
signal oups_we_found_problem2	: std_logic;

attribute mark_debug : string; 
--attribute mark_debug of oups_we_found_problem 	     : signal is "true";  
--attribute mark_debug of oups_we_found_problem2 	     : signal is "true";  
--attribute mark_debug of trig_num 			         : signal is "true";  
--attribute mark_debug of counter 			         : signal is "true";  
--attribute mark_debug of word 			             : signal is "true";  
--attribute mark_debug of word_ena 			         : signal is "true";  

--***************************************************************** 
--*************<     CODE START HERE    >**************************
--***************************************************************** 
 begin 
 
--process(word)
--begin
--    swap_loop:for I in 0  to 31 loop
--        swapped_data(I*8+7 downto I*8) <= word(255-8*I downto 248-8*I);
--    end loop;
--end process;


process(clock_axi)
begin
    if rising_edge(clock_axi) then
        if word_ena = '1' then
            temp_a <= word(127 downto 0);
            temp_b <= temp_a;
            temp_c <= temp_b;
            temp_d <= temp_c;
        end if;
    end if;
end process;




process(resetn_axi,clock_axi)
variable load_from_header       : std_logic;

begin
	if resetn_axi = '0' then 
		oups_we_found_problem	<= '0'; 
		oups_we_found_problem2  <= '0';
	elsif rising_edge(clock_axi) then	
		if word_ena = '1' then
		  if      word(31 downto 0) = x"C1C1C1C1" then
		      if word(63 downto 32) /= trig_num  then
		          oups_we_found_problem <='1';
		      end if;
		  elsif    word(159 downto 128) = x"C1C1C1C1" then
		      if word(191 downto 160) /= trig_num  then
		          oups_we_found_problem <='1';
		      end if;
		  end if;
		  
		  if temp_a(31 downto 0) = x"C1C1C1C1" and temp_b(31 downto 0) = x"C1C1C1C1" and temp_c(31 downto 0) = x"C1C1C1C1" and temp_d(31 downto 0) = x"C1C1C1C1"
		      and temp_a(63 downto 48) /= temp_b(63 downto 48) then
		      oups_we_found_problem2 <= '1';
		  end if;
		end if;
    end if;
end process;
		 
process(clock_axi)
begin
    if rising_edge(clock_axi) then		
		if word_ena = '1' then
		   if     word(127 downto 112) = x"475A" then
		      trig_num <= word(63 downto 32);
		   elsif  word(255 downto 240) = x"475A" then
		      trig_num <= word(191 downto 160);
		  end if;
		end if;
	end if;
end process;
 
 
check_oups  <= oups_we_found_problem or oups_we_found_problem2;
 

	
end behavioral;