----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.10.2018 13:23:55
-- Design Name: 
-- Module Name: add_16x16 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
-- Calculate the checksum
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CS_Compute is
	Port ( 
		aclrp		: IN std_logic;
		clock		: IN std_logic;
		clken		: IN std_logic  := '1';
		last_word	: in std_logic;
		data		: IN std_logic_vector (511 DOWNTO 0);
		data_ena	: in std_logic_vector(3 downto 0);
		
		result		: OUT std_logic_vector (15 DOWNTO 0);
		CS_done		: out std_logic
);
end CS_Compute;

--******************************************************************************
architecture Behavioral of CS_Compute is

signal first_word							: std_logic_vector(127 downto 0);
signal second_word							: std_logic_vector(127 downto 0);
signal third_word							: std_logic_vector(127 downto 0);
signal fourth_word							: std_logic_vector(127 downto 0);

signal clk_ena_reg							: std_logic;
signal last_word_reg						: std_logic;
signal last_word_A_reg						: std_logic;
signal last_word_B_reg						: std_logic;

component add_8x16 is
	Port ( 
		aclrp		: IN STD_LOGIC  := '0';
		clock		: IN STD_LOGIC  := '0';
		clken		: IN STD_LOGIC  := '1';
		data0x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data1x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data2x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data3x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data4x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data5x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data6x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		data7x		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		
		result		: OUT STD_LOGIC_VECTOR (18 DOWNTO 0)
);
end component;

signal inter_A0_result				: std_logic_vector(18 downto 0);
signal inter_A1_result				: std_logic_vector(18 downto 0);
signal inter_A2_result				: std_logic_vector(18 downto 0);
signal inter_A3_result				: std_logic_vector(18 downto 0);

signal cken_del_A						: std_logic;
signal cken_del_B						: std_logic;

component accu_4x31 is
	Port ( 
		aclrp		: IN STD_LOGIC  := '0';
		clock		: IN STD_LOGIC  := '0';
		clken		: IN STD_LOGIC  := '1';
		data0x		: IN STD_LOGIC_VECTOR (18 DOWNTO 0);
		data1x		: IN STD_LOGIC_VECTOR (18 DOWNTO 0);
		data2x		: IN STD_LOGIC_VECTOR (18 DOWNTO 0);
		data3x		: IN STD_LOGIC_VECTOR (18 DOWNTO 0);

		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
);
end component;

signal inter_B_result				: std_logic_vector(31 downto 0);
 
signal last_word_del_A				: std_logic;
signal last_word_del_B				: std_logic;

component add_1x16_carry is
	Port ( 
		aclrp				: IN STD_LOGIC  := '0';
		clock				: IN STD_LOGIC  := '0';
		finish_Checksum		: in std_logic;		-- a pulse add 21..16  + 15..0   and the next internal pulse add the carry
		data0x				: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result				: OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
		cs_done				: out std_logic
);
end component;

 
--*********************************************************************
--******************     CODE    START     HERE        ****************
--*********************************************************************
begin

process(clock)
begin
	if rising_edge(clock) then
		first_word				<= (others => '0');
		if data_ena(0) = '1' then
			first_word			<=	data(127 downto 000);
		end if;
				
		second_word				<= (others => '0');
		if data_ena(1) = '1' then
			second_word			<=	data(255 downto 128);
		end if;
		
		third_word				<= (others => '0');
		if data_ena(2) = '1' then
			third_word			<=	data(383 downto 256);
		end if;

		fourth_word				<= (others => '0');
		if data_ena(3) = '1' then
			fourth_word			<=	data(511 downto 384);
		end if;
 
		last_word_reg		<= last_word;
		
		clk_ena_reg			<= clken;
	end if;
end process;

CS_stage_1_0:add_8x16  
	Port Map( 
		aclrp		=> aclrp							, 
		clock		=> clock						, 
		clken		=> clk_ena_reg						, 
		data0x		=> first_word(015 downto 000)	, 
		data1x		=> first_word(031 downto 016)	, 
		data2x		=> first_word(047 downto 032)	, 
		data3x		=> first_word(063 downto 048)	, 
		data4x		=> first_word(079 downto 064)	, 
		data5x		=> first_word(095 downto 080)	, 
		data6x		=> first_word(111 downto 096)	, 
		data7x		=> first_word(127 downto 112)	, 
		                                              
		result		=> inter_A0_result				 
);

CS_stage_1_1:add_8x16  
	Port Map( 
		aclrp		=> aclrp							, 
		clock		=> clock						, 
		clken		=> clk_ena_reg						, 
		data0x		=> second_word(015 downto 000)	, 
		data1x		=> second_word(031 downto 016)	, 
		data2x		=> second_word(047 downto 032)	, 
		data3x		=> second_word(063 downto 048)	, 
		data4x		=> second_word(079 downto 064)	, 
		data5x		=> second_word(095 downto 080)	, 
		data6x		=> second_word(111 downto 096)	, 
		data7x		=> second_word(127 downto 112)	, 
		                                              
		result		=> inter_A1_result				 
);

CS_stage_1_2:add_8x16  
	Port Map( 
		aclrp		=> aclrp							, 
		clock		=> clock						, 
		clken		=> clk_ena_reg						, 
		data0x		=> third_word(015 downto 000)	, 
		data1x		=> third_word(031 downto 016)	, 
		data2x		=> third_word(047 downto 032)	, 
		data3x		=> third_word(063 downto 048)	, 
		data4x		=> third_word(079 downto 064)	, 
		data5x		=> third_word(095 downto 080)	, 
		data6x		=> third_word(111 downto 096)	, 
		data7x		=> third_word(127 downto 112)	, 
		                                              
		result		=> inter_A2_result				 
);

CS_stage_1_3:add_8x16  
	Port Map( 
		aclrp		=> aclrp							, 
		clock		=> clock						, 
		clken		=> clk_ena_reg						, 
		data0x		=> fourth_word(015 downto 000)	, 
		data1x		=> fourth_word(031 downto 016)	, 
		data2x		=> fourth_word(047 downto 032)	, 
		data3x		=> fourth_word(063 downto 048)	, 
		data4x		=> fourth_word(079 downto 064)	, 
		data5x		=> fourth_word(095 downto 080)	, 
		data6x		=> fourth_word(111 downto 096)	, 
		data7x		=> fourth_word(127 downto 112)	, 
		                                              
		result		=> inter_A3_result				 
);

process(clock) 
begin
	if rising_edge(clock) then
	    cken_del_B          <= cken_del_A;
		cken_del_A 		    <= clk_ena_reg;
		
		last_word_B_reg     <= last_word_A_reg;
        last_word_A_reg     <= last_word_reg;
        
		last_word_del_A		<= '0';
		if cken_del_B = '1' and last_word_B_reg = '1' then
			last_word_del_A	<= '1';
		end if;
	end if;
end process;

CS_stage_2:accu_4x31 
	Port Map( 
		aclrp		=> aclrp				,
		clock		=> clock			,
		clken		=> cken_del_A		,--try to implement the HBM read with 2 clk cycle and use cken_del_B
		data0x		=> inter_A0_result	,
		data1x		=> inter_A1_result	,
		data2x		=> inter_A2_result	,
		data3x		=> inter_A3_result	,
                                         
		result		=> inter_B_result	 
);


CS_stage_last:add_1x16_carry  
	Port Map( 
		aclrp				=> aclrp,
		clock				=> clock,
		finish_Checksum		=> last_word_del_A,
		data0x				=> inter_B_result,
		result				=> result,
		cs_done				=> CS_done 
);




end Behavioral;
