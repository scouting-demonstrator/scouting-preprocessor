------------------------------------------------------
-- I2C interface for SPF module
-- freq 100 kHz (ext clock x4)
--  Ver 2.00
--  add a bir to remove the stop between a access to set a register to be read and the read access
-- Dominique Gigi March 2017
------------------------------------------------------
--   register send to access the component
--		func(0) bit 31-24  = address of the I2C componennt
--		func(0) bit 24   '1'  read access ; '0' write access
--		func(0) bit 23-22 RESERVDED
--		func(0) bit 21-20 = number of register offset byte
--		func(0) bit 19-17 = number of data byte
--		func(0) bit 16 = 0 stop between set address and read value  = 1 no-stop between set address and read value   (this is only valid when a read access)
--		func(0) bit 15-0 register offset to be accessed
--

-- 		func(1) bit 31-0 data to be written
--  returned data
--		func(0) bit 17 '1' indicate that previous access had a error (reset by a access request
--		func(0) bit 16 '1' previous access is finished ; data value for read access; a write spend 7 ms;
--		func(1) bit 31-0  data read
--
-- Ex:  A2002356 : execute a write on address 0xA2  register 0x23 data 0x56
--		  A10034xx : execute a read request on address 0xA0  register 0x34
--							a read on DTO will return the data read
--
--
-- ----------------------------------------------------
-- I2C memory content
-- there are 3 bytes per 8bit in i2c
-- the bit 7..0 contains an address (with bit 0 =0 write // =1 read
--                       an offset
--                       a  data
-- the bit 15..8 contains  bit 8 indicates (if 1) that the access will contain an offset (bit23..21 not null)
--                         bit 9 indicates (if 1) that the access will not finisd by a STOP but a START will be generated to start the next access
--                         bit 10 Reserved
--                         bit 11 the current access is finished, should be followed by a new access
--                         bit 12 Reserved
--                         bit 13 Reserved
--                         bit 14 Reserved
--                         bit 15 all the accesses are done finish the autoread
-- the bit 23..16 contains b20..16 data length
--                         b23..21 offset length
------------------------------------------------------
LIBRARY ieee;
-- LIBRARY altera_mf;
-- LIBRARY altera;

USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.vcomponents.all;


entity i2c_master_32b_autoread is
port (
	low_clk			: in std_logic; -- 1.6 MHz
	ena_low_clk		: in std_logic; -- enable the clock to have a clock of 4000 Khz double frequency of the I2C bus (400 KHz --> 800 KHz)
	reset_n		   	: in std_logic;
	restart_auto_read : in std_logic;
	clock			: in std_logic;

    I2C_SDA 			: inout std_logic := '1';
    I2C_SCL 			: inout std_logic := '1';

	dti				: in std_logic_vector(31 downto 0);
	cs			 	: in std_logic;
	func			: in std_logic_vector(1 downto 0);
	wen				: in std_logic;
	rd 				: in std_logic;
	dto				: out std_logic_vector(31 downto 0);
	-- direct output of I2C read
	Byte_i2c_out	: out std_logic_vector(7 downto 0);
	Byte_i2c_latch	: out std_logic;
	ack_rq			: out std_logic
	);


end i2c_master_32b_autoread;
architecture behavioral of i2c_master_32b_autoread is

signal reset_n_internal               : std_logic;

COMPONENT I2C_command
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    addra : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(23 DOWNTO 0)
  );
END COMPONENT;

attribute mark_debug : string;

 type boot_type is (		idle,
                            Start_sequence_boot,
							Read_byte,
							Latch_byte,
                            wait_end_byte,
                            Wait_end_I2C_Accesses
							);
signal Boot_sequence: boot_type;


 type i2c_type is (			idle,
							start_acc,
							interm_0,
							byte,
							ack,
							stop_acc
							);
signal I2C_sequence: i2c_type;

--ATTRIBUTE ENUM_ENCODING : STRING;
--ATTRIBUTE ENUM_ENCODING OF i2c_type: TYPE IS "000001 000010 000100 001000 010000 100000";

 type access_type is (		idle,
							start,
							i2c_add,
							i2c_add_ack,
							offset_reg,
							offset_reg_ack,
							wr_data,
							wr_data_ack,
							read_data,
							read_data_ack,
							stop_start,
							Time_btw_Start_Stop,
							stop
							);
signal access_SM: access_type;
--attribute mark_debug of access_SM: signal is "true";


signal CTRL_req                             : std_logic;
signal CTRL_acc_ongoing                     : std_logic;
signal tog							        : std_logic_vector(1 downto 0);
attribute mark_debug of tog          : signal is "true";
signal reg							        : std_logic_vector(31 downto 0);
signal sh							        : std_logic_vector(7 downto 0);
signal Start_rq_sync				        : std_logic := '0';
signal CTRL_req_sync				        : std_logic := '0';
signal value_cnt					        : std_logic_vector(7 downto 0);
signal load_cnt 					        : std_logic;
signal rd_sh						        : std_logic_vector(31 downto 0);
signal chg_state					        : std_logic;
signal SDA_reg 					         	: std_logic;
signal SCL_z 					         	: std_logic;
signal SCL_o 					         	: std_logic;
signal SCL_i 					         	: std_logic;
signal SDA_i			                    : std_logic;-- separate the IN and the OUT if we want to implement multiple
signal SDA_o			                    : std_logic;
signal SDA_z			                    : std_logic;
signal strobe_acc_cell				        : std_logic;
signal strobe_acc_rst				        : std_logic;

signal stop_access				         	: std_logic;

signal data_register				        : std_logic_vector(31 downto 0);
signal nb_data						        : std_logic_vector(5 downto 0);
signal nb_reg_offset				        : std_logic_vector(1 downto 0);
signal nb_data_cnt				          	: std_logic_vector(5 downto 0);
signal nb_reg_offset_cnt		          	: std_logic_vector(1 downto 0);
signal add_register				          	: std_logic_vector(7 downto 0);
signal Offset_data					        : std_logic_vector(7 downto 0);
signal reg_offset					        : std_logic_vector(15 downto 0);
signal start_stop_btw			          	: std_logic;
signal bit_counter				          	: std_logic_vector(3 downto 0);
signal start_done					        : std_logic := '0';
signal stop_done					        : std_logic  := '0';
signal fall_edge					        : std_logic;
signal rise_edge					        : std_logic;
signal nack_acc					          	: std_logic;
signal read_access_pre  		          	: std_logic;
signal read_access				          	: std_logic;
signal read_request				          	: std_logic;
signal first_set_offset			          	: std_logic;
signal reset_First_set_off   	          	: std_logic;
signal end_access					        : std_logic;
signal end_access_cell				        : std_logic;
signal error						        : std_logic;

signal I2C_state_ack				        : std_logic;
signal I2C_state_start			          	: std_logic;
signal Access_state_start_stop			 	: std_logic;
signal last_data_read			          	: std_logic;
signal Sequence_idle                    	: std_logic;
signal Access_done                    		: std_logic;
signal end_access_resync               		: std_logic;
signal reset_low_clock						: std_logic := '0';
signal delay_access_done_1_i2c_clock_start	: std_logic;
signal delay_access_done_1_i2c_clock		: std_logic_vector(1 downto 0);

signal time_btw_Start_stop_done             : std_logic;
signal counter_btw_Start_stop               : std_logic_vector(7 downto 0);

signal Byte_i2c_latch_cell					: std_logic;

signal Boot_read        					: std_logic;
signal Boot_latch        					: std_logic;
signal boot_Addr        					: std_logic_vector(7 downto 0);
signal boot_add_data    					: std_logic_vector(7 downto 0);
signal boot_Command     					: std_logic_vector(7 downto 0);
signal boot_length     					: std_logic_vector(7 downto 0);

signal State_Ack                            : std_logic;
signal State_Ack_1C                         : std_logic;
signal Ack_req_boot_Done                        : std_logic;

signal Start_MAC_read                               : std_logic;
signal Start_MAC_read_counter                       : std_logic_vector(15 downto 0):= x"0000";

--attribute mark_debug of Start_MAC_read_counter          : signal is "true";
--attribute mark_debug of Start_MAC_read       	: signal is "true";
--attribute mark_debug of restart_auto_read          	: signal is "true";
--attribute mark_debug of reset_n_internal         	: signal is "true";
--attribute mark_debug of reset_n	          	: signal is "true";

--attribute mark_debug of SDA_i	          	: signal is "true";
--attribute mark_debug of SDA_o	          	: signal is "true";
--attribute mark_debug of SDA_z	          	: signal is "true";
--attribute mark_debug of SCL_i	          	: signal is "true";
--attribute mark_debug of SCL_o	          	: signal is "true";
--attribute mark_debug of SCL_z	          	: signal is "true";
--attribute mark_debug of Boot_sequence	          	: signal is "true";
--attribute mark_debug of I2C_sequence	          	: signal is "true";
--attribute mark_debug of access_SM	          	: signal is "true";
--attribute mark_debug of error	          	: signal is "true";
--######################################################################
--                 Core start HERE
--######################################################################
begin

--******************* BUF IO *********************

IOBUF_SDA : IOBUF
port map (
O       => SDA_i, -- 1-bit output: Buffer output
I       => SDA_o, -- 1-bit input: Buffer input
IO      => I2C_SDA, -- 1-bit inout: Buffer inout (connect directly to top-level port)
T       => SDA_z
);
IOBUF_SCL : IOBUF
port map (
O       => SCL_i, -- 1-bit output: Buffer output
I       => SCL_o, -- 1-bit input: Buffer input
IO      => I2C_SCL, -- 1-bit inout: Buffer inout (connect directly to top-level port)
T       => SCL_z
);

--*********************** AUTO START ***************

process(reset_n,restart_auto_read,clock)
begin
    if restart_auto_read = '1' or reset_n = '0' then
        Start_MAC_read_counter  <= (others => '0');
        Start_MAC_read          <= '0';
    elsif rising_edge(clock) then
        -- Counter after load if no reset
        if Start_MAC_read_counter(7) = '0'  then --11
            Start_MAC_read_counter  <= Start_MAC_read_counter + '1';
        end if;

        -- When reach 0x400  start auto read
        Start_MAC_read          <= '0';
        if Start_MAC_read_counter = x"0050" then --x400
            Start_MAC_read      <= '1';
        end if;

    end if;
end process;


-- Generate internal reset before auto read and when external reset
process(reset_n,clock)
begin
    if reset_n = '0' then
        reset_n_internal        <= '0';
    elsif rising_edge(clock) then
        reset_n_internal        <= '1';
        if Start_MAC_read_counter < x"0020" then --x100
            reset_n_internal        <= '0';
        end if;
    end if;
end process;

--******************************************************************************
--  ROM containing the boot load of the MAC address from the EEPROM
Boot_ROM:I2C_command
  PORT MAP(
    clka                =>  clock,--: IN STD_LOGIC;
    ena                 =>  Boot_read,--: IN STD_LOGIC;
    addra               =>  boot_Addr,--: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    douta(07 downto 00) =>  boot_add_data,--: OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
    douta(15 downto 08) =>  boot_Command, --: OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
                            --b0 : First set the offset
                            --b1 : No Stop only a restart
                            --b2 :
                            --b3 : Restart with a new address
                            --b4 :
                            --b5 :
                            --b6 :
                            --b7 : End of Boot sequence
    douta(23 downto 16) =>  boot_length --: OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
                            --b5..0  data length
                            --b7..6  offset length
  );


i3:entity work.resync_pulse_ena
	port map(
		aresetn		=> reset_n_internal,
		clocki		=> clock,
		in_s		=> strobe_acc_cell,
		clocko		=> low_clk,
		ena_clock	=> ena_low_clk,
		out_s		=> Start_rq_sync
		);


--#### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
-- State machine for bit transfer : bit , ack, start , stop,..

boot_clk:process(low_clk,reset_low_clock)
begin
	if reset_low_clock = '0' then
		Boot_sequence 		<= idle;
		Ack_req_boot_Done   <= '0';
	elsif rising_edge(low_clk) then
--		if ena_low_clk = '1' then
		Ack_req_boot_Done   <= '0';
			Case Boot_sequence is
				when idle =>
					if Start_rq_sync = '1' and ena_low_clk = '1' then
						Boot_sequence 	<= Start_sequence_boot;
					end if;

				when Start_sequence_boot =>
				    if ena_low_clk = '1' and (access_SM = Idle or access_SM = Start ) then
					   Boot_sequence 	<= Read_byte;
					 end if;

				when Read_byte =>
				    if ena_low_clk = '1' then
					   Boot_sequence 	<= Latch_byte;
					end if;

				when Latch_byte =>
				    if ena_low_clk = '1' then
					   Boot_sequence 	<= wait_end_byte;
					end if;

				when wait_end_byte =>
					if nack_acc = '1'  then
				        Boot_sequence 	<= Idle;
				    elsif    State_Ack_1C ='0' and State_Ack = '1' and boot_command(7) = '1' then
						Boot_sequence 	<= Wait_end_I2C_Accesses;
		            elsif  State_Ack_1C ='0' and State_Ack = '1' and boot_command(3) = '1' then
						Boot_sequence 	<= Start_sequence_boot;
		            elsif  State_Ack_1C ='0' and State_Ack = '1'  then
						Boot_sequence 	<= Read_byte;
				    end if;

				when Wait_end_I2C_Accesses =>
				    if access_SM = Idle then
				       Ack_req_boot_Done   <= '1';
					   Boot_sequence <= idle;
					end if;

				when others =>
					Boot_sequence <= idle;
			  end case;
--		end if
	end if;
end process;

--****************************************************************************
ack_rq <= '1' when  Ack_req_boot_Done = '1' else '0';

--**********************************************
--  command from external interface
process(Start_MAC_read,clock)
begin
	if Start_MAC_read = '1' then
        boot_Addr                       <= (others => '0');

	elsif rising_edge(clock) then
	    if Boot_latch = '1' then
            boot_Addr                   <= boot_Addr + '1';
        end if;
	end if;
end process;


process(reset_n_internal,clock)
begin
	if reset_n_internal = '0' then
		add_register(7 downto 1)		<= (others => '0');-- set the address to xA0
		add_register(0)					<= '0';
		read_request					<= '1';
		Offset_data						<= (others => '0');--set the offset to 0x0002
		nb_reg_offset					<= (others => '0');--set EEPROM address to 0
		nb_data							<= (others => '0'); --read 2 bytes (MAC lower part at boot
		start_stop_btw 					<= '0';
        Boot_read                       <= '0';
        first_set_offset                <= '0';
        State_Ack                       <= '0';
        State_Ack_1C                    <= '0';
        CTRL_req                        <= '0';
        CTRL_acc_ongoing                <= '0';
	elsif rising_edge(clock) then

        CTRL_req		<= '0';
        if cs = '1' and wen = '1' and func(0) = '1' then
            CTRL_req					<= '1';
            CTRL_acc_ongoing            <= '1';
            add_register(7 downto 1)	<= dti(31 downto 25);
            add_register(0)				<= dti(24);
            read_request				<= dti(24);
            nb_reg_offset				<= dti(21 downto 20);
            nb_data(5 downto 3)			<= "000";
            nb_data(2 downto 0)			<= dti(19 downto 17);
            reg_offset					<= dti(15 downto 0);
            start_stop_btw 				<= dti(16);
            first_set_offset            <= '0';
            if dti(21 downto 20) /= "000" and  dti(24) = '1' then
                first_set_offset        <= '1';
            end if;
        elsif  end_access_resync = '1' then
            CTRL_acc_ongoing              <= '0';
        elsif Boot_latch = '1' and access_SM = start  then
            add_register                <= boot_add_data;
            read_request                <= boot_add_data(0);
            nb_reg_offset               <= boot_length(7 downto 6);
            nb_data                     <= boot_length(5 downto 0);
            start_stop_btw              <= boot_command(1);
            first_set_offset            <= boot_command(0);
        elsif reset_First_set_off = '1' then
            first_set_offset            <= '0';
            start_stop_btw              <= '0';
            reg_offset                  <= (others => '0');

        end if;

        if CTRL_acc_ongoing = '1'  then
            if nb_reg_offset_cnt /= "00"  then
                if nb_reg_offset_cnt = "10" then
                    Offset_data		<= reg_offset(15 downto 8);
                else
                    Offset_data		<= reg_offset(07 downto 0);
                end if;
            elsif read_request = '0' and nb_data_cnt /= "000" then
                if nb_data_cnt = "100" then
                    Offset_data		<= data_register(31 downto 24);
                elsif nb_data_cnt = "011" then
                    Offset_data		<= data_register(23 downto 16);
                elsif nb_data_cnt = "010" then
                    Offset_data		<= data_register(15 downto 08);
                else
                    Offset_data		<= data_register(07 downto 00);
                end if;
            end if;
        elsif Boot_latch = '1'  then
            Offset_data             <= boot_add_data;
        end if;

        Boot_latch      <= Boot_read;
        Boot_read       <= '0';
        if Boot_sequence = Read_byte and  ena_low_clk = '1'  then
            Boot_read   <= '1';
        end if;

        State_Ack       <= '0';
        if I2C_sequence = Ack  then
            State_Ack   <= '1';
        end if;

        State_Ack_1C    <= State_Ack;
	end if;
end process;

process(clock)
begin
	if rising_edge(clock) then

		if cs = '1' and wen = '1' and func(1) = '1' then
			data_register				<= dti;
		end if;
	end if;
end process;

process(clock)
begin
	if rising_edge(clock) then
		if rd = '1' then
			dto							<= (others => '0');
			if 		func(0) = '1' then
				dto(16) 				<= Access_done;
				dto(17) 				<= error;
			elsif 	func(1) = '1' then
				dto						<= rd_sh;
			end if;
		end if;
	end if;
end process;

strobe_acc_cell		<= Start_MAC_read;


resync_rst_i2:entity work.resetn_resync
port map(
	aresetn			=> reset_n_internal,
	clock			=> low_clk,
	Resetn_sync		=> reset_low_clock
	);

--#############################################
-- logic to specify when the bus is available
--  we add a delay of 1 clock I2C cycle
--  to avoid to have access request to fast after the end of previous access
process(low_clk,reset_low_clock)
begin
	if reset_low_clock = '0' then
		delay_access_done_1_i2c_clock		<= "00";
		delay_access_done_1_i2c_clock_start <= '0';
	elsif rising_edge(low_clk) then
		if   ena_low_clk = '1' then
			if end_access = '1'	then
				delay_access_done_1_i2c_clock_start <= '1';
			elsif  delay_access_done_1_i2c_clock = "11" then
				delay_access_done_1_i2c_clock_start <= '0';
			end if;

			if delay_access_done_1_i2c_clock_start = '1' then
				delay_access_done_1_i2c_clock	<= delay_access_done_1_i2c_clock + '1';
			else
				delay_access_done_1_i2c_clock	<= "00";
			end if;
		end if;
	end if;
end process;


end_access_cell <= '1' when delay_access_done_1_i2c_clock = "11" and ena_low_clk = '1' else '0';

i0:entity work.resync_pulse_ena
	port map(
		aresetn		=> reset_low_clock,
		clocki		=> low_clk,
		in_s		=> end_access_cell,
		clocko		=> clock,
		ena_clock	=> '1',
		out_s		=> end_access_resync
		);

process(reset_n_internal,clock)
begin
	if reset_n_internal = '0' then
		Access_done	<= '1';
	elsif rising_edge(clock) then
		if CTRL_req = '1' then 			-- start the access
			Access_done	<= '0';
		elsif end_access_resync = '1' then 	-- end access
			Access_done	<= '1';
		end if;
	end if;
end process;

--***********************************************

--**********    SCL clock made ******************
toggle:process(Sequence_idle,low_clk)
begin
	if Sequence_idle = '1'  then
		tog 			<= "10";
	elsif rising_edge(low_clk) then
		if ena_low_clk = '1' then
			tog(0)		<= '0';
			if I2C_sequence = start_acc or I2C_sequence = stop_acc then
				tog(0)	<= '1';
			--elsif (I2C_sequence = byte or  I2C_sequence = ack)  then
			elsif (I2C_sequence = byte or  I2C_sequence = ack) and (SCL_i = '1' or tog(1) = '0')  then
				tog(0) 	<= not(Tog(0));
			end if;

			if  tog(0) = '1' and (I2C_sequence = byte or  I2C_sequence = ack or I2C_sequence = Interm_0) then
				tog(1) 	<= not(tog(1));
			elsif I2C_sequence = stop_acc then
				tog(1)	<= '1';
			end if;
		end if;
	end if;
end process;



-- resync the state before using it as an asynchronous value
process(low_clk)
begin
    if rising_edge(low_clk) then
       Sequence_idle        <= '0';
       if I2C_sequence = idle then
            Sequence_idle 	<= '1';
       end if;
    end if;
end process;

process(tog,access_SM)
begin
		rise_edge 		<= '0';
		if tog(1) = '0' and tog(0) = '1' and access_SM /= Idle then
			rise_edge 	<= '1';
		end if;

		fall_edge		<= '0';
		if tog(1) = '1' and tog(0) = '1' and access_SM /= Idle then
			fall_edge 	<= '1';
		end if;
end process;


process(low_clk)
begin
	if rising_edge(low_clk) then
		if ena_low_clk = '1' then
			start_done 		<= '0';
			if access_SM = start and SDA_i = '0' and tog(1) = '1' then
				start_done 	<= '1';
			end if;

			stop_done 		<= '0';
			if (access_SM = stop or access_SM = stop_start) and (SDA_i = '0' ) and tog(1) = '1' then
				stop_done 	<= '1';
			end if;
		end if;
	end if;
end process;

chg_state <= '1' when tog(0) = '1' and tog(1) = '1'   else '0';

SCL_o <= '0';
-- Tristate Clock output when Idle
scl_clock:process(start_stop_btw,I2C_sequence,tog)
begin
	if I2C_sequence = idle then
		SCL_z <= '1';
	else
		if tog(1) = '1' and not(start_stop_btw = '1' and I2C_sequence = stop_acc) then
			SCL_z <= '1';
		else
		    SCL_z <= '0';
		end if;
	end if;
end process;



--#### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
-- State machine for bit transfer : bit , ack, start , stop,..

resync_sigi3:entity work.resync_pulse_ena
	port map(
		aresetn		=> reset_n_internal,
		clocki		=> clock,
		in_s		=> CTRL_req,
		clocko		=> low_clk,-- to be check
		ena_clock	=> ena_low_clk,
		out_s		=> CTRL_req_sync
		);

I2C_clk:process(low_clk,reset_low_clock)
begin
	if reset_low_clock = '0' then
		I2C_sequence 		<= idle;
	elsif rising_edge(low_clk) then
		if ena_low_clk = '1' then
			Case I2C_sequence is
				when idle =>
					if Boot_sequence  = Start_sequence_boot  or  CTRL_req_sync = '1' then
						I2C_sequence 	<= start_acc;
					end if;

				when start_acc =>
					if Start_done = '1' then
						I2C_sequence 	<= interm_0;
					end if;

				when interm_0 =>
					if Tog(1) = '0' and SDA_i = '0' then
						I2C_sequence 	<= byte;
					end if;

				when byte =>
					if stop_access = '1' then
						I2C_sequence 	<= stop_acc;
					elsif bit_counter = x"1" and fall_edge = '1' then
						I2C_sequence 	<= ack;
					end if;

				when ack =>
					if fall_edge = '1' then
						if (nack_acc = '1') or (first_set_offset = '1' and nb_reg_offset_cnt = "00" )  then --and read_request = '1'
							I2C_sequence 	<= stop_acc;
						else
							I2C_sequence 	<= byte;
						end if;
					end if;

				when stop_acc =>
					if stop_done = '1' then
						if Access_state_start_stop = '1' then
							I2C_sequence <= start_acc;
						else
							I2C_sequence <= idle;
						end if;
					end if;

				when others =>
					I2C_sequence <= idle;
			  end case;
		end if;
	end if;
end process;

--#### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
-- State machine for word transfer (Address, data, pointers,...

resync_rst_i1:entity work.resetp_resync
port map(
	aresetp				=>	strobe_acc_cell,
	clock				=>	low_clk,
	Resetp_sync			=>	strobe_acc_rst
	);

-- indicates the end of the access (I2C bus released)
Access_status:process(low_clk)
begin
	if rising_edge(low_clk) then
			end_access	<= '0';
		if access_SM = stop and stop_done = '1'  then
			end_access	<= '1';
		end if;
	end if;
end process;

access_clk:process(low_clk,reset_n_internal)
begin
	if reset_n_internal = '0' then
		Byte_i2c_latch_cell	<= '0';
		access_SM			<= idle;
		error				<= '0';
		stop_access 		<= '0';
		nb_data_cnt 		<= (others => '0');
		nb_reg_offset_cnt 	<= (others => '0');
		reset_First_set_off <= '0';
	elsif rising_edge(low_clk) then
		Byte_i2c_latch_cell	<= '0';
		reset_First_set_off <= '0';

		if ena_low_clk = '1' then
			stop_access 	<= '0';
			Case access_SM is
				when idle =>
					if Boot_sequence  = Start_sequence_boot or CTRL_req_sync = '1' then
						access_SM <= start;
					end if;

				when start =>
					if I2C_state_start = '1'  and start_done = '1' then
					    nb_reg_offset_cnt   <= nb_reg_offset;
						access_SM 	       <= i2c_add;
					end if;

				when i2c_add =>
					if I2C_state_ack = '1' then
						access_SM 	<= i2c_add_ack;
					end if;

				when i2c_add_ack =>
					nb_data_cnt 		<= nb_data;
					if fall_edge = '1' then
						if nack_acc = '1' then
							error				<= '1';
							access_SM 			<= stop;
						elsif (nb_reg_offset /= "00" and read_request = '0') or first_set_offset = '1'  then
							access_SM 			<= offset_reg;
						elsif read_request = '0' and nb_data /= "000000" then
							access_SM 			<= wr_data;
						elsif read_request = '1' and nb_data /= "000000" then
							access_SM 			<= read_data;
						end if;
					end if;

				when offset_reg =>
					if I2C_state_ack = '1' then
						nb_reg_offset_cnt 	<= nb_reg_offset_cnt - '1';
						access_SM 			<= offset_reg_ack;
					end if;

				when offset_reg_ack =>
					if fall_edge = '1' then
						if nb_reg_offset_cnt /= "00" then
							access_SM 			<= offset_reg;
						elsif first_set_offset = '1' then
							access_SM 			<= stop_start;
						elsif read_request = '0' and nb_data /= "000000" then
							access_SM 			<= wr_data;
						elsif read_request = '1' and nb_data /= "000000" then
							access_SM 			<= read_data;
						else
							stop_access			<= '1';
							access_SM 			<= stop;
						end if;
					end if;

				when wr_data =>
					if I2C_state_ack = '1' then
						nb_data_cnt		<= nb_data_cnt - '1';
						access_SM 		<= wr_data_ack;
					end if;

				when wr_data_ack =>
					if fall_edge = '1' then
						if nack_acc = '1' then
							--error				<= '1';
							access_SM 			<= stop;
						elsif nb_data_cnt /= "000000"  then
							access_SM 			<= wr_data;
						else
							stop_access			<= '1';
							access_SM 			<= stop;
						end if;
					end if;

				when stop_start =>
					stop_access		<= '1';
					if stop_done = '1' then
		                reset_First_set_off <= '1';
						access_SM 	<= Time_btw_Start_Stop;
					end if;

				when Time_btw_Start_Stop =>
					if time_btw_Start_stop_done = '1' then
						access_SM 	<= start;
					end if;

				when read_data =>
					if  I2C_state_ack = '1' then
						nb_data_cnt	<= nb_data_cnt - '1';
						access_SM 	<= read_data_ack;
					end if;

				when read_data_ack =>
					if fall_edge = '1' then
						Byte_i2c_latch_cell	<= '1';
						-- if nack_acc = '1' then
							-- access_SM 			<= stop;
							-- error					<= '1';
						-- els
						if nb_data_cnt /= "000000"  then
							access_SM 		<= read_data;
						else
							stop_access		<= '1';
							access_SM 		<= stop;
						end if;
					end if;

				when stop =>
					if stop_done = '1' then
						access_SM <= idle;
					end if;

				when others =>
					access_SM <= idle;
				end case;
			end if;
		end if;
end process;

process(low_clk)
begin
    if rising_edge(low_clk) then
        if ( access_SM = read_data or access_SM = wr_data or access_SM = offset_reg ) and ena_low_clk = '1'   then
            read_access <= read_access_pre;
        elsif access_SM = Idle then
            read_access <= '0';
        end if;

        if  access_SM = i2c_add and I2C_sequence = byte and bit_counter = x"1" and ena_low_clk = '1' and SCL_i = '1'  then
            read_access_pre <= SDA_o;
        end if;

        if access_SM = idle then
            counter_btw_Start_stop  <= (others => '0');
        elsif access_SM =  Time_btw_Start_Stop  and ena_low_clk = '1' then
            counter_btw_Start_stop  <= counter_btw_Start_stop  + '1';
        end if;

        time_btw_Start_stop_done    <= '0';
        if counter_btw_Start_stop = x"30"  then
            time_btw_Start_stop_done <= '1';
        end if;

    end if;
end process;

Access_state_start_stop <= '1' when  access_SM = stop_start else '0';


I2C_state_ack			<= '1' when I2C_sequence = ack  else '0';
I2C_state_start			<= '1' when I2C_sequence = start_acc else '0';

process(reset_low_clock,low_clk)
begin
	if reset_low_clock = '0' then
		bit_counter	<= (others => '0');
	elsif rising_edge(low_clk) then
	 	if ena_low_clk = '1' then
			if I2C_sequence  = start_acc or  I2C_sequence  = ack then
				bit_counter 	<= x"8";
			elsif  I2C_sequence  = byte and fall_edge = '1' then
				bit_counter 	<= bit_counter - '1';
			end if;
		end if;
	end if;
end process;

--############################


process(low_clk,reset_low_clock)
begin
	if reset_low_clock = '0' then
		nack_acc	<= '0';
	elsif rising_edge(low_clk) then
 		if ena_low_clk = '1' then
			if (access_SM = i2c_add_ack or access_SM = offset_reg_ack or access_SM = wr_data_ack) and SCL_i = '1' then
				if SDA_i = '1' then
					nack_acc <= '1';
				end if;
			elsif access_SM = idle then
				nack_acc <= '0';
			end if;
		end if;
	end if;
end process;


SDA_ctrl:process(low_clk)
begin
	if rising_edge(low_clk) then
		if ena_low_clk = '1' then
			if	 	access_SM = start then
				sh 			<= add_register;--add_register;
		    elsif   I2C_sequence = interm_0 then
				if nb_reg_offset_cnt /= "00" and CTRL_acc_ongoing = '1' and first_set_offset = '1' then
					sh(0) 	<= '0';
				end if;
			elsif I2C_sequence = ack then
				if nb_reg_offset_cnt /= "00"  then
					sh			<= Offset_data(07 downto 0);
				elsif read_request = '0' and nb_data_cnt /= "000000" then
                    sh			<= Offset_data(07 downto 00);
				end if;

			elsif tog(0) = '0' and tog(1) = '0' and (I2C_sequence = byte) then
				if I2C_sequence = byte then
					sh(7 downto 1) 	<= sh(6 downto 0);
					sh(0)			<= '0';
				end if;
			end if;

			--read bit from I2C bus
			if access_SM = read_data then
				if I2C_sequence = byte and rise_edge = '1' then
					rd_sh(31 downto 1) 	<= rd_sh(30 downto 0);
					rd_sh(0) 			<= SDA_i;
				end if;
			end if;

		end if;
	end if;
end process;

--*************************
-- out byte read from I2C  and execute a latch (this is usefull for the read of a memory at Boot at the FPGA (Serial Number, MAC,....
Byte_i2c_out	<= rd_sh(7 downto 0);
Byte_i2c_latch	<= '1' when Byte_i2c_latch_cell = '1' and Boot_sequence /= Idle else '0';

SDA_ctrl0:process(reset_low_clock,low_clk)
begin
	if reset_low_clock = '0' then
		SDA_reg <= '1';
	elsif rising_edge(low_clk) then
		if ena_low_clk = '1' then
			if	  access_SM = start then
				SDA_reg 	<= '0';
			elsif access_SM = stop and I2C_sequence /= stop_acc then
				SDA_reg 	<= '0';
			elsif tog(0) = '0' and tog(1) = '0' and (I2C_sequence = byte) then
				SDA_reg 	<= sh(7);
			elsif Tog(1) = '1' and I2C_sequence = stop_acc then
				SDA_reg 	<= '1';
			elsif  I2C_sequence = ack then
				SDA_reg 	<= '0';

			end if;
		end if;
	end if;
end process;



last_data_read <= '1' when nb_data_cnt = "000000" else '0';

SDA_bit:process(access_SM,SDA_reg,I2C_sequence,read_access,last_data_read )
begin
        SDA_o <= SDA_reg;
		if access_SM = read_data_ack then
			SDA_o <= last_data_read;
	--	else

	--		SDAo <= SDA_reg;
		end if;

	if (I2C_sequence = Idle or (I2C_sequence = ack and read_access = '0') or access_SM = read_data) then
		SDA_z <= '1'; -- High Impedance active high
	else
		SDA_z <= '0';

	end if;
end process;

end behavioral;
