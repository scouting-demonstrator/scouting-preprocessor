------------------------------------------------------
-- I2C interface for SPF module
-- freq 100 kHz (ext clock x4)
--  Ver 2.00
--  add a bir to remove the stop between a access to set a register to be read and the read access
-- Dominique Gigi March 2017
------------------------------------------------------
--   register send to access the component
--		func(0) bit 31-24  = address of the I2C componennt 
--		func(0) bit 24   '1'  read access ; '0' write access
--		func(0) bit 23-22 RESERVDED
--		func(0) bit 21-20 = number of register offset byte    	
--		func(0) bit 19-17 = number of data byte    
--		func(0) bit 16 = 0 stop between set address and read value  = 1 no-stop between set address and read value   (this is only valid when a read access)
--		func(0) bit 15-0 register offset to be accessed
--

-- 		func(1) bit 31-0 data to be written
--  returned data
--		func(0) bit 17 '1' indicate that previous access had a error (reset by a access request
--		func(0) bit 16 '1' previous access is finished ; data value for read access; a write spend 7 ms;
--		func(1) bit 31-0  data read
--
-- Ex:  A2002356 : execute a write on address 0xA2  register 0x23 data 0x56
--		  A10034xx : execute a read request on address 0xA0  register 0x34
--							a read on DTO will return the data read
------------------------------------------------------
LIBRARY ieee;
-- LIBRARY altera_mf;
-- LIBRARY altera;

USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;



entity i2c_master_32b is

port (
	low_clk			: in std_logic; -- 1.6 MHz
	ena_low_clk		: in std_logic; -- enable the clock to have a clock of 4000 Khz double frequency of the I2C bus (400 KHz --> 800 KHz)
	reset_n		   	: in std_logic;
	clock			: in std_logic;
			
	SCL				: INOUT std_logic;
	SDAi			: IN std_logic;-- separate the IN and the OUT if we want to implement multiple 
	SDAo			: OUT std_logic;
	SDAz			: OUT std_logic;
			
	dti				: in std_logic_vector(31 downto 0);
	cs			 	: in std_logic;
	func			: in std_logic_vector(1 downto 0);
	wen				: in std_logic;
	rd 				: in std_logic;
	dto				: out std_logic_vector(31 downto 0);
	ack_rq			: out std_logic --- use for simulation
	);
	
	
end i2c_master_32b;
architecture behavioral of i2c_master_32b is

attribute mark_debug : string;

 type i2c_type is (			idle,
							start_acc,
							interm_0,
							byte,
							ack,
							stop_acc
							);
signal I2C_sequence: i2c_type;

--ATTRIBUTE ENUM_ENCODING : STRING;
--ATTRIBUTE ENUM_ENCODING OF i2c_type: TYPE IS "000001 000010 000100 001000 010000 100000";

--attribute mark_debug of I2C_sequence: signal is "true";

 type access_type is (		idle,
							start,
							i2c_add,
							i2c_add_ack,
							offset_reg,
							offset_reg_ack,
							wr_data,
							wr_data_ack,
							read_data,
							read_data_ack,
							stop_start,
							stop
							);
signal access_SM: access_type;
--attribute mark_debug of access_SM: signal is "true";

component resync_ena_v4 
	port (
		aresetn			: in std_logic;
		clocki			: in std_logic;
		clocko			: in std_logic;
		input			: in std_logic;
		ena_clock		: in std_logic;
		output			: out std_logic
		);
end component;

component resetp_resync is
port (
	aresetp				: in std_logic;
	clock				: in std_logic; 
	Resetp_sync			: out std_logic
	);
end component;

component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic; 
	Resetn_sync			: out std_logic;
	Resetp_sync			: out std_logic
	);
end component;

signal tog							        : std_logic_vector(1 downto 0);
--attribute mark_debug of tog          : signal is "true";
signal reg							        : std_logic_vector(31 downto 0);
signal sh							        : std_logic_vector(7 downto 0);
signal Start_rq_sync				        : std_logic; 
signal value_cnt					        : std_logic_vector(7 downto 0);
signal load_cnt 					        : std_logic;
signal rd_sh						        : std_logic_vector(31 downto 0);
signal chg_state					        : std_logic;
signal SDA_reg 					         	: std_logic;

signal strobe_acc					        : std_logic; 
signal strobe_acc_rst				        : std_logic; 
signal remove_stop_i2c			          	: std_logic;

signal stop_access				         	: std_logic;

signal data_register				        : std_logic_vector(31 downto 0);
signal nb_data						        : std_logic_vector( 2 downto 0);
signal nb_reg_offset				        : std_logic_vector( 1 downto 0); 
signal nb_data_cnt				          	: std_logic_vector( 2 downto 0);
signal nb_reg_offset_cnt		          	: std_logic_vector( 1 downto 0); 
signal add_register				          	: std_logic_vector(7 downto 0);
signal reg_offset					        : std_logic_vector(15 downto 0);
signal start_stop_btw			          	: std_logic;
signal bit_counter				          	: std_logic_vector(3 downto 0);
signal start_done					        : std_logic;
signal stop_done					        : std_logic;
signal fall_edge					        : std_logic;
signal rise_edge					        : std_logic;
signal nack_acc					          	: std_logic;
signal read_access				          	: std_logic;
signal read_request				          	: std_logic;
signal first_set_offset			          	: std_logic;
signal end_access					        : std_logic;
signal end_access_cell				        : std_logic;
signal error						        : std_logic;

signal I2C_state_ack				        : std_logic;
signal I2C_state_start			          	: std_logic;
signal Access_state_start_stop			 	: std_logic; 
signal last_data_read			          	: std_logic;
signal Sequence_idle                    	: std_logic;
signal Access_done                    		: std_logic;
signal end_access_resync               		: std_logic;
signal reset_low_clock						: std_logic;
signal delay_access_done_1_i2c_clock_start	: std_logic;
signal delay_access_done_1_i2c_clock		: std_logic_vector(1 downto 0);

--attribute mark_debug of end_access          : signal is "true";
--attribute mark_debug of nb_data_cnt       	: signal is "true";
--attribute mark_debug of nack_acc          	: signal is "true";
--attribute mark_debug of fall_edge         	: signal is "true";
--attribute mark_debug of rd_sh	          	: signal is "true";
--attribute mark_debug of error	          	: signal is "true";
--######################################################################
--                 Core start HERE
--######################################################################
begin 
 

ack_rq <= '1' when access_SM = i2c_add_ack or access_SM = offset_reg_ack  or access_SM = wr_data_ack else '0';

--**********************************************
--  command from external interface

process(clock)
begin
if rising_edge(clock) then
	strobe_acc		<= '0';
	if cs = '1' and wen = '1' and func(0) = '1' then
		strobe_acc					<= '1';
		add_register(7 downto 1)	<= dti(31 downto 25);
		add_register(0)				<= '0';
		read_request				<= dti(24);
		nb_reg_offset				<= dti(21 downto 20);
		nb_data						<= dti(19 downto 17);
		reg_offset					<= dti(15 downto 0);
		start_stop_btw 				<= dti(16);
	end if;
	
	if cs = '1' and wen = '1' and func(1) = '1' then
		data_register				<= dti;
	end if;	
end if;
end process;

process(clock)
begin
	if rising_edge(clock) then
		if rd = '1' then
			dto							<= (others => '0');
			if 		func(0) = '1' then
				dto(16) 				<= Access_done;
				dto(17) 				<= error;
			elsif 	func(1) = '1' then
				dto						<= rd_sh;
			end if;
		end if;
	end if;
end process;


--#############################################
-- logic to specify when the bus is available
--  we add a delay of 1 clock I2C cycle 
--  to avoid to have access request to fast after the end of previous access
process(low_clk,reset_low_clock)
begin
	if reset_low_clock = '0' then
		delay_access_done_1_i2c_clock		<= "00";
		delay_access_done_1_i2c_clock_start <= '0';
	elsif rising_edge(low_clk) then
		if   ena_low_clk = '1' then
			if end_access = '1'	then
				delay_access_done_1_i2c_clock_start <= '1';
			elsif  delay_access_done_1_i2c_clock = "11" then
				delay_access_done_1_i2c_clock_start <= '0';
			end if;
			
			if delay_access_done_1_i2c_clock_start = '1' then
				delay_access_done_1_i2c_clock	<= delay_access_done_1_i2c_clock + '1';
			else	
				delay_access_done_1_i2c_clock	<= "00";
			end if;
		end if;
	end if;
end process;
 

end_access_cell <= '1' when delay_access_done_1_i2c_clock = "11" and ena_low_clk = '1' else '0';

resync_sig_i0:resync_ena_v4 
	port map(
		aresetn		=> reset_low_clock,
		clocki		=> low_clk,
		input		=> end_access_cell,
		clocko		=> clock, 
		ena_clock	=> '1',
		output		=> end_access_resync
		);

process(reset_n,clock)
begin
	if reset_n = '0' then	
		Access_done	<= '1';
	elsif rising_edge(clock) then 
		if strobe_acc = '1' then 			-- start the access
			Access_done	<= '0';
		elsif end_access_resync = '1' then 	-- end access
			Access_done	<= '1';
		end if;
	end if;
end process;

--***********************************************

--**********    SCL clock made ******************
toggle:process(Sequence_idle,low_clk)
begin
	if Sequence_idle = '1'  then
		tog 			<= "10";
	elsif rising_edge(low_clk) then
		if ena_low_clk = '1' then
			tog(0)		<= '0';
			if I2C_sequence = start_acc or I2C_sequence = stop_acc then
				tog(0)	<= '1';
			--elsif (I2C_sequence = byte or  I2C_sequence = ack)  then 
			elsif (I2C_sequence = byte or  I2C_sequence = ack) and (SCL = '1' or tog(1) = '0')  then 
				tog(0) 	<= not(Tog(0));
			end if;
			
			if  tog(0) = '1' and (I2C_sequence = byte or  I2C_sequence = ack or I2C_sequence = Interm_0) then 
				tog(1) 	<= not(tog(1));
			elsif I2C_sequence = stop_acc then	
				tog(1)	<= '1';
			end if;
		end if;
	end if;
end process;



-- resync the state before using it as an asynchronous value
process(low_clk)
begin
    if rising_edge(low_clk) then
       Sequence_idle        <= '0';
       if I2C_sequence = idle then
            Sequence_idle 	<= '1';
       end if;
    end if;
end process;

process(tog,access_SM)
begin
		rise_edge 		<= '0';
		if tog(1) = '0' and tog(0) = '1' and access_SM /= Idle then
			rise_edge 	<= '1';
		end if;
		
		fall_edge		<= '0';
		if tog(1) = '1' and tog(0) = '1' and access_SM /= Idle then
			fall_edge 	<= '1';
		end if;
end process;

		
process(low_clk)
begin
	if rising_edge(low_clk) then	
		if ena_low_clk = '1' then
			start_done 		<= '0';
			if access_SM = start and SDAi = '0' and tog(1) = '1' then
				start_done 	<= '1';
			end if;

			stop_done 		<= '0';
			if (access_SM = stop or access_SM = stop_start) and (SDAi = '0' or remove_stop_i2c = '1') and tog(1) = '1' then
				stop_done 	<= '1';
			end if;	
		end if;
	end if;
end process;

chg_state <= '1' when tog(0) = '1' and tog(1) = '1'   else '0';

 
-- Tristate Clock output when Idle
scl_clock:process(I2C_sequence,tog)
begin
	if I2C_sequence = idle then 
		SCL <= 'Z';
	else
		if tog(1) = '1' then
			SCL <= 'Z';
		else
			SCL <= '0';
		end if;
	end if;
end process;
 
 
resync_sigi3:resync_ena_v4 
	port map(
		aresetn		=> reset_n,
		clocki		=> clock,
		input		=> strobe_acc,
		clocko		=> low_clk,-- to be check
		ena_clock	=> ena_low_clk,
		output		=> Start_rq_sync
		);
		
 	
--#### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
-- State machine for bit transfer : bit , ack, start , stop,..

resync_reset_i2:resetn_resync  
port map(
	aresetn			=> reset_n,
	clock			=> low_clk,
	Resetn_sync		=> reset_low_clock 
	);

I2C_clk:process(low_clk,reset_low_clock)
begin
	if reset_low_clock = '0' then
		I2C_sequence 	<= idle;
	elsif rising_edge(low_clk) then 
		if ena_low_clk = '1' then
			Case I2C_sequence is
				when idle =>	
					if Start_rq_sync = '1'  then
						I2C_sequence 	<= start_acc;
					end if;
					
				when start_acc =>
					if Start_done = '1' then
						I2C_sequence 	<= interm_0;
					end if;
					
				when interm_0 =>
					if Tog(1) = '0' and SDAi = '0' then
						I2C_sequence 	<= byte;
					end if;
					
				when byte => 
					if stop_access = '1' then
						I2C_sequence 	<= stop_acc;
					elsif bit_counter = x"1" and fall_edge = '1' then
						I2C_sequence 	<= ack;
					end if;
					
				when ack => 
					if fall_edge = '1' then
						if (nack_acc = '1') or (first_set_offset = '1' and nb_reg_offset_cnt = "00")  then
							I2C_sequence 	<= stop_acc;
						else
							I2C_sequence 	<= byte;
						end if;
					end if;
					
				when stop_acc =>
					if stop_done = '1' then
						if Access_state_start_stop = '1' then
							I2C_sequence <= start_acc;
						else
							I2C_sequence <= idle;
						end if;
					end if;
					
				when others =>
					I2C_sequence <= idle;
			  end case;
		end if;
	end if;
end process; 
 
--#### #### #### #### #### #### #### #### #### #### #### #### #### #### ####
-- State machine for word transfer (Address, data, pointers,...

resync_reset_i1:resetp_resync  
port map(
	aresetp				=>	strobe_acc,
	clock				=>	low_clk,
	Resetp_sync			=>	strobe_acc_rst
	);

-- indicates the end of the access (I2C bus released)	
Access_status:process(low_clk)
begin
	if rising_edge(low_clk) then
			end_access	<= '0';
		if access_SM = stop and stop_done = '1'  then 
			end_access	<= '1';
		end if;
	end if;
end process;

access_clk:process(low_clk,reset_n)
begin
	if reset_n = '0' then
		access_SM			<= idle; 
		error				<= '0';
		stop_access 		<= '0';
		read_access			<= '0';
		first_set_offset	<= '0';	
		nb_data_cnt 		<= (others => '0');
		nb_reg_offset_cnt 	<= (others => '0');
	elsif rising_edge(low_clk) then 
		if ena_low_clk = '1' then
			stop_access 	<= '0';
			read_access		<= '0';
			Case access_SM is
				when idle =>
					nb_reg_offset_cnt <= nb_reg_offset;
					if Start_rq_sync = '1'  then
						first_set_offset		<= '0'; 
						if nb_reg_offset /= "00" and read_request = '1' then
							first_set_offset	<= '1';
						end if;
						access_SM <= start;
					end if;
				
				when start =>
					if I2C_state_start = '1'  and start_done = '1' then
						access_SM 	<= i2c_add;
					end if;
					
				when i2c_add =>
					if I2C_state_ack = '1' then
						access_SM 	<= i2c_add_ack;
					end if;			
					
				when i2c_add_ack =>
					nb_data_cnt 		<= nb_data;
					if fall_edge = '1' then
						if nack_acc = '1' then
							error				<= '1';
							access_SM 			<= stop;
						elsif (nb_reg_offset /= "00" and read_request = '0') or first_set_offset = '1'  then
							access_SM 			<= offset_reg;						
						elsif read_request = '0' and nb_data /= "000" then
							access_SM 			<= wr_data;
						elsif read_request = '1' and nb_data /= "000" then
							access_SM 			<= read_data;
						end if;		
					end if;
					
				when offset_reg =>
					if I2C_state_ack = '1' then
						nb_reg_offset_cnt 	<= nb_reg_offset_cnt - '1';
						access_SM 			<= offset_reg_ack;
					end if;	
				
				when offset_reg_ack =>
					if fall_edge = '1' then
						if nb_reg_offset_cnt /= "00" then
							access_SM 			<= offset_reg;
						elsif first_set_offset = '1' then
							first_set_offset 	<= '0';
							access_SM 			<= stop_start;
						elsif read_request = '0' and nb_data /= "000" then
							access_SM 			<= wr_data;
						elsif read_request = '1' and nb_data /= "000" then
							access_SM 			<= read_data;
						else
							stop_access			<= '1';
							access_SM 			<= stop;
						end if;
					end if;
					
				when wr_data =>
					if I2C_state_ack = '1' then
						nb_data_cnt		<= nb_data_cnt - '1';
						access_SM 		<= wr_data_ack;
					end if;	
					
				when wr_data_ack =>
					if fall_edge = '1' then
						if nack_acc = '1' then
							--error				<= '1'; 
							access_SM 			<= stop;
						elsif nb_data_cnt /= "000"  then
							access_SM 			<= wr_data;
						else
							stop_access			<= '1'; 
							access_SM 			<= stop;
						end if;		
					end if;			
					
				when stop_start =>
					stop_access		<= '1';
					if stop_done = '1' then
						access_SM 	<= start;
					end if;
					
				when read_data =>
					read_access		<= '1';
					if  I2C_state_ack = '1' then
						nb_data_cnt	<= nb_data_cnt - '1';
						access_SM 	<= read_data_ack;
					end if;	
					
				when read_data_ack =>
					read_access		<= '1';
					if fall_edge = '1' then
						-- if nack_acc = '1' then
							-- access_SM 			<= stop;
							-- error					<= '1';
						-- els
						if nb_data_cnt /= "000"  then
							access_SM 		<= read_data;
						else
							stop_access		<= '1'; 
							access_SM 		<= stop;
						end if;		
					end if;	
					
				when stop =>		
					if stop_done = '1' then
						access_SM <= idle;
					end if;
					
				when others =>
					access_SM <= idle;
				end case;
			end if;
		end if;
end process;
Access_state_start_stop <= '1' when  access_SM = stop_start else '0';


I2C_state_ack			<= '1' when I2C_sequence = ack  else '0';
I2C_state_start			<= '1' when I2C_sequence = start_acc else '0';

process(reset_low_clock,low_clk) 
begin
	if reset_low_clock = '0' then
		bit_counter	<= (others => '0');
	elsif rising_edge(low_clk) then 
	 	if ena_low_clk = '1' then
			if I2C_sequence  = start_acc or  I2C_sequence  = ack then
				bit_counter 	<= x"8";
			elsif  I2C_sequence  = byte and fall_edge = '1' then
				bit_counter 	<= bit_counter - '1';
			end if;
		end if;
	end if;
end process;

--############################
process(reset_low_clock,low_clk)
begin
	if reset_low_clock = '0' then
		remove_stop_i2c	<= '0';
	elsif rising_edge(low_clk) then 
	 	if ena_low_clk = '1' then
			if I2C_sequence = start_acc then	
				if remove_stop_i2c =  '1' then
					remove_stop_i2c <=  '0';
				elsif   start_stop_btw = '1' then
					remove_stop_i2c <=  '1';
				end if;	
			end if;
		end if;
	end if;
end process;

--############################


process(low_clk,reset_low_clock)
begin
	if reset_low_clock = '0' then
		nack_acc	<= '0';
	elsif rising_edge(low_clk) then 
 		if ena_low_clk = '1' then
			if (access_SM = i2c_add_ack or access_SM = offset_reg_ack or access_SM = wr_data_ack) and fall_edge = '1' then
				if SDAi = '1' then
					nack_acc <= '1';
				end if;
			elsif access_SM = idle then
				nack_acc <= '0';
			end if;
		end if;
	end if;
end process;


SDA_ctrl:process(low_clk)
begin
	if rising_edge(low_clk) then
		if ena_low_clk = '1' then	 
			if	 	access_SM = start then
				sh 			<= add_register;
				if read_request = '1' and nb_reg_offset_cnt = "00" then
					sh(0) 	<= '1';
				end if;
			elsif I2C_sequence = ack then
				if nb_reg_offset_cnt /= "00"  then
					if nb_reg_offset_cnt = "10" then
						sh			<= reg_offset(15 downto 8);
					else 
						sh			<= reg_offset(07 downto 0);
					end if;
				elsif read_request = '0' and nb_data_cnt /= "000" then
					if nb_data_cnt = "100" then
						sh			<= data_register(31 downto 24);
					elsif nb_data_cnt = "011" then
						sh			<= data_register(23 downto 16);
					elsif nb_data_cnt = "010" then
						sh			<= data_register(15 downto 08);
					else  
						sh			<= data_register(07 downto 00);
					end if;
				end if;
			
			elsif tog(0) = '0' and tog(1) = '0' and (I2C_sequence = byte) then
				if I2C_sequence = byte then
					sh(7 downto 1) 	<= sh(6 downto 0);
					sh(0)			<= '0';
				end if;
			end if;
			
			--read bit from I2C bus
			if access_SM = read_data then
				if I2C_sequence = byte and rise_edge = '1' then
					rd_sh(31 downto 1) 	<= rd_sh(30 downto 0);
					rd_sh(0) 			<= SDAi;
				end if;
			end if;
 
		end if;
	end if;
end process;

SDA_ctrl0:process(reset_low_clock,low_clk)
begin
	if reset_low_clock = '0' then
		SDA_reg <= '1';
	elsif rising_edge(low_clk) then
		if ena_low_clk = '1' then	 
			if	 	access_SM = start then
				SDA_reg 	<= '0';
			elsif access_SM = stop and I2C_sequence /= stop_acc then
				SDA_reg 	<= '0';
			elsif tog(0) = '0' and tog(1) = '0' and (I2C_sequence = byte) then
				SDA_reg 	<= sh(7);
			elsif Tog(1) = '0' and Tog(0) = '0' and I2C_sequence = stop_acc then
				SDA_reg 	<= (start_stop_btw);
			elsif Tog(1) = '1' and I2C_sequence = stop_acc then
				SDA_reg 	<= '1';
			elsif  read_access = '1' then	
				SDA_reg 	<= '0';
			end if;
		end if;
	end if;
end process;

last_data_read <= '1' when nb_data_cnt = "000" else '0';

SDA_bit:process(access_SM,SDA_reg,I2C_sequence,read_access,remove_stop_i2c,last_data_read )
begin
SDAo <= SDA_reg;
	if (I2C_sequence = Idle or (I2C_sequence = ack and read_access = '0') or access_SM = read_data) or (access_SM = stop_start and remove_stop_i2c = '1') then
		SDAz <= '0'; -- High Impedance active low
	else
		SDAz <= '1';
		if access_SM = read_data_ack then
			SDAo <= last_data_read;
	--	else
	
	--		SDAo <= SDA_reg;
		end if;
	end if;	
end process;
 
end behavioral;
