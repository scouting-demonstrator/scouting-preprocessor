library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.datatypes.all;





entity freq_measure_base_array is
    generic (
        NCLOCKS     : natural;
        freq_used   : std_logic_vector(31 downto 0)
    );
    port (
        clk_base                                    : in  std_logic;
        clk_measure                                 : in  std_logic_vector(NCLOCKS - 1 downto 0);
        freq_measure                                : out TBus32b(NCLOCKS - 1 downto 0)
    );
end freq_measure_base_array;





architecture Behavioral of freq_measure_base_array is
begin


    ---- measure clk's frequencies
    freq_measure_loop : for i in freq_measure'range generate
        freq_measure_i : entity work.freq_measure_base
            generic map (
                freq_used   => freq_used
            )
            port map (
                resetn      => '1',
                sysclk      => clk_measure(i),
                base_clk    => clk_base,
                frequency   => freq_measure(i)
            );
    end generate freq_measure_loop;

end Behavioral;
