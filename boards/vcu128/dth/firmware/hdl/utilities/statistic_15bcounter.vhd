----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.08.2017 13:46:18
-- Design Name: 
-- Module Name: statistic_15bcounter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity statistic_15bcounter is
    Port ( 
    	reset : in STD_LOGIC;
		clock : in STD_LOGIC;
  		ena : in STD_LOGIC;
 		values : out STD_LOGIC_VECTOR (15 downto 0)
 		);
end statistic_15bcounter;

architecture Behavioral of statistic_15bcounter is

signal counter	: std_logic_vector(15 downto 0);
signal full		: std_logic;

--#############################################################################
-- Code start here
--#############################################################################
begin

process(reset,clock) 
begin
	if reset = '1' then
		counter 	<= (others => '0');
		full 		<= '0';
	elsif rising_edge(clock) then
		if ena = '1' then
			counter <= counter + '1';
		end if;
		
		if counter = x"8000" then
			full <= '1';
		end if;
	end if;
end process;


values(14 downto 0) 	<= counter(14 downto 0);
values(15) 				<= full;

end Behavioral;
