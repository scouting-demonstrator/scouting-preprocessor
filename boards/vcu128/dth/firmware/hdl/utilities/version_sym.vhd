-- /////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\
--***********   VERSION FILE   ******************
--\\\\\\\\\\\\\\\\\\\\\\/////////////////////////

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY version_comp IS
	PORT
	(
		VERSION		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
END version_comp;

ARCHITECTURE BEHAVIORAL OF version_comp IS
BEGIN
	VERSION <= x"43710002";
END BEHAVIORAL;


--  437 is DTH DAQ FPGA
--     0 Golden design
--     1 DTH_xSRs_xSRr_1DAQ
--	   2 

-- version .......
--
--*****************************************************************
-- version "00000000";
-- First version
--*****************************************************************  
--43710001->Thu 05 Mar 2020 15:48 Change some address offset
--43710002->Mon 20 Apr 2020 09:08 Compile with last SR version (Master/Slave share clocks)
