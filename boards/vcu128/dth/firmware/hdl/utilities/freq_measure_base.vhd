-------------------------------------------------------------------------------
-- Frequency Clock detection
--
--  Ver 1.1
--
-- Dominique Gigi Jan 2015
-- Rocco Ardino Sept 2022
-------------------------------------------------------------------------------
--   Measure the clock frequency of selected clock
--
--
--
-------------------------------------------------------------------------------
library ieee;
library work;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;





entity freq_measure_base is
    generic (
        freq_used   : std_logic_vector(31 downto 0) := x"07735940"
    );
    port (
        resetn      : in std_logic;
        sysclk      : in std_logic;                     -- clock whose freq has to be measured
        base_clk    : in std_logic;                     -- clock base used to measure the sysclk freq
        frequency   : out std_logic_vector(31 downto 0) -- freq measure
    );
end freq_measure_base;





architecture behavioral of freq_measure_base is

    ---------------------------------------------------------------------------
    --
    -- Components declaration
    --
    ---------------------------------------------------------------------------
    component resetn_resync is
    port (
        aresetn     : in std_logic;
        clock       : in std_logic;

        Resetn_sync : out std_logic
        );
    end component;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Signals declaration
    --
    ---------------------------------------------------------------------------
    signal counter_base     : std_logic_vector(31 downto 0);
    signal counter_measure  : std_logic_vector(31 downto 0);
    signal measure          : std_logic_vector(31 downto 0);
    signal latch_value      : std_logic;
    signal reset_cnt        : std_logic;
    signal reset_cnt_rsyc   : std_logic;
    ---------------------------------------------------------------------------

begin

    ---- counter base clk
    process(base_clk)
    begin
        if rising_edge(base_clk) then
            -- check in versioning file to know with freq base is used
            latch_value <= '0';
            if counter_base = freq_used then
                counter_base <= (others => '0');
                latch_value  <= '1';
            else
                counter_base <= counter_base + '1';
            end if;
        end if;
    end process;



    ---- counter measure
    process(sysclk,reset_cnt_rsyc)
    begin
        if reset_cnt_rsyc = '0' then
            counter_measure <= (others => '0');
        elsif rising_edge(sysclk) then
            counter_measure <= counter_measure + '1';
        end if;
    end process;



    ---- resync reset counter to sysclk domain
    resync_reset_i1 : resetn_resync
        port map(
            aresetn     => reset_cnt,
            clock       => sysclk,
            Resetn_sync => reset_cnt_rsyc
        );



    ---- latch the frequency
    process(base_clk)
    begin
        if rising_edge(base_clk) then
            reset_cnt <= '1'; -- reset the counter measure when the measure is latched
            if latch_value = '1' then
                measure   <= counter_measure;
                reset_cnt <= '0';
            end if;
        end if;
    end process;



    ---- connect measurement to output port "frequency"
    frequency <= measure;

end behavioral;
