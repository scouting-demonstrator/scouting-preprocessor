--######################################################################################
--
--				Pulse generator for LED activity
--
-- 			This file contains: 
--						-  
--						 
--						 
--
--
--######################################################################################
--
--		D. Gigi  PH/CMD   Augist 2016
--
--######################################################################################
--  Modif:	Date  change
--  this component hasn't been tested for the metastability
-- pulse time is : 2500 ns (400KHZ for low_clock) * 16384 => ~0.1 sec pulse cycle
--
--######################################################################################

LIBRARY ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
 

--#############################   ENTITY     ###################################
entity pulse_led is

port (

	reset			: in std_logic;
	fast_clock	    : in std_logic;
	ena_clock	    : in std_logic;
	activity		: in std_logic;
	
	led_pulse	    : out std_logic
);
end pulse_led ;

--#############################   ARCHITECTURE     ###################################
architecture behavioral of pulse_led is

signal counter 			: std_logic_vector(15 downto 0);
signal mem_pulse		: std_logic;
signal resync_mem_pulse	: std_logic;
 
signal flip_ON_OFF		: std_logic;


--######################################################################################
--#################          CODE           START        HERE          #################
--######################################################################################
begin

-- receive a pulse and memorise it
process(activity,flip_ON_OFF)
begin
	if flip_ON_OFF = '1' then
		mem_pulse	<= '0';
	elsif rising_edge(activity)  then
		mem_pulse	<= '1';
	end if;
end process;

led_pulse	<= not(mem_pulse);

-- create a symetric pulse of 2^14 clocks cycles (detirmine the LED pulse cylce time)
process(fast_clock)
begin
	if rising_edge(fast_clock) then
		if ena_clock = '1' then
			resync_mem_pulse	<= mem_pulse;
			
			if counter(14) = '1' then
				if flip_ON_OFF = '1' then	
					flip_ON_OFF <= '0';
				elsif resync_mem_pulse = '1' then
					flip_ON_OFF <= '1';
				end if;
			end if;
		end if;
	end if;
end process;

--counter 
process(fast_clock,reset)
begin
	if reset = '0' then
		counter 		<= (others => '0');
	elsif rising_edge(fast_clock) then
		if ena_clock = '1' then
			if counter(14) = '1' then
				counter 		<= (others => '0');
			elsif resync_mem_pulse = '1' or flip_ON_OFF = '1' then
				counter <= counter + '1';
			end if;
		end if;
	end if;
end process;

end behavioral;