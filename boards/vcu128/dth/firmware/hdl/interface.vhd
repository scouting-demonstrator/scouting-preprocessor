library IEEE;
library WORK;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

--this address should be multiply by 4 to be byte aligned
-- the selection chip are 16#0 ...16#3FFF   64 bit addressing
package interface is

    constant Orbit_Header_indic     : std_logic_vector(15 downto 0):= x"4F48";
    constant Fragment_Header_indic  : std_logic_vector(15 downto 0):= x"4648";

    constant number_of_HBM_port     : integer := 14;
    constant number_of_port         : integer := 12;

    type TCP_port_number is array (0 to 255) of std_logic_vector(15 downto 0);

    --################################################################################################################################
    type rx_usr_record is
        record
            data_bus    : std_logic_vector(127 downto 0);
            sopckt      : std_logic;
            data_ena    : std_logic;
            eopckt      : std_logic;
            err_pckt    : std_logic;
            empty_pckt  : std_logic_vector(3 downto 0);
        end record;

    type tx_usr_record is
        record
            data_bus    : std_logic_vector(127 downto 0);
            sopckt      : std_logic;
            data_ena    : std_logic;
            eopckt      : std_logic;
            err_pckt    : std_logic;
            empty_pckt  : std_logic_vector(3 downto 0);
        end record;

    type rx_user_ift is array (0 to 3) of rx_usr_record;
    type tx_user_ift is array (0 to 3) of tx_usr_record;



    --################################################################################################################################
    -- record for signals Coming from SlinkRocket

    type packager_tdata is array (natural range <>) of std_logic_vector(255 downto 0);
    type packager_tlast is array (natural range <>) of std_logic;
    type packager_tvalid is array (natural range <>) of std_logic;
    type packager_tready is array (natural range <>) of std_logic;
    type packager_tkeep is array (natural range <>) of std_logic_vector(31 downto 0);
    type packager_tuser is array (natural range <>) of std_logic_vector(1 downto 0);
    type packager_tcounter is array (natural range <>) of unsigned(63 downto 0);
    type packager_tbit is array (natural range <>) of std_logic;

    type SR_rcv_data_record is
        record
            data_for_HBM_mem    : std_logic_vector(255 downto 0);
            size_for_HBM_mem    : std_logic_vector(11 downto 0);
            HD_space_in_HBM     : std_logic_vector(2 downto 0);
            ready_for_HBM_mem   : std_logic;
        end record;

    type SR_rcv_ctrl_record is
        record
            read_for_HBM_mem        : std_logic;
            Block_read_done_for_HBM : std_logic;
        end record;

    type TStream_HBM_data is array (natural range <>) of SR_rcv_data_record;
    type TStream_HBM_ctrl is array (natural range <>) of SR_rcv_ctrl_record;
    type SR_rcv_data_hlf is array (0 to number_of_HBM_port-1) of SR_rcv_data_record;
    type SR_rcv_ctrl_hlf is array (0 to number_of_HBM_port-1) of SR_rcv_ctrl_record;



    --################################################################################################################################
    -- record for signals between Ethernet and HBM

    type TCP_req_record is
        record
            TCP_wen_rq_Send_pkt : std_logic;
            TCP_SEQ_Send        : std_logic_vector(31 downto 0);
            TCP_ACK_Send        : std_logic_vector(31 downto 0);
            TCP_FLAG_Send       : std_logic_vector(7 downto 0);
            TCP_option_Send     : std_logic_vector(2 downto 0);
            TCP_Length_Send     : std_logic_vector(31 downto 0);
            TCP_pckt_wo_dt      : std_logic;
            TCP_wen_rq_ack_pkt  : std_logic;
            TCP_SEQ_R_Ack_Pkt   : std_logic_vector(31 downto 0);
            TCP_ACK_R_Ack_Pkt   : std_logic_vector(31 downto 0);
            TCP_FLAG_R_Ack_Pkt  : std_logic_vector(7 downto 0);
            TCP_Opt_R_Ack_Pkt   : std_logic_vector(2 downto 0);
            --TCP_Px_request_ack                : std_logic;  will be out of rht record because record can have only 1 direction
        end record;

    type TCP_req_pipe is array (0 to 0) of TCP_req_record;

    -- record to transfert the data block with Eth parameters to DAQ module
    type HBM_to_TCP_block_record is
        record
            Request_TCP_packet  : std_logic;
            Sel_TCP_Stream      : std_logic_vector(7 downto 0);
            TCP_SEQ_Send        : std_logic_vector(31 downto 0);
            TCP_ACK_Send        : std_logic_vector(31 downto 0);
            TCP_FLAG_Send       : std_logic_vector(7 downto 0);
            TCP_option_Send     : std_logic_vector(2 downto 0);
            TCP_Length_Send     : std_logic_vector(31 downto 0);
            TCP_max_size        : std_logic_vector(15 downto 0);
            TCP_scale           : std_logic_vector( 7 downto 0);
            TCP_win             : std_logic_vector(15 downto 0);
            TCP_TS              : std_logic_vector(31 downto 0);
            TCP_TS_rply         : std_logic_vector(31 downto 0);
            TCP_pckt_wo_dt      : std_logic;
            HBM_PL_data         : std_logic_vector(511 downto 0);
            HBM_PL_ena          : std_logic_vector(3 downto 0);
            HBM_PL_last         : std_logic;
            HBM_blk_CS          : std_logic_vector(15 downto 0);
            HBM_blk_wen         : std_logic;
        end record;

    type HBM_to_TCP_block_pipe is array (number_of_HBM_port-1 downto 0) of HBM_to_TCP_block_record;

    -- record to send the packet received from Eth to TCP logic (should selected by the port #
    type TCP_ack_rcvo_record is
        record
            TCP_Px_ack_num_rcv      : std_logic_vector(31 downto 0);
            TCP_Px_seq_num_rcv      : std_logic_vector(31 downto 0);
            TCP_Px_win_num_rcv      : std_logic_vector(15 downto 0);
            TCP_Px_flag_num_rcv     : std_logic_vector(7 downto 0);
            TCP_Px_dt_len_rcv       : std_logic_vector(15 downto 0);
            TCP_Px_new_Scale_rcv    : std_logic_vector(7 downto 0);
            TCP_Px_update_Scale_rcv : std_logic;
            TCP_Px_new_MMS_rcv      : std_logic_vector(15 downto 0);
            TCP_Px_update_MMS_rcv   : std_logic;
            TCP_Px_Port_sel_rcv     : std_logic_vector(11 downto 0);
        end record;

    type TCP_ack_rcvo_type    is array (0 to number_of_HBM_port-1) of TCP_ack_rcvo_record;
    type TCP_ack_rcvo_type_x4 is array (0 to 3) of TCP_ack_rcvo_record;



    --################################################################################################################################
    -- record for HBM access

    type HBM_bus_256_bit is array (0 to number_of_HBM_port-1) of std_logic_vector(255 downto 0);
    type HBM_bus_32_bit  is array (0 to number_of_HBM_port-1) of std_logic_vector(31 downto 0);
    type HBM_bus_6_bit   is array (0 to number_of_HBM_port-1) of std_logic_vector(5 downto 0);
    type HBM_bus_1_bit   is array (0 to number_of_HBM_port-1) of std_logic;

    type HBM_write_req_record is
        record
            write_req   : std_logic;
            WrID        : std_logic_vector(5 downto 0);
            H_ADD_wr    : std_logic_vector(4 downto 0);
            address_wr  : std_logic_vector(27 downto 0);
            data_i      : std_logic_vector(255 downto 0);
            last_data_i : std_logic;
            write_val   : std_logic;
            wrwd_length : std_logic_vector(3 downto 0); -- indicates the number of words (256 bits)
            word_enable : std_logic_vector(1 downto 0); -- strobe 128-bit access
        end record;

    type HBM_write_ack_record is
        record
            write_ack       : std_logic;
            write_ready     : std_logic;
            WrID_return     : std_logic_vector(5 downto 0);
            Wr_burst_done   : std_logic;
        end record;

    type HBM_write_req_type is array (0 to number_of_HBM_port-1)   of HBM_write_req_record;
    type HBM_write_ack_type is array (0 to number_of_HBM_port-1)   of HBM_write_ack_record;
    type HBM_write_req_hlf  is array (0 to number_of_HBM_port/2-1) of HBM_write_req_record;
    type HBM_write_ack_hlf  is array (0 to number_of_HBM_port/2-1) of HBM_write_ack_record;

    type HBM_read_req_record    is
        record
            read_req    : std_logic;
            H_ADD_rd    : std_logic_vector(4 downto 0);
            address_rd  : std_logic_vector(27 downto 0);
            RdID        : std_logic_vector(5 downto 0);
            rdwd_length : std_logic_vector(3 downto 0);
        end record;

    type HBM_read_ack_record is
        record
            data_o          : std_logic_vector(255 downto 0);
            read_ack        : std_logic;
            rd_data_last    : std_logic;
            Rd_error        : std_logic_vector(3 downto 0);
            rd_valid_data   : std_logic;
            RdID_return     : std_logic_vector(5 downto 0);
        end record;

    type HBM_read_req_type is array (0 to number_of_HBM_port-1)   of HBM_read_req_record;
    type HBM_read_ack_type is array (0 to number_of_HBM_port-1)   of HBM_read_ack_record;
    type HBM_read_req_hlf  is array (0 to number_of_HBM_port/2-1) of HBM_read_req_record;
    type HBM_read_ack_hlf  is array (0 to number_of_HBM_port/2-1) of HBM_read_ack_record;

    -- Specify the HBM hign address used per the SR
    type HBM_Sx is array (0 to 13) of integer;
    constant HBM_S0 : HBM_Sx := (0,1,2,3, 4,5,6,7, 8,9,10,11, 12,13);-----------------------------OFFSET--------------------


end package;
