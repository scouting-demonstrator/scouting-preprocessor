library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use work.address_table.all;
use work.interface.all;





entity DAQ_module is
    generic (
        QSFP_port_num           : std_logic_vector(7 downto 0);
        addr_offset_100g_eth    : integer := 0
    );
    port (
        clock_low               : in std_logic;                         -- low clock ~1MHz for ARP probe
        MAC_Card                : in std_logic_vector(47 downto 0);     -- MAC address of port
        MAC_Card_P0             : in std_logic_vector(47 downto 0);     -- MAC address of port 00 (reference)
        Ld_MAC_add              : in std_logic;

        -- PCIe local interface
        usr_clk                 : in  std_logic;
        rst_usr_clk_n           : in  std_logic;
        usr_func_wr             : in  std_logic_vector(16383 downto 0);
        usr_wen                 : in  std_logic;
        usr_data_wr             : in  std_logic_vector(63 downto 0);

        usr_func_rd             : in  std_logic_vector(16383 downto 0);
        usr_rden                : in  std_logic;
        usr_dto                 : out std_logic_vector(63 downto 0);

        -- TCP <--> HBM
        ETH_Clock               : out std_logic;

        Usr_resetp_TCP_clock    : in  std_logic;
        TCP_clock               : in  std_logic;
        TCP_ack_rcvo            : out TCP_ack_rcvo_record;              -- receive ACK packet
        TCP_Px_Ack_present      : out std_logic;
        rd_TCP_Px_val_rcvo      : in  std_logic;

        TCP_BLOCK_TO_SEND       : in  HBM_to_TCP_block_record;
        HBM_ack_packet          : out std_logic;
        TCP_packet_done         : out std_logic;

        resetp_counter_out      : out std_logic;

        -- QSFP connection
        QSFP_RX_P               : in  std_logic_vector(3 downto 0);
        QSFP_RX_N               : in  std_logic_vector(3 downto 0);
        QSFP_TX_P               : out std_logic_vector(3 downto 0);
        QSFP_TX_N               : out std_logic_vector(3 downto 0);

        -- SERDES/MAC
        SERDES_reset_p          : in  std_logic;
        init_clock              : in  std_logic;
        GT_ref_clock_p          : in  std_logic;
        GT_ref_clock_n          : in  std_logic;
        gt_ref_clk_out          : out std_logic;                        -- bring mgt clock out
        status_out              : out std_logic_vector(31 downto 0)
    );
end DAQ_module;





architecture Behavioral of DAQ_module is


    component resync_v4
        port (
            aresetn : in  std_logic;
            clocki  : in  std_logic;
            clocko  : in  std_logic;
            input   : in  std_logic;
            output  : out std_logic
            );
    end component;

    component resetp_resync is
        port (
            aresetp     : in std_logic;
            clock       : in std_logic;
            Resetp_sync : out std_logic
        );
    end component;


    signal error_detect             : std_logic;

    signal resetp_mem               : std_logic;
    signal resetn_mem               : std_logic;

    signal rx_lbus                  : rx_user_ift;
    signal mac_rx_clk               : std_logic;
    signal tx_lbus                  : tx_user_ift;
    signal tx_lbus_ready            : std_logic;
    signal mac_tx_clk               : std_logic;

    signal TX_lbus_in               : tx_user_ift;
    signal TX_lbus_wr               : std_logic;
    signal TX_pack_ready            : std_logic;
    signal TX_pckt_ready            : std_logic;
    signal TX_FIFO_Full             : std_logic;

    signal dto_serdes               : std_logic_vector(63 downto 0);
    signal dto_eth                  : std_logic_vector(63 downto 0);
    signal dto_slink_merger         : std_logic_vector(63 downto 0);
    signal dto_data_manager0        : std_logic_vector(63 downto 0);

    signal trigger_reg              : std_logic;
    signal rst_usr_clk_p            : std_logic;
    signal Usr_resetp_MAC_TX_clock  : std_logic;

    signal MAC_itf_ready            : std_logic;

    attribute mark_debug : string;

begin

    rst_usr_clk_p <= not(rst_usr_clk_n);



    resync_reset_i2 : resetp_resync
        port map(
            aresetp     => rst_usr_clk_p,
            clock       => mac_tx_clk,
            Resetp_sync => Usr_resetp_MAC_TX_clock
        );



    --==========================================================================
    -- Common Ethernet builder
    ethernet_i0 : entity work.ethernet_block
        generic map(
            addr_offset_100G_eth    => addr_offset_100G_eth,
            QSFP_port_num           => QSFP_port_num
        )
        port map(
            clock_low               => clock_low,               -- low clock ~1MHz for ARP probe
            MAC_Card                => MAC_Card,
            MAC_Card_P0             => MAC_Card_P0,
            Ld_MAC_add              => Ld_MAC_add,
            MAC_itf_ready           => MAC_itf_ready,

            ----------------------------------------------------------------
            -- signals used to requets a data packet to be sent over ethernet
            Usr_resetp_TCP_clock    => Usr_resetp_TCP_clock,
            TCP_clock               => TCP_clock,
            TCP_ack_rcvo            => TCP_ack_rcvo,
            TCP_Px_Ack_present      => TCP_Px_Ack_present,
            rd_TCP_Px_val_rcvo      => rd_TCP_Px_val_rcvo,

            TCP_BLOCK_TO_SEND       => TCP_BLOCK_TO_SEND,
            HBM_ack_packet          => HBM_ack_packet,
            TCP_packet_done         => TCP_packet_done,
            Backpressure            => TX_FIFO_Full,

            ----------------------------------------------------------------
            -- Control bus
            usr_clk                 => usr_clk,
            usr_rst_n               => rst_usr_clk_n,
            usr_func_wr             => usr_func_wr,
            usr_wen                 => usr_wen,
            usr_data_wr             => usr_data_wr,
            usr_func_rd             => usr_func_rd,
            usr_rden                => usr_rden,
            usr_data_rd             => dto_eth,

            ----------------------------------------------------------------
            -- signals to Ethernet part (TCP_clock)
            TX_lbus_in              => TX_lbus_in,
            TX_lbus_wr              => TX_lbus_wr,
            TX_pack_ready           => TX_pckt_ready,
            ----------------------------------------------------------------
            -- signals to and from SERDES MAC 100G RX_lBus clocks
            rx_lbus                 => rx_lbus,
            rx_lbus_clk             => mac_rx_clk,

            resetp_counter_out      => resetp_counter_out
    );



    buf_packet_i1 : entity work.buffer_to_TX_lbus
        port map(
            resetp_clock_in     => Usr_resetp_TCP_clock,
            clock_in            => TCP_clock,
            TX_lbus_in          => TX_lbus_in,
            TX_lbus_wr          => TX_lbus_wr,
            TX_pckt_ready       => TX_pckt_ready,
            TX_FIFO_Full        => TX_FIFO_Full,

            resetp_clock_out    => Usr_resetp_MAC_TX_clock,
            clock_out           => mac_tx_clk,
            tx_lbus             => tx_lbus,
            TX_ready            => tx_lbus_ready
        );



    --=================================================================
    -- Common SERDES  for 1 100G ethernet link
    serdes_i1 : entity work.Hundred_gb_core
        generic map(
            addr_offset_100G_eth    => addr_offset_100G_eth,
            QSFP_port_num           => QSFP_port_num
        )
        port map(
            init_clk                => init_clock,
            Serdes_Reset_p          => SERDES_reset_p,
            rx_lbus                 => rx_lbus,
            mac_rx_clk              => mac_rx_clk,
            tx_lbus                 => tx_lbus,
            tx_lbus_ready           => tx_lbus_ready,
            Usr_resetp_MAC_TX_clock => Usr_resetp_MAC_TX_clock,
            mac_tx_clk              => mac_tx_clk,

            usr_clk                 => usr_clk,
            usr_rst_n               => rst_usr_clk_n,
            usr_func_wr             => usr_func_wr,
            usr_wen                 => usr_wen,
            usr_data_wr             => usr_data_wr,

            usr_func_rd             => usr_func_rd,
            usr_rden                => usr_rden,
            usr_data_rd             => dto_serdes,

            QSFP_TX4_N              => QSFP_TX_N(3),
            QSFP_TX4_P              => QSFP_TX_P(3),
            QSFP_TX3_N              => QSFP_TX_N(2),
            QSFP_TX3_P              => QSFP_TX_P(2),
            QSFP_TX2_N              => QSFP_TX_N(1),
            QSFP_TX2_P              => QSFP_TX_P(1),
            QSFP_TX1_N              => QSFP_TX_N(0),
            QSFP_TX1_P              => QSFP_TX_P(0),
            QSFP_RX4_P              => QSFP_RX_P(3),
            QSFP_RX4_N              => QSFP_RX_N(3),
            QSFP_RX3_P              => QSFP_RX_P(2),
            QSFP_RX3_N              => QSFP_RX_N(2),
            QSFP_RX2_P              => QSFP_RX_P(1),
            QSFP_RX2_N              => QSFP_RX_N(1),
            QSFP_RX1_P              => QSFP_RX_P(0),
            QSFP_RX1_N              => QSFP_RX_N(0),
            GT_ref_clock_p          => GT_ref_clock_p,
            GT_ref_clock_n          => GT_ref_clock_n,
            gt_ref_clk_out          => gt_ref_clk_out,          -- bring mgt clock out
            MAC_ready               => MAC_itf_ready,
            status_out              => status_out
        );

    ETH_Clock <= mac_tx_clk;

    usr_dto <= dto_serdes or
               dto_eth or
               dto_slink_merger or
               dto_data_manager0;

end Behavioral;