-- Name                                 SubName                                 Data_Mask               read_write      Comment
-- Setting                              Setting_b0                              xffffffffffffffff       rw
--------------------------------------------------------------------------------------------------------------------------------------
ugmt_scouting_ctrl_register_offset      ugmt_scouting_ctrl_register_offset      x00000000ffffffff       r

ugmt_stream_enable_mask                 ugmt_stream_enable_mask                 x0000000000ffffff       rw
ugmt_local_reset                        ugmt_local_reset                        x0000000000000001       rw
ugmt_link_map_00                        ugmt_link_map_00                        x000000000000001f       rw
ugmt_link_map_01                        ugmt_link_map_01                        x000000000000001f       rw
ugmt_link_map_02                        ugmt_link_map_02                        x000000000000001f       rw
ugmt_link_map_03                        ugmt_link_map_03                        x000000000000001f       rw
ugmt_disable_zs                         ugmt_disable_zs                         x0000000000000001       rw
ugmt_orbits_per_packet                  ugmt_orbits_per_packet                  x000000000000ffff       rw
ugmt_orbits_per_chunk                   ugmt_orbits_per_chunk                   x00000000000fffff       rw
ugmt_wait_for_oc1                       ugmt_wait_for_oc1                       x0000000000000001       rw
ugmt_enable_data_gen                    ugmt_enable_data_gen                    x0000000000000001       rw
ugmt_gen_orbit_full_length              ugmt_gen_orbit_full_length              x000000000000ffff       rw
ugmt_gen_orbit_data_length              ugmt_gen_orbit_data_length              x000000000000ffff       rw
ugmt_disable_reshape                    ugmt_disable_reshape                    x0000000000000001       rw
ugmt_scouting_source_id_00              ugmt_scouting_source_id_00              x00000000ffffffff       rw

ugmt_scouting_moni_register_offset      ugmt_scouting_moni_register_offset      x00000000ffffffff       r

ugmt_rx_byte_is_aligned_info            ugmt_rx_byte_is_aligned_info            x0000000000ffffff       r
ugmt_gt_power_good_info                 ugmt_gt_power_good_info                 x0000000000ffffff       r
ugmt_cdr_stable_info                    ugmt_cdr_stable_info                    x000000000000003f       r
ugmt_gt_tx_reset_done_info              ugmt_gt_tx_reset_done_info              x000000000000003f       r
ugmt_gt_rx_reset_done_info              ugmt_gt_rx_reset_done_info              x000000000000003f       r
ugmt_gt_reset_tx_pll_datapath_info      ugmt_gt_reset_tx_pll_datapath_info      x0000000000000001       r
ugmt_gt_reset_tx_datapath_info          ugmt_gt_reset_tx_datapath_info          x0000000000000001       r
ugmt_gt_reset_rx_datapath_info          ugmt_gt_reset_rx_datapath_info          x0000000000000001       r
ugmt_gt_init_done_info                  ugmt_gt_init_done_info                  x000000000000003f       r

ugmt_waiting_for_orbit_end_info         ugmt_waiting_for_orbit_end_info         x0000000000000001       r

ugmt_packager_seen_orbits_00            ugmt_packager_seen_orbits_00            x00000000ffffffff       r

ugmt_packager_dropped_orbits_00         ugmt_packager_dropped_orbits_00         x00000000ffffffff       r

ugmt_orbit_length_bxs_00                ugmt_orbit_length_bxs_00                x00000000ffffffff       r

ugmt_axi_backpressure_seen_info         ugmt_axi_backpressure_seen_info         x0000000000000fff       r
ugmt_orbit_exceeds_size_info            ugmt_orbit_exceeds_size_info            x0000000000000fff       r
ugmt_autorealigns_total                 ugmt_autorealigns_total                 x00000000ffffffff       r

ugmt_algo_version                       ugmt_algo_version                       x0000000000ffffff       r
ugmt_fw_type                            ugmt_fw_type                            x00000000000000ff       r

ugmt_freq_clk_rec_00                    ugmt_freq_clk_rec_00                    x00000000ffffffff       r
ugmt_freq_clk_rec_01                    ugmt_freq_clk_rec_01                    x00000000ffffffff       r
ugmt_freq_clk_rec_02                    ugmt_freq_clk_rec_02                    x00000000ffffffff       r
ugmt_freq_clk_rec_03                    ugmt_freq_clk_rec_03                    x00000000ffffffff       r

ugmt_crc_error_counter_00               ugmt_crc_error_counter_00               x00000000ffffffff       r
ugmt_crc_error_counter_01               ugmt_crc_error_counter_01               x00000000ffffffff       r
ugmt_crc_error_counter_02               ugmt_crc_error_counter_02               x00000000ffffffff       r
ugmt_crc_error_counter_03               ugmt_crc_error_counter_03               x00000000ffffffff       r
