library ieee;
library unisim;
library xpm;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use unisim.vcomponents.all;

use work.top_decl.all;
use work.algo_decl.all;
use work.datatypes.all;
use work.address_table.all;
use work.interface.all;





entity top is
    port(
        clk_100MHz_p        : in std_logic;
        clk_100MHz_n        : in std_logic;
        CLK1_100MHZ_P       : in std_logic;
        CLK1_100MHZ_N       : in std_logic;

        HBM_clk_ref_p       : in std_logic;
        HBM_clk_ref_n       : in std_logic;

        -- pci express
        PCIE_TX0_P          : out std_logic;
        PCIE_TX0_N          : out std_logic;
        PCIE_RX0_P          : in  std_logic;
        PCIE_RX0_N          : in  std_logic;

        PCIE_clk_p          : in  std_logic;
        PCIE_clk_n          : in  std_logic;
        PCIE_rst            : in  std_logic;

        -- leds
        GPIO_LED_0_LS       : out std_logic;
        GPIO_LED_1_LS       : out std_logic;
        GPIO_LED_2_LS       : out std_logic;
        GPIO_LED_3_LS       : out std_logic;
        GPIO_LED_4_LS       : out std_logic;
        GPIO_LED_5_LS       : out std_logic;
        GPIO_LED_6_LS       : out std_logic;
        GPIO_LED_7_LS       : out std_logic;

        -- p00
        QSFP_D_MODSEL       : out std_logic;
        QSFP_D_RESETL       : out std_logic;
        QSFP_D_MODPRSL      : in  std_logic;
        QSFP_D_INTL         : in  std_logic;
        QSFP_D_LPMODE       : out std_logic;

        -- p01
        QSFP_C_MODSEL       : out std_logic;
        QSFP_C_RESETL       : out std_logic;
        QSFP_C_MODPRSL      : in  std_logic;
        QSFP_C_INTL         : in  std_logic;
        QSFP_C_LPMODE       : out std_logic;

        -- p02
        QSFP_B_MODSEL       : out std_logic;
        QSFP_B_RESETL       : out std_logic;
        QSFP_B_MODPRSL      : in  std_logic;
        QSFP_B_INTL         : in  std_logic;
        QSFP_B_LPMODE       : out std_logic;

        -- p03
        QSFP_A_MODSEL       : out std_logic;
        QSFP_A_RESETL       : out std_logic;
        QSFP_A_MODPRSL      : in  std_logic;
        QSFP_A_INTL         : in  std_logic;
        QSFP_A_LPMODE       : out std_logic;

        QSFP_MAIN_SDA       : inout std_logic;
        QSFP_MAIN_SCL       : inout std_logic;

        sysMon_i2c_sda      : inout std_logic;
        sysMon_i2c_sclk     : inout std_logic;

        -- MGT ref clocks
        mgtrefclk0_x0y0_p   : in  std_logic;
        mgtrefclk0_x0y0_n   : in  std_logic;
        mgtrefclk0_x0y1_p   : in  std_logic;
        mgtrefclk0_x0y1_n   : in  std_logic;
        mgtrefclk0_x0y2_p   : in  std_logic;
        mgtrefclk0_x0y2_n   : in  std_logic;
        mgtrefclk0_x0y3_p   : in  std_logic;
        mgtrefclk0_x0y3_n   : in  std_logic;
        mgtrefclk0_x0y4_p   : in  std_logic;
        mgtrefclk0_x0y4_n   : in  std_logic;
        mgtrefclk0_x0y5_p   : in  std_logic;
        mgtrefclk0_x0y5_n   : in  std_logic;
        mgtrefclk0_x0y7_p   : in  std_logic;
        mgtrefclk0_x0y7_n   : in  std_logic;
        mgtrefclk0_x0y8_p   : in  std_logic;
        mgtrefclk0_x0y8_n   : in  std_logic;
        mgtrefclk0_x0y10_p  : in  std_logic;
        mgtrefclk0_x0y10_n  : in  std_logic;
        mgtrefclk0_x0y11_p  : in  std_logic;
        mgtrefclk0_x0y11_n  : in  std_logic;

        -- Serial data ports for gty transceivers
        mgtgty_quad124_rxn_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad124_rxp_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad124_txn_out  : out std_logic_vector(3 downto 0);
        mgtgty_quad124_txp_out  : out std_logic_vector(3 downto 0);

        mgtgty_quad125_rxn_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad125_rxp_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad125_txn_out  : out std_logic_vector(3 downto 0);
        mgtgty_quad125_txp_out  : out std_logic_vector(3 downto 0);

        mgtgty_quad126_rxn_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad126_rxp_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad126_txn_out  : out std_logic_vector(3 downto 0);
        mgtgty_quad126_txp_out  : out std_logic_vector(3 downto 0);

        mgtgty_quad127_rxn_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad127_rxp_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad127_txn_out  : out std_logic_vector(3 downto 0);
        mgtgty_quad127_txp_out  : out std_logic_vector(3 downto 0);

        mgtgty_quad128_rxn_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad128_rxp_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad128_txn_out  : out std_logic_vector(3 downto 0);
        mgtgty_quad128_txp_out  : out std_logic_vector(3 downto 0);

        -- mgtgty_quad129_rxn_in   : in  std_logic_vector(3 downto 0);
        -- mgtgty_quad129_rxp_in   : in  std_logic_vector(3 downto 0);
        -- mgtgty_quad129_txn_out  : out std_logic_vector(3 downto 0);
        -- mgtgty_quad129_txp_out  : out std_logic_vector(3 downto 0);

        mgtgty_quad131_rxn_in   : in  std_logic_vector(3 downto 0); -- p00
        mgtgty_quad131_rxp_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad131_txn_out  : out std_logic_vector(3 downto 0);
        mgtgty_quad131_txp_out  : out std_logic_vector(3 downto 0);

        mgtgty_quad132_rxn_in   : in  std_logic_vector(3 downto 0); -- p01
        mgtgty_quad132_rxp_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad132_txn_out  : out std_logic_vector(3 downto 0);
        mgtgty_quad132_txp_out  : out std_logic_vector(3 downto 0);

        mgtgty_quad134_rxn_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad134_rxp_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad134_txn_out  : out std_logic_vector(3 downto 0);
        mgtgty_quad134_txp_out  : out std_logic_vector(3 downto 0);

        mgtgty_quad135_rxn_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad135_rxp_in   : in  std_logic_vector(3 downto 0);
        mgtgty_quad135_txn_out  : out std_logic_vector(3 downto 0);
        mgtgty_quad135_txp_out  : out std_logic_vector(3 downto 0)
    );
end top;





architecture Behavioral of top is

    ---------------------------------------------------------------------------
    --
    -- Components
    --
    ---------------------------------------------------------------------------
    ---- ILAs for debug
    component ugt_ila
        port (
            clk    : in std_logic;
            probe0 : in std_logic_vector(639 downto 0);
            probe1 : in std_logic_vector( 19 downto 0);
            probe2 : in std_logic_vector( 19 downto 0);
            probe3 : in std_logic_vector(639 downto 0);
            probe4 : in std_logic_vector( 19 downto 0);
            probe5 : in std_logic_vector( 19 downto 0);
            probe6 : in std_logic_vector(255 downto 0);
            probe7 : in std_logic_vector( 20 downto 0);
            probe8 : in std_logic_vector(  0 downto 0)
        );
    end component;

    component calo_ila
        port (
            clk    : in std_logic;
            probe0 : in std_logic_vector(255 downto 0);
            probe1 : in std_logic_vector(  7 downto 0);
            probe2 : in std_logic_vector(  7 downto 0);
            probe3 : in std_logic_vector(255 downto 0);
            probe4 : in std_logic_vector(  7 downto 0);
            probe5 : in std_logic_vector(  7 downto 0);
            probe6 : in std_logic_vector(255 downto 0);
            probe7 : in std_logic_vector( 20 downto 0);
            probe8 : in std_logic_vector(  3 downto 0)
        );
    end component;



    ---- vio for monitoring
    component link_vio
        port (
            clk         : in  std_logic;
            probe_in0   : in  std_logic_vector(0 downto 0);
            probe_in1   : in  std_logic_vector(0 downto 0);
            probe_in2   : in  std_logic_vector(0 downto 0);
            probe_in3   : in  std_logic_vector(3 downto 0);
            probe_in4   : in  std_logic_vector(7 downto 0);
            probe_in5   : in  std_logic_vector(7 downto 0);
            probe_in6   : in  std_logic_vector(7 downto 0);
            probe_in7   : in  std_logic_vector(0 downto 0);
            probe_in8   : in  std_logic_vector(0 downto 0);
            probe_in9   : in  std_logic_vector(7 downto 0);
            probe_in10  : in  std_logic_vector(0 downto 0);
            probe_in11  : in  std_logic_vector(7 downto 0);
            probe_in12  : in  std_logic_vector(14 downto 0);
            probe_in13  : in  std_logic_vector(14 downto 0);
            probe_in14  : in  std_logic_vector(14 downto 0);
            probe_in15  : in  std_logic_vector(14 downto 0);
            probe_in16  : in  std_logic_vector(14 downto 0);
            probe_in17  : in  std_logic_vector(14 downto 0);
            probe_in18  : in  std_logic_vector(14 downto 0);
            probe_in19  : in  std_logic_vector(14 downto 0);
            probe_in20  : in  std_logic_vector(14 downto 0);
            probe_in21  : in  std_logic_vector(14 downto 0);
            probe_in22  : in  std_logic_vector(14 downto 0);
            probe_in23  : in  std_logic_vector(14 downto 0);
            probe_in24  : in  std_logic_vector(14 downto 0);
            probe_in25  : in  std_logic_vector(14 downto 0);
            probe_in26  : in  std_logic_vector(14 downto 0);
            probe_in27  : in  std_logic_vector(14 downto 0);
            probe_in28  : in  std_logic_vector(63 downto 0);
            probe_in29  : in  std_logic_vector(63 downto 0);
            probe_in30  : in  std_logic_vector(2 downto 0);
            probe_in31  : in  std_logic_vector(9 downto 0);
            probe_in32  : in  std_logic_vector(63 downto 0);
            probe_in33  : in  std_logic_vector(31 downto 0);
            probe_in34  : in  std_logic_vector(63 downto 0);
            probe_out0  : out std_logic_vector(0 downto 0);
            probe_out1  : out std_logic_vector(0 downto 0);
            probe_out2  : out std_logic_vector(7 downto 0);
            probe_out3  : out std_logic_vector(0 downto 0);
            probe_out4  : out std_logic_vector(0 downto 0);
            probe_out5  : out std_logic_vector(0 downto 0);
            probe_out6  : out std_logic_vector(23 downto 0);
            probe_out7  : out std_logic_vector(0 downto 0);
            probe_out8  : out std_logic_vector(0 downto 0);
            probe_out9  : out std_logic_vector(0 downto 0);
            probe_out10 : out std_logic_vector(0 downto 0);
            probe_out11 : out std_logic_vector(15 downto 0);
            probe_out12 : out std_logic_vector(0 downto 0);
            probe_out13 : out std_logic_vector(0 downto 0)
        );
    end component;



    ---- MMCM to generate i2c and free-running clocks
    component clk_wiz_1
        port (
            -- Clock in ports
            clk_in1_p   : in  std_logic;
            clk_in1_n   : in  std_logic;
            -- Clock out ports
            clk_out1    : out std_logic;
            clk_out2    : out std_logic;
            clk_out3    : out std_logic;
            clk_out4    : out std_logic;
            clk_out5    : out std_logic
        );
    end component;

    component HBM_MMCM
        port (
            -- Clock in ports
            clk_in1_p   : in  std_logic;
            clk_in1_n   : in  std_logic;
            -- Clock out ports
            clk_out1    : out std_logic;
            clk_out2    : out std_logic;
            clk_out3    : out std_logic;
            -- Status and control signals
            locked      : out std_logic
        );
    end component;

    component freq_measure_base is
        generic (
            freq_used   : std_logic_vector(31 downto 0) := x"0001E848"
        );
        port (
            sysclk      : in  std_logic;
            base_clk    : in  std_logic;                        -- clock base used to measure the sysclk
            frequency   : out std_logic_vector(31 downto 0)     -- measure of the frequency
        );
    end component;

    component freq_measure_base_array is
        generic (
            NCLOCKS         : natural;
            freq_used       : std_logic_vector(31 downto 0)
        );
        port (
            clk_base        : in  std_logic;
            clk_measure     : in  std_logic_vector(NCLOCKS - 1 downto 0);
            freq_measure    : out TBus32b(NCLOCKS - 1 downto 0)
            );
    end component;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- scouting core signals declaration
    --
    ---------------------------------------------------------------------------
    ---- MGT CLOCK signals
    signal mgtrefclk0_x0y11_int,  mgtrefclk0_x0y11_int_buf,  mgtrefclk0_x0y11_int_free  : std_logic;    -- x0y11
    signal mgtrefclk0_x0y10_int,  mgtrefclk0_x0y10_int_buf,  mgtrefclk0_x0y10_int_free  : std_logic;    -- x0y10
    signal mgtrefclk0_x0y8_int,   mgtrefclk0_x0y8_int_buf,   mgtrefclk0_x0y8_int_free   : std_logic;    -- x0y8
    signal mgtrefclk0_x0y7_int,   mgtrefclk0_x0y7_int_buf,   mgtrefclk0_x0y7_int_free   : std_logic;    -- x0y7
    signal mgtrefclk0_x0y5_int,   mgtrefclk0_x0y5_int_buf,   mgtrefclk0_x0y5_int_free   : std_logic;    -- x0y5
    signal mgtrefclk0_x0y4_int,   mgtrefclk0_x0y4_int_buf,   mgtrefclk0_x0y4_int_free   : std_logic;    -- x0y4
    signal mgtrefclk0_x0y3_int,   mgtrefclk0_x0y3_int_buf,   mgtrefclk0_x0y3_int_free   : std_logic;    -- x0y3
    signal mgtrefclk0_x0y2_int,   mgtrefclk0_x0y2_int_buf,   mgtrefclk0_x0y2_int_free   : std_logic;    -- x0y2
    signal mgtrefclk0_x0y1_int,   mgtrefclk0_x0y1_int_buf,   mgtrefclk0_x0y1_int_free   : std_logic;    -- x0y1
    signal mgtrefclk0_x0y0_int,   mgtrefclk0_x0y0_int_buf,   mgtrefclk0_x0y0_int_free   : std_logic;    -- x0y0

    ---- CLK signals
    signal clk_freerun_buf  : std_logic;
    signal clk_i2c          : std_logic;
    signal clk_axi          : std_logic;
    signal locked           : std_logic;

    ---- RST signals
    signal rst_global               : std_logic;
    signal rst_scone                : std_logic;
    signal rst_merged               : std_logic;

    -- clk frequency measurement
    signal freq_clk_x0y11                               : std_logic_vector(31 downto 0);
    signal freq_clk_x0y10                               : std_logic_vector(31 downto 0);
    signal freq_clk_x0y8                                : std_logic_vector(31 downto 0);
    signal freq_clk_x0y7                                : std_logic_vector(31 downto 0);
    signal freq_clk_x0y5                                : std_logic_vector(31 downto 0);
    signal freq_clk_x0y4                                : std_logic_vector(31 downto 0);
    signal freq_clk_x0y3                                : std_logic_vector(31 downto 0);
    signal freq_clk_x0y2                                : std_logic_vector(31 downto 0);
    signal freq_clk_x0y1                                : std_logic_vector(31 downto 0);
    signal freq_clk_x0y0                                : std_logic_vector(31 downto 0);
    signal freq_clk_axi                                 : std_logic_vector(31 downto 0);
    signal freq_clk_i2c                                 : std_logic_vector(31 downto 0);

    ---- PCIE INTERFACE signals and some registers
    signal axi_rstn                                     : std_logic;
    signal pci_exp_lnk_up                               : std_logic := '0';
    -- AXI lite
    signal m_axil_awaddr,  m_axil_araddr                : std_logic_vector(31 downto 0);
    signal m_axil_awprot,  m_axil_arprot                : std_logic_vector( 2 downto 0);
    signal m_axil_awvalid, m_axil_awready               : std_logic;                     -- write address ready/valid
    signal m_axil_arvalid, m_axil_arready               : std_logic;
    signal m_axil_wdata,   m_axil_rdata                 : std_logic_vector(31 downto 0);
    signal m_axil_wstrb                                 : std_logic_vector(3 downto 0);
    signal m_axil_wvalid,  m_axil_wready                : std_logic;                     -- write data valid/ready
    signal m_axil_rvalid,  m_axil_rready                : std_logic;
    signal m_axil_bvalid,  m_axil_bready                : std_logic;                     -- write response valid/ready
    signal m_axil_rresp,   m_axil_bresp                 : std_logic_vector(1 downto 0);

    signal delay_retreive_data_back                     : std_logic_vector(2 downto 0);
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- UGT scouting signals declaration
    --
    ---------------------------------------------------------------------------
    ---- constants (for now here for debugging)
    constant N_REGION_UGT       : integer := 5;
    constant N_STREAM_UGT       : integer := 18;
    constant N_HBM_PORT_UGT     : integer := 1;
    constant QUAD_MAP_UGT       : TQuadMap(N_REGION_UGT - 1 downto 0) := (128, 127, 126, 125, 124);

    ---- reset signals
    signal ugt_local_reset                              : std_logic;
    signal ugt_rst_packager                             : std_logic;
    signal ugt_rst_link                                 : std_logic_vector(4*N_REGION_UGT - 1 downto 0);
    signal ugt_rst_link_hbm_sync                        : std_logic_vector(4*N_REGION_UGT - 1 downto 0);
    signal ugt_hb0_gtwiz_reset_tx_pll_and_datapath_int  : std_logic_vector(0 downto 0);
    signal ugt_hb0_gtwiz_reset_tx_datapath_int          : std_logic_vector(0 downto 0);
    signal ugt_hb_gtwiz_reset_rx_datapath_vio_int       : std_logic_vector(0 downto 0);

    ---- GTWIZARD signals: serial data ports
    signal ugt_mgtgtyrxn_in                             : std_logic_vector(4*N_REGION_UGT - 1 downto 0);
    signal ugt_mgtgtyrxp_in                             : std_logic_vector(4*N_REGION_UGT - 1 downto 0);
    signal ugt_mgtgtytxn_out                            : std_logic_vector(4*N_REGION_UGT - 1 downto 0);
    signal ugt_mgtgtytxp_out                            : std_logic_vector(4*N_REGION_UGT - 1 downto 0);

    ---- GTWIZARD signals: clocking
    signal ugt_clk_rec                                  : std_logic_vector(4*N_REGION_UGT - 1 downto 0);

    ---- inputs module decoder signals
    signal ugt_s_ifcomma_ila_sync                       : std_logic_vector(4*N_REGION_UGT - 1 downto 0);
    signal ugt_s_ifpadding_ila_sync                     : std_logic_vector(4*N_REGION_UGT - 1 downto 0);
    signal ugt_s_ifinvalid_ila_sync                     : std_logic_vector(4*N_REGION_UGT - 1 downto 0);
    signal ugt_s_ifdata_ila_sync                        : std_logic_vector(4*N_REGION_UGT - 1 downto 0);
    signal ugt_s_ifelse_ila_sync                        : std_logic_vector(4*N_REGION_UGT - 1 downto 0);

    ---- non-aligned data words signals
    signal ugt_d_gap_cleaner_ila_sync                   : ldata(4*N_REGION_UGT - 1 downto 0);
    signal ugt_d_inputs_aligner_ila_sync                : ldata(4*N_REGION_UGT - 1 downto 0);
    signal ugt_lid_ila_sync                             : TAuxInfo(4*N_REGION_UGT - 1 downto 0);
    signal ugt_crc_ila_sync                             : TAuxInfo(4*N_REGION_UGT - 1 downto 0);
    signal ugt_d_align_ila_sync                         : ldata(4*N_REGION_UGT - 1 downto 0);
    signal ugt_waiting_orbit_end_ila_sync               : std_logic;

    ---- aligned data words signals
    signal ugt_q_align_ila_sync                         : adata(4*N_REGION_UGT - 1 downto 0);
    signal ugt_d_zs_ila_sync                            : adata(4*N_REGION_UGT - 1 downto 0);
    signal ugt_d_bx_ila_sync                            : TStream_aframe(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_d_trailer_ila_sync                       : TStream_aframe(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_d_reshape_ila_sync                       : TStream_aframe(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_d_package_ila_sync                       : TStream_aframe(N_HBM_PORT_UGT - 1 downto 0);

    ---- aligned data control signals
    signal ugt_q_ctrl_align_ila_sync                    : acontrol;
    signal ugt_d_ctrl_zs_ila_sync                       : acontrol;
    signal ugt_d_ctrl_bx_ila_sync                       : TStream_acontrol(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_d_ctrl_trailer_ila_sync                  : TStream_acontrol(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_d_ctrl_reshape_ila_sync                  : TStream_acontrol(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_d_ctrl_package_ila_sync                  : TStream_acontrol(N_HBM_PORT_UGT - 1 downto 0);

    ---- packager signals
    signal ugt_packager_dropped_orbits_ila_sync         : packager_tcounter(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_packager_seen_orbits_ila_sync            : packager_tcounter(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_packager_orbit_length_bxs_ila_sync       : packager_tcounter(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_packager_axi_backpressure_seen_ila_sync  : std_logic_vector(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_packager_orbit_exceeds_size_ila_sync     : std_logic_vector(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_packager_Fragment_Header_ila_sync        : packager_tdata(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_packager_Write_Fragment_Header_ila_sync  : packager_tbit(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_packager_Orbit_Header_ila_sync           : packager_tdata(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_packager_Write_Orbit_Header_ila_sync     : packager_tbit(N_HBM_PORT_UGT - 1 downto 0);

    ---- HBM signals
    signal ugt_scouting_to_HBM_data                     : TStream_HBM_data(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_scouting_to_HBM_ctrl                     : TStream_HBM_ctrl(N_HBM_PORT_UGT - 1 downto 0);
    signal ugt_HBM_almost_full                          : std_logic_vector(N_HBM_PORT_UGT - 1 downto 0);

    ---- monitoring and control
    signal ugt_scout_data_rd                            : std_logic_vector(63 downto 0);
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- CALO scouting signals declaration
    --
    ---------------------------------------------------------------------------
    ---- constants (for now here for debugging)
    constant N_REGION_CALO      : integer := 2;
    constant N_STREAM_CALO      : integer := 7;
    constant N_HBM_PORT_CALO    : integer := 4;
    constant QUAD_MAP_CALO      : TQuadMap(N_REGION_CALO - 1 downto 0) := (135, 134);

    ---- reset signals
    signal calo_rst_packager                            : std_logic;
    signal calo_rst_link                                : std_logic_vector(4*N_REGION_CALO - 1 downto 0);
    signal calo_rst_link_hbm_sync                       : std_logic_vector(4*N_REGION_CALO - 1 downto 0);
    signal calo_hb0_gtwiz_reset_tx_pll_and_datapath_int : std_logic_vector(0 downto 0);
    signal calo_hb0_gtwiz_reset_tx_datapath_int         : std_logic_vector(0 downto 0);
    signal calo_hb_gtwiz_reset_rx_datapath_vio_int      : std_logic_vector(0 downto 0);

    ---- GTWIZARD signals: serial data ports
    signal calo_mgtgtyrxn_in                            : std_logic_vector(4*N_REGION_CALO - 1 downto 0);
    signal calo_mgtgtyrxp_in                            : std_logic_vector(4*N_REGION_CALO - 1 downto 0);
    signal calo_mgtgtytxn_out                           : std_logic_vector(4*N_REGION_CALO - 1 downto 0);
    signal calo_mgtgtytxp_out                           : std_logic_vector(4*N_REGION_CALO - 1 downto 0);

    ---- GTWIZARD signals: clocking
    signal calo_clk_rec                                 : std_logic_vector(4*N_REGION_CALO - 1 downto 0);

    ---- inputs module decoder signals
    signal calo_s_ifcomma_ila_sync                      : std_logic_vector(4*N_REGION_CALO - 1 downto 0);
    signal calo_s_ifpadding_ila_sync                    : std_logic_vector(4*N_REGION_CALO - 1 downto 0);
    signal calo_s_ifinvalid_ila_sync                    : std_logic_vector(4*N_REGION_CALO - 1 downto 0);
    signal calo_s_ifdata_ila_sync                       : std_logic_vector(4*N_REGION_CALO - 1 downto 0);
    signal calo_s_ifelse_ila_sync                       : std_logic_vector(4*N_REGION_CALO - 1 downto 0);

    ---- non-aligned data words signals
    signal calo_d_gap_cleaner_ila_sync                  : ldata(4*N_REGION_CALO - 1 downto 0);
    signal calo_d_inputs_aligner_ila_sync               : ldata(4*N_REGION_CALO - 1 downto 0);
    signal calo_lid_ila_sync                            : TAuxInfo(4*N_REGION_CALO - 1 downto 0);
    signal calo_crc_ila_sync                            : TAuxInfo(4*N_REGION_CALO - 1 downto 0);
    signal calo_d_align_ila_sync                        : ldata(4*N_REGION_CALO - 1 downto 0);
    signal calo_waiting_orbit_end_ila_sync              : std_logic;

    ---- aligned data words signals
    signal calo_q_align_ila_sync                        : adata(4*N_REGION_CALO - 1 downto 0);
    signal calo_d_zs_ila_sync                           : adata(4*N_REGION_CALO - 1 downto 0);
    signal calo_d_bx_ila_sync                           : TStream_aframe(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_d_trailer_ila_sync                      : TStream_aframe(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_d_reshape_ila_sync                      : TStream_aframe(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_d_package_ila_sync                      : TStream_aframe(N_HBM_PORT_CALO - 1 downto 0);

    ---- aligned data control signals
    signal calo_q_ctrl_align_ila_sync                   : acontrol;
    signal calo_d_ctrl_zs_ila_sync                      : acontrol;
    signal calo_d_ctrl_bx_ila_sync                      : TStream_acontrol(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_d_ctrl_trailer_ila_sync                 : TStream_acontrol(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_d_ctrl_reshape_ila_sync                 : TStream_acontrol(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_d_ctrl_package_ila_sync                 : TStream_acontrol(N_HBM_PORT_CALO - 1 downto 0);

    ---- packager signals
    signal calo_packager_dropped_orbits_ila_sync        : packager_tcounter(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_packager_seen_orbits_ila_sync           : packager_tcounter(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_packager_orbit_length_bxs_ila_sync      : packager_tcounter(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_packager_axi_backpressure_seen_ila_sync : std_logic_vector(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_packager_orbit_exceeds_size_ila_sync    : std_logic_vector(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_packager_Fragment_Header_ila_sync       : packager_tdata(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_packager_Write_Fragment_Header_ila_sync : packager_tbit(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_packager_Orbit_Header_ila_sync          : packager_tdata(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_packager_Write_Orbit_Header_ila_sync    : packager_tbit(N_HBM_PORT_CALO - 1 downto 0);

    ---- HBM signals
    signal calo_scouting_to_HBM_data                    : TStream_HBM_data(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_scouting_to_HBM_ctrl                    : TStream_HBM_ctrl(N_HBM_PORT_CALO - 1 downto 0);
    signal calo_HBM_almost_full                         : std_logic_vector(N_HBM_PORT_CALO - 1 downto 0);

    ---- monitoring and control
    signal calo_scout_data_rd                           : std_logic_vector(63 downto 0);
    ---------------------------------------------------------------------------






    ---------------------------------------------------------------------------
    --
    -- HBM, 100G MAC, DAQ signals declaration
    --
    ---------------------------------------------------------------------------
    signal compilation_version          : std_logic_vector(31 downto 0);
    signal clock_low                    : std_logic;

    ---- memory block builder
    signal almost_full_buffer           : std_logic_vector(number_of_HBM_port-1 downto 0);
    signal scouting_to_HBM_data         : SR_rcv_data_hlf;
    signal scouting_to_HBM_ctrl         : SR_rcv_ctrl_hlf;
    signal HBM_almost_full              : HBM_bus_1_bit;
    signal UR_rst_p                     : std_logic;
    signal ur_mem_resetp                : std_logic;
    signal ur_mem_resetn                : std_logic;

    signal HBM_usr_dto                  : std_logic_vector(63 downto 0);
    signal HBM_RESETn                   : std_logic := '1';
    signal HBM_RESETp                   : std_logic := '1';
    signal HBM_memory_ready             : std_logic;

    signal DAQx_TCP_ack_rcv             : TCP_ack_rcvo_type;                                -- array (0 to number_of_HBM_port-1) of TCP_ack_rcvo_record
    signal DAQx_TCP_Ack_present         : std_logic_vector(number_of_HBM_port-1 downto 0);
    signal DAQx_rd_TCP_val_rcvo         : std_logic_vector(number_of_HBM_port-1 downto 0);

    -- packet ready to be read from the Ethernet logic
    signal DAQx_eth_packet_to_be_sent   : HBM_to_TCP_block_pipe;
    signal DAQx_eth_gnt_access          : std_logic_vector(number_of_HBM_port-1 downto 0);  -- give the access to DAQ unit
    signal DAQx_eth_ack_packet          : std_logic_vector(number_of_HBM_port-1 downto 0);  -- pulse to read CS and release the place

    signal DAQ_TCP_ack_rcvo_p00         : TCP_ack_rcvo_record;
    signal DAQ_TCP_ack_rcvo_p01         : TCP_ack_rcvo_record;
    signal DAQ_TCP_ack_rcvo_p02         : TCP_ack_rcvo_record;
    signal DAQ_TCP_ack_rcvo_p03         : TCP_ack_rcvo_record;

    signal DAQ_TCP_Ack_present_p00      : std_logic;
    signal DAQ_TCP_Ack_present_p01      : std_logic;
    signal DAQ_TCP_Ack_present_p02      : std_logic;
    signal DAQ_TCP_Ack_present_p03      : std_logic;

    signal rd_DAQ_TCP_val_rcvo_p00      : std_logic;
    signal rd_DAQ_TCP_val_rcvo_p01      : std_logic;
    signal rd_DAQ_TCP_val_rcvo_p02      : std_logic;
    signal rd_DAQ_TCP_val_rcvo_p03      : std_logic;

    signal TCP_BLOCK_TO_SEND_p00        : HBM_to_TCP_block_record;
    signal TCP_BLOCK_TO_SEND_p01        : HBM_to_TCP_block_record;
    signal TCP_BLOCK_TO_SEND_p02        : HBM_to_TCP_block_record;
    signal TCP_BLOCK_TO_SEND_p03        : HBM_to_TCP_block_record;

    signal DAQ_TCP_packet_done_p00      : std_logic;
    signal DAQ_TCP_packet_done_p01      : std_logic;
    signal DAQ_TCP_packet_done_p02      : std_logic;
    signal DAQ_TCP_packet_done_p03      : std_logic;

    signal Clock_ETH_p00                : std_logic;
    signal Clock_ETH_p01                : std_logic;
    signal Clock_ETH_p02                : std_logic;
    signal Clock_ETH_p03                : std_logic;

    signal clock_FED                    : std_logic;
    signal TCDS2_clock                  : std_logic;
    signal status_out                   : std_logic_vector(31 downto 0);
    signal status_out_p00               : std_logic_vector(31 downto 0);
    signal status_out_p01               : std_logic_vector(31 downto 0);
    signal status_out_p02               : std_logic_vector(31 downto 0);
    signal status_out_p03               : std_logic_vector(31 downto 0);

    signal User_rstn_clock_buffer       : std_logic;
    signal usr_rstp_clk_buffer          : std_logic;

    signal HBM_clock_buffer             : std_logic;

    signal merger_data_user_p00         : std_logic_vector(63 downto 0) := (others => '0');
    signal merger_data_user_p01         : std_logic_vector(63 downto 0) := (others => '0');
    signal merger_data_user_p02         : std_logic_vector(63 downto 0) := (others => '0');

    signal resetp_counter_timer_resync_p00  : std_logic;
    signal resetp_counter_timer_resync_p01  : std_logic;
    signal resetp_counter_timer_resync_p02  : std_logic;
    signal resetp_counter_timer_resync_p03  : std_logic;

    -- Clock Signals
    signal clock_HBM                    : std_logic;
    signal HBM_clk_ref                  : std_logic;
    signal CLK_125MHZ                   : std_logic;

    -- User control signals
    signal usr_clk                      : std_logic;
    signal CLK_100MHz                   : std_logic;
    signal TCP_clock                    : std_logic;
    signal rst_usr_clk_n                : std_logic;

    -- User control signals
    signal usr_func_wr                  : std_logic_vector(16383 downto 0);
    signal usr_wren                     : std_logic;
    signal usr_data_wr                  : std_logic_vector(63 downto 0);
    signal usr_be                       : std_logic_vector(7 downto 0);
    signal usr_func_rd                  : std_logic_vector(16383 downto 0);
    signal usr_rden                     : std_logic;
    signal usr_data_rd                  : std_logic_vector(63 downto 0);

    signal funct                        : std_logic_vector(15 downto 0);
    signal data_out                     : std_logic_vector(31 downto 0);
    signal data_in                      : std_logic_vector(31 downto 0);
    signal data_out_cell                : std_logic_vector(31 downto 0);
    signal data_in_rg                   : std_logic_vector(63 downto 0);
    signal scout_data_in_rg             : std_logic_vector(63 downto 0);
    signal wr_ena                       : std_logic;
    signal rd_ena                       : std_logic;
    signal wr_ena_cell                  : std_logic;
    signal rd_ena_cell                  : std_logic;

    signal led_monitor                  : std_logic_vector(31 downto 0);

    SIGNAL TCP_clock_resetn             : std_logic;
    SIGNAL TCP_clock_resetp             : std_logic;

    -- I2C signals
    signal I2C_ena_clock_div            : std_logic_vector(31 downto 0);

    signal QSFP_I2C_RESET_cell          : std_logic;
    signal FF_I2C_RESET_cell            : std_logic;
    signal Clock_I2C_RESET_cell         : std_logic;

    signal QSFP_MAIN_SDA_cell           : std_logic;
    signal QSFP_MAIN_SDA_tri            : std_logic;
    signal cs_i2c_QSFP_master           : std_logic;
    signal func_i2c_QSFP_master         : std_logic_vector(1 downto 0);
    signal dto_i2c_QSFP_monitor         : std_logic_vector(63 downto 0) := (others => '0');

    signal ClockGen_1_rst_cell          : std_logic;
    signal ClockGen_2_rst_cell          : std_logic;
    signal ClockGen_3_rst_cell          : std_logic;

    signal ena_clock_i2c                : std_logic;

    signal I2C_reset_p                  : std_logic;
    signal I2C_reset_n                  : std_logic;
    signal resetp_counters              : std_logic;

    -- QSFP control signals
    signal QSFP_A_MODSEL_reg            : std_logic;
    signal QSFP_A_RESETL_reg            : std_logic;
    signal QSFP_A_LPMODE_reg            : std_logic;
    signal QSFP_B_MODSEL_reg            : std_logic;
    signal QSFP_B_RESETL_reg            : std_logic;
    signal QSFP_B_LPMODE_reg            : std_logic;
    signal QSFP_C_MODSEL_reg            : std_logic;
    signal QSFP_C_RESETL_reg            : std_logic;
    signal QSFP_C_LPMODE_reg            : std_logic;
    signal QSFP_D_MODSEL_reg            : std_logic;
    signal QSFP_D_RESETL_reg            : std_logic;
    signal QSFP_D_LPMODE_reg            : std_logic;

    -- MAC
    signal Start_MAC_read_restart       : std_logic;
    signal MAC_byte                     : std_logic_vector(7 downto 0);
    signal MAC_byte_latch               : std_logic;
    signal MAC_ready                    : std_logic;

    type MAC_address_type is array (0 to 29) of std_logic_vector(7 downto 0);
    signal MAC_bytes_low                : MAC_address_type;
    signal Counter_latch_MAC_add        : std_logic_vector(7 downto 0);

    signal Byte_i2c_out	                : std_logic_vector(7 downto 0);
    signal Byte_i2c_latch	            : std_logic;
    signal Start_LLDP                   : std_logic;


    signal SERDES_RESET_P               : std_logic;
    signal dto_DAQ_module               : std_logic_vector(63 downto 0) := (others => '0');
    signal dto_DAQ_module_p00           : std_logic_vector(63 downto 0) := (others => '0');
    signal dto_DAQ_module_p01           : std_logic_vector(63 downto 0) := (others => '0');
    signal dto_DAQ_module_p02           : std_logic_vector(63 downto 0) := (others => '0');
    signal dto_DAQ_module_p03           : std_logic_vector(63 downto 0) := (others => '0');

    signal debug0_counter               : std_logic_vector(31 downto 0);

    signal measure_UR_clock             : std_logic_vector(31 downto 0);
    signal measure_SR0_clock            : std_logic_vector(31 downto 0);
    signal measure_ETH_clock            : std_logic_vector(31 downto 0);
    signal measure_ETH_clock_p00        : std_logic_vector(31 downto 0);
    signal measure_ETH_clock_p01        : std_logic_vector(31 downto 0);
    signal measure_ETH_clock_p02        : std_logic_vector(31 downto 0);
    signal measure_ETH_clock_p03        : std_logic_vector(31 downto 0);
    ---------------------------------------------------------------------------

begin

    ---------------------------------------------------------------------------
    --
    -- CLK buffers, MMCM, I2C driver for MGT clk programming
    --
    ---------------------------------------------------------------------------
    bufg_clk_freerun_inst : BUFG
        port map (
            I => CLK_100MHz,
            O => clk_freerun_buf
        );

    mgtrefclk_buffers : entity work.mgtrefclk_buf
        generic map (
            x0y0    => true,
            x0y1    => true,
            x0y2    => true,
            x0y3    => true,
            x0y4    => true,
            x0y5    => true,
            x0y7    => false,
            x0y8    => false,
            x0y10   => true,
            x0y11   => true
        )
        port map (
            -- input
            mgtrefclk0_x0y0_p         => mgtrefclk0_x0y0_p,
            mgtrefclk0_x0y0_n         => mgtrefclk0_x0y0_n,
            mgtrefclk0_x0y1_p         => mgtrefclk0_x0y1_p,
            mgtrefclk0_x0y1_n         => mgtrefclk0_x0y1_n,
            mgtrefclk0_x0y2_p         => mgtrefclk0_x0y2_p,
            mgtrefclk0_x0y2_n         => mgtrefclk0_x0y2_n,
            mgtrefclk0_x0y3_p         => mgtrefclk0_x0y3_p,
            mgtrefclk0_x0y3_n         => mgtrefclk0_x0y3_n,
            mgtrefclk0_x0y4_p         => mgtrefclk0_x0y4_p,
            mgtrefclk0_x0y4_n         => mgtrefclk0_x0y4_n,
            mgtrefclk0_x0y5_p         => mgtrefclk0_x0y5_p,
            mgtrefclk0_x0y5_n         => mgtrefclk0_x0y5_n,
            mgtrefclk0_x0y7_p         => mgtrefclk0_x0y7_p,
            mgtrefclk0_x0y7_n         => mgtrefclk0_x0y7_n,
            mgtrefclk0_x0y8_p         => mgtrefclk0_x0y8_p,
            mgtrefclk0_x0y8_n         => mgtrefclk0_x0y8_n,
            mgtrefclk0_x0y10_p        => mgtrefclk0_x0y10_p,
            mgtrefclk0_x0y10_n        => mgtrefclk0_x0y10_n,
            mgtrefclk0_x0y11_p        => mgtrefclk0_x0y11_p,
            mgtrefclk0_x0y11_n        => mgtrefclk0_x0y11_n,
            -- output
            mgtrefclk0_x0y0_int       => mgtrefclk0_x0y0_int,
            mgtrefclk0_x0y1_int       => mgtrefclk0_x0y1_int,
            mgtrefclk0_x0y2_int       => mgtrefclk0_x0y2_int,
            mgtrefclk0_x0y3_int       => mgtrefclk0_x0y3_int,
            mgtrefclk0_x0y4_int       => mgtrefclk0_x0y4_int,
            mgtrefclk0_x0y5_int       => mgtrefclk0_x0y5_int,
            mgtrefclk0_x0y10_int      => mgtrefclk0_x0y10_int,
            mgtrefclk0_x0y11_int      => mgtrefclk0_x0y11_int,
            -- output (buffered)
            mgtrefclk0_x0y0_int_free  => mgtrefclk0_x0y0_int_free,
            mgtrefclk0_x0y1_int_free  => mgtrefclk0_x0y1_int_free,
            mgtrefclk0_x0y2_int_free  => mgtrefclk0_x0y2_int_free,
            mgtrefclk0_x0y3_int_free  => mgtrefclk0_x0y3_int_free,
            mgtrefclk0_x0y4_int_free  => mgtrefclk0_x0y4_int_free,
            mgtrefclk0_x0y5_int_free  => mgtrefclk0_x0y5_int_free,
            mgtrefclk0_x0y10_int_free => mgtrefclk0_x0y10_int_free,
            mgtrefclk0_x0y11_int_free => mgtrefclk0_x0y11_int_free
        );



    ---- PLL/MMCM clocks
    pll_inst : clk_wiz_1
        port map (
            -- Clock in ports
            clk_in1_p   => CLK1_100MHZ_P,
            clk_in1_n   => CLK1_100MHZ_N,
            -- Clock out ports
            clk_out1    => CLK_100MHz,      -- 100 Mhz
            clk_out2    => CLK_125MHZ,      -- 125 Mhz
            clk_out3    => clock_FED,       -- 200 MHz
            clk_out4    => TCP_clock,       -- 250 MHz
            clk_out5    => TCDS2_clock      --  40 MHz
        );

    HBM_clocks : HBM_MMCM
        port map (
            -- Clock in ports
            clk_in1_p   => HBM_clk_ref_p,
            clk_in1_n   => HBM_clk_ref_n,
            -- Clock out ports
            clk_out1    => clock_HBM,
            clk_out2    => HBM_clk_ref
        );

    UR_clock_resync_reset_i1 : entity work.resetp_resync
        port map(
            aresetp     => UR_rst_p,
            clock       => HBM_clock_buffer,
            resetp_sync => ur_mem_resetp
        );

    TCP_clock_resync_reset_i1 : entity work.resetn_resync
        port map (
            aresetn     => rst_usr_clk_n,
            clock       => TCP_clock,
            resetn_sync => TCP_clock_resetn
        );


    resync_reset_i3 : entity work.resetn_resync
        port map (
            aresetn     => rst_usr_clk_n,
            clock       => HBM_clock_buffer,
            resetn_sync => User_rstn_clock_buffer
        );

    ur_mem_resetn       <= not(ur_mem_resetp);
    TCP_clock_resetp    <= not(TCP_clock_resetn);
    usr_rstp_clk_buffer <= not(User_rstn_clock_buffer);
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Reset controllers and freq measurement
    --
    ---------------------------------------------------------------------------
    ---- measure clk's frequencies
    freq_measure_clks : freq_measure_base_array
        generic map (
            NCLOCKS   => 16,
            freq_used => x"07735940"
        )
        port map (
            -- reference clk
            clk_base => CLK_125MHZ,
            -- HBM clock
            clk_measure(00) => clock_HBM,
            -- HBM buffered clock
            clk_measure(01) => HBM_clock_buffer,
            -- Ethernet clock
            clk_measure(02) => Clock_ETH_p00,
            clk_measure(03) => Clock_ETH_p01,
            clk_measure(04) => Clock_ETH_p02,
            -- mgt ref clocks
            clk_measure(05) => mgtrefclk0_x0y0_int_free,
            clk_measure(06) => mgtrefclk0_x0y1_int_free,
            clk_measure(07) => mgtrefclk0_x0y2_int_free,
            clk_measure(08) => mgtrefclk0_x0y3_int_free,
            clk_measure(09) => mgtrefclk0_x0y4_int_free,
            clk_measure(10) => mgtrefclk0_x0y5_int_free,
            clk_measure(11) => mgtrefclk0_x0y7_int_free,
            clk_measure(12) => mgtrefclk0_x0y8_int_free,
            clk_measure(13) => mgtrefclk0_x0y10_int_free,
            clk_measure(14) => mgtrefclk0_x0y11_int_free,
            -- axi clock
            clk_measure(15) => clk_axi,

            -- link recovered clocks
            -- HBM clock
            freq_measure(00) => measure_UR_clock,
            -- HBM buffered clock
            freq_measure(01) => measure_SR0_clock,
            -- Ethernet clock
            freq_measure(02) => measure_ETH_clock_p00,
            freq_measure(03) => measure_ETH_clock_p01,
            freq_measure(04) => measure_ETH_clock_p02,
            -- mgt ref clocks
            freq_measure(05) => freq_clk_x0y0,
            freq_measure(06) => freq_clk_x0y1,
            freq_measure(07) => freq_clk_x0y2,
            freq_measure(08) => freq_clk_x0y3,
            freq_measure(09) => freq_clk_x0y4,
            freq_measure(10) => freq_clk_x0y5,
            freq_measure(11) => freq_clk_x0y7,
            freq_measure(12) => freq_clk_x0y8,
            freq_measure(13) => freq_clk_x0y10,
            freq_measure(14) => freq_clk_x0y11,
            -- axi clock
            freq_measure(15) => freq_clk_axi
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- QSFP control pins
    --
    ---------------------------------------------------------------------------
    ver_comp_i1 : entity work.version_comp
        port map (
            version => compilation_version
        );

    ---- write
    process(rst_usr_clk_n, usr_clk)
    begin
        if rst_usr_clk_n = '0' then
            led_monitor(31 downto 2)    <= (others => '0');
            I2C_reset_p                 <= '0'; --?
            QSFP_D_RESETL_reg           <= '0';
            QSFP_C_RESETL_reg           <= '0';
            QSFP_B_RESETL_reg           <= '0';
            QSFP_I2C_RESET_cell         <= '0';
            SERDES_RESET_P              <= '0';
            HBM_RESETp                  <= '0';
            UR_rst_p                    <= '0';
            resetp_counters             <= '0';
            Start_MAC_read_restart      <= '0';
        elsif rising_edge(usr_clk) then

            Start_MAC_read_restart  <= '0';

            if usr_wren = '1' then

                if usr_func_wr(LED_control) = '1' then
                    led_monitor(31 downto 2)    <= usr_data_wr(31 downto 2);
                end if;

                if usr_func_wr(reset_hw_comp) = '1' then
                    SERDES_RESET_P          <= usr_data_wr(0);
                    Start_MAC_read_restart  <= usr_data_wr(8);
                    resetp_counters         <= usr_data_wr(16);
                    HBM_RESETp              <= usr_data_wr(17);
                    UR_rst_p                <= usr_data_wr(27);
                    QSFP_I2C_RESET_cell     <= usr_data_wr(28);
                    FF_I2C_RESET_cell       <= usr_data_wr(29);
                    Clock_I2C_RESET_cell    <= usr_data_wr(30);
                    I2C_reset_p             <= usr_data_wr(31);
                end if;

                if usr_func_wr(QSFP_monitoring) = '1' then
                    QSFP_A_RESETL_reg       <= usr_data_wr(1);
                    QSFP_A_MODSEL_reg       <= usr_data_wr(0);
                    QSFP_A_LPMODE_reg       <= usr_data_wr(4);

                    QSFP_B_RESETL_reg       <= usr_data_wr(9);
                    QSFP_B_MODSEL_reg       <= usr_data_wr(8);
                    QSFP_B_LPMODE_reg       <= usr_data_wr(12);

                    QSFP_C_RESETL_reg       <= usr_data_wr(17);
                    QSFP_C_MODSEL_reg       <= usr_data_wr(16);
                    QSFP_C_LPMODE_reg       <= usr_data_wr(20);

                    QSFP_D_RESETL_reg       <= usr_data_wr(25);
                    QSFP_D_MODSEL_reg       <= usr_data_wr(24);
                    QSFP_D_LPMODE_reg       <= usr_data_wr(28);
                end if;

                if usr_func_wr(CLOCK_gen_status) = '1' then
                    ClockGen_1_rst_cell     <= usr_data_wr(0);
                    ClockGen_2_rst_cell     <= usr_data_wr(4);
                    ClockGen_3_rst_cell     <= usr_data_wr(8);
                end if;

            end if;
        end if;
    end process;



    ---- read
    process(usr_clk)
    begin
        if rising_edge(usr_clk) then

            data_in_rg  <= (others => '0');

            if usr_func_rd(LED_control) = '1' then
                data_in_rg(29 downto 0) <= led_monitor(29 downto 0);

            elsif usr_func_rd(Compilation_version_off) = '1' then
                data_in_rg(31 downto 0) <= compilation_version;

            elsif usr_func_rd(reset_hw_comp) = '1' then

                data_in_rg(16)          <= resetp_counters;
                data_in_rg(17)          <= HBM_RESETp;
                data_in_rg(27)          <= UR_rst_p;
                data_in_rg(28)          <= QSFP_I2C_RESET_cell;
                data_in_rg(29)          <= FF_I2C_RESET_cell;
                data_in_rg(30)          <= Clock_I2C_RESET_cell;
                data_in_rg(31)          <= I2C_reset_p;

            elsif usr_func_rd(QSFP_monitoring) = '1' then

                data_in_rg(0)           <= QSFP_A_MODSEL_reg;   -- module select
                data_in_rg(1)           <= QSFP_A_RESETL_reg;   -- module reset
                data_in_rg(2)           <= QSFP_A_MODPRSL;      -- module present
                data_in_rg(3)           <= QSFP_A_INTL;         -- interruption
                data_in_rg(4)           <= QSFP_A_LPMODE_reg;   -- low power mode

                data_in_rg(8)           <= QSFP_B_MODSEL_reg;   -- module select
                data_in_rg(9)           <= QSFP_B_RESETL_reg;   -- module reset
                data_in_rg(10)          <= QSFP_B_MODPRSL;      -- module present
                data_in_rg(11)          <= QSFP_B_INTL;         -- interruption
                data_in_rg(12)          <= QSFP_B_LPMODE_reg;   -- low power mode

                data_in_rg(16)          <= QSFP_C_MODSEL_reg;   -- module select
                data_in_rg(17)          <= QSFP_C_RESETL_reg;   -- module reset
                data_in_rg(18)          <= QSFP_C_MODPRSL;      -- module present
                data_in_rg(19)          <= QSFP_C_INTL;         -- interruption
                data_in_rg(20)          <= QSFP_C_LPMODE_reg;   -- low power mode

                data_in_rg(24)          <= QSFP_D_MODSEL_reg;   -- module select
                data_in_rg(25)          <= QSFP_D_RESETL_reg;   -- module reset
                data_in_rg(26)          <= QSFP_D_MODPRSL;      -- module present
                data_in_rg(27)          <= QSFP_D_INTL;         -- interruption
                data_in_rg(28)          <= QSFP_D_LPMODE_reg;   -- low power mode

            elsif usr_func_rd(clock_HBM_measure) = '1' then
                data_in_rg(31 downto 0) <= measure_UR_clock;

            elsif usr_func_rd(clock_SR0_measure) = '1' then
                data_in_rg(31 downto 0) <= measure_SR0_clock;       -- HBM buffered clock

            elsif usr_func_rd(clock_eth_measure + offset_daq_eth_100gb(0)) = '1' then
                data_in_rg(31 downto 0) <= measure_ETH_clock_p00;   -- clock from DAQ p00

            elsif usr_func_rd(clock_eth_measure + offset_daq_eth_100gb(1)) = '1' then
                data_in_rg(31 downto 0) <= measure_ETH_clock_p01;   -- clock from DAQ p01
            end if;
        end if;
    end process;

    usr_data_rd <= merger_data_user_p00     or  -- merger for p00
                   merger_data_user_p01     or  -- merger for p01
                   scout_data_in_rg         or  -- generic scouting registers
                   ugt_scout_data_rd        or  -- ugt scouting pipeline
                   calo_scout_data_rd       or  -- calo scouting pipeline
                   data_in_rg               or  -- general items (mainly QSFPs control)
                   HBM_usr_dto              or  -- HBM registers
                   dto_i2c_QSFP_monitor     or  -- i2c monitoring
                   dto_DAQ_module_p00       or  -- DAQ p00 registers
                   dto_DAQ_module_p01;          -- DAQ p01 registers



    ---- QSFP control
    QSFP_A_MODSEL <= QSFP_A_MODSEL_reg;   -- should '0' to reply to the I2C bus
    QSFP_A_RESETL <= QSFP_A_RESETL_reg;   -- should be '0' to reset the module
    QSFP_A_LPMODE <= QSFP_A_LPMODE_reg;   -- should be '1' to be in low power mode

    QSFP_B_MODSEL <= QSFP_B_MODSEL_reg;   -- should '0' to reply to the I2C bus
    QSFP_B_RESETL <= QSFP_B_RESETL_reg;   -- should be '0' to reset the module
    QSFP_B_LPMODE <= QSFP_B_LPMODE_reg;   -- should be '1' to be in low power mode

    QSFP_C_MODSEL <= QSFP_C_MODSEL_reg;   -- should '0' to reply to the I2C bus
    QSFP_C_RESETL <= QSFP_C_RESETL_reg;   -- should be '0' to reset the module
    QSFP_C_LPMODE <= QSFP_C_LPMODE_reg;   -- should be '1' to be in low power mode

    QSFP_D_MODSEL <= QSFP_D_MODSEL_reg;   -- should '0' to reply to the I2C bus
    QSFP_D_RESETL <= QSFP_D_RESETL_reg;   -- should be '0' to reset the module
    QSFP_D_LPMODE <= QSFP_D_LPMODE_reg;   -- should be '1' to be in low power mode
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- UGT scouting pipeline
    --
    ---------------------------------------------------------------------------
    ugt_mgtgtyrxn_in  <= mgtgty_quad128_rxn_in  & mgtgty_quad127_rxn_in  & mgtgty_quad126_rxn_in  & mgtgty_quad125_rxn_in  & mgtgty_quad124_rxn_in;
    ugt_mgtgtyrxp_in  <= mgtgty_quad128_rxp_in  & mgtgty_quad127_rxp_in  & mgtgty_quad126_rxp_in  & mgtgty_quad125_rxp_in  & mgtgty_quad124_rxp_in;
    ugt_mgtgtytxn_out <= mgtgty_quad128_txn_out & mgtgty_quad127_txn_out & mgtgty_quad126_txn_out & mgtgty_quad125_txn_out & mgtgty_quad124_txn_out;
    ugt_mgtgtytxp_out <= mgtgty_quad128_txp_out & mgtgty_quad127_txp_out & mgtgty_quad126_txp_out & mgtgty_quad125_txp_out & mgtgty_quad124_txp_out;

    ugt_scouting_pipeline : entity work.ugt_scouting_pipeline
        generic map(
            N_QUAD      => N_REGION_UGT,
            N_STREAM    => N_STREAM_UGT,
            N_HBM_PORTS => N_HBM_PORT_UGT,
            QUAD_MAP    => QUAD_MAP_UGT
        )
        port map(
            ---- serial data ports
            mgtgtyrxn_in            => ugt_mgtgtyrxn_in,
            mgtgtyrxp_in            => ugt_mgtgtyrxp_in,
            mgtgtytxn_out           => ugt_mgtgtytxn_out,
            mgtgtytxp_out           => ugt_mgtgtytxp_out,

            ---- output for HBM
            scouting_to_HBM_data    => ugt_scouting_to_HBM_data,
            scouting_to_HBM_ctrl    => ugt_scouting_to_HBM_ctrl,

            ---- HBM backpressure signal
            HBM_almost_full         => ugt_HBM_almost_full,

            ---- input clocks
            clk_i2c                 => ena_clock_i2c,
            clk_ref                 => CLK_125MHz,
            clk_freerun             => CLK_100MHz,
            clk_freerun_buf         => clk_freerun_buf,
            mgtrefclk0_x0y0_int     => mgtrefclk0_x0y0_int,
            mgtrefclk0_x0y1_int     => mgtrefclk0_x0y1_int,
            mgtrefclk0_x0y2_int     => mgtrefclk0_x0y2_int,
            mgtrefclk0_x0y3_int     => mgtrefclk0_x0y3_int,
            mgtrefclk0_x0y4_int     => mgtrefclk0_x0y4_int,
            mgtrefclk0_x0y5_int     => mgtrefclk0_x0y5_int,
            mgtrefclk0_x0y7_int     => mgtrefclk0_x0y7_int,
            mgtrefclk0_x0y8_int     => mgtrefclk0_x0y8_int,
            mgtrefclk0_x0y10_int    => mgtrefclk0_x0y10_int,
            mgtrefclk0_x0y11_int    => mgtrefclk0_x0y11_int,
            HBM_clock_buffer        => HBM_clock_buffer,

            ---- external reset signals
            usr_rstp_clk_buffer     => ur_mem_resetp,

            ---- output clocks clocks
            o_clk_rec               => ugt_clk_rec,

            ---- monitoring and control
            clk_axi                 => clk_axi,
            axi_rstn                => axi_rstn,
            usr_func_wr             => usr_func_wr,
            usr_wren                => usr_wren,
            usr_data_wr             => usr_data_wr,
            usr_func_rd             => usr_func_rd,
            usr_rden                => usr_rden,
            usr_data_rd             => ugt_scout_data_rd,

            ---- inputs module decoder signals
            o_ifcomma_ila_sync      => ugt_s_ifcomma_ila_sync,
            o_ifpadding_ila_sync    => ugt_s_ifpadding_ila_sync,
            o_ifinvalid_ila_sync    => ugt_s_ifinvalid_ila_sync,
            o_ifdata_ila_sync       => ugt_s_ifdata_ila_sync,
            o_ifelse_ila_sync       => ugt_s_ifelse_ila_sync,

            ---- non-aligned data words signals
            o_d_gap_cleaner_ila_sync     => ugt_d_gap_cleaner_ila_sync,
            o_d_inputs_aligner_ila_sync  => ugt_d_inputs_aligner_ila_sync,
            o_lid_ila_sync               => ugt_lid_ila_sync,
            o_crc_ila_sync               => ugt_crc_ila_sync,
            o_d_align_ila_sync           => ugt_d_align_ila_sync,
            o_waiting_orbit_end_ila_sync => ugt_waiting_orbit_end_ila_sync,

            ---- aligned data words signals
            o_q_align_ila_sync   => ugt_q_align_ila_sync,
            o_d_zs_ila_sync      => ugt_d_zs_ila_sync,
            o_d_bx_ila_sync      => ugt_d_bx_ila_sync,
            o_d_trailer_ila_sync => ugt_d_trailer_ila_sync,
            o_d_reshape_ila_sync => ugt_d_reshape_ila_sync,
            o_d_package_ila_sync => ugt_d_package_ila_sync,

            ---- aligned data control signals
            o_q_ctrl_align_ila_sync   => ugt_q_ctrl_align_ila_sync,
            o_d_ctrl_zs_ila_sync      => ugt_d_ctrl_zs_ila_sync,
            o_d_ctrl_bx_ila_sync      => ugt_d_ctrl_bx_ila_sync,
            o_d_ctrl_trailer_ila_sync => ugt_d_ctrl_trailer_ila_sync,
            o_d_ctrl_reshape_ila_sync => ugt_d_ctrl_reshape_ila_sync,
            o_d_ctrl_package_ila_sync => ugt_d_ctrl_package_ila_sync,

            ---- packager signals
            o_packager_dropped_orbits_ila_sync        => ugt_packager_dropped_orbits_ila_sync,
            o_packager_seen_orbits_ila_sync           => ugt_packager_seen_orbits_ila_sync,
            o_packager_orbit_length_bxs_ila_sync      => ugt_packager_orbit_length_bxs_ila_sync,
            o_packager_axi_backpressure_seen_ila_sync => ugt_packager_axi_backpressure_seen_ila_sync,
            o_packager_orbit_exceeds_size_ila_sync    => ugt_packager_orbit_exceeds_size_ila_sync,
            o_packager_Fragment_Header_ila_sync       => ugt_packager_Fragment_Header_ila_sync,
            o_packager_Write_Fragment_Header_ila_sync => ugt_packager_Write_Fragment_Header_ila_sync,
            o_packager_Orbit_Header_ila_sync          => ugt_packager_Orbit_Header_ila_sync,
            o_packager_Write_Orbit_Header_ila_sync    => ugt_packager_Write_Orbit_Header_ila_sync
        );

    ugt_HBM_signal_assignment_loop : for i in N_HBM_PORT_UGT - 1 downto 0 generate
    begin
        scouting_to_HBM_data(i)     <= ugt_scouting_to_HBM_data(i);
        ugt_scouting_to_HBM_ctrl(i) <= scouting_to_HBM_ctrl(i);
        ugt_HBM_almost_full(i)      <= HBM_almost_full(i);
    end generate ugt_HBM_signal_assignment_loop;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- CALO scouting pipeline
    --
    ---------------------------------------------------------------------------
    calo_mgtgtyrxn_in  <= mgtgty_quad135_rxn_in  & mgtgty_quad134_rxn_in;
    calo_mgtgtyrxp_in  <= mgtgty_quad135_rxp_in  & mgtgty_quad134_rxp_in;
    calo_mgtgtytxn_out <= mgtgty_quad135_txn_out & mgtgty_quad134_txn_out;
    calo_mgtgtytxp_out <= mgtgty_quad135_txp_out & mgtgty_quad134_txp_out;

    calo_scouting_pipeline : entity work.calo_scouting_pipeline
        generic map(
            N_QUAD      => N_REGION_CALO,
            N_STREAM    => N_STREAM_CALO,
            N_HBM_PORTS => N_HBM_PORT_CALO,
            QUAD_MAP    => QUAD_MAP_CALO
        )
        port map(
            ---- serial data ports
            mgtgtyrxn_in            => calo_mgtgtyrxn_in,
            mgtgtyrxp_in            => calo_mgtgtyrxp_in,
            mgtgtytxn_out           => calo_mgtgtytxn_out,
            mgtgtytxp_out           => calo_mgtgtytxp_out,

            ---- output for HBM
            scouting_to_HBM_data    => calo_scouting_to_HBM_data,
            scouting_to_HBM_ctrl    => calo_scouting_to_HBM_ctrl,

            ---- HBM backpressure signal
            HBM_almost_full         => calo_HBM_almost_full,

            ---- input clocks
            clk_i2c                 => ena_clock_i2c,
            clk_ref                 => CLK_125MHz,
            clk_freerun             => CLK_100MHz,
            clk_freerun_buf         => clk_freerun_buf,
            mgtrefclk0_x0y0_int     => mgtrefclk0_x0y0_int,
            mgtrefclk0_x0y1_int     => mgtrefclk0_x0y1_int,
            mgtrefclk0_x0y2_int     => mgtrefclk0_x0y2_int,
            mgtrefclk0_x0y3_int     => mgtrefclk0_x0y3_int,
            mgtrefclk0_x0y4_int     => mgtrefclk0_x0y4_int,
            mgtrefclk0_x0y5_int     => mgtrefclk0_x0y5_int,
            mgtrefclk0_x0y7_int     => mgtrefclk0_x0y7_int,
            mgtrefclk0_x0y8_int     => mgtrefclk0_x0y8_int,
            mgtrefclk0_x0y10_int    => mgtrefclk0_x0y10_int,
            mgtrefclk0_x0y11_int    => mgtrefclk0_x0y11_int,
            HBM_clock_buffer        => HBM_clock_buffer,

            ---- external reset signals
            usr_rstp_clk_buffer     => ur_mem_resetp,

            ---- output clocks clocks
            o_clk_rec               => calo_clk_rec,

            ---- monitoring and control
            clk_axi                 => clk_axi,
            axi_rstn                => axi_rstn,
            usr_func_wr             => usr_func_wr,
            usr_wren                => usr_wren,
            usr_data_wr             => usr_data_wr,
            usr_func_rd             => usr_func_rd,
            usr_rden                => usr_rden,
            usr_data_rd             => calo_scout_data_rd,

            ---- inputs module decoder signals
            o_ifcomma_ila_sync      => calo_s_ifcomma_ila_sync,
            o_ifpadding_ila_sync    => calo_s_ifpadding_ila_sync,
            o_ifinvalid_ila_sync    => calo_s_ifinvalid_ila_sync,
            o_ifdata_ila_sync       => calo_s_ifdata_ila_sync,
            o_ifelse_ila_sync       => calo_s_ifelse_ila_sync,

            ---- non-aligned data words signals
            o_d_gap_cleaner_ila_sync     => calo_d_gap_cleaner_ila_sync,
            o_d_inputs_aligner_ila_sync  => calo_d_inputs_aligner_ila_sync,
            o_lid_ila_sync               => calo_lid_ila_sync,
            o_crc_ila_sync               => calo_crc_ila_sync,
            o_d_align_ila_sync           => calo_d_align_ila_sync,
            o_waiting_orbit_end_ila_sync => calo_waiting_orbit_end_ila_sync,

            ---- aligned data words signals
            o_q_align_ila_sync   => calo_q_align_ila_sync,
            o_d_zs_ila_sync      => calo_d_zs_ila_sync,
            o_d_bx_ila_sync      => calo_d_bx_ila_sync,
            o_d_trailer_ila_sync => calo_d_trailer_ila_sync,
            o_d_reshape_ila_sync => calo_d_reshape_ila_sync,
            o_d_package_ila_sync => calo_d_package_ila_sync,

            ---- aligned data control signals
            o_q_ctrl_align_ila_sync   => calo_q_ctrl_align_ila_sync,
            o_d_ctrl_zs_ila_sync      => calo_d_ctrl_zs_ila_sync,
            o_d_ctrl_bx_ila_sync      => calo_d_ctrl_bx_ila_sync,
            o_d_ctrl_trailer_ila_sync => calo_d_ctrl_trailer_ila_sync,
            o_d_ctrl_reshape_ila_sync => calo_d_ctrl_reshape_ila_sync,
            o_d_ctrl_package_ila_sync => calo_d_ctrl_package_ila_sync,

            ---- packager signals
            o_packager_dropped_orbits_ila_sync        => calo_packager_dropped_orbits_ila_sync,
            o_packager_seen_orbits_ila_sync           => calo_packager_seen_orbits_ila_sync,
            o_packager_orbit_length_bxs_ila_sync      => calo_packager_orbit_length_bxs_ila_sync,
            o_packager_axi_backpressure_seen_ila_sync => calo_packager_axi_backpressure_seen_ila_sync,
            o_packager_orbit_exceeds_size_ila_sync    => calo_packager_orbit_exceeds_size_ila_sync,
            o_packager_Fragment_Header_ila_sync       => calo_packager_Fragment_Header_ila_sync,
            o_packager_Write_Fragment_Header_ila_sync => calo_packager_Write_Fragment_Header_ila_sync,
            o_packager_Orbit_Header_ila_sync          => calo_packager_Orbit_Header_ila_sync,
            o_packager_Write_Orbit_Header_ila_sync    => calo_packager_Write_Orbit_Header_ila_sync
        );

    calo_HBM_signal_assignment_loop : for i in N_HBM_PORT_CALO - 1 downto 0 generate
    begin
        scouting_to_HBM_data(N_HBM_PORT_UGT + i) <= calo_scouting_to_HBM_data(i);
        calo_scouting_to_HBM_ctrl(i)             <= scouting_to_HBM_ctrl(N_HBM_PORT_UGT + i);
        calo_HBM_almost_full(i)                  <= HBM_almost_full(N_HBM_PORT_UGT + i);
    end generate calo_HBM_signal_assignment_loop;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- HBM access
    --
    ---------------------------------------------------------------------------
    HBM_RESETn <= not(HBM_RESETp);

    HBM_i1 : entity work.HBM_module
        port map (
            -- Control local interface
            usr_clk                     => usr_clk,
            rst_usr_clk_n               => rst_usr_clk_n,
            usr_func_wr                 => usr_func_wr,
            usr_wen                     => usr_wren,
            usr_data_wr                 => usr_data_wr,

            usr_func_rd                 => usr_func_rd,
            usr_rden                    => usr_rden,
            usr_dto                     => HBM_usr_dto,

            HBM_almost_full             => HBM_almost_full,

            HBM_RESETn                  => HBM_RESETn,
            HBM_clk_ref                 => HBM_clk_ref,
            Clock_HBM                   => clock_HBM,
            resetp_counter_i            => resetp_counters,

            -- DATA to HBM TCP stream
            User_rstn_clock_buffer      => User_rstn_clock_buffer,
            HBM_clock_buffer            => HBM_clock_buffer,

            -- input
            SRx_rcv_in                  => scouting_to_HBM_data,
            -- output
            SRx_rcv_out                 => scouting_to_HBM_ctrl,

            -- Ethernet Connections
            TCP_clock_resetn            => TCP_clock_resetn,
            TCP_clock                   => TCP_clock,
            resetp_counter_timer_resync => resetp_counter_timer_resync_p00 or resetp_counter_timer_resync_p01,

            DAQx_TCP_ack_rcv            => DAQx_TCP_ack_rcv,
            DAQx_TCP_Ack_present        => DAQx_TCP_Ack_present,
            DAQx_rd_TCP_val_rcvo        => DAQx_rd_TCP_val_rcvo,

            -- Packet ready to be read from the Ethernet logic
            DAQx_eth_packet_to_be_sent  => DAQx_eth_packet_to_be_sent,
            DAQx_eth_gnt_access         => DAQx_eth_gnt_access,
            DAQx_eth_ack_packet         => DAQx_eth_ack_packet,     -- pulse to read CS and release the place

            HBM_memory_ready            => HBM_memory_ready
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- UGT scouting
    -- Connection between DAQ and TCP/HBM (Dominique)
    -- Port p00
    --
    ---------------------------------------------------------------------------
    Merger_DAQ_p00 : entity work.Merge_HBM_to_DAQ_x1
        generic map (
            addr_offset_100g_eth        => offset_daq_eth_100gb(0),
            addr_offset_hbm_item        => offset_hbm_item(0)
        )
        port map (
            usr_clk                     => usr_clk,
            usr_rst_n                   => rst_usr_clk_n,
            usr_func_wr                 => usr_func_wr,
            usr_wen                     => usr_wren,
            usr_data_wr                 => usr_data_wr,

            usr_func_rd                 => usr_func_rd,
            usr_rden                    => usr_rden,
            usr_data_rd                 => merger_data_user_p00,

            TCP_clock_resetn            => TCP_clock_resetn,
            TCP_Clock                   => TCP_clock,
            resetp_counter_timer_resync => resetp_counter_timer_resync_p00,

            -- bus merging data source from TCP/HBM block
            TCPxx_BLOCK_TO_SEND         => TCP_BLOCK_TO_SEND_p00,
            ETH_TCP_x_packet_done       => DAQ_TCP_packet_done_p00,

            -- input record (coming from TCP/HBM logic  when a block is ready to be sent
            TCP00_BLOCK_TO_SEND         => DAQx_eth_packet_to_be_sent(0),
            TCP00_gnt_access            => DAQx_eth_gnt_access(0),
            TCP00_packet_done           => DAQx_eth_ack_packet(0)
        );

    Split_p00 : entity work.Split_ETH_ack_x1
        port map(
            reset_n                     => TCP_clock_resetn,
            TCP_clock                   => TCP_clock,

            -- informations from DAQ ethernet
            ETH_TCP_ack_rcvo            => DAQ_TCP_ack_rcvo_p00,        -- receive ACK packet
            ETH_TCP_Px_Ack_present      => DAQ_TCP_Ack_present_p00,
            ETH_rd_TCP_Px_val_rcvo      => rd_DAQ_TCP_val_rcvo_p00,

            -- send the DAQ ethernet packet infos to the coresponding TCP logic (selected by the port#)
            TCP00_ack_rcvo               => DAQx_TCP_ack_rcv(0),         -- receive ACK packet
            TCP00_Px_Ack_present         => DAQx_TCP_Ack_present(0),
            rd_TCP00_val_rcvo            => DAQx_rd_TCP_val_rcvo(0)
        );

    DAQ_Bxxx_p00 : entity work.DAQ_module
        generic map (
            QSFP_port_num           => x"30",
            addr_offset_100G_eth    => offset_daq_eth_100gb(0)
        )
        port map (
            clock_low               => clock_low,                                           -- low clock for ARP probe
            MAC_card                => x"080030F4" & MAC_bytes_low(1) & MAC_bytes_low(0),
            MAC_card_P0             => x"080030F4" & MAC_bytes_low(1) & MAC_bytes_low(0) ,  -- should be the MAC of the port p00
            Ld_MAC_add              => Start_LLDP,

            -- PCIe local interface
            usr_clk                 => usr_clk,
            rst_usr_clk_n           => rst_usr_clk_n,
            usr_func_wr             => usr_func_wr,
            usr_wen                 => usr_wren,
            usr_data_wr             => usr_data_wr,

            usr_func_rd             => usr_func_rd,
            usr_rden                => usr_rden,
            usr_dto                 => dto_DAQ_module_p00,

            -- TCP <--> HBM
            ETH_Clock               => Clock_ETH_p00,                                       -- output ETH clock for port p00

            Usr_resetp_TCP_clock    => TCP_clock_resetp,
            TCP_clock               => TCP_clock,
            TCP_ack_rcvo            => DAQ_TCP_ack_rcvo_p00,                                -- receive ACK packet
            TCP_Px_Ack_present      => DAQ_TCP_Ack_present_p00,
            rd_TCP_Px_val_rcvo      => rd_DAQ_TCP_val_rcvo_p00,

            TCP_BLOCK_TO_SEND       => TCP_BLOCK_TO_SEND_p00,
            TCP_packet_done         => DAQ_TCP_packet_done_p00,

            -- QSFP connection
            QSFP_TX_P               => mgtgty_quad131_txp_out,
            QSFP_TX_N               => mgtgty_quad131_txn_out,
            QSFP_RX_P               => mgtgty_quad131_rxp_in,
            QSFP_RX_N               => mgtgty_quad131_rxn_in,

            -- SERDES/MAC
            SERDES_reset_p          => SERDES_RESET_P,
            init_clock              => CLK_100MHz,
            GT_ref_clock_p          => mgtrefclk0_x0y7_p,
            GT_ref_clock_n          => mgtrefclk0_x0y7_n,
            gt_ref_clk_out          => mgtrefclk0_x0y7_int_free,                            -- bring mgt clock out
            status_out              => status_out_p00
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- CALO scouting
    -- Connection between DAQ and TCP/HBM (Dominique)
    -- Port p01
    --
    ---------------------------------------------------------------------------
    ----
    Merger_DAQ_p01 : entity work.Merge_HBM_to_DAQ_x4
        generic map (
            addr_offset_100g_eth        => offset_daq_eth_100gb(1),
            addr_offset_hbm_item        => offset_hbm_item(0)
        )
        port map (
            usr_clk                     => usr_clk,
            usr_rst_n                   => rst_usr_clk_n,
            usr_func_wr                 => usr_func_wr,
            usr_wen                     => usr_wren,
            usr_data_wr                 => usr_data_wr,

            usr_func_rd                 => usr_func_rd,
            usr_rden                    => usr_rden,
            usr_data_rd                 => merger_data_user_p01,

            TCP_clock_resetn            => TCP_clock_resetn,
            TCP_Clock                   => TCP_clock,
            resetp_counter_timer_resync => resetp_counter_timer_resync_p01,

            -- bus merging data source from TCP/HBM block
            TCPxx_BLOCK_TO_SEND         => TCP_BLOCK_TO_SEND_p01,
            ETH_TCP_x_packet_done       => DAQ_TCP_packet_done_p01,

            -- input record (coming from TCP/HBM logic  when a block is ready to be sent
            TCP00_BLOCK_TO_SEND         => DAQx_eth_packet_to_be_sent(1),
            TCP00_gnt_access            => DAQx_eth_gnt_access(1),
            TCP00_packet_done           => DAQx_eth_ack_packet(1),

            TCP01_BLOCK_TO_SEND         => DAQx_eth_packet_to_be_sent(2),
            TCP01_gnt_access            => DAQx_eth_gnt_access(2),
            TCP01_packet_done           => DAQx_eth_ack_packet(2),

            TCP02_BLOCK_TO_SEND         => DAQx_eth_packet_to_be_sent(3),
            TCP02_gnt_access            => DAQx_eth_gnt_access(3),
            TCP02_packet_done           => DAQx_eth_ack_packet(3),

            TCP03_BLOCK_TO_SEND         => DAQx_eth_packet_to_be_sent(4),
            TCP03_gnt_access            => DAQx_eth_gnt_access(4),
            TCP03_packet_done           => DAQx_eth_ack_packet(4)
        );

    Split_p01 : entity work.Split_ETH_ack_x4
        port map(
            reset_n                     => TCP_clock_resetn,
            TCP_clock                   => TCP_clock,

            -- informations from DAQ ethernet
            ETH_TCP_ack_rcvo            => DAQ_TCP_ack_rcvo_p01,        -- receive ACK packet
            ETH_TCP_Px_Ack_present      => DAQ_TCP_Ack_present_p01,
            ETH_rd_TCP_Px_val_rcvo      => rd_DAQ_TCP_val_rcvo_p01,

            -- send the DAQ ethernet packet infos to the coresponding TCP logic (selected by the port#)
            TCP00_ack_rcvo              => DAQx_TCP_ack_rcv(1),         -- receive ACK packet
            TCP00_Px_Ack_present        => DAQx_TCP_Ack_present(1),
            rd_TCP00_val_rcvo           => DAQx_rd_TCP_val_rcvo(1),

            TCP01_ack_rcvo              => DAQx_TCP_ack_rcv(2),         -- receive ACK packet
            TCP01_Px_Ack_present        => DAQx_TCP_Ack_present(2),
            rd_TCP01_val_rcvo           => DAQx_rd_TCP_val_rcvo(2),

            TCP02_ack_rcvo              => DAQx_TCP_ack_rcv(3),         -- receive ACK packet
            TCP02_Px_Ack_present        => DAQx_TCP_Ack_present(3),
            rd_TCP02_val_rcvo           => DAQx_rd_TCP_val_rcvo(3),

            TCP03_ack_rcvo              => DAQx_TCP_ack_rcv(4),         -- receive ACK packet
            TCP03_Px_Ack_present        => DAQx_TCP_Ack_present(4),
            rd_TCP03_val_rcvo           => DAQx_rd_TCP_val_rcvo(4)
        );

    DAQ_Bxxx_p01 : entity work.DAQ_module
        generic map (
            QSFP_port_num           => x"31",
            addr_offset_100G_eth    => offset_daq_eth_100gb(1)
        )
        port map (
            clock_low               => clock_low,                                           -- low clock for ARP probe
            MAC_card                => x"080030F4" & MAC_bytes_low(3) & MAC_bytes_low(2),
            MAC_card_P0             => x"080030F4" & MAC_bytes_low(1) & MAC_bytes_low(0) ,  -- should be the MAC of the port p00
            Ld_MAC_add              => Start_LLDP,

            -- PCIe local interface
            usr_clk                 => usr_clk,
            rst_usr_clk_n           => rst_usr_clk_n,
            usr_func_wr             => usr_func_wr,
            usr_wen                 => usr_wren,
            usr_data_wr             => usr_data_wr,

            usr_func_rd             => usr_func_rd,
            usr_rden                => usr_rden,
            usr_dto                 => dto_DAQ_module_p01,

            -- TCP <--> HBM
            ETH_Clock               => Clock_ETH_p01,                                       -- output ETH clock for port p01

            Usr_resetp_TCP_clock    => TCP_clock_resetp,
            TCP_clock               => TCP_clock,
            TCP_ack_rcvo            => DAQ_TCP_ack_rcvo_p01,                                -- receive ACK packet
            TCP_Px_Ack_present      => DAQ_TCP_Ack_present_p01,
            rd_TCP_Px_val_rcvo      => rd_DAQ_TCP_val_rcvo_p01,

            TCP_BLOCK_TO_SEND       => TCP_BLOCK_TO_SEND_p01,
            TCP_packet_done         => DAQ_TCP_packet_done_p01,

            -- QSFP connection
            QSFP_TX_P               => mgtgty_quad132_txp_out,
            QSFP_TX_N               => mgtgty_quad132_txn_out,
            QSFP_RX_P               => mgtgty_quad132_rxp_in,
            QSFP_RX_N               => mgtgty_quad132_rxn_in,

            -- SERDES/MAC
            SERDES_reset_p          => SERDES_RESET_P,
            init_clock              => CLK_100MHz,
            GT_ref_clock_p          => mgtrefclk0_x0y8_p,
            GT_ref_clock_n          => mgtrefclk0_x0y8_n,
            gt_ref_clk_out          => mgtrefclk0_x0y8_int_free,                            -- bring mgt clock out
            status_out              => status_out_p01
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- other DAQ modules
    --
    ---------------------------------------------------------------------------
    clock_low <= ena_clock_i2c;

    DAQ_transmit_led : entity work.pulse_led
        port map (
            reset       => rst_usr_clk_n,
            fast_clock  => usr_clk,
            ena_clock   => ena_clock_i2c,
            activity    => status_out_p00(0),
            led_pulse   => led_monitor(0)
        );

    DAQ_receive_led : entity work.pulse_led
        port map (
            reset       => rst_usr_clk_n,
            fast_clock  => usr_clk,
            ena_clock   => ena_clock_i2c,
            activity    => status_out_p00(1),
            led_pulse   => led_monitor(1)
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Debug
    --
    ---------------------------------------------------------------------------
    ---- debug
    process(CLK_100MHz)
    begin
        if rising_edge(CLK_100MHz) then
            debug0_counter  <= debug0_counter + '1';
        end if;
    end process;

    GPIO_LED_0_LS <= led_monitor(0);        -- DAQ transmit
    GPIO_LED_1_LS <= led_monitor(1);        -- DAQ receive
    GPIO_LED_2_LS <= debug0_counter(22);    -- led_monitor(2);
    GPIO_LED_3_LS <= led_monitor(3);
    GPIO_LED_4_LS <= led_monitor(4);
    GPIO_LED_5_LS <= I2C_ena_clock_div(22);
    GPIO_LED_6_LS <= led_monitor(6);
    GPIO_LED_7_LS <= led_monitor(7);
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- I2C clock divider and QSFP28 clock programming (Dominique)
    --
    ---------------------------------------------------------------------------
    process(usr_clk)
    begin
        if rising_edge(usr_clk) then
            I2C_ena_clock_div <= I2C_ena_clock_div + '1';
        end if;
    end process;

    ena_clock_i2c <= '1' when I2C_ena_clock_div(8 downto 0) = "100000000" else '0';

    I2C_reset_n   <= not(I2C_reset_p);

    monitor_itf_i2c_QSFP : entity work.i2c_master_32b_autoread
        port map(
            low_clk             => usr_clk,                             -- clock from PCIe core
            ena_low_clk         => ena_clock_i2c,                       -- lower usr_clk frequency
            reset_n             => I2C_reset_n,
            restart_auto_read   => Start_MAC_read_restart,
            clock               => usr_clk,

            I2C_SDA             => QSFP_MAIN_SDA,
            I2C_SCL             => QSFP_MAIN_SCL,

            dti                 => usr_data_wr(31 downto 0),
            cs                  => '1',
            func                => func_i2c_QSFP_master,
            wen                 => usr_wren,
            rd                  => usr_rden,
            dto                 => dto_i2c_QSFP_monitor(31 downto 0),

            -- direct output of I2C read
            Byte_i2c_out        => MAC_byte,
            Byte_i2c_latch      => MAC_byte_latch,
            ack_rq              => Start_LLDP
        );


    func_i2c_QSFP_master(0) <= '1' when (usr_func_wr(QSFP_i2c_access_a) = '1' and usr_wren = '1' ) or (usr_func_rd(QSFP_i2c_access_a) = '1' and usr_rden = '1' ) else '0'; -- Address; function and Offset
    func_i2c_QSFP_master(1) <= '1' when (usr_func_wr(QSFP_i2c_access_b) = '1' and usr_wren = '1' ) or (usr_func_rd(QSFP_i2c_access_b) = '1' and usr_rden = '1' ) else '0'; -- Data

    -- memorize the MAC lower part read from EEPROM after reset
    process(usr_clk)
    begin
        if rising_edge(usr_clk) then
            for I in 0 to 29 loop
                if MAC_byte_latch = '1'  and Counter_latch_MAC_add = std_logic_vector(to_unsigned(i,8)) then
                    MAC_bytes_low(I)(7 downto 0) <= MAC_byte;
                end if;
            end loop;
        end if;
    end process;

    process(I2C_reset_n,usr_clk)
    begin
        if I2C_reset_n = '0'  then
            Counter_latch_MAC_add <= (others => '0');
        elsif rising_edge(usr_clk) then
            if MAC_byte_latch = '1' then
                Counter_latch_MAC_add <= Counter_latch_MAC_add + '1';
            end if;
        end if;
    end process;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- PCIe interface
    --
    ---------------------------------------------------------------------------
    ---- PCIe interface
    monitor_interface : entity work.PCIe_interface
        port map(
            pcie_rst    => PCIE_rst,
            pcie_clk_n  => PCIE_clk_n,
            pcie_clk_p  => PCIE_clk_p,
            pcie_tx_n   => PCIE_TX0_N,
            pcie_tx_p   => PCIE_TX0_P,
            pcie_rx_n   => PCIE_RX0_N,
            pcie_rx_p   => PCIE_RX0_P,

            usr_clk     => clk_axi,
            usr_rst_n   => axi_rstn,
            usr_func_wr => usr_func_wr,
            usr_wen     => usr_wren,
            usr_data_wr => usr_data_wr,
            usr_func_rd => usr_func_rd,
            usr_rden    => usr_rden,
            usr_data_rd => usr_data_rd,
            usr_rd_val  => delay_retreive_data_back(2)
        );

    usr_clk       <= clk_axi;
    rst_usr_clk_n <= axi_rstn;


    process(clk_axi)
    begin
        if rising_edge(clk_axi) then
            delay_retreive_data_back(2 downto 1) <= delay_retreive_data_back(1 downto 0);
            delay_retreive_data_back(0)          <= usr_rden;
        end if;
    end process;




    ---- scouting ctrl registers
    -- process(axi_rstn, clk_axi)
    -- begin
    --     if axi_rstn = '0' then
    --         --
    --     elsif rising_edge(clk_axi) then

    --         if usr_wren = '1' then

    --         end if;
    --     end if;
    -- end process;



    ---- scouting monitor registers
    process(axi_rstn, clk_axi)
    begin
        if axi_rstn = '0' then

            scout_data_in_rg <= (others => '0');

        elsif rising_edge(clk_axi) then

            if usr_rden = '1' then

                scout_data_in_rg <= (others => '0');

                -- fw/algo version
                if    usr_func_rd(algo_version_func  ) = '1' then scout_data_in_rg(23 downto  0) <= ALGO_REV;
                elsif usr_func_rd(fw_type_func       ) = '1' then scout_data_in_rg(31 downto 24) <= FW_TYPE;

                -- clk frequencies
                elsif usr_func_rd(freq_clk_x0y0_func ) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y0;
                elsif usr_func_rd(freq_clk_x0y1_func ) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y1;
                elsif usr_func_rd(freq_clk_x0y2_func ) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y2;
                elsif usr_func_rd(freq_clk_x0y3_func ) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y3;
                elsif usr_func_rd(freq_clk_x0y4_func ) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y4;
                elsif usr_func_rd(freq_clk_x0y5_func ) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y5;
                elsif usr_func_rd(freq_clk_x0y7_func ) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y7;
                elsif usr_func_rd(freq_clk_x0y8_func ) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y8;
                elsif usr_func_rd(freq_clk_x0y10_func) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y10;
                elsif usr_func_rd(freq_clk_x0y11_func) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_x0y11;
                elsif usr_func_rd(freq_clk_axi_func  ) = '1' then scout_data_in_rg(31 downto 0) <= freq_clk_axi;
                end if;
            end if;
        end if;
    end process;
    ---------------------------------------------------------------------------



    ---- VIO
    central_vio : link_vio
        port map(
            clk             => HBM_clock_buffer,
            probe_in0(0)    => '0',
            probe_in1(0)    => '0',
            probe_in2       => "0",
            probe_in3       => (others => '0'),
            probe_in4       => (others => '0'),
            probe_in5       => (others => '0'),
            probe_in6       => (others => '0'),
            probe_in7       => "0",
            probe_in8       => "0",
            probe_in9       => (others => '0'),
            probe_in10      => (others => '0'),
            probe_in11      => (others => '0'),
            probe_in12      => (others => '0'),
            probe_in13      => (others => '0'),
            probe_in14      => (others => '0'),
            probe_in15      => (others => '0'),
            probe_in16      => (others => '0'),
            probe_in17      => (others => '0'),
            probe_in18      => (others => '0'),
            probe_in19      => (others => '0'),
            probe_in20      => (others => '0'),
            probe_in21      => (others => '0'),
            probe_in22      => (others => '0'),
            probe_in23      => (others => '0'),
            probe_in24      => (others => '0'),
            probe_in25      => (others => '0'),
            probe_in26      => (others => '0'),
            probe_in27      => (others => '0'),
            probe_in28      => std_logic_vector(ugt_packager_seen_orbits_ila_sync(0)),
            probe_in29      => std_logic_vector(ugt_packager_dropped_orbits_ila_sync(0)),
            probe_in30(0)   => ugt_packager_axi_backpressure_seen_ila_sync(0),
            probe_in30(1)   => ugt_packager_orbit_exceeds_size_ila_sync(0),
            probe_in30(2)   => '0',
            probe_in31      => (others => '0'),
            probe_in32      => (others => '0'),
            probe_in33      => (others => '0'),
            probe_in34      => (others => '0'),
            probe_out0      => open,
            probe_out1(0)   => rst_global,
            probe_out2      => open,
            probe_out3      => open,
            probe_out4      => open,
            probe_out5      => open,
            probe_out6      => open,
            probe_out7      => open,
            probe_out8(0)   => open,
            probe_out9(0)   => open,
            probe_out10(0)  => open,
            probe_out11     => open,
            probe_out12(0)  => open,
            probe_out13(0)  => open
        );



    ---- ILA
    ugt_ila_i : ugt_ila
        port map(
            clk                    => HBM_clock_buffer,
            probe0( 31 downto   0) => ugt_d_gap_cleaner_ila_sync(00).data,
            probe0( 63 downto  32) => ugt_d_gap_cleaner_ila_sync(01).data,
            probe0( 95 downto  64) => ugt_d_gap_cleaner_ila_sync(02).data,
            probe0(127 downto  96) => ugt_d_gap_cleaner_ila_sync(03).data,
            probe0(159 downto 128) => ugt_d_gap_cleaner_ila_sync(04).data,
            probe0(191 downto 160) => ugt_d_gap_cleaner_ila_sync(05).data,
            probe0(223 downto 192) => ugt_d_gap_cleaner_ila_sync(06).data,
            probe0(255 downto 224) => ugt_d_gap_cleaner_ila_sync(07).data,
            probe0(287 downto 256) => ugt_d_gap_cleaner_ila_sync(08).data,
            probe0(319 downto 288) => ugt_d_gap_cleaner_ila_sync(09).data,
            probe0(351 downto 320) => ugt_d_gap_cleaner_ila_sync(10).data,
            probe0(383 downto 352) => ugt_d_gap_cleaner_ila_sync(11).data,
            probe0(415 downto 384) => ugt_d_gap_cleaner_ila_sync(12).data,
            probe0(447 downto 416) => ugt_d_gap_cleaner_ila_sync(13).data,
            probe0(479 downto 448) => ugt_d_gap_cleaner_ila_sync(14).data,
            probe0(511 downto 480) => ugt_d_gap_cleaner_ila_sync(15).data,
            probe0(543 downto 512) => ugt_d_gap_cleaner_ila_sync(16).data,
            probe0(575 downto 544) => ugt_d_gap_cleaner_ila_sync(17).data,
            probe0(607 downto 576) => ugt_d_gap_cleaner_ila_sync(18).data,
            probe0(639 downto 608) => ugt_d_gap_cleaner_ila_sync(19).data,
            probe1(00)             => ugt_d_gap_cleaner_ila_sync(00).valid,
            probe1(01)             => ugt_d_gap_cleaner_ila_sync(01).valid,
            probe1(02)             => ugt_d_gap_cleaner_ila_sync(02).valid,
            probe1(03)             => ugt_d_gap_cleaner_ila_sync(03).valid,
            probe1(04)             => ugt_d_gap_cleaner_ila_sync(04).valid,
            probe1(05)             => ugt_d_gap_cleaner_ila_sync(05).valid,
            probe1(06)             => ugt_d_gap_cleaner_ila_sync(06).valid,
            probe1(07)             => ugt_d_gap_cleaner_ila_sync(07).valid,
            probe1(08)             => ugt_d_gap_cleaner_ila_sync(08).valid,
            probe1(09)             => ugt_d_gap_cleaner_ila_sync(09).valid,
            probe1(10)             => ugt_d_gap_cleaner_ila_sync(10).valid,
            probe1(11)             => ugt_d_gap_cleaner_ila_sync(11).valid,
            probe1(12)             => ugt_d_gap_cleaner_ila_sync(12).valid,
            probe1(13)             => ugt_d_gap_cleaner_ila_sync(13).valid,
            probe1(14)             => ugt_d_gap_cleaner_ila_sync(14).valid,
            probe1(15)             => ugt_d_gap_cleaner_ila_sync(15).valid,
            probe1(16)             => ugt_d_gap_cleaner_ila_sync(16).valid,
            probe1(17)             => ugt_d_gap_cleaner_ila_sync(17).valid,
            probe1(18)             => ugt_d_gap_cleaner_ila_sync(18).valid,
            probe1(19)             => ugt_d_gap_cleaner_ila_sync(19).valid,
            probe2(00)             => ugt_d_gap_cleaner_ila_sync(00).strobe,
            probe2(01)             => ugt_d_gap_cleaner_ila_sync(01).strobe,
            probe2(02)             => ugt_d_gap_cleaner_ila_sync(02).strobe,
            probe2(03)             => ugt_d_gap_cleaner_ila_sync(03).strobe,
            probe2(04)             => ugt_d_gap_cleaner_ila_sync(04).strobe,
            probe2(05)             => ugt_d_gap_cleaner_ila_sync(05).strobe,
            probe2(06)             => ugt_d_gap_cleaner_ila_sync(06).strobe,
            probe2(07)             => ugt_d_gap_cleaner_ila_sync(07).strobe,
            probe2(08)             => ugt_d_gap_cleaner_ila_sync(08).strobe,
            probe2(09)             => ugt_d_gap_cleaner_ila_sync(09).strobe,
            probe2(10)             => ugt_d_gap_cleaner_ila_sync(10).strobe,
            probe2(11)             => ugt_d_gap_cleaner_ila_sync(11).strobe,
            probe2(12)             => ugt_d_gap_cleaner_ila_sync(12).strobe,
            probe2(13)             => ugt_d_gap_cleaner_ila_sync(13).strobe,
            probe2(14)             => ugt_d_gap_cleaner_ila_sync(14).strobe,
            probe2(15)             => ugt_d_gap_cleaner_ila_sync(15).strobe,
            probe2(16)             => ugt_d_gap_cleaner_ila_sync(16).strobe,
            probe2(17)             => ugt_d_gap_cleaner_ila_sync(17).strobe,
            probe2(18)             => ugt_d_gap_cleaner_ila_sync(18).strobe,
            probe2(19)             => ugt_d_gap_cleaner_ila_sync(19).strobe,
            probe3( 31 downto   0) => ugt_d_align_ila_sync(00).data,
            probe3( 63 downto  32) => ugt_d_align_ila_sync(01).data,
            probe3( 95 downto  64) => ugt_d_align_ila_sync(02).data,
            probe3(127 downto  96) => ugt_d_align_ila_sync(03).data,
            probe3(159 downto 128) => ugt_d_align_ila_sync(04).data,
            probe3(191 downto 160) => ugt_d_align_ila_sync(05).data,
            probe3(223 downto 192) => ugt_d_align_ila_sync(06).data,
            probe3(255 downto 224) => ugt_d_align_ila_sync(07).data,
            probe3(287 downto 256) => ugt_d_align_ila_sync(08).data,
            probe3(319 downto 288) => ugt_d_align_ila_sync(09).data,
            probe3(351 downto 320) => ugt_d_align_ila_sync(10).data,
            probe3(383 downto 352) => ugt_d_align_ila_sync(11).data,
            probe3(415 downto 384) => ugt_d_align_ila_sync(12).data,
            probe3(447 downto 416) => ugt_d_align_ila_sync(13).data,
            probe3(479 downto 448) => ugt_d_align_ila_sync(14).data,
            probe3(511 downto 480) => ugt_d_align_ila_sync(15).data,
            probe3(543 downto 512) => ugt_d_align_ila_sync(16).data,
            probe3(575 downto 544) => ugt_d_align_ila_sync(17).data,
            probe3(607 downto 576) => ugt_d_align_ila_sync(18).data,
            probe3(639 downto 608) => ugt_d_align_ila_sync(19).data,
            probe4(00)             => ugt_d_align_ila_sync(00).valid,
            probe4(01)             => ugt_d_align_ila_sync(01).valid,
            probe4(02)             => ugt_d_align_ila_sync(02).valid,
            probe4(03)             => ugt_d_align_ila_sync(03).valid,
            probe4(04)             => ugt_d_align_ila_sync(04).valid,
            probe4(05)             => ugt_d_align_ila_sync(05).valid,
            probe4(06)             => ugt_d_align_ila_sync(06).valid,
            probe4(07)             => ugt_d_align_ila_sync(07).valid,
            probe4(08)             => ugt_d_align_ila_sync(08).valid,
            probe4(09)             => ugt_d_align_ila_sync(09).valid,
            probe4(10)             => ugt_d_align_ila_sync(10).valid,
            probe4(11)             => ugt_d_align_ila_sync(11).valid,
            probe4(12)             => ugt_d_align_ila_sync(12).valid,
            probe4(13)             => ugt_d_align_ila_sync(13).valid,
            probe4(14)             => ugt_d_align_ila_sync(14).valid,
            probe4(15)             => ugt_d_align_ila_sync(15).valid,
            probe4(16)             => ugt_d_align_ila_sync(16).valid,
            probe4(17)             => ugt_d_align_ila_sync(17).valid,
            probe4(18)             => ugt_d_align_ila_sync(18).valid,
            probe4(19)             => ugt_d_align_ila_sync(19).valid,
            probe5(00)             => ugt_d_align_ila_sync(00).strobe,
            probe5(01)             => ugt_d_align_ila_sync(01).strobe,
            probe5(02)             => ugt_d_align_ila_sync(02).strobe,
            probe5(03)             => ugt_d_align_ila_sync(03).strobe,
            probe5(04)             => ugt_d_align_ila_sync(04).strobe,
            probe5(05)             => ugt_d_align_ila_sync(05).strobe,
            probe5(06)             => ugt_d_align_ila_sync(06).strobe,
            probe5(07)             => ugt_d_align_ila_sync(07).strobe,
            probe5(08)             => ugt_d_align_ila_sync(08).strobe,
            probe5(09)             => ugt_d_align_ila_sync(09).strobe,
            probe5(10)             => ugt_d_align_ila_sync(10).strobe,
            probe5(11)             => ugt_d_align_ila_sync(11).strobe,
            probe5(12)             => ugt_d_align_ila_sync(12).strobe,
            probe5(13)             => ugt_d_align_ila_sync(13).strobe,
            probe5(14)             => ugt_d_align_ila_sync(14).strobe,
            probe5(15)             => ugt_d_align_ila_sync(15).strobe,
            probe5(16)             => ugt_d_align_ila_sync(16).strobe,
            probe5(17)             => ugt_d_align_ila_sync(17).strobe,
            probe5(18)             => ugt_d_align_ila_sync(18).strobe,
            probe5(19)             => ugt_d_align_ila_sync(19).strobe,
            probe6( 31 downto   0) => ugt_d_bx_ila_sync(0)(00),
            probe6( 63 downto  32) => ugt_d_bx_ila_sync(0)(01),
            probe6( 95 downto  64) => ugt_d_bx_ila_sync(0)(02),
            probe6(127 downto  96) => ugt_d_bx_ila_sync(0)(03),
            probe6(159 downto 128) => ugt_d_bx_ila_sync(0)(04),
            probe6(191 downto 160) => ugt_d_bx_ila_sync(0)(05),
            probe6(223 downto 192) => ugt_d_bx_ila_sync(0)(06),
            probe6(255 downto 224) => ugt_d_bx_ila_sync(0)(07),
            probe7(0)              => ugt_d_ctrl_bx_ila_sync(0).valid,
            probe7(1)              => ugt_d_ctrl_bx_ila_sync(0).strobe,
            probe7(2)              => ugt_d_ctrl_bx_ila_sync(0).bx_start,
            probe7(3)              => ugt_d_ctrl_bx_ila_sync(0).last,
            probe7(4)              => ugt_d_ctrl_bx_ila_sync(0).header,
            probe7( 20 downto   5) => std_logic_vector(to_unsigned(ugt_d_ctrl_bx_ila_sync(0).bx_counter, 16)),
            probe8                 => ugt_HBM_almost_full
        );

    calo_ila_i : calo_ila
        port map(
            clk                    => HBM_clock_buffer,
            probe0( 31 downto   0) => calo_d_gap_cleaner_ila_sync(00).data,
            probe0( 63 downto  32) => calo_d_gap_cleaner_ila_sync(01).data,
            probe0( 95 downto  64) => calo_d_gap_cleaner_ila_sync(02).data,
            probe0(127 downto  96) => calo_d_gap_cleaner_ila_sync(03).data,
            probe0(159 downto 128) => calo_d_gap_cleaner_ila_sync(04).data,
            probe0(191 downto 160) => calo_d_gap_cleaner_ila_sync(05).data,
            probe0(223 downto 192) => calo_d_gap_cleaner_ila_sync(06).data,
            probe0(255 downto 224) => calo_d_gap_cleaner_ila_sync(07).data,
            probe1(00)             => calo_d_gap_cleaner_ila_sync(00).valid,
            probe1(01)             => calo_d_gap_cleaner_ila_sync(01).valid,
            probe1(02)             => calo_d_gap_cleaner_ila_sync(02).valid,
            probe1(03)             => calo_d_gap_cleaner_ila_sync(03).valid,
            probe1(04)             => calo_d_gap_cleaner_ila_sync(04).valid,
            probe1(05)             => calo_d_gap_cleaner_ila_sync(05).valid,
            probe1(06)             => calo_d_gap_cleaner_ila_sync(06).valid,
            probe1(07)             => calo_d_gap_cleaner_ila_sync(07).valid,
            probe2(00)             => calo_d_gap_cleaner_ila_sync(00).strobe,
            probe2(01)             => calo_d_gap_cleaner_ila_sync(01).strobe,
            probe2(02)             => calo_d_gap_cleaner_ila_sync(02).strobe,
            probe2(03)             => calo_d_gap_cleaner_ila_sync(03).strobe,
            probe2(04)             => calo_d_gap_cleaner_ila_sync(04).strobe,
            probe2(05)             => calo_d_gap_cleaner_ila_sync(05).strobe,
            probe2(06)             => calo_d_gap_cleaner_ila_sync(06).strobe,
            probe2(07)             => calo_d_gap_cleaner_ila_sync(07).strobe,
            probe3( 31 downto   0) => calo_d_align_ila_sync(00).data,
            probe3( 63 downto  32) => calo_d_align_ila_sync(01).data,
            probe3( 95 downto  64) => calo_d_align_ila_sync(02).data,
            probe3(127 downto  96) => calo_d_align_ila_sync(03).data,
            probe3(159 downto 128) => calo_d_align_ila_sync(04).data,
            probe3(191 downto 160) => calo_d_align_ila_sync(05).data,
            probe3(223 downto 192) => calo_d_align_ila_sync(06).data,
            probe3(255 downto 224) => calo_d_align_ila_sync(07).data,
            probe4(00)             => calo_d_align_ila_sync(00).valid,
            probe4(01)             => calo_d_align_ila_sync(01).valid,
            probe4(02)             => calo_d_align_ila_sync(02).valid,
            probe4(03)             => calo_d_align_ila_sync(03).valid,
            probe4(04)             => calo_d_align_ila_sync(04).valid,
            probe4(05)             => calo_d_align_ila_sync(05).valid,
            probe4(06)             => calo_d_align_ila_sync(06).valid,
            probe4(07)             => calo_d_align_ila_sync(07).valid,
            probe5(00)             => calo_d_align_ila_sync(00).strobe,
            probe5(01)             => calo_d_align_ila_sync(01).strobe,
            probe5(02)             => calo_d_align_ila_sync(02).strobe,
            probe5(03)             => calo_d_align_ila_sync(03).strobe,
            probe5(04)             => calo_d_align_ila_sync(04).strobe,
            probe5(05)             => calo_d_align_ila_sync(05).strobe,
            probe5(06)             => calo_d_align_ila_sync(06).strobe,
            probe5(07)             => calo_d_align_ila_sync(07).strobe,
            probe6( 31 downto   0) => calo_d_zs_ila_sync(00),
            probe6( 63 downto  32) => calo_d_zs_ila_sync(01),
            probe6( 95 downto  64) => calo_d_zs_ila_sync(02),
            probe6(127 downto  96) => calo_d_zs_ila_sync(03),
            probe6(159 downto 128) => calo_d_zs_ila_sync(04),
            probe6(191 downto 160) => calo_d_zs_ila_sync(05),
            probe6(223 downto 192) => calo_d_zs_ila_sync(06),
            probe6(255 downto 224) => calo_d_zs_ila_sync(07),
            probe7(0)              => calo_d_ctrl_zs_ila_sync.valid,
            probe7(1)              => calo_d_ctrl_zs_ila_sync.strobe,
            probe7(2)              => calo_d_ctrl_zs_ila_sync.bx_start,
            probe7(3)              => calo_d_ctrl_zs_ila_sync.last,
            probe7(4)              => calo_d_ctrl_zs_ila_sync.header,
            probe7( 20 downto   5) => std_logic_vector(to_unsigned(calo_d_ctrl_zs_ila_sync.bx_counter, 16)),
            probe8                 => calo_HBM_almost_full
        );
    ---------------------------------------------------------------------------

end Behavioral;
