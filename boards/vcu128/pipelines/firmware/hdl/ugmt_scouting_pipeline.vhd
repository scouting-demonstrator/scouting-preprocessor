library ieee;
library unisim;
library xpm;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use unisim.vcomponents.all;

use work.top_decl.all;
use work.algo_decl.all;
use work.datatypes.all;
use work.address_table.all;
use work.interface.all;





entity ugmt_scouting_pipeline is
    generic(
        N_QUAD      : integer;
        N_STREAM    : integer;
        N_HBM_PORTS : integer;
        QUAD_MAP    : TQuadMap(N_QUAD - 1 downto 0)
    );
    port(
        ---- serial data ports
        mgtgtyrxn_in                                : in  std_logic_vector(4*N_QUAD - 1 downto 0);
        mgtgtyrxp_in                                : in  std_logic_vector(4*N_QUAD - 1 downto 0);
        mgtgtytxn_out                               : out std_logic_vector(4*N_QUAD - 1 downto 0);
        mgtgtytxp_out                               : out std_logic_vector(4*N_QUAD - 1 downto 0);

        ---- output for HBM
        scouting_to_HBM_data                        : out TStream_HBM_data(N_HBM_PORTS-1 downto 0);
        scouting_to_HBM_ctrl                        : in  TStream_HBM_ctrl(N_HBM_PORTS-1 downto 0);

        ---- input clocks
        clk_i2c                                     : in std_logic;
        clk_ref                                     : in std_logic;
        clk_freerun                                 : in std_logic;
        clk_freerun_buf                             : in std_logic;
        mgtrefclk0_x0y0_int                         : in std_logic;
        mgtrefclk0_x0y1_int                         : in std_logic;
        mgtrefclk0_x0y2_int                         : in std_logic;
        mgtrefclk0_x0y3_int                         : in std_logic;
        mgtrefclk0_x0y4_int                         : in std_logic;
        mgtrefclk0_x0y5_int                         : in std_logic;
        mgtrefclk0_x0y7_int                         : in std_logic;
        mgtrefclk0_x0y8_int                         : in std_logic;
        mgtrefclk0_x0y10_int                        : in std_logic;
        mgtrefclk0_x0y11_int                        : in std_logic;
        HBM_clock_buffer                            : in std_logic;

        ---- HBM backpressure signal
        HBM_almost_full                             : in std_logic_vector(N_HBM_PORTS - 1 downto 0);

        ---- external reset signals
        usr_rstp_clk_buffer                         : in std_logic;

        ---- output clocks clocks
        o_clk_rec                                   : out std_logic_vector(4*N_QUAD - 1 downto 0);

        ---- monitoring and control
        clk_axi                                     : in  std_logic;
        axi_rstn                                    : in  std_logic;
        usr_func_wr                                 : in  std_logic_vector(16383 downto 0);
        usr_wren                                    : in  std_logic;
        usr_data_wr                                 : in  std_logic_vector(63 downto 0);
        usr_func_rd                                 : in  std_logic_vector(16383 downto 0);
        usr_rden                                    : in  std_logic;
        usr_data_rd                                 : out std_logic_vector(63 downto 0);

        ---- inputs module decoder signals
        o_ifcomma_ila_sync                          : out std_logic_vector(4*N_QUAD - 1 downto 0);
        o_ifpadding_ila_sync                        : out std_logic_vector(4*N_QUAD - 1 downto 0);
        o_ifinvalid_ila_sync                        : out std_logic_vector(4*N_QUAD - 1 downto 0);
        o_ifdata_ila_sync                           : out std_logic_vector(4*N_QUAD - 1 downto 0);
        o_ifelse_ila_sync                           : out std_logic_vector(4*N_QUAD - 1 downto 0);

        ---- non-aligned data words signals
        o_d_gap_cleaner_ila_sync                    : out ldata(4*N_QUAD - 1 downto 0);
        o_d_inputs_aligner_ila_sync                 : out ldata(4*N_QUAD - 1 downto 0);
        o_lid_ila_sync                              : out TAuxInfo(4*N_QUAD - 1 downto 0);
        o_crc_ila_sync                              : out TAuxInfo(4*N_QUAD - 1 downto 0);
        o_d_align_ila_sync                          : out ldata(4*N_QUAD - 1 downto 0);
        o_waiting_orbit_end_ila_sync                : out std_logic;

        ---- aligned data words signals
        o_q_align_ila_sync                          : out adata(4*N_QUAD - 1 downto 0);
        o_d_zs_ila_sync                             : out adata(4*N_QUAD - 1 downto 0);
        o_d_bx_ila_sync                             : out TStream_aframe(N_HBM_PORTS - 1 downto 0);
        o_d_trailer_ila_sync                        : out TStream_aframe(N_HBM_PORTS - 1 downto 0);
        o_d_reshape_ila_sync                        : out TStream_aframe(N_HBM_PORTS - 1 downto 0);
        o_d_package_ila_sync                        : out TStream_aframe(N_HBM_PORTS - 1 downto 0);

        ---- aligned data control signals
        o_q_ctrl_align_ila_sync                     : out acontrol;
        o_d_ctrl_zs_ila_sync                        : out acontrol;
        o_d_ctrl_bx_ila_sync                        : out TStream_acontrol(N_HBM_PORTS - 1 downto 0);
        o_d_ctrl_trailer_ila_sync                   : out TStream_acontrol(N_HBM_PORTS - 1 downto 0);
        o_d_ctrl_reshape_ila_sync                   : out TStream_acontrol(N_HBM_PORTS - 1 downto 0);
        o_d_ctrl_package_ila_sync                   : out TStream_acontrol(N_HBM_PORTS - 1 downto 0);

        ---- packager signals
        o_packager_dropped_orbits_ila_sync          : out packager_tcounter(N_HBM_PORTS - 1 downto 0);
        o_packager_seen_orbits_ila_sync             : out packager_tcounter(N_HBM_PORTS - 1 downto 0);
        o_packager_orbit_length_bxs_ila_sync        : out packager_tcounter(N_HBM_PORTS - 1 downto 0);
        o_packager_axi_backpressure_seen_ila_sync   : out std_logic_vector(N_HBM_PORTS - 1 downto 0);
        o_packager_orbit_exceeds_size_ila_sync      : out std_logic_vector(N_HBM_PORTS - 1 downto 0);
        o_packager_Fragment_Header_ila_sync         : out packager_tdata(N_HBM_PORTS - 1 downto 0);
        o_packager_Write_Fragment_Header_ila_sync   : out packager_tbit(N_HBM_PORTS - 1 downto 0);
        o_packager_Orbit_Header_ila_sync            : out packager_tdata(N_HBM_PORTS - 1 downto 0);
        o_packager_Write_Orbit_Header_ila_sync      : out packager_tbit(N_HBM_PORTS - 1 downto 0)
    );
end ugmt_scouting_pipeline;





architecture Behavioral of ugmt_scouting_pipeline is

    ---- reset
    signal local_reset                                  : std_logic;
    signal hb0_gtwiz_reset_tx_pll_and_datapath_int      : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_datapath_int              : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_datapath_vio_int           : std_logic_vector(0 downto 0);
    signal rst_packager                                 : std_logic;
    signal rst_link                                     : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal rst_link_hbm_sync                            : std_logic_vector(4*N_QUAD - 1 downto 0);

    ---- GTWIZARD signals
    signal gen_orbit_full_length                        : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(3564, 16));
    signal gen_orbit_data_length                        : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(150, 16));
    signal gen_orbit_full_length_hbm_sync               : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(3564, 16));
    signal gen_orbit_data_length_hbm_sync               : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(150, 16));

    ---- input links rx GTWIZARD signals: 1 x QUAD
    -- init done
    signal init_done_int                                : std_logic_vector(  N_QUAD - 1 downto 0);
    signal init_done_axi_sync                           : std_logic_vector(  N_QUAD - 1 downto 0);
    signal init_retry_ctr                               : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal init_retry_ctr_axi_sync                      : std_logic_vector(4*N_QUAD - 1 downto 0);
    -- reset rx/tx done
    signal gtwiz_reset_tx_done_free_sync                : std_logic_vector(  N_QUAD - 1 downto 0);
    signal gtwiz_reset_tx_done_axi_sync                 : std_logic_vector(  N_QUAD - 1 downto 0);
    signal gtwiz_reset_rx_done_free_sync                : std_logic_vector(  N_QUAD - 1 downto 0);
    signal gtwiz_reset_rx_done_axi_sync                 : std_logic_vector(  N_QUAD - 1 downto 0);
    -- clock data recovery stable
    signal cdr_stable                                   : std_logic_vector(  N_QUAD - 1 downto 0);
    signal cdr_stable_axi_sync                          : std_logic_vector(  N_QUAD - 1 downto 0);

    ---- input links rx GTWIZARD signals: 1 x link
    signal gtpowergood_free_sync                        : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal gtpowergood_axi_sync                         : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal txpmaresetdone_free_sync                     : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal txpmaresetdone_axi_sync                      : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal rxpmaresetdone_free_sync                     : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal rxpmaresetdone_axi_sync                      : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal loopback                                     : std_logic_vector(3*4*N_QUAD - 1 downto 0);
    -- rx data
    signal rx_data                                      : std_logic_vector(32*4*N_QUAD - 1 downto 0);
    -- byte alignment
    signal sRxbyteisaligned                             : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal sRxbyteisaligned_axi_sync                    : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal sCommaDet                                    : std_logic_vector(4*N_QUAD - 1 downto 0);
    -- rxctrl 8b/10b decoding signals
    signal rxctrl0_int                                  : std_logic_vector(16*4*N_QUAD - 1 downto 0);
    signal rxctrl1_int                                  : std_logic_vector(16*4*N_QUAD - 1 downto 0);
    signal rxctrl2_int                                  : std_logic_vector( 8*4*N_QUAD - 1 downto 0);
    signal rxctrl3_int                                  : std_logic_vector( 8*4*N_QUAD - 1 downto 0);
    -- input data type decoding
    signal s_ifcomma, s_ifcomma_ila_sync                : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal s_ifpadding, s_ifpadding_ila_sync            : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal s_ifinvalid, s_ifinvalid_ila_sync            : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal s_ifdata, s_ifdata_ila_sync                  : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal s_ifelse, s_ifelse_ila_sync                  : std_logic_vector(4*N_QUAD - 1 downto 0);

    ---- input links rx GTWIZARD signals: 1 x ip
    signal hb0_gtwiz_reset_tx_pll_and_datapath_axi_sync : std_logic;
    signal hb0_gtwiz_reset_tx_datapath_axi_sync         : std_logic;
    signal hb_gtwiz_reset_rx_datapath_axi_sync          : std_logic;

    ---- input links rx GTWIZARD signals: clocking
    signal clk_rec                                      : std_logic_vector(4*N_QUAD - 1 downto 0);

    ---- GAP_CLEANER/ALIGNER/ZS/RESHAPE signals
    signal q_inputs                                     : ldata(4*N_QUAD - 1 downto 0);
    signal q_inputs_d1                                  : ldata(4*N_QUAD - 1 downto 0);
    signal q_inputs_d2                                  : ldata(4*N_QUAD - 1 downto 0);
    signal q_inputs_d3                                  : ldata(4*N_QUAD - 1 downto 0);
    -- link id and crc (at the end of the orbit)
    signal lid, lid_ila_sync                            : TAuxInfo(4*N_QUAD - 1 downto 0);
    signal crc, crc_ila_sync                            : TAuxInfo(4*N_QUAD - 1 downto 0);
    signal crc_error_counters                           : TCounter32b(4*N_QUAD - 1 downto 0);
    signal crc_error_counters_axi_sync                  : TAuxInfo(4*N_QUAD - 1 downto 0);

    -- link enable/disable
    signal stream_enable_mask                           : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal stream_enable_mask_hbm_sync                  : std_logic_vector(4*N_QUAD - 1 downto 0);
    -- gap cleaner
    signal d_gap_cleaner                                : ldata(4*N_QUAD - 1 downto 0);
    signal d_gap_cleaner_ila_sync                       : ldata(4*N_QUAD - 1 downto 0);
    -- inputs aligner
    signal d_inputs_aligner                             : ldata(4*N_QUAD - 1 downto 0);
    signal d_inputs_aligner_ila_sync                    : ldata(4*N_QUAD - 1 downto 0);
    signal q_inputs_aligner                             : ldata(4*N_QUAD - 1 downto 0);
    -- data generator
    signal q_gen                                        : ldata(4*N_QUAD - 1 downto 0);
    -- aligner
    signal d_align                                      : ldata(4*N_QUAD - 1 downto 0);
    signal d_align_ila_sync                             : ldata(4*N_QUAD - 1 downto 0);
    signal d_align_ila_sync_del1                        : ldata(4*N_QUAD - 1 downto 0);
    signal d_align_ila_sync_del2                        : ldata(4*N_QUAD - 1 downto 0);
    signal d_align_ila_sync_delayed                     : ldata(4*N_QUAD - 1 downto 0);
    -- aligner output
    signal q_align                                      : adata(4*N_QUAD - 1 downto 0);
    signal q_align_d1                                   : adata(4*N_QUAD - 1 downto 0);
    signal q_align_d2                                   : adata(4*N_QUAD - 1 downto 0);
    signal q_align_d3                                   : adata(4*N_QUAD - 1 downto 0);
    -- zero suppression
    signal d_zs                                         : adata(4*N_QUAD - 1 downto 0);
    signal q_zs                                         : TStream_aframe(N_HBM_PORTS - 1 downto 0);
    -- trailer generator
    signal d_bx                                         : TStream_aframe(N_HBM_PORTS - 1 downto 0);
    signal d_trailer                                    : TStream_aframe(N_HBM_PORTS - 1 downto 0);
    signal q_trailer                                    : TStream_aframe(N_HBM_PORTS - 1 downto 0);
    -- algo to apply to data
    signal d_reshape                                    : TStream_aframe(N_HBM_PORTS - 1 downto 0);
    signal q_reshape                                    : TStream_aframe(N_HBM_PORTS - 1 downto 0);
    -- packager
    signal d_package                                    : TStream_aframe(N_HBM_PORTS - 1 downto 0);

    -- control signals
    signal q_ctrl_align                                 : acontrol;
    signal q_ctrl_align_d1                              : acontrol;
    signal q_ctrl_align_d2                              : acontrol;
    signal q_ctrl_align_d3                              : acontrol;
    signal q_ctrl_gen                                   : acontrol;
    signal d_ctrl_zs                                    : acontrol;
    signal q_ctrl_zs                                    : TStream_acontrol(N_HBM_PORTS - 1 downto 0);
    signal d_ctrl_bx                                    : TStream_acontrol(N_HBM_PORTS - 1 downto 0);
    signal d_ctrl_trailer                               : TStream_acontrol(N_HBM_PORTS - 1 downto 0);
    signal q_ctrl_trailer                               : TStream_acontrol(N_HBM_PORTS - 1 downto 0);
    signal d_ctrl_reshape                               : TStream_acontrol(N_HBM_PORTS - 1 downto 0);
    signal q_ctrl_reshape                               : TStream_acontrol(N_HBM_PORTS - 1 downto 0);
    signal d_ctrl_package                               : TStream_acontrol(N_HBM_PORTS - 1 downto 0);

    signal rst_aligner                                  : std_logic;

    signal disable_zs                                   : std_logic := '0';
    signal disable_zs_hbm_sync                          : std_logic := '0';
    signal disable_reshape                              : std_logic := '0';
    signal disable_reshape_hbm_sync                     : std_logic := '0';

    signal enable_data_gen                              : std_logic := '0';
    signal enable_data_gen_hbm_sync                     : std_logic := '0';

    ---- packager signals
    signal orbits_per_packet                            : std_logic_vector(15 downto 0) := x"0001";
    signal orbits_per_packet_axi_sync                   : std_logic_vector(15 downto 0) := x"0001";
    signal orbits_per_packet_hbm_sync                   : std_logic_vector(15 downto 0) := x"0001";
    signal orbits_per_chunk                             : std_logic_vector(19 downto 0) := x"01000";
    signal orbits_per_chunk_axi_sync                    : std_logic_vector(19 downto 0) := x"01000";
    signal orbits_per_chunk_hbm_sync                    : std_logic_vector(19 downto 0) := x"01000";
    signal wait_for_oc1                                 : std_logic := '0';
    signal wait_for_oc1_axi_sync                        : std_logic := '0';
    signal wait_for_oc1_hbm_sync                        : std_logic := '0';

    signal autorealign_counter                          : unsigned(63 downto 0);
    signal autorealign_counter_axi_sync                 : std_logic_vector(31 downto 0);

    signal waiting_orbit_end                            : std_logic;
    signal waiting_orbit_end_axi_sync                   : std_logic;
    signal waiting_orbit_end_free_sync                  : std_logic;

    signal packager_out_tdata                           : packager_tdata(N_HBM_PORTS-1 downto 0);
    signal packager_out_tlast                           : packager_tlast(N_HBM_PORTS-1 downto 0);
    signal packager_out_tvalid                          : packager_tvalid(N_HBM_PORTS-1 downto 0);
    signal packager_in_tready                           : packager_tready(N_HBM_PORTS-1 downto 0);
    signal packager_out_tkeep                           : packager_tkeep(N_HBM_PORTS-1 downto 0);
    signal packager_out_tuser                           : packager_tuser(N_HBM_PORTS-1 downto 0);

    signal packager_dropped_orbits                      : packager_tcounter(N_HBM_PORTS-1 downto 0);
    signal packager_seen_orbits                         : packager_tcounter(N_HBM_PORTS-1 downto 0);
    signal packager_orbit_length_bxs                    : packager_tcounter(N_HBM_PORTS-1 downto 0);
    signal packager_axi_backpressure_seen               : std_logic_vector(N_HBM_PORTS-1 downto 0);
    signal packager_orbit_exceeds_size                  : std_logic_vector(N_HBM_PORTS-1 downto 0);

    signal packager_dropped_orbits_axi_sync             : TBus32b(N_HBM_PORTS-1 downto 0);
    signal packager_seen_orbits_axi_sync                : TBus32b(N_HBM_PORTS-1 downto 0);
    signal packager_orbit_length_bxs_axi_sync           : TBus32b(N_HBM_PORTS-1 downto 0);
    signal packager_axi_backpressure_seen_axi_sync      : std_logic_vector(N_HBM_PORTS-1 downto 0);
    signal packager_orbit_exceeds_size_axi_sync         : std_logic_vector(N_HBM_PORTS-1 downto 0);

    signal packager_Fragment_Header                     : packager_tdata(N_HBM_PORTS-1 downto 0);
    signal packager_Write_Fragment_Header               : packager_tbit(N_HBM_PORTS-1 downto 0);
    signal packager_Orbit_Header                        : packager_tdata(N_HBM_PORTS-1 downto 0);
    signal packager_Write_Orbit_Header                  : packager_tbit(N_HBM_PORTS-1 downto 0);

    ---- memory block builder
    signal almost_full_buffer                           : std_logic_vector(N_HBM_PORTS-1 downto 0);

    -- clk frequency measurement
    signal freq_clk_rec                                 : TAuxInfo(4*N_QUAD - 1 downto 0);
    signal freq_clk_rec_axi_sync                        : TAuxInfo(4*N_QUAD - 1 downto 0);

    ---- Link mapping
    --  0,  1,  2,  3 : QUAD_124
    --  4,  5,  6,  7 : QUAD_125
    --  8,  9, 10, 11 : QUAD_126
    -- 12, 13, 14, 15 : QUAD_127
    -- 16, 17, 18, 19 : QUAD_128
    -- 20, 21, 22, 23 : QUAD_129
    type link_map_t is array(natural range <>) of std_logic_vector(4 downto 0);
    signal link_map : link_map_t(4*N_QUAD - 1 downto 0) := (
         0 => std_logic_vector(to_unsigned( 0, 5)),
         1 => std_logic_vector(to_unsigned( 1, 5)),
         2 => std_logic_vector(to_unsigned( 2, 5)),
         3 => std_logic_vector(to_unsigned( 3, 5))
    );
    signal link_map_hbm_sync : link_map_t(4*N_QUAD - 1 downto 0) := (
         0 => std_logic_vector(to_unsigned( 0, 5)),
         1 => std_logic_vector(to_unsigned( 1, 5)),
         2 => std_logic_vector(to_unsigned( 2, 5)),
         3 => std_logic_vector(to_unsigned( 3, 5))
   );

    ---- scouting source id to place in scouting packet header
    signal scouting_source_id           : TBus32b(N_HBM_PORTS - 1 downto 0);
    signal scouting_source_id_hbm_sync  : TBus32b(N_HBM_PORTS - 1 downto 0);

begin

    ---------------------------------------------------------------------------
    --
    -- Reset controllers and freq measurement
    --
    ---------------------------------------------------------------------------
    reset_controller : entity work.reset
        generic map (
            NSTREAMS => 4*N_QUAD
        )
        port map (
            clk_free     => clk_freerun_buf,
            clk_i2c      => clk_i2c,
            clk_link     => clk_rec,
            clk_axi      => clk_axi,
            clk_hbm      => HBM_clock_buffer,
            rst_global   => local_reset,
            enable_i2c   => open,
            rst_i2c      => open,
            write_i2c    => open,
            rst_pll      => hb0_gtwiz_reset_tx_pll_and_datapath_int(0),
            rst_tx       => hb0_gtwiz_reset_tx_datapath_int(0),
            rst_rx       => hb_gtwiz_reset_rx_datapath_vio_int(0),
            rst_link     => rst_link,
            rst_packager => rst_packager
        );

    ---- measure recovered clks frequencies
    freq_measure_clk_rec_loop : for i in clk_rec'range generate
        freq_measure_clk_rec : entity work.freq_measure_base
            generic map (
                freq_used => x"07735940"    -- 125 MHz
            )
            port map (
                resetn      => '1',
                sysclk      => clk_rec(i),
                base_clk    => clk_ref,
                frequency   => freq_clk_rec(i)
            );
    end generate freq_measure_clk_rec_loop;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Input data receiver block (GTY wrapper)
    --
    ---------------------------------------------------------------------------
    inputs : entity work.inputs_10gbps
        generic map (
            N_QUAD   => N_QUAD,
            QUAD_MAP => QUAD_MAP
        )
        port map (
            -- Serial data ports for FMCP transceiver
            mgtgtyrxn_in                                => mgtgtyrxn_in,
            mgtgtyrxp_in                                => mgtgtyrxp_in,
            mgtgtytxn_out                               => mgtgtytxn_out,
            mgtgtytxp_out                               => mgtgtytxp_out,

            -- free-running clock
            clk_freerun                                 => clk_freerun,                     -- 100 MHz clock

            mgtrefclk0_x0y0_int                         => mgtrefclk0_x0y0_int,
            mgtrefclk0_x0y1_int                         => mgtrefclk0_x0y1_int,
            mgtrefclk0_x0y2_int                         => mgtrefclk0_x0y2_int,
            mgtrefclk0_x0y3_int                         => mgtrefclk0_x0y3_int,
            mgtrefclk0_x0y4_int                         => mgtrefclk0_x0y4_int,
            mgtrefclk0_x0y5_int                         => mgtrefclk0_x0y5_int,
            mgtrefclk0_x0y7_int                         => mgtrefclk0_x0y7_int,
            mgtrefclk0_x0y8_int                         => mgtrefclk0_x0y8_int,
            mgtrefclk0_x0y10_int                        => mgtrefclk0_x0y10_int,
            mgtrefclk0_x0y11_int                        => mgtrefclk0_x0y11_int,

            init_done_int                               => init_done_int,
            init_retry_ctr_int                          => init_retry_ctr,
            gtpowergood_vio_sync                        => gtpowergood_free_sync,
            txpmaresetdone_vio_sync                     => txpmaresetdone_free_sync,
            rxpmaresetdone_vio_sync                     => rxpmaresetdone_free_sync,
            gtwiz_reset_tx_done_vio_sync                => gtwiz_reset_tx_done_free_sync,
            gtwiz_reset_rx_done_vio_sync                => gtwiz_reset_rx_done_free_sync,
            hb_gtwiz_reset_all_vio_int                  => "0",
            hb0_gtwiz_reset_tx_pll_and_datapath_int     => hb0_gtwiz_reset_tx_pll_and_datapath_int,
            hb0_gtwiz_reset_tx_datapath_int             => hb0_gtwiz_reset_tx_datapath_int,
            hb_gtwiz_reset_rx_pll_and_datapath_vio_int  => "0",
            hb_gtwiz_reset_rx_datapath_vio_int          => hb_gtwiz_reset_rx_datapath_vio_int,
            link_down_latched_reset_vio_int             => "0",
            loopback                                    => loopback,

            -- debug: check byte alignment and clock data recovery (CDR) stable
            oRxbyteisaligned                            => sRxbyteisaligned,
            cdr_stable                                  => cdr_stable,

            -- debug: decoded rx words type
            ifcomma                                     => s_ifcomma,
            ifpadding                                   => s_ifpadding,
            ifinvalid                                   => s_ifinvalid,
            ifdata                                      => s_ifdata,
            ifelse                                      => s_ifelse,

            clk                                         => clk_rec,
            q                                           => d_gap_cleaner,
            oCommaDet                                   => sCommaDet
        );
    ---------------------------------------------------------------------------




    ---------------------------------------------------------------------------
    --
    -- Comma-Gap-Cleaner + Aligner + Zero-Suppression + Fifo Chain
    --
    ---------------------------------------------------------------------------
    ---- Comma-Gap-Cleaner
    gap_cleaner_loop : for i in 4*N_QUAD-1 downto 0 generate
    begin
        gap_cleaner : entity work.comma_gap_cleaner
            generic map (
                NSTREAMS => 1
            )
            port map (
                clk => clk_rec(i),
                rst => rst_link(i),
                d   => d_gap_cleaner(i downto i),
                q   => d_inputs_aligner(i downto i),
                lid => lid(i downto i),
                crc => crc(i downto i)
            );
    end generate gap_cleaner_loop;



    ---- crc generator and checker
    check_crc_loop : for i in crc'range generate
    begin
        check_crc : entity work.check_crc
            generic map (
                NSTREAMS => 1
            )
            port map(
                clk           => clk_rec(i),
                rst           => rst_link(i),
                d             => d_inputs_aligner(i downto i),
                crc           => crc(i downto i),
                crc_err_count => crc_error_counters(i downto i)
            );
    end generate check_crc_loop;



    ---- inputs aligner
    inputs_align_loop : for i in d_inputs_aligner'range generate
    begin
        inputs_align : entity work.inputs_aligner
            generic map (
                NSTREAMS => 1
            )
            port map (
                clk_wr                => clk_rec(i),
                clk_rd                => HBM_clock_buffer,
                rst                   => rst_aligner,
                enable                => "1",
                d                     => d_inputs_aligner(i downto i),
                q                     => q_inputs_aligner(i downto i)
            );
    end generate inputs_align_loop;



    ---- pattern generator
    data_generator : entity work.pattern_gen
        generic map (
            NSTREAMS => 4*N_QUAD
        )
        port map (
            clk                   => HBM_clock_buffer,
            rst                   => rst_packager,
            q                     => q_gen,
            gen_orbit_full_length => to_integer(unsigned(gen_orbit_full_length_hbm_sync)),
            gen_orbit_data_length => to_integer(unsigned(gen_orbit_data_length_hbm_sync))
        );



    ---- choose between data from mgt's and generated data
    d_align <= q_inputs_aligner when enable_data_gen_hbm_sync = '0' else q_gen;



    ---- Autorealign controller (if misalignment is found, trigger a reset)
    auto_realign_controller_1 : entity work.auto_realign_controller
        port map (
            axi_clk           => HBM_clock_buffer,
            rst_in            => rst_packager,

            clk_aligner       => HBM_clock_buffer,
            rst_aligner_out   => rst_aligner,
            misalignment_seen => packager_orbit_exceeds_size(0),

            autoreset_count   => autorealign_counter
        );



    ---- Aligner (and link masking)
    align : entity work.orbit_and_bx_aware_aligner
        generic map (
            NSTREAMS => 4*N_QUAD
        )
        port map (
            clk_wr            => HBM_clock_buffer,
            clk_rd            => HBM_clock_buffer,
            rst               => rst_aligner,
            enable            => stream_enable_mask_hbm_sync,
            waiting_orbit_end => waiting_orbit_end,
            d                 => d_align,
            q                 => q_align,
            q_ctrl            => q_ctrl_align
        );



    ---- mapping
    process(HBM_clock_buffer, rst_packager)
    begin
        if rst_packager = '1' then
            q_align_d1               <= (others => AWORD_NULL);
            q_align_d2               <= (others => AWORD_NULL);
            q_align_d3               <= (others => AWORD_NULL);
            d_zs                     <= (others => AWORD_NULL);
            q_ctrl_align_d1          <= ACONTROL_NULL;
            q_ctrl_align_d2          <= ACONTROL_NULL;
            q_ctrl_align_d3          <= ACONTROL_NULL;
            d_ctrl_zs                <= ACONTROL_NULL;
        else
            if rising_edge(HBM_clock_buffer) then
                q_align_d1      <= q_align;
                q_ctrl_align_d1 <= q_ctrl_align;

                q_align_d2      <= q_align_d1;
                q_ctrl_align_d2 <= q_ctrl_align_d1;

                q_align_d3      <= q_align_d2;
                q_ctrl_align_d3 <= q_ctrl_align_d2;

                for chan in d_zs'range loop
                    case link_map_hbm_sync(chan) is
                        when "00000" => d_zs(chan) <= q_align_d3(00);
                        when "00001" => d_zs(chan) <= q_align_d3(01);
                        when "00010" => d_zs(chan) <= q_align_d3(02);
                        when "00011" => d_zs(chan) <= q_align_d3(03);
                        when others  => d_zs(chan) <= AWORD_NULL;
                    end case;
                end loop;
                d_ctrl_zs <= q_ctrl_align_d3;
            end if;
        end if;
    end process;



    ---- Zero-Suppression
    zs_ugmt : entity work.zs_ugmt
        generic map(
            NSTREAMS => 4
        )
        port map(
            clk    => HBM_clock_buffer,
            rst    => rst_packager,
            d      => d_zs(3 downto 0),
            d_ctrl => d_ctrl_zs,
            q      => q_zs(0)(3 downto 0),
            q_ctrl => q_ctrl_zs(0)
        );

    d_bx(0)      <= q_zs(0)      when disable_zs_hbm_sync = '0' else(0 => d_zs(0), 1 => d_zs(1), 2 => d_zs(2), 3 => d_zs(3), others => AWORD_NULL);
    d_ctrl_bx(0) <= q_ctrl_zs(0) when disable_zs_hbm_sync = '0' else d_ctrl_zs;



    stream_loop : for i in N_HBM_PORTS-1 downto 0 generate
    begin

        ---- buffer to improve timing
        delay_line_0 : entity work.tstream_delay_line
            generic map(
                delay_length => 6
            )
            port map(
                clk    => HBM_clock_buffer,
                d      => d_bx(i),
                d_ctrl => d_ctrl_bx(i),
                q      => d_trailer(i),
                q_ctrl => d_ctrl_trailer(i)
            );



        ---- generate trailer with kept BXs info
        gen_trailer : entity work.filled_bx_trailer_generator
            generic map(
                NSTREAMS => 8
            )
            port map(
                clk    => HBM_clock_buffer,
                rst    => rst_packager,
                d      => d_trailer(i),
                d_ctrl => d_ctrl_trailer(i),
                q      => q_trailer(i),
                q_ctrl => q_ctrl_trailer(i)
            );



        ---- buffer to improve timing
        delay_line_1 : entity work.tstream_delay_line
            generic map(
                delay_length => 6
            )
            port map(
                clk    => HBM_clock_buffer,
                d      => q_trailer(i),
                d_ctrl => q_ctrl_trailer(i),
                q      => d_reshape(i),
                q_ctrl => d_ctrl_reshape(i)
            );



        ---- reshape streams
        stream_reshape : entity work.ugmt_stream_reshape
            generic map(
                NSTREAMS => 8
            )
            port map(
                clk             => HBM_clock_buffer,
                rst             => rst_packager,
                d               => d_reshape(i),
                d_ctrl          => d_ctrl_reshape(i),
                q               => q_reshape(i),
                q_ctrl          => q_ctrl_reshape(i),
                disable_reshape => disable_reshape_hbm_sync
            );



        ---- buffer to improve timing
        delay_line_2 : entity work.tstream_delay_line
            generic map(
                delay_length => 6
            )
            port map(
                clk    => HBM_clock_buffer,
                d      => q_reshape(i),
                d_ctrl => q_ctrl_reshape(i),
                q      => d_package(i),
                q_ctrl => d_ctrl_package(i)
            );



        ---- packager and header producer
        packager : entity work.packager
            generic map (
                NSTREAMS => 8
            )
            port map (
                d_clk                  => HBM_clock_buffer,
                rst                    => rst_packager,
                orbits_per_packet      => unsigned(orbits_per_packet_hbm_sync),
                orbits_per_chunk       => orbits_per_chunk_hbm_sync,
                wait_for_oc1           => wait_for_oc1_hbm_sync,
                d                      => d_package(i),
                d_ctrl                 => d_ctrl_package(i),
                m_aclk                 => HBM_clock_buffer,
                m_axis_tvalid          => packager_out_tvalid(i),
                m_axis_tready          => almost_full_buffer(i),
                m_axis_tdata           => packager_out_tdata(i),
                m_axis_tuser           => packager_out_tuser(i),
                m_axis_tkeep           => packager_out_tkeep(i),
                m_axis_tlast           => packager_out_tlast(i),
                dropped_orbit_counter  => packager_dropped_orbits(i),
                orbit_counter          => packager_seen_orbits(i),
                axi_backpressure_seen  => packager_axi_backpressure_seen(i),
                orbit_length           => packager_orbit_length_bxs(i),
                orbit_exceeds_size     => packager_orbit_exceeds_size(i),
                in_autorealign_counter => autorealign_counter,
                source_id              => scouting_source_id_hbm_sync(i),

                HBM_almost_full        => HBM_almost_full(i),

                header_dout_d6_o       => packager_Orbit_Header(i),
                header_wen_d6_o        => packager_Write_Orbit_Header(i),
                frag_header_dout_d5_o  => packager_Fragment_Header(i),
                frag_header_wen_d5_o   => packager_Write_Fragment_Header(i)
            );


        ---- HBM block maker
        convert_to_blocks : entity work.hbm_blocks_builder
            generic map(
                swapp => true
            )
            port map(
                reset_p                 => usr_rstp_clk_buffer,
                CLOCK                   => HBM_clock_buffer,

                write_dt_from_scouting  => packager_out_tvalid(i),                                      -- used to read the intermediate FIFO
                data_i_from_scouting    => packager_out_tdata(i),
                user_i_from_scouting    => packager_out_tuser(i),
                uctrl_i_from_scouting   => packager_out_tuser(i)(0) and not(packager_out_tuser(i)(1)),  -- active high
                Write_Fragment_Header   => packager_Write_Fragment_Header(i),
                Fragment_Header         => packager_Fragment_Header(i),
                Write_Orbit_Header      => packager_Write_Orbit_Header(i),
                Orbit_Header            => packager_Orbit_Header(i),

                -- output data to memory stream
                Data_o_to_HBM           => scouting_to_HBM_data(i).data_for_HBM_mem,
                Size_ready_to_HBM       => scouting_to_HBM_data(i).size_for_HBM_mem,                    -- SIZE READY TO BE TRANSFERED (IF 0x000 AND NOT Data_ready_to_HBM (4kBYTES)
                Data_Spec_to_HBM        => scouting_to_HBM_data(i).HD_space_in_HBM,                     -- 001   data words  010  Fragment Header  100 Orbit Header block
                Data_ready_to_HBM       => scouting_to_HBM_data(i).ready_for_HBM_mem,                   -- DATA IN fifo
                Read_data_for_HBM       => scouting_to_HBM_ctrl(i).read_for_HBM_mem,
                Block_read_done_for_HBM => scouting_to_HBM_ctrl(i).Block_read_done_for_HBM,

                internal_LFF            => almost_full_buffer(i)
            );
    end generate stream_loop;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- output (debug) signals
    --
    ---------------------------------------------------------------------------
    ---- inputs module decoder signals
    o_ifcomma_ila_sync   <= s_ifcomma_ila_sync;
    o_ifpadding_ila_sync <= s_ifpadding_ila_sync;
    o_ifinvalid_ila_sync <= s_ifinvalid_ila_sync;
    o_ifdata_ila_sync    <= s_ifdata_ila_sync;
    o_ifelse_ila_sync    <= s_ifelse_ila_sync;

    ---- non-aligned data words signals
    o_d_gap_cleaner_ila_sync     <= d_gap_cleaner_ila_sync;
    o_d_inputs_aligner_ila_sync  <= d_inputs_aligner_ila_sync;
    o_lid_ila_sync               <= lid_ila_sync;
    o_crc_ila_sync               <= crc_ila_sync;
    o_d_align_ila_sync           <= d_align_ila_sync;
    o_waiting_orbit_end_ila_sync <= waiting_orbit_end;

    ---- aligned data words signals
    o_q_align_ila_sync   <= q_align;
    o_d_zs_ila_sync      <= d_zs;
    o_d_bx_ila_sync      <= d_bx;
    o_d_trailer_ila_sync <= d_trailer;
    o_d_reshape_ila_sync <= d_reshape;
    o_d_package_ila_sync <= d_package;

    ---- aligned data control signals
    o_q_ctrl_align_ila_sync   <= q_ctrl_align;
    o_d_ctrl_zs_ila_sync      <= d_ctrl_zs;
    o_d_ctrl_bx_ila_sync      <= d_ctrl_bx;
    o_d_ctrl_trailer_ila_sync <= d_ctrl_trailer;
    o_d_ctrl_reshape_ila_sync <= d_ctrl_reshape;
    o_d_ctrl_package_ila_sync <= d_ctrl_package;

    ---- packager signals
    o_packager_dropped_orbits_ila_sync        <= packager_dropped_orbits;
    o_packager_seen_orbits_ila_sync           <= packager_seen_orbits;
    o_packager_orbit_length_bxs_ila_sync      <= packager_orbit_length_bxs;
    o_packager_axi_backpressure_seen_ila_sync <= packager_axi_backpressure_seen;
    o_packager_orbit_exceeds_size_ila_sync    <= packager_orbit_exceeds_size;
    o_packager_Fragment_Header_ila_sync       <= packager_Fragment_Header;
    o_packager_Write_Fragment_Header_ila_sync <= packager_Write_Fragment_Header;
    o_packager_Orbit_Header_ila_sync          <= packager_Orbit_Header;
    o_packager_Write_Orbit_Header_ila_sync    <= packager_Write_Orbit_Header;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- PCIe interface
    --
    ---------------------------------------------------------------------------
    ---- scouting ctrl registers
    process(axi_rstn, clk_axi)
    begin
        if axi_rstn = '0' then
            --
        elsif rising_edge(clk_axi) then

            if usr_wren = '1' then

                if usr_func_wr(ugmt_stream_enable_mask_func   ) = '1' then stream_enable_mask     <= usr_data_wr(4*N_QUAD - 1 downto 0); end if;
                if usr_func_wr(ugmt_local_reset_func          ) = '1' then local_reset            <= usr_data_wr(0);                     end if;
                if usr_func_wr(ugmt_link_map_00_func          ) = '1' then link_map( 0)           <= usr_data_wr( 4 downto 0);           end if;
                if usr_func_wr(ugmt_link_map_01_func          ) = '1' then link_map( 1)           <= usr_data_wr( 4 downto 0);           end if;
                if usr_func_wr(ugmt_link_map_02_func          ) = '1' then link_map( 2)           <= usr_data_wr( 4 downto 0);           end if;
                if usr_func_wr(ugmt_link_map_03_func          ) = '1' then link_map( 3)           <= usr_data_wr( 4 downto 0);           end if;
                if usr_func_wr(ugmt_disable_zs_func           ) = '1' then disable_zs             <= usr_data_wr(0);                     end if;
                if usr_func_wr(ugmt_orbits_per_packet_func    ) = '1' then orbits_per_packet      <= usr_data_wr(15 downto 0);           end if;
                if usr_func_wr(ugmt_orbits_per_chunk_func     ) = '1' then orbits_per_chunk       <= usr_data_wr(19 downto 0);           end if;
                if usr_func_wr(ugmt_wait_for_oc1_func         ) = '1' then wait_for_oc1           <= usr_data_wr(0);                     end if;
                if usr_func_wr(ugmt_enable_data_gen_func      ) = '1' then enable_data_gen        <= usr_data_wr(0);                     end if;
                if usr_func_wr(ugmt_gen_orbit_full_length_func) = '1' then gen_orbit_full_length  <= usr_data_wr(15 downto 0);           end if;
                if usr_func_wr(ugmt_gen_orbit_data_length_func) = '1' then gen_orbit_data_length  <= usr_data_wr(15 downto 0);           end if;
                if usr_func_wr(ugmt_disable_reshape_func      ) = '1' then disable_reshape        <= usr_data_wr(0);                     end if;
                if usr_func_wr(ugmt_scouting_source_id_00_func) = '1' then scouting_source_id(00) <= usr_data_wr(31 downto 0);           end if;
            end if;
        end if;
    end process;



    ---- scouting monitor registers
    process(axi_rstn, clk_axi)
    begin
        if axi_rstn = '0' then

            usr_data_rd <= (others => '0');

        elsif rising_edge(clk_axi) then

            if usr_rden = '1' then

                usr_data_rd <= (others => '0');

                ---- first half
                if    usr_func_rd(ugmt_stream_enable_mask_func   ) = '1' then usr_data_rd(4*N_QUAD - 1 downto 0) <= stream_enable_mask;
                elsif usr_func_rd(ugmt_local_reset_func          ) = '1' then usr_data_rd(0)                     <= local_reset;
                elsif usr_func_rd(ugmt_link_map_00_func          ) = '1' then usr_data_rd( 4 downto 0)           <= link_map( 0);
                elsif usr_func_rd(ugmt_link_map_01_func          ) = '1' then usr_data_rd( 4 downto 0)           <= link_map( 1);
                elsif usr_func_rd(ugmt_link_map_02_func          ) = '1' then usr_data_rd( 4 downto 0)           <= link_map( 2);
                elsif usr_func_rd(ugmt_link_map_03_func          ) = '1' then usr_data_rd( 4 downto 0)           <= link_map( 3);
                elsif usr_func_rd(ugmt_disable_zs_func           ) = '1' then usr_data_rd(0)                     <= disable_zs;
                elsif usr_func_rd(ugmt_orbits_per_packet_func    ) = '1' then usr_data_rd(15 downto 0)           <= orbits_per_packet;
                elsif usr_func_rd(ugmt_orbits_per_chunk_func     ) = '1' then usr_data_rd(19 downto 0)           <= orbits_per_chunk;
                elsif usr_func_rd(ugmt_wait_for_oc1_func         ) = '1' then usr_data_rd(0)                     <= wait_for_oc1;
                elsif usr_func_rd(ugmt_enable_data_gen_func      ) = '1' then usr_data_rd(0)                     <= enable_data_gen;
                elsif usr_func_rd(ugmt_gen_orbit_full_length_func) = '1' then usr_data_rd(15 downto 0)           <= gen_orbit_full_length;
                elsif usr_func_rd(ugmt_gen_orbit_data_length_func) = '1' then usr_data_rd(15 downto 0)           <= gen_orbit_data_length;
                elsif usr_func_rd(ugmt_disable_reshape_func      ) = '1' then usr_data_rd(0)                     <= disable_reshape;
                elsif usr_func_rd(ugmt_scouting_source_id_00_func) = '1' then usr_data_rd(31 downto 0)           <= scouting_source_id(00);

                ---- second half
                -- input
                elsif usr_func_rd(ugmt_rx_byte_is_aligned_info_func      ) = '1' then usr_data_rd(4*N_QUAD - 1 downto 0) <= sRxbyteisaligned_axi_sync;
                elsif usr_func_rd(ugmt_gt_power_good_info_func           ) = '1' then usr_data_rd(4*N_QUAD - 1 downto 0) <= gtpowergood_axi_sync;
                elsif usr_func_rd(ugmt_cdr_stable_info_func              ) = '1' then usr_data_rd(  N_QUAD - 1 downto 0) <= cdr_stable_axi_sync;
                elsif usr_func_rd(ugmt_gt_tx_reset_done_info_func        ) = '1' then usr_data_rd(  N_QUAD - 1 downto 0) <= gtwiz_reset_tx_done_axi_sync;
                elsif usr_func_rd(ugmt_gt_rx_reset_done_info_func        ) = '1' then usr_data_rd(  N_QUAD - 1 downto 0) <= gtwiz_reset_rx_done_axi_sync;
                elsif usr_func_rd(ugmt_gt_reset_tx_pll_datapath_info_func) = '1' then usr_data_rd(0)                     <= hb0_gtwiz_reset_tx_pll_and_datapath_axi_sync;
                elsif usr_func_rd(ugmt_gt_reset_tx_datapath_info_func    ) = '1' then usr_data_rd(0)                     <= hb0_gtwiz_reset_tx_datapath_axi_sync;
                elsif usr_func_rd(ugmt_gt_reset_rx_datapath_info_func    ) = '1' then usr_data_rd(0)                     <= hb_gtwiz_reset_rx_datapath_axi_sync;
                elsif usr_func_rd(ugmt_gt_init_done_info_func            ) = '1' then usr_data_rd(  N_QUAD - 1 downto 0) <= init_done_axi_sync;
                -- aligner
                elsif usr_func_rd(ugmt_waiting_for_orbit_end_info_func   ) = '1' then usr_data_rd(0)           <= waiting_orbit_end_axi_sync;

                -- packager
                elsif usr_func_rd(ugmt_packager_seen_orbits_00_func      ) = '1' then usr_data_rd(31 downto 0) <= packager_seen_orbits_axi_sync(00);

                elsif usr_func_rd(ugmt_packager_dropped_orbits_00_func   ) = '1' then usr_data_rd(31 downto 0) <= packager_dropped_orbits_axi_sync(00);

                elsif usr_func_rd(ugmt_orbit_length_bxs_00_func          ) = '1' then usr_data_rd(31 downto 0) <= packager_orbit_length_bxs_axi_sync(00);

                elsif usr_func_rd(ugmt_axi_backpressure_seen_info_func   ) = '1' then usr_data_rd(N_HBM_PORTS - 1 downto 0) <= packager_axi_backpressure_seen_axi_sync;
                elsif usr_func_rd(ugmt_orbit_exceeds_size_info_func      ) = '1' then usr_data_rd(N_HBM_PORTS - 1 downto 0) <= packager_orbit_exceeds_size_axi_sync;
                elsif usr_func_rd(ugmt_autorealigns_total_func           ) = '1' then usr_data_rd(31 downto 0)              <= autorealign_counter_axi_sync;

                -- fw/algo version
                elsif usr_func_rd(ugmt_algo_version_func                 ) = '1' then usr_data_rd(23 downto 0) <= ALGO_REV;
                elsif usr_func_rd(ugmt_fw_type_func                      ) = '1' then usr_data_rd( 7 downto 0) <= FW_TYPE;

                -- clk frequencies
                elsif usr_func_rd(ugmt_freq_clk_rec_00_func) = '1' then usr_data_rd(31 downto 0) <= freq_clk_rec_axi_sync(00);
                elsif usr_func_rd(ugmt_freq_clk_rec_01_func) = '1' then usr_data_rd(31 downto 0) <= freq_clk_rec_axi_sync(01);
                elsif usr_func_rd(ugmt_freq_clk_rec_02_func) = '1' then usr_data_rd(31 downto 0) <= freq_clk_rec_axi_sync(02);
                elsif usr_func_rd(ugmt_freq_clk_rec_03_func) = '1' then usr_data_rd(31 downto 0) <= freq_clk_rec_axi_sync(03);

                -- crc's counters
                elsif usr_func_rd(ugmt_crc_error_counter_00_func) = '1' then usr_data_rd(31 downto 0) <= crc_error_counters_axi_sync(00);
                elsif usr_func_rd(ugmt_crc_error_counter_01_func) = '1' then usr_data_rd(31 downto 0) <= crc_error_counters_axi_sync(01);
                elsif usr_func_rd(ugmt_crc_error_counter_02_func) = '1' then usr_data_rd(31 downto 0) <= crc_error_counters_axi_sync(02);
                elsif usr_func_rd(ugmt_crc_error_counter_03_func) = '1' then usr_data_rd(31 downto 0) <= crc_error_counters_axi_sync(03);
                end if;
            end if;
        end if;
    end process;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Synchronizers
    -- clk_freerun to clk_axi
    --
    ---------------------------------------------------------------------------
    -- gtpowergood_free_sync
    sync_gtpowergood : entity work.cdc_array
        generic map(
            bit_width   => gtpowergood_free_sync'length
        )
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => gtpowergood_free_sync,
            q           => gtpowergood_axi_sync
        );

    -- gtwiz_reset_tx_done_free_sync
    sync_reset_tx_done : entity work.cdc_array
        generic map(
            bit_width   => gtwiz_reset_tx_done_free_sync'length
        )
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => gtwiz_reset_tx_done_free_sync,
            q           => gtwiz_reset_tx_done_axi_sync
        );

    -- gtwiz_reset_rx_done_free_sync
    sync_reset_rx_done : entity work.cdc_array
        generic map(
            bit_width   => gtwiz_reset_rx_done_free_sync'length
        )
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => gtwiz_reset_rx_done_free_sync,
            q           => gtwiz_reset_rx_done_axi_sync
        );

    -- init_done_int
    sync_init_done : entity work.cdc_array
        generic map(
            bit_width   => init_done_int'length
        )
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => init_done_int,
            q           => init_done_axi_sync
        );

    -- hb0_gtwiz_reset_tx_pll_and_datapath_int
    sync_reset_tx_pll : entity work.cdc_single
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => hb0_gtwiz_reset_tx_pll_and_datapath_int(0),
            q           => hb0_gtwiz_reset_tx_pll_and_datapath_axi_sync
        );

    -- hb0_gtwiz_reset_tx_datapath_int
    sync_reset_tx_datapath : entity work.cdc_single
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => hb0_gtwiz_reset_tx_datapath_int(0),
            q           => hb0_gtwiz_reset_tx_datapath_axi_sync
        );

    -- hb_gtwiz_reset_rx_datapath_vio_int
    sync_reset_rx_datapath : entity work.cdc_single
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => hb_gtwiz_reset_rx_datapath_vio_int(0),
            q           => hb_gtwiz_reset_rx_datapath_axi_sync
        );

    -- txpmaresetdone_free_sync
    sync_txpmaresetdone : entity work.cdc_array
        generic map(
            bit_width   => txpmaresetdone_free_sync'length
        )
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => txpmaresetdone_free_sync,
            q           => txpmaresetdone_axi_sync
        );

    -- rxpmaresetdone_free_sync
    sync_rxpmaresetdone : entity work.cdc_array
        generic map(
            bit_width   => rxpmaresetdone_free_sync'length
        )
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => rxpmaresetdone_free_sync,
            q           => rxpmaresetdone_axi_sync
        );

    -- init_retry_ctr
    sync_init_retr_ctr : entity work.cdc_array
        generic map(
            bit_width   => init_retry_ctr'length
        )
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => init_retry_ctr,
            q           => init_retry_ctr_axi_sync
        );

    -- cdr_stable
    sync_cdr_stable : entity work.cdc_array
        generic map(
            bit_width   => cdr_stable'length
        )
        port map(
            clk_src     => clk_freerun_buf,
            clk_dest    => clk_axi,
            d           => cdr_stable,
            q           => cdr_stable_axi_sync
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Synchronizers
    -- clk_axi to clk_hbm
    --
    ---------------------------------------------------------------------------
    -- stream_enable_mask
    sync_stream_enable_mask : entity work.cdc_array
        generic map(
            bit_width   => stream_enable_mask'length
        )
        port map(
            clk_src     => clk_axi,
            clk_dest    => HBM_clock_buffer,
            d           => stream_enable_mask,
            q           => stream_enable_mask_hbm_sync
        );

    -- orbits_per_packet
    sync_orbits_per_packet : entity work.cdc_array
        generic map(
            bit_width   => orbits_per_packet'length
        )
        port map(
            clk_src     => clk_axi,
            clk_dest    => HBM_clock_buffer,
            d           => orbits_per_packet,
            q           => orbits_per_packet_hbm_sync
        );

    -- orbits_per_chunk
    sync_orbits_per_chunk : entity work.cdc_array
        generic map(
            bit_width   => orbits_per_chunk'length
        )
        port map(
            clk_src     => clk_axi,
            clk_dest    => HBM_clock_buffer,
            d           => orbits_per_chunk,
            q           => orbits_per_chunk_hbm_sync
        );

    -- wait_for_oc1
    sync_wait_for_oc1 : entity work.cdc_single
        port map(
            clk_src     => clk_axi,
            clk_dest    => HBM_clock_buffer,
            d           => wait_for_oc1,
            q           => wait_for_oc1_hbm_sync
        );

    -- disable_zs
    sync_disable_zs : entity work.cdc_single
        port map(
            clk_src     => clk_axi,
            clk_dest    => HBM_clock_buffer,
            d           => disable_zs,
            q           => disable_zs_hbm_sync
        );

    -- link_map
    sync_link_map_xx_loop : for chan in link_map'range generate
        sync_link_map_xx: entity work.cdc_array
            generic map(
                bit_width => link_map(chan)'length
            )
            port map(
                clk_src  => clk_axi,
                clk_dest => HBM_clock_buffer,
                d        => link_map(chan),
                q        => link_map_hbm_sync(chan)
            );
    end generate sync_link_map_xx_loop;

    -- enable_data_gen
    sync_enable_data_gen : entity work.cdc_single
        port map(
            clk_src     => clk_axi,
            clk_dest    => HBM_clock_buffer,
            d           => enable_data_gen,
            q           => enable_data_gen_hbm_sync
        );

    -- gen_orbit_full_length
    sync_gen_orbit_full_length : entity work.cdc_array
        generic map(
            bit_width   => gen_orbit_full_length'length
        )
        port map(
            clk_src     => clk_axi,
            clk_dest    => HBM_clock_buffer,
            d           => gen_orbit_full_length,
            q           => gen_orbit_full_length_hbm_sync
        );

    -- gen_orbit_data_length
    sync_gen_orbit_data_length : entity work.cdc_array
        generic map(
            bit_width   => gen_orbit_full_length'length
        )
        port map(
            clk_src     => clk_axi,
            clk_dest    => HBM_clock_buffer,
            d           => gen_orbit_data_length,
            q           => gen_orbit_data_length_hbm_sync
        );

    -- disable_reshape
    sync_disable_reshape : entity work.cdc_single
        port map(
            clk_src     => clk_axi,
            clk_dest    => HBM_clock_buffer,
            d           => disable_reshape,
            q           => disable_reshape_hbm_sync
        );

    -- scouting_source_id
    sync_scouting_source_id_xx_loop : for chan in scouting_source_id'range generate
        scouting_source_id_xx: entity work.cdc_array
            generic map(
                bit_width => scouting_source_id(chan)'length
            )
            port map(
                clk_src  => clk_axi,
                clk_dest => HBM_clock_buffer,
                d        => scouting_source_id(chan),
                q        => scouting_source_id_hbm_sync(chan)
            );
    end generate sync_scouting_source_id_xx_loop;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Synchronizers
    -- clk_hbm to clk_axi
    --
    ---------------------------------------------------------------------------
    -- waiting_orbit_end
    sync_waiting_orbit_end : entity work.cdc_single
        port map(
            clk_src     => HBM_clock_buffer,
            clk_dest    => clk_axi,
            d           => waiting_orbit_end,
            q           => waiting_orbit_end_axi_sync
        );

    sync_packager_out_loop : for i in N_HBM_PORTS-1 downto 0 generate
        -- packager_seen_orbits
        sync_packager_seen_orbits : entity work.cdc_array
            generic map(
                bit_width   => packager_seen_orbits_axi_sync(i)'length
            )
            port map(
                clk_src     => HBM_clock_buffer,
                clk_dest    => clk_axi,
                d           => std_logic_vector(packager_seen_orbits(i)(31 downto 0)),
                q           => packager_seen_orbits_axi_sync(i)
            );

        -- packager_dropped_orbits_axi_sync
        sync_packager_dropped_orbits : entity work.cdc_array
            generic map(
                bit_width   => packager_dropped_orbits_axi_sync(i)'length
            )
            port map(
                clk_src     => HBM_clock_buffer,
                clk_dest    => clk_axi,
                d           => std_logic_vector(packager_dropped_orbits(i)(31 downto 0)),
                q           => packager_dropped_orbits_axi_sync(i)
            );

        -- packager_axi_backpressure_seen
        sync_packager_axi_backpressure_seen : entity work.cdc_single
            port map(
                clk_src     => HBM_clock_buffer,
                clk_dest    => clk_axi,
                d           => packager_axi_backpressure_seen(i),
                q           => packager_axi_backpressure_seen_axi_sync(i)
            );

        -- packager_orbit_exceeds_size
        sync_packager_orbit_exceeds_size : entity work.cdc_single
            port map(
                clk_src     => HBM_clock_buffer,
                clk_dest    => clk_axi,
                d           => packager_orbit_exceeds_size(i),
                q           => packager_orbit_exceeds_size_axi_sync(i)
            );

        -- packager_orbit_length_bxs
        sync_packager_orbit_length_bxs : entity work.cdc_array
            generic map(
                bit_width   => packager_orbit_length_bxs_axi_sync(i)'length
            )
            port map(
                clk_src     => HBM_clock_buffer,
                clk_dest    => clk_axi,
                d           => std_logic_vector(packager_orbit_length_bxs(i)(31 downto 0)),
                q           => packager_orbit_length_bxs_axi_sync(i)
            );
    end generate sync_packager_out_loop;

    -- autorealign_counter
    sync_autorealign_counter : entity work.cdc_array
        generic map(
            bit_width   => autorealign_counter_axi_sync'length
        )
        port map(
            clk_src     => HBM_clock_buffer,
            clk_dest    => clk_axi,
            d           => std_logic_vector(autorealign_counter(31 downto 0)),
            q           => autorealign_counter_axi_sync
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Synchronizers
    -- clk_link to clk_hbm
    --
    ---------------------------------------------------------------------------
    -- d_gap_cleaner
    sync_d_gap_cleaner_loop : for chan in d_gap_cleaner'range generate
        sync_d_gap_cleaner_valid : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => d_gap_cleaner(chan).valid,
                q           => d_gap_cleaner_ila_sync(chan).valid
            );

        sync_d_gap_cleaner_strobe : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => d_gap_cleaner(chan).strobe,
                q           => d_gap_cleaner_ila_sync(chan).strobe
            );

        sync_d_gap_cleaner_done : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => d_gap_cleaner(chan).done,
                q           => d_gap_cleaner_ila_sync(chan).done
            );

        sync_d_gap_cleaner_data : entity work.cdc_array
            generic map(
                bit_width   => d_gap_cleaner(chan).data'length
            )
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => d_gap_cleaner(chan).data,
                q           => d_gap_cleaner_ila_sync(chan).data
            );
    end generate sync_d_gap_cleaner_loop;

    -- d_align
    sync_d_align_loop : for chan in d_align'range generate
        sync_d_align_valid : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => d_align(chan).valid,
                q           => d_align_ila_sync(chan).valid
            );

        sync_d_align_strobe : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => d_align(chan).strobe,
                q           => d_align_ila_sync(chan).strobe
            );

        sync_d_align_done : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => d_align(chan).done,
                q           => d_align_ila_sync(chan).done
            );

        sync_d_align_data : entity work.cdc_array
            generic map(
                bit_width   => d_align(chan).data'length
            )
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => d_align(chan).data,
                q           => d_align_ila_sync(chan).data
            );
    end generate sync_d_align_loop;

    -- lid and crc
    sync_link_trailer_loop : for chan in lid'range generate
        sync_lid : entity work.cdc_array
            generic map(
                bit_width => lid(chan)'length
            )
            port map(
                clk_src  => clk_rec(chan),
                clk_dest => HBM_clock_buffer,
                d        => lid(chan),
                q        => lid_ila_sync(chan)
            );

        sync_crc : entity work.cdc_array
            generic map(
                bit_width => crc(chan)'length
            )
            port map(
                clk_src  => clk_rec(chan),
                clk_dest => HBM_clock_buffer,
                d        => crc(chan),
                q        => crc_ila_sync(chan)
            );
    end generate sync_link_trailer_loop;

    -- input decoding signals
    sync_input_decoding_loop : for chan in lid'range generate
        -- s_ifcomma
        sync_s_ifcomma : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => s_ifcomma(chan),
                q           => s_ifcomma_ila_sync(chan)
            );

        -- s_ifpadding
        sync_s_ifpadding : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => s_ifpadding(chan),
                q           => s_ifpadding_ila_sync(chan)
            );

        -- s_ifinvalid
        sync_s_ifinvalid : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => s_ifinvalid(chan),
                q           => s_ifinvalid_ila_sync(chan)
            );

        -- s_ifdata
        sync_s_ifdata : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => s_ifdata(chan),
                q           => s_ifdata_ila_sync(chan)
            );

        -- s_ifelse
        sync_s_ifelse : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => HBM_clock_buffer,
                d           => s_ifelse(chan),
                q           => s_ifelse_ila_sync(chan)
            );
    end generate sync_input_decoding_loop;
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Synchronizers
    -- clk_link to clk_axi
    --
    ---------------------------------------------------------------------------
    -- sRxbyteisaligned
    sync_sRxbyteisaligned_loop : for chan in sRxbyteisaligned'range generate
        sync_sRxbyteisaligned : entity work.cdc_single
            port map(
                clk_src     => clk_rec(chan),
                clk_dest    => clk_axi,
                d           => sRxbyteisaligned(chan),
                q           => sRxbyteisaligned_axi_sync(chan)
            );
    end generate sync_sRxbyteisaligned_loop;

    -- recovered input links clock frequency
    sync_freq_clk_rec_loop : for chan in freq_clk_rec'range generate
        sync_freq_clk_rec : entity work.cdc_array
            generic map(
                bit_width => freq_clk_rec(chan)'length
            )
            port map(
                clk_src  => clk_rec(chan),
                clk_dest => clk_axi,
                d        => freq_clk_rec(chan),
                q        => freq_clk_rec_axi_sync(chan)
            );
    end generate;

    -- crc error counters
    sync_crc_error_counters_loop : for chan in crc_error_counters'range generate
        sync_crc_error_counters : entity work.cdc_array
            generic map(
                bit_width => crc_error_counters(chan)'length
            )
            port map(
                clk_src  => clk_rec(chan),
                clk_dest => clk_axi,
                d        => std_logic_vector(crc_error_counters(chan)),
                q        => crc_error_counters_axi_sync(chan)
            );
    end generate;
    ---------------------------------------------------------------------------

end Behavioral;