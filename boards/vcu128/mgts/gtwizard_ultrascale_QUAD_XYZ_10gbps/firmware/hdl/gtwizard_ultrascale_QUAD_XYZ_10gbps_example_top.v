//------------------------------------------------------------------------------
//  (c) Copyright 2013-2018 Xilinx, Inc. All rights reserved.
//
//  This file contains confidential and proprietary information
//  of Xilinx, Inc. and is protected under U.S. and
//  international copyright and other intellectual property
//  laws.
//
//  DISCLAIMER
//  This disclaimer is not a license and does not grant any
//  rights to the materials distributed herewith. Except as
//  otherwise provided in a valid license issued to you by
//  Xilinx, and to the maximum extent permitted by applicable
//  law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
//  WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
//  AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
//  BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
//  INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
//  (2) Xilinx shall not be liable (whether in contract or tort,
//  including negligence, or under any other theory of
//  liability) for any loss or damage of any kind or nature
//  related to, arising under or in connection with these
//  materials, including for any direct, or any indirect,
//  special, incidental, or consequential loss or damage
//  (including loss of data, profits, goodwill, or any type of
//  loss or damage suffered as a result of any action brought
//  by a third party) even if such damage or loss was
//  reasonably foreseeable or Xilinx had been advised of the
//  possibility of the same.
//
//  CRITICAL APPLICATIONS
//  Xilinx products are not designed or intended to be fail-
//  safe, or for use in any application requiring fail-safe
//  performance, such as life-support or safety devices or
//  systems, Class III medical devices, nuclear facilities,
//  applications related to the deployment of airbags, or any
//  other applications that could lead to death, personal
//  injury, or severe property or environmental damage
//  (individually and collectively, "Critical
//  Applications"). Customer assumes the sole risk and
//  liability of any use of Xilinx products in Critical
//  Applications, subject only to applicable laws and
//  regulations governing limitations on product liability.
//
//  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
//  PART OF THIS FILE AT ALL TIMES.
//------------------------------------------------------------------------------


`timescale 1ps/1ps

// =====================================================================================================================
// This example design top module instantiates the example design wrapper; slices vectored ports for per-channel
// assignment; and instantiates example resources such as buffers, pattern generators, and pattern checkers for core
// demonstration purposes
// =====================================================================================================================

module gtwizard_ultrascale_QUAD_XYZ_10gbps_example_top #(
  parameter QUAD_XYZ = 129
)(

  // Differential reference clock inputs
  input  wire mgtrefclk0_x0y0_int,
  input  wire mgtrefclk0_x0y1_int,
  input  wire mgtrefclk0_x0y2_int,
  input  wire mgtrefclk0_x0y3_int,
  input  wire mgtrefclk0_x0y4_int,
  input  wire mgtrefclk0_x0y5_int,
  input  wire mgtrefclk0_x0y7_int,
  input  wire mgtrefclk0_x0y8_int,
  input  wire mgtrefclk0_x0y10_int,
  input  wire mgtrefclk0_x0y11_int,

  // Serial data ports for transceiver channel 0
  input  wire ch0_gtyrxn_in,
  input  wire ch0_gtyrxp_in,
  output wire ch0_gtytxn_out,
  output wire ch0_gtytxp_out,

  // Serial data ports for transceiver channel 1
  input  wire ch1_gtyrxn_in,
  input  wire ch1_gtyrxp_in,
  output wire ch1_gtytxn_out,
  output wire ch1_gtytxp_out,

  // Serial data ports for transceiver channel 2
  input  wire ch2_gtyrxn_in,
  input  wire ch2_gtyrxp_in,
  output wire ch2_gtytxn_out,
  output wire ch2_gtytxp_out,

  // Serial data ports for transceiver channel 3
  input  wire ch3_gtyrxn_in,
  input  wire ch3_gtyrxp_in,
  output wire ch3_gtytxn_out,
  output wire ch3_gtytxp_out,

  // User-provided ports for reset helper block(s)
  input  wire hb_gtwiz_reset_clk_freerun_buf_int,
  output wire hb_gtwiz_reset_clk_freerun_out,

  // Other signals
  output wire [0:0]   init_done_int,
  output wire [3:0]   init_retry_ctr_int,
  output wire [3:0]   gtpowergood_vio_sync,
  output wire [3:0]   txpmaresetdone_vio_sync,
  output wire [3:0]   rxpmaresetdone_vio_sync,
  output wire         gtwiz_reset_tx_done_vio_sync,
  output wire         gtwiz_reset_rx_done_vio_sync,
  input  wire [0:0]   hb_gtwiz_reset_all_vio_int,
  input  wire [0:0]   hb0_gtwiz_reset_tx_pll_and_datapath_int,
  input  wire [0:0]   hb0_gtwiz_reset_tx_datapath_int,
  input  wire [0:0]   hb_gtwiz_reset_rx_pll_and_datapath_vio_int,
  input  wire [0:0]   hb_gtwiz_reset_rx_datapath_vio_int,
  input  wire [0:0]   link_down_latched_reset_vio_int,
  input  wire [11:0]  loopback,  // This vector is not sliced because it is directly assigned in a debug core instance below

  input  wire [31:0]  charisk_in,
  output wire [3:0]   rxcommadet_out,
  input  wire [127:0] gtwiz_userdata_tx_in,
  output wire [127:0] gtwiz_userdata_rx_out,
  output wire [3:0]   gtwiz_userclk_rx_usrclk_out, // [0:0] -> [3:0]
  output wire [63:0]  rxctrl0_out,
  output wire [63:0]  rxctrl1_out,
  output wire [31:0]  rxctrl2_out,
  output wire [31:0]  rxctrl3_out,
  output wire [3:0]   rxbyteisaligned_out,
  output wire [0:0]   gtwiz_reset_rx_cdr_stable_out

);


  // ===================================================================================================================
  // PER-CHANNEL SIGNAL ASSIGNMENTS
  // ===================================================================================================================

  // The core and example design wrapper vectorize ports across all enabled transceiver channel and common instances for
  // simplicity and compactness. This example design top module assigns slices of each vector to individual, per-channel
  // signal vectors for use if desired. Signals which connect to helper blocks are prefixed "hb#", signals which connect
  // to transceiver common primitives are prefixed "cm#", and signals which connect to transceiver channel primitives
  // are prefixed "ch#", where "#" is the sequential resource number.

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtyrxn_int;
  assign gtyrxn_int[0:0] = ch0_gtyrxn_in;
  assign gtyrxn_int[1:1] = ch1_gtyrxn_in;
  assign gtyrxn_int[2:2] = ch2_gtyrxn_in;
  assign gtyrxn_int[3:3] = ch3_gtyrxn_in;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtyrxp_int;
  assign gtyrxp_int[0:0] = ch0_gtyrxp_in;
  assign gtyrxp_int[1:1] = ch1_gtyrxp_in;
  assign gtyrxp_int[2:2] = ch2_gtyrxp_in;
  assign gtyrxp_int[3:3] = ch3_gtyrxp_in;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtytxn_int;
  assign ch0_gtytxn_out = gtytxn_int[0:0];
  assign ch1_gtytxn_out = gtytxn_int[1:1];
  assign ch2_gtytxn_out = gtytxn_int[2:2];
  assign ch3_gtytxn_out = gtytxn_int[3:3];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtytxp_int;
  assign ch0_gtytxp_out = gtytxp_int[0:0];
  assign ch1_gtytxp_out = gtytxp_int[1:1];
  assign ch2_gtytxp_out = gtytxp_int[2:2];
  assign ch3_gtytxp_out = gtytxp_int[3:3];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_reset_int;
  wire [0:0] hb0_gtwiz_userclk_tx_reset_int;
  assign gtwiz_userclk_tx_reset_int[0:0] = hb0_gtwiz_userclk_tx_reset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtwiz_userclk_tx_srcclk_int;
  wire [3:0] hb0_gtwiz_userclk_tx_srcclk_int;
  assign hb0_gtwiz_userclk_tx_srcclk_int = gtwiz_userclk_tx_srcclk_int[3:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtwiz_userclk_tx_usrclk_int;
  wire [3:0] hb0_gtwiz_userclk_tx_usrclk_int;
  assign hb0_gtwiz_userclk_tx_usrclk_int = gtwiz_userclk_tx_usrclk_int[3:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtwiz_userclk_tx_usrclk2_int;
  wire [3:0] hb0_gtwiz_userclk_tx_usrclk2_int;
  assign hb0_gtwiz_userclk_tx_usrclk2_int = gtwiz_userclk_tx_usrclk2_int[3:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_active_int;
  wire [0:0] hb0_gtwiz_userclk_tx_active_int;
  assign hb0_gtwiz_userclk_tx_active_int = gtwiz_userclk_tx_active_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_reset_int;
  wire [0:0] hb0_gtwiz_userclk_rx_reset_int;
  assign gtwiz_userclk_rx_reset_int[0:0] = hb0_gtwiz_userclk_rx_reset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtwiz_userclk_rx_srcclk_int;
  wire [3:0] hb0_gtwiz_userclk_rx_srcclk_int;
  assign hb0_gtwiz_userclk_rx_srcclk_int = gtwiz_userclk_rx_srcclk_int[3:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtwiz_userclk_rx_usrclk_int;
  wire [3:0] hb0_gtwiz_userclk_rx_usrclk_int;
  assign hb0_gtwiz_userclk_rx_usrclk_int = gtwiz_userclk_rx_usrclk_int[3:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtwiz_userclk_rx_usrclk2_int;
  wire [3:0] hb0_gtwiz_userclk_rx_usrclk2_int;
  assign hb0_gtwiz_userclk_rx_usrclk2_int = gtwiz_userclk_rx_usrclk2_int[3:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_active_int;
  wire [0:0] hb0_gtwiz_userclk_rx_active_int;
  assign hb0_gtwiz_userclk_rx_active_int = gtwiz_userclk_rx_active_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_clk_freerun_int;
  wire [0:0] hb0_gtwiz_reset_clk_freerun_int = 1'b0;
  assign gtwiz_reset_clk_freerun_int[0:0] = hb0_gtwiz_reset_clk_freerun_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_all_int;
  wire [0:0] hb0_gtwiz_reset_all_int = 1'b0;
  assign gtwiz_reset_all_int[0:0] = hb0_gtwiz_reset_all_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_tx_pll_and_datapath_int;
  assign gtwiz_reset_tx_pll_and_datapath_int[0:0] = hb0_gtwiz_reset_tx_pll_and_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_tx_datapath_int;
  assign gtwiz_reset_tx_datapath_int[0:0] = hb0_gtwiz_reset_tx_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_pll_and_datapath_int;
  wire [0:0] hb0_gtwiz_reset_rx_pll_and_datapath_int = 1'b0;
  assign gtwiz_reset_rx_pll_and_datapath_int[0:0] = hb0_gtwiz_reset_rx_pll_and_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_datapath_int;
  wire [0:0] hb0_gtwiz_reset_rx_datapath_int = 1'b0;
  assign gtwiz_reset_rx_datapath_int[0:0] = hb0_gtwiz_reset_rx_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_cdr_stable_int;
  wire [0:0] hb0_gtwiz_reset_rx_cdr_stable_int;
  assign hb0_gtwiz_reset_rx_cdr_stable_int = gtwiz_reset_rx_cdr_stable_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_tx_done_int;
  wire [0:0] hb0_gtwiz_reset_tx_done_int;
  assign hb0_gtwiz_reset_tx_done_int = gtwiz_reset_tx_done_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_done_int;
  wire [0:0] hb0_gtwiz_reset_rx_done_int;
  assign hb0_gtwiz_reset_rx_done_int = gtwiz_reset_rx_done_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [127:0] gtwiz_userdata_tx_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [127:0] gtwiz_userdata_rx_int;
  wire [31:0] hb0_gtwiz_userdata_rx_int;
  wire [31:0] hb1_gtwiz_userdata_rx_int;
  wire [31:0] hb2_gtwiz_userdata_rx_int;
  wire [31:0] hb3_gtwiz_userdata_rx_int;
  assign hb0_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[31:0];
  assign hb1_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[63:32];
  assign hb2_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[95:64];
  assign hb3_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[127:96];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtrefclk00_int;
  wire [0:0] cm0_gtrefclk00_int;
  assign gtrefclk00_int[0:0] = cm0_gtrefclk00_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] qpll0outclk_int;
  wire [0:0] cm0_qpll0outclk_int;
  assign cm0_qpll0outclk_int = qpll0outclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] qpll0outrefclk_int;
  wire [0:0] cm0_qpll0outrefclk_int;
  assign cm0_qpll0outrefclk_int = qpll0outrefclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [39:0] drpaddr_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] drpclk_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] drpdi_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] drpen_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] drpwe_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] eyescanreset_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rx8b10ben_int;
  wire [0:0] ch0_rx8b10ben_int = 1'b1;
  wire [0:0] ch1_rx8b10ben_int = 1'b1;
  wire [0:0] ch2_rx8b10ben_int = 1'b1;
  wire [0:0] ch3_rx8b10ben_int = 1'b1;
  assign rx8b10ben_int[0:0] = ch0_rx8b10ben_int;
  assign rx8b10ben_int[1:1] = ch1_rx8b10ben_int;
  assign rx8b10ben_int[2:2] = ch2_rx8b10ben_int;
  assign rx8b10ben_int[3:3] = ch3_rx8b10ben_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rxcommadeten_int;
  wire [0:0] ch0_rxcommadeten_int = 1'b1;
  wire [0:0] ch1_rxcommadeten_int = 1'b1;
  wire [0:0] ch2_rxcommadeten_int = 1'b1;
  wire [0:0] ch3_rxcommadeten_int = 1'b1;
  assign rxcommadeten_int[0:0] = ch0_rxcommadeten_int;
  assign rxcommadeten_int[1:1] = ch1_rxcommadeten_int;
  assign rxcommadeten_int[2:2] = ch2_rxcommadeten_int;
  assign rxcommadeten_int[3:3] = ch3_rxcommadeten_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rxlpmen_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rxmcommaalignen_int;
  wire [0:0] ch0_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch1_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch2_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch3_rxmcommaalignen_int = 1'b1;
  assign rxmcommaalignen_int[0:0] = ch0_rxmcommaalignen_int;
  assign rxmcommaalignen_int[1:1] = ch1_rxmcommaalignen_int;
  assign rxmcommaalignen_int[2:2] = ch2_rxmcommaalignen_int;
  assign rxmcommaalignen_int[3:3] = ch3_rxmcommaalignen_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rxpcommaalignen_int;
  wire [0:0] ch0_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch1_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch2_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch3_rxpcommaalignen_int = 1'b1;
  assign rxpcommaalignen_int[0:0] = ch0_rxpcommaalignen_int;
  assign rxpcommaalignen_int[1:1] = ch1_rxpcommaalignen_int;
  assign rxpcommaalignen_int[2:2] = ch2_rxpcommaalignen_int;
  assign rxpcommaalignen_int[3:3] = ch3_rxpcommaalignen_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rxpolarity_int;
  wire [0:0] ch0_rxpolarity_int = 1'b0;
  wire [0:0] ch1_rxpolarity_int = 1'b0;
  wire [0:0] ch2_rxpolarity_int = 1'b0;
  wire [0:0] ch3_rxpolarity_int = 1'b0;
  assign rxpolarity_int[0:0] = ch0_rxpolarity_int;
  assign rxpolarity_int[1:1] = ch1_rxpolarity_int;
  assign rxpolarity_int[2:2] = ch2_rxpolarity_int;
  assign rxpolarity_int[3:3] = ch3_rxpolarity_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [11:0] rxrate_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] tx8b10ben_int;
  wire [0:0] ch0_tx8b10ben_int = 1'b1;
  wire [0:0] ch1_tx8b10ben_int = 1'b1;
  wire [0:0] ch2_tx8b10ben_int = 1'b1;
  wire [0:0] ch3_tx8b10ben_int = 1'b1;
  assign tx8b10ben_int[0:0] = ch0_tx8b10ben_int;
  assign tx8b10ben_int[1:1] = ch1_tx8b10ben_int;
  assign tx8b10ben_int[2:2] = ch2_tx8b10ben_int;
  assign tx8b10ben_int[3:3] = ch3_tx8b10ben_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] txctrl0_int;
  wire [15:0] ch0_txctrl0_int;
  wire [15:0] ch1_txctrl0_int;
  wire [15:0] ch2_txctrl0_int;
  wire [15:0] ch3_txctrl0_int;
  assign txctrl0_int[15:0] = ch0_txctrl0_int;
  assign txctrl0_int[31:16] = ch1_txctrl0_int;
  assign txctrl0_int[47:32] = ch2_txctrl0_int;
  assign txctrl0_int[63:48] = ch3_txctrl0_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] txctrl1_int;
  wire [15:0] ch0_txctrl1_int;
  wire [15:0] ch1_txctrl1_int;
  wire [15:0] ch2_txctrl1_int;
  wire [15:0] ch3_txctrl1_int;
  assign txctrl1_int[15:0] = ch0_txctrl1_int;
  assign txctrl1_int[31:16] = ch1_txctrl1_int;
  assign txctrl1_int[47:32] = ch2_txctrl1_int;
  assign txctrl1_int[63:48] = ch3_txctrl1_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [31:0] txctrl2_int;
  wire [7:0] ch0_txctrl2_int;
  wire [7:0] ch1_txctrl2_int;
  wire [7:0] ch2_txctrl2_int;
  wire [7:0] ch3_txctrl2_int;
  assign txctrl2_int[7:0] = ch0_txctrl2_int;
  assign txctrl2_int[15:8] = ch1_txctrl2_int;
  assign txctrl2_int[23:16] = ch2_txctrl2_int;
  assign txctrl2_int[31:24] = ch3_txctrl2_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [19:0] txdiffctrl_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] txpolarity_int;
  wire [0:0] ch0_txpolarity_int = 1'b0;
  wire [0:0] ch1_txpolarity_int = 1'b0;
  wire [0:0] ch2_txpolarity_int = 1'b0;
  wire [0:0] ch3_txpolarity_int = 1'b0;
  assign txpolarity_int[0:0] = ch0_txpolarity_int;
  assign txpolarity_int[1:1] = ch1_txpolarity_int;
  assign txpolarity_int[2:2] = ch2_txpolarity_int;
  assign txpolarity_int[3:3] = ch3_txpolarity_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [19:0] txpostcursor_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [19:0] txprecursor_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] drpdo_int;
  wire [15:0] ch0_drpdo_int;
  wire [15:0] ch1_drpdo_int;
  wire [15:0] ch2_drpdo_int;
  wire [15:0] ch3_drpdo_int;
  assign ch0_drpdo_int = drpdo_int[15:0];
  assign ch1_drpdo_int = drpdo_int[31:16];
  assign ch2_drpdo_int = drpdo_int[47:32];
  assign ch3_drpdo_int = drpdo_int[63:48];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] drprdy_int;
  wire [0:0] ch0_drprdy_int;
  wire [0:0] ch1_drprdy_int;
  wire [0:0] ch2_drprdy_int;
  wire [0:0] ch3_drprdy_int;
  assign ch0_drprdy_int = drprdy_int[0:0];
  assign ch1_drprdy_int = drprdy_int[1:1];
  assign ch2_drprdy_int = drprdy_int[2:2];
  assign ch3_drprdy_int = drprdy_int[3:3];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] gtpowergood_int;
  wire [0:0] ch0_gtpowergood_int;
  wire [0:0] ch1_gtpowergood_int;
  wire [0:0] ch2_gtpowergood_int;
  wire [0:0] ch3_gtpowergood_int;
  assign ch0_gtpowergood_int = gtpowergood_int[0:0];
  assign ch1_gtpowergood_int = gtpowergood_int[1:1];
  assign ch2_gtpowergood_int = gtpowergood_int[2:2];
  assign ch3_gtpowergood_int = gtpowergood_int[3:3];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rxbyteisaligned_int;
  wire [0:0] ch0_rxbyteisaligned_int;
  wire [0:0] ch1_rxbyteisaligned_int;
  wire [0:0] ch2_rxbyteisaligned_int;
  wire [0:0] ch3_rxbyteisaligned_int;
  assign ch0_rxbyteisaligned_int = rxbyteisaligned_int[0:0];
  assign ch1_rxbyteisaligned_int = rxbyteisaligned_int[1:1];
  assign ch2_rxbyteisaligned_int = rxbyteisaligned_int[2:2];
  assign ch3_rxbyteisaligned_int = rxbyteisaligned_int[3:3];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rxbyterealign_int;
  wire [0:0] ch0_rxbyterealign_int;
  wire [0:0] ch1_rxbyterealign_int;
  wire [0:0] ch2_rxbyterealign_int;
  wire [0:0] ch3_rxbyterealign_int;
  assign ch0_rxbyterealign_int = rxbyterealign_int[0:0];
  assign ch1_rxbyterealign_int = rxbyterealign_int[1:1];
  assign ch2_rxbyterealign_int = rxbyterealign_int[2:2];
  assign ch3_rxbyterealign_int = rxbyterealign_int[3:3];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rxcommadet_int;
  wire [0:0] ch0_rxcommadet_int;
  wire [0:0] ch1_rxcommadet_int;
  wire [0:0] ch2_rxcommadet_int;
  wire [0:0] ch3_rxcommadet_int;
  assign ch0_rxcommadet_int = rxcommadet_int[0:0];
  assign ch1_rxcommadet_int = rxcommadet_int[1:1];
  assign ch2_rxcommadet_int = rxcommadet_int[2:2];
  assign ch3_rxcommadet_int = rxcommadet_int[3:3];

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] rxctrl0_int;
  wire [15:0] ch0_rxctrl0_int;
  wire [15:0] ch1_rxctrl0_int;
  wire [15:0] ch2_rxctrl0_int;
  wire [15:0] ch3_rxctrl0_int;
  assign ch0_rxctrl0_int = rxctrl0_int[15:0];
  assign ch1_rxctrl0_int = rxctrl0_int[31:16];
  assign ch2_rxctrl0_int = rxctrl0_int[47:32];
  assign ch3_rxctrl0_int = rxctrl0_int[63:48];

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] rxctrl1_int;
  wire [15:0] ch0_rxctrl1_int;
  wire [15:0] ch1_rxctrl1_int;
  wire [15:0] ch2_rxctrl1_int;
  wire [15:0] ch3_rxctrl1_int;
  assign ch0_rxctrl1_int = rxctrl1_int[15:0];
  assign ch1_rxctrl1_int = rxctrl1_int[31:16];
  assign ch2_rxctrl1_int = rxctrl1_int[47:32];
  assign ch3_rxctrl1_int = rxctrl1_int[63:48];

  //--------------------------------------------------------------------------------------------------------------------
  wire [31:0] rxctrl2_int;
  wire [7:0] ch0_rxctrl2_int;
  wire [7:0] ch1_rxctrl2_int;
  wire [7:0] ch2_rxctrl2_int;
  wire [7:0] ch3_rxctrl2_int;
  assign ch0_rxctrl2_int = rxctrl2_int[7:0];
  assign ch1_rxctrl2_int = rxctrl2_int[15:8];
  assign ch2_rxctrl2_int = rxctrl2_int[23:16];
  assign ch3_rxctrl2_int = rxctrl2_int[31:24];

  //--------------------------------------------------------------------------------------------------------------------
  wire [31:0] rxctrl3_int;
  wire [7:0] ch0_rxctrl3_int;
  wire [7:0] ch1_rxctrl3_int;
  wire [7:0] ch2_rxctrl3_int;
  wire [7:0] ch3_rxctrl3_int;
  assign ch0_rxctrl3_int = rxctrl3_int[7:0];
  assign ch1_rxctrl3_int = rxctrl3_int[15:8];
  assign ch2_rxctrl3_int = rxctrl3_int[23:16];
  assign ch3_rxctrl3_int = rxctrl3_int[31:24];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] rxpmaresetdone_int;
  wire [0:0] ch0_rxpmaresetdone_int;
  wire [0:0] ch1_rxpmaresetdone_int;
  wire [0:0] ch2_rxpmaresetdone_int;
  wire [0:0] ch3_rxpmaresetdone_int;
  assign ch0_rxpmaresetdone_int = rxpmaresetdone_int[0:0];
  assign ch1_rxpmaresetdone_int = rxpmaresetdone_int[1:1];
  assign ch2_rxpmaresetdone_int = rxpmaresetdone_int[2:2];
  assign ch3_rxpmaresetdone_int = rxpmaresetdone_int[3:3];

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] txpmaresetdone_int;
  wire [0:0] ch0_txpmaresetdone_int;
  wire [0:0] ch1_txpmaresetdone_int;
  wire [0:0] ch2_txpmaresetdone_int;
  wire [0:0] ch3_txpmaresetdone_int;
  assign ch0_txpmaresetdone_int = txpmaresetdone_int[0:0];
  assign ch1_txpmaresetdone_int = txpmaresetdone_int[1:1];
  assign ch2_txpmaresetdone_int = txpmaresetdone_int[2:2];
  assign ch3_txpmaresetdone_int = txpmaresetdone_int[3:3];


  // ===================================================================================================================
  // BUFFERS
  // ===================================================================================================================

  // Buffer the hb_gtwiz_reset_all_in input and logically combine it with the internal signal from the example
  // initialization block as well as the VIO-sourced reset
  wire hb_gtwiz_reset_all_init_int;
  wire hb_gtwiz_reset_all_int;

  assign hb_gtwiz_reset_all_int = hb_gtwiz_reset_all_init_int || hb_gtwiz_reset_all_vio_int;

  // // Globally buffer the free-running input clock
  // wire hb_gtwiz_reset_clk_freerun_buf_int;

  // BUFG bufg_clk_freerun_inst (
  //   .I (hb_gtwiz_reset_clk_freerun_in),
  //   .O (hb_gtwiz_reset_clk_freerun_buf_int)
  // );

  assign hb_gtwiz_reset_clk_freerun_out = hb_gtwiz_reset_clk_freerun_buf_int;  // Send globally buffered clock back out again.

  // Instantiate a differential reference clock buffer for each reference clock differential pair in this configuration,
  // and assign the single-ended output of each differential reference clock buffer to the appropriate PLL input signal

  generate
    case (QUAD_XYZ)
      124 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y0_int;  end // QUAD_124
      125 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y1_int;  end // QUAD_125
      126 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y2_int;  end // QUAD_126
      127 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y3_int;  end // QUAD_127
      128 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y4_int;  end // QUAD_128
      129 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y5_int;  end // QUAD_129
      131 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y7_int;  end // QUAD_131
      132 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y8_int;  end // QUAD_132
      134 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y10_int; end // QUAD_134
      135 : begin assign cm0_gtrefclk00_int = mgtrefclk0_x0y11_int; end // QUAD_135
      default : begin
        // do nothing
      end
    endcase
  endgenerate


  // ===================================================================================================================
  // USER CLOCKING RESETS
  // ===================================================================================================================

  // The TX user clocking helper block should be held in reset until the clock source of that block is known to be
  // stable. The following assignment is an example of how that stability can be determined, based on the selected TX
  // user clock source. Replace the assignment with the appropriate signal or logic to achieve that behavior as needed.
  assign hb0_gtwiz_userclk_tx_reset_int = ~(&txpmaresetdone_int);

  // The RX user clocking helper block should be held in reset until the clock source of that block is known to be
  // stable. The following assignment is an example of how that stability can be determined, based on the selected RX
  // user clock source. Replace the assignment with the appropriate signal or logic to achieve that behavior as needed.
  assign hb0_gtwiz_userclk_rx_reset_int = ~(&rxpmaresetdone_int);


  // ===================================================================================================================
  // PRBS STIMULUS, CHECKING, AND LINK MANAGEMENT
  // ===================================================================================================================

  // PRBS match and related link management
  // -------------------------------------------------------------------------------------------------------------------

  // Implement an example link status state machine using a simple leaky bucket mechanism. The link status indicates
  // the continual PRBS match status to both the top-level observer and the initialization state machine, while being
  // tolerant of occasional bit errors. This is an example and can be modified as necessary.
  localparam ST_LINK_DOWN = 1'b0;
  localparam ST_LINK_UP   = 1'b1;
  reg        sm_link      = ST_LINK_UP;

  // Synchronize the latched link down reset input and the VIO-driven signal into the free-running clock domain
  wire link_down_latched_reset_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_link_down_latched_reset_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (link_down_latched_reset_vio_int),
    // .i_in   (link_down_latched_reset_in || link_down_latched_reset_vio_int),
    .o_out  (link_down_latched_reset_sync)
  );


  // ===================================================================================================================
  // INITIALIZATION
  // ===================================================================================================================

  // Declare the receiver reset signals that interface to the reset controller helper block. For this configuration,
  // which uses the same PLL type for transmitter and receiver, the "reset RX PLL and datapath" feature is not used.
  wire hb_gtwiz_reset_rx_pll_and_datapath_int = 1'b0;
  wire hb_gtwiz_reset_rx_datapath_int;

  // Combine the receiver reset signals form the initialization module and the VIO to drive the appropriate reset
  // controller helper block reset input
  wire hb_gtwiz_reset_rx_datapath_init_int;

  assign hb_gtwiz_reset_rx_datapath_int = hb_gtwiz_reset_rx_datapath_init_int || hb_gtwiz_reset_rx_datapath_vio_int;

  // The example initialization module interacts with the reset controller helper block and other example design logic
  // to retry failed reset attempts in order to mitigate bring-up issues such as initially-unavilable reference clocks
  // or data connections. It also resets the receiver in the event of link loss in an attempt to regain link, so please
  // note the possibility that this behavior can have the effect of overriding or disturbing user-provided inputs that
  // destabilize the data stream. It is a demonstration only and can be modified to suit your system needs.
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_init example_init_inst (
    .clk_freerun_in  (hb_gtwiz_reset_clk_freerun_buf_int),
    .reset_all_in    (hb_gtwiz_reset_all_int),
    .tx_init_done_in (gtwiz_reset_tx_done_int),
    .rx_init_done_in (gtwiz_reset_rx_done_int),
    .rx_data_good_in (sm_link),
    .reset_all_out   (hb_gtwiz_reset_all_init_int),
    .reset_rx_out    (hb_gtwiz_reset_rx_datapath_init_int),
    .init_done_out   (init_done_int),
    .retry_ctr_out   (init_retry_ctr_int)
  );


  // ===================================================================================================================
  // VIO FOR HARDWARE BRING-UP AND DEBUG
  // ===================================================================================================================

  // Synchronize gtpowergood into the free-running clock domain for VIO usage
  wire [3:0] gtpowergood_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[0]),
    .o_out  (gtpowergood_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[1]),
    .o_out  (gtpowergood_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[2]),
    .o_out  (gtpowergood_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[3]),
    .o_out  (gtpowergood_vio_sync[3])
  );

  // Synchronize txpmaresetdone into the free-running clock domain for VIO usage
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[0]),
    .o_out  (txpmaresetdone_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[1]),
    .o_out  (txpmaresetdone_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[2]),
    .o_out  (txpmaresetdone_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[3]),
    .o_out  (txpmaresetdone_vio_sync[3])
  );

  // Synchronize rxpmaresetdone into the free-running clock domain for VIO usage
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[0]),
    .o_out  (rxpmaresetdone_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[1]),
    .o_out  (rxpmaresetdone_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[2]),
    .o_out  (rxpmaresetdone_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[3]),
    .o_out  (rxpmaresetdone_vio_sync[3])
  );

  // Synchronize gtwiz_reset_tx_done into the free-running clock domain for VIO usage
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_gtwiz_reset_tx_done_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtwiz_reset_tx_done_int[0]),
    .o_out  (gtwiz_reset_tx_done_vio_sync)
  );

  // Synchronize gtwiz_reset_rx_done into the free-running clock domain for VIO usage
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_bit_synchronizer bit_synchronizer_vio_gtwiz_reset_rx_done_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtwiz_reset_rx_done_int[0]),
    .o_out  (gtwiz_reset_rx_done_vio_sync)
  );


  // ===================================================================================================================
  // IN-SYSTEM IBERT FOR HARDWARE BRING-UP AND LINK ANALYSIS
  // ===================================================================================================================

  // // Instantiate the In-System IBERT IP core for hardware bring-up and link analysis purposes. For usage, refer to
  // // Vivado Design Suite User Guide: Programming and Debugging (UG908)
  // // In-System IBERT IP instance property dictionary is as follows:
  // // CONFIG.C_GT_TYPE {GTY} CONFIG.C_GTS_USED {X0Y0 X0Y1 X0Y2 X0Y3} CONFIG.C_ENABLE_INPUT_PORTS {true}
  // gtwizard_ultrascale_QUAD_124_10gbps_in_system_ibert_0 gtwizard_ultrascale_QUAD_124_10gbps_in_system_ibert_0_inst (
  //   .drpclk_o       (drpclk_int),
  //   .gt0_drpen_o    (drpen_int[0:0]),
  //   .gt0_drpwe_o    (drpwe_int[0:0]),
  //   .gt0_drpaddr_o  (drpaddr_int[9:0]),
  //   .gt0_drpdi_o    (drpdi_int[15:0]),
  //   .gt0_drprdy_i   (drprdy_int[0:0]),
  //   .gt0_drpdo_i    (drpdo_int[15:0]),
  //   .gt1_drpen_o    (drpen_int[1:1]),
  //   .gt1_drpwe_o    (drpwe_int[1:1]),
  //   .gt1_drpaddr_o  (drpaddr_int[19:10]),
  //   .gt1_drpdi_o    (drpdi_int[31:16]),
  //   .gt1_drprdy_i   (drprdy_int[1:1]),
  //   .gt1_drpdo_i    (drpdo_int[31:16]),
  //   .gt2_drpen_o    (drpen_int[2:2]),
  //   .gt2_drpwe_o    (drpwe_int[2:2]),
  //   .gt2_drpaddr_o  (drpaddr_int[29:20]),
  //   .gt2_drpdi_o    (drpdi_int[47:32]),
  //   .gt2_drprdy_i   (drprdy_int[2:2]),
  //   .gt2_drpdo_i    (drpdo_int[47:32]),
  //   .gt3_drpen_o    (drpen_int[3:3]),
  //   .gt3_drpwe_o    (drpwe_int[3:3]),
  //   .gt3_drpaddr_o  (drpaddr_int[39:30]),
  //   .gt3_drpdi_o    (drpdi_int[63:48]),
  //   .gt3_drprdy_i   (drprdy_int[3:3]),
  //   .gt3_drpdo_i    (drpdo_int[63:48]),
  //   .eyescanreset_o (eyescanreset_int),
  //   .rxrate_o       (rxrate_int),
  //   .txdiffctrl_o   (txdiffctrl_int),
  //   .txprecursor_o  (txprecursor_int),
  //   .txpostcursor_o (txpostcursor_int),
  //   .rxlpmen_o      (rxlpmen_int),
  //   .rxrate_i       ({4{3'b000}}),
  //   .txdiffctrl_i   ({4{5'b11000}}),
  //   .txprecursor_i  ({4{5'b00000}}),
  //   .txpostcursor_i ({4{5'b00000}}),
  //   .rxlpmen_i      ({4{1'b0}}),
  //   .rxoutclk_i     ({4{hb0_gtwiz_userclk_rx_usrclk2_int}}),
  //   .drpclk_i       ({4{hb_gtwiz_reset_clk_freerun_buf_int}}),
  //   .clk            (hb_gtwiz_reset_clk_freerun_buf_int)
  // );


  // ===================================================================================================================
  // EXAMPLE WRAPPER INSTANCE
  // ===================================================================================================================

  // Instantiate the example design wrapper, mapping its enabled ports to per-channel internal signals and example
  // resources as appropriate
  gtwizard_ultrascale_QUAD_XYZ_10gbps_example_wrapper #(
    .QUAD_XYZ(QUAD_XYZ)
  ) example_wrapper_inst (
     .gtyrxn_in                               (gtyrxn_int)
    ,.gtyrxp_in                               (gtyrxp_int)
    ,.gtytxn_out                              (gtytxn_int)
    ,.gtytxp_out                              (gtytxp_int)
    ,.gtwiz_userclk_tx_reset_in               (gtwiz_userclk_tx_reset_int)
    ,.gtwiz_userclk_tx_srcclk_out             (gtwiz_userclk_tx_srcclk_int)
    ,.gtwiz_userclk_tx_usrclk_out             (gtwiz_userclk_tx_usrclk_int)
    ,.gtwiz_userclk_tx_usrclk2_out            (gtwiz_userclk_tx_usrclk2_int)
    ,.gtwiz_userclk_tx_active_out             (gtwiz_userclk_tx_active_int)
    ,.gtwiz_userclk_rx_reset_in               (gtwiz_userclk_rx_reset_int)
    ,.gtwiz_userclk_rx_srcclk_out             (gtwiz_userclk_rx_srcclk_int)
    ,.gtwiz_userclk_rx_usrclk_out             (gtwiz_userclk_rx_usrclk_int)
    ,.gtwiz_userclk_rx_usrclk2_out            (gtwiz_userclk_rx_usrclk2_int)
    ,.gtwiz_userclk_rx_active_out             (gtwiz_userclk_rx_active_int)
    ,.gtwiz_reset_clk_freerun_in              ({1{hb_gtwiz_reset_clk_freerun_buf_int}})
    ,.gtwiz_reset_all_in                      ({1{hb_gtwiz_reset_all_int}})
    ,.gtwiz_reset_tx_pll_and_datapath_in      (gtwiz_reset_tx_pll_and_datapath_int)
    ,.gtwiz_reset_tx_datapath_in              (gtwiz_reset_tx_datapath_int)
    ,.gtwiz_reset_rx_pll_and_datapath_in      ({1{hb_gtwiz_reset_rx_pll_and_datapath_int}})
    ,.gtwiz_reset_rx_datapath_in              ({1{hb_gtwiz_reset_rx_datapath_int}})
    ,.gtwiz_reset_rx_cdr_stable_out           (gtwiz_reset_rx_cdr_stable_int)
    ,.gtwiz_reset_tx_done_out                 (gtwiz_reset_tx_done_int)
    ,.gtwiz_reset_rx_done_out                 (gtwiz_reset_rx_done_int)
    ,.gtwiz_userdata_tx_in                    (gtwiz_userdata_tx_int)
    ,.gtwiz_userdata_rx_out                   (gtwiz_userdata_rx_int)
    ,.gtrefclk00_in                           (gtrefclk00_int)
    ,.qpll0outclk_out                         (qpll0outclk_int)
    ,.qpll0outrefclk_out                      (qpll0outrefclk_int)
    ,.drpaddr_in                              (drpaddr_int)
    ,.drpclk_in                               (drpclk_int)
    ,.drpdi_in                                (drpdi_int)
    ,.drpen_in                                (drpen_int)
    ,.drpwe_in                                (drpwe_int)
    ,.eyescanreset_in                         (eyescanreset_int)
    ,.loopback_in                             (loopback_int)
    ,.rx8b10ben_in                            (rx8b10ben_int)
    ,.rxcommadeten_in                         (rxcommadeten_int)
    ,.rxlpmen_in                              (rxlpmen_int)
    ,.rxmcommaalignen_in                      (rxmcommaalignen_int)
    ,.rxpcommaalignen_in                      (rxpcommaalignen_int)
    ,.rxpolarity_in                           (rxpolarity_int)
    ,.rxrate_in                               (rxrate_int)
    ,.tx8b10ben_in                            (tx8b10ben_int)
    ,.txctrl0_in                              (txctrl0_int)
    ,.txctrl1_in                              (txctrl1_int)
    ,.txctrl2_in                              (txctrl2_int)
    ,.txdiffctrl_in                           (txdiffctrl_int)
    ,.txpolarity_in                           (txpolarity_int)
    ,.txpostcursor_in                         (txpostcursor_int)
    ,.txprecursor_in                          (txprecursor_int)
    ,.drpdo_out                               (drpdo_int)
    ,.drprdy_out                              (drprdy_int)
    ,.gtpowergood_out                         (gtpowergood_int)
    ,.rxbyteisaligned_out                     (rxbyteisaligned_int)
    ,.rxbyterealign_out                       (rxbyterealign_int)
    ,.rxcommadet_out                          (rxcommadet_int)
    ,.rxctrl0_out                             (rxctrl0_int)
    ,.rxctrl1_out                             (rxctrl1_int)
    ,.rxctrl2_out                             (rxctrl2_int)
    ,.rxctrl3_out                             (rxctrl3_int)
    ,.rxpmaresetdone_out                      (rxpmaresetdone_int)
    ,.txpmaresetdone_out                      (txpmaresetdone_int)
  );

  assign txctrl2_int                   = charisk_in;
  assign rxcommadet_out                = rxcommadet_int;
  assign gtwiz_userdata_tx_int         = gtwiz_userdata_tx_in;
  assign gtwiz_userdata_rx_out         = gtwiz_userdata_rx_int;
  assign gtwiz_userclk_rx_usrclk_out   = gtwiz_userclk_rx_usrclk_int;
  assign rxbyteisaligned_out           = rxbyteisaligned_int;
  assign rxctrl0_out                   = rxctrl0_int;
  assign rxctrl1_out                   = rxctrl1_int;
  assign rxctrl2_out                   = rxctrl2_int;
  assign rxctrl3_out                   = rxctrl3_int;
  assign gtwiz_reset_rx_cdr_stable_out = gtwiz_reset_rx_cdr_stable_int;

endmodule
