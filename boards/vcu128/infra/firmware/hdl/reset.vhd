library ieee;
use ieee.std_logic_1164.all;





entity reset is
    generic (
        NSTREAMS : integer
    );
    port (
        clk_free     : in std_logic;
        clk_i2c      : in std_logic;
        clk_link     : in std_logic_vector(NSTREAMS - 1 downto 0);
        clk_axi      : in std_logic;
        clk_hbm      : in std_logic;
        rst_global   : in std_logic;
        enable_i2c   : out std_logic; -- clk_i2c domain
        rst_i2c      : out std_logic; -- clk_i2c domain
        write_i2c    : out std_logic; -- clk_i2c domain
        rst_pll      : out std_logic; -- clk_free domain
        rst_tx       : out std_logic; -- clk_free domain
        rst_rx       : out std_logic; -- clk_free domain
        rst_link     : out std_logic_vector(NSTREAMS - 1 downto 0); -- clk_link domain
        rst_packager : out std_logic  -- clk_axi domain
    );
end reset;





architecture Behavioral of reset is
    constant I2C_READY_WAIT    : natural := 500;                  -- Find this out.
    constant I2C_RST_WAIT      : natural := I2C_READY_WAIT + 500; -- Find this out.
    constant I2C_WR_WAIT       : natural := I2C_RST_WAIT + 500;   -- Find this out.
    constant PLL_RST_WAIT      : natural := I2C_WR_WAIT + 500;    -- Find this out.
    constant TX_RST_WAIT       : natural := PLL_RST_WAIT + 500;   -- Find this out.
    constant RX_RST_WAIT       : natural := TX_RST_WAIT + 500;    -- Find this out.
    constant ALGO_RST_WAIT     : natural := RX_RST_WAIT + 500;    -- Find this out.
    constant FIFO_RST_WAIT     : natural := ALGO_RST_WAIT + 500;
    constant PACKAGER_RST_WAIT : natural := FIFO_RST_WAIT + 500; -- Find this out.

    signal rst_global_i2c, rst_global_free, rst_global_algo, rst_global_hbm : std_logic;
    signal rst_global_multilink : std_logic_vector(NSTREAMS - 1 downto 0);
begin

    sync_rst_to_clk_i2c : entity work.cdc_single
        port map(
            clk_src  => clk_axi,
            clk_dest => clk_i2c,
            d        => rst_global,
            q        => rst_global_i2c);

    reset_i2c : process (clk_i2c)
        variable wait_count : natural range 0 to 10000;
    begin
        if clk_i2c'event and clk_i2c = '1' then
            if rst_global_i2c = '1' then
                wait_count := 0;
                enable_i2c <= '1';
                rst_i2c    <= '1';
            else
                wait_count := wait_count + 1;
                if wait_count = I2C_READY_WAIT then
                    rst_i2c <= '0';
                end if;
                if wait_count = I2C_RST_WAIT then
                    write_i2c <= '1';
                end if;
                if wait_count = I2C_WR_WAIT then
                    write_i2c  <= '0';
                    enable_i2c <= '0';
                end if;
            end if;
        end if;
    end process;



    sync_rst_to_clk_free : entity work.cdc_single
        port map(
            clk_src  => clk_axi,
            clk_dest => clk_free,
            d        => rst_global,
            q        => rst_global_free);

    reset_mgt : process (clk_free)
        variable wait_count : natural range 0 to 10000;
    begin
        if clk_free'event and clk_free = '1' then
            if rst_global_free = '1' then
                wait_count := 0;
                rst_pll <= '1';
                rst_tx  <= '1';
                rst_rx  <= '1';
            else
                wait_count := wait_count + 1;
                if wait_count = PLL_RST_WAIT then
                    rst_pll <= '0';
                end if;
                if wait_count = TX_RST_WAIT then
                    rst_tx <= '0';
                end if;
                if wait_count = RX_RST_WAIT then
                    rst_rx <= '0';
                end if;
            end if;
        end if;
    end process;



    -- clk_link is the recovered clock
    sync_rst_to_clk_link_loop : for i in NSTREAMS - 1 downto 0 generate
    begin
        sync_rst_to_clk_link : entity work.cdc_single
            port map(
                clk_src  => clk_axi,
                clk_dest => clk_link(i),
                d        => rst_global,
                q        => rst_global_multilink(i));

        reset_algos : process (clk_link(i))
            variable wait_count : natural range 0 to 10000;
        begin
            if clk_link(i)'event and clk_link(i) = '1' then
                if rst_global_multilink(i) = '1' then
                    wait_count := 0;
                    rst_link(i) <= '1';
                else
                    wait_count := wait_count + 1;
                    if wait_count = ALGO_RST_WAIT then
                        rst_link(i) <= '0';
                    end if;
                end if;
            end if;
        end process;
    end generate sync_rst_to_clk_link_loop;



    sync_rst_to_clk_hbm : entity work.cdc_single
        port map(
            clk_src  => clk_axi,
            clk_dest => clk_hbm,
            d        => rst_global,
            q        => rst_global_hbm);

    reset_axi : process (clk_hbm)
        variable wait_count : natural range 0 to 10000;
    begin
        if clk_hbm'event and clk_hbm = '1' then
            if rst_global_hbm = '1' then
                wait_count := 0;
                rst_packager <= '1';
            else
                wait_count := wait_count + 1;
                if wait_count = PACKAGER_RST_WAIT then
                    rst_packager <= '0';
                end if;
            end if;
        end if;
    end process;

end Behavioral;
