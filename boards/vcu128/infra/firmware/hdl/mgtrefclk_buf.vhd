-- mgtrefclk_buf
--
-- Buffer of mgtrefclk's of xcvu37p
--
-- R. Ardino, February 2023
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.datatypes.all;





entity mgtrefclk_buf is
    generic (
        x0y0    :   boolean := false;
        x0y1    :   boolean := false;
        x0y2    :   boolean := false;
        x0y3    :   boolean := false;
        x0y4    :   boolean := false;
        x0y5    :   boolean := false;
        x0y7    :   boolean := false;
        x0y8    :   boolean := false;
        x0y10   :   boolean := false;
        x0y11   :   boolean := false
    );
    port (
        mgtrefclk0_x0y0_p           : in  std_logic;
        mgtrefclk0_x0y0_n           : in  std_logic;
        mgtrefclk0_x0y1_p           : in  std_logic;
        mgtrefclk0_x0y1_n           : in  std_logic;
        mgtrefclk0_x0y2_p           : in  std_logic;
        mgtrefclk0_x0y2_n           : in  std_logic;
        mgtrefclk0_x0y3_p           : in  std_logic;
        mgtrefclk0_x0y3_n           : in  std_logic;
        mgtrefclk0_x0y4_p           : in  std_logic;
        mgtrefclk0_x0y4_n           : in  std_logic;
        mgtrefclk0_x0y5_p           : in  std_logic;
        mgtrefclk0_x0y5_n           : in  std_logic;
        mgtrefclk0_x0y7_p           : in  std_logic;
        mgtrefclk0_x0y7_n           : in  std_logic;
        mgtrefclk0_x0y8_p           : in  std_logic;
        mgtrefclk0_x0y8_n           : in  std_logic;
        mgtrefclk0_x0y10_p          : in  std_logic;
        mgtrefclk0_x0y10_n          : in  std_logic;
        mgtrefclk0_x0y11_p          : in  std_logic;
        mgtrefclk0_x0y11_n          : in  std_logic;

        mgtrefclk0_x0y0_int         : out std_logic;
        mgtrefclk0_x0y1_int         : out std_logic;
        mgtrefclk0_x0y2_int         : out std_logic;
        mgtrefclk0_x0y3_int         : out std_logic;
        mgtrefclk0_x0y4_int         : out std_logic;
        mgtrefclk0_x0y5_int         : out std_logic;
        mgtrefclk0_x0y7_int         : out std_logic;
        mgtrefclk0_x0y8_int         : out std_logic;
        mgtrefclk0_x0y10_int        : out std_logic;
        mgtrefclk0_x0y11_int        : out std_logic;

        mgtrefclk0_x0y0_int_free    : out std_logic;
        mgtrefclk0_x0y1_int_free    : out std_logic;
        mgtrefclk0_x0y2_int_free    : out std_logic;
        mgtrefclk0_x0y3_int_free    : out std_logic;
        mgtrefclk0_x0y4_int_free    : out std_logic;
        mgtrefclk0_x0y5_int_free    : out std_logic;
        mgtrefclk0_x0y7_int_free    : out std_logic;
        mgtrefclk0_x0y8_int_free    : out std_logic;
        mgtrefclk0_x0y10_int_free   : out std_logic;
        mgtrefclk0_x0y11_int_free   : out std_logic
    );
end mgtrefclk_buf;





architecture Behavioral of mgtrefclk_buf is

    signal mgtrefclk0_x0y0_int_buf      : std_logic;    -- x0y0
    signal mgtrefclk0_x0y1_int_buf      : std_logic;    -- x0y1
    signal mgtrefclk0_x0y2_int_buf      : std_logic;    -- x0y2
    signal mgtrefclk0_x0y3_int_buf      : std_logic;    -- x0y3
    signal mgtrefclk0_x0y4_int_buf      : std_logic;    -- x0y4
    signal mgtrefclk0_x0y5_int_buf      : std_logic;    -- x0y5
    signal mgtrefclk0_x0y7_int_buf      : std_logic;    -- x0y7
    signal mgtrefclk0_x0y8_int_buf      : std_logic;    -- x0y8
    signal mgtrefclk0_x0y10_int_buf     : std_logic;    -- x0y10
    signal mgtrefclk0_x0y11_int_buf     : std_logic;    -- x0y11

begin

    ---- Differential reference clock buffer for mgtrefclk0_x0y0
    generate_x0y0 : if x0y0 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y0_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y0_p,
                IB    => mgtrefclk0_x0y0_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y0_int,
                ODIV2 => mgtrefclk0_x0y0_int_buf
            );

        BUFG_GT_X0Y0_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y0_int_buf,
                O       => mgtrefclk0_x0y0_int_free
            );
    end generate;



    ---- Differential reference clock buffer for mgtrefclk0_x0y1
    generate_x0y1 : if x0y1 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y1_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y1_p,
                IB    => mgtrefclk0_x0y1_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y1_int,
                ODIV2 => mgtrefclk0_x0y1_int_buf
            );

        BUFG_GT_X0Y1_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y1_int_buf,
                O       => mgtrefclk0_x0y1_int_free
            );
    end generate;



    ---- Differential reference clock buffer for mgtrefclk0_x0y2
    generate_x0y2 : if x0y2 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y2_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y2_p,
                IB    => mgtrefclk0_x0y2_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y2_int,
                ODIV2 => mgtrefclk0_x0y2_int_buf
            );

        BUFG_GT_X0Y2_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y2_int_buf,
                O       => mgtrefclk0_x0y2_int_free
            );
    end generate;



    ---- Differential reference clock buffer for mgtrefclk0_x0y3
    generate_x0y3 : if x0y3 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y3_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y3_p,
                IB    => mgtrefclk0_x0y3_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y3_int,
                ODIV2 => mgtrefclk0_x0y3_int_buf
            );

        BUFG_GT_X0Y3_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y3_int_buf,
                O       => mgtrefclk0_x0y3_int_free
            );
    end generate;



    ---- Differential reference clock buffer for mgtrefclk0_x0y4
    generate_x0y4 : if x0y4 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y4_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y4_p,
                IB    => mgtrefclk0_x0y4_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y4_int,
                ODIV2 => mgtrefclk0_x0y4_int_buf
            );

        BUFG_GT_X0Y4_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y4_int_buf,
                O       => mgtrefclk0_x0y4_int_free
            );
    end generate;



    ---- Differential reference clock buffer for mgtrefclk0_x0y5
    generate_x0y5 : if x0y5 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y5_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y5_p,
                IB    => mgtrefclk0_x0y5_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y5_int,
                ODIV2 => mgtrefclk0_x0y5_int_buf
            );

        BUFG_GT_X0Y5_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y5_int_buf,
                O       => mgtrefclk0_x0y5_int_free
            );
    end generate;



    ---- Differential reference clock buffer for mgtrefclk0_x0y7
    generate_x0y7 : if x0y7 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y7_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y7_p,
                IB    => mgtrefclk0_x0y7_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y7_int,
                ODIV2 => mgtrefclk0_x0y7_int_buf
            );

        BUFG_GT_X0Y7_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y7_int_buf,
                O       => mgtrefclk0_x0y7_int_free
            );
    end generate;



    ---- Differential reference clock buffer for mgtrefclk0_x0y8
    generate_x0y8 : if x0y8 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y8_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y8_p,
                IB    => mgtrefclk0_x0y8_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y8_int,
                ODIV2 => mgtrefclk0_x0y8_int_buf
            );

        BUFG_GT_X0Y8_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y8_int_buf,
                O       => mgtrefclk0_x0y8_int_free
            );
    end generate;



    ---- Differential reference clock buffer for mgtrefclk0_x0y10
    generate_x0y10 : if x0y10 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y10_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y10_p,
                IB    => mgtrefclk0_x0y10_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y10_int,
                ODIV2 => mgtrefclk0_x0y10_int_buf
            );

        BUFG_GT_X0Y10_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y10_int_buf,
                O       => mgtrefclk0_x0y10_int_free
            );
    end generate;



    ---- Differential reference clock buffer for mgtrefclk0_x0y11
    generate_x0y11 : if x0y11 = true generate
        IBUFDS_GTE4_MGTREFCLK0_X0Y11_INST : IBUFDS_GTE4
            generic map (
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX    => "00"
            )
            port map (
                I     => mgtrefclk0_x0y11_p,
                IB    => mgtrefclk0_x0y11_n,
                CEB   => '0',
                O     => mgtrefclk0_x0y11_int,
                ODIV2 => mgtrefclk0_x0y11_int_buf
            );

        BUFG_GT_X0Y11_INST : BUFG_GT
            port map (
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => mgtrefclk0_x0y11_int_buf,
                O       => mgtrefclk0_x0y11_int_free
            );
    end generate;

end Behavioral;
