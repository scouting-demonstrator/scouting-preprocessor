-- inputs
--
-- Optical serial inputs to 40 MHz scouting demonstrator
--
-- D. R. May 2018
-- R. Ardino, February 2023
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.datatypes.all;

entity inputs_10gbps is
    generic (
        N_QUAD   : integer;
        QUAD_MAP : TQuadMap(N_QUAD - 1 downto 0)
    );
    port (
        -- Serial data ports for FMCP transceiver
        mgtgtyrxn_in                                : in  std_logic_vector(4*N_QUAD - 1 downto 0);
        mgtgtyrxp_in                                : in  std_logic_vector(4*N_QUAD - 1 downto 0);
        mgtgtytxn_out                               : out std_logic_vector(4*N_QUAD - 1 downto 0);
        mgtgtytxp_out                               : out std_logic_vector(4*N_QUAD - 1 downto 0);

        clk_freerun                                 : in  std_logic;
        mgtrefclk0_x0y0_int                         : in  std_logic;
        mgtrefclk0_x0y1_int                         : in  std_logic;
        mgtrefclk0_x0y2_int                         : in  std_logic;
        mgtrefclk0_x0y3_int                         : in  std_logic;
        mgtrefclk0_x0y4_int                         : in  std_logic;
        mgtrefclk0_x0y5_int                         : in  std_logic;
        mgtrefclk0_x0y7_int                         : in  std_logic;
        mgtrefclk0_x0y8_int                         : in  std_logic;
        mgtrefclk0_x0y10_int                        : in  std_logic;
        mgtrefclk0_x0y11_int                        : in  std_logic;

        init_done_int                               : out std_logic_vector(N_QUAD - 1 downto 0);
        init_retry_ctr_int                          : out std_logic_vector(4*N_QUAD - 1 downto 0);
        gtpowergood_vio_sync                        : out std_logic_vector(4*N_QUAD - 1 downto 0);
        txpmaresetdone_vio_sync                     : out std_logic_vector(4*N_QUAD - 1 downto 0);
        rxpmaresetdone_vio_sync                     : out std_logic_vector(4*N_QUAD - 1 downto 0);
        gtwiz_reset_tx_done_vio_sync                : out std_logic_vector(N_QUAD - 1 downto 0);
        gtwiz_reset_rx_done_vio_sync                : out std_logic_vector(N_QUAD - 1 downto 0);
        hb_gtwiz_reset_all_vio_int                  : in  std_logic_vector(0 downto 0);
        hb0_gtwiz_reset_tx_pll_and_datapath_int     : in  std_logic_vector(0 downto 0);
        hb0_gtwiz_reset_tx_datapath_int             : in  std_logic_vector(0 downto 0);
        hb_gtwiz_reset_rx_pll_and_datapath_vio_int  : in  std_logic_vector(0 downto 0);
        hb_gtwiz_reset_rx_datapath_vio_int          : in  std_logic_vector(0 downto 0);
        link_down_latched_reset_vio_int             : in  std_logic_vector(0 downto 0);
        loopback                                    : in  std_logic_vector(3*4*N_QUAD - 1 downto 0);

        clk                                         : out std_logic_vector(4*N_QUAD - 1 downto 0);
        q                                           : out ldata(4*N_QUAD - 1 downto 0);
        oCommaDet                                   : out std_logic_vector(4*N_QUAD - 1 downto 0);
        oLinkData                                   : out std_logic_vector(32*4*N_QUAD - 1 downto 0);

        cdr_stable                                  : out std_logic_vector(N_QUAD - 1 downto 0);
        rxctrl0_out                                 : out std_logic_vector(16*4*N_QUAD - 1 downto 0);
        rxctrl1_out                                 : out std_logic_vector(16*4*N_QUAD - 1 downto 0);
        rxctrl2_out                                 : out std_logic_vector( 8*4*N_QUAD - 1 downto 0);
        rxctrl3_out                                 : out std_logic_vector( 8*4*N_QUAD - 1 downto 0);

        oRxbyteisaligned                            : out std_logic_vector(4*N_QUAD - 1 downto 0);

        ifcomma                                     : out std_logic_vector(4*N_QUAD - 1 downto 0);
        ifpadding                                   : out std_logic_vector(4*N_QUAD - 1 downto 0);
        ifinvalid                                   : out std_logic_vector(4*N_QUAD - 1 downto 0);
        ifdata                                      : out std_logic_vector(4*N_QUAD - 1 downto 0);
        ifelse                                      : out std_logic_vector(4*N_QUAD - 1 downto 0)
    );
end inputs_10gbps;





architecture Behavioral of inputs_10gbps is

    signal rx_data          : std_logic_vector(32*4*N_QUAD - 1 downto 0);
    signal tx_data          : std_logic_vector(32*4*N_QUAD - 1 downto 0);
    signal usr_clk          : std_logic_vector(4*N_QUAD - 1 downto 0);

    signal sRxbyteisaligned : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal sCommaDet        : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal sCharisk         : std_logic_vector(8*4*N_QUAD - 1 downto 0);

    signal rxctrl0_int      : std_logic_vector(16*4*N_QUAD - 1 downto 0);
    signal rxctrl1_int      : std_logic_vector(16*4*N_QUAD - 1 downto 0);
    signal rxctrl2_int      : std_logic_vector( 8*4*N_QUAD - 1 downto 0);
    signal rxctrl3_int      : std_logic_vector( 8*4*N_QUAD - 1 downto 0);

    -- DEBUG
    signal s_ifcomma        : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal s_ifpadding      : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal s_ifinvalid      : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal s_ifdata         : std_logic_vector(4*N_QUAD - 1 downto 0);
    signal s_ifelse         : std_logic_vector(4*N_QUAD - 1 downto 0);

begin

    ---------------------------------------------------------------------------
    --
    -- Wrappers
    --
    ---------------------------------------------------------------------------
    gty_wrapper_loop : for i in 0 to N_QUAD-1 generate
        gty_wrapper : entity work.gtwizard_ultrascale_QUAD_XYZ_10gbps_example_top
            generic map (
                QUAD_XYZ             => QUAD_MAP(i)
            )
            port map (
                -- Differential reference clock inputs
                mgtrefclk0_x0y0_int  => mgtrefclk0_x0y0_int,
                mgtrefclk0_x0y1_int  => mgtrefclk0_x0y1_int,
                mgtrefclk0_x0y2_int  => mgtrefclk0_x0y2_int,
                mgtrefclk0_x0y3_int  => mgtrefclk0_x0y3_int,
                mgtrefclk0_x0y4_int  => mgtrefclk0_x0y4_int,
                mgtrefclk0_x0y5_int  => mgtrefclk0_x0y5_int,
                mgtrefclk0_x0y7_int  => mgtrefclk0_x0y7_int,
                mgtrefclk0_x0y8_int  => mgtrefclk0_x0y8_int,
                mgtrefclk0_x0y10_int => mgtrefclk0_x0y10_int,
                mgtrefclk0_x0y11_int => mgtrefclk0_x0y11_int,

                -- Serial data ports for transceiver channel 0
                ch0_gtyrxn_in   => mgtgtyrxn_in (i*4 + 0),
                ch0_gtyrxp_in   => mgtgtyrxp_in (i*4 + 0),
                ch0_gtytxn_out  => mgtgtytxn_out(i*4 + 0),
                ch0_gtytxp_out  => mgtgtytxp_out(i*4 + 0),

                -- Serial data ports for transceiver channel 1
                ch1_gtyrxn_in   => mgtgtyrxn_in (i*4 + 1),
                ch1_gtyrxp_in   => mgtgtyrxp_in (i*4 + 1),
                ch1_gtytxn_out  => mgtgtytxn_out(i*4 + 1),
                ch1_gtytxp_out  => mgtgtytxp_out(i*4 + 1),

                -- Serial data ports for transceiver channel 2
                ch2_gtyrxn_in   => mgtgtyrxn_in (i*4 + 2),
                ch2_gtyrxp_in   => mgtgtyrxp_in (i*4 + 2),
                ch2_gtytxn_out  => mgtgtytxn_out(i*4 + 2),
                ch2_gtytxp_out  => mgtgtytxp_out(i*4 + 2),

                -- Serial data ports for transceiver channel 3
                ch3_gtyrxn_in   => mgtgtyrxn_in (i*4 + 3),
                ch3_gtyrxp_in   => mgtgtyrxp_in (i*4 + 3),
                ch3_gtytxn_out  => mgtgtytxn_out(i*4 + 3),
                ch3_gtytxp_out  => mgtgtytxp_out(i*4 + 3),

                -- User-provided ports for reset helper block(s)
                hb_gtwiz_reset_clk_freerun_buf_int          => clk_freerun,

                init_done_int                               => init_done_int(i),
                init_retry_ctr_int                          => init_retry_ctr_int((i+1)*4-1 downto i*4),
                gtpowergood_vio_sync                        => gtpowergood_vio_sync((i+1)*4-1 downto i*4),
                txpmaresetdone_vio_sync                     => txpmaresetdone_vio_sync((i+1)*4-1 downto i*4),
                rxpmaresetdone_vio_sync                     => rxpmaresetdone_vio_sync((i+1)*4-1 downto i*4),
                gtwiz_reset_tx_done_vio_sync                => gtwiz_reset_tx_done_vio_sync(i),
                gtwiz_reset_rx_done_vio_sync                => gtwiz_reset_rx_done_vio_sync(i),
                hb_gtwiz_reset_all_vio_int                  => hb_gtwiz_reset_all_vio_int,
                hb0_gtwiz_reset_tx_pll_and_datapath_int     => hb0_gtwiz_reset_tx_pll_and_datapath_int,
                hb0_gtwiz_reset_tx_datapath_int             => hb0_gtwiz_reset_tx_datapath_int,
                hb_gtwiz_reset_rx_pll_and_datapath_vio_int  => hb_gtwiz_reset_rx_pll_and_datapath_vio_int,
                hb_gtwiz_reset_rx_datapath_vio_int          => hb_gtwiz_reset_rx_datapath_vio_int,
                link_down_latched_reset_vio_int             => link_down_latched_reset_vio_int,
                loopback                                    => loopback((i+1)*12 - 1 downto i*12),
                rxbyteisaligned_out                         => sRxbyteisaligned((i+1)*4-1 downto i*4),

                charisk_in                                  => sCharisk((i+1)*32 - 1 downto i*32),
                rxcommadet_out                              => open,
                gtwiz_userdata_rx_out                       => rx_data((i+1)*128 - 1 downto i*128),
                gtwiz_userdata_tx_in                        => tx_data((i+1)*128 - 1 downto i*128),
                rxctrl0_out                                 => rxctrl0_int((i+1)*64 - 1 downto i*64),  -- If RXCTRL3 is low: Corresponding rxdata byte is a KWORD
                rxctrl1_out                                 => rxctrl1_int((i+1)*64 - 1 downto i*64),
                rxctrl2_out                                 => rxctrl2_int((i+1)*32 - 1 downto i*32),  -- Corresponding byte is a comma.
                rxctrl3_out                                 => rxctrl3_int((i+1)*32 - 1 downto i*32),  -- Can't decode the word.
                gtwiz_userclk_rx_usrclk_out                 => usr_clk((i+1)*4-1 downto i*4),
                gtwiz_reset_rx_cdr_stable_out               => cdr_stable(i)
            );
    end generate;
    ---------------------------------------------------------------------------



    ---------------------------------------------------------------------------
    --
    -- Data assignment and output
    --
    ---------------------------------------------------------------------------
    data_assignment_loop : for i in 0 to 4*N_QUAD-1 generate
        data_assignment : process(rx_data((i+1)*32 - 1 downto i*32),
                                  rxctrl0_int((i+1)*16 - 1 downto i*16),
                                  rxctrl2_int((i+1)*8 - 1 downto i*8),
                                  rxctrl3_int((i+1)*8 - 1 downto i*8),
                                  sRxbyteisaligned(i))
        begin

            q(i).data <= rx_data((i+1)*32 - 1 downto i*32);
            q(i).done <= '0';

            -- Comma word (0x505050BC)
            if rxctrl3_int(i*8) = '0' and rxctrl0_int(i*16) = '1' and rxctrl2_int(i*8) = '1' and sRxbyteisaligned(i) = '1' then
                sCommaDet(i) <= '1';
                q(i).valid   <= '0';
                q(i).strobe  <= '1';
                -- DEBUG
                s_ifcomma(i)   <= '1';
                s_ifpadding(i) <= '0';
                s_ifinvalid(i) <= '0';
                s_ifdata(i)    <= '0';
                s_ifelse(i)    <= '0';

            -- Padding word (0xF7F7F7F7)
            elsif rxctrl3_int(i*8 + 3 downto i*8) = "0000" and rxctrl0_int(i*16 + 3 downto i*16) = "1111" and sRxbyteisaligned(i) = '1' then
                sCommaDet(i) <= '0';
                q(i).valid   <= '1';
                q(i).strobe  <= '0';
                -- DEBUG
                s_ifcomma(i)   <= '0';
                s_ifpadding(i) <= '1';
                s_ifinvalid(i) <= '0';
                s_ifdata(i)    <= '0';
                s_ifelse(i)    <= '0';

            -- Invalid word
            elsif rxctrl3_int(i*8 + 3 downto i*8) = "1111" and rxctrl0_int(i*16 + 3 downto i*16) = "0000" then
                sCommaDet(i) <= '0';
                q(i).valid   <= '0';
                q(i).strobe  <= '0';
                -- DEBUG
                s_ifcomma(i)   <= '0';
                s_ifpadding(i) <= '0';
                s_ifinvalid(i) <= '1';
                s_ifdata(i)    <= '0';
                s_ifelse(i)    <= '0';

            -- Data word
            elsif rxctrl3_int(i*8 + 3 downto i*8) = "0000" and rxctrl0_int(i*16 + 3 downto i*16) = "0000" and rxctrl2_int(i*8) = '0' and sRxbyteisaligned(i) = '1' then
                sCommaDet(i) <= '0';
                q(i).valid   <= '1';
                q(i).strobe  <= '1';
                -- DEBUG
                s_ifcomma(i)   <= '0';
                s_ifpadding(i) <= '0';
                s_ifinvalid(i) <= '0';
                s_ifdata(i)    <= '1';
                s_ifelse(i)    <= '0';

            -- Others
            else
                sCommaDet(i) <= '0';
                q(i).valid   <= '0';
                q(i).strobe  <= '0';
                -- DEBUG
                s_ifcomma(i)   <= '0';
                s_ifpadding(i) <= '0';
                s_ifinvalid(i) <= '0';
                s_ifdata(i)    <= '0';
                s_ifelse(i)    <= '1';
            end if;
        end process;
    end generate;



    clk       <= usr_clk;
    oCommaDet <= sCommaDet;

    oLinkData(32*4*N_QUAD - 1 downto 0) <= rx_data;

    rxctrl0_out <= rxctrl0_int;
    rxctrl1_out <= rxctrl1_int;
    rxctrl2_out <= rxctrl2_int;
    rxctrl3_out <= rxctrl3_int;

    oRxbyteisaligned <= sRxbyteisaligned;

    ifcomma   <= s_ifcomma;
    ifpadding <= s_ifpadding;
    ifinvalid <= s_ifinvalid;
    ifdata    <= s_ifdata;
    ifelse    <= s_ifelse;

end Behavioral;
