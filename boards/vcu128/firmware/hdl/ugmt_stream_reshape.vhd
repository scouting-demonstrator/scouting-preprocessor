-- Bmtf input stubs stream reshaper
--
-- 2 columns x 6 frames
--           |
--           V
-- 8 columns x 2 frames
library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;





entity ugmt_stream_reshape is
    generic (
        NSTREAMS     : integer := 8
    );
    port (
        clk             : in  std_logic;
        rst             : in  std_logic;
        d               : in  adata;
        d_ctrl          : in  acontrol;
        q               : out adata;
        q_ctrl          : out acontrol;
        disable_reshape : in  std_logic
    );
end ugmt_stream_reshape;





architecture Behavioral of ugmt_stream_reshape is

    signal d1, d2, d3, d4, d5, d6                               : adata(NSTREAMS - 1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5, d_ctrl6 : acontrol;

begin

    stream_reshape : process(clk, rst)
    begin
        if rst = '1' then
            d1      <= (others => AWORD_NULL);
            d_ctrl1 <= ACONTROL_NULL;
            d2      <= (others => AWORD_NULL);
            d_ctrl2 <= ACONTROL_NULL;
            d3      <= (others => AWORD_NULL);
            d_ctrl3 <= ACONTROL_NULL;
            d4      <= (others => AWORD_NULL);
            d_ctrl4 <= ACONTROL_NULL;
            d5      <= (others => AWORD_NULL);
            d_ctrl5 <= ACONTROL_NULL;
        else
            if clk'event and clk = '1' then
                q       <= d5;
                q_ctrl  <= d_ctrl5;
                d5      <= d4;
                d_ctrl5 <= d_ctrl4;
                d4      <= d3;
                d_ctrl4 <= d_ctrl3;
                d3      <= d2;
                d_ctrl3 <= d_ctrl2;
                d2      <= d1;
                d_ctrl2 <= d_ctrl1;
                d1      <= d;
                d_ctrl1 <= d_ctrl;

                if d_ctrl5.valid = '1' and d_ctrl5.bx_start = '1' and disable_reshape = '0' then

                    ---- first frame
                    q (0) <= d5(0);
                    q (1) <= d5(1);
                    q (2) <= d5(2);
                    q (3) <= d5(3);
                    q (4) <= d4(0);
                    q (5) <= d4(1);
                    q (6) <= d4(2);
                    q (7) <= d4(3);
                    ---- second frame
                    d5(0) <= d3(0);
                    d5(1) <= d3(1);
                    d5(2) <= d3(2);
                    d5(3) <= d3(3);
                    d5(4) <= d2(0);
                    d5(5) <= d2(1);
                    d5(6) <= d2(2);
                    d5(7) <= d2(3);
                    ---- third frame
                    d4(0) <= d1(0);
                    d4(1) <= d1(1);
                    d4(2) <= d1(2);
                    d4(3) <= d1(3);
                    d4(4) <= d (0);
                    d4(5) <= d (1);
                    d4(6) <= d (2);
                    d4(7) <= d (3);
                    ---- fourth -> sixth frame: empty
                    d3    <= (others => AWORD_NULL);
                    d2    <= (others => AWORD_NULL);
                    d1    <= (others => AWORD_NULL);

                    if d_ctrl5.strobe = '1' then
                        q_ctrl.strobe  <= '1';
                        d_ctrl5.strobe <= '1';
                        d_ctrl4.strobe <= '1';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    else
                        q_ctrl.strobe  <= '0';
                        d_ctrl5.strobe <= '0';
                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

end Behavioral;
