-- Bmtf input stubs stream reshaper
--
-- 2 columns x 6 frames
--           |
--           V
-- 8 columns x 2 frames
library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;





entity bmtf_stream_reshape is
    generic (
        NSTREAMS     : integer := 8
    );
    port (
        clk             : in  std_logic;
        rst             : in  std_logic;
        d               : in  adata;
        d_ctrl          : in  acontrol;
        q               : out adata;
        q_ctrl          : out acontrol;
        disable_reshape : in  std_logic
    );
end bmtf_stream_reshape;





architecture Behavioral of bmtf_stream_reshape is

    signal d1, d2, d3, d4, d5, d6                               : adata(NSTREAMS - 1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5, d_ctrl6 : acontrol;

begin

    stream_reshape : process(clk, rst)
    begin
        if rst = '1' then
            d1      <= (others => AWORD_NULL);
            d_ctrl1 <= ACONTROL_NULL;
            d2      <= (others => AWORD_NULL);
            d_ctrl2 <= ACONTROL_NULL;
            d3      <= (others => AWORD_NULL);
            d_ctrl3 <= ACONTROL_NULL;
            d4      <= (others => AWORD_NULL);
            d_ctrl4 <= ACONTROL_NULL;
            d5      <= (others => AWORD_NULL);
            d_ctrl5 <= ACONTROL_NULL;
        else
            if clk'event and clk = '1' then
                q       <= d5;
                q_ctrl  <= d_ctrl5;
                d5      <= d4;
                d_ctrl5 <= d_ctrl4;
                d4      <= d3;
                d_ctrl4 <= d_ctrl3;
                d3      <= d2;
                d_ctrl3 <= d_ctrl2;
                d2      <= d1;
                d_ctrl2 <= d_ctrl1;
                d1      <= d;
                d_ctrl1 <= d_ctrl;

                if d_ctrl5.valid = '1' and d_ctrl5.bx_start = '1' and disable_reshape = '0' then

                    ---- Reshuffle and Rewrite for output
                    ---- first frame: first stub column
                    -- stub 1
                    q (0)(31 downto 00) <= d5(0)(31 downto 00);
                    q (1)(15 downto 00) <= d4(0)(15 downto 00);
                    q (1)(31 downto 16) <= (others => '0');
                    -- stub 2
                    q (2)(15 downto 00) <= d4(0)(31 downto 16);
                    q (2)(31 downto 16) <= d3(0)(15 downto 00);
                    q (3)(15 downto 00) <= d3(0)(31 downto 16);
                    q (3)(31 downto 16) <= (others => '0');
                    -- stub 3
                    q (4)(31 downto 00) <= d2(0)(31 downto 00);
                    q (5)(15 downto 00) <= d1(0)(15 downto 00);
                    q (5)(31 downto 16) <= (others => '0');
                    -- stub 4
                    q (6)(15 downto 00) <= d1(0)(31 downto 16);
                    q (6)(31 downto 16) <=  d(0)(15 downto 00);
                    q (7)(15 downto 00) <=  d(0)(31 downto 16);
                    q (7)(31 downto 16) <= (others => '0');
                    ---- second frame: second stub column
                    -- stub 5
                    d5(0)(31 downto 00) <= d5(1)(31 downto 00);
                    d5(1)(15 downto 00) <= d4(1)(15 downto 00);
                    d5(1)(31 downto 16) <= (others => '0');
                    -- stub 2
                    d5(2)(15 downto 00) <= d4(1)(31 downto 16);
                    d5(2)(31 downto 16) <= d3(1)(15 downto 00);
                    d5(3)(15 downto 00) <= d3(1)(31 downto 16);
                    d5(3)(31 downto 16) <= (others => '0');
                    -- stub 3
                    d5(4)(31 downto 00) <= d2(1)(31 downto 00);
                    d5(5)(15 downto 00) <= d1(1)(15 downto 00);
                    d5(5)(31 downto 16) <= (others => '0');
                    -- stub 4
                    d5(6)(15 downto 00) <= d1(1)(31 downto 16);
                    d5(6)(31 downto 16) <=  d(1)(15 downto 00);
                    d5(7)(15 downto 00) <=  d(1)(31 downto 16);
                    d5(7)(31 downto 16) <= (others => '0');
                    ---- third -> sixth frame: empty
                    d4                  <= (others => AWORD_NULL);
                    d3                  <= (others => AWORD_NULL);
                    d2                  <= (others => AWORD_NULL);
                    d1                  <= (others => AWORD_NULL);

                    if d_ctrl5.strobe = '1' then
                        q_ctrl.strobe  <= '1';
                        d_ctrl5.strobe <= '1';
                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    else
                        q_ctrl.strobe  <= '0';
                        d_ctrl5.strobe <= '0';
                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

end Behavioral;
