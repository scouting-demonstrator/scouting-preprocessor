-- Bmtf input stubs stream reshaper
--
-- 2 columns x 6 frames
--           |
--           V
-- 8 columns x 2 frames
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;





entity calo_stream_reshape is
    generic (
        NSTREAMS    : integer := 8;
        OBJ_TYPE    : integer
    );
    port (
        clk             : in  std_logic;
        rst             : in  std_logic;
        d               : in  adata;
        d_ctrl          : in  acontrol;
        q               : out adata;
        q_ctrl          : out acontrol;
        disable_reshape : in  std_logic
    );
end calo_stream_reshape;





architecture Behavioral of calo_stream_reshape is

    signal d1, d2, d3, d4, d5, d6                               : adata(NSTREAMS - 1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5, d_ctrl6 : acontrol;

begin

    stream_reshape : process(clk, rst)
    begin
        if rst = '1' then
            d1      <= (others => AWORD_NULL);
            d_ctrl1 <= ACONTROL_NULL;
            d2      <= (others => AWORD_NULL);
            d_ctrl2 <= ACONTROL_NULL;
            d3      <= (others => AWORD_NULL);
            d_ctrl3 <= ACONTROL_NULL;
            d4      <= (others => AWORD_NULL);
            d_ctrl4 <= ACONTROL_NULL;
            d5      <= (others => AWORD_NULL);
            d_ctrl5 <= ACONTROL_NULL;
        else
            if clk'event and clk = '1' then
                q       <= d5;
                q_ctrl  <= d_ctrl5;
                d5      <= d4;
                d_ctrl5 <= d_ctrl4;
                d4      <= d3;
                d_ctrl4 <= d_ctrl3;
                d3      <= d2;
                d_ctrl3 <= d_ctrl2;
                d2      <= d1;
                d_ctrl2 <= d_ctrl1;
                d1      <= d;
                d_ctrl1 <= d_ctrl;

                if d_ctrl5.valid = '1' and d_ctrl5.bx_start = '1' and disable_reshape = '0' then

                    ---- Reshuffle and Rewrite for output
                    case OBJ_TYPE is
                        when 0|1|3 =>
                            ---- first frame
                            q (0) <= d5(1);
                            q (1) <= d4(1);
                            q (2) <= d3(1);
                            q (3) <= d2(1);
                            q (4) <= d1(1);
                            q (5) <= d (1);
                            q (6) <= d5(0);
                            q (7) <= d4(0);
                            ---- second frame
                            d5(0) <= d3(0);
                            d5(1) <= d2(0);
                            d5(2) <= d1(0);
                            d5(3) <= d (0);
                            d5(4) <= std_logic_vector(to_unsigned(d_ctrl5.bx_counter, 32)); -- for debugging purposes but it might simplify BX decoding in the future...
                            d5(5) <= AWORD_NULL;
                            d5(6) <= AWORD_NULL;
                            d5(7) <= AWORD_NULL;

                        when 2 =>
                            ---- first frame
                            q (0) <= d5(1);
                            q (1) <= d4(1);
                            q (2) <= d3(1);
                            q (3) <= d2(1);
                            q (4) <= d1(1);
                            q (5) <= d (1);
                            q (6) <= std_logic_vector(to_unsigned(d_ctrl5.bx_counter, 32)); -- for debugging purposes but it might simplify BX decoding in the future...
                            q (7) <= AWORD_NULL;
                            d5    <= (others => AWORD_NULL);

                        when others =>
                            null;
                    end case;

                    ---- third -> sixth frame: empty
                    d4 <= (others => AWORD_NULL);
                    d3 <= (others => AWORD_NULL);
                    d2 <= (others => AWORD_NULL);
                    d1 <= (others => AWORD_NULL);

                    if d_ctrl5.strobe = '1' then
                        case OBJ_TYPE is
                            when 0|1|3 =>
                                q_ctrl.strobe  <= '1';
                                d_ctrl5.strobe <= '1';
                            when 2 =>
                                q_ctrl.strobe  <= '1';
                                d_ctrl5.strobe <= '0';
                            when others =>
                                null;
                        end case;

                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    else
                        q_ctrl.strobe  <= '0';
                        d_ctrl5.strobe <= '0';
                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

end Behavioral;
