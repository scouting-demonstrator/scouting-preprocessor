-- Ugt stream reshaper
--
-- Doesn't do anything at the moment
library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;





entity ugt_stream_reshape is
    generic (
        NSTREAMS     : integer := 8
    );
    port (
        clk             : in  std_logic;
        rst             : in  std_logic;
        d               : in  adata;
        d_ctrl          : in  acontrol;
        q               : out adata;
        q_ctrl          : out acontrol;
        disable_reshape : in  std_logic
    );
end ugt_stream_reshape;





architecture Behavioral of ugt_stream_reshape is

    signal d1, d2, d3, d4, d5, d6                               : adata(NSTREAMS - 1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5, d_ctrl6 : acontrol;

begin

    stream_reshape : process(clk, rst)
    begin
        if rst = '1' then
            d1      <= (others => AWORD_NULL);
            d_ctrl1 <= ACONTROL_NULL;
            d2      <= (others => AWORD_NULL);
            d_ctrl2 <= ACONTROL_NULL;
            d3      <= (others => AWORD_NULL);
            d_ctrl3 <= ACONTROL_NULL;
            d4      <= (others => AWORD_NULL);
            d_ctrl4 <= ACONTROL_NULL;
            d5      <= (others => AWORD_NULL);
            d_ctrl5 <= ACONTROL_NULL;
        else
            if clk'event and clk = '1' then
                q       <= d5;
                q_ctrl  <= d_ctrl5;
                d5      <= d4;
                d_ctrl5 <= d_ctrl4;
                d4      <= d3;
                d_ctrl4 <= d_ctrl3;
                d3      <= d2;
                d_ctrl3 <= d_ctrl2;
                d2      <= d1;
                d_ctrl2 <= d_ctrl1;
                d1      <= d;
                d_ctrl1 <= d_ctrl;

                if d_ctrl5.valid = '1' and d_ctrl5.bx_start = '1' and disable_reshape = '0' then
                    if d_ctrl5.strobe = '1' then
                        q_ctrl.strobe  <= '1';
                        d_ctrl5.strobe <= '1';
                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    else
                        q_ctrl.strobe  <= '0';
                        d_ctrl5.strobe <= '0';
                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

end Behavioral;
