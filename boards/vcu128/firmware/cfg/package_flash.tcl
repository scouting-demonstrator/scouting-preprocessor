# package_flash.tcl: Script for producing memory configuration file for vcu128

puts "Package flash in [pwd]"
set current_dir [pwd]
set bitfile [exec find . -name "*.bit"]
write_cfgmem -format bin -size 256 -interface SPIx4 -loadbit {up 0x00000000 "products/scouting_build.bit"} -checksum -file "products/scouting_build.bin"
