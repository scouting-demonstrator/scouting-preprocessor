################################################################################
#
# LEDs
#
################################################################################
set_property PACKAGE_PIN BH24 [get_ports GPIO_LED_0_LS]
set_property IOSTANDARD LVCMOS18 [get_ports GPIO_LED_0_LS]
set_property PACKAGE_PIN BG24 [get_ports GPIO_LED_1_LS]
set_property IOSTANDARD LVCMOS18 [get_ports GPIO_LED_1_LS]
set_property PACKAGE_PIN BG25 [get_ports GPIO_LED_2_LS]
set_property IOSTANDARD LVCMOS18 [get_ports GPIO_LED_2_LS]
set_property PACKAGE_PIN BF25 [get_ports GPIO_LED_3_LS]
set_property IOSTANDARD LVCMOS18 [get_ports GPIO_LED_3_LS]
set_property PACKAGE_PIN BF26 [get_ports GPIO_LED_4_LS]
set_property IOSTANDARD LVCMOS18 [get_ports GPIO_LED_4_LS]
set_property PACKAGE_PIN BF27 [get_ports GPIO_LED_5_LS]
set_property IOSTANDARD LVCMOS18 [get_ports GPIO_LED_5_LS]
set_property PACKAGE_PIN BG27 [get_ports GPIO_LED_6_LS]
set_property IOSTANDARD LVCMOS18 [get_ports GPIO_LED_6_LS]
set_property PACKAGE_PIN BG28 [get_ports GPIO_LED_7_LS]
set_property IOSTANDARD LVCMOS18 [get_ports GPIO_LED_7_LS]
################################################################################





################################################################################
#
# Clocks
#
################################################################################
# QDR4 100 MHz clk
set_property PACKAGE_PIN BJ4 [get_ports CLK1_100MHZ_P]
set_property PACKAGE_PIN BK3 [get_ports CLK1_100MHZ_N]
set_property IOSTANDARD LVDS [get_ports CLK1_100MHZ_P]
set_property IOSTANDARD LVDS [get_ports CLK1_100MHZ_N]


# HBM
set_property PACKAGE_PIN BH51 [get_ports HBM_clk_ref_p]
set_property PACKAGE_PIN BJ51 [get_ports HBM_clk_ref_n]
set_property IOSTANDARD LVDS [get_ports HBM_clk_ref_p]
set_property IOSTANDARD LVDS [get_ports HBM_clk_ref_n]


# FMCP mgtrefclks
set_property package_pin AV43 [get_ports mgtrefclk0_x0y0_n]
set_property package_pin AV42 [get_ports mgtrefclk0_x0y0_p]
set_property package_pin AR41 [get_ports mgtrefclk0_x0y1_n]
set_property package_pin AR40 [get_ports mgtrefclk0_x0y1_p]
set_property package_pin AN41 [get_ports mgtrefclk0_x0y2_n]
set_property package_pin AN40 [get_ports mgtrefclk0_x0y2_p]
set_property package_pin AL41 [get_ports mgtrefclk0_x0y3_n]
set_property package_pin AL40 [get_ports mgtrefclk0_x0y3_p]
set_property package_pin AJ41 [get_ports mgtrefclk0_x0y4_n]
set_property package_pin AJ40 [get_ports mgtrefclk0_x0y4_p]
set_property package_pin AG41 [get_ports mgtrefclk0_x0y5_n]
set_property package_pin AG40 [get_ports mgtrefclk0_x0y5_p]


# On-board QSFPs
set_property PACKAGE_PIN AB42 [get_ports mgtrefclk0_x0y7_p]
set_property PACKAGE_PIN Y42  [get_ports mgtrefclk0_x0y8_p]
set_property PACKAGE_PIN T42  [get_ports mgtrefclk0_x0y10_p]
set_property PACKAGE_PIN P42  [get_ports mgtrefclk0_x0y11_p]
################################################################################





################################################################################
#
# QSFPs
#
################################################################################
# QSFP4
set_property PACKAGE_PIN BK23 [get_ports QSFP_D_MODSEL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_D_MODSEL]
set_property PACKAGE_PIN BK24 [get_ports QSFP_D_RESETL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_D_RESETL]
set_property PACKAGE_PIN BL22 [get_ports QSFP_D_MODPRSL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_D_MODPRSL]
set_property PACKAGE_PIN BH21 [get_ports QSFP_D_INTL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_D_INTL]
set_property PACKAGE_PIN BF23 [get_ports QSFP_D_LPMODE]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_D_LPMODE]

# QSFP3
set_property PACKAGE_PIN BM5 [get_ports QSFP_C_MODSEL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_C_MODSEL]
set_property PACKAGE_PIN BL6 [get_ports QSFP_C_RESETL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_C_RESETL]
set_property PACKAGE_PIN BM7 [get_ports QSFP_C_MODPRSL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_C_MODPRSL]
set_property PACKAGE_PIN BL7 [get_ports QSFP_C_INTL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_C_INTL]
set_property PACKAGE_PIN BN4 [get_ports QSFP_C_LPMODE]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_C_LPMODE]

# QSFP2
set_property PACKAGE_PIN BN5 [get_ports QSFP_B_MODSEL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_B_MODSEL]
set_property PACKAGE_PIN BN6 [get_ports QSFP_B_RESETL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_B_RESETL]
set_property PACKAGE_PIN BN7 [get_ports QSFP_B_MODPRSL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_B_MODPRSL]
set_property PACKAGE_PIN BP6 [get_ports QSFP_B_INTL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_B_INTL]
set_property PACKAGE_PIN BP7 [get_ports QSFP_B_LPMODE]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_B_LPMODE]

# QSFP1
set_property PACKAGE_PIN BM24 [get_ports QSFP_A_MODSEL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_A_MODSEL]
set_property PACKAGE_PIN BN25 [get_ports QSFP_A_RESETL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_A_RESETL]
set_property PACKAGE_PIN BM25 [get_ports QSFP_A_MODPRSL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_A_MODPRSL]
set_property PACKAGE_PIN BP24 [get_ports QSFP_A_INTL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_A_INTL]
set_property PACKAGE_PIN BN24 [get_ports QSFP_A_LPMODE]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_A_LPMODE]
################################################################################





################################################################################
#
# PCIe
#
################################################################################
# PCIe interface on X3Y0  PCIE40E4_X1Y0  to profite from the early load of the PCIe interface
set_property PACKAGE_PIN AL2 [get_ports PCIE_RX0_P]
set_property PACKAGE_PIN AL11 [get_ports PCIE_TX0_P]

set_property PACKAGE_PIN AL15 [get_ports PCIE_clk_p]

set_property PACKAGE_PIN BF41 [get_ports PCIE_rst]
set_property IOSTANDARD LVCMOS12 [get_ports PCIE_rst]
################################################################################





################################################################################
#
# I2C sda/scl
#
################################################################################
###  TCA9548
## sel 0x80   QSFP 4   DAQ
## sel 0x20   QSFP 2 SR Sender
## sel 0x10   QSFP 1 SR receive
set_property PACKAGE_PIN BM27 [get_ports QSFP_MAIN_SCL]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_MAIN_SCL]
set_property PACKAGE_PIN BL28 [get_ports QSFP_MAIN_SDA]
set_property IOSTANDARD LVCMOS18 [get_ports QSFP_MAIN_SDA]
################################################################################
