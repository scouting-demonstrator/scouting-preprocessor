set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/WRITE_TO_HBM_LOOP*[*].SND_NEW_reg*[*]*/C}] -to [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*.Readout_HBM_for_ETH*/SND_NEW_resync*[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Snd_una_OK_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/sync_SND_UNA_reg[*]/D}]
set_false_path 																														  -to [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/usr_data_rd_reg[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/TCP_win_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/Init_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/TCP_max_size_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/TCP_scale_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/TCP_TS_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/TCP_TS_rply_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/snd_cwnd_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/load_new_snd_cwnd_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/timer_rtt_I_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/timer_rtt_I_sync_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/timer_persist_I_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/thresold_retrans_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/TCP_Rexmt_CWND_sh_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/TCP_stream_number_reg*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/TCP_ST_S_Length*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/TCP_ST_S_Seq_num*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/flag_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/stat_snd_rexmit_byte_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/stat_dupack_max_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/stat_wnd_max_rg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/stat_wnd_min_rg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/measure_RTT_max_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/TCP_state*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/local_RTT*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/measure_RTT_min_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/Measure_RTT_accu_rg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/READ_FROM_HBM_LOOP*/TCP_logic*/rtt_shifts_MAX_reg*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM*/HBM_data*/*READ_FROM_HBM_LOOP*/TCP_logic*/*/TCP_stream_number*[*]*/C}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ HBM_*/*READ_FROM_HBM_LOOP*/TCP_logic*/stats_i0/out_dt*[*]/D}]

set _xlnx_shared_i0 [get_pins -hierarchical -filter {NAME =~ HBM_*/*READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/Ctrl_out_rg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/MSS_reg*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/option_SCALE*[*]/C}] -to $_xlnx_shared_i0
set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*READ_FROM_HBM_LOOP*/TCP_logic*/Register_setting*/TCP_win_from_HOST*[*]/C}] -to $_xlnx_shared_i0


##################################################
#  HBM

set_false_path -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data_*/usr_data_rd_reg*[*]/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data_*/Control_data_out*[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data_*/HBM_*/HBM_User_address*[*]/C}]



#set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/first_word_reg[*]/C}]               -to [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_1_0/result_reg_reg[*]/D}]
#set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/second_word_reg[*]/C}]              -to [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_1_1/result_reg_reg[*]/D}]
#set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/third_word_reg[*]/C}]               -to [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_1_2/result_reg_reg[*]/D}]
#set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/fourth_word_reg[*]/C}]              -to [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_1_3/result_reg_reg[*]/D}]

#set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_1_0/result_reg_reg[*]/C}]  -to [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_2/result_reg_reg[*]/D}]
#set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_1_1/result_reg_reg[*]/C}]  -to [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_2/result_reg_reg[*]/D}]
#set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_1_2/result_reg_reg[*]/C}]  -to [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_2/result_reg_reg[*]/D}]
#set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_1_3/result_reg_reg[*]/C}]  -to [get_pins -hierarchical -filter {NAME =~HBM*/Data_manager_*/Readout_HBM_for_ETH_*/CS_compute_i1/CS_stage_2/result_reg_reg[*]/D}]

set_false_path -from [get_pins -hier -filter {NAME=~ HBM_*/HBM_data_i1/*HBM_STREAM_i1/AXI_reg*_resetn*/C}] -to [get_pins -hier -filter {NAME=~ HBM_*/HBM_data_i1/*HBM_STREAM_i1/*AXI_reg1_resetn*/D}]


set_multicycle_path 3 -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/TCP_win_from_HOST_reg*[*]/D}]
set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/TCP_win_from_HOST_reg*[*]/D}]
set_multicycle_path 3 -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/option_SCALE*[*]/CE}]
set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/option_SCALE*[*]/CE}]
set_multicycle_path 3 -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/option_SCALE*[*]/D}]
set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/option_SCALE*[*]/D}]
set_multicycle_path 3 -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/MSS_reg*[*]/CE}]
set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/MSS_reg*[*]/CE}]
set_multicycle_path 3 -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/MSS_reg*[*]/D}]
set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/MSS_reg*[*]/D}]

set_multicycle_path 3 -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/rcv_seg_SEQ*[*]/D}]
set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/rcv_seg_SEQ*[*]/D}]
set_multicycle_path 3 -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/rcv_seg_ACK*[*]/D}]
set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/rcv_seg_ACK*[*]/D}]
set_multicycle_path 3 -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/rcv_FLAG*[*]/D}]
set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/rcv_FLAG*[*]/D}]
set_multicycle_path 3 -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/rcv_len*[*]/D}]
set_multicycle_path 1 -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/FIFO_rcv*/inst_fifo_gen*/RDCLK}] -to [get_pins -hierarchical -filter {NAME =~ HBM_*/HBM_data*/*Readout_HBM_for_ETH*/TCP_logic*/rcv_len*[*]/D}]
