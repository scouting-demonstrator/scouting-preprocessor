set_false_path -to [get_pins -hierarchical -filter {NAME =~ freq_measure_*/measure_reg*[*]/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ freq_measure_*/freq_measure_*/measure_reg*[*]/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ_*_led/resync_mem_pulse_reg/D}]

set_false_path -to [get_pins -hierarchical -filter {NAME =~ data_in_rg*[*]/D}]


set_property USER_SLR_ASSIGNMENT SLR1 [get_cells -hierarchical -filter {NAME =~ *DAQ_Bxxx_p00/buf_packet_i1*}]
set_property USER_SLR_ASSIGNMENT SLR2 [get_cells -hierarchical -filter {NAME =~ *DAQ_Bxxx_p01/buf_packet_i1*}]
set_property USER_SLR_ASSIGNMENT SLR2 [get_cells -hierarchical -filter {NAME =~ *DAQ_Bxxx_p02/buf_packet_i1*}]

set_property USER_SLR_ASSIGNMENT SLR0 [get_cells -hierarchical -filter {NAME =~ *stream_loop[*].convert_to_blocks/*}]