create_generated_clock -name ena_clock_i2c -source [get_pins {monitor_interface/PCIe_serdes_itf/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O}] [get_pins {ena_clock_i2c_reg/Q}]

set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/*/reg_1st_stage*/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/*/reg_2nd_stage_async*/D}] 3.000
set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/*/Start_LLDP_pckt*/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/*/Start_LLDP_pckt_resync*/D}] 3.000
set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/*/mem_req_ARP*/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_B*/ethernet*/*/reg_async*/D}] 3.000

set_false_path -from [get_pins -hierarchical -filter {NAME =~ MAC_bytes_low*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/eth_decoder*/*MSS_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/eth_decoder*/ARP_MAC_src_rg*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/counter_rst*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/nb_retrans_val*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/ping_req_counter*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/ping_rec_counter*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/nb_retrans_val*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/counter_pause_frame_reg*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/delay_pause_frame_reg*[*]/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/ethernet*/Ctrl_setup_register*/PORT_D_reg*[*]*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i0/Ctrl_setup_register_i1/PORT_S_reg_reg[*][*]*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i0/Ctrl_setup_register_i1/PORT_D_reg_reg[*][*]*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i0/Ctrl_setup_register_i1/IP_S_reg*[*]*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i0/Ctrl_setup_register_i1/IP_D_reg*[*]*/C}]

#static values
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_*/ARP_probe*/ARP_dst_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_*/ARP_probe*/ARP_gw_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_*/ARP_probe*/ARP_probe_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_*/ARP_probe*/probe_DONE_reg*/C}]



set_false_path -to [get_pins {DAQ_*/ethernet_i0/Ctrl_setup_register_i1/Ctrl_out_rg*[*]/D}]

set_false_path -from [get_pins {DAQ_*/serdes_i1/AXI_rq_add*[*]/C}]
set_false_path -from [get_pins {DAQ_*/serdes_i1/AXI_rq_dti*[*]/C}]
set_false_path -from [get_pins {DAQ_*/serdes_i1/AXI_rq_BE*[*]/C}]
set_false_path -from [get_pins {DAQ_*/serdes_i1/AXI_tick*/C}]

set_false_path -from [get_pins {DAQ_*/ethernet_i0/LLDP_i1/word_counter_setup*[*]*/C}]
set_false_path -from [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_timeout_cnt*[*]*/C}]
set_false_path -from [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_mem_add*[*]*/C}]
set_false_path -from [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_mem_add*[*]*/C}]
set_false_path -from [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_mem_ena*[*]*/C}]
set_false_path -from [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_mem_wr_dt*[*]*/C}] -to [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_memory_i1/word_LLDP*[*][*]*/D}]
set_false_path -from [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_memory_i1/word_LLDP*[*][*]/C}] -to [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_memory*/reg_out_ctrl*[*]*/D}]
set_false_path -to [get_pins {DAQ_*/ethernet_i0/LLDP_i1/usr_out_reg*[*]*/D}]

set_multicycle_path -setup -from [get_pins {DAQ_*/ethernet_i0/eth_decoder_i1/Px_PORT_Home_reg_reg[*]/C}] -to [get_pins {DAQ_*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg[*]/CE}] 3
set_multicycle_path -hold -from [get_pins {DAQ_*/ethernet_i0/eth_decoder_i1/Px_PORT_Home_reg_reg[*]/C}] -to [get_pins {DAQ_*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg[*]/CE}] 2
set_multicycle_path -setup -from [get_pins {DAQ_*/ethernet_i0/eth_decoder_i1/Px_PORT_Home_reg_reg[*]/C}] -to [get_pins {DAQ_*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg[*]/D}] 3
set_multicycle_path -hold -from [get_pins {DAQ_*/ethernet_i0/eth_decoder_i1/Px_PORT_Home_reg_reg[*]/C}] -to [get_pins {DAQ_*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg[*]/D}] 2

set _xlnx_shared_i0 [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/Ctrl_setup_register_*/PORT_D_reg*/C}]
set_false_path -from $_xlnx_shared_i0 -to [get_pins DAQ_*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg*/D]
set_false_path -from $_xlnx_shared_i0 -to [get_pins DAQ_*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg*/CE]
set_false_path -from $_xlnx_shared_i0 -to [get_pins DAQ_*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg*/D]
set_false_path -from $_xlnx_shared_i0 -to [get_pins DAQ_*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg*/CE]

set_false_path -from [get_pins {DAQ_*/ethernet_i0/Ctrl_setup_register_i1/IP_GATEWAY_reg*[*]*/C}]


# Decoder timing cosntraints
set_multicycle_path -setup -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*packet_decode*/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*packet_decode*/D}] 3
set_multicycle_path -hold -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*packet_decode*/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*packet_decode*/D}] 2

set _xlnx_shared_i1 [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/packet_valid*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/packet_valid*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/packet_error*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/packet_error*/D}] 1
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_rcvOK_reg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_rcvOK_reg*/D}]
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_gwOK_reg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_gwOK_reg*/D}]
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_MAC_Dest_rg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_MAC_Dest_rg*/D}]
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_MAC_GateWay_rg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_MAC_GateWay_rg*/D}]
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/check_MAC_src_rg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/check_MAC_src_rg*/D}]
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/check_IP_src_rg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/check_IP_src_rg*/D}]
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_MAC_error_rg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_MAC_error_rg*/D}]
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/ARP_MAC_src_rg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/ARP_MAC_src_rg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/ARP_IP_src_rg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/ARP_IP_src_rg*/D}] 1
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_Dest_rg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_Dest_rg*/D}]
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_GateWay_rg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_GateWay_rg*/D}]
set_multicycle_path 2 -setup -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_error_rg*/D}]
set_multicycle_path 1 -hold -through [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/*rcv_buffer*/*dout*}]  -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/MAC_error_rg*/D}]
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_PORT_Host_reg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_PORT_Host_reg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_PORT_Home_reg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_PORT_Home_reg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_SEQ_number_reg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_SEQ_number_reg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_ACK_number_reg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_ACK_number_reg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_TCP_flag_reg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_TCP_flag_reg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_TCP_len_reg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_TCP_len_reg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/TCP_win_tmp*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/TCP_win_tmp*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_option_MMS*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_option_MMS*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_option_SCALE*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/Px_option_SCALE*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_Px_option_MMS_rg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_Px_option_MMS_rg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/update_MSS*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/update_MSS*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_Px_option_SCALE_rg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/latch_Px_option_SCALE_rg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/update_Scale*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/update_Scale*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_data_rg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_data_rg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_tmp_data*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_tmp_data*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_Last_dt_rg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_Last_dt_rg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/delay_pause_frame_reg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/delay_pause_frame_reg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_MAC_dst_rg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_MAC_dst_rg*/D}] 1
set_multicycle_path -setup -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_IP_dst_rg*/D}] 2
set_multicycle_path -hold -through $_xlnx_shared_i1 -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/ethernet_i*/eth_decoder_*/PING_IP_dst_rg*/D}] 1

set_false_path -from [get_pins {DAQ_*/ethernet_i0/Ping_time_lathced*[*]*/C}] -to [get_pins {DAQ_*/ethernet_i0/Data_rd_reg*[*]*/D}]

