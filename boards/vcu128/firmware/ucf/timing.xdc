set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ *sync*/reg_1st_stage*/C}] -to [get_pins -hierarchical -filter {NAME =~ *sync*/reg_2nd_stage_async*/D}] 4.000
set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ */C}] -to [get_pins -hierarchical -filter {NAME =~ *resync_sig*/reg_async*/D}] 4.000

set_false_path -to [get_pins -hierarchical -filter {NAME =~ *resync_reset*/reg*[*]*/CLR}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *resync_reset*/reg*[*]*/PRE}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *reset_resync*/reg*[*]*/CLR}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *reset_resync*/reg*[*]*/PRE}]

#set_false_path                                                                                       -to [get_pins {resync_rst*/reg_reg[*]*/CLR}]

#  Bu in the IP or Vivado ??  AR# 72607

## top file
set_false_path -from [get_pins QSFP_I2C_RESET_cell*/C] -to [get_pins -hierarchical -filter {NAME =~ data_in_rg*[*]/D}]
set_false_path -from [get_pins FF_I2C_RESET_cell*/C] -to [get_pins -hierarchical -filter {NAME =~ data_in_rg*[*]/D}]
set_false_path -from [get_pins Clock_I2C_RESET_cell*/C] -to [get_pins -hierarchical -filter {NAME =~ data_in_rg*[*]/D}]
#


set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*led/*/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ*led/mem_pulse*/CLR}]


set _xlnx_shared_i0 [get_pins -hierarchical -filter {NAME =~ *stat*[*]/CLR}]
set_false_path -to $_xlnx_shared_i0
set _xlnx_shared_i1 [get_pins -hierarchical -filter {NAME =~ *stat*[*]/PRE}]
set_false_path -to $_xlnx_shared_i1
set _xlnx_shared_i2 [get_pins -hierarchical -filter {NAME =~ *backpressure*[*]/CLR}]
set_false_path -to $_xlnx_shared_i2
set_false_path                                                                                  -to [get_pins -hierarchical -filter {NAME =~ *backpressure*[*]/PRE}]
set _xlnx_shared_i3 [get_pins -hierarchical -filter {NAME =~ *BIFI_max*[*]/CLR}]
set_false_path -to $_xlnx_shared_i3
set_false_path                                                                                  -to [get_pins -hierarchical -filter {NAME =~ *BIFI_max*[*]/PRE}]
set _xlnx_shared_i4 [get_pins -hierarchical -filter {NAME =~ *check_i1*[*]/CLR}]
set_false_path -to $_xlnx_shared_i4
set_false_path                                                                                  -to [get_pins -hierarchical -filter {NAME =~ *check_i1*[*]/PRE}]

set_false_path -to [get_pins -hierarchical -filter {NAME =~ Merger_DAQ*/data_in_rg*[*]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ *freq_measure*/counter_measure*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *freq_measure*/measure*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *freq_measure*/*freq_measure*/counter_measure*[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *freq_measure*/*freq_measure*/measure*[*]/D}]

# As the reset on statistic registers are multiple Cylces with flase path

set_false_path -to $_xlnx_shared_i0
set_false_path -to $_xlnx_shared_i1
set_false_path -to $_xlnx_shared_i2
set_false_path   -to [get_pins -hierarchical -filter {NAME =~ *backpressure*[*]/PRE}]
set_false_path -to $_xlnx_shared_i3
set_false_path   -to [get_pins -hierarchical -filter {NAME =~ *BIFI_max*[*]/PRE}]
set_false_path -to $_xlnx_shared_i4
set_false_path   -to [get_pins -hierarchical -filter {NAME =~ *check_i1*[*]/PRE}]


#
set_false_path -from [get_pins freq_meas_clk_axi/store_freq_reg/C] -to [get_pins {freq_meas_clk_axi/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_link/store_freq_reg/C] -to [get_pins {freq_meas_clk_link/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_x0y11/store_freq_reg/C] -to [get_pins {freq_meas_clk_x0y11/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_x0y5/store_freq_reg/C] -to [get_pins {freq_meas_clk_x0y5/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_x0y4/store_freq_reg/C] -to [get_pins {freq_meas_clk_x0y4/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_x0y3/store_freq_reg/C] -to [get_pins {freq_meas_clk_x0y3/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_x0y2/store_freq_reg/C] -to [get_pins {freq_meas_clk_x0y2/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_x0y1/store_freq_reg/C] -to [get_pins {freq_meas_clk_x0y1/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_x0y0/store_freq_reg/C] -to [get_pins {freq_meas_clk_x0y0/store_freq_d_reg[0]/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ *reset_controller/rst_pll_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *sync_reset_tx_pll_to_clk_axi/xpm_cdc_single_inst/src_ff_reg/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *reset_controller/rst_tx_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *sync_reset_tx_datapath_to_clk_axi/xpm_cdc_single_inst/src_ff_reg/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *reset_controller/rst_rx_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *sync_reset_rx_datapath_to_clk_axi/xpm_cdc_single_inst/src_ff_reg/D}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ *local_reset_reg[0]/C}] -to [get_pins -hierarchical -filter {NAME =~ *reset_controller/sync_rst_to_clk_link_loop[*].sync_rst_to_clk_link/xpm_cdc_single_inst/src_ff_reg/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *sync_enable_data_gen/xpm_cdc_single_inst/syncstages_ff_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *sync_d_align_loop[*].sync_d_align_*/xpm_cdc_array_single_inst/src_ff_reg[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *sync_enable_data_gen/xpm_cdc_single_inst/syncstages_ff_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *sync_d_align_loop[*].sync_d_align_*/xpm_cdc_single_inst/src_ff_reg/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *auto_realign_controller_1/sync_aligner_rst_to_clk/xpm_cdc_single_inst/syncstages_ff_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *inputs_align_loop[*].inputs_align/reset_all_fifos_reg/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *inputs_align_loop[*].inputs_align/q_reg[*][data][*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *sync_d_align_loop[*].sync_d_align_data/xpm_cdc_array_single_inst/src_ff_reg[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *data_generator/q_int_reg[*][data][*]/C}] -to [get_pins -hierarchical -filter {NAME =~ *sync_d_align_loop[*].sync_d_align_data/xpm_cdc_array_single_inst/src_ff_reg[*]/D}]

set_false_path -to [get_pins -hierarchical -filter {NAME =~ *xpm_cdc_single_inst/src_ff_reg/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *xpm_cdc_array_single_inst/src_ff_reg[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *xpm_cdc_single_inst/syncstages_ff_reg[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ *xpm_cdc_array_single_inst/syncstages_ff_reg[*][*]/C}]

set_false_path -to [get_pins {ugmt_ila_i/U0/PROBE_PIPE.shift_probes_reg[*][*]/D}]
set_false_path -to [get_pins {calo_ila_i/U0/PROBE_PIPE.shift_probes_reg[*][*]/D}]
set_false_path -to [get_pins {bmtf_ila_i/U0/PROBE_PIPE.shift_probes_reg[*][*]/D}]
set_false_path -to [get_pins {ugt_ila_i/U0/PROBE_PIPE.shift_probes_reg[*][*]/D}]

set_false_path -from [get_pins {MAC_bytes_low[*][*]/C}]