create_clock -period 10.000 -name CLK_100MHZ_P [get_ports CLK_100MHZ_P]

create_clock -period 10.000 -name HBM_clock_ref0_p [get_ports HBM_clock_ref0_p]
create_clock -period 10.000 -name HBM_clock_ref1_p [get_ports HBM_clock_ref1_p]

create_generated_clock -name ena_clock_i2c -source [get_pins pll_inst/inst/mmcme4_adv_inst/CLKOUT0] -divide_by 64 [get_pins ena_clock_i2c_reg/Q]

create_clock -period 10.000 -name PCIE_clk_p [get_ports PCIE_clk_p]

set_false_path -from [get_ports PCIE_rst]

set_false_path -to [get_ports ClockGen_MAIN_RESET]
set_false_path -to [get_ports sysMon_i2c_sda]
set_false_path -to [get_ports sysMon_i2c_sclk]
set_false_path -to [get_ports ClockGen_MAIN_SDA]
set_false_path -to [get_ports ClockGen_MAIN_SCL]
set_false_path -to [get_ports FireFly_MAIN_SDA]
set_false_path -to [get_ports FireFly_MAIN_SCL]
set_false_path -to [get_ports QSFP_MAIN_SDA]
set_false_path -to [get_ports QSFP_MAIN_SCL]
set_false_path -to [get_ports SN_MAIN_SDA]
set_false_path -to [get_ports SN_MAIN_SCL]

set_multicycle_path -setup -to [get_ports sysMon_i2c_sclk] 60
set_multicycle_path -hold -to [get_ports sysMon_i2c_sclk] 3
set_multicycle_path -setup -to [get_ports ClockGen_MAIN_SCL] 60
set_multicycle_path -hold -to [get_ports ClockGen_MAIN_SCL] 3
set_multicycle_path -setup -to [get_ports FireFly_MAIN_SCL] 60
set_multicycle_path -hold -to [get_ports FireFly_MAIN_SCL] 3
set_multicycle_path -setup -to [get_ports QSFP_MAIN_SCL] 60
set_multicycle_path -hold -to [get_ports QSFP_MAIN_SCL] 3
set_multicycle_path -setup -to [get_ports SN_MAIN_SCL] 60
set_multicycle_path -hold -to [get_ports SN_MAIN_SCL] 3

set_multicycle_path -setup -to [get_ports ClockGen_MAIN_SDA] 60
set_multicycle_path -hold -to [get_ports ClockGen_MAIN_SDA] 3
set_multicycle_path -setup -to [get_ports FireFly_MAIN_SDA] 60
set_multicycle_path -hold -to [get_ports FireFly_MAIN_SDA] 3
set_multicycle_path -setup -to [get_ports QSFP_MAIN_SDA] 60
set_multicycle_path -hold -to [get_ports QSFP_MAIN_SDA] 3
set_multicycle_path -setup -to [get_ports SN_MAIN_SDA] 60
set_multicycle_path -hold -to [get_ports SN_MAIN_SDA] 3
set_multicycle_path -setup -to [get_ports sysMon_i2c_sda] 60


set_false_path -to [get_ports {Flash_add[*]}]
set_false_path -to [get_ports GPIO_LED_*_LS]

#
set_max_delay -to [get_ports GPIO_LED_0_LS] 20.000
set_max_delay -to [get_ports GPIO_LED_1_LS] 20.000
set_max_delay -to [get_ports GPIO_LED_2_LS] 20.000
set_max_delay -to [get_ports GPIO_LED_3_LS] 20.000
set_max_delay -to [get_ports GPIO_LED_4_LS] 20.000
set_max_delay -to [get_ports GPIO_LED_5_LS] 20.000
set_max_delay -to [get_ports GPIO_LED_6_LS] 20.000
set_max_delay -to [get_ports GPIO_LED_7_LS] 20.000

set_max_delay -to [get_ports QSFP_*_MODSEL] 20.000
set_max_delay -to [get_ports QSFP_*_RESETL] 20.000
set_max_delay -to [get_ports QSFP_*_LPMODE] 20.000

set_false_path -from [get_ports QSFP_*_MODPRSL]
set_false_path -from [get_ports QSFP_*_INTL]

create_clock -period 10000.000 -name POWER_MAIN_SCL_virtual
create_clock -period 10000.000 -name SN_MAIN_SCL_virtual
create_clock -period 10000.000 -name ClockGen_MAIN_SCL_virtual
create_clock -period 10000.000 -name HMC_MAIN_SCL_virtual
create_clock -period 10000.000 -name FireFly_MAIN_SCL_virtual
create_clock -period 10000.000 -name QSFP_MAIN_SCL_virtual

#
set_false_path -to [get_ports ClockGen_*_rst_n]




create_clock -name clk_mgtrefclk0_x0y0_p  -period 6.4 [get_ports mgtrefclk0_x0y0_p]
create_clock -name clk_mgtrefclk0_x0y1_p  -period 6.4 [get_ports mgtrefclk0_x0y1_p]
create_clock -name clk_mgtrefclk0_x0y2_p  -period 6.4 [get_ports mgtrefclk0_x0y2_p]
create_clock -name clk_mgtrefclk0_x0y3_p  -period 6.4 [get_ports mgtrefclk0_x0y3_p]
create_clock -name clk_mgtrefclk0_x0y4_p  -period 6.4 [get_ports mgtrefclk0_x0y4_p]
create_clock -name clk_mgtrefclk0_x0y5_p  -period 6.4 [get_ports mgtrefclk0_x0y5_p]

create_clock -period 3.103 -name mgtrefclk0_x0y7_p  -waveform {0.000 1.552} [get_ports mgtrefclk0_x0y7_p]
create_clock -period 3.103 -name mgtrefclk0_x0y8_p  -waveform {0.000 1.552} [get_ports mgtrefclk0_x0y8_p]
create_clock -period 3.103 -name mgtrefclk0_x0y10_p -waveform {0.000 1.552} [get_ports mgtrefclk0_x0y10_p]
create_clock -period 3.103 -name mgtrefclk0_x0y11_p -waveform {0.000 1.552} [get_ports mgtrefclk0_x0y11_p]