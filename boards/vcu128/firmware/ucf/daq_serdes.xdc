#set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/*/reg_1st_stage*/C}]   		                                -to [get_pins -hierarchical -filter {NAME =~ DAQ_B*/*/reg_2nd_stage_async*/D}] 3.000
set_max_delay -to [get_pins -hierarchical -filter {NAME =~ DAQ_B*/serdes_i1/MAC_GT_share_logic_i1/i_MAC100GBv1_reset_wrapper/i_MAC100GBv1_exdes_cmac_cdc_sync_axi_usr_rx_serdes_reset*/reg_async_reg/D}] 3.000

set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ DAQ_*/serdes_i1/gt_rxprbssel_async_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_*/serdes_i1/gt_rxprbssel_async_reg[*]/D}] 3.000
## DAQ*/serdes*

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/axi_inst*/axi_dt_rd*/C}]                                            -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/axi_inst*/FSM_sequential_AXI_access*/C}]                            -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/axi_inst*/axi_error_wr*/C}]                                         -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/axi_inst*/axi_error_rd*/C}]                                         -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/data_in_rg*[*]/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gt_txprbssel_sync_inter*/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gt_rxprbssel_sync_inter*/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gt_rxprbscntreset_sync_inter*/D}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/*txoutclksel_async*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/*gt_txpcsreset_async*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gt_reset*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/sys_reset*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/tx_diff_ctrl*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/txpostcursor*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/txprecursor*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/tx_polarity*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/rx_polarity*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/rx_rxlpmen*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gt_loopback_in_rg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/txpippmen_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/txpippmsel_reg*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/rx_crc_error_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/rx_frame_l*_err_cnt_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/tx_ovfout_mem*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/tx_unfout_mem*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/stat_rx_bip_err_cnt_reg*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/stat_rx_pause_counter*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/stat_tx_local_fault_cnt*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/stat_tx_bad_fcs_cnt*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/stat_tx_broadcast_cnt*/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/stat_tx_frame_error_cnt*/C}]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/stat_tx_frame_error_cnt*/C}]



# drp for commun
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/drp_addr*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/drp_di*[*]/C}]

# drp for channel
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gtx_ch_drp_addr*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gtx_ch_drp_di*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gtx_ch_drp_en*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/gtx_ch_drp_do*[*]/C}]


set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ */gt_txprbssel_async*/C}] -to [get_pins -hierarchical -filter {NAME =~ */gt_txprbssel_sync*/D}] 3.000
set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ */gt_rxprbssel_async*/C}] -to [get_pins -hierarchical -filter {NAME =~ */gt_rxprbssel_sync*/D}] 3.000

#enable are resync
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/AXI_rq_add*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/AXI_rq_dti*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/AXI_rq_BE*[*]/C}]

#has to be checked
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/ctl_tx_pause_req*[*]/C}]
set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ*/serdes*/ctl_tx_resend_pause*/C}]


set_false_path -from [get_pins -hierarchical -filter {NAME =~ DAQ_B*/*/i_MAC100GBv1_0_axi4_lite_if_wrapper/*/RESET_REG_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ DAQ_B*/serdes_i1/*/i_MAC100GBv1_reset_wrapper/*/reg_async_reg/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ DAQ_B*/serdes_i1/*/reg_async_*/D}]