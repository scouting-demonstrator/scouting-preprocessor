create_waiver -type CDC -id {CDC-14} -user "dgigi" -desc "value stable when write or read is done" -from [get_pins {DAQ_*/serdes_i1/*drp_di*[*]/C}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-14} -user "dgigi" -desc "value stable when write or read is done" -from [get_pins {DAQ_*/serdes_i1/*drp_addr*[*]/C}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"

create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value is stable when aread or write occurs" -from [get_pins {DAQ*/ethernet_i0/LLDP_i1/LLDP_mem_add_reg[*]/C}] -to [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_memory_i1/word_LLDP_reg[*][*]/CE}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value is stable when aread or write occurs" -from [get_pins {DAQ*/ethernet_i0/LLDP_i1/LLDP_mem_add_reg[*]/C}] -to [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_memory_i1/word_LLDP_reg[*][*]/D}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value is stable when aread or write occurs" -from [get_pins {DAQ*/ethernet_i0/LLDP_i1/LLDP_mem_add_reg[*]/C}] -to [get_pins {DAQ_*/ethernet_i0/LLDP_i1/LLDP_memory_i1/word_LLDP_reg[*][*]/R}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Data are stable when the WEN = '1'" -from [get_pins {DAQ*/ethernet_i0/LLDP_i1/LLDP_mem_wr_dt_reg[*]/C}] -to [get_pins {DAQ*/ethernet_i0/LLDP_i1/LLDP_memory_i1/word_LLDP_reg[*][*]/D}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "stable data when we read (address is present for multiple clocks before" -from [get_pins {DAQ*/ethernet_i0/LLDP_i1/LLDP_memory_i1/word_LLDP_reg[*][*]/C}] -to [get_pins {DAQ*/ethernet_i0/LLDP_i1/LLDP_memory_i1/reg_out_ctrl_reg[*]/D}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"

create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "port value is stable during the operation" -from [get_pins {DAQ_*/ethernet_i0/Ctrl_setup_register_i1/PORT_S_reg_reg[*][*]/C}] -to [get_pins {DAQ_*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg[*]/CE}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"

create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "data read by controller are stable" -to [get_pins {DAQ*/ethernet_i0/Data_rd_reg*[*]/D}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"

create_waiver -type CDC -id {CDC-4} -user "dgigi" -desc "data are stable multiple clock cycles before" -from [get_pins {DAQ*/serdes_i1/gtx_ch_drp_do_reg[*]/C}] -to [get_pins {DAQ*/serdes_i1/data_in_rg*[*]/D}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "data are stable multiple clock cycles before" -to [get_pins {DAQ*/serdes_i1/data_in_rg*[*]/D}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-4} -user "dgigi" -desc "data are stable multiple clock cycles before" -to [get_pins {DAQ*/serdes_i1/data_in_rg*[*]/D}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"

create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value are stable when used" -from [get_pins {DAQ*/ethernet_i0/Ctrl_setup_register_i1/PORT_D_reg*[*][*]/C}] -to [get_pins {DAQ*/ethernet_i0/eth_decoder_i1/TCP_port_number_reg_reg[*]/CE}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"

create_waiver -type CDC -id {CDC-13} -user "dgigi" -desc "The addresses registers are stable when the read or write accesses accoured" -from [get_pins {HBM_*/HBM_data_i1/*HBM_STREAM_*/HBM_User_address*[*]/C}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-14} -user "dgigi" -desc "The addresses registers are stable when the read or write accesses accoured" -from [get_pins {HBM_*/HBM_data_i1/*HBM_STREAM_*/HBM_User_address*[*]/C}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"

create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value are set before the RUN start and are stable" -from [get_pins {HBM_*/HBM_data_i1/*/TCP_logic_*/Register_setting_i1/Init_reg_reg[*]/C}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value are set before the RUN start and are stable" -from [get_pins {HBM_*/HBM_data_i1/*/TCP_logic_*/Register_setting_i1/thresold_retrans_reg*[*]/C}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value are set before the RUN start and are stable" -from [get_pins {HBM_*/HBM_data_i1/*/TCP_logic_*/Register_setting_i1/TCP_stream_number_reg*[*]/C}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-4} -user "dgigi" -desc "To be review if problem of stability of value (read status)" -from [get_pins {HBM*/HBM_data_i1/*Readout_HBM_for_ETH*/BIFI_max_reg*[*]/C}] -to [get_pins {HBM*/HBM_data_i1/*Readout_HBM_for_ETH*/usr_data_rd_reg*[*]/D}] -timestamp "Thu Oct 21 14:53:11 GMT 2021"
create_waiver -type CDC -id {CDC-4} -user "dgigi" -desc "To be review if problem of stability of value (read status)" -from [get_pins {HBM*/HBM_data_i1/*Readout_HBM_for_ETH*/BIFI_in_backpressure*[*]/C}] -to [get_pins {HBM*/HBM_data_i1/*Readout_HBM_for_ETH*/usr_data_rd_reg*[*]/D}] -timestamp "Thu Oct 21 14:53:12 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "To be review if problem of stability of value (read status)" -to [get_pins {HBM*/HBM_data_i1/usr_data_rd_reg*[*]/D}] -timestamp "Thu Oct 21 14:53:12 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value are stable when readout" -to [get_pins {HBM*/HBM_data_i1/*HBM_STREAM_i1/Control_data_out*[*]/D}] -timestamp "Thu Oct 21 14:53:12 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value are stable when readout" -to [get_pins {HBM*/HBM_data_i1/*/TCP_logic*/Register_setting*/Ctrl_out_rg*[*]/D}] -timestamp "Thu Oct 21 14:53:12 GMT 2021"
create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Value are stable when readout" -to [get_pins {HBM*/HBM_data_i1/*/TCP_logic*/stat*/out_dt*[*]/D}] -timestamp "Thu Oct 21 14:53:12 GMT 2021"



create_waiver -type CDC -id {CDC-1} -user "dgigi" -desc "Input async" -from [get_ports QSFP*] -to [get_pins {data_in_rg_reg[*]/D}] -timestamp "Thu Oct 21 14:53:12 GMT 2021"
