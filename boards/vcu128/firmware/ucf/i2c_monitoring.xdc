
#set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ monitor_itf*/i0/reg_1st_stage*/C}] -to [get_pins -hierarchical -filter {NAME =~ monitor_itf*/i0/reg_2nd_stage_async*/D}] 15.000
#set_max_delay -datapath_only -from [get_pins -hierarchical -filter {NAME =~ monitor_itf*/i3/reg_1st_stage*/C}] -to [get_pins -hierarchical -filter {NAME =~ monitor_itf*/i3/reg_2nd_stage_async*/D}] 30.000

set_false_path -from [get_pins monitor_itf_i2c*/Access_done*/C] -to [get_pins {monitor_itf_i2c*/dto*[*]/D}]
set_false_path -from [get_pins monitor_itf_i2c*/error*/C] -to [get_pins {monitor_itf_i2c*/dto*[*]/D}]
set_false_path -from [get_pins {monitor_itf_i2c*/rd_sh*[*]/C}] -to [get_pins {monitor_itf_i2c*/dto*[*]/D}]

set_false_path -from [get_pins {monitor_itf_i2c*/add_register*[*]*/C}]
set_false_path -from [get_pins monitor_itf_i2c*/read_request*/C]
set_false_path -from [get_pins {monitor_itf_i2c*/nb_reg_offset*[*]*/C}]
set_false_path -from [get_pins {monitor_itf_i2c*/nb_data*[*]*/C}]
set_false_path -from [get_pins {monitor_itf_i2c*/reg_offset*[*]*/C}]
set_false_path -from [get_pins monitor_itf_i2c*/start_stop_btw*/C]
set_false_path -from [get_pins {monitor_itf_i2c*/data_register*[*]*/C}]

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/delay_access_done_1_i2c_clock_start*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/delay_access_done_1_i2c_clock_start*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/delay_access_done_1_i2c_clock*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/delay_access_done_1_i2c_clock*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/resync_ena*/*reg_2nd*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/resync_ena*/*reg_2nd*/D] 62
set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/resync_ena*/*reg_o*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/resync_ena*/*reg_o*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/tog*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/tog*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/start_done*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/start_done*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/stop_done*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/stop_done*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/*I2C_sequence*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/*I2C_sequence*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/*access_SM*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/*access_SM*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/error*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/error*/D] 62
set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/stop_access*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/stop_access*/D] 62
set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/read_access*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/read_access*/D] 62
set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/first_set_offset*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/first_set_offset*/D] 62
set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/nb_data_cnt*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/nb_data_cnt*/D] 62
set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/nb_reg_offset_cnt*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/nb_reg_offset_cnt*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/bit_counter*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/bit_counter*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/remove_stop_i2c*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/remove_stop_i2c*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/nack_acc*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/nack_acc*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/sh*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/sh*/D] 62

set_multicycle_path -setup -to [get_pins monitor_itf_i2c*/SDA_reg*/D] 63
set_multicycle_path -hold -to [get_pins monitor_itf_i2c*/SDA_reg*/D] 62