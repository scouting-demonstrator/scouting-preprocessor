--
-- Top-level entity for KCU1500 dev kit
--
-- D. R. May 2018
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.algo_decl.all;
use work.datatypes.all;
use std.textio.all;
use work.test_file_io.all;

use work.scouting_register_constants.all;

library xpm;
use xpm.vcomponents.all;

entity top is
    port (
        -- MGT ref clock
        mgtrefclk0_x0y3_p : in std_logic;
        mgtrefclk0_x0y3_n : in std_logic;

        -- Serial data ports for transceiver channel 0
        ch0_gthrxn_in  : in std_logic;
        ch0_gthrxp_in  : in std_logic;
        ch0_gthtxn_out : out std_logic;
        ch0_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 1
        ch1_gthrxn_in  : in std_logic;
        ch1_gthrxp_in  : in std_logic;
        ch1_gthtxn_out : out std_logic;
        ch1_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 2
        ch2_gthrxn_in  : in std_logic;
        ch2_gthrxp_in  : in std_logic;
        ch2_gthtxn_out : out std_logic;
        ch2_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 3
        ch3_gthrxn_in  : in std_logic;
        ch3_gthrxp_in  : in std_logic;
        ch3_gthtxn_out : out std_logic;
        ch3_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 4
        ch4_gthrxn_in  : in std_logic;
        ch4_gthrxp_in  : in std_logic;
        ch4_gthtxn_out : out std_logic;
        ch4_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 5
        ch5_gthrxn_in  : in std_logic;
        ch5_gthrxp_in  : in std_logic;
        ch5_gthtxn_out : out std_logic;
        ch5_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 6
        ch6_gthrxn_in  : in std_logic;
        ch6_gthrxp_in  : in std_logic;
        ch6_gthtxn_out : out std_logic;
        ch6_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 7
        ch7_gthrxn_in  : in std_logic;
        ch7_gthrxp_in  : in std_logic;
        ch7_gthtxn_out : out std_logic;
        ch7_gthtxp_out : out std_logic;

        -- User-provided ports for reset helper block(s)
        sysclk300_p : in std_logic;
        sysclk300_n : in std_logic;
        --        hb_gtwiz_reset_all_in : in std_logic;

        -- Clock for PCIe
        sysclk_in_p : in std_logic;
        sysclk_in_n : in std_logic;
        sys_rst_n   : in std_logic;

        pci_exp_rxp : in std_logic_vector(7 downto 0);
        pci_exp_rxn : in std_logic_vector(7 downto 0);
        pci_exp_txp : out std_logic_vector(7 downto 0);
        pci_exp_txn : out std_logic_vector(7 downto 0);

        sda                 : inout std_logic;
        scl                 : inout std_logic;
        I2C_MAIN_RESET_B_LS : out std_logic
    );
end top;

architecture Behavioral of top is

    signal mgtrefclk0_x0y3_int : std_logic;
    signal sysclk300_in        : std_logic;

    signal d_gap_cleaner, d_align, d_align_sync                       : ldata(3 + (N_REGION - 1) * 4 downto 0);
    signal d_align_sync_del1, d_align_sync_del2, d_align_sync_delayed : ldata(3 + (N_REGION - 1) * 4 downto 0);

    signal d_algo, d_bx, d_trailer, d_zs, d_calib, q_zs, q_bril, q_bril_buf, d_algo_intermediate, d_algo_reg, d_bx_reg : adata(4 * N_REGION - 1 downto 0);
    signal d_package, d_package_delayed : adata(4 * N_REGION - 1 downto 0);
    signal d_ctrl_algo, d_ctrl_bx, d_ctrl_bx_reg, d_ctrl_calib, d_ctrl_trailer, d_ctrl_zs, q_ctrl_zs, d_ctrl_algo_reg, q_ctrl_bril, q_ctrl_bril_buf : acontrol;

    signal d_ctrl_package, d_ctrl_package_delayed : acontrol;

    signal rst_global, rst_scone, rst_merged : std_logic;
    signal clk_link, rst_link, rst_link_sync : std_logic;
    signal rst_packager                      : std_logic;
    signal rst_aligner, rst_aligner_sync     : std_logic;

    signal clk_axi, axi_rstn : std_logic;
    signal pcie_lnk_up       : std_logic := '0'; -- TODO: Attach to VIO for debugging?
    -- AXI Lite
    signal m_axil_awaddr, m_axil_araddr   : std_logic_vector(31 downto 0);
    signal m_axil_awprot, m_axil_arprot   : std_logic_vector(2 downto 0);
    signal m_axil_awvalid, m_axil_awready : std_logic; -- Write address ready/valid
    signal m_axil_arvalid, m_axil_arready : std_logic;
    signal m_axil_wdata, m_axil_rdata     : std_logic_vector(31 downto 0);
    signal m_axil_wstrb                   : std_logic_vector(3 downto 0);
    signal m_axil_wvalid, m_axil_wready   : std_logic; -- Write data valid/ready
    signal m_axil_rvalid, m_axil_rready   : std_logic;
    signal m_axil_bvalid, m_axil_bready   : std_logic; -- Write response valid/ready
    signal m_axil_rresp, m_axil_bresp     : std_logic_vector(1 downto 0);
    -- AXI stream
    signal m_axis_c2h_tdata_0, m_axis_c2h_tdata_0_delayed   : std_logic_vector(255 downto 0);
    signal m_axis_c2h_tlast_0, m_axis_c2h_tlast_0_delayed   : std_logic;
    signal m_axis_c2h_tvalid_0, m_axis_c2h_tvalid_0_delayed : std_logic;
    signal m_axis_c2h_tready_0, m_axis_c2h_tready_0_delayed : std_logic;
    signal m_axis_h2c_tready_0                              : std_logic;
    signal m_axis_c2h_tkeep_0, m_axis_c2h_tkeep_0_delayed   : std_logic_vector(31 downto 0);
    signal packet_length                                    : std_logic_vector(15 downto 0) := x"0020";
    signal fifo_cnt                                         : std_logic_vector(14 downto 0) := (others => '0');
    signal fifo_read, fifo_write, fifo_empty                : std_logic                     := '0';
    type state_s is (idle, wait_s1, wait_s2, wait_s3, wait_s4, read);
    signal state : state_s := idle;

    signal validVec, doneVec : std_logic_vector(7 downto 0);

    signal init_done_int                                : std_logic_vector(0 downto 0);
    signal init_done_axi_sync                           : std_logic;
    signal init_retry_ctr, init_retry_ctr_sync          : std_logic_vector(3 downto 0);
    signal gtpowergood_sync, gtpowergood_axi_sync       : std_logic_vector(4 * N_REGION - 1 downto 0);
    signal txpmaresetdone_sync                          : std_logic_vector(4 * N_REGION - 1 downto 0);
    signal txpmaresetdone_axi_sync                      : std_logic;
    signal rxpmaresetdone_sync                          : std_logic_vector(4 * N_REGION - 1 downto 0);
    signal rxpmaresetdone_axi_sync                      : std_logic;
    signal gtwiz_reset_tx_done_sync                     : std_logic_vector(0 downto 0);
    signal gtwiz_reset_tx_done_axi_sync                 : std_logic;
    signal gtwiz_reset_rx_done_sync                     : std_logic_vector(0 downto 0);
    signal gtwiz_reset_rx_done_axi_sync                 : std_logic;
    signal hb0_gtwiz_reset_tx_pll_and_datapath_int      : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_pll_and_datapath_axi_sync : std_logic;
    signal hb0_gtwiz_reset_tx_datapath_int              : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_datapath_axi_sync         : std_logic;
    signal hb_gtwiz_reset_rx_datapath_int               : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_datapath_axi_sync          : std_logic;
    signal loopback                                     : std_logic_vector(3 * 4 * N_REGION - 1 downto 0);

    signal rx_data : std_logic_vector(8 * 32 - 1 downto 0);

    signal sRxbyteisaligned, sRxbyteisaligned_sync : std_logic_vector(4 * N_REGION - 1 downto 0);
    signal cdr_stable, cdr_stable_sync             : std_logic_vector(0 downto 0);

    signal clk_i2c, hb_gtwiz_reset_clk_freerun_in, hb_gtwiz_reset_clk_freerun_int, clk_fb : std_logic;

    signal str_rd, str_wr, rst_i2c                : std_logic;
    signal str_rd_sync, str_wr_sync, rst_i2c_sync : std_logic;
    signal data_rd, data_rd_axi_sync              : std_logic_vector(7 downto 0);
    signal sCommaDet                              : std_logic_vector(4 * N_REGION - 1 downto 0);

    signal lid, lid_sync : TAuxInfo(4 * N_REGION - 1 downto 0);
    signal crc, crc_sync : TAuxInfo(4 * N_REGION - 1 downto 0);

    signal do_calibration_suppression : std_logic;

    signal crc_error_counters      : TCounter32b(4 * N_REGION - 1 downto 0);
    signal crc_error_counters_sync : TAuxInfo(4 * N_REGION - 1 downto 0);

    signal stream_enable_mask, stream_enable_mask_sync : std_logic_vector(4 * N_REGION - 1 downto 0);

    signal axi_prog_full : std_logic;

    signal filler_orbit_counter, filler_orbit_counter_delayed                 : unsigned(63 downto 0);
    signal filler_dropped_orbit_counter, filler_dropped_orbit_counter_delayed : unsigned(63 downto 0);
    signal axi_backpressure_seen, axi_backpressure_seen_delayed               : std_logic;
    signal orbits_per_packet, orbits_per_packet_delayed                       : std_logic_vector(15 downto 0);
    signal orbit_length, orbit_length_delayed                                 : unsigned (63 downto 0);
    signal orbit_length_fat_orbit_check, orbit_length_fat_orbit_check_delayed : unsigned (63 downto 0);
    signal orbit_exceeds_size, orbit_exceeds_size_delayed                     : std_logic := '1';
    signal autorealign_counter, autorealign_counter_delayed                   : unsigned (63 downto 0);

    signal waiting_orbit_end, waiting_orbit_end_sync : std_logic;

    signal disable_zs : std_logic;

    signal freq_input : std_logic_vector(31 downto 0);

    signal rxctrl0_int : std_logic_vector(63 downto 0);
    signal rxctrl1_int : std_logic_vector(63 downto 0);
    signal rxctrl2_int : std_logic_vector(31 downto 0);
    signal rxctrl3_int : std_logic_vector(31 downto 0);

    signal moni_reg, ctrl_reg, ctrl_reg_delayed : SRegister(2 ** moni_ctrl_register_address_width - 1 downto 0);

    signal trigger_bril_readout : std_logic := '0';

    -- For simulation
    constant t_period : time := 4 ns;
    constant t_halfperiod : time := t_period / 2;
    constant t_hold : time := t_period / 5;
    signal enable : std_logic_vector(7 downto 0);

    component axi_distfifo_256x16
        port (
            s_aclk        : in std_logic;
            s_aresetn     : in std_logic;
            s_axis_tvalid : in std_logic;
            s_axis_tready : out std_logic;
            s_axis_tdata  : in std_logic_vector(255 downto 0);
            s_axis_tkeep  : in std_logic_vector(31 downto 0);
            s_axis_tlast  : in std_logic;
            s_axis_tuser  : in std_logic_vector(  1 downto 0);

	    m_axis_tvalid : out std_logic;
            m_axis_tready : in std_logic;
            m_axis_tdata  : out std_logic_vector(255 downto 0);
            m_axis_tkeep  : out std_logic_vector(31 downto 0);
            m_axis_tlast  : out std_logic;
            m_axis_tuser  : out std_logic_vector(  1 downto 0)
        );
    end component;

    component ila_0
        port (
            clk    : in std_logic;
            probe0 : in std_logic_vector(255 downto 0);
            probe1 : in std_logic_vector(255 downto 0);
            probe2 : in std_logic_vector(3 downto 0);
            probe3 : in std_logic_vector(15 downto 0);
            probe4 : in std_logic_vector(0 downto 0);
            probe5 : in std_logic_vector(0 downto 0);
            probe6 : in std_logic_vector(0 downto 0);
            probe7 : in std_logic_vector(31 downto 0);
            probe8 : in std_logic_vector(31 downto 0)
        );
    end component;

begin

    rst_merged <= rst_scone or rst_global;

    global_reset : entity work.reset
        port map(
            clk_free   => hb_gtwiz_reset_clk_freerun_int,
            clk_i2c    => clk_i2c,
            clk_link   => clk_link,
            clk_axi    => clk_axi,
            rst_global => rst_merged,
            rst_pll      => hb0_gtwiz_reset_tx_pll_and_datapath_int(0),
            rst_tx       => hb0_gtwiz_reset_tx_datapath_int(0),
            rst_rx       => hb_gtwiz_reset_rx_datapath_int(0),
            rst_algo     => rst_link,
            rst_packager => rst_packager
        );

hw_gen : if SIM = false generate

    axi_register_interface : entity work.axi_register_interface_wrapper
        generic map(
            REG_DATA_WIDTH => 32,
            REG_ADDR_WIDTH => full_register_address_width
        )
        port map(
            -- register interface
            moni_reg => moni_reg,
            ctrl_reg => ctrl_reg,
            -- axi interface
            S_AXI_ACLK    => clk_axi,
            S_AXI_ARESETN => axi_rstn,
            S_AXI_ARADDR  => m_axil_araddr,
            S_AXI_ARPROT  => m_axil_arprot,
            S_AXI_ARREADY => m_axil_arready,
            S_AXI_ARVALID => m_axil_arvalid,
            S_AXI_AWADDR  => m_axil_awaddr,
            S_AXI_AWPROT  => m_axil_awprot,
            S_AXI_AWREADY => m_axil_awready,
            S_AXI_AWVALID => m_axil_awvalid,
            S_AXI_BREADY  => m_axil_bready,
            S_AXI_BRESP   => m_axil_bresp,
            S_AXI_BVALID  => m_axil_bvalid,
            S_AXI_RDATA   => m_axil_rdata,
            S_AXI_RREADY  => m_axil_rready,
            S_AXI_RRESP   => m_axil_rresp,
            S_AXI_RVALID  => m_axil_rvalid,
            S_AXI_WDATA   => m_axil_wdata,
            S_AXI_WREADY  => m_axil_wready,
            S_AXI_WSTRB   => m_axil_wstrb,
            S_AXI_WVALID  => m_axil_wvalid
        );

    -- Set MGT clock via I2C
    ---------------------------------------------------------------------------
    i2c_i : entity work.i2c_driver
        port map(
            clk     => clk_i2c, -- 50 MHz from PLL below.
            reset   => rst_i2c_sync,
            str_wr  => str_wr_sync,
            str_rd  => str_rd_sync,
            data_rd => data_rd,
            sda     => sda,
            scl     => scl
        );

    I2C_MAIN_RESET_B_LS <= '1'; -- TODO: Set via VIO?
    ---------------------------------------------------------------------------
    -- Differential reference clock buffer for freerunning clock
    IBUFDS_FREERUN_INST : IBUFDS
    port map(
        I  => sysclk300_p,
        IB => sysclk300_n,
        O  => sysclk300_in
    );

    PLLE3_ADV_INST : PLLE3_ADV
    generic map(
        COMPENSATION       => "AUTO",
        STARTUP_WAIT       => "FALSE",
        DIVCLK_DIVIDE      => 1,
        CLKFBOUT_MULT      => 2,
        CLKFBOUT_PHASE     => 0.000,
        CLKOUT0_DIVIDE     => 4,
        CLKOUT0_PHASE      => 0.000,
        CLKOUT0_DUTY_CYCLE => 0.500,
        CLKOUT1_DIVIDE     => 12,
        CLKOUT1_PHASE      => 0.000,
        CLKOUT1_DUTY_CYCLE => 0.500,
        CLKIN_PERIOD       => 3.333)
    port map(
        CLKFBOUT    => clk_fb,
        CLKOUT0     => hb_gtwiz_reset_clk_freerun_in,
        CLKOUT0B    => open,
        CLKOUT1     => clk_i2c,
        CLKOUT1B    => open,
        CLKFBIN     => clk_fb,
        CLKIN       => sysclk300_in,
        DADDR => (others => '0'),
        DCLK        => '0',
        DEN         => '0',
        DI => (others => '0'),
        DO          => open,
        DRDY        => open,
        DWE         => '0',
        CLKOUTPHYEN => '0',
        PWRDWN      => '0',
        RST         => '0'
    );


 -- Differential reference clock buffer for mgtrefclk0
    IBUFDS_GTE3_MGTREFCLK0_X0Y3_INST : IBUFDS_GTE3
    generic map(
        REFCLK_EN_TX_PATH  => '0',
        REFCLK_HROW_CK_SEL => "00",
        REFCLK_ICNTL_RX    => "00")
    port map(
        I     => mgtrefclk0_x0y3_p,
        IB    => mgtrefclk0_x0y3_n,
        CEB   => '0',
        O     => mgtrefclk0_x0y3_int,
        ODIV2 => open
    );

    sync_gtpowergood_to_clk_axi : entity work.cdc_array
        generic map(bit_width => gtpowergood_sync'length)
        port map(clk_src => hb_gtwiz_reset_clk_freerun_int, clk_dest => clk_axi, d => gtpowergood_sync, q => gtpowergood_axi_sync);
    sync_reset_tx_done_to_clk_axi : entity work.cdc_single
        port map(clk_src => hb_gtwiz_reset_clk_freerun_int, clk_dest => clk_axi, d => gtwiz_reset_tx_done_sync(0), q => gtwiz_reset_tx_done_axi_sync);
    sync_reset_rx_done_to_clk_axi : entity work.cdc_single
        port map(clk_src => hb_gtwiz_reset_clk_freerun_int, clk_dest => clk_axi, d => gtwiz_reset_rx_done_sync(0), q => gtwiz_reset_rx_done_axi_sync);
    sync_data_rd_to_clk_axi : entity work.cdc_array
        generic map(bit_width => data_rd'length)
        port map(clk_src => clk_i2c, clk_dest => clk_axi, d => data_rd, q => data_rd_axi_sync);
    sync_init_done_to_clk_axi : entity work.cdc_single
        port map(clk_src => hb_gtwiz_reset_clk_freerun_int, clk_dest => clk_axi, d => init_done_int(0), q => init_done_axi_sync);
    sync_reset_tx_pll_to_clk_axi : entity work.cdc_single
        port map(clk_src => hb_gtwiz_reset_clk_freerun_int, clk_dest => clk_axi, d => hb0_gtwiz_reset_tx_pll_and_datapath_int(0), q => hb0_gtwiz_reset_tx_pll_and_datapath_axi_sync);
    sync_reset_tx_datapath_to_clk_axi : entity work.cdc_single
        port map(clk_src => hb_gtwiz_reset_clk_freerun_int, clk_dest => clk_axi, d => hb0_gtwiz_reset_tx_datapath_int(0), q => hb0_gtwiz_reset_tx_datapath_axi_sync);
    sync_reset_rx_datapath_to_clk_axi : entity work.cdc_single
        port map(clk_src => hb_gtwiz_reset_clk_freerun_int, clk_dest => clk_axi, d => hb_gtwiz_reset_rx_datapath_int(0), q => hb_gtwiz_reset_rx_datapath_axi_sync);
    sync_txpmaresetdone_to_clk_axi : entity work.cdc_single
        port map(clk_src => hb_gtwiz_reset_clk_freerun_int, clk_dest => clk_axi, d => txpmaresetdone_sync(0), q => txpmaresetdone_axi_sync);
    sync_rxpmaresetdone_to_clk_axi : entity work.cdc_single
        port map(clk_src => hb_gtwiz_reset_clk_freerun_int, clk_dest => clk_axi, d => rxpmaresetdone_sync(0), q => rxpmaresetdone_axi_sync);
    sync_crc_error_counters_to_clk_axi : for chan in crc_error_counters'range generate
        sync_crc_err_counter_to_clk_axi : entity work.cdc_array
            generic map(bit_width => crc_error_counters(chan)'length)
            port map(clk_src => clk_link, clk_dest => clk_axi, d => std_logic_vector(crc_error_counters(chan)), q => crc_error_counters_sync(chan));
    end generate;

    stream_enable_mask <= ctrl_reg(link_enable_address)(link_enable_width + link_enable_offset - 1 downto link_enable_offset);
    rst_scone          <= ctrl_reg(reset_board_address)(reset_board_offset);
    orbits_per_packet  <= ctrl_reg(orbits_per_packet_address)(orbits_per_packet_width + orbits_per_packet_offset - 1 downto orbits_per_packet_offset);
    rst_i2c            <= ctrl_reg(rst_i2c_address)(rst_i2c_offset);
    str_rd             <= ctrl_reg(str_rd_address)(str_rd_offset);
    str_wr             <= ctrl_reg(str_wr_address)(str_wr_offset);
    disable_zs         <= ctrl_reg(disable_zs_address)(disable_zs_offset);

    moni_reg(cdr_stable_info_address)(cdr_stable_info_offset)                                                                                                               <= cdr_stable_sync(0);
    moni_reg(rst_link_address)(rst_link_offset)                                                                                                                             <= rst_link_sync;
    moni_reg(rst_aligner_address)(rst_aligner_offset)                                                                                                                       <= rst_aligner_sync;
    moni_reg(init_retry_ctr_address)(init_retry_ctr_offset)                                                                                                                 <= init_retry_ctr_sync(0);
    moni_reg(rx_byte_is_aligned_info_address)(rx_byte_is_aligned_info_offset + rx_byte_is_aligned_info_width - 1 downto rx_byte_is_aligned_info_offset)                     <= sRxbyteisaligned_sync;
    moni_reg(gt_power_good_info_address)(gt_power_good_info_offset + gt_power_good_info_width - 1 downto gt_power_good_info_offset)                                         <= gtpowergood_axi_sync;
    moni_reg(gt_tx_reset_done_info_address)(gt_tx_reset_done_info_offset)                                                                                                   <= gtwiz_reset_tx_done_axi_sync;
    moni_reg(gt_rx_reset_done_info_address)(gt_rx_reset_done_info_offset)                                                                                                   <= gtwiz_reset_rx_done_axi_sync;
    moni_reg(tx_pma_reset_done_info_address)(tx_pma_reset_done_info_offset)                                                                                                 <= txpmaresetdone_axi_sync; -- TODO: Should put all the bits into AXI reg?
    moni_reg(rx_pma_reset_done_info_address)(rx_pma_reset_done_info_offset)                                                                                                 <= rxpmaresetdone_axi_sync; -- TODO: Should put all the bits into AXI reg?
    moni_reg(gt_reset_tx_pll_datapath_info_address)(gt_reset_tx_pll_datapath_info_offset)                                                                                   <= hb0_gtwiz_reset_tx_pll_and_datapath_axi_sync;
    moni_reg(gt_reset_tx_datapath_info_address)(gt_reset_tx_datapath_info_offset)                                                                                           <= hb0_gtwiz_reset_tx_datapath_axi_sync;
    moni_reg(gt_reset_rx_datapath_info_address)(gt_reset_rx_datapath_info_offset)                                                                                           <= hb_gtwiz_reset_rx_datapath_axi_sync;
    moni_reg(init_done_info_address)(init_done_info_offset)                                                                                                                 <= init_done_axi_sync;
    moni_reg(frequency_input_hertz_address)(frequency_input_hertz_offset + frequency_input_hertz_width - 1 downto frequency_input_hertz_offset)                             <= freq_input;
    moni_reg(filler_orbits_seen_orbits_address)(filler_orbits_seen_orbits_offset + filler_orbits_seen_orbits_width - 1 downto filler_orbits_seen_orbits_offset)             <= std_logic_vector(filler_orbit_counter(31 downto 0));
    moni_reg(filler_orbits_dropped_orbits_address)(filler_orbits_dropped_orbits_offset + filler_orbits_dropped_orbits_width - 1 downto filler_orbits_dropped_orbits_offset) <= std_logic_vector(filler_dropped_orbit_counter(31 downto 0));
    moni_reg(axi_backpressure_seen_info_address)(axi_backpressure_seen_info_offset)                                                                                         <= axi_backpressure_seen_delayed;
    moni_reg(orbit_exceeds_size_info_address)(orbit_exceeds_size_info_offset)                                                                                               <= orbit_exceeds_size_delayed;
    moni_reg(waiting_for_orbit_end_info_address)(waiting_for_orbit_end_info_offset)                                                                                         <= waiting_orbit_end_sync;
    moni_reg(i2c_value_read_info_address)(i2c_value_read_info_offset + i2c_value_read_info_width - 1 downto i2c_value_read_info_offset)                                     <= data_rd_axi_sync;
    moni_reg(autorealigns_total_address)(autorealigns_total_offset + autorealigns_total_width - 1 downto autorealigns_total_offset)                                         <= std_logic_vector(autorealign_counter(31 downto 0));
    moni_reg(orbit_length_bxs_address)(orbit_length_bxs_offset + orbit_length_bxs_width - 1 downto orbit_length_bxs_offset)                                                 <= std_logic_vector(orbit_length_delayed(31 downto 0));
    moni_reg(algo_version_address)(algo_version_offset + algo_version_width - 1 downto algo_version_offset)                                                                 <= ALGO_REV;
    moni_reg(fw_type_address)(fw_type_offset + fw_type_width - 1 downto fw_type_offset)                                                                                     <= FW_TYPE;
    moni_reg(crc_error_counter_0_address)(crc_error_counter_0_offset + crc_error_counter_0_width - 1 downto crc_error_counter_0_offset)                                     <= crc_error_counters_sync(0);
    moni_reg(crc_error_counter_1_address)(crc_error_counter_1_offset + crc_error_counter_1_width - 1 downto crc_error_counter_1_offset)                                     <= crc_error_counters_sync(1);
    moni_reg(crc_error_counter_2_address)(crc_error_counter_2_offset + crc_error_counter_2_width - 1 downto crc_error_counter_2_offset)                                     <= crc_error_counters_sync(2);
    moni_reg(crc_error_counter_3_address)(crc_error_counter_3_offset + crc_error_counter_3_width - 1 downto crc_error_counter_3_offset)                                     <= crc_error_counters_sync(3);
    moni_reg(crc_error_counter_4_address)(crc_error_counter_4_offset + crc_error_counter_4_width - 1 downto crc_error_counter_4_offset)                                     <= crc_error_counters_sync(4);
    moni_reg(crc_error_counter_5_address)(crc_error_counter_5_offset + crc_error_counter_5_width - 1 downto crc_error_counter_5_offset)                                     <= crc_error_counters_sync(5);
    moni_reg(crc_error_counter_6_address)(crc_error_counter_6_offset + crc_error_counter_6_width - 1 downto crc_error_counter_6_offset)                                     <= crc_error_counters_sync(6);
    moni_reg(crc_error_counter_7_address)(crc_error_counter_7_offset + crc_error_counter_7_width - 1 downto crc_error_counter_7_offset)                                     <= crc_error_counters_sync(7);

    axiToi2c : entity work.debug_signals_axiClkToI2cClk
        port map(
            clk_axi      => clk_axi,
            clk_i2c      => clk_i2c,
            rst_i2c_in   => rst_i2c,
            str_rd_in    => str_rd,
            str_wr_in    => str_wr,
            rst_i2c_sync => rst_i2c_sync,
            str_rd_sync  => str_rd_sync,
            str_wr_sync  => str_wr_sync
        );

    axiToLink : entity work.debug_signals_axiClkToLinkClk
        port map(
            clk_axi                 => clk_axi,
            clk_link                => clk_link,
            stream_enable_mask_in   => stream_enable_mask,
            disable_zs_in           => disable_zs,
            stream_enable_mask_sync => stream_enable_mask_sync,
            disable_zs_sync         => open
        );

    ila_linkToAxi : entity work.debug_signals_linkClkToAxiClk
        generic map(
            N_REGION => N_REGION
        )
        port map(
            clk_link                => clk_link,
            clk_axi                 => clk_axi,
            sRxbyteisaligned_in     => sRxbyteisaligned,
            waiting_orbit_end_in    => waiting_orbit_end,
            rst_algo_in             => rst_link,
            rst_aligner_in          => rst_aligner,
            d_align_in              => d_align,
            lid_in                  => lid,
            crc_in                  => crc,
            sRxbyteisaligned_sync   => sRxbyteisaligned_sync,
            waiting_orbit_end_sync  => waiting_orbit_end_sync,
            rst_algo_sync           => rst_link_sync,
            rst_aligner_sync        => rst_aligner_sync,
            d_align_sync            => d_align_sync,
            lid_sync                => lid_sync,
            crc_sync                => crc_sync
        );

    ila_freeToAxi : entity work.debug_signals_freeClkToAxiClk
        generic map(
            N_REGION => N_REGION
        )
        port map(
            clk_free            => hb_gtwiz_reset_clk_freerun_int,
            clk_axi             => clk_axi,
            init_retry_ctr_in   => init_retry_ctr,
            cdr_stable_in       => cdr_stable(0),
            init_retry_ctr_sync => init_retry_ctr_sync,
            cdr_stable_sync     => cdr_stable_sync(0)
        );

    reg_for_ila : process (clk_axi)
    begin
        if rising_edge(clk_axi) then
            d_align_sync_del1    <= d_align_sync;
            d_align_sync_del2    <= d_align_sync_del1;
            d_align_sync_delayed <= d_align_sync_del2;
        end if;
    end process;

    link_ila : ila_0
    port map(
        clk                    => clk_axi,
        probe0(31 downto 0)    => crc_sync(7),
        probe0(63 downto 32)   => d_align_sync_delayed(7).data,
        probe0(95 downto 64)   => (others => '0'),
        probe0(127 downto 96)  => (others => '0'),
        probe0(159 downto 128) => (others => '0'),
        probe0(191 downto 160) => (others => '0'),
        probe0(223 downto 192) => (others => '0'),
        probe0(255 downto 224) => (others => '0'),
        probe1(31 downto 0)    => d_zs(0),
        probe1(63 downto 32)   => d_zs(1),
        probe1(95 downto 64)   => d_zs(2),
        probe1(127 downto 96)  => d_zs(3),
        probe1(159 downto 128) => d_zs(4),
        probe1(191 downto 160) => d_zs(5),
        probe1(223 downto 192) => d_zs(6),
        probe1(255 downto 224) => d_zs(7),
        probe2(0)              => d_ctrl_zs.valid,
        probe2(1)              => d_ctrl_zs.strobe,
        probe2(2)              => d_ctrl_zs.bx_start,
        probe2(3)              => d_ctrl_zs.last,
        probe3(0)              => d_align_sync_delayed(0).valid,
        probe3(1)              => d_align_sync_delayed(1).valid,
        probe3(2)              => d_align_sync_delayed(2).valid,
        probe3(3)              => d_align_sync_delayed(3).valid,
        probe3(4)              => d_align_sync_delayed(4).valid,
        probe3(5)              => d_align_sync_delayed(5).valid,
        probe3(6)              => d_align_sync_delayed(6).valid,
        probe3(7)              => d_align_sync_delayed(7).valid,
        probe3(8)              => d_align_sync_delayed(0).strobe,
        probe3(9)              => d_align_sync_delayed(1).strobe,
        probe3(10)             => d_align_sync_delayed(2).strobe,
        probe3(11)             => d_align_sync_delayed(3).strobe,
        probe3(12)             => d_align_sync_delayed(4).strobe,
        probe3(13)             => d_align_sync_delayed(5).strobe,
        probe3(14)             => d_align_sync_delayed(6).strobe,
        probe3(15)             => d_align_sync_delayed(7).strobe,
        probe4(0)              => axi_backpressure_seen_delayed,
        probe5(0)              => orbit_exceeds_size_delayed,
        probe6(0)              => rst_packager,
        probe7                 => (others => '0'),
        probe8                 => (others => '0')
    );

    inputs : entity work.inputs
        generic map(
            N_MGTREFCLKS => 1
        )
        port map(
            axi_clk    => clk_axi,
            mgtrefclks(0) => mgtrefclk0_x0y3_int,

            gtrxn_in(0)  => ch0_gthrxn_in,
            gtrxn_in(1)  => ch1_gthrxn_in,
            gtrxn_in(2)  => ch2_gthrxn_in,
            gtrxn_in(3)  => ch3_gthrxn_in,
            gtrxn_in(4)  => ch4_gthrxn_in,
            gtrxn_in(5)  => ch5_gthrxn_in,
            gtrxn_in(6)  => ch6_gthrxn_in,
            gtrxn_in(7)  => ch7_gthrxn_in,

            gtrxp_in(0)  => ch0_gthrxp_in,
            gtrxp_in(1)  => ch1_gthrxp_in,
            gtrxp_in(2)  => ch2_gthrxp_in,
            gtrxp_in(3)  => ch3_gthrxp_in,
            gtrxp_in(4)  => ch4_gthrxp_in,
            gtrxp_in(5)  => ch5_gthrxp_in,
            gtrxp_in(6)  => ch6_gthrxp_in,
            gtrxp_in(7)  => ch7_gthrxp_in,

            gttxn_out(0) => ch0_gthtxn_out,
            gttxn_out(1) => ch1_gthtxn_out,
            gttxn_out(2) => ch2_gthtxn_out,
            gttxn_out(3) => ch3_gthtxn_out,
            gttxn_out(4) => ch4_gthtxn_out,
            gttxn_out(5) => ch5_gthtxn_out,
            gttxn_out(6) => ch6_gthtxn_out,
            gttxn_out(7) => ch7_gthtxn_out,

            gttxp_out(0) => ch0_gthtxp_out,
            gttxp_out(1) => ch1_gthtxp_out,
            gttxp_out(2) => ch2_gthtxp_out,
            gttxp_out(3) => ch3_gthtxp_out,
            gttxp_out(4) => ch4_gthtxp_out,
            gttxp_out(5) => ch5_gthtxp_out,
            gttxp_out(6) => ch6_gthtxp_out,
            gttxp_out(7) => ch7_gthtxp_out,


            clk_freerun     => hb_gtwiz_reset_clk_freerun_in,
            clk_freerun_buf => hb_gtwiz_reset_clk_freerun_int, -- Clock after being globally buffered
            init_done_int                           => init_done_int,
            init_retry_ctr_int                      => init_retry_ctr,
            gtpowergood_sync                        => gtpowergood_sync,
            txpmaresetdone_sync                     => txpmaresetdone_sync,
            rxpmaresetdone_sync                     => rxpmaresetdone_sync,
            gtwiz_reset_tx_done_sync                => gtwiz_reset_tx_done_sync,
            gtwiz_reset_rx_done_sync                => gtwiz_reset_rx_done_sync,
            hb_gtwiz_reset_all_int                  => "0",
            hb0_gtwiz_reset_tx_pll_and_datapath_int => hb0_gtwiz_reset_tx_pll_and_datapath_int,
            hb0_gtwiz_reset_tx_datapath_int         => hb0_gtwiz_reset_tx_datapath_int,
            hb_gtwiz_reset_rx_pll_and_datapath_int  => "0",
            hb_gtwiz_reset_rx_datapath_int          => hb_gtwiz_reset_rx_datapath_int,
            link_down_latched_reset_int             => "0",
            loopback                                => loopback,

            oLinkData => rx_data,

            oRxbyteisaligned => sRxbyteisaligned,
            cdr_stable       => cdr_stable,
            rxctrl0_out      => rxctrl0_int,
            rxctrl1_out      => rxctrl1_int,
            rxctrl2_out      => rxctrl2_int,
            rxctrl3_out      => rxctrl3_int,

            clk           => clk_link,
            q             => d_gap_cleaner,
            oCommaDet     => sCommaDet
            );

    frequ_meas : entity work.freq_meas
        port map(
            clk      => clk_axi,
            rst      => rst_packager,
            clk_meas => clk_link,
            freq     => freq_input
        );

    gap_cleaner : entity work.comma_gap_cleaner
        port map(
            clk => clk_link,
            rst => rst_link,
            d   => d_gap_cleaner,
            q   => d_align,
            lid => lid,
            crc => crc
        );
end generate;

    check_crc : entity work.check_crc
        port map(
            clk           => clk_link,
            rst           => rst_link,
            d             => d_align,
            crc           => crc,
            crc_err_count => crc_error_counters
        );

    auto_realign_controller_1 : entity work.auto_realign_controller
        port map(
            axi_clk           => clk_axi,
            rst_in            => rst_packager,
            clk_aligner       => clk_link,
            rst_aligner_out   => rst_aligner,
            misalignment_seen => orbit_exceeds_size_delayed,
            autoreset_count   => autorealign_counter
        );

    align : entity work.bx_aware_aligner
        generic map(
            NSTREAMS => 4 * N_REGION)
        port map(
            clk_wr            => clk_link,
            rst               => rst_aligner,
            d                 => d_align,
            enable            => stream_enable_mask_sync,
            waiting_orbit_end => waiting_orbit_end,
            clk_rd            => clk_axi,
            q                 => d_zs,
            q_ctrl            => d_ctrl_zs
        );

  gen_bril : if BRIL = true generate
    scout_bril : entity work.scout_bril
        generic map (
            NSTREAMS =>4*N_REGION)
        port map(
           clk    => clk_axi,
           rst    => rst_packager,
           d      => d_zs,
           d_ctrl => d_ctrl_zs,
           q      => q_bril,
           q_ctrl => q_ctrl_bril,
           trigger_bril_readout_i => trigger_bril_readout
        );
  end generate;



    select_demux : if ZS_TYPE = "DEMUX" generate
        zs_demux : entity work.zs_demux
            generic map(
                NSTREAMS       => 4 * N_REGION,
                REG_ADDR_WIDTH => moni_ctrl_register_address_width
            )
            port map(
                clk      => clk_axi,
                rst      => rst_packager,
                ctrl_reg => ctrl_reg_delayed,
                d        => d_zs,
                d_ctrl   => d_ctrl_zs,
                q        => q_zs,
                q_ctrl   => q_ctrl_zs
            );

     end generate; -- select_demux

    select_ugmt : if ZS_TYPE = "UGMT" generate
        zs_ugmt : entity work.zs_ugmt
            generic map(
                NSTREAMS => 4 * N_REGION
            )
            port map(
                clk    => clk_axi,
                rst    => rst_packager,
                d      => d_zs,
                d_ctrl => d_ctrl_zs,
                q      => q_zs,
                q_ctrl => q_ctrl_zs
            );
    end generate; -- select_ugmt

    d_bx_reg <= q_zs when disable_zs = '0' else d_zs;
    d_ctrl_bx_reg <= q_ctrl_zs when disable_zs = '0' else d_ctrl_zs;

    q_bril_buf <= q_bril;
    q_ctrl_bril_buf <= q_ctrl_bril;
    d_bx <= d_bx_reg;
    d_ctrl_bx <= d_ctrl_bx_reg;

    gen_bx_counter : entity work.bx_counter
        generic map(
            NSTREAMS => 4 * N_REGION
        )
        port map(
            clk    => clk_axi,
            rst    => rst_packager,
            d      => d_bx,
            d_ctrl => d_ctrl_bx,
            q      => d_calib,
            q_ctrl => d_ctrl_calib
        );

    is_calo_board_for_calib_sup : if ZS_TYPE = "DEMUX" generate
        suppress_calibration_sequence : entity work.suppress_calibration_data
            generic map(
                NSTREAMS       => 4 * N_REGION,
                REG_ADDR_WIDTH => moni_ctrl_register_address_width
            )
            port map(
                clk      => clk_axi,
                rst      => rst_packager,
                ctrl_reg => ctrl_reg_delayed,
                d        => d_calib,
                d_ctrl   => d_ctrl_calib,
                q        => d_trailer,
                q_ctrl   => d_ctrl_trailer
            );
    end generate; -- is_calo_board_for_calib_sup

    not_calo_board_for_calib_sup : if ZS_TYPE /= "DEMUX" generate
        d_trailer      <= d_calib;
        d_ctrl_trailer <= d_ctrl_calib;
    end generate; -- not_calo_board_for_calib_sup

    gen_trailer : entity work.filled_bx_trailer_generator
        generic map(
            NSTREAMS => 4 * N_REGION
        )
        port map(
            clk    => clk_axi,
            rst    => rst_packager,
            d      => d_trailer,
            d_ctrl => d_ctrl_trailer,
            q      => d_algo_reg,
            q_ctrl => d_ctrl_algo_reg
        );

    d_algo <= d_algo_reg when BRIL = false else q_bril_buf;

    d_ctrl_algo <= d_ctrl_algo_reg when BRIL = false else q_ctrl_bril_buf;

    algo : entity work.algo
        port map(
            clk    => clk_axi,
            rst    => rst_packager,
            d      => d_algo,
            d_ctrl => d_ctrl_algo,
            q      => d_package,
            q_ctrl => d_ctrl_package
        );

    -- delay line to help with timing at slr crossing
    cross_to_slr1_delay_line : entity work.cross_to_slr1_delay_line
    generic map(
        delay_length => 12
    )
	port map(
        clk                       => clk_axi,
        orbits_per_packet         => orbits_per_packet,
        orbits_per_packet_out     => orbits_per_packet_delayed,
        d_package                 => d_package,
        d_package_out             => d_package_delayed,
        d_ctrl_package            => d_ctrl_package,
        d_ctrl_package_out        => d_ctrl_package_delayed,
        filler_orbit_counter      => filler_orbit_counter,
        filler_orbit_counter_out  => filler_orbit_counter_delayed,
        axi_backpressure_seen     => axi_backpressure_seen,
        axi_backpressure_seen_out => axi_backpressure_seen_delayed,
        orbit_length              => orbit_length,
        orbit_length_out          => orbit_length_delayed,
        orbit_exceeds_size        => orbit_exceeds_size,
        orbit_exceeds_size_out    => orbit_exceeds_size_delayed,
        autorealign_counter       => autorealign_counter,
        autorealign_counter_out   => autorealign_counter_delayed,
        ctrl_reg                  => ctrl_reg,
        ctrl_reg_out              => ctrl_reg_delayed,
        filler_dropped_orbit_counter       => filler_dropped_orbit_counter,
        filler_dropped_orbit_counter_out   => filler_dropped_orbit_counter_delayed
     );

   bril_fifo_filler : if BRIL = true generate

       bril_fifo_fill : process(clk_axi)
       begin
           for i in d_package'range loop
               m_axis_c2h_tdata_0_delayed(31 + i*32 downto i*32) <= d_package_delayed(i);
           end loop;
           m_axis_c2h_tvalid_0_delayed <= d_ctrl_package_delayed.valid;
           m_axis_c2h_tkeep_0_delayed  <= x"FFFFFFFF";
           m_axis_c2h_tlast_0_delayed   <= '0';
        end process;

   end generate;

   gen_fifo_filler : if BRIL /= true generate

      -- Small FIFO that buffers the data until ready makes it into SLR1
      distrRAM_slr0arrival : axi_distfifo_256x16
      port map(
          s_aclk        => clk_axi,
          s_aresetn     => axi_rstn,
          s_axis_tvalid => m_axis_c2h_tvalid_0,
          s_axis_tready => m_axis_c2h_tready_0_delayed,
          s_axis_tdata  => m_axis_c2h_tdata_0,
          s_axis_tkeep  => m_axis_c2h_tkeep_0,
          s_axis_tlast  => m_axis_c2h_tlast_0,
          s_axis_tuser  => (others => '0'), -- needed because fifo_generator_2 now has tuser ports

          m_axis_tvalid => m_axis_c2h_tvalid_0_delayed,
          m_axis_tready => m_axis_c2h_tready_0,
          m_axis_tdata  => m_axis_c2h_tdata_0_delayed,
          m_axis_tkeep  => m_axis_c2h_tkeep_0_delayed,
          m_axis_tlast  => m_axis_c2h_tlast_0_delayed,
          m_axis_tuser  => open
          );

      fifo_filler : entity work.fifo_filler
      generic map(
        NSTREAMS => 4 * N_REGION)
      port map(
          d_clk                  => clk_axi,
          rst                    => rst_packager,
          orbits_per_packet      => unsigned(orbits_per_packet_delayed),
          d                      => d_package_delayed,
          d_ctrl                 => d_ctrl_package_delayed,
          m_aclk                 => clk_axi,
          m_axis_tvalid          => m_axis_c2h_tvalid_0,
          m_axis_tready          => m_axis_c2h_tready_0_delayed,
          m_axis_tdata           => m_axis_c2h_tdata_0,
          m_axis_tkeep           => m_axis_c2h_tkeep_0,
          m_axis_tlast           => m_axis_c2h_tlast_0,
          dropped_orbit_counter  => filler_dropped_orbit_counter,
          orbit_counter          => filler_orbit_counter,
          axi_backpressure_seen  => axi_backpressure_seen,
          orbit_length           => orbit_length,
          orbit_exceeds_size     => orbit_exceeds_size,
          in_autorealign_counter => autorealign_counter_delayed,
          orbit_length_fat_orbit_check => orbit_length_fat_orbit_check
          );

   end generate;

  dma_gen : if SIM = false generate

    dma_engine_i : entity work.xilinx_dma_pcie_ep
        generic map(
            PL_LINK_CAP_MAX_LINK_WIDTH => 8,
            PL_SIM_FAST_LINK_TRAINING  => false,
            PL_LINK_CAP_MAX_LINK_SPEED => 4,
            C_DATA_WIDTH               => 256,
            EXT_PIPE_SIM               => false,
            C_ROOT_PORT                => false,
            C_DEVICE_NUMBER            => 0)
        port map(
            pci_exp_txp => pci_exp_txp,
            pci_exp_txn => pci_exp_txn,
            pci_exp_rxp => pci_exp_rxp,
            pci_exp_rxn => pci_exp_rxn,
            sys_clk_p   => sysclk_in_p,
            sys_clk_n   => sysclk_in_n,
            sys_rst_n   => sys_rst_n,
            -- AXI Master Write Address Channel
            m_axil_awaddr  => m_axil_awaddr,
            m_axil_awprot  => m_axil_awprot,
            m_axil_awvalid => m_axil_awvalid,
            m_axil_awready => m_axil_awready,
            -- AXI Master Write Data Channel
            m_axil_wdata  => m_axil_wdata,
            m_axil_wstrb  => m_axil_wstrb,
            m_axil_wvalid => m_axil_wvalid,
            m_axil_wready => m_axil_wready,
            -- AXI Master Write Response Channel
            m_axil_bvalid => m_axil_bvalid,
            m_axil_bready => m_axil_bready,
            -- AXI Master Read Address Channel
            m_axil_araddr  => m_axil_araddr,
            m_axil_arprot  => m_axil_arprot,
            m_axil_arvalid => m_axil_arvalid,
            m_axil_arready => m_axil_arready,
            -- AXI Master Read Data Channel
            m_axil_rdata  => m_axil_rdata,
            m_axil_rresp  => m_axil_rresp,
            m_axil_rvalid => m_axil_rvalid,
            m_axil_rready => m_axil_rready,
            m_axil_bresp  => m_axil_bresp,
            axi_clk       => clk_axi, -- This is the clock I want to run the algo with?
            axi_rstn      => axi_rstn,
            pcie_lnk_up   => pcie_lnk_up,
            --            m_axis_c2h_tdata_0  => test_tdata,
            m_axis_c2h_tdata_0 => m_axis_c2h_tdata_0_delayed,
            m_axis_h2c_tdata_0 => open,
            --            m_axis_c2h_tlast_0  => doneVec(0),
            m_axis_c2h_tlast_0 => m_axis_c2h_tlast_0_delayed,
            m_axis_h2c_tlast_0 => open,
            --            m_axis_c2h_tvalid_0 => validVec(0),
            m_axis_c2h_tvalid_0 => m_axis_c2h_tvalid_0_delayed,
            m_axis_h2c_tvalid_0 => open,
            m_axis_c2h_tready_0 => m_axis_c2h_tready_0,
            m_axis_h2c_tready_0 => m_axis_h2c_tready_0, -- Unconnected
            m_axis_c2h_tkeep_0  => m_axis_c2h_tkeep_0_delayed,
            m_axis_h2c_tkeep_0  => open);

  end generate;

  sim_gen : if SIM = true generate

              runtb : process
    variable i : integer := 0;
    variable j : integer := 0;
    variable frame : ldata( 7 downto 0);

    file F  : text open read_mode is "../../../../../../../../../scripts/common/testfile_5new2.ptrn";

    begin

    orbits_per_packet <= x"0001";
    disable_zs <= '0';
    stream_enable_mask <= x"FF";

    for j in 0 to 7 loop
      enable(j) <= '1';
      d_align(j).data <= (others => '0');
      d_align(j).valid <= '0';
      d_align(j).strobe <= '0';
      d_align(j).done <= '0';
    end loop;

    for i in 0 to 20000 loop
      clk_link <= '0';
      clk_axi <= '0';
      wait for t_halfperiod;
      clk_link <= '1';
      clk_axi <= '1';
      wait for t_hold;

      -- change signals
      if i>=5 and i <=30 then
        rst_global <= '1';

     else
        rst_global <= '0';
      end if;

      wait for (t_halfperiod - t_hold);

    end loop;

    while ( not endfile(F) ) loop
      clk_link <= '0';
      clk_axi <= '0';
      wait for t_halfperiod;
      clk_link <= '1';
      clk_axi <= '1';
      wait for t_hold;

      ReadFrame(f, 8, frame);
      d_align <= frame;

      wait for (t_halfperiod - t_hold);
    end loop;

   for i in 0 to 200000 loop
      clk_link <= '0';
      clk_axi <= '0';
      wait for t_halfperiod;
      clk_link <= '1';
      clk_axi <= '1';
      wait for t_hold;
      wait for (t_halfperiod - t_hold);

    end loop;

  end process runtb;

  end generate;

end Behavioral;
