# For interplay of VIO with PCIe Tandem config.
# Add the Tcl file in the tcl.pre* field of the Place Design section in the Implementation settings
# set master_cfg_site [get_sites -of_objects [get_slrs -filter {IS_MASTER==true}] -filter {NAME =~ CONFIG_SITE_*}]
# set bscan_cells [get_cells -hierarchical -filter { PRIMITIVE_TYPE =~ CONFIGURATION.BSCAN.* } ]
# set_property HD.TANDEM 1 $master_cfg_site
# add_cells_to_pblock [get_pblocks -of_objects [get_sites $master_cfg_site]] $master_cfg_site

set_property HD.TANDEM 1 [get_cells dbg_hub/inst/BSCANID.u_xsdbm_id/SWITCH_N_EXT_BSCAN.bscan_inst/SERIES7_BSCAN.bscan_inst]
add_cells_to_pblock [get_pblocks -of_object [get_sites CONFIG_SITE_X0Y0]] [get_cells dbg_hub/inst/BSCANID.u_xsdbm_id/SWITCH_N_EXT_BSCAN.bscan_inst/SERIES7_BSCAN.bscan_inst]

# For replicated logic:
set_property HD.TANDEM 1 [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_4_LOPT_REMAP}]
set_property HD.TANDEM_IP_PBLOCK Stage1_Config_IO [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_4_LOPT_REMAP}]
set_property HD.TANDEM 1 [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_3_LOPT_REMAP_1}]
set_property HD.TANDEM_IP_PBLOCK Stage1_Config_IO [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_3_LOPT_REMAP_1}]
set_property HD.TANDEM 1 [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_3_LOPT_REMAP}]
set_property HD.TANDEM_IP_PBLOCK Stage1_Config_IO [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_3_LOPT_REMAP}]
set_property HD.TANDEM 1 [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_2_LOPT_REMAP_1}]
set_property HD.TANDEM_IP_PBLOCK Stage1_Config_IO [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_2_LOPT_REMAP_1}]
set_property HD.TANDEM 1 [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_2_LOPT_REMAP}]
set_property HD.TANDEM_IP_PBLOCK Stage1_Config_IO [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_2_LOPT_REMAP}]
set_property HD.TANDEM 1 [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_1_LOPT_REMAP_1}]
set_property HD.TANDEM_IP_PBLOCK Stage1_Config_IO [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_1_LOPT_REMAP_1}]
set_property HD.TANDEM 1 [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_1_LOPT_REMAP}]
set_property HD.TANDEM_IP_PBLOCK Stage1_Config_IO [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_1_LOPT_REMAP}]
set_property HD.TANDEM 1 [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_0_LOPT_REMAP_1}]
set_property HD.TANDEM_IP_PBLOCK Stage1_Config_IO [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_0_LOPT_REMAP_1}]
set_property HD.TANDEM 1 [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_0_LOPT_REMAP}]
set_property HD.TANDEM_IP_PBLOCK Stage1_Config_IO [get_cells {dma_gen.dma_engine_i/xdma_0_i/inst/pcie3_ip_i/U0/i_0_LOPT_REMAP}]
