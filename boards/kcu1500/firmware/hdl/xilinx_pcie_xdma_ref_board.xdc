##-----------------------------------------------------------------------------
##
## (c) Copyright 2012-2012 Xilinx, Inc. All rights reserved.
##
## This file contains confidential and proprietary information
## of Xilinx, Inc. and is protected under U.S. and
## international copyright and other intellectual property
## laws.
##
## DISCLAIMER
## This disclaimer is not a license and does not grant any
## rights to the materials distributed herewith. Except as
## otherwise provided in a valid license issued to you by
## Xilinx, and to the maximum extent permitted by applicable
## law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
## WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
## AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
## BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
## INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
## (2) Xilinx shall not be liable (whether in contract or tort,
## including negligence, or under any other theory of
## liability) for any loss or damage of any kind or nature
## related to, arising under or in connection with these
## materials, including for any direct, or any indirect,
## special, incidental, or consequential loss or damage
## (including loss of data, profits, goodwill, or any type of
## loss or damage suffered as a result of any action brought
## by a third party) even if such damage or loss was
## reasonably foreseeable or Xilinx had been advised of the
## possibility of the same.
##
## CRITICAL APPLICATIONS
## Xilinx products are not designed or intended to be fail-
## safe, or for use in any application requiring fail-safe
## performance, such as life-support or safety devices or
## systems, Class III medical devices, nuclear facilities,
## applications related to the deployment of airbags, or any
## other applications that could lead to death, personal
## injury, or severe property or environmental damage
## (individually and collectively, "Critical
## Applications"). Customer assumes the sole risk and
## liability of any use of Xilinx products in Critical
## Applications, subject only to applicable laws and
## regulations governing limitations on product liability.
##
## THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
## PART OF THIS FILE AT ALL TIMES.
##
##-----------------------------------------------------------------------------
##
## Project    : The Xilinx PCI Express DMA 
## File       : xilinx_pcie_xdma_ref_board.xdc
## Version    : 4.1
##-----------------------------------------------------------------------------
# User Configuration 
# Link Width   - x8
# Link Speed   - gen3
# Family       - kintexu
# Part         - xcku115
# Package      - flvb2104
# Speed grade  - -2
# PCIe Block   - X0Y0
# Ref Board    - KCU1500
#
###############################################################################
#
#########################################################################################################################
# User Constraints
#########################################################################################################################
###############################################################################
# User Time Names / User Time Groups / Time Specs
###############################################################################
create_clock -period 10.000 -name sys_clk [get_ports sysclk_in_p]
set_false_path -from [get_ports sys_rst_n]
#set_false_path -through [get_pins xdma_0_i/inst/pcie3_ip_i/inst/xdma_0_pcie3_ip_pcie3_uscale_top_inst/pcie3_uscale_wrapper_inst/PCIE_3_1_inst/CFGMAX*]
#set_false_path -through [get_nets xdma_0_i/inst/cfg_max*]

###############################################################################
# User Physical Constraints
###############################################################################
### ADDED:
set_property CONFIG_MODE SPIx4 [current_design]
### ADDED:
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
###############################################################################
# Pinout and Related I/O Constraints
###############################################################################

### BEGIN ADDED
#GTH Bank 224 PCIe lanes 7:4
set_property PACKAGE_PIN BC2 [get_ports {pci_exp_rxp[7]}]
set_property PACKAGE_PIN BC1 [get_ports {pci_exp_rxn[7]}]
set_property PACKAGE_PIN BF5 [get_ports {pci_exp_txp[7]}]
set_property PACKAGE_PIN BF4 [get_ports {pci_exp_txn[7]}]
set_property PACKAGE_PIN BA2 [get_ports {pci_exp_rxp[6]}]
set_property PACKAGE_PIN BA1 [get_ports {pci_exp_rxn[6]}]
set_property PACKAGE_PIN BD5 [get_ports {pci_exp_txp[6]}]
set_property PACKAGE_PIN BD4 [get_ports {pci_exp_txn[6]}]
set_property PACKAGE_PIN AW4 [get_ports {pci_exp_rxp[5]}]
set_property PACKAGE_PIN AW3 [get_ports {pci_exp_rxn[5]}]
set_property PACKAGE_PIN BB5 [get_ports {pci_exp_txp[5]}]
set_property PACKAGE_PIN BB4 [get_ports {pci_exp_txn[5]}]
set_property PACKAGE_PIN AV2 [get_ports {pci_exp_rxp[4]}]
set_property PACKAGE_PIN AV1 [get_ports {pci_exp_rxn[4]}]
set_property PACKAGE_PIN AV7 [get_ports {pci_exp_txp[4]}]
set_property PACKAGE_PIN AV6 [get_ports {pci_exp_txn[4]}]
#set_property PACKAGE_PIN AW9 [get_ports "No Connect"] ;# MGTREFCLK0P
#set_property PACKAGE_PIN AW8 [get_ports "No Connect"] ;# MGTREFCLK0N
#set_property PACKAGE_PIN AV11 [get_ports "No Connect"] ;# MGTREFCLK1P
#set_property PACKAGE_PIN AV10 [get_ports "No Connect"] ;# MGTREFCLK1N
#GTH Bank 225 PCIe lanes 3:0
set_property PACKAGE_PIN AU4 [get_ports {pci_exp_rxp[3]}]
set_property PACKAGE_PIN AU3 [get_ports {pci_exp_rxn[3]}]
set_property PACKAGE_PIN AU9 [get_ports {pci_exp_txp[3]}]
set_property PACKAGE_PIN AU8 [get_ports {pci_exp_txn[3]}]
set_property PACKAGE_PIN AT2 [get_ports {pci_exp_rxp[2]}]
set_property PACKAGE_PIN AT1 [get_ports {pci_exp_rxn[2]}]
set_property PACKAGE_PIN AT7 [get_ports {pci_exp_txp[2]}]
set_property PACKAGE_PIN AT6 [get_ports {pci_exp_txn[2]}]
set_property PACKAGE_PIN AR4 [get_ports {pci_exp_rxp[1]}]
set_property PACKAGE_PIN AR3 [get_ports {pci_exp_rxn[1]}]
set_property PACKAGE_PIN AR9 [get_ports {pci_exp_txp[1]}]
set_property PACKAGE_PIN AR8 [get_ports {pci_exp_txn[1]}]
set_property PACKAGE_PIN AP2 [get_ports {pci_exp_rxp[0]}]
set_property PACKAGE_PIN AP1 [get_ports {pci_exp_rxn[0]}]
set_property PACKAGE_PIN AP7 [get_ports {pci_exp_txp[0]}]
set_property PACKAGE_PIN AP6 [get_ports {pci_exp_txn[0]}]
set_property PACKAGE_PIN AT10 [get_ports sysclk_in_n]
set_property PACKAGE_PIN AT11 [get_ports sysclk_in_p]
#set_property PACKAGE_PIN AP11 [get_ports "No Connect"] ;# MGTREFCLK1P
#set_property PACKAGE_PIN AP10 [get_ports "No Connect"] ;# MGTREFCLK1N

### END ADDED

#set_property LOC [get_package_pins -filter {PIN_FUNC == IO_T3U_N12_PERSTN0_65}] [get_ports sys_rst_n]
set_property PACKAGE_PIN AR26 [get_ports sys_rst_n]
set_property PULLUP true [get_ports sys_rst_n]
set_property IOSTANDARD LVCMOS18 [get_ports sys_rst_n]

set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CFGBVS GND [current_design]
## ADDED:
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]


##### REFCLK_IBUF###########
#set_property LOC GTHE3_COMMON_X1Y1 [get_cells refclk_ibuf]
set_property LOC AT11 [get_ports sysclk_in_p]
##### LED's ###########
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
######################################################################
#
set_false_path -to [get_pins -hier *sync_reg[0]/D]
#
