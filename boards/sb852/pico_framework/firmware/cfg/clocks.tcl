# set HMC rx_clk region asynchronous to all others
if {[llength [get_clocks -quiet -of_objects [get_nets hmc_subsystem_inst/hmc_native_top/NUM_LINK_GEN[0].hmc_node_512/rx_clk]]]>0} {
   set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets hmc_subsystem_inst/hmc_native_top/NUM_LINK_GEN[0].hmc_node_512/rx_clk]]
}

# set HMC rx_clk region asynchronous to all others
if {[llength [get_clocks -quiet -of_objects [get_nets hmc_subsystem_inst/hmc_native_top/NUM_LINK_GEN[1].hmc_node_512/rx_clk]]]>0} {
   set_clock_groups -asynchronous -group [get_clocks -of_objects [get_nets hmc_subsystem_inst/hmc_native_top/NUM_LINK_GEN[1].hmc_node_512/rx_clk]]
}

# set HMC user clock region asynchronous to all others.  This may be unused/unnecessary.
if {[llength [get_clocks -quiet hmc_clk_p0]]>0} {
   set_clock_groups -asynchronous -group [get_clocks hmc_clk_p0]
}

# set extra_clk region asynchronous to all others.  
if {[llength [get_clocks -quiet extra_clk]]>0} {
   set_clock_groups -asynchronous -group [get_clocks extra_clk]
}

# set user PicoBus clock (from MMCM) region asynchronous to all others
if {[llength [get_pins -quiet UserWrapper/UserModule_s2pb/s2pb/pico_clk_pll_i/clk_out1]]} {
     set_clock_groups -async -group [get_clocks -of_objects [get_pins UserWrapper/UserModule_s2pb/s2pb/pico_clk_pll_i/clk_out1]]
}

# generated StreamToFlashCtl clock
# Divide the 250 MHz stream clock by 2 down to 125 MHz
if {[llength [get_pins -quiet UserWrapper/StreamToFlashCtl/clk_gen/clk_reg_reg/C]]>0} {
    create_generated_clock -name StreamToFlashCtl_clk -divide_by 2 -source [get_pins UserWrapper/StreamToFlashCtl/clk_gen/clk_reg_reg/C] [get_pins UserWrapper/StreamToFlashCtl/clk_gen/inst/O]
    set_clock_groups -async -group [get_clocks StreamToFlashCtl_clk]
}

# identify all asynchronous clock domain crossing registers found in generic_fifo (when set to be SYNCHRONOUS=0)
set meta1_reg [list [get_cells -quiet -hier -filter {NAME =~ */meta1_reg}] \
                    [get_cells -quiet -hier -filter {NAME =~ */meta1_reg[*]}]]
                    
# set constraints from any clocked path to meta1_reg async registers
set_max_delay 4.0 -from  [get_clocks *] -to $meta1_reg -datapath_only
set_false_path -quiet -to $meta1_reg -hold

# this signal only toggles at startup when the memory system completes its initial training/startup sequence
 # the false path is needed when the user clock is not the stream clock (such as when user clk = DDR clock)
if {[llength [get_pins UserWrapper/stream_to_mem_0/stream_control/init_cmptd_q_reg/D]]} {
    set_false_path -to [get_pins UserWrapper/stream_to_mem_0/stream_control/init_cmptd_q_reg/D]
}
