// PicoAXISlave.sv
// Copyright 2014, Pico Computing, Inc.
// 
// Slave node in the interconnect.
// Block diagram:
// Read Channel:
// =============================================================================
//                                                                              
//                pico_daisychain_active_src_token_central                      
//                            +---------+                                       
// interconnect               |         |                           interconnect
// loop-in                    |         |                               loop-out
// request token stream       |request  |                   request token stream            
// -------------------------->|token    +-------------------------------------->
//                            |generator|                                       
//                            |         |                                       
//                            +---------+                                       
//                                                                              
// request data stream                                          read data stream
// -------------------+                         +------------------------------>
//                    |                         |                               
//                    |                         |                               
//                    V                         |                               
//              +-----------+            +------+------+            interconnect
//              |           |            |             |            clock domain
// .............|  request  |............|    data     |........................
//              |  fifo     |            |    fifo     |         ddr3 controller
//              |           |            |             |            clock domain
//              +-----+-----+            +-------------+                        
//                    |                         A                               
//                    |                         |                               
//                    |                         |                               
//                    V                         |                               
//        +-------------------------------------+-------------------+           
//        |                                                         |           
//        |                                                         |           
//        |                                                         |           
//        |                    ddr3 controller                      |           
//        |                                                         |           
//        |                                                         |           
//        |                                                         |           
//        +---------------------------------------------------------+           
//                                                                              
//                                                                              
// =============================================================================
//                                                                              
//                                                                              
// Write Channel:                                                               
// =============================================================================
//                                                                              
//                pico_daisychain_active_src_token_central                      
//                            +---------+                                       
// interconnect               |         |                           interconnect
// loop-in                    |         |                               loop-out
// request token stream       |request  |                   request token stream
// -------------------------->|token    +-------------------------------------->
//                            |generator|                                       
//                            |         |                                       
//                            +---------+                                       
//                                                                              
//                           pico_daisychain_passive_src_token_central          
//                                          +---------+                         
// write request stream                     |data     |        data token stream
// -----------------------------+---------->|token    |------------------------>
//                              |           |generator|                         
//                              |           |         |                         
//                              |           +---------+                         
// write data stream            |                              write resp stream
// --------------+              |                 +---------------------------->
//               |              |                 |                             
//               V              V                 |                             
//           +------+      +----+----+      +-----+----+            interconnect
//           |      |      |         |      |          |            clock domain
// ..........| data |......| request |......| response |........................
//           | fifo |      | fifo    |      | fifo     |         ddr3 controller
//           |      |      |         |      |          |            clock domain
//           +---+--+      +----+----+      +----------+                        
//               |              |                 A                             
//               |              |                 |                             
//               |              |                 |                             
//               V              V                 |                             
//        +---------------------------------------+-----------------+           
//        |                                                         |           
//        |                                                         |           
//        |                                                         |           
//        |                    ddr3 controller                      |           
//        |                                                         |           
//        |                                                         |           
//        |                                                         |           
//        +---------------------------------------------------------+           
//                                                                              
//                                                                              
// =============================================================================
//                                                                              

module PicoAXISlave #(
    parameter MASTER_ID_WIDTH       = 4,
    parameter C_AXI_ID_WIDTH        = 12,
    parameter C_AXI_ADDR_WIDTH      = 33,
    parameter C_AXI_DATA_WIDTH      = 256
) (
    input wire clk,
    input wire rst,

    ddr3_core_uif.master            slave,

    pico_daisychain_a_if.in         rd_req_in,
    pico_daisychain_a_if.out        rd_req_out,
    pico_daisychain_b_if.out        rd_data_out,

    pico_daisychain_a_if.in         wr_req_in,
    pico_daisychain_a_if.out        wr_req_out,
    pico_daisychain_b_if.in         wr_data_in,
    pico_daisychain_b_if.out        wr_data_out,
    pico_daisychain_b_if.out        wr_resp_out
);

    // some axi constant
    assign slave.arsize   = 3'h5;
    assign slave.arburst  = 2'h1;
    assign slave.arlock   = 1'h0;
    assign slave.arcache  = 4'h0;
    assign slave.arprot   = 3'h0;
    assign slave.arqos    = 4'h0;

    assign slave.awsize   = 3'h5;
    assign slave.awburst  = 2'h1;
    assign slave.awlock   = 1'h0;
    assign slave.awcache  = 4'h0;
    assign slave.awprot   = 3'h0;
    assign slave.awqos    = 4'h0;

    always @(*) begin
        rd_req_out.data     <= 0;
        rd_req_out.valid    <= 0;

        wr_req_out.data     <= 0;
        wr_req_out.valid    <= 0;

        wr_data_out.data    <= 0;
        wr_data_out.valid   <= 0;
    end
    
    localparam RD_REQ_WIDTH  = C_AXI_ID_WIDTH+C_AXI_ADDR_WIDTH+8;
    localparam RD_DATA_WIDTH = C_AXI_ID_WIDTH+C_AXI_DATA_WIDTH+2+1;
    localparam WR_REQ_WIDTH  = C_AXI_ID_WIDTH+C_AXI_ADDR_WIDTH+8;
    localparam WR_DATA_WIDTH = C_AXI_DATA_WIDTH+C_AXI_DATA_WIDTH/8+1;
    localparam WR_RESP_WIDTH = C_AXI_ID_WIDTH+2;

    wire rd_req_almostfull, rd_req_fifo_empty;
    wire [RD_REQ_WIDTH-1:0] rd_req_fifo_dout;
    // read request fifo for buffering and clock domain crossing
    FIFO #(
        .SYNC(0),
        .DATA_WIDTH(RD_REQ_WIDTH),
        .ALMOST_FULL(13'h180)
    ) rd_req_fifo (
        .wr_clk     (clk),
        .wr_rst     (rst),
        .wr_en      (rd_req_in.valid),
        .din        (rd_req_in.data),
        .almostfull (rd_req_almostfull),

        .rd_clk     (slave.clk),
        .rd_rst     (slave.rst),
        .rd_en      (slave.arready),
        .dout       (rd_req_fifo_dout),
        .empty      (rd_req_fifo_empty)
    );

    always_comb begin
        {slave.arid, slave.araddr, slave.arlen} <= rd_req_fifo_dout;
        slave.arvalid <= ~rd_req_fifo_empty;
    end

    // read request interconnect token generator
    pico_daisychain_active_src_token_central #(.ID_WIDTH(MASTER_ID_WIDTH))
    rd_req_token_central (
        .clk        (clk),
        .rst        (rst),
        .hold       (rd_req_almostfull),
        .token_i    (rd_req_in),
        .token_o    (rd_req_out)
    );

    wire rd_data_fifo_full;
    wire rd_data_fifo_empty;
    wire [RD_DATA_WIDTH-1:0] rd_data_fifo_dout;
    // read data fifo for buffering and clock domain crossing
    FIFO #(
        .SYNC(0),
        .DATA_WIDTH(RD_DATA_WIDTH),
        .ALMOST_FULL(13'h200 - 13'h10)
    ) rd_data_fifo (
        .wr_clk     (slave.clk),
        .wr_rst     (slave.rst),
        .wr_en      (slave.rvalid),
        .din        ({slave.rid, slave.rdata, slave.rresp, slave.rlast}),
        .full       (rd_data_fifo_full),

        .rd_clk     (clk),
        .rd_rst     (rst),
        .rd_en      (~rd_data_fifo_empty),
        .dout       (rd_data_fifo_dout),
        .empty      (rd_data_fifo_empty)
    );
    assign slave.rready = ~rd_data_fifo_full;
    always @ (posedge clk) begin
        rd_data_out.data    <= rd_data_fifo_dout[0 +: RD_DATA_WIDTH-MASTER_ID_WIDTH];
        rd_data_out.valid   <= ~rd_data_fifo_empty;
        rd_data_out.id      <= rd_data_fifo_dout[RD_DATA_WIDTH-1 -: MASTER_ID_WIDTH];
    end

    wire wr_req_fifo_almostfull, wr_req_fifo_empty;
    wire [WR_REQ_WIDTH-1:0] wr_req_fifo_dout;
    // write request fifo for buffering and clock domain crossing
    FIFO #(
        .SYNC(0),
        .DATA_WIDTH(WR_REQ_WIDTH),
        .ALMOST_FULL(13'h180)
    ) wr_req_fifo (
        .wr_clk     (clk),
        .wr_rst     (rst),
        .wr_en      (wr_req_in.valid),
        .din        (wr_req_in.data),
        .almostfull (wr_req_fifo_almostfull),

        .rd_clk     (slave.clk),
        .rd_rst     (slave.rst),
        .rd_en      (slave.awready),
        .dout       (wr_req_fifo_dout),
        .empty      (wr_req_fifo_empty)
    );

    always_comb begin
        {slave.awid, slave.awaddr, slave.awlen} <= wr_req_fifo_dout;
        slave.awvalid <= ~wr_req_fifo_empty;
    end

    wire wr_req_token_almostfull;
    // write request interconnect token generator
    pico_daisychain_active_src_token_central #(.ID_WIDTH(MASTER_ID_WIDTH))
    wr_req_token_central (
        .clk        (clk),
        .rst        (rst),
        .hold       (wr_req_fifo_almostfull | wr_req_token_almostfull),
        .token_i    (wr_req_in),
        .token_o    (wr_req_out)
    );

    wire wr_data_fifo_almostfull;
    wire wr_data_fifo_empty;
    wire [WR_DATA_WIDTH-1:0] wr_data_fifo_dout;
    // write data fifo for buffering and clock domain crossing
    FIFO #(
        .SYNC(0),
        .DATA_WIDTH(WR_DATA_WIDTH),
        .ALMOST_FULL(13'h200 - 13'h80)
    ) wr_data_fifo (
        .wr_clk     (clk),
        .wr_rst     (rst),
        .wr_en      (wr_data_in.valid),
        .din        (wr_data_in.data),
        .almostfull (wr_data_fifo_almostfull),

        .rd_clk     (slave.clk),
        .rd_rst     (slave.rst),
        .rd_en      (slave.wready),
        .dout       (wr_data_fifo_dout),
        .empty      (wr_data_fifo_empty)
    );
    always_comb begin
        {slave.wdata, slave.wstrb, slave.wlast} <= wr_data_fifo_dout;
        slave.wvalid = ~wr_data_fifo_empty;
    end

    // write data interconnect token generator
    pico_daisychain_passive_src_token_central #(.ID_WIDTH(MASTER_ID_WIDTH))
    wr_data_token_central (
        .clk        (clk),
        .rst        (rst),
        .hold       (wr_data_fifo_almostfull),
        .req_id     (wr_req_in.data[C_AXI_ID_WIDTH + C_AXI_ADDR_WIDTH+8-1 -: MASTER_ID_WIDTH]),
        .req_len    (wr_req_in.data[7:0]+1),
        .req_valid  (wr_req_in.valid),
        .req_almostfull (wr_req_token_almostfull),
        .token_o    (wr_data_out)
    );

    wire wr_resp_fifo_full;
    wire wr_resp_fifo_empty;
    wire [WR_RESP_WIDTH-1:0] wr_resp_fifo_dout;
    // write response fifo for buffering and clock domain crossing
    FIFO #(
        .SYNC(0),
        .DATA_WIDTH(WR_RESP_WIDTH),
        .ALMOST_FULL(13'h200 - 13'h10)
    ) wr_resp_fifo (
        .wr_clk     (slave.clk),
        .wr_rst     (slave.rst),
        .wr_en      (slave.bvalid),
        .din        ({slave.bid, slave.bresp}),
        .full       (wr_resp_fifo_full),

        .rd_clk     (clk),
        .rd_rst     (rst),
        .rd_en      (~wr_resp_fifo_empty),
        .dout       (wr_resp_fifo_dout),
        .empty      (wr_resp_fifo_empty)
    );
    assign slave.bready = ~wr_resp_fifo_full;
    always @ (posedge clk) begin
        wr_resp_out.data    <= wr_resp_fifo_dout[0 +: WR_RESP_WIDTH-MASTER_ID_WIDTH];
        wr_resp_out.valid   <= ~wr_resp_fifo_empty;
        wr_resp_out.id      <= wr_resp_fifo_dout[WR_RESP_WIDTH-1 -: MASTER_ID_WIDTH];
    end

endmodule
