// PicoDefines.v - here we configure the base firmware for our project

// This includes a placeholder "user" module that you will replace with your code.
// To use your own module, just change the name from PicoBus128_counter to your
// module's name, and then add the file to your ISE project.
`define USER_MODULE_NAME scout_top
`define XILINX_FPGA
`define XILINX_ULTRASCALE
`define XILINX_ULTRASCALE_PLUS
`define PCIE_GEN3X8
`define DDR4_ENABLED_BOARD

`define SB852_16G
`define ENABLE_QSPI_FLASH
`define PICOBUS_WIDTH 32
`define STREAM2_OUT_256BIT
`define STREAM2_IN_256BIT
`define STREAM2_IN_WIDTH 256
`define STREAM2_OUT_WIDTH 256

`define EXTRA_CLK
// We define the type of FPGA and card we're using.
`define PICO_MODEL_SB852
`define PICO_FPGA_VU9P

`include "BasePicoDefines.v"


