//
// Copyright (c) 2009-2019 Micron Technology, Inc. All Rights Reserved.
// This source code contains confidential information and trade secrets of
// Micron Technology, Inc.  Use, disclosure, or reproduction is prohibited
// without the prior express written permission of Micron Technology, Inc.
//
//
// *********************************************************
// striping.sv 
// This module implements striping, or address bit swizzling to cause     
// successive linear memory blocks to spread out over four DDR 
// memory modules.  An AXI transaction splitter divides a single AXI
// request into two requests if it is a burst that would cross the
// striped boundary.
//
// *********************************************************

`include "PicoDefines.v"

module striping
    #(parameter C0_C_S_AXI_ADDR_WIDTH    = 36,                       // AXI address width 
      parameter C0_C_S_AXI_ID_WIDTH      = 8,                        // width of the ID fields
      parameter C0_C_S_AXI_DATA_WIDTH    = 512,                      // AXI data width 
      parameter STRIPING_LSB             = C0_C_S_AXI_ADDR_WIDTH-2   // 13 is 8K striping, 14 is 16KB striping, 15 is 32K striping. 
                                                                     // C0_C_S_AXI_ADDR_WIDTH-2 means no striping but transactions will still be 
                                                                     // split if the boundary is crossed, in case the interconnect does not handle it.
                                                                     // LEN field must not cause burst addresses to cross two striping boundaries, 
                                                                     // which could happen for striping blocks less than 16KB
    )
    (
      input                                clk,
      input                                rst,

      // AXI ports from the user module
      // AXI write address channel signals
      output                               axi_awready,    // Indicates slave is ready to accept a
      input [C0_C_S_AXI_ID_WIDTH-1:0]      axi_awid,       // Write ID
      input [C0_C_S_AXI_ADDR_WIDTH-1:0]    axi_awaddr,     // Write address
      input [7:0]                          axi_awlen,      // Write Burst Length
      input [2:0]                          axi_awsize,     // Write Burst size
      input [1:0]                          axi_awburst,    // Write Burst type
      input                                axi_awlock,     // Write lock type
      input [3:0]                          axi_awcache,    // Write Cache type
      input [2:0]                          axi_awprot,     // Write Protection type
      input [3:0]                          axi_awqos,      // Write Quality of Service Signaling
      input                                axi_awvalid,    // Write address valid

      // AXI write data channel signals
      output                               axi_wready,     // Write data ready
      input [C0_C_S_AXI_DATA_WIDTH-1:0]    axi_wdata,      // Write data
      input [C0_C_S_AXI_DATA_WIDTH/8-1:0]  axi_wstrb,      // Write strobes
      input                                axi_wlast,      // Last write transaction
      input                                axi_wvalid,     // Write valid

      // AXI write response channel signals
      output  [C0_C_S_AXI_ID_WIDTH-1:0]    axi_bid,        // Response ID
      output  [1:0]                        axi_bresp,      // Write response
      output                               axi_bvalid,     // Write reponse valid
      input                                axi_bready,     // Response ready

      // AXI read address channel signals
      output                               axi_arready,    // Read address ready
      input [C0_C_S_AXI_ID_WIDTH-1:0]      axi_arid,       // Read ID
      input [C0_C_S_AXI_ADDR_WIDTH-1:0]    axi_araddr,     // Read address
      input [7:0]                          axi_arlen,      // Read Burst Length
      input [2:0]                          axi_arsize,     // Read Burst size
      input [1:0]                          axi_arburst,    // Read Burst type
      input                                axi_arlock,     // Read lock type
      input [3:0]                          axi_arcache,    // Read Cache type
      input [2:0]                          axi_arprot,     // Read Protection type
      input                                axi_arvalid,    // Read address valid
      input [3:0]                          axi_arqos,      // Read Quality of Service Signaling

      // AXI read data channel signals
      output  [C0_C_S_AXI_ID_WIDTH-1:0]    axi_rid,        // Response ID
      output  [1:0]                        axi_rresp,      // Read response
      output                               axi_rvalid,     // Read reponse valid
      output  [C0_C_S_AXI_DATA_WIDTH-1:0]  axi_rdata,      // Read data
      output                               axi_rlast,      // Read last
      input                                axi_rready,     // Read Response ready

      // Striped ports to the interconnect
      // AXI write address channel signals
      input                                axi_awready_striped,    // Indicates slave is ready to accept a
      output [C0_C_S_AXI_ID_WIDTH-1:0]     axi_awid_striped,       // Write ID
      output [C0_C_S_AXI_ADDR_WIDTH-1:0]   axi_awaddr_striped,     // Write address
      output [7:0]                         axi_awlen_striped,      // Write Burst Length
      output [2:0]                         axi_awsize_striped,     // Write Burst size
      output [1:0]                         axi_awburst_striped,    // Write Burst type
      output                               axi_awlock_striped,     // Write lock type
      output [3:0]                         axi_awcache_striped,    // Write Cache type
      output [2:0]                         axi_awprot_striped,     // Write Protection type
      output [3:0]                         axi_awqos_striped,      // Write Quality of Service Signaling
      output                               axi_awvalid_striped,    // Write address valid

      // AXI write data channel signals
      input                                axi_wready_striped,     // Write data ready
      output [C0_C_S_AXI_DATA_WIDTH-1:0]   axi_wdata_striped,      // Write data
      output [C0_C_S_AXI_DATA_WIDTH/8-1:0] axi_wstrb_striped,      // Write strobes
      output                               axi_wlast_striped,      // Last write transaction
      output                               axi_wvalid_striped,     // Write valid

      // AXI write response channel signals
      input  [C0_C_S_AXI_ID_WIDTH-1:0]     axi_bid_striped,        // Response ID
      input  [1:0]                         axi_bresp_striped,      // Write response
      input                                axi_bvalid_striped,     // Write reponse valid
      output                               axi_bready_striped,     // Response ready

      // AXI read address channel signals
      input                                axi_arready_striped,    // Read address ready
      output [C0_C_S_AXI_ID_WIDTH-1:0]     axi_arid_striped,       // Read ID
      output [C0_C_S_AXI_ADDR_WIDTH-1:0]   axi_araddr_striped,     // Read address
      output [7:0]                         axi_arlen_striped,      // Read Burst Length
      output [2:0]                         axi_arsize_striped,     // Read Burst size
      output [1:0]                         axi_arburst_striped,    // Read Burst type
      output                               axi_arlock_striped,     // Read lock type
      output [3:0]                         axi_arcache_striped,    // Read Cache type
      output [2:0]                         axi_arprot_striped,     // Read Protection type
      output                               axi_arvalid_striped,    // Read address valid
      output [3:0]                         axi_arqos_striped,      // Read Quality of Service Signaling

      // AXI read data channel signals
      input  [C0_C_S_AXI_ID_WIDTH-1:0]     axi_rid_striped,        // Response ID
      input  [1:0]                         axi_rresp_striped,      // Read response
      input                                axi_rvalid_striped,     // Read reponse valid
      input  [C0_C_S_AXI_DATA_WIDTH-1:0]   axi_rdata_striped,      // Read data
      input                                axi_rlast_striped,      // Read last
      output                               axi_rready_striped      // Read Response ready
    );

    // function to compute ceil( log( x ) )
    // Note: log is computed in base 2
    function integer clogb2;
        input [31:0] value;
        begin
            value = value - 1;
            for (clogb2 = 0; value > 0; clogb2 = clogb2 + 1) begin
                value = value >> 1;
            end
        end
    endfunction

    // Number of the first address bit on a word basis, ie A[6] for 512-bit data, A[5] for 256-bit data
    localparam WORD_ADDR_LSB = clogb2(C0_C_S_AXI_DATA_WIDTH/8);  

    wire    [C0_C_S_AXI_ADDR_WIDTH-1:0]     axi_araddr_swizzled;
    wire    [C0_C_S_AXI_ADDR_WIDTH-1:0]     axi_awaddr_swizzled;
    
    // swapped address lines so STRIPING_LSB and STRIPING_LSB+1 determine which DDR DIMM to address
    generate 
        if (STRIPING_LSB == C0_C_S_AXI_ADDR_WIDTH-2) begin
            assign  axi_araddr_swizzled =   axi_araddr[C0_C_S_AXI_ADDR_WIDTH-1:0];    // C0_C_S_AXI_ADDR_WIDTH-2 means no striping.
            assign  axi_awaddr_swizzled =   axi_awaddr[C0_C_S_AXI_ADDR_WIDTH-1:0];    // C0_C_S_AXI_ADDR_WIDTH-2 means no striping.
        end else begin
            assign  axi_araddr_swizzled =  {axi_araddr[STRIPING_LSB+1], axi_araddr[STRIPING_LSB],    // swap A[35:34](for 16GB DIMMS) and A[15:14]
                                            axi_araddr[C0_C_S_AXI_ADDR_WIDTH-3:STRIPING_LSB+2],
                                            axi_araddr[C0_C_S_AXI_ADDR_WIDTH-1], axi_araddr[C0_C_S_AXI_ADDR_WIDTH-2],
                                            axi_araddr[STRIPING_LSB-1:0]};


            assign  axi_awaddr_swizzled =  {axi_awaddr[STRIPING_LSB+1], axi_awaddr[STRIPING_LSB],    // swap A[35:34] and A[15:14]
                                            axi_awaddr[C0_C_S_AXI_ADDR_WIDTH-3:STRIPING_LSB+2],
                                            axi_awaddr[C0_C_S_AXI_ADDR_WIDTH-1], axi_awaddr[C0_C_S_AXI_ADDR_WIDTH-2],
                                            axi_awaddr[STRIPING_LSB-1:0]};


       end
   endgenerate

    
    // AXI Read transaction splitter, convert an AXI burst transaction into two transactions when it crosses the striping 
    // boundary so that each part goes to the correct DIMM.
    // Single arid and awid values must be used with this module as read data reordering is not done.  Single ID insures
    // that read data comes back in order from the inter-connect and the two halves of a split transaction will be
    // returned in order and not have any data from any other transaction come back between the two halves. With this
    // assumption the rid's are not saved and the read data is not saved in a fifo, it is just passed through in the
    // order received. The rlast of the first half is removed.  The B response of the first half of a split write transaction
    // is suppressed, so the requester sees a single correct response, as if the transaction was not split.

    wire [STRIPING_LSB:WORD_ADDR_LSB] ar_max_burst_addr;           // one extra bit for rollover 
    wire                              ar_stripe_boundary_crossed;  
    wire [7:0]                        arlen_first_half;
    reg  [7:0]                        arlen_first_half_latched;
    wire [7:0]                        arlen_second_half;
    reg                               ar_split;
    reg  [7:0]                        arlen_latched;
    reg  [C0_C_S_AXI_ADDR_WIDTH-1:0]  araddr_latched;
    reg  [C0_C_S_AXI_ID_WIDTH-1:0]    axi_arid_latched;
    reg  [2:0]                        axi_arsize_latched;
    reg  [1:0]                        axi_arburst_latched;
    reg                               axi_arlock_latched;
    reg  [3:0]                        axi_arcache_latched;
    reg  [2:0]                        axi_arprot_latched;
    reg  [3:0]                        axi_arqos_latched;
    wire [C0_C_S_AXI_ADDR_WIDTH-1:0]  axi_araddr_second_half;
    wire [C0_C_S_AXI_ADDR_WIDTH-1:0]  axi_araddr_second_half_swizzled;

    // For 512-bit data bus, A[5:0] are zero for 64-byte word alignment. arlen+1 is the number of words in a burst.
    // If axi_araddr[STRIPING_LSB-1:6] + arlen rolls over then the stripe boundary would be 
    // crossed in the burst and the transaction must be split into two transactions.
    assign ar_max_burst_addr          = {1'b0, axi_araddr[STRIPING_LSB-1:WORD_ADDR_LSB]} + axi_arlen;

    // If high bit of ar_max_burst_addr rolls over a stripe boundary would be crossed 
    assign ar_stripe_boundary_crossed =  ar_max_burst_addr[STRIPING_LSB]; 
 
    // The len field for the first half of a split transaction is the number of words that 
    // can go out without incrementing past the striping boundary minus 1.  This is equal to the 
    // inverted value of the word address low bits. This is only used on a split transaction
    assign arlen_first_half           =  {8'h0, ~axi_araddr[STRIPING_LSB-1:WORD_ADDR_LSB]};

    // The len field for the second half of a split transaction is the original len
    // minus the number of words sent in the first half minus 1. 
    assign arlen_second_half          = arlen_latched - arlen_first_half_latched - 1;

    // Maintain a circular buffer of 1-bit status for each AR command, 1=split, 0=no split.
    reg [31:0] ar_split_status;   // 32-bit vector for 32 outstanding AR requests
    reg [4:0]  ar_split_wrptr;
    reg [4:0]  ar_split_rdptr;
    wire       ar_split_full;

    always @ (posedge clk) begin
        if (rst) begin
            ar_split_status<= 0;
            ar_split_wrptr <= 0;
            ar_split_rdptr <= 0;
        end else begin
            if (axi_arvalid && axi_arready) begin   
                ar_split_status[ar_split_wrptr] <= ar_stripe_boundary_crossed;
                ar_split_wrptr <= ar_split_wrptr + 1;
            end else if (ar_split && axi_arvalid_striped && axi_arready_striped) begin 
                ar_split_status[ar_split_wrptr] <= 1'b0;   // flag not set for second half transaction 
                ar_split_wrptr <= ar_split_wrptr + 1;
            end
            if (axi_rvalid_striped && axi_rready_striped && axi_rlast_striped)  
                ar_split_rdptr <= ar_split_rdptr + 1; 
        end
    end

    assign ar_split_full  = ((ar_split_wrptr + 5'h1) == ar_split_rdptr);

    // ar_split is assserted when a transaction that needs to be split is issued with arvalid & arready.
    // It is deasserted after the second half of the split transaction is generated.
    always @ (posedge clk) begin
        if (rst) begin
            ar_split <= 0;
        end else if (axi_arvalid && axi_arready) begin
            ar_split       <= ar_stripe_boundary_crossed;
        end else if (axi_arvalid_striped && axi_arready_striped)
            ar_split       <= 1'b0;
    end

    // latch the araddr and arlen for the second half of a split transaction
    always @ (posedge clk) begin
        if (axi_arvalid && axi_arready) begin
            araddr_latched           <= axi_araddr;
            arlen_latched            <= axi_arlen;
            arlen_first_half_latched <= arlen_first_half;
            axi_arid_latched         <= axi_arid;
            axi_arsize_latched       <= axi_arsize;
            axi_arburst_latched      <= axi_arburst;
            axi_arlock_latched       <= axi_arlock;
            axi_arcache_latched      <= axi_arcache;
            axi_arprot_latched       <= axi_arprot;
            axi_arqos_latched        <= axi_arqos;
        end
    end

    // Suppress rlast when returning rdata for first half of split transaction.
    assign axi_rlast = axi_rlast_striped & ~ar_split_status[ar_split_rdptr];

    // Deassert axi_arready while second half of split transaction is being generated
    // and when the ar_split_status buffer is full
    assign axi_arready = axi_arready_striped & ~ar_split & ~ar_split_full;

    // pass through axi_arvalid, also force it high for the second half of a split transaction
    assign axi_arvalid_striped = (axi_arvalid | ar_split) & ~ar_split_full;

    // axi_arlen is unmodified for unsplit requests, shortened for first half of split requests and 
    // set to the remainder of the burst for the second half of split requests
    assign axi_arlen_striped = ar_split ? arlen_second_half : (ar_stripe_boundary_crossed ? arlen_first_half : axi_arlen);

    // axi_araddr is passed through for unsplit transactions and the first half of split transactions.
    // axi_araddr is incremented across the stripe boundary for the second half of split transactions.
    assign axi_araddr_striped = ar_split ? axi_araddr_second_half_swizzled : axi_araddr_swizzled; 

    // Add the arlen+1 of the first half to the original word address
    assign axi_araddr_second_half[C0_C_S_AXI_ADDR_WIDTH-1:WORD_ADDR_LSB]     = araddr_latched[C0_C_S_AXI_ADDR_WIDTH-1:WORD_ADDR_LSB] + arlen_first_half_latched  + 1; 
    assign axi_araddr_second_half[WORD_ADDR_LSB-1:0]                         = araddr_latched[WORD_ADDR_LSB-1:0]; 


    generate 
        if (STRIPING_LSB == C0_C_S_AXI_ADDR_WIDTH-2) 
            assign axi_araddr_second_half_swizzled =  axi_araddr_second_half[C0_C_S_AXI_ADDR_WIDTH-1:0];   
        else
            assign axi_araddr_second_half_swizzled = {axi_araddr_second_half[STRIPING_LSB+1], axi_araddr_second_half[STRIPING_LSB],    
                                                      axi_araddr_second_half[C0_C_S_AXI_ADDR_WIDTH-3:STRIPING_LSB+2],
                                                      axi_araddr_second_half[C0_C_S_AXI_ADDR_WIDTH-1], axi_araddr_second_half[C0_C_S_AXI_ADDR_WIDTH-2],
                                                      axi_araddr_second_half[STRIPING_LSB-1:0]};
    endgenerate


    // Pass through all other AR an R channel signals to/from the striped port directly
    // for non-split transactions and using the latched value for the second
    // half of a split transaction.
    assign  axi_arid_striped    = ar_split ? axi_arid_latched    : axi_arid;
    assign  axi_arsize_striped  = ar_split ? axi_arsize_latched  : axi_arsize;
    assign  axi_arburst_striped = ar_split ? axi_arburst_latched : axi_arburst;
    assign  axi_arlock_striped  = ar_split ? axi_arlock_latched  : axi_arlock;
    assign  axi_arcache_striped = ar_split ? axi_arcache_latched : axi_arcache;
    assign  axi_arprot_striped  = ar_split ? axi_arprot_latched  : axi_arprot;
    assign  axi_arqos_striped   = ar_split ? axi_arqos_latched   : axi_arqos;

    assign  axi_rid             = axi_rid_striped;
    assign  axi_rresp           = axi_rresp_striped;
    assign  axi_rvalid          = axi_rvalid_striped;
    assign  axi_rdata           = axi_rdata_striped;
    assign  axi_rready_striped  = axi_rready;


    // AXI Write transaction splitter, convert an AXI burst transaction into two transactions when it crosses the striping 
    // boundary so that each part goes to the correct DIMM. axi_wlast is forced at the end of the first half of a split
    // transaction.  The B response of the first half of a split write transaction
    // is suppressed, so the requester sees a single correct response, as if the transaction was not split.
  
    wire [STRIPING_LSB:WORD_ADDR_LSB] aw_max_burst_addr;           // one extra bit for rollover
    wire                              aw_stripe_boundary_crossed;
    wire [7:0]                        awlen_first_half;
    reg  [7:0]                        awlen_first_half_latched;
    wire [7:0]                        awlen_second_half;
    reg                               aw_split;
    reg  [7:0]                        awlen_latched;
    reg  [C0_C_S_AXI_ADDR_WIDTH-1:0]  awaddr_latched;
    reg  [C0_C_S_AXI_ID_WIDTH-1:0]    axi_awid_latched;
    reg  [2:0]                        axi_awsize_latched;
    reg  [1:0]                        axi_awburst_latched;
    reg                               axi_awlock_latched;
    reg  [3:0]                        axi_awcache_latched;
    reg  [2:0]                        axi_awprot_latched;
    reg  [3:0]                        axi_awqos_latched;
    wire [C0_C_S_AXI_ADDR_WIDTH-1:0]  axi_awaddr_second_half;
    wire [C0_C_S_AXI_ADDR_WIDTH-1:0]  axi_awaddr_second_half_swizzled;

    // For 512-bit data bus, A[5:0] are zero for 64-byte word alignment. awlen+1 is the number of words in a burst.
    // If axi_awaddr[STRIPING_LSB-1:6] + awlen rolls over then the stripe boundary would be
    // crossed in the burst and the transaction must be split into two transactions.
    assign aw_max_burst_addr          = {1'b0, axi_awaddr[STRIPING_LSB-1:WORD_ADDR_LSB]} + axi_awlen;

    // If high bit of aw_max_burst_addr rolls over a stripe boundary would be crossed
    assign aw_stripe_boundary_crossed =  aw_max_burst_addr[STRIPING_LSB];

    // The len field for the first half of a split transaction is the number of words that
    // can go out without incrementing past the striping boundary minus 1.  This is equal to the
    // inverted value of the word address low bits up to the STRIPING_LSB. This is only used on a split transaction
    assign awlen_first_half           =  {8'h0, ~axi_awaddr[STRIPING_LSB-1:WORD_ADDR_LSB]};

    // The len field for the second half of a split transaction is the original len
    // minus the number of words sent in the first half minus 1. 
    assign awlen_second_half          = awlen_latched - awlen_first_half_latched - 1;

    // Maintain a circular buffer of 1-bit status for each AW command, 1=split, 0=no split.
    reg [31:0] aw_split_status;   // 32-bit vector for 32 outstanding AW requests
    reg [4:0]  aw_split_wrptr;
    reg [4:0]  aw_split_rdptr;
    wire       aw_split_full;

    wire       aw_length_full;
    wire       aw_length_empty;

    always @ (posedge clk) begin
        if (rst) begin
            aw_split_status<= 0;
            aw_split_wrptr <= 0;
            aw_split_rdptr <= 0;
        end else begin
            if (axi_awvalid && axi_awready) begin
                aw_split_status[aw_split_wrptr] <= aw_stripe_boundary_crossed;
                aw_split_wrptr <= aw_split_wrptr + 1;
            end else if (aw_split && axi_awvalid_striped && axi_awready_striped) begin 
                aw_split_status[aw_split_wrptr] <= 1'b0;   // flag not set for second half transaction 
                aw_split_wrptr <= aw_split_wrptr + 1;
            end
            if (axi_bvalid_striped && axi_bready_striped)
                aw_split_rdptr <= aw_split_rdptr + 1;
        end
    end

    assign aw_split_full  = ((aw_split_wrptr + 5'h1) == aw_split_rdptr);

    // aw_split is assserted when a write transaction that needs to be split is issued with awvalid & awready.
    // It is deasserted after the second half of the split transaction is generated.
    always @ (posedge clk) begin
        if (rst) begin
            aw_split <= 0;
        end else if (axi_awvalid && axi_awready) begin
            aw_split       <= aw_stripe_boundary_crossed;
        end else if (axi_awvalid_striped && axi_awready_striped)
            aw_split       <= 1'b0;
    end

    // latch the awaddr and awlen for the second half of a split transaction
    always @ (posedge clk) begin
        if (axi_awvalid && axi_awready) begin
            awaddr_latched           <= axi_awaddr;
            awlen_latched            <= axi_awlen;
            awlen_first_half_latched <= awlen_first_half;
            axi_awid_latched         <= axi_awid;
            axi_awsize_latched       <= axi_awsize;
            axi_awburst_latched      <= axi_awburst;
            axi_awlock_latched       <= axi_awlock;
            axi_awcache_latched      <= axi_awcache;
            axi_awprot_latched       <= axi_awprot;
            axi_awqos_latched        <= axi_awqos;
        end
    end

    // Suppress bvalid when it comes back for the first half of a split transaction.
    assign axi_bvalid = axi_bvalid_striped & ~aw_split_status[aw_split_rdptr];

    // Force axi_bready_striped if the next B response to come back is for the first half
    // of a split transaction, which will not go back to the user port.
    assign  axi_bready_striped   =  axi_bready | aw_split_status[aw_split_rdptr];

    // Deassert axi_awready while second half of split transaction is being generated
    // and when the aw_split_status buffer or aw_length buffer is full.
    assign axi_awready = axi_awready_striped & ~aw_split & ~aw_split_full & ~aw_length_full;

    // pass through axi_awvalid, also force it high for the second half of a split transaction
    assign axi_awvalid_striped = (axi_awvalid | aw_split) & ~aw_split_full & ~aw_length_full;

    // axi_awlen is unmodified for unsplit requests, shortened for first half of split requests and
    // set to the remainder of the burst for the second half of split requests
    assign axi_awlen_striped = aw_split ? awlen_second_half : (aw_stripe_boundary_crossed ? awlen_first_half : axi_awlen);

    // axi_awaddr is passed through for unsplit transactions and the first half of split transactions.
    // axi_awaddr is incremented across the stripe boundary for the second half of split transactions.
    assign axi_awaddr_striped = aw_split ? axi_awaddr_second_half_swizzled : axi_awaddr_swizzled;

    // Add the awlen+1 of the first half to the original word address
    assign axi_awaddr_second_half[C0_C_S_AXI_ADDR_WIDTH-1:WORD_ADDR_LSB]     = awaddr_latched[C0_C_S_AXI_ADDR_WIDTH-1:WORD_ADDR_LSB] + awlen_first_half_latched  + 1;
    assign axi_awaddr_second_half[WORD_ADDR_LSB-1:0]                         = awaddr_latched[WORD_ADDR_LSB-1:0];

    generate 
        if (STRIPING_LSB == C0_C_S_AXI_ADDR_WIDTH-2) 
            assign axi_awaddr_second_half_swizzled =  axi_awaddr_second_half[C0_C_S_AXI_ADDR_WIDTH-1:0];   
        else
            assign axi_awaddr_second_half_swizzled = {axi_awaddr_second_half[STRIPING_LSB+1], axi_awaddr_second_half[STRIPING_LSB],
                                                      axi_awaddr_second_half[C0_C_S_AXI_ADDR_WIDTH-3:STRIPING_LSB+2],
                                                      axi_awaddr_second_half[C0_C_S_AXI_ADDR_WIDTH-1], axi_awaddr_second_half[C0_C_S_AXI_ADDR_WIDTH-2],
                                                      axi_awaddr_second_half[STRIPING_LSB-1:0]};
    endgenerate

    // We need to force wlast on the last word of the first half of a split transaction.
    // Save the length fields of each AW command, both split and unsplit, in 
    // a buffer. When the wdata comes through count down the length value and assert 
    // wlast on the last word.

    // Circular buffer of 8-bit LEN values from each AW command.  It is the same size as 
    // the aw_split_status buffer and the write pointer is incremented at the same time.
    // The read pointer is incremented on each wlast instead of each bready.
    reg [7:0]  aw_length  [0:31];   // 32 entries 
    reg [7:0]  aw_length_cntr;
    reg [4:0]  aw_length_wrptr;
    reg [4:0]  aw_length_rdptr;
    reg        aw_length_cntr_in_use;
    wire       force_wlast;

    always @ (posedge clk) begin
        if (rst) begin
            aw_length_wrptr <= 0;
            aw_length_rdptr <= 0;
        end else begin
            if (axi_awvalid && axi_awready) begin
                aw_length[aw_length_wrptr] <= axi_awlen_striped;
                aw_length_wrptr <= aw_length_wrptr + 1;
            end else if (aw_split && axi_awvalid_striped && axi_awready_striped) begin 
                aw_length[aw_length_wrptr] <= axi_awlen_striped;
                aw_length_wrptr <= aw_length_wrptr + 1;
            end
            if (axi_wvalid_striped && axi_wready_striped && axi_wlast_striped)
                aw_length_rdptr <= aw_length_rdptr + 1;
        end
    end

    // Deassert aw_ready if the buffer fills up
    assign aw_length_full  = ((aw_length_wrptr + 5'h1) == aw_length_rdptr);

    // aw_length_empty is used to disable wready and wvalid_striped until the length is known.
    assign aw_length_empty = (aw_length_wrptr == aw_length_rdptr);

    // Load a downcounter on axi_wvalid & axi_wready with the aw_length if the length is 
    // not zero.  Count down on each wdata sent and force wlast when the cnt is zero.
    // wlast is asserted when the length in the buffer or length the downcounter is zero.
    // aw_length_cntr_in_use is a signal that denotes the down counter is in use
    always @ (posedge clk) begin
        if (rst) begin
            aw_length_cntr <= 0;
            aw_length_cntr_in_use <= 0;
        end else if (axi_wvalid & axi_wready) begin
            if (~aw_length_cntr_in_use) begin
                if (aw_length[aw_length_rdptr] == 0) begin
                    aw_length_cntr <= 0;
                    aw_length_cntr_in_use <= 0;
                end else begin
                    aw_length_cntr <= (aw_length[aw_length_rdptr] - 1);
                    aw_length_cntr_in_use <= 1'b1;
                end
            end else begin
               if (aw_length_cntr == 0)
                   aw_length_cntr_in_use <= 0;
               else
                   aw_length_cntr <= aw_length_cntr - 1;
            end
        end
    end 

    // The aw_length value from the buffer may be zero, in which case wlast must be asserted with 
    // the first wdata value.  Or it may be 1 to 255, in which case it is saved in the aw_length_cntr
    // and decremented on each axi_wvalid & axi_wready. wlast is forced when the saved length in the 
    // the downcounter reaches zero.
    assign force_wlast =  (aw_length_cntr_in_use & (aw_length_cntr == 0)) | (aw_length[aw_length_rdptr] == 0);

    assign  axi_wlast_striped    =  axi_wlast | force_wlast;  

    // Deassert axi_wready before the aw_length is known because we don't know
    // if the transaction must be split and wlast asserted.
     assign  axi_wready          =  axi_wready_striped & ~aw_length_empty;

    // Also deassert axi_wvalid_striped before an AW command is received because the interconnect
    // could be driving axi_wready_striped and would take the wdata
    assign  axi_wvalid_striped   =  axi_wvalid & ~aw_length_empty;

    // Pass through all other AW and W channel signals to/from the striped port directly
    // for non-split transactions and using the latched value for the second
    // half of a split transaction.
    assign  axi_awid_striped     =  aw_split ? axi_awid_latched    : axi_awid;
    assign  axi_awsize_striped   =  aw_split ? axi_awsize_latched  : axi_awsize;
    assign  axi_awburst_striped  =  aw_split ? axi_awburst_latched : axi_awburst;
    assign  axi_awlock_striped   =  aw_split ? axi_awlock_latched  : axi_awlock;
    assign  axi_awcache_striped  =  aw_split ? axi_awcache_latched : axi_awcache;
    assign  axi_awprot_striped   =  aw_split ? axi_awprot_latched  : axi_awprot;
    assign  axi_awqos_striped    =  aw_split ? axi_awqos_latched   : axi_awqos;

    assign  axi_wdata_striped    =  axi_wdata;
    assign  axi_wstrb_striped    =  axi_wstrb;

    assign  axi_bid              =  axi_bid_striped;
    assign  axi_bresp            =  axi_bresp_striped;

endmodule

