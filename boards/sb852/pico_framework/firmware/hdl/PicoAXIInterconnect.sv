// PicoAXIInterconnect.sv
// Copyright 2014, Pico Computing, Inc.
//
// Daisy chain AXI interconnect wrapper
//
// Complete block diagram of the interconnect:
// =============================================================================
//                                                                              
//                                                                              
//          +-----------------------------------------------------+             
//          |                                                     |             
//          |                                                     |             
//          |                  ddr3 controller                    |             
//          |                                                     |             
//          |                                                     |             
//          +---------------------+-------------------------------+             
//                                |   A                                         
//                                |   |                                         
//                                V   |                                         
//                      +-------------+-------+                                 
//                      |                     |                                 
//                      |                     |                  ddr3 controller
//                      |      interconnect   |                     clock domain
// .....................|       slave node    |.................................
//                      |                     |                     interconnect
//                      |                     |                     clock domain
//                      |    (PicoAXISlave)   |                                 
//                      |                     |                                 
//         +------------<  read request token <---------------+                 
//         |            |           read data >--------------+|                 
//         | +----------< write request token <-------------+||                 
//         | |+---------< write data token    |             |||                 
//         | ||         |          write data <-----------+ |||                 
//         | ||         |      write response >----------+| |||                 
//         | ||         |                     |          || |||                 
//         | ||         +---------------------+          || |||                 
//         | ||                                          || |||                 
//         | ||                                          || |||                 
//         | ||                                          || |||                 
//         | ||                                          || |||                 
//         | ||                                          || |||                 
//         | ||                                          || |||                 
//         | ||                                          || |||                 
//         V VV                                          V| |V|                 
//    +--------------+       +--------------+        +----+++-+-----+           
//    |              |       |              |        |              |           
//    |              |<------+              |<-------+              |           
//    |              +------>|              +------->|              |           
//    |              +------>|              +------->|              |           
//    | interconnect +------>| interconnect +------->| interconnect |           
//    | master node 3|<------+ master node 2|<-------+ master node 1|     inter-
//    |              +------>|              +------->|              |    connect
//    |(PicoAXIMaster)       |(PicoAXIMaster)        |(PicoAXIMaster)      clock
//    |              |       |              |        |              |     domain
// ...|              |.......|              |........|              |...........
//    |              |       |              |        |              |       user
//    |              |       |              |        |              |      clock
//    +-----+--------+       +-----+--------+        +------+-------+     domain
//          | A                    | A                      | A                 
//          | |                    | |                      | |                 
//          | |                    | |                      | |                 
//          V |                    V |                      V |                 
//       user logic             user logic            pico stream to axi        
//                                                                              
// =============================================================================
// 


module PicoAXIInterconnect #(
    parameter   PICO_AXI_MASTERS                = 2,
                    // RANGE = (1:16)
                    // Number of Slave Interfaces (SI) for connecting 
                    // to master IP.
    parameter   C_AXI_ID_WIDTH                  = 12, 
                    // Bits of ID signals at master port.
                    // RANGE = (4:12)
    parameter   C_AXI_ADDR_WIDTH                = 33, 
                    // RANGE = (12:32)
                    // Width of S_AXI_AWADDR, S_AXI_ARADDR, M_AXI_AWADDR and 
                    // M_AXI_ARADDR for every SI/MI.
    parameter   C_AXI_DATA_WIDTH                = 256
                    // RANGE = (128)
    )
    (
 
    input  wire                                 ddr3_ui_clk,
    input  wire                                 ddr3_ui_rst,

    // System Signals
    input  wire                                 clk,
    input  wire                                 rst,

    // slave port 0
    input  wire                                 s0_axi_clk,
    input  wire [C_AXI_ID_WIDTH-5:0]            s0_axi_awid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s0_axi_awaddr,
    input  wire [7:0]                           s0_axi_awlen,
    input  wire [2:0]                           s0_axi_awsize,
    input  wire [1:0]                           s0_axi_awburst,
    input  wire                                 s0_axi_awlock,
    input  wire [3:0]                           s0_axi_awcache,
    input  wire [2:0]                           s0_axi_awprot,
    input  wire [3:0]                           s0_axi_awqos,
    input  wire                                 s0_axi_awvalid,
    output wire                                 s0_axi_awready,
    input  wire [C_AXI_DATA_WIDTH-1:0]          s0_axi_wdata,
    input  wire [C_AXI_DATA_WIDTH/8-1:0]        s0_axi_wstrb,
    input  wire                                 s0_axi_wlast,
    input  wire                                 s0_axi_wvalid,
    output wire                                 s0_axi_wready,
    output wire [C_AXI_ID_WIDTH-5:0]            s0_axi_bid,
    output wire [1:0]                           s0_axi_bresp,
    output wire                                 s0_axi_bvalid,
    input  wire                                 s0_axi_bready,
    input  wire [C_AXI_ID_WIDTH-5:0]            s0_axi_arid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s0_axi_araddr,
    input  wire [7:0]                           s0_axi_arlen,
    input  wire [2:0]                           s0_axi_arsize,
    input  wire [1:0]                           s0_axi_arburst,
    input  wire                                 s0_axi_arlock,
    input  wire [3:0]                           s0_axi_arcache,
    input  wire [2:0]                           s0_axi_arprot,
    input  wire [3:0]                           s0_axi_arqos,
    input  wire                                 s0_axi_arvalid,
    output wire                                 s0_axi_arready,
    output wire [C_AXI_ID_WIDTH-5:0]            s0_axi_rid,
    output wire [C_AXI_DATA_WIDTH-1:0]          s0_axi_rdata,
    output wire [1:0]                           s0_axi_rresp,
    output wire                                 s0_axi_rlast,
    output wire                                 s0_axi_rvalid,
    input  wire                                 s0_axi_rready,

    // slave port 1
    input  wire                                 s1_axi_clk,
    input  wire [C_AXI_ID_WIDTH-5:0]            s1_axi_awid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s1_axi_awaddr,
    input  wire [7:0]                           s1_axi_awlen,
    input  wire [2:0]                           s1_axi_awsize,
    input  wire [1:0]                           s1_axi_awburst,
    input  wire                                 s1_axi_awlock,
    input  wire [3:0]                           s1_axi_awcache,
    input  wire [2:0]                           s1_axi_awprot,
    input  wire [3:0]                           s1_axi_awqos,
    input  wire                                 s1_axi_awvalid,
    output wire                                 s1_axi_awready,
    input  wire [C_AXI_DATA_WIDTH-1:0]          s1_axi_wdata,
    input  wire [C_AXI_DATA_WIDTH/8-1:0]        s1_axi_wstrb,
    input  wire                                 s1_axi_wlast,
    input  wire                                 s1_axi_wvalid,
    output wire                                 s1_axi_wready,
    output wire [C_AXI_ID_WIDTH-5:0]            s1_axi_bid,
    output wire [1:0]                           s1_axi_bresp,
    output wire                                 s1_axi_bvalid,
    input  wire                                 s1_axi_bready,
    input  wire [C_AXI_ID_WIDTH-5:0]            s1_axi_arid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s1_axi_araddr,
    input  wire [7:0]                           s1_axi_arlen,
    input  wire [2:0]                           s1_axi_arsize,
    input  wire [1:0]                           s1_axi_arburst,
    input  wire                                 s1_axi_arlock,
    input  wire [3:0]                           s1_axi_arcache,
    input  wire [2:0]                           s1_axi_arprot,
    input  wire [3:0]                           s1_axi_arqos,
    input  wire                                 s1_axi_arvalid,
    output wire                                 s1_axi_arready,
    output wire [C_AXI_ID_WIDTH-5:0]            s1_axi_rid,
    output wire [C_AXI_DATA_WIDTH-1:0]          s1_axi_rdata,
    output wire [1:0]                           s1_axi_rresp,
    output wire                                 s1_axi_rlast,
    output wire                                 s1_axi_rvalid,
    input  wire                                 s1_axi_rready,

    // slave port 2
    input  wire                                 s2_axi_clk,
    input  wire [C_AXI_ID_WIDTH-5:0]            s2_axi_awid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s2_axi_awaddr,
    input  wire [7:0]                           s2_axi_awlen,
    input  wire [2:0]                           s2_axi_awsize,
    input  wire [1:0]                           s2_axi_awburst,
    input  wire                                 s2_axi_awlock,
    input  wire [3:0]                           s2_axi_awcache,
    input  wire [2:0]                           s2_axi_awprot,
    input  wire [3:0]                           s2_axi_awqos,
    input  wire                                 s2_axi_awvalid,
    output wire                                 s2_axi_awready,
    input  wire [C_AXI_DATA_WIDTH-1:0]          s2_axi_wdata,
    input  wire [C_AXI_DATA_WIDTH/8-1:0]        s2_axi_wstrb,
    input  wire                                 s2_axi_wlast,
    input  wire                                 s2_axi_wvalid,
    output wire                                 s2_axi_wready,
    output wire [C_AXI_ID_WIDTH-5:0]            s2_axi_bid,
    output wire [1:0]                           s2_axi_bresp,
    output wire                                 s2_axi_bvalid,
    input  wire                                 s2_axi_bready,
    input  wire [C_AXI_ID_WIDTH-5:0]            s2_axi_arid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s2_axi_araddr,
    input  wire [7:0]                           s2_axi_arlen,
    input  wire [2:0]                           s2_axi_arsize,
    input  wire [1:0]                           s2_axi_arburst,
    input  wire                                 s2_axi_arlock,
    input  wire [3:0]                           s2_axi_arcache,
    input  wire [2:0]                           s2_axi_arprot,
    input  wire [3:0]                           s2_axi_arqos,
    input  wire                                 s2_axi_arvalid,
    output wire                                 s2_axi_arready,
    output wire [C_AXI_ID_WIDTH-5:0]            s2_axi_rid,
    output wire [C_AXI_DATA_WIDTH-1:0]          s2_axi_rdata,
    output wire [1:0]                           s2_axi_rresp,
    output wire                                 s2_axi_rlast,
    output wire                                 s2_axi_rvalid,
    input  wire                                 s2_axi_rready,

    // slave port 3
    input  wire                                 s3_axi_clk,
    input  wire [C_AXI_ID_WIDTH-5:0]            s3_axi_awid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s3_axi_awaddr,
    input  wire [7:0]                           s3_axi_awlen,
    input  wire [2:0]                           s3_axi_awsize,
    input  wire [1:0]                           s3_axi_awburst,
    input  wire                                 s3_axi_awlock,
    input  wire [3:0]                           s3_axi_awcache,
    input  wire [2:0]                           s3_axi_awprot,
    input  wire [3:0]                           s3_axi_awqos,
    input  wire                                 s3_axi_awvalid,
    output wire                                 s3_axi_awready,
    input  wire [C_AXI_DATA_WIDTH-1:0]          s3_axi_wdata,
    input  wire [C_AXI_DATA_WIDTH/8-1:0]        s3_axi_wstrb,
    input  wire                                 s3_axi_wlast,
    input  wire                                 s3_axi_wvalid,
    output wire                                 s3_axi_wready,
    output wire [C_AXI_ID_WIDTH-5:0]            s3_axi_bid,
    output wire [1:0]                           s3_axi_bresp,
    output wire                                 s3_axi_bvalid,
    input  wire                                 s3_axi_bready,
    input  wire [C_AXI_ID_WIDTH-5:0]            s3_axi_arid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s3_axi_araddr,
    input  wire [7:0]                           s3_axi_arlen,
    input  wire [2:0]                           s3_axi_arsize,
    input  wire [1:0]                           s3_axi_arburst,
    input  wire                                 s3_axi_arlock,
    input  wire [3:0]                           s3_axi_arcache,
    input  wire [2:0]                           s3_axi_arprot,
    input  wire [3:0]                           s3_axi_arqos,
    input  wire                                 s3_axi_arvalid,
    output wire                                 s3_axi_arready,
    output wire [C_AXI_ID_WIDTH-5:0]            s3_axi_rid,
    output wire [C_AXI_DATA_WIDTH-1:0]          s3_axi_rdata,
    output wire [1:0]                           s3_axi_rresp,
    output wire                                 s3_axi_rlast,
    output wire                                 s3_axi_rvalid,
    input  wire                                 s3_axi_rready,

    // slave port 4
    input  wire                                 s4_axi_clk,
    input  wire [C_AXI_ID_WIDTH-5:0]            s4_axi_awid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s4_axi_awaddr,
    input  wire [7:0]                           s4_axi_awlen,
    input  wire [2:0]                           s4_axi_awsize,
    input  wire [1:0]                           s4_axi_awburst,
    input  wire                                 s4_axi_awlock,
    input  wire [3:0]                           s4_axi_awcache,
    input  wire [2:0]                           s4_axi_awprot,
    input  wire [3:0]                           s4_axi_awqos,
    input  wire                                 s4_axi_awvalid,
    output wire                                 s4_axi_awready,
    input  wire [C_AXI_DATA_WIDTH-1:0]          s4_axi_wdata,
    input  wire [C_AXI_DATA_WIDTH/8-1:0]        s4_axi_wstrb,
    input  wire                                 s4_axi_wlast,
    input  wire                                 s4_axi_wvalid,
    output wire                                 s4_axi_wready,
    output wire [C_AXI_ID_WIDTH-5:0]            s4_axi_bid,
    output wire [1:0]                           s4_axi_bresp,
    output wire                                 s4_axi_bvalid,
    input  wire                                 s4_axi_bready,
    input  wire [C_AXI_ID_WIDTH-5:0]            s4_axi_arid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s4_axi_araddr,
    input  wire [7:0]                           s4_axi_arlen,
    input  wire [2:0]                           s4_axi_arsize,
    input  wire [1:0]                           s4_axi_arburst,
    input  wire                                 s4_axi_arlock,
    input  wire [3:0]                           s4_axi_arcache,
    input  wire [2:0]                           s4_axi_arprot,
    input  wire [3:0]                           s4_axi_arqos,
    input  wire                                 s4_axi_arvalid,
    output wire                                 s4_axi_arready,
    output wire [C_AXI_ID_WIDTH-5:0]            s4_axi_rid,
    output wire [C_AXI_DATA_WIDTH-1:0]          s4_axi_rdata,
    output wire [1:0]                           s4_axi_rresp,
    output wire                                 s4_axi_rlast,
    output wire                                 s4_axi_rvalid,
    input  wire                                 s4_axi_rready,

    // slave port 5
    input  wire                                 s5_axi_clk,
    input  wire [C_AXI_ID_WIDTH-5:0]            s5_axi_awid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s5_axi_awaddr,
    input  wire [7:0]                           s5_axi_awlen,
    input  wire [2:0]                           s5_axi_awsize,
    input  wire [1:0]                           s5_axi_awburst,
    input  wire                                 s5_axi_awlock,
    input  wire [3:0]                           s5_axi_awcache,
    input  wire [2:0]                           s5_axi_awprot,
    input  wire [3:0]                           s5_axi_awqos,
    input  wire                                 s5_axi_awvalid,
    output wire                                 s5_axi_awready,
    input  wire [C_AXI_DATA_WIDTH-1:0]          s5_axi_wdata,
    input  wire [C_AXI_DATA_WIDTH/8-1:0]        s5_axi_wstrb,
    input  wire                                 s5_axi_wlast,
    input  wire                                 s5_axi_wvalid,
    output wire                                 s5_axi_wready,
    output wire [C_AXI_ID_WIDTH-5:0]            s5_axi_bid,
    output wire [1:0]                           s5_axi_bresp,
    output wire                                 s5_axi_bvalid,
    input  wire                                 s5_axi_bready,
    input  wire [C_AXI_ID_WIDTH-5:0]            s5_axi_arid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s5_axi_araddr,
    input  wire [7:0]                           s5_axi_arlen,
    input  wire [2:0]                           s5_axi_arsize,
    input  wire [1:0]                           s5_axi_arburst,
    input  wire                                 s5_axi_arlock,
    input  wire [3:0]                           s5_axi_arcache,
    input  wire [2:0]                           s5_axi_arprot,
    input  wire [3:0]                           s5_axi_arqos,
    input  wire                                 s5_axi_arvalid,
    output wire                                 s5_axi_arready,
    output wire [C_AXI_ID_WIDTH-5:0]            s5_axi_rid,
    output wire [C_AXI_DATA_WIDTH-1:0]          s5_axi_rdata,
    output wire [1:0]                           s5_axi_rresp,
    output wire                                 s5_axi_rlast,
    output wire                                 s5_axi_rvalid,
    input  wire                                 s5_axi_rready,

    // slave port 6
    input  wire                                 s6_axi_clk,
    input  wire [C_AXI_ID_WIDTH-5:0]            s6_axi_awid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s6_axi_awaddr,
    input  wire [7:0]                           s6_axi_awlen,
    input  wire [2:0]                           s6_axi_awsize,
    input  wire [1:0]                           s6_axi_awburst,
    input  wire                                 s6_axi_awlock,
    input  wire [3:0]                           s6_axi_awcache,
    input  wire [2:0]                           s6_axi_awprot,
    input  wire [3:0]                           s6_axi_awqos,
    input  wire                                 s6_axi_awvalid,
    output wire                                 s6_axi_awready,
    input  wire [C_AXI_DATA_WIDTH-1:0]          s6_axi_wdata,
    input  wire [C_AXI_DATA_WIDTH/8-1:0]        s6_axi_wstrb,
    input  wire                                 s6_axi_wlast,
    input  wire                                 s6_axi_wvalid,
    output wire                                 s6_axi_wready,
    output wire [C_AXI_ID_WIDTH-5:0]            s6_axi_bid,
    output wire [1:0]                           s6_axi_bresp,
    output wire                                 s6_axi_bvalid,
    input  wire                                 s6_axi_bready,
    input  wire [C_AXI_ID_WIDTH-5:0]            s6_axi_arid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s6_axi_araddr,
    input  wire [7:0]                           s6_axi_arlen,
    input  wire [2:0]                           s6_axi_arsize,
    input  wire [1:0]                           s6_axi_arburst,
    input  wire                                 s6_axi_arlock,
    input  wire [3:0]                           s6_axi_arcache,
    input  wire [2:0]                           s6_axi_arprot,
    input  wire [3:0]                           s6_axi_arqos,
    input  wire                                 s6_axi_arvalid,
    output wire                                 s6_axi_arready,
    output wire [C_AXI_ID_WIDTH-5:0]            s6_axi_rid,
    output wire [C_AXI_DATA_WIDTH-1:0]          s6_axi_rdata,
    output wire [1:0]                           s6_axi_rresp,
    output wire                                 s6_axi_rlast,
    output wire                                 s6_axi_rvalid,
    input  wire                                 s6_axi_rready,

    // slave port 7
    input  wire                                 s7_axi_clk,
    input  wire [C_AXI_ID_WIDTH-5:0]            s7_axi_awid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s7_axi_awaddr,
    input  wire [7:0]                           s7_axi_awlen,
    input  wire [2:0]                           s7_axi_awsize,
    input  wire [1:0]                           s7_axi_awburst,
    input  wire                                 s7_axi_awlock,
    input  wire [3:0]                           s7_axi_awcache,
    input  wire [2:0]                           s7_axi_awprot,
    input  wire [3:0]                           s7_axi_awqos,
    input  wire                                 s7_axi_awvalid,
    output wire                                 s7_axi_awready,
    input  wire [C_AXI_DATA_WIDTH-1:0]          s7_axi_wdata,
    input  wire [C_AXI_DATA_WIDTH/8-1:0]        s7_axi_wstrb,
    input  wire                                 s7_axi_wlast,
    input  wire                                 s7_axi_wvalid,
    output wire                                 s7_axi_wready,
    output wire [C_AXI_ID_WIDTH-5:0]            s7_axi_bid,
    output wire [1:0]                           s7_axi_bresp,
    output wire                                 s7_axi_bvalid,
    input  wire                                 s7_axi_bready,
    input  wire [C_AXI_ID_WIDTH-5:0]            s7_axi_arid,
    input  wire [C_AXI_ADDR_WIDTH-1:0]          s7_axi_araddr,
    input  wire [7:0]                           s7_axi_arlen,
    input  wire [2:0]                           s7_axi_arsize,
    input  wire [1:0]                           s7_axi_arburst,
    input  wire                                 s7_axi_arlock,
    input  wire [3:0]                           s7_axi_arcache,
    input  wire [2:0]                           s7_axi_arprot,
    input  wire [3:0]                           s7_axi_arqos,
    input  wire                                 s7_axi_arvalid,
    output wire                                 s7_axi_arready,
    output wire [C_AXI_ID_WIDTH-5:0]            s7_axi_rid,
    output wire [C_AXI_DATA_WIDTH-1:0]          s7_axi_rdata,
    output wire [1:0]                           s7_axi_rresp,
    output wire                                 s7_axi_rlast,
    output wire                                 s7_axi_rvalid,
    input  wire                                 s7_axi_rready,

   // Master Interface
    output wire [C_AXI_ID_WIDTH-1:0]            m_axi_awid,
    output wire [C_AXI_ADDR_WIDTH-1:0]          m_axi_awaddr,
    output wire [7:0]                           m_axi_awlen,
    output wire [2:0]                           m_axi_awsize,
    output wire [1:0]                           m_axi_awburst,
    output wire                                 m_axi_awlock,
    output wire [3:0]                           m_axi_awcache,
    output wire [2:0]                           m_axi_awprot,
    output wire [3:0]                           m_axi_awqos,
    output wire                                 m_axi_awvalid,
    input  wire                                 m_axi_awready,
    output wire [C_AXI_DATA_WIDTH-1:0]          m_axi_wdata,
    output wire [C_AXI_DATA_WIDTH/8-1:0]        m_axi_wstrb,
    output wire                                 m_axi_wlast,
    output wire                                 m_axi_wvalid,
    input  wire                                 m_axi_wready,
    input  wire [C_AXI_ID_WIDTH-1:0]            m_axi_bid,
    input  wire [1:0]                           m_axi_bresp,
    input  wire                                 m_axi_bvalid,
    output wire                                 m_axi_bready,
    output wire [C_AXI_ID_WIDTH-1:0]            m_axi_arid,
    output wire [C_AXI_ADDR_WIDTH-1:0]          m_axi_araddr,
    output wire [7:0]                           m_axi_arlen,
    output wire [2:0]                           m_axi_arsize,
    output wire [1:0]                           m_axi_arburst,
    output wire                                 m_axi_arlock,
    output wire [3:0]                           m_axi_arcache,
    output wire [2:0]                           m_axi_arprot,
    output wire [3:0]                           m_axi_arqos,
    output wire                                 m_axi_arvalid,
    input  wire                                 m_axi_arready,
    input  wire [C_AXI_ID_WIDTH-1:0]            m_axi_rid,
    input  wire [C_AXI_DATA_WIDTH-1:0]          m_axi_rdata,
    input  wire [1:0]                           m_axi_rresp,
    input  wire                                 m_axi_rlast,
    input  wire                                 m_axi_rvalid,
    output wire                                 m_axi_rready
    );
    
    ddr3_core_uif #(
        .C_AXI_ID_WIDTH(C_AXI_ID_WIDTH), 
        .C_AXI_ADDR_WIDTH(C_AXI_ADDR_WIDTH), 
        .C_AXI_DATA_WIDTH(C_AXI_DATA_WIDTH)
    ) ddr3_ui();

    assign ddr3_ui.clk = ddr3_ui_clk;
    assign ddr3_ui.rst = ddr3_ui_rst;

    assign m_axi_awid = ddr3_ui.awid;
    assign m_axi_awaddr = ddr3_ui.awaddr;
    assign m_axi_awlen = ddr3_ui.awlen;
    assign m_axi_awvalid = ddr3_ui.awvalid;
    assign m_axi_awsize = ddr3_ui.awsize;
    assign m_axi_awburst = ddr3_ui.awburst;
    assign m_axi_awlock = ddr3_ui.awlock;
    assign m_axi_awcache = ddr3_ui.awcache;
    assign m_axi_awprot = ddr3_ui.awprot;
    assign m_axi_awqos = ddr3_ui.awqos;
    assign m_axi_wdata = ddr3_ui.wdata;
    assign m_axi_wstrb = ddr3_ui.wstrb;
    assign m_axi_wlast = ddr3_ui.wlast;
    assign m_axi_wvalid = ddr3_ui.wvalid;
    assign m_axi_bready = ddr3_ui.bready;
    assign m_axi_arid = ddr3_ui.arid;
    assign m_axi_araddr = ddr3_ui.araddr;
    assign m_axi_arlen = ddr3_ui.arlen;
    assign m_axi_arvalid = ddr3_ui.arvalid;
    assign m_axi_arsize = ddr3_ui.arsize;
    assign m_axi_arburst = ddr3_ui.arburst;
    assign m_axi_arlock = ddr3_ui.arlock;
    assign m_axi_arcache = ddr3_ui.arcache;
    assign m_axi_arprot = ddr3_ui.arprot;
    assign m_axi_arqos = ddr3_ui.arqos;
    assign m_axi_rready = ddr3_ui.rready;

    assign ddr3_ui.awready = m_axi_awready;
    assign ddr3_ui.wready = m_axi_wready;
    assign ddr3_ui.bid = m_axi_bid;
    assign ddr3_ui.bresp = m_axi_bresp;
    assign ddr3_ui.bvalid = m_axi_bvalid;
    assign ddr3_ui.arready = m_axi_arready;
    assign ddr3_ui.rid = m_axi_rid;
    assign ddr3_ui.rdata = m_axi_rdata;
    assign ddr3_ui.rresp = m_axi_rresp;
    assign ddr3_ui.rlast = m_axi_rlast;
    assign ddr3_ui.rvalid = m_axi_rvalid;

    axi_if #(
        .C_AXI_ID_WIDTH(C_AXI_ID_WIDTH-4), 
        .C_AXI_ADDR_WIDTH(C_AXI_ADDR_WIDTH), 
        .C_AXI_DATA_WIDTH(C_AXI_DATA_WIDTH)
    ) ddr3_axi_masters [8:0] ();  // axi master interface
    
    localparam AXI_RD_REQ_WIDTH  = C_AXI_ID_WIDTH+C_AXI_ADDR_WIDTH+8;
    localparam AXI_RD_DATA_WIDTH = C_AXI_ID_WIDTH+C_AXI_DATA_WIDTH+2+1;
    localparam AXI_WR_REQ_WIDTH  = C_AXI_ID_WIDTH+C_AXI_ADDR_WIDTH+8;
    localparam AXI_WR_DATA_WIDTH = C_AXI_DATA_WIDTH+C_AXI_DATA_WIDTH/8+1;
    localparam AXI_WR_RESP_WIDTH = C_AXI_ID_WIDTH+2;

    pico_daisychain_a_if #(.ID_WIDTH(4), .DATA_WIDTH(AXI_RD_REQ_WIDTH))  axi_rd_req  [PICO_AXI_MASTERS:0] ();
    pico_daisychain_b_if #(.ID_WIDTH(4), .DATA_WIDTH(AXI_RD_DATA_WIDTH)) axi_rd_data [PICO_AXI_MASTERS:0] ();
    pico_daisychain_a_if #(.ID_WIDTH(4), .DATA_WIDTH(AXI_WR_REQ_WIDTH))  axi_wr_req  [PICO_AXI_MASTERS:0] ();
    pico_daisychain_b_if #(.ID_WIDTH(4), .DATA_WIDTH(AXI_WR_DATA_WIDTH)) axi_wr_data [PICO_AXI_MASTERS:0] ();
    pico_daisychain_b_if #(.ID_WIDTH(4), .DATA_WIDTH(AXI_WR_RESP_WIDTH)) axi_wr_resp [PICO_AXI_MASTERS:0] ();


    PicoAXISlave #(
        .MASTER_ID_WIDTH        (4),
        .C_AXI_ID_WIDTH         (C_AXI_ID_WIDTH),
        .C_AXI_ADDR_WIDTH       (C_AXI_ADDR_WIDTH),
        .C_AXI_DATA_WIDTH       (C_AXI_DATA_WIDTH)
    ) axi_slave (
        .clk            (clk),
        .rst            (rst),

        .slave          (ddr3_ui),

        .rd_req_in      (axi_rd_req[0]),
        .rd_req_out     (axi_rd_req[PICO_AXI_MASTERS]),
        .rd_data_out    (axi_rd_data[0]),
        .wr_req_in      (axi_wr_req[0]),
        .wr_req_out     (axi_wr_req[PICO_AXI_MASTERS]),
        .wr_data_in     (axi_wr_data[0]),
        .wr_data_out    (axi_wr_data[PICO_AXI_MASTERS]),
        .wr_resp_out    (axi_wr_resp[0])
    );

    genvar s;
    generate
        for (s=0; s<PICO_AXI_MASTERS; s++) begin: GEN_USER_AXI_MASTER
            PicoAXIMaster #(
                .MASTER_ID              (s+1),
                .MASTER_ID_WIDTH        (4),
                .C_AXI_ID_WIDTH         (C_AXI_ID_WIDTH),
                .C_AXI_ADDR_WIDTH       (C_AXI_ADDR_WIDTH),
                .C_AXI_DATA_WIDTH       (C_AXI_DATA_WIDTH)
            ) axi_master (
                .clk            (clk),
                .rst            (rst),

                .master         (ddr3_axi_masters[s]),

                .rd_req_in      (axi_rd_req[s+1]),
                .rd_req_out     (axi_rd_req[s]),
                .rd_data_in     (axi_rd_data[s]),
                .rd_data_out    (axi_rd_data[s+1]),
                .wr_req_in      (axi_wr_req[s+1]),
                .wr_req_out     (axi_wr_req[s]),
                .wr_data_in     (axi_wr_data[s+1]),
                .wr_data_out    (axi_wr_data[s]),
                .wr_resp_in     (axi_wr_resp[s]),
                .wr_resp_out    (axi_wr_resp[s+1])
            );
        end
    endgenerate

    // MASTER 0
    assign ddr3_axi_masters[0].clk = s0_axi_clk;

    assign ddr3_axi_masters[0].awid = s0_axi_awid;
    assign ddr3_axi_masters[0].awaddr = s0_axi_awaddr;
    assign ddr3_axi_masters[0].awlen = s0_axi_awlen;
    assign ddr3_axi_masters[0].awvalid = s0_axi_awvalid;
    assign ddr3_axi_masters[0].awsize = s0_axi_awsize;
    assign ddr3_axi_masters[0].awburst = s0_axi_awburst;
    assign ddr3_axi_masters[0].awlock = s0_axi_awlock;
    assign ddr3_axi_masters[0].awcache = s0_axi_awcache;
    assign ddr3_axi_masters[0].awprot = s0_axi_awprot;
    assign ddr3_axi_masters[0].awqos = s0_axi_awqos;
    assign ddr3_axi_masters[0].wdata = s0_axi_wdata;
    assign ddr3_axi_masters[0].wstrb = s0_axi_wstrb;
    assign ddr3_axi_masters[0].wlast = s0_axi_wlast;
    assign ddr3_axi_masters[0].wvalid = s0_axi_wvalid;
    assign ddr3_axi_masters[0].bready = s0_axi_bready;
    assign ddr3_axi_masters[0].arid = s0_axi_arid;
    assign ddr3_axi_masters[0].araddr = s0_axi_araddr;
    assign ddr3_axi_masters[0].arlen = s0_axi_arlen;
    assign ddr3_axi_masters[0].arvalid = s0_axi_arvalid;
    assign ddr3_axi_masters[0].arsize = s0_axi_arsize;
    assign ddr3_axi_masters[0].arburst = s0_axi_arburst;
    assign ddr3_axi_masters[0].arlock = s0_axi_arlock;
    assign ddr3_axi_masters[0].arcache = s0_axi_arcache;
    assign ddr3_axi_masters[0].arprot = s0_axi_arprot;
    assign ddr3_axi_masters[0].arqos = s0_axi_arqos;
    assign ddr3_axi_masters[0].rready = s0_axi_rready;

    assign s0_axi_awready = ddr3_axi_masters[0].awready;
    assign s0_axi_wready = ddr3_axi_masters[0].wready;
    assign s0_axi_bid = ddr3_axi_masters[0].bid;
    assign s0_axi_bresp = ddr3_axi_masters[0].bresp;
    assign s0_axi_bvalid = ddr3_axi_masters[0].bvalid;
    assign s0_axi_arready = ddr3_axi_masters[0].arready;
    assign s0_axi_rid = ddr3_axi_masters[0].rid;
    assign s0_axi_rdata = ddr3_axi_masters[0].rdata;
    assign s0_axi_rresp = ddr3_axi_masters[0].rresp;
    assign s0_axi_rlast = ddr3_axi_masters[0].rlast;
    assign s0_axi_rvalid = ddr3_axi_masters[0].rvalid;
    // MASTER 1
    assign ddr3_axi_masters[1].clk = s1_axi_clk;

    assign ddr3_axi_masters[1].awid = s1_axi_awid;
    assign ddr3_axi_masters[1].awaddr = s1_axi_awaddr;
    assign ddr3_axi_masters[1].awlen = s1_axi_awlen;
    assign ddr3_axi_masters[1].awvalid = s1_axi_awvalid;
    assign ddr3_axi_masters[1].awsize = s1_axi_awsize;
    assign ddr3_axi_masters[1].awburst = s1_axi_awburst;
    assign ddr3_axi_masters[1].awlock = s1_axi_awlock;
    assign ddr3_axi_masters[1].awcache = s1_axi_awcache;
    assign ddr3_axi_masters[1].awprot = s1_axi_awprot;
    assign ddr3_axi_masters[1].awqos = s1_axi_awqos;
    assign ddr3_axi_masters[1].wdata = s1_axi_wdata;
    assign ddr3_axi_masters[1].wstrb = s1_axi_wstrb;
    assign ddr3_axi_masters[1].wlast = s1_axi_wlast;
    assign ddr3_axi_masters[1].wvalid = s1_axi_wvalid;
    assign ddr3_axi_masters[1].bready = s1_axi_bready;
    assign ddr3_axi_masters[1].arid = s1_axi_arid;
    assign ddr3_axi_masters[1].araddr = s1_axi_araddr;
    assign ddr3_axi_masters[1].arlen = s1_axi_arlen;
    assign ddr3_axi_masters[1].arvalid = s1_axi_arvalid;
    assign ddr3_axi_masters[1].arsize = s1_axi_arsize;
    assign ddr3_axi_masters[1].arburst = s1_axi_arburst;
    assign ddr3_axi_masters[1].arlock = s1_axi_arlock;
    assign ddr3_axi_masters[1].arcache = s1_axi_arcache;
    assign ddr3_axi_masters[1].arprot = s1_axi_arprot;
    assign ddr3_axi_masters[1].arqos = s1_axi_arqos;
    assign ddr3_axi_masters[1].rready = s1_axi_rready;

    assign s1_axi_awready = ddr3_axi_masters[1].awready;
    assign s1_axi_wready = ddr3_axi_masters[1].wready;
    assign s1_axi_bid = ddr3_axi_masters[1].bid;
    assign s1_axi_bresp = ddr3_axi_masters[1].bresp;
    assign s1_axi_bvalid = ddr3_axi_masters[1].bvalid;
    assign s1_axi_arready = ddr3_axi_masters[1].arready;
    assign s1_axi_rid = ddr3_axi_masters[1].rid;
    assign s1_axi_rdata = ddr3_axi_masters[1].rdata;
    assign s1_axi_rresp = ddr3_axi_masters[1].rresp;
    assign s1_axi_rlast = ddr3_axi_masters[1].rlast;
    assign s1_axi_rvalid = ddr3_axi_masters[1].rvalid;
    // MASTER 2
    assign ddr3_axi_masters[2].clk = s2_axi_clk;

    assign ddr3_axi_masters[2].awid = s2_axi_awid;
    assign ddr3_axi_masters[2].awaddr = s2_axi_awaddr;
    assign ddr3_axi_masters[2].awlen = s2_axi_awlen;
    assign ddr3_axi_masters[2].awvalid = s2_axi_awvalid;
    assign ddr3_axi_masters[2].awsize = s2_axi_awsize;
    assign ddr3_axi_masters[2].awburst = s2_axi_awburst;
    assign ddr3_axi_masters[2].awlock = s2_axi_awlock;
    assign ddr3_axi_masters[2].awcache = s2_axi_awcache;
    assign ddr3_axi_masters[2].awprot = s2_axi_awprot;
    assign ddr3_axi_masters[2].awqos = s2_axi_awqos;
    assign ddr3_axi_masters[2].wdata = s2_axi_wdata;
    assign ddr3_axi_masters[2].wstrb = s2_axi_wstrb;
    assign ddr3_axi_masters[2].wlast = s2_axi_wlast;
    assign ddr3_axi_masters[2].wvalid = s2_axi_wvalid;
    assign ddr3_axi_masters[2].bready = s2_axi_bready;
    assign ddr3_axi_masters[2].arid = s2_axi_arid;
    assign ddr3_axi_masters[2].araddr = s2_axi_araddr;
    assign ddr3_axi_masters[2].arlen = s2_axi_arlen;
    assign ddr3_axi_masters[2].arvalid = s2_axi_arvalid;
    assign ddr3_axi_masters[2].arsize = s2_axi_arsize;
    assign ddr3_axi_masters[2].arburst = s2_axi_arburst;
    assign ddr3_axi_masters[2].arlock = s2_axi_arlock;
    assign ddr3_axi_masters[2].arcache = s2_axi_arcache;
    assign ddr3_axi_masters[2].arprot = s2_axi_arprot;
    assign ddr3_axi_masters[2].arqos = s2_axi_arqos;
    assign ddr3_axi_masters[2].rready = s2_axi_rready;

    assign s2_axi_awready = ddr3_axi_masters[2].awready;
    assign s2_axi_wready = ddr3_axi_masters[2].wready;
    assign s2_axi_bid = ddr3_axi_masters[2].bid;
    assign s2_axi_bresp = ddr3_axi_masters[2].bresp;
    assign s2_axi_bvalid = ddr3_axi_masters[2].bvalid;
    assign s2_axi_arready = ddr3_axi_masters[2].arready;
    assign s2_axi_rid = ddr3_axi_masters[2].rid;
    assign s2_axi_rdata = ddr3_axi_masters[2].rdata;
    assign s2_axi_rresp = ddr3_axi_masters[2].rresp;
    assign s2_axi_rlast = ddr3_axi_masters[2].rlast;
    assign s2_axi_rvalid = ddr3_axi_masters[2].rvalid;
    // MASTER 3
    assign ddr3_axi_masters[3].clk = s3_axi_clk;

    assign ddr3_axi_masters[3].awid = s3_axi_awid;
    assign ddr3_axi_masters[3].awaddr = s3_axi_awaddr;
    assign ddr3_axi_masters[3].awlen = s3_axi_awlen;
    assign ddr3_axi_masters[3].awvalid = s3_axi_awvalid;
    assign ddr3_axi_masters[3].awsize = s3_axi_awsize;
    assign ddr3_axi_masters[3].awburst = s3_axi_awburst;
    assign ddr3_axi_masters[3].awlock = s3_axi_awlock;
    assign ddr3_axi_masters[3].awcache = s3_axi_awcache;
    assign ddr3_axi_masters[3].awprot = s3_axi_awprot;
    assign ddr3_axi_masters[3].awqos = s3_axi_awqos;
    assign ddr3_axi_masters[3].wdata = s3_axi_wdata;
    assign ddr3_axi_masters[3].wstrb = s3_axi_wstrb;
    assign ddr3_axi_masters[3].wlast = s3_axi_wlast;
    assign ddr3_axi_masters[3].wvalid = s3_axi_wvalid;
    assign ddr3_axi_masters[3].bready = s3_axi_bready;
    assign ddr3_axi_masters[3].arid = s3_axi_arid;
    assign ddr3_axi_masters[3].araddr = s3_axi_araddr;
    assign ddr3_axi_masters[3].arlen = s3_axi_arlen;
    assign ddr3_axi_masters[3].arvalid = s3_axi_arvalid;
    assign ddr3_axi_masters[3].arsize = s3_axi_arsize;
    assign ddr3_axi_masters[3].arburst = s3_axi_arburst;
    assign ddr3_axi_masters[3].arlock = s3_axi_arlock;
    assign ddr3_axi_masters[3].arcache = s3_axi_arcache;
    assign ddr3_axi_masters[3].arprot = s3_axi_arprot;
    assign ddr3_axi_masters[3].arqos = s3_axi_arqos;
    assign ddr3_axi_masters[3].rready = s3_axi_rready;

    assign s3_axi_awready = ddr3_axi_masters[3].awready;
    assign s3_axi_wready = ddr3_axi_masters[3].wready;
    assign s3_axi_bid = ddr3_axi_masters[3].bid;
    assign s3_axi_bresp = ddr3_axi_masters[3].bresp;
    assign s3_axi_bvalid = ddr3_axi_masters[3].bvalid;
    assign s3_axi_arready = ddr3_axi_masters[3].arready;
    assign s3_axi_rid = ddr3_axi_masters[3].rid;
    assign s3_axi_rdata = ddr3_axi_masters[3].rdata;
    assign s3_axi_rresp = ddr3_axi_masters[3].rresp;
    assign s3_axi_rlast = ddr3_axi_masters[3].rlast;
    assign s3_axi_rvalid = ddr3_axi_masters[3].rvalid;
    // MASTER 4
    assign ddr3_axi_masters[4].clk = s4_axi_clk;

    assign ddr3_axi_masters[4].awid = s4_axi_awid;
    assign ddr3_axi_masters[4].awaddr = s4_axi_awaddr;
    assign ddr3_axi_masters[4].awlen = s4_axi_awlen;
    assign ddr3_axi_masters[4].awvalid = s4_axi_awvalid;
    assign ddr3_axi_masters[4].awsize = s4_axi_awsize;
    assign ddr3_axi_masters[4].awburst = s4_axi_awburst;
    assign ddr3_axi_masters[4].awlock = s4_axi_awlock;
    assign ddr3_axi_masters[4].awcache = s4_axi_awcache;
    assign ddr3_axi_masters[4].awprot = s4_axi_awprot;
    assign ddr3_axi_masters[4].awqos = s4_axi_awqos;
    assign ddr3_axi_masters[4].wdata = s4_axi_wdata;
    assign ddr3_axi_masters[4].wstrb = s4_axi_wstrb;
    assign ddr3_axi_masters[4].wlast = s4_axi_wlast;
    assign ddr3_axi_masters[4].wvalid = s4_axi_wvalid;
    assign ddr3_axi_masters[4].bready = s4_axi_bready;
    assign ddr3_axi_masters[4].arid = s4_axi_arid;
    assign ddr3_axi_masters[4].araddr = s4_axi_araddr;
    assign ddr3_axi_masters[4].arlen = s4_axi_arlen;
    assign ddr3_axi_masters[4].arvalid = s4_axi_arvalid;
    assign ddr3_axi_masters[4].arsize = s4_axi_arsize;
    assign ddr3_axi_masters[4].arburst = s4_axi_arburst;
    assign ddr3_axi_masters[4].arlock = s4_axi_arlock;
    assign ddr3_axi_masters[4].arcache = s4_axi_arcache;
    assign ddr3_axi_masters[4].arprot = s4_axi_arprot;
    assign ddr3_axi_masters[4].arqos = s4_axi_arqos;
    assign ddr3_axi_masters[4].rready = s4_axi_rready;

    assign s4_axi_awready = ddr3_axi_masters[4].awready;
    assign s4_axi_wready = ddr3_axi_masters[4].wready;
    assign s4_axi_bid = ddr3_axi_masters[4].bid;
    assign s4_axi_bresp = ddr3_axi_masters[4].bresp;
    assign s4_axi_bvalid = ddr3_axi_masters[4].bvalid;
    assign s4_axi_arready = ddr3_axi_masters[4].arready;
    assign s4_axi_rid = ddr3_axi_masters[4].rid;
    assign s4_axi_rdata = ddr3_axi_masters[4].rdata;
    assign s4_axi_rresp = ddr3_axi_masters[4].rresp;
    assign s4_axi_rlast = ddr3_axi_masters[4].rlast;
    assign s4_axi_rvalid = ddr3_axi_masters[4].rvalid;
    // MASTER 5
    assign ddr3_axi_masters[5].clk = s5_axi_clk;

    assign ddr3_axi_masters[5].awid = s5_axi_awid;
    assign ddr3_axi_masters[5].awaddr = s5_axi_awaddr;
    assign ddr3_axi_masters[5].awlen = s5_axi_awlen;
    assign ddr3_axi_masters[5].awvalid = s5_axi_awvalid;
    assign ddr3_axi_masters[5].awsize = s5_axi_awsize;
    assign ddr3_axi_masters[5].awburst = s5_axi_awburst;
    assign ddr3_axi_masters[5].awlock = s5_axi_awlock;
    assign ddr3_axi_masters[5].awcache = s5_axi_awcache;
    assign ddr3_axi_masters[5].awprot = s5_axi_awprot;
    assign ddr3_axi_masters[5].awqos = s5_axi_awqos;
    assign ddr3_axi_masters[5].wdata = s5_axi_wdata;
    assign ddr3_axi_masters[5].wstrb = s5_axi_wstrb;
    assign ddr3_axi_masters[5].wlast = s5_axi_wlast;
    assign ddr3_axi_masters[5].wvalid = s5_axi_wvalid;
    assign ddr3_axi_masters[5].bready = s5_axi_bready;
    assign ddr3_axi_masters[5].arid = s5_axi_arid;
    assign ddr3_axi_masters[5].araddr = s5_axi_araddr;
    assign ddr3_axi_masters[5].arlen = s5_axi_arlen;
    assign ddr3_axi_masters[5].arvalid = s5_axi_arvalid;
    assign ddr3_axi_masters[5].arsize = s5_axi_arsize;
    assign ddr3_axi_masters[5].arburst = s5_axi_arburst;
    assign ddr3_axi_masters[5].arlock = s5_axi_arlock;
    assign ddr3_axi_masters[5].arcache = s5_axi_arcache;
    assign ddr3_axi_masters[5].arprot = s5_axi_arprot;
    assign ddr3_axi_masters[5].arqos = s5_axi_arqos;
    assign ddr3_axi_masters[5].rready = s5_axi_rready;

    assign s5_axi_awready = ddr3_axi_masters[5].awready;
    assign s5_axi_wready = ddr3_axi_masters[5].wready;
    assign s5_axi_bid = ddr3_axi_masters[5].bid;
    assign s5_axi_bresp = ddr3_axi_masters[5].bresp;
    assign s5_axi_bvalid = ddr3_axi_masters[5].bvalid;
    assign s5_axi_arready = ddr3_axi_masters[5].arready;
    assign s5_axi_rid = ddr3_axi_masters[5].rid;
    assign s5_axi_rdata = ddr3_axi_masters[5].rdata;
    assign s5_axi_rresp = ddr3_axi_masters[5].rresp;
    assign s5_axi_rlast = ddr3_axi_masters[5].rlast;
    assign s5_axi_rvalid = ddr3_axi_masters[5].rvalid;
    // MASTER 6
    assign ddr3_axi_masters[6].clk = s6_axi_clk;

    assign ddr3_axi_masters[6].awid = s6_axi_awid;
    assign ddr3_axi_masters[6].awaddr = s6_axi_awaddr;
    assign ddr3_axi_masters[6].awlen = s6_axi_awlen;
    assign ddr3_axi_masters[6].awvalid = s6_axi_awvalid;
    assign ddr3_axi_masters[6].awsize = s6_axi_awsize;
    assign ddr3_axi_masters[6].awburst = s6_axi_awburst;
    assign ddr3_axi_masters[6].awlock = s6_axi_awlock;
    assign ddr3_axi_masters[6].awcache = s6_axi_awcache;
    assign ddr3_axi_masters[6].awprot = s6_axi_awprot;
    assign ddr3_axi_masters[6].awqos = s6_axi_awqos;
    assign ddr3_axi_masters[6].wdata = s6_axi_wdata;
    assign ddr3_axi_masters[6].wstrb = s6_axi_wstrb;
    assign ddr3_axi_masters[6].wlast = s6_axi_wlast;
    assign ddr3_axi_masters[6].wvalid = s6_axi_wvalid;
    assign ddr3_axi_masters[6].bready = s6_axi_bready;
    assign ddr3_axi_masters[6].arid = s6_axi_arid;
    assign ddr3_axi_masters[6].araddr = s6_axi_araddr;
    assign ddr3_axi_masters[6].arlen = s6_axi_arlen;
    assign ddr3_axi_masters[6].arvalid = s6_axi_arvalid;
    assign ddr3_axi_masters[6].arsize = s6_axi_arsize;
    assign ddr3_axi_masters[6].arburst = s6_axi_arburst;
    assign ddr3_axi_masters[6].arlock = s6_axi_arlock;
    assign ddr3_axi_masters[6].arcache = s6_axi_arcache;
    assign ddr3_axi_masters[6].arprot = s6_axi_arprot;
    assign ddr3_axi_masters[6].arqos = s6_axi_arqos;
    assign ddr3_axi_masters[6].rready = s6_axi_rready;

    assign s6_axi_awready = ddr3_axi_masters[6].awready;
    assign s6_axi_wready = ddr3_axi_masters[6].wready;
    assign s6_axi_bid = ddr3_axi_masters[6].bid;
    assign s6_axi_bresp = ddr3_axi_masters[6].bresp;
    assign s6_axi_bvalid = ddr3_axi_masters[6].bvalid;
    assign s6_axi_arready = ddr3_axi_masters[6].arready;
    assign s6_axi_rid = ddr3_axi_masters[6].rid;
    assign s6_axi_rdata = ddr3_axi_masters[6].rdata;
    assign s6_axi_rresp = ddr3_axi_masters[6].rresp;
    assign s6_axi_rlast = ddr3_axi_masters[6].rlast;
    assign s6_axi_rvalid = ddr3_axi_masters[6].rvalid;
    // MASTER 7
    assign ddr3_axi_masters[7].clk = s7_axi_clk;

    assign ddr3_axi_masters[7].awid = s7_axi_awid;
    assign ddr3_axi_masters[7].awaddr = s7_axi_awaddr;
    assign ddr3_axi_masters[7].awlen = s7_axi_awlen;
    assign ddr3_axi_masters[7].awvalid = s7_axi_awvalid;
    assign ddr3_axi_masters[7].awsize = s7_axi_awsize;
    assign ddr3_axi_masters[7].awburst = s7_axi_awburst;
    assign ddr3_axi_masters[7].awlock = s7_axi_awlock;
    assign ddr3_axi_masters[7].awcache = s7_axi_awcache;
    assign ddr3_axi_masters[7].awprot = s7_axi_awprot;
    assign ddr3_axi_masters[7].awqos = s7_axi_awqos;
    assign ddr3_axi_masters[7].wdata = s7_axi_wdata;
    assign ddr3_axi_masters[7].wstrb = s7_axi_wstrb;
    assign ddr3_axi_masters[7].wlast = s7_axi_wlast;
    assign ddr3_axi_masters[7].wvalid = s7_axi_wvalid;
    assign ddr3_axi_masters[7].bready = s7_axi_bready;
    assign ddr3_axi_masters[7].arid = s7_axi_arid;
    assign ddr3_axi_masters[7].araddr = s7_axi_araddr;
    assign ddr3_axi_masters[7].arlen = s7_axi_arlen;
    assign ddr3_axi_masters[7].arvalid = s7_axi_arvalid;
    assign ddr3_axi_masters[7].arsize = s7_axi_arsize;
    assign ddr3_axi_masters[7].arburst = s7_axi_arburst;
    assign ddr3_axi_masters[7].arlock = s7_axi_arlock;
    assign ddr3_axi_masters[7].arcache = s7_axi_arcache;
    assign ddr3_axi_masters[7].arprot = s7_axi_arprot;
    assign ddr3_axi_masters[7].arqos = s7_axi_arqos;
    assign ddr3_axi_masters[7].rready = s7_axi_rready;

    assign s7_axi_awready = ddr3_axi_masters[7].awready;
    assign s7_axi_wready = ddr3_axi_masters[7].wready;
    assign s7_axi_bid = ddr3_axi_masters[7].bid;
    assign s7_axi_bresp = ddr3_axi_masters[7].bresp;
    assign s7_axi_bvalid = ddr3_axi_masters[7].bvalid;
    assign s7_axi_arready = ddr3_axi_masters[7].arready;
    assign s7_axi_rid = ddr3_axi_masters[7].rid;
    assign s7_axi_rdata = ddr3_axi_masters[7].rdata;
    assign s7_axi_rresp = ddr3_axi_masters[7].rresp;
    assign s7_axi_rlast = ddr3_axi_masters[7].rlast;
    assign s7_axi_rvalid = ddr3_axi_masters[7].rvalid;
endmodule



