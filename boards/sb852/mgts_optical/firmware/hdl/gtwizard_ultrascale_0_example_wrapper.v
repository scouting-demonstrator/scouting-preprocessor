//------------------------------------------------------------------------------
//  (c) Copyright 2013-2018 Xilinx, Inc. All rights reserved.
//
//  This file contains confidential and proprietary information
//  of Xilinx, Inc. and is protected under U.S. and
//  international copyright and other intellectual property
//  laws.
//
//  DISCLAIMER
//  This disclaimer is not a license and does not grant any
//  rights to the materials distributed herewith. Except as
//  otherwise provided in a valid license issued to you by
//  Xilinx, and to the maximum extent permitted by applicable
//  law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
//  WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
//  AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
//  BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
//  INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
//  (2) Xilinx shall not be liable (whether in contract or tort,
//  including negligence, or under any other theory of
//  liability) for any loss or damage of any kind or nature
//  related to, arising under or in connection with these
//  materials, including for any direct, or any indirect,
//  special, incidental, or consequential loss or damage
//  (including loss of data, profits, goodwill, or any type of
//  loss or damage suffered as a result of any action brought
//  by a third party) even if such damage or loss was
//  reasonably foreseeable or Xilinx had been advised of the
//  possibility of the same.
//
//  CRITICAL APPLICATIONS
//  Xilinx products are not designed or intended to be fail-
//  safe, or for use in any application requiring fail-safe
//  performance, such as life-support or safety devices or
//  systems, Class III medical devices, nuclear facilities,
//  applications related to the deployment of airbags, or any
//  other applications that could lead to death, personal
//  injury, or severe property or environmental damage
//  (individually and collectively, "Critical
//  Applications"). Customer assumes the sole risk and
//  liability of any use of Xilinx products in Critical
//  Applications, subject only to applicable laws and
//  regulations governing limitations on product liability.
//
//  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
//  PART OF THIS FILE AT ALL TIMES.
//------------------------------------------------------------------------------


`timescale 1ps/1ps

// =====================================================================================================================
// This example design wrapper module instantiates the core and any helper blocks which the user chose to exclude from
// the core, connects them as appropriate, and maps enabled ports
// =====================================================================================================================

module gtwizard_ultrascale_0_example_wrapper (
  input  wire [7:0] gtyrxn_in
 ,input  wire [7:0] gtyrxp_in
 ,output wire [7:0] gtytxn_out
 ,output wire [7:0] gtytxp_out
 ,input  wire [0:0] gtwiz_userclk_tx_reset_in
 ,output wire [0:0] gtwiz_userclk_tx_srcclk_out
 ,output wire [0:0] gtwiz_userclk_tx_usrclk_out
 ,output wire [0:0] gtwiz_userclk_tx_usrclk2_out
 ,output wire [0:0] gtwiz_userclk_tx_active_out
 ,input  wire [0:0] gtwiz_userclk_rx_reset_in
 ,output wire [0:0] gtwiz_userclk_rx_srcclk_out
 ,output wire [0:0] gtwiz_userclk_rx_usrclk_out
 ,output wire [0:0] gtwiz_userclk_rx_usrclk2_out
 ,output wire [0:0] gtwiz_userclk_rx_active_out
 ,input  wire [0:0] gtwiz_reset_clk_freerun_in
 ,input  wire [0:0] gtwiz_reset_all_in
 ,input  wire [0:0] gtwiz_reset_tx_pll_and_datapath_in
 ,input  wire [0:0] gtwiz_reset_tx_datapath_in
 ,input  wire [0:0] gtwiz_reset_rx_pll_and_datapath_in
 ,input  wire [0:0] gtwiz_reset_rx_datapath_in
 ,output wire [0:0] gtwiz_reset_rx_cdr_stable_out
 ,output wire [0:0] gtwiz_reset_tx_done_out
 ,output wire [0:0] gtwiz_reset_rx_done_out
 ,input  wire [255:0] gtwiz_userdata_tx_in
 ,output wire [255:0] gtwiz_userdata_rx_out
 ,input  wire [1:0] bgbypassb_in
 ,input  wire [1:0] bgmonitorenb_in
 ,input  wire [1:0] bgpdb_in
 ,input  wire [9:0] bgrcalovrd_in
 ,input  wire [1:0] bgrcalovrdenb_in
 ,input  wire [1:0] gtnorthrefclk00_in
 ,input  wire [1:0] gtnorthrefclk01_in
 ,input  wire [1:0] gtnorthrefclk10_in
 ,input  wire [1:0] gtnorthrefclk11_in
 ,input  wire [1:0] gtrefclk00_in
 ,input  wire [1:0] gtrefclk01_in
 ,input  wire [1:0] gtrefclk10_in
 ,input  wire [1:0] gtrefclk11_in
 ,input  wire [1:0] gtsouthrefclk00_in
 ,input  wire [1:0] gtsouthrefclk01_in
 ,input  wire [1:0] gtsouthrefclk10_in
 ,input  wire [1:0] gtsouthrefclk11_in
 ,input  wire [15:0] pmarsvd0_in
 ,input  wire [15:0] pmarsvd1_in
 ,input  wire [1:0] qpll0clkrsvd0_in
 ,input  wire [1:0] qpll0clkrsvd1_in
 ,input  wire [1:0] qpll0lockdetclk_in
 ,input  wire [1:0] qpll0locken_in
 ,input  wire [1:0] qpll0pd_in
 ,input  wire [5:0] qpll0refclksel_in
 ,input  wire [1:0] qpll1clkrsvd0_in
 ,input  wire [1:0] qpll1clkrsvd1_in
 ,input  wire [1:0] qpll1lockdetclk_in
 ,input  wire [1:0] qpll1locken_in
 ,input  wire [1:0] qpll1pd_in
 ,input  wire [5:0] qpll1refclksel_in
 ,input  wire [15:0] qpllrsvd1_in
 ,input  wire [9:0] qpllrsvd2_in
 ,input  wire [9:0] qpllrsvd3_in
 ,input  wire [15:0] qpllrsvd4_in
 ,input  wire [1:0] rcalenb_in
 ,input  wire [49:0] sdm0data_in
 ,input  wire [1:0] sdm0reset_in
 ,input  wire [3:0] sdm0width_in
 ,input  wire [49:0] sdm1data_in
 ,input  wire [1:0] sdm1reset_in
 ,input  wire [3:0] sdm1width_in
 ,output wire [1:0] qpll0fbclklost_out
 ,output wire [1:0] qpll0lock_out
 ,output wire [1:0] qpll0outclk_out
 ,output wire [1:0] qpll0outrefclk_out
 ,output wire [1:0] qpll0refclklost_out
 ,output wire [1:0] qpll1fbclklost_out
 ,output wire [1:0] qpll1lock_out
 ,output wire [1:0] qpll1outclk_out
 ,output wire [1:0] qpll1outrefclk_out
 ,output wire [1:0] qpll1refclklost_out
 ,output wire [15:0] qplldmonitor0_out
 ,output wire [15:0] qplldmonitor1_out
 ,output wire [1:0] refclkoutmonitor0_out
 ,output wire [1:0] refclkoutmonitor1_out
 ,input  wire [7:0] cfgreset_in
 ,input  wire [7:0] cplllockdetclk_in
 ,input  wire [7:0] cplllocken_in
 ,input  wire [23:0] cpllrefclksel_in
 ,input  wire [7:0] cpllreset_in
 ,input  wire [7:0] eyescanreset_in
 ,input  wire [7:0] gtgrefclk_in
 ,input  wire [7:0] gtnorthrefclk0_in
 ,input  wire [7:0] gtnorthrefclk1_in
 ,input  wire [7:0] gtrefclk0_in
 ,input  wire [7:0] gtrefclk1_in
 ,input  wire [7:0] gtsouthrefclk0_in
 ,input  wire [7:0] gtsouthrefclk1_in
 ,input  wire [7:0] resetovrd_in
 ,input  wire [7:0] rx8b10ben_in
 ,input  wire [7:0] rxbufreset_in
 ,input  wire [7:0] rxcdrfreqreset_in
 ,input  wire [7:0] rxcdrreset_in
 ,input  wire [7:0] rxcommadeten_in
 ,input  wire [7:0] rxdfelpmreset_in
 ,input  wire [7:0] rxdlybypass_in
 ,input  wire [7:0] rxgearboxslip_in
 ,input  wire [7:0] rxlatclk_in
 ,input  wire [7:0] rxmcommaalignen_in
 ,input  wire [7:0] rxoobreset_in
 ,input  wire [7:0] rxoscalreset_in
 ,input  wire [23:0] rxoutclksel_in
 ,input  wire [7:0] rxpcommaalignen_in
 ,input  wire [7:0] rxpcsreset_in
 ,input  wire [15:0] rxpllclksel_in
 ,input  wire [7:0] rxpmareset_in
 ,input  wire [23:0] rxrate_in
 ,input  wire [7:0] rxratemode_in
 ,input  wire [7:0] rxslide_in
 ,input  wire [15:0] rxsysclksel_in
 ,input  wire [7:0] tx8b10ben_in
 ,input  wire [127:0] txctrl0_in
 ,input  wire [127:0] txctrl1_in
 ,input  wire [63:0] txctrl2_in
 ,input  wire [7:0] txpcsreset_in
 ,input  wire [15:0] txpllclksel_in
 ,input  wire [7:0] txpmareset_in
 ,input  wire [15:0] txsysclksel_in
 ,output wire [7:0] cpllfbclklost_out
 ,output wire [7:0] cplllock_out
 ,output wire [7:0] cpllrefclklost_out
 ,output wire [7:0] gtpowergood_out
 ,output wire [7:0] gtrefclkmonitor_out
 ,output wire [127:0] pcsrsvdout_out
 ,output wire [7:0] resetexception_out
 ,output wire [23:0] rxbufstatus_out
 ,output wire [7:0] rxbyteisaligned_out
 ,output wire [7:0] rxbyterealign_out
 ,output wire [7:0] rxcommadet_out
 ,output wire [127:0] rxctrl0_out
 ,output wire [127:0] rxctrl1_out
 ,output wire [63:0] rxctrl2_out
 ,output wire [63:0] rxctrl3_out
 ,output wire [1023:0] rxdata_out
 ,output wire [63:0] rxdataextendrsvd_out
 ,output wire [15:0] rxdatavalid_out
 ,output wire [47:0] rxheader_out
 ,output wire [15:0] rxheadervalid_out
 ,output wire [7:0] rxosintdone_out
 ,output wire [7:0] rxoutclkfabric_out
 ,output wire [7:0] rxoutclkpcs_out
 ,output wire [7:0] rxpmaresetdone_out
 ,output wire [7:0] rxprgdivresetdone_out
 ,output wire [7:0] rxratedone_out
 ,output wire [7:0] rxresetdone_out
 ,output wire [15:0] rxstartofseq_out
 ,output wire [7:0] txpmaresetdone_out
 ,output wire [7:0] txresetdone_out
);


  // ===================================================================================================================
  // PARAMETERS AND FUNCTIONS
  // ===================================================================================================================

  // Declare and initialize local parameters and functions used for HDL generation
  //localparam [191:0] P_CHANNEL_ENABLE = 192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011110000111100000000;
  `include "gtwizard_ultrascale_0_example_wrapper_functions.v"
  localparam integer P_TX_MASTER_CH_PACKED_IDX = f_calc_pk_mc_idx(19);
  localparam integer P_RX_MASTER_CH_PACKED_IDX = f_calc_pk_mc_idx(19);


  // ===================================================================================================================
  // HELPER BLOCKS
  // ===================================================================================================================

  // Any helper blocks which the user chose to exclude from the core will appear below. In addition, some signal
  // assignments related to optionally-enabled ports may appear below.

  // -------------------------------------------------------------------------------------------------------------------
  // Transmitter user clocking network helper block
  // -------------------------------------------------------------------------------------------------------------------

  wire [7:0] txusrclk_int;
  wire [7:0] txusrclk2_int;
  wire [7:0] txoutclk_int;

  // Generate a single module instance which is driven by a clock source associated with the master transmitter channel,
  // and which drives TXUSRCLK and TXUSRCLK2 for all channels

  // The source clock is TXOUTCLK from the master transmitter channel
  assign gtwiz_userclk_tx_srcclk_out = txoutclk_int[P_TX_MASTER_CH_PACKED_IDX];

  // Instantiate a single instance of the transmitter user clocking network helper block
  gtwizard_ultrascale_0_example_gtwiz_userclk_tx gtwiz_userclk_tx_inst (
    .gtwiz_userclk_tx_srcclk_in   (gtwiz_userclk_tx_srcclk_out),
    .gtwiz_userclk_tx_reset_in    (gtwiz_userclk_tx_reset_in),
    .gtwiz_userclk_tx_usrclk_out  (gtwiz_userclk_tx_usrclk_out),
    .gtwiz_userclk_tx_usrclk2_out (gtwiz_userclk_tx_usrclk2_out),
    .gtwiz_userclk_tx_active_out  (gtwiz_userclk_tx_active_out)
  );

  // Drive TXUSRCLK and TXUSRCLK2 for all channels with the respective helper block outputs
  assign txusrclk_int  = {8{gtwiz_userclk_tx_usrclk_out}};
  assign txusrclk2_int = {8{gtwiz_userclk_tx_usrclk2_out}};

  // -------------------------------------------------------------------------------------------------------------------
  // Receiver user clocking network helper block
  // -------------------------------------------------------------------------------------------------------------------

  wire [7:0] rxusrclk_int;
  wire [7:0] rxusrclk2_int;
  wire [7:0] rxoutclk_int;

  // Generate a single module instance which is driven by a clock source associated with the master receiver channel,
  // and which drives RXUSRCLK and RXUSRCLK2 for all channels

  // The source clock is RXOUTCLK from the master receiver channel
  assign gtwiz_userclk_rx_srcclk_out = rxoutclk_int[P_RX_MASTER_CH_PACKED_IDX];

  // Instantiate a single instance of the receiver user clocking network helper block
  gtwizard_ultrascale_0_example_gtwiz_userclk_rx gtwiz_userclk_rx_inst (
    .gtwiz_userclk_rx_srcclk_in   (gtwiz_userclk_rx_srcclk_out),
    .gtwiz_userclk_rx_reset_in    (gtwiz_userclk_rx_reset_in),
    .gtwiz_userclk_rx_usrclk_out  (gtwiz_userclk_rx_usrclk_out),
    .gtwiz_userclk_rx_usrclk2_out (gtwiz_userclk_rx_usrclk2_out),
    .gtwiz_userclk_rx_active_out  (gtwiz_userclk_rx_active_out)
  );

  // Drive RXUSRCLK and RXUSRCLK2 for all channels with the respective helper block outputs
  assign rxusrclk_int  = {8{gtwiz_userclk_rx_usrclk_out}};
  assign rxusrclk2_int = {8{gtwiz_userclk_rx_usrclk2_out}};

  wire [7:0] rxdlybypass_int;

  // Required assignment to expose the RXDLYBYPASS port per user request
  assign rxdlybypass_int = rxdlybypass_in;
  wire [1:0] qpll1reset_int;

  // Required assignment to expose the QPLL1RESET port per user request
  assign qpll1reset_int = {2{1'b1}};
  wire [7:0] cpllpd_int;

  // Required assignment to expose the CPLLPD port per user request
  assign cpllpd_int = {8{1'b1}};
  wire [7:0] gtpowergood_int;

  // Required assignment to expose the GTPOWERGOOD port per user request
  assign gtpowergood_out = gtpowergood_int;
  wire [7:0] txresetdone_int;

  // Required assignment to expose the TXRESETDONE port per user request
  assign txresetdone_out = txresetdone_int;
  wire [7:0] rxresetdone_int;

  // Required assignment to expose the RXRESETDONE port per user request
  assign rxresetdone_out = rxresetdone_int;
  wire [1:0] qpll0lock_int;

  // Required assignment to expose the QPLL0LOCK port per user request
  assign qpll0lock_out = qpll0lock_int;
  wire [1:0] qpll1lock_int;

  // Required assignment to expose the QPLL1LOCK port per user request
  assign qpll1lock_out = qpll1lock_int;
  wire [7:0] cplllock_int;

  // Required assignment to expose the CPLLLOCK port per user request
  assign cplllock_out = cplllock_int;

  // ----------------------------------------------------------------------------------------------------------------
  // Assignments to expose data ports, or data control ports, per configuration requirement or user request
  // ----------------------------------------------------------------------------------------------------------------

  wire [127:0] txctrl0_int;

  // Required assignment to expose the TXCTRL0 port per configuration requirement or user request
  assign txctrl0_int = txctrl0_in;
  wire [127:0] txctrl1_int;

  // Required assignment to expose the TXCTRL1 port per configuration requirement or user request
  assign txctrl1_int = txctrl1_in;
  wire [1023:0] rxdata_int;

  // Required assignment to expose the RXDATA port per user request
  assign rxdata_out = rxdata_int;
  wire [127:0] rxctrl0_int;

  // Required assignment to expose the RXCTRL0 port per configuration requirement or user request
  assign rxctrl0_out = rxctrl0_int;
  wire [127:0] rxctrl1_int;

  // Required assignment to expose the RXCTRL1 port per configuration requirement or user request
  assign rxctrl1_out = rxctrl1_int;


  // ===================================================================================================================
  // CORE INSTANCE
  // ===================================================================================================================

  // Instantiate the core, mapping its enabled ports to example design ports and helper blocks as appropriate
  gtwizard_ultrascale_QUAD_X0Y2_X0Y4 gtwizard_ultrascale_0_inst (
    .gtyrxn_in                               (gtyrxn_in)
   ,.gtyrxp_in                               (gtyrxp_in)
   ,.gtytxn_out                              (gtytxn_out)
   ,.gtytxp_out                              (gtytxp_out)
   ,.gtwiz_userclk_tx_active_in              (gtwiz_userclk_tx_active_out)
   ,.gtwiz_userclk_rx_active_in              (gtwiz_userclk_rx_active_out)
   ,.gtwiz_reset_clk_freerun_in              (gtwiz_reset_clk_freerun_in)
   ,.gtwiz_reset_all_in                      (gtwiz_reset_all_in)
   ,.gtwiz_reset_tx_pll_and_datapath_in      (gtwiz_reset_tx_pll_and_datapath_in)
   ,.gtwiz_reset_tx_datapath_in              (gtwiz_reset_tx_datapath_in)
   ,.gtwiz_reset_rx_pll_and_datapath_in      (gtwiz_reset_rx_pll_and_datapath_in)
   ,.gtwiz_reset_rx_datapath_in              (gtwiz_reset_rx_datapath_in)
   ,.gtwiz_reset_rx_cdr_stable_out           (gtwiz_reset_rx_cdr_stable_out)
   ,.gtwiz_reset_tx_done_out                 (gtwiz_reset_tx_done_out)
   ,.gtwiz_reset_rx_done_out                 (gtwiz_reset_rx_done_out)
   ,.gtwiz_userdata_tx_in                    (gtwiz_userdata_tx_in)
   ,.gtwiz_userdata_rx_out                   (gtwiz_userdata_rx_out)
   ,.bgbypassb_in                            (bgbypassb_in)
   ,.bgmonitorenb_in                         (bgmonitorenb_in)
   ,.bgpdb_in                                (bgpdb_in)
   ,.bgrcalovrd_in                           (bgrcalovrd_in)
   ,.bgrcalovrdenb_in                        (bgrcalovrdenb_in)
   ,.gtnorthrefclk00_in                      (gtnorthrefclk00_in)
   ,.gtnorthrefclk01_in                      (gtnorthrefclk01_in)
   ,.gtnorthrefclk10_in                      (gtnorthrefclk10_in)
   ,.gtnorthrefclk11_in                      (gtnorthrefclk11_in)
   ,.gtrefclk00_in                           (gtrefclk00_in)
   ,.gtrefclk01_in                           (gtrefclk01_in)
   ,.gtrefclk10_in                           (gtrefclk10_in)
   ,.gtrefclk11_in                           (gtrefclk11_in)
   ,.gtsouthrefclk00_in                      (gtsouthrefclk00_in)
   ,.gtsouthrefclk01_in                      (gtsouthrefclk01_in)
   ,.gtsouthrefclk10_in                      (gtsouthrefclk10_in)
   ,.gtsouthrefclk11_in                      (gtsouthrefclk11_in)
   ,.pmarsvd0_in                             (pmarsvd0_in)
   ,.pmarsvd1_in                             (pmarsvd1_in)
   ,.qpll0clkrsvd0_in                        (qpll0clkrsvd0_in)
   ,.qpll0clkrsvd1_in                        (qpll0clkrsvd1_in)
   ,.qpll0lockdetclk_in                      (qpll0lockdetclk_in)
   ,.qpll0locken_in                          (qpll0locken_in)
   ,.qpll0pd_in                              (qpll0pd_in)
   ,.qpll0refclksel_in                       (qpll0refclksel_in)
   ,.qpll1clkrsvd0_in                        (qpll1clkrsvd0_in)
   ,.qpll1clkrsvd1_in                        (qpll1clkrsvd1_in)
   ,.qpll1lockdetclk_in                      (qpll1lockdetclk_in)
   ,.qpll1locken_in                          (qpll1locken_in)
   ,.qpll1pd_in                              (qpll1pd_in)
   ,.qpll1refclksel_in                       (qpll1refclksel_in)
   ,.qpll1reset_in                           (qpll1reset_int)
   ,.qpllrsvd1_in                            (qpllrsvd1_in)
   ,.qpllrsvd2_in                            (qpllrsvd2_in)
   ,.qpllrsvd3_in                            (qpllrsvd3_in)
   ,.qpllrsvd4_in                            (qpllrsvd4_in)
   ,.rcalenb_in                              (rcalenb_in)
   ,.sdm0data_in                             (sdm0data_in)
   ,.sdm0reset_in                            (sdm0reset_in)
   ,.sdm0width_in                            (sdm0width_in)
   ,.sdm1data_in                             (sdm1data_in)
   ,.sdm1reset_in                            (sdm1reset_in)
   ,.sdm1width_in                            (sdm1width_in)
   ,.qpll0fbclklost_out                      (qpll0fbclklost_out)
   ,.qpll0lock_out                           (qpll0lock_int)
   ,.qpll0outclk_out                         (qpll0outclk_out)
   ,.qpll0outrefclk_out                      (qpll0outrefclk_out)
   ,.qpll0refclklost_out                     (qpll0refclklost_out)
   ,.qpll1fbclklost_out                      (qpll1fbclklost_out)
   ,.qpll1lock_out                           (qpll1lock_int)
   ,.qpll1outclk_out                         (qpll1outclk_out)
   ,.qpll1outrefclk_out                      (qpll1outrefclk_out)
   ,.qpll1refclklost_out                     (qpll1refclklost_out)
   ,.qplldmonitor0_out                       (qplldmonitor0_out)
   ,.qplldmonitor1_out                       (qplldmonitor1_out)
   ,.refclkoutmonitor0_out                   (refclkoutmonitor0_out)
   ,.refclkoutmonitor1_out                   (refclkoutmonitor1_out)
   ,.cfgreset_in                             (cfgreset_in)
   ,.cplllockdetclk_in                       (cplllockdetclk_in)
   ,.cplllocken_in                           (cplllocken_in)
   ,.cpllpd_in                               (cpllpd_int)
   ,.cpllrefclksel_in                        (cpllrefclksel_in)
   ,.cpllreset_in                            (cpllreset_in)
   ,.eyescanreset_in                         (eyescanreset_in)
   ,.gtgrefclk_in                            (gtgrefclk_in)
   ,.gtnorthrefclk0_in                       (gtnorthrefclk0_in)
   ,.gtnorthrefclk1_in                       (gtnorthrefclk1_in)
   ,.gtrefclk0_in                            (gtrefclk0_in)
   ,.gtrefclk1_in                            (gtrefclk1_in)
   ,.gtsouthrefclk0_in                       (gtsouthrefclk0_in)
   ,.gtsouthrefclk1_in                       (gtsouthrefclk1_in)
   ,.resetovrd_in                            (resetovrd_in)
   ,.rx8b10ben_in                            (rx8b10ben_in)
   //,.rxbufreset_in                           (rxbufreset_in)
   ,.rxcdrfreqreset_in                       (rxcdrfreqreset_in)
   ,.rxcdrreset_in                           (rxcdrreset_in)
   ,.rxcommadeten_in                         (rxcommadeten_in)
   ,.rxdfelpmreset_in                        (rxdfelpmreset_in)
   ,.rxdlybypass_in                          (rxdlybypass_int)
   ,.rxgearboxslip_in                        (rxgearboxslip_in)
   ,.rxlatclk_in                             (rxlatclk_in)
   ,.rxmcommaalignen_in                      (rxmcommaalignen_in)
   ,.rxoobreset_in                           (rxoobreset_in)
   ,.rxoscalreset_in                         (rxoscalreset_in)
   ,.rxoutclksel_in                          (rxoutclksel_in)
   ,.rxpcommaalignen_in                      (rxpcommaalignen_in)
   ,.rxpcsreset_in                           (rxpcsreset_in)
   ,.rxpllclksel_in                          (rxpllclksel_in)
   ,.rxpmareset_in                           (rxpmareset_in)
   ,.rxrate_in                               (rxrate_in)
   ,.rxratemode_in                           (rxratemode_in)
   ,.rxslide_in                              (rxslide_in)
   ,.rxsysclksel_in                          (rxsysclksel_in)
   ,.rxusrclk_in                             (rxusrclk_int)
   ,.rxusrclk2_in                            (rxusrclk2_int)
   ,.tx8b10ben_in                            (tx8b10ben_in)
   ,.txctrl0_in                              (txctrl0_int)
   ,.txctrl1_in                              (txctrl1_int)
   ,.txctrl2_in                              (txctrl2_in)
   ,.txpcsreset_in                           (txpcsreset_in)
   ,.txpllclksel_in                          (txpllclksel_in)
   ,.txpmareset_in                           (txpmareset_in)
   ,.txsysclksel_in                          (txsysclksel_in)
   ,.txusrclk_in                             (txusrclk_int)
   ,.txusrclk2_in                            (txusrclk2_int)
   ,.cpllfbclklost_out                       (cpllfbclklost_out)
   ,.cplllock_out                            (cplllock_int)
   ,.cpllrefclklost_out                      (cpllrefclklost_out)
   ,.gtpowergood_out                         (gtpowergood_int)
   ,.gtrefclkmonitor_out                     (gtrefclkmonitor_out)
   ,.pcsrsvdout_out                          (pcsrsvdout_out)
   ,.resetexception_out                      (resetexception_out)
   //,.rxbufstatus_out                         (rxbufstatus_out)
   ,.rxbyteisaligned_out                     (rxbyteisaligned_out)
   ,.rxbyterealign_out                       (rxbyterealign_out)
   ,.rxcommadet_out                          (rxcommadet_out)
   ,.rxctrl0_out                             (rxctrl0_int)
   ,.rxctrl1_out                             (rxctrl1_int)
   ,.rxctrl2_out                             (rxctrl2_out)
   ,.rxctrl3_out                             (rxctrl3_out)
   ,.rxdata_out                              (rxdata_int)
   ,.rxdataextendrsvd_out                    (rxdataextendrsvd_out)
   ,.rxdatavalid_out                         (rxdatavalid_out)
   ,.rxheader_out                            (rxheader_out)
   ,.rxheadervalid_out                       (rxheadervalid_out)
   ,.rxosintdone_out                         (rxosintdone_out)
   ,.rxoutclk_out                            (rxoutclk_int)
   ,.rxoutclkfabric_out                      (rxoutclkfabric_out)
   ,.rxoutclkpcs_out                         (rxoutclkpcs_out)
   ,.rxpmaresetdone_out                      (rxpmaresetdone_out)
   ,.rxprgdivresetdone_out                   (rxprgdivresetdone_out)
   ,.rxratedone_out                          (rxratedone_out)
   ,.rxresetdone_out                         (rxresetdone_int)
   ,.rxstartofseq_out                        (rxstartofseq_out)
   ,.txoutclk_out                            (txoutclk_int)
   ,.txpmaresetdone_out                      (txpmaresetdone_out)
   ,.txresetdone_out                         (txresetdone_int)
);

endmodule
