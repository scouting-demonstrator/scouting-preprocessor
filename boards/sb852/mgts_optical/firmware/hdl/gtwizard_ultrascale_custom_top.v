//------------------------------------------------------------------------------
//  (c) Copyright 2013-2018 Xilinx, Inc. All rights reserved.
//
//  This file contains confidential and proprietary information
//  of Xilinx, Inc. and is protected under U.S. and
//  international copyright and other intellectual property
//  laws.
//
//  DISCLAIMER
//  This disclaimer is not a license and does not grant any
//  rights to the materials distributed herewith. Except as
//  otherwise provided in a valid license issued to you by
//  Xilinx, and to the maximum extent permitted by applicable
//  law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
//  WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
//  AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
//  BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
//  INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
//  (2) Xilinx shall not be liable (whether in contract or tort,
//  including negligence, or under any other theory of
//  liability) for any loss or damage of any kind or nature
//  related to, arising under or in connection with these
//  materials, including for any direct, or any indirect,
//  special, incidental, or consequential loss or damage
//  (including loss of data, profits, goodwill, or any type of
//  loss or damage suffered as a result of any action brought
//  by a third party) even if such damage or loss was
//  reasonably foreseeable or Xilinx had been advised of the
//  possibility of the same.
//
//  CRITICAL APPLICATIONS
//  Xilinx products are not designed or intended to be fail-
//  safe, or for use in any application requiring fail-safe
//  performance, such as life-support or safety devices or
//  systems, Class III medical devices, nuclear facilities,
//  applications related to the deployment of airbags, or any
//  other applications that could lead to death, personal
//  injury, or severe property or environmental damage
//  (individually and collectively, "Critical
//  Applications"). Customer assumes the sole risk and
//  liability of any use of Xilinx products in Critical
//  Applications, subject only to applicable laws and
//  regulations governing limitations on product liability.
//
//  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
//  PART OF THIS FILE AT ALL TIMES.
//------------------------------------------------------------------------------


`timescale 1ps/1ps

// =====================================================================================================================
// This example design top module instantiates the example design wrapper; slices vectored ports for per-channel
// assignment; and instantiates example resources such as buffers, pattern generators, and pattern checkers for core
// demonstration purposes
// =====================================================================================================================

module gt_ultrascale_custom_top (

  // Differential reference clock inputs
//  input  wire mgtrefclk0_x0y2_p,
//  input  wire mgtrefclk0_x0y2_n,
//  input  wire mgtrefclk0_x0y4_p,
//  input  wire mgtrefclk0_x0y4_n,
  
  input wire mgtrefclk0_x0y2_int,
  input wire mgtrefclk0_x0y4_int,

  // Serial data ports for transceiver channel 0
  input  wire ch0_gtyrxn_in,
  input  wire ch0_gtyrxp_in,
  output wire ch0_gtytxn_out,
  output wire ch0_gtytxp_out,

  // Serial data ports for transceiver channel 1
  input  wire ch1_gtyrxn_in,
  input  wire ch1_gtyrxp_in,
  output wire ch1_gtytxn_out,
  output wire ch1_gtytxp_out,

  // Serial data ports for transceiver channel 2
  input  wire ch2_gtyrxn_in,
  input  wire ch2_gtyrxp_in,
  output wire ch2_gtytxn_out,
  output wire ch2_gtytxp_out,

  // Serial data ports for transceiver channel 3
  input  wire ch3_gtyrxn_in,
  input  wire ch3_gtyrxp_in,
  output wire ch3_gtytxn_out,
  output wire ch3_gtytxp_out,

  // Serial data ports for transceiver channel 4
  input  wire ch4_gtyrxn_in,
  input  wire ch4_gtyrxp_in,
  output wire ch4_gtytxn_out,
  output wire ch4_gtytxp_out,

  // Serial data ports for transceiver channel 5
  input  wire ch5_gtyrxn_in,
  input  wire ch5_gtyrxp_in,
  output wire ch5_gtytxn_out,
  output wire ch5_gtytxp_out,

  // Serial data ports for transceiver channel 6
  input  wire ch6_gtyrxn_in,
  input  wire ch6_gtyrxp_in,
  output wire ch6_gtytxn_out,
  output wire ch6_gtytxp_out,

  // Serial data ports for transceiver channel 7
  input  wire ch7_gtyrxn_in,
  input  wire ch7_gtyrxp_in,
  output wire ch7_gtytxn_out,
  output wire ch7_gtytxp_out,

  // User-provided ports for reset helper block(s)
  input  wire hb_gtwiz_reset_clk_freerun_in,
  input  wire hb_gtwiz_reset_all_in,

  // PRBS-based link status ports
  input  wire link_down_latched_reset_in,
  // output wire link_status_out,
  output reg  link_down_latched_out = 1'b1,
  output wire [255:0] gtwiz_userdata_rx_out,
  
   output wire gtwiz_userclk_rx_usrclk_out,
 
  output wire [127:0] rxctrl0_out, 
  output wire [127:0] rxctrl1_out, 
  output wire [63:0] rxctrl2_out, 
  output wire [63:0] rxctrl3_out,

  output wire init_done_int,
  
  output wire [3:0] init_retry_ctr_int,                         
  output wire [7:0] gtpowergood_vio_sync,  
  output wire [7:0] rxbyteisaligned_out,                     
  output wire [7:0] txpmaresetdone_vio_sync,                    
  output wire [7:0] rxpmaresetdone_vio_sync,                   
  output wire [0:0] gtwiz_reset_tx_done_vio_sync,             
  output wire [0:0] gtwiz_reset_rx_done_vio_sync,              
  input wire  [0:0] link_down_latched_reset_vio_int,
  output wire [0:0] gtwiz_reset_rx_cdr_stable_out


);


  // ===================================================================================================================
  // PER-CHANNEL SIGNAL ASSIGNMENTS
  // ===================================================================================================================

  // The core and example design wrapper vectorize ports across all enabled transceiver channel and common instances for
  // simplicity and compactness. This example design top module assigns slices of each vector to individual, per-channel
  // signal vectors for use if desired. Signals which connect to helper blocks are prefixed "hb#", signals which connect
  // to transceiver common primitives are prefixed "cm#", and signals which connect to transceiver channel primitives
  // are prefixed "ch#", where "#" is the sequential resource number.

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtyrxn_int;
  assign gtyrxn_int[0:0] = ch0_gtyrxn_in;
  assign gtyrxn_int[1:1] = ch1_gtyrxn_in;
  assign gtyrxn_int[2:2] = ch2_gtyrxn_in;
  assign gtyrxn_int[3:3] = ch3_gtyrxn_in;
  assign gtyrxn_int[4:4] = ch4_gtyrxn_in;
  assign gtyrxn_int[5:5] = ch5_gtyrxn_in;
  assign gtyrxn_int[6:6] = ch6_gtyrxn_in;
  assign gtyrxn_int[7:7] = ch7_gtyrxn_in;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtyrxp_int;
  assign gtyrxp_int[0:0] = ch0_gtyrxp_in;
  assign gtyrxp_int[1:1] = ch1_gtyrxp_in;
  assign gtyrxp_int[2:2] = ch2_gtyrxp_in;
  assign gtyrxp_int[3:3] = ch3_gtyrxp_in;
  assign gtyrxp_int[4:4] = ch4_gtyrxp_in;
  assign gtyrxp_int[5:5] = ch5_gtyrxp_in;
  assign gtyrxp_int[6:6] = ch6_gtyrxp_in;
  assign gtyrxp_int[7:7] = ch7_gtyrxp_in;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtytxn_int;
  assign ch0_gtytxn_out = gtytxn_int[0:0];
  assign ch1_gtytxn_out = gtytxn_int[1:1];
  assign ch2_gtytxn_out = gtytxn_int[2:2];
  assign ch3_gtytxn_out = gtytxn_int[3:3];
  assign ch4_gtytxn_out = gtytxn_int[4:4];
  assign ch5_gtytxn_out = gtytxn_int[5:5];
  assign ch6_gtytxn_out = gtytxn_int[6:6];
  assign ch7_gtytxn_out = gtytxn_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtytxp_int;
  assign ch0_gtytxp_out = gtytxp_int[0:0];
  assign ch1_gtytxp_out = gtytxp_int[1:1];
  assign ch2_gtytxp_out = gtytxp_int[2:2];
  assign ch3_gtytxp_out = gtytxp_int[3:3];
  assign ch4_gtytxp_out = gtytxp_int[4:4];
  assign ch5_gtytxp_out = gtytxp_int[5:5];
  assign ch6_gtytxp_out = gtytxp_int[6:6];
  assign ch7_gtytxp_out = gtytxp_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_reset_int;
  wire [0:0] hb0_gtwiz_userclk_tx_reset_int;
  assign gtwiz_userclk_tx_reset_int[0:0] = hb0_gtwiz_userclk_tx_reset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_srcclk_int;
  wire [0:0] hb0_gtwiz_userclk_tx_srcclk_int;
  assign hb0_gtwiz_userclk_tx_srcclk_int = gtwiz_userclk_tx_srcclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_usrclk_int;
  wire [0:0] hb0_gtwiz_userclk_tx_usrclk_int;
  assign hb0_gtwiz_userclk_tx_usrclk_int = gtwiz_userclk_tx_usrclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_usrclk2_int;
  wire [0:0] hb0_gtwiz_userclk_tx_usrclk2_int;
  assign hb0_gtwiz_userclk_tx_usrclk2_int = gtwiz_userclk_tx_usrclk2_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_active_int;
  wire [0:0] hb0_gtwiz_userclk_tx_active_int;
  assign hb0_gtwiz_userclk_tx_active_int = gtwiz_userclk_tx_active_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_reset_int;
  wire [0:0] hb0_gtwiz_userclk_rx_reset_int;
  assign gtwiz_userclk_rx_reset_int[0:0] = hb0_gtwiz_userclk_rx_reset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_srcclk_int;
  wire [0:0] hb0_gtwiz_userclk_rx_srcclk_int;
  assign hb0_gtwiz_userclk_rx_srcclk_int = gtwiz_userclk_rx_srcclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_usrclk_int;
  wire [0:0] hb0_gtwiz_userclk_rx_usrclk_int;
  assign hb0_gtwiz_userclk_rx_usrclk_int = gtwiz_userclk_rx_usrclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_usrclk2_int;
  wire [0:0] hb0_gtwiz_userclk_rx_usrclk2_int;
  assign hb0_gtwiz_userclk_rx_usrclk2_int = gtwiz_userclk_rx_usrclk2_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_active_int;
  wire [0:0] hb0_gtwiz_userclk_rx_active_int;
  assign hb0_gtwiz_userclk_rx_active_int = gtwiz_userclk_rx_active_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_clk_freerun_int;
  wire [0:0] hb0_gtwiz_reset_clk_freerun_int = 1'b0;
  assign gtwiz_reset_clk_freerun_int[0:0] = hb0_gtwiz_reset_clk_freerun_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_all_int;
  wire [0:0] hb0_gtwiz_reset_all_int = 1'b0;
  assign gtwiz_reset_all_int[0:0] = hb0_gtwiz_reset_all_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_tx_pll_and_datapath_int;
  wire [0:0] hb0_gtwiz_reset_tx_pll_and_datapath_int;
  assign gtwiz_reset_tx_pll_and_datapath_int[0:0] = hb0_gtwiz_reset_tx_pll_and_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_tx_datapath_int;
  wire [0:0] hb0_gtwiz_reset_tx_datapath_int;
  assign gtwiz_reset_tx_datapath_int[0:0] = hb0_gtwiz_reset_tx_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_pll_and_datapath_int;
  wire [0:0] hb0_gtwiz_reset_rx_pll_and_datapath_int = 1'b0;
  assign gtwiz_reset_rx_pll_and_datapath_int[0:0] = hb0_gtwiz_reset_rx_pll_and_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_datapath_int;
  wire [0:0] hb0_gtwiz_reset_rx_datapath_int = 1'b0;
  assign gtwiz_reset_rx_datapath_int[0:0] = hb0_gtwiz_reset_rx_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_cdr_stable_int;
  wire [0:0] hb0_gtwiz_reset_rx_cdr_stable_int;
  assign hb0_gtwiz_reset_rx_cdr_stable_int = gtwiz_reset_rx_cdr_stable_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_tx_done_int;
  wire [0:0] hb0_gtwiz_reset_tx_done_int;
  assign hb0_gtwiz_reset_tx_done_int = gtwiz_reset_tx_done_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_done_int;
  wire [0:0] hb0_gtwiz_reset_rx_done_int;
  assign hb0_gtwiz_reset_rx_done_int = gtwiz_reset_rx_done_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [255:0] gtwiz_userdata_tx_int;
  wire [31:0] hb0_gtwiz_userdata_tx_int;
  wire [31:0] hb1_gtwiz_userdata_tx_int;
  wire [31:0] hb2_gtwiz_userdata_tx_int;
  wire [31:0] hb3_gtwiz_userdata_tx_int;
  wire [31:0] hb4_gtwiz_userdata_tx_int;
  wire [31:0] hb5_gtwiz_userdata_tx_int;
  wire [31:0] hb6_gtwiz_userdata_tx_int;
  wire [31:0] hb7_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[31:0] = hb0_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[63:32] = hb1_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[95:64] = hb2_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[127:96] = hb3_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[159:128] = hb4_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[191:160] = hb5_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[223:192] = hb6_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[255:224] = hb7_gtwiz_userdata_tx_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [255:0] gtwiz_userdata_rx_int;
  wire [31:0] hb0_gtwiz_userdata_rx_int;
  wire [31:0] hb1_gtwiz_userdata_rx_int;
  wire [31:0] hb2_gtwiz_userdata_rx_int;
  wire [31:0] hb3_gtwiz_userdata_rx_int;
  wire [31:0] hb4_gtwiz_userdata_rx_int;
  wire [31:0] hb5_gtwiz_userdata_rx_int;
  wire [31:0] hb6_gtwiz_userdata_rx_int;
  wire [31:0] hb7_gtwiz_userdata_rx_int;
  assign hb0_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[31:0];
  assign hb1_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[63:32];
  assign hb2_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[95:64];
  assign hb3_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[127:96];
  assign hb4_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[159:128];
  assign hb5_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[191:160];
  assign hb6_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[223:192];
  assign hb7_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[255:224];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] bgbypassb_int;
  wire [0:0] cm0_bgbypassb_int = 1'b1;
  wire [0:0] cm1_bgbypassb_int = 1'b1;
  assign bgbypassb_int[0:0] = cm0_bgbypassb_int;
  assign bgbypassb_int[1:1] = cm1_bgbypassb_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] bgmonitorenb_int;
  wire [0:0] cm0_bgmonitorenb_int = 1'b1;
  wire [0:0] cm1_bgmonitorenb_int = 1'b1;
  assign bgmonitorenb_int[0:0] = cm0_bgmonitorenb_int;
  assign bgmonitorenb_int[1:1] = cm1_bgmonitorenb_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] bgpdb_int;
  wire [0:0] cm0_bgpdb_int = 1'b1;
  wire [0:0] cm1_bgpdb_int = 1'b1;
  assign bgpdb_int[0:0] = cm0_bgpdb_int;
  assign bgpdb_int[1:1] = cm1_bgpdb_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [9:0] bgrcalovrd_int;
  wire [4:0] cm0_bgrcalovrd_int = 5'b10000;
  wire [4:0] cm1_bgrcalovrd_int = 5'b10000;
  assign bgrcalovrd_int[4:0] = cm0_bgrcalovrd_int;
  assign bgrcalovrd_int[9:5] = cm1_bgrcalovrd_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] bgrcalovrdenb_int;
  wire [0:0] cm0_bgrcalovrdenb_int = 1'b1;
  wire [0:0] cm1_bgrcalovrdenb_int = 1'b1;
  assign bgrcalovrdenb_int[0:0] = cm0_bgrcalovrdenb_int;
  assign bgrcalovrdenb_int[1:1] = cm1_bgrcalovrdenb_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtnorthrefclk00_int;
  wire [0:0] cm0_gtnorthrefclk00_int = 1'b0;
  wire [0:0] cm1_gtnorthrefclk00_int = 1'b0;
  assign gtnorthrefclk00_int[0:0] = cm0_gtnorthrefclk00_int;
  assign gtnorthrefclk00_int[1:1] = cm1_gtnorthrefclk00_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtnorthrefclk01_int;
  wire [0:0] cm0_gtnorthrefclk01_int = 1'b0;
  wire [0:0] cm1_gtnorthrefclk01_int = 1'b0;
  assign gtnorthrefclk01_int[0:0] = cm0_gtnorthrefclk01_int;
  assign gtnorthrefclk01_int[1:1] = cm1_gtnorthrefclk01_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtnorthrefclk10_int;
  wire [0:0] cm0_gtnorthrefclk10_int = 1'b0;
  wire [0:0] cm1_gtnorthrefclk10_int = 1'b0;
  assign gtnorthrefclk10_int[0:0] = cm0_gtnorthrefclk10_int;
  assign gtnorthrefclk10_int[1:1] = cm1_gtnorthrefclk10_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtnorthrefclk11_int;
  wire [0:0] cm0_gtnorthrefclk11_int = 1'b0;
  wire [0:0] cm1_gtnorthrefclk11_int = 1'b0;
  assign gtnorthrefclk11_int[0:0] = cm0_gtnorthrefclk11_int;
  assign gtnorthrefclk11_int[1:1] = cm1_gtnorthrefclk11_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtrefclk00_int;
  wire [0:0] cm0_gtrefclk00_int;
  wire [0:0] cm1_gtrefclk00_int;
  assign gtrefclk00_int[0:0] = cm0_gtrefclk00_int;
  assign gtrefclk00_int[1:1] = cm1_gtrefclk00_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtrefclk01_int;
  wire [0:0] cm0_gtrefclk01_int = 1'b0;
  wire [0:0] cm1_gtrefclk01_int = 1'b0;
  assign gtrefclk01_int[0:0] = cm0_gtrefclk01_int;
  assign gtrefclk01_int[1:1] = cm1_gtrefclk01_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtrefclk10_int;
  wire [0:0] cm0_gtrefclk10_int = 1'b0;
  wire [0:0] cm1_gtrefclk10_int = 1'b0;
  assign gtrefclk10_int[0:0] = cm0_gtrefclk10_int;
  assign gtrefclk10_int[1:1] = cm1_gtrefclk10_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtrefclk11_int;
  wire [0:0] cm0_gtrefclk11_int = 1'b0;
  wire [0:0] cm1_gtrefclk11_int = 1'b0;
  assign gtrefclk11_int[0:0] = cm0_gtrefclk11_int;
  assign gtrefclk11_int[1:1] = cm1_gtrefclk11_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtsouthrefclk00_int;
  wire [0:0] cm0_gtsouthrefclk00_int = 1'b0;
  wire [0:0] cm1_gtsouthrefclk00_int = 1'b0;
  assign gtsouthrefclk00_int[0:0] = cm0_gtsouthrefclk00_int;
  assign gtsouthrefclk00_int[1:1] = cm1_gtsouthrefclk00_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtsouthrefclk01_int;
  wire [0:0] cm0_gtsouthrefclk01_int = 1'b0;
  wire [0:0] cm1_gtsouthrefclk01_int = 1'b0;
  assign gtsouthrefclk01_int[0:0] = cm0_gtsouthrefclk01_int;
  assign gtsouthrefclk01_int[1:1] = cm1_gtsouthrefclk01_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtsouthrefclk10_int;
  wire [0:0] cm0_gtsouthrefclk10_int = 1'b0;
  wire [0:0] cm1_gtsouthrefclk10_int = 1'b0;
  assign gtsouthrefclk10_int[0:0] = cm0_gtsouthrefclk10_int;
  assign gtsouthrefclk10_int[1:1] = cm1_gtsouthrefclk10_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] gtsouthrefclk11_int;
  wire [0:0] cm0_gtsouthrefclk11_int = 1'b0;
  wire [0:0] cm1_gtsouthrefclk11_int = 1'b0;
  assign gtsouthrefclk11_int[0:0] = cm0_gtsouthrefclk11_int;
  assign gtsouthrefclk11_int[1:1] = cm1_gtsouthrefclk11_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] pmarsvd0_int;
  wire [7:0] cm0_pmarsvd0_int = 8'b00000000;
  wire [7:0] cm1_pmarsvd0_int = 8'b00000000;
  assign pmarsvd0_int[7:0] = cm0_pmarsvd0_int;
  assign pmarsvd0_int[15:8] = cm1_pmarsvd0_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] pmarsvd1_int;
  wire [7:0] cm0_pmarsvd1_int = 8'b00000000;
  wire [7:0] cm1_pmarsvd1_int = 8'b00000000;
  assign pmarsvd1_int[7:0] = cm0_pmarsvd1_int;
  assign pmarsvd1_int[15:8] = cm1_pmarsvd1_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0clkrsvd0_int;
  wire [0:0] cm0_qpll0clkrsvd0_int = 1'b0;
  wire [0:0] cm1_qpll0clkrsvd0_int = 1'b0;
  assign qpll0clkrsvd0_int[0:0] = cm0_qpll0clkrsvd0_int;
  assign qpll0clkrsvd0_int[1:1] = cm1_qpll0clkrsvd0_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0clkrsvd1_int;
  wire [0:0] cm0_qpll0clkrsvd1_int = 1'b0;
  wire [0:0] cm1_qpll0clkrsvd1_int = 1'b0;
  assign qpll0clkrsvd1_int[0:0] = cm0_qpll0clkrsvd1_int;
  assign qpll0clkrsvd1_int[1:1] = cm1_qpll0clkrsvd1_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0lockdetclk_int;
  wire [0:0] cm0_qpll0lockdetclk_int = 1'b0;
  wire [0:0] cm1_qpll0lockdetclk_int = 1'b0;
  assign qpll0lockdetclk_int[0:0] = cm0_qpll0lockdetclk_int;
  assign qpll0lockdetclk_int[1:1] = cm1_qpll0lockdetclk_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0locken_int;
  wire [0:0] cm0_qpll0locken_int = 1'b1;
  wire [0:0] cm1_qpll0locken_int = 1'b1;
  assign qpll0locken_int[0:0] = cm0_qpll0locken_int;
  assign qpll0locken_int[1:1] = cm1_qpll0locken_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0pd_int;
  wire [0:0] cm0_qpll0pd_int = 1'b0;
  wire [0:0] cm1_qpll0pd_int = 1'b0;
  assign qpll0pd_int[0:0] = cm0_qpll0pd_int;
  assign qpll0pd_int[1:1] = cm1_qpll0pd_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [5:0] qpll0refclksel_int;
  wire [2:0] cm0_qpll0refclksel_int = 3'b001;
  wire [2:0] cm1_qpll0refclksel_int = 3'b001;
  assign qpll0refclksel_int[2:0] = cm0_qpll0refclksel_int;
  assign qpll0refclksel_int[5:3] = cm1_qpll0refclksel_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1clkrsvd0_int;
  wire [0:0] cm0_qpll1clkrsvd0_int = 1'b0;
  wire [0:0] cm1_qpll1clkrsvd0_int = 1'b0;
  assign qpll1clkrsvd0_int[0:0] = cm0_qpll1clkrsvd0_int;
  assign qpll1clkrsvd0_int[1:1] = cm1_qpll1clkrsvd0_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1clkrsvd1_int;
  wire [0:0] cm0_qpll1clkrsvd1_int = 1'b0;
  wire [0:0] cm1_qpll1clkrsvd1_int = 1'b0;
  assign qpll1clkrsvd1_int[0:0] = cm0_qpll1clkrsvd1_int;
  assign qpll1clkrsvd1_int[1:1] = cm1_qpll1clkrsvd1_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1lockdetclk_int;
  wire [0:0] cm0_qpll1lockdetclk_int = 1'b0;
  wire [0:0] cm1_qpll1lockdetclk_int = 1'b0;
  assign qpll1lockdetclk_int[0:0] = cm0_qpll1lockdetclk_int;
  assign qpll1lockdetclk_int[1:1] = cm1_qpll1lockdetclk_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1locken_int;
  wire [0:0] cm0_qpll1locken_int = 1'b0;
  wire [0:0] cm1_qpll1locken_int = 1'b0;
  assign qpll1locken_int[0:0] = cm0_qpll1locken_int;
  assign qpll1locken_int[1:1] = cm1_qpll1locken_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1pd_int;
  wire [0:0] cm0_qpll1pd_int = 1'b1;
  wire [0:0] cm1_qpll1pd_int = 1'b1;
  assign qpll1pd_int[0:0] = cm0_qpll1pd_int;
  assign qpll1pd_int[1:1] = cm1_qpll1pd_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [5:0] qpll1refclksel_int;
  wire [2:0] cm0_qpll1refclksel_int = 3'b001;
  wire [2:0] cm1_qpll1refclksel_int = 3'b001;
  assign qpll1refclksel_int[2:0] = cm0_qpll1refclksel_int;
  assign qpll1refclksel_int[5:3] = cm1_qpll1refclksel_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] qpllrsvd1_int;
  wire [7:0] cm0_qpllrsvd1_int = 8'b00000000;
  wire [7:0] cm1_qpllrsvd1_int = 8'b00000000;
  assign qpllrsvd1_int[7:0] = cm0_qpllrsvd1_int;
  assign qpllrsvd1_int[15:8] = cm1_qpllrsvd1_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [9:0] qpllrsvd2_int;
  wire [4:0] cm0_qpllrsvd2_int = 5'b00000;
  wire [4:0] cm1_qpllrsvd2_int = 5'b00000;
  assign qpllrsvd2_int[4:0] = cm0_qpllrsvd2_int;
  assign qpllrsvd2_int[9:5] = cm1_qpllrsvd2_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [9:0] qpllrsvd3_int;
  wire [4:0] cm0_qpllrsvd3_int = 5'b00000;
  wire [4:0] cm1_qpllrsvd3_int = 5'b00000;
  assign qpllrsvd3_int[4:0] = cm0_qpllrsvd3_int;
  assign qpllrsvd3_int[9:5] = cm1_qpllrsvd3_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] qpllrsvd4_int;
  wire [7:0] cm0_qpllrsvd4_int = 8'b00000000;
  wire [7:0] cm1_qpllrsvd4_int = 8'b00000000;
  assign qpllrsvd4_int[7:0] = cm0_qpllrsvd4_int;
  assign qpllrsvd4_int[15:8] = cm1_qpllrsvd4_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] rcalenb_int;
  wire [0:0] cm0_rcalenb_int = 1'b1;
  wire [0:0] cm1_rcalenb_int = 1'b1;
  assign rcalenb_int[0:0] = cm0_rcalenb_int;
  assign rcalenb_int[1:1] = cm1_rcalenb_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [49:0] sdm0data_int;
  wire [24:0] cm0_sdm0data_int = 25'b0000000000000000000000000;
  wire [24:0] cm1_sdm0data_int = 25'b0000000000000000000000000;
  assign sdm0data_int[24:0] = cm0_sdm0data_int;
  assign sdm0data_int[49:25] = cm1_sdm0data_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] sdm0reset_int;
  wire [0:0] cm0_sdm0reset_int = 1'b0;
  wire [0:0] cm1_sdm0reset_int = 1'b0;
  assign sdm0reset_int[0:0] = cm0_sdm0reset_int;
  assign sdm0reset_int[1:1] = cm1_sdm0reset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] sdm0width_int;
  wire [1:0] cm0_sdm0width_int = 2'b00;
  wire [1:0] cm1_sdm0width_int = 2'b00;
  assign sdm0width_int[1:0] = cm0_sdm0width_int;
  assign sdm0width_int[3:2] = cm1_sdm0width_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [49:0] sdm1data_int;
  wire [24:0] cm0_sdm1data_int = 25'b0000000000000000000000000;
  wire [24:0] cm1_sdm1data_int = 25'b0000000000000000000000000;
  assign sdm1data_int[24:0] = cm0_sdm1data_int;
  assign sdm1data_int[49:25] = cm1_sdm1data_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] sdm1reset_int;
  wire [0:0] cm0_sdm1reset_int = 1'b0;
  wire [0:0] cm1_sdm1reset_int = 1'b0;
  assign sdm1reset_int[0:0] = cm0_sdm1reset_int;
  assign sdm1reset_int[1:1] = cm1_sdm1reset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [3:0] sdm1width_int;
  wire [1:0] cm0_sdm1width_int = 2'b00;
  wire [1:0] cm1_sdm1width_int = 2'b00;
  assign sdm1width_int[1:0] = cm0_sdm1width_int;
  assign sdm1width_int[3:2] = cm1_sdm1width_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0fbclklost_int;
  wire [0:0] cm0_qpll0fbclklost_int;
  wire [0:0] cm1_qpll0fbclklost_int;
  assign cm0_qpll0fbclklost_int = qpll0fbclklost_int[0:0];
  assign cm1_qpll0fbclklost_int = qpll0fbclklost_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0lock_int;
  wire [0:0] cm0_qpll0lock_int;
  wire [0:0] cm1_qpll0lock_int;
  assign cm0_qpll0lock_int = qpll0lock_int[0:0];
  assign cm1_qpll0lock_int = qpll0lock_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0outclk_int;
  wire [0:0] cm0_qpll0outclk_int;
  wire [0:0] cm1_qpll0outclk_int;
  assign cm0_qpll0outclk_int = qpll0outclk_int[0:0];
  assign cm1_qpll0outclk_int = qpll0outclk_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0outrefclk_int;
  wire [0:0] cm0_qpll0outrefclk_int;
  wire [0:0] cm1_qpll0outrefclk_int;
  assign cm0_qpll0outrefclk_int = qpll0outrefclk_int[0:0];
  assign cm1_qpll0outrefclk_int = qpll0outrefclk_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll0refclklost_int;
  wire [0:0] cm0_qpll0refclklost_int;
  wire [0:0] cm1_qpll0refclklost_int;
  assign cm0_qpll0refclklost_int = qpll0refclklost_int[0:0];
  assign cm1_qpll0refclklost_int = qpll0refclklost_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1fbclklost_int;
  wire [0:0] cm0_qpll1fbclklost_int;
  wire [0:0] cm1_qpll1fbclklost_int;
  assign cm0_qpll1fbclklost_int = qpll1fbclklost_int[0:0];
  assign cm1_qpll1fbclklost_int = qpll1fbclklost_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1lock_int;
  wire [0:0] cm0_qpll1lock_int;
  wire [0:0] cm1_qpll1lock_int;
  assign cm0_qpll1lock_int = qpll1lock_int[0:0];
  assign cm1_qpll1lock_int = qpll1lock_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1outclk_int;
  wire [0:0] cm0_qpll1outclk_int;
  wire [0:0] cm1_qpll1outclk_int;
  assign cm0_qpll1outclk_int = qpll1outclk_int[0:0];
  assign cm1_qpll1outclk_int = qpll1outclk_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1outrefclk_int;
  wire [0:0] cm0_qpll1outrefclk_int;
  wire [0:0] cm1_qpll1outrefclk_int;
  assign cm0_qpll1outrefclk_int = qpll1outrefclk_int[0:0];
  assign cm1_qpll1outrefclk_int = qpll1outrefclk_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] qpll1refclklost_int;
  wire [0:0] cm0_qpll1refclklost_int;
  wire [0:0] cm1_qpll1refclklost_int;
  assign cm0_qpll1refclklost_int = qpll1refclklost_int[0:0];
  assign cm1_qpll1refclklost_int = qpll1refclklost_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] qplldmonitor0_int;
  wire [7:0] cm0_qplldmonitor0_int;
  wire [7:0] cm1_qplldmonitor0_int;
  assign cm0_qplldmonitor0_int = qplldmonitor0_int[7:0];
  assign cm1_qplldmonitor0_int = qplldmonitor0_int[15:8];

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] qplldmonitor1_int;
  wire [7:0] cm0_qplldmonitor1_int;
  wire [7:0] cm1_qplldmonitor1_int;
  assign cm0_qplldmonitor1_int = qplldmonitor1_int[7:0];
  assign cm1_qplldmonitor1_int = qplldmonitor1_int[15:8];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] refclkoutmonitor0_int;
  wire [0:0] cm0_refclkoutmonitor0_int;
  wire [0:0] cm1_refclkoutmonitor0_int;
  assign cm0_refclkoutmonitor0_int = refclkoutmonitor0_int[0:0];
  assign cm1_refclkoutmonitor0_int = refclkoutmonitor0_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1:0] refclkoutmonitor1_int;
  wire [0:0] cm0_refclkoutmonitor1_int;
  wire [0:0] cm1_refclkoutmonitor1_int;
  assign cm0_refclkoutmonitor1_int = refclkoutmonitor1_int[0:0];
  assign cm1_refclkoutmonitor1_int = refclkoutmonitor1_int[1:1];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] cfgreset_int;
  wire [0:0] ch0_cfgreset_int = 1'b0;
  wire [0:0] ch1_cfgreset_int = 1'b0;
  wire [0:0] ch2_cfgreset_int = 1'b0;
  wire [0:0] ch3_cfgreset_int = 1'b0;
  wire [0:0] ch4_cfgreset_int = 1'b0;
  wire [0:0] ch5_cfgreset_int = 1'b0;
  wire [0:0] ch6_cfgreset_int = 1'b0;
  wire [0:0] ch7_cfgreset_int = 1'b0;
  assign cfgreset_int[0:0] = ch0_cfgreset_int;
  assign cfgreset_int[1:1] = ch1_cfgreset_int;
  assign cfgreset_int[2:2] = ch2_cfgreset_int;
  assign cfgreset_int[3:3] = ch3_cfgreset_int;
  assign cfgreset_int[4:4] = ch4_cfgreset_int;
  assign cfgreset_int[5:5] = ch5_cfgreset_int;
  assign cfgreset_int[6:6] = ch6_cfgreset_int;
  assign cfgreset_int[7:7] = ch7_cfgreset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] cplllockdetclk_int;
  wire [0:0] ch0_cplllockdetclk_int = 1'b0;
  wire [0:0] ch1_cplllockdetclk_int = 1'b0;
  wire [0:0] ch2_cplllockdetclk_int = 1'b0;
  wire [0:0] ch3_cplllockdetclk_int = 1'b0;
  wire [0:0] ch4_cplllockdetclk_int = 1'b0;
  wire [0:0] ch5_cplllockdetclk_int = 1'b0;
  wire [0:0] ch6_cplllockdetclk_int = 1'b0;
  wire [0:0] ch7_cplllockdetclk_int = 1'b0;
  assign cplllockdetclk_int[0:0] = ch0_cplllockdetclk_int;
  assign cplllockdetclk_int[1:1] = ch1_cplllockdetclk_int;
  assign cplllockdetclk_int[2:2] = ch2_cplllockdetclk_int;
  assign cplllockdetclk_int[3:3] = ch3_cplllockdetclk_int;
  assign cplllockdetclk_int[4:4] = ch4_cplllockdetclk_int;
  assign cplllockdetclk_int[5:5] = ch5_cplllockdetclk_int;
  assign cplllockdetclk_int[6:6] = ch6_cplllockdetclk_int;
  assign cplllockdetclk_int[7:7] = ch7_cplllockdetclk_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] cplllocken_int;
  wire [0:0] ch0_cplllocken_int = 1'b0;
  wire [0:0] ch1_cplllocken_int = 1'b0;
  wire [0:0] ch2_cplllocken_int = 1'b0;
  wire [0:0] ch3_cplllocken_int = 1'b0;
  wire [0:0] ch4_cplllocken_int = 1'b0;
  wire [0:0] ch5_cplllocken_int = 1'b0;
  wire [0:0] ch6_cplllocken_int = 1'b0;
  wire [0:0] ch7_cplllocken_int = 1'b0;
  assign cplllocken_int[0:0] = ch0_cplllocken_int;
  assign cplllocken_int[1:1] = ch1_cplllocken_int;
  assign cplllocken_int[2:2] = ch2_cplllocken_int;
  assign cplllocken_int[3:3] = ch3_cplllocken_int;
  assign cplllocken_int[4:4] = ch4_cplllocken_int;
  assign cplllocken_int[5:5] = ch5_cplllocken_int;
  assign cplllocken_int[6:6] = ch6_cplllocken_int;
  assign cplllocken_int[7:7] = ch7_cplllocken_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [23:0] cpllrefclksel_int;
  wire [2:0] ch0_cpllrefclksel_int = 3'b001;
  wire [2:0] ch1_cpllrefclksel_int = 3'b001;
  wire [2:0] ch2_cpllrefclksel_int = 3'b001;
  wire [2:0] ch3_cpllrefclksel_int = 3'b001;
  wire [2:0] ch4_cpllrefclksel_int = 3'b001;
  wire [2:0] ch5_cpllrefclksel_int = 3'b001;
  wire [2:0] ch6_cpllrefclksel_int = 3'b001;
  wire [2:0] ch7_cpllrefclksel_int = 3'b001;
  assign cpllrefclksel_int[2:0] = ch0_cpllrefclksel_int;
  assign cpllrefclksel_int[5:3] = ch1_cpllrefclksel_int;
  assign cpllrefclksel_int[8:6] = ch2_cpllrefclksel_int;
  assign cpllrefclksel_int[11:9] = ch3_cpllrefclksel_int;
  assign cpllrefclksel_int[14:12] = ch4_cpllrefclksel_int;
  assign cpllrefclksel_int[17:15] = ch5_cpllrefclksel_int;
  assign cpllrefclksel_int[20:18] = ch6_cpllrefclksel_int;
  assign cpllrefclksel_int[23:21] = ch7_cpllrefclksel_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] cpllreset_int;
  wire [0:0] ch0_cpllreset_int = 1'b1;
  wire [0:0] ch1_cpllreset_int = 1'b1;
  wire [0:0] ch2_cpllreset_int = 1'b1;
  wire [0:0] ch3_cpllreset_int = 1'b1;
  wire [0:0] ch4_cpllreset_int = 1'b1;
  wire [0:0] ch5_cpllreset_int = 1'b1;
  wire [0:0] ch6_cpllreset_int = 1'b1;
  wire [0:0] ch7_cpllreset_int = 1'b1;
  assign cpllreset_int[0:0] = ch0_cpllreset_int;
  assign cpllreset_int[1:1] = ch1_cpllreset_int;
  assign cpllreset_int[2:2] = ch2_cpllreset_int;
  assign cpllreset_int[3:3] = ch3_cpllreset_int;
  assign cpllreset_int[4:4] = ch4_cpllreset_int;
  assign cpllreset_int[5:5] = ch5_cpllreset_int;
  assign cpllreset_int[6:6] = ch6_cpllreset_int;
  assign cpllreset_int[7:7] = ch7_cpllreset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] eyescanreset_int;
  wire [0:0] ch0_eyescanreset_int = 1'b0;
  wire [0:0] ch1_eyescanreset_int = 1'b0;
  wire [0:0] ch2_eyescanreset_int = 1'b0;
  wire [0:0] ch3_eyescanreset_int = 1'b0;
  wire [0:0] ch4_eyescanreset_int = 1'b0;
  wire [0:0] ch5_eyescanreset_int = 1'b0;
  wire [0:0] ch6_eyescanreset_int = 1'b0;
  wire [0:0] ch7_eyescanreset_int = 1'b0;
  assign eyescanreset_int[0:0] = ch0_eyescanreset_int;
  assign eyescanreset_int[1:1] = ch1_eyescanreset_int;
  assign eyescanreset_int[2:2] = ch2_eyescanreset_int;
  assign eyescanreset_int[3:3] = ch3_eyescanreset_int;
  assign eyescanreset_int[4:4] = ch4_eyescanreset_int;
  assign eyescanreset_int[5:5] = ch5_eyescanreset_int;
  assign eyescanreset_int[6:6] = ch6_eyescanreset_int;
  assign eyescanreset_int[7:7] = ch7_eyescanreset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtgrefclk_int;
  wire [0:0] ch0_gtgrefclk_int = 1'b0;
  wire [0:0] ch1_gtgrefclk_int = 1'b0;
  wire [0:0] ch2_gtgrefclk_int = 1'b0;
  wire [0:0] ch3_gtgrefclk_int = 1'b0;
  wire [0:0] ch4_gtgrefclk_int = 1'b0;
  wire [0:0] ch5_gtgrefclk_int = 1'b0;
  wire [0:0] ch6_gtgrefclk_int = 1'b0;
  wire [0:0] ch7_gtgrefclk_int = 1'b0;
  assign gtgrefclk_int[0:0] = ch0_gtgrefclk_int;
  assign gtgrefclk_int[1:1] = ch1_gtgrefclk_int;
  assign gtgrefclk_int[2:2] = ch2_gtgrefclk_int;
  assign gtgrefclk_int[3:3] = ch3_gtgrefclk_int;
  assign gtgrefclk_int[4:4] = ch4_gtgrefclk_int;
  assign gtgrefclk_int[5:5] = ch5_gtgrefclk_int;
  assign gtgrefclk_int[6:6] = ch6_gtgrefclk_int;
  assign gtgrefclk_int[7:7] = ch7_gtgrefclk_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtnorthrefclk0_int;
  wire [0:0] ch0_gtnorthrefclk0_int = 1'b0;
  wire [0:0] ch1_gtnorthrefclk0_int = 1'b0;
  wire [0:0] ch2_gtnorthrefclk0_int = 1'b0;
  wire [0:0] ch3_gtnorthrefclk0_int = 1'b0;
  wire [0:0] ch4_gtnorthrefclk0_int = 1'b0;
  wire [0:0] ch5_gtnorthrefclk0_int = 1'b0;
  wire [0:0] ch6_gtnorthrefclk0_int = 1'b0;
  wire [0:0] ch7_gtnorthrefclk0_int = 1'b0;
  assign gtnorthrefclk0_int[0:0] = ch0_gtnorthrefclk0_int;
  assign gtnorthrefclk0_int[1:1] = ch1_gtnorthrefclk0_int;
  assign gtnorthrefclk0_int[2:2] = ch2_gtnorthrefclk0_int;
  assign gtnorthrefclk0_int[3:3] = ch3_gtnorthrefclk0_int;
  assign gtnorthrefclk0_int[4:4] = ch4_gtnorthrefclk0_int;
  assign gtnorthrefclk0_int[5:5] = ch5_gtnorthrefclk0_int;
  assign gtnorthrefclk0_int[6:6] = ch6_gtnorthrefclk0_int;
  assign gtnorthrefclk0_int[7:7] = ch7_gtnorthrefclk0_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtnorthrefclk1_int;
  wire [0:0] ch0_gtnorthrefclk1_int = 1'b0;
  wire [0:0] ch1_gtnorthrefclk1_int = 1'b0;
  wire [0:0] ch2_gtnorthrefclk1_int = 1'b0;
  wire [0:0] ch3_gtnorthrefclk1_int = 1'b0;
  wire [0:0] ch4_gtnorthrefclk1_int = 1'b0;
  wire [0:0] ch5_gtnorthrefclk1_int = 1'b0;
  wire [0:0] ch6_gtnorthrefclk1_int = 1'b0;
  wire [0:0] ch7_gtnorthrefclk1_int = 1'b0;
  assign gtnorthrefclk1_int[0:0] = ch0_gtnorthrefclk1_int;
  assign gtnorthrefclk1_int[1:1] = ch1_gtnorthrefclk1_int;
  assign gtnorthrefclk1_int[2:2] = ch2_gtnorthrefclk1_int;
  assign gtnorthrefclk1_int[3:3] = ch3_gtnorthrefclk1_int;
  assign gtnorthrefclk1_int[4:4] = ch4_gtnorthrefclk1_int;
  assign gtnorthrefclk1_int[5:5] = ch5_gtnorthrefclk1_int;
  assign gtnorthrefclk1_int[6:6] = ch6_gtnorthrefclk1_int;
  assign gtnorthrefclk1_int[7:7] = ch7_gtnorthrefclk1_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtrefclk0_int;
  wire [0:0] ch0_gtrefclk0_int;
  wire [0:0] ch1_gtrefclk0_int;
  wire [0:0] ch2_gtrefclk0_int;
  wire [0:0] ch3_gtrefclk0_int;
  wire [0:0] ch4_gtrefclk0_int;
  wire [0:0] ch5_gtrefclk0_int;
  wire [0:0] ch6_gtrefclk0_int;
  wire [0:0] ch7_gtrefclk0_int;
  assign gtrefclk0_int[0:0] = ch0_gtrefclk0_int;
  assign gtrefclk0_int[1:1] = ch1_gtrefclk0_int;
  assign gtrefclk0_int[2:2] = ch2_gtrefclk0_int;
  assign gtrefclk0_int[3:3] = ch3_gtrefclk0_int;
  assign gtrefclk0_int[4:4] = ch4_gtrefclk0_int;
  assign gtrefclk0_int[5:5] = ch5_gtrefclk0_int;
  assign gtrefclk0_int[6:6] = ch6_gtrefclk0_int;
  assign gtrefclk0_int[7:7] = ch7_gtrefclk0_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtrefclk1_int;
  wire [0:0] ch0_gtrefclk1_int = 1'b0;
  wire [0:0] ch1_gtrefclk1_int = 1'b0;
  wire [0:0] ch2_gtrefclk1_int = 1'b0;
  wire [0:0] ch3_gtrefclk1_int = 1'b0;
  wire [0:0] ch4_gtrefclk1_int = 1'b0;
  wire [0:0] ch5_gtrefclk1_int = 1'b0;
  wire [0:0] ch6_gtrefclk1_int = 1'b0;
  wire [0:0] ch7_gtrefclk1_int = 1'b0;
  assign gtrefclk1_int[0:0] = ch0_gtrefclk1_int;
  assign gtrefclk1_int[1:1] = ch1_gtrefclk1_int;
  assign gtrefclk1_int[2:2] = ch2_gtrefclk1_int;
  assign gtrefclk1_int[3:3] = ch3_gtrefclk1_int;
  assign gtrefclk1_int[4:4] = ch4_gtrefclk1_int;
  assign gtrefclk1_int[5:5] = ch5_gtrefclk1_int;
  assign gtrefclk1_int[6:6] = ch6_gtrefclk1_int;
  assign gtrefclk1_int[7:7] = ch7_gtrefclk1_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtsouthrefclk0_int;
  wire [0:0] ch0_gtsouthrefclk0_int = 1'b0;
  wire [0:0] ch1_gtsouthrefclk0_int = 1'b0;
  wire [0:0] ch2_gtsouthrefclk0_int = 1'b0;
  wire [0:0] ch3_gtsouthrefclk0_int = 1'b0;
  wire [0:0] ch4_gtsouthrefclk0_int = 1'b0;
  wire [0:0] ch5_gtsouthrefclk0_int = 1'b0;
  wire [0:0] ch6_gtsouthrefclk0_int = 1'b0;
  wire [0:0] ch7_gtsouthrefclk0_int = 1'b0;
  assign gtsouthrefclk0_int[0:0] = ch0_gtsouthrefclk0_int;
  assign gtsouthrefclk0_int[1:1] = ch1_gtsouthrefclk0_int;
  assign gtsouthrefclk0_int[2:2] = ch2_gtsouthrefclk0_int;
  assign gtsouthrefclk0_int[3:3] = ch3_gtsouthrefclk0_int;
  assign gtsouthrefclk0_int[4:4] = ch4_gtsouthrefclk0_int;
  assign gtsouthrefclk0_int[5:5] = ch5_gtsouthrefclk0_int;
  assign gtsouthrefclk0_int[6:6] = ch6_gtsouthrefclk0_int;
  assign gtsouthrefclk0_int[7:7] = ch7_gtsouthrefclk0_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtsouthrefclk1_int;
  wire [0:0] ch0_gtsouthrefclk1_int = 1'b0;
  wire [0:0] ch1_gtsouthrefclk1_int = 1'b0;
  wire [0:0] ch2_gtsouthrefclk1_int = 1'b0;
  wire [0:0] ch3_gtsouthrefclk1_int = 1'b0;
  wire [0:0] ch4_gtsouthrefclk1_int = 1'b0;
  wire [0:0] ch5_gtsouthrefclk1_int = 1'b0;
  wire [0:0] ch6_gtsouthrefclk1_int = 1'b0;
  wire [0:0] ch7_gtsouthrefclk1_int = 1'b0;
  assign gtsouthrefclk1_int[0:0] = ch0_gtsouthrefclk1_int;
  assign gtsouthrefclk1_int[1:1] = ch1_gtsouthrefclk1_int;
  assign gtsouthrefclk1_int[2:2] = ch2_gtsouthrefclk1_int;
  assign gtsouthrefclk1_int[3:3] = ch3_gtsouthrefclk1_int;
  assign gtsouthrefclk1_int[4:4] = ch4_gtsouthrefclk1_int;
  assign gtsouthrefclk1_int[5:5] = ch5_gtsouthrefclk1_int;
  assign gtsouthrefclk1_int[6:6] = ch6_gtsouthrefclk1_int;
  assign gtsouthrefclk1_int[7:7] = ch7_gtsouthrefclk1_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] resetovrd_int;
  wire [0:0] ch0_resetovrd_int = 1'b0;
  wire [0:0] ch1_resetovrd_int = 1'b0;
  wire [0:0] ch2_resetovrd_int = 1'b0;
  wire [0:0] ch3_resetovrd_int = 1'b0;
  wire [0:0] ch4_resetovrd_int = 1'b0;
  wire [0:0] ch5_resetovrd_int = 1'b0;
  wire [0:0] ch6_resetovrd_int = 1'b0;
  wire [0:0] ch7_resetovrd_int = 1'b0;
  assign resetovrd_int[0:0] = ch0_resetovrd_int;
  assign resetovrd_int[1:1] = ch1_resetovrd_int;
  assign resetovrd_int[2:2] = ch2_resetovrd_int;
  assign resetovrd_int[3:3] = ch3_resetovrd_int;
  assign resetovrd_int[4:4] = ch4_resetovrd_int;
  assign resetovrd_int[5:5] = ch5_resetovrd_int;
  assign resetovrd_int[6:6] = ch6_resetovrd_int;
  assign resetovrd_int[7:7] = ch7_resetovrd_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rx8b10ben_int;
  wire [0:0] ch0_rx8b10ben_int = 1'b1;
  wire [0:0] ch1_rx8b10ben_int = 1'b1;
  wire [0:0] ch2_rx8b10ben_int = 1'b1;
  wire [0:0] ch3_rx8b10ben_int = 1'b1;
  wire [0:0] ch4_rx8b10ben_int = 1'b1;
  wire [0:0] ch5_rx8b10ben_int = 1'b1;
  wire [0:0] ch6_rx8b10ben_int = 1'b1;
  wire [0:0] ch7_rx8b10ben_int = 1'b1;
  assign rx8b10ben_int[0:0] = ch0_rx8b10ben_int;
  assign rx8b10ben_int[1:1] = ch1_rx8b10ben_int;
  assign rx8b10ben_int[2:2] = ch2_rx8b10ben_int;
  assign rx8b10ben_int[3:3] = ch3_rx8b10ben_int;
  assign rx8b10ben_int[4:4] = ch4_rx8b10ben_int;
  assign rx8b10ben_int[5:5] = ch5_rx8b10ben_int;
  assign rx8b10ben_int[6:6] = ch6_rx8b10ben_int;
  assign rx8b10ben_int[7:7] = ch7_rx8b10ben_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxbufreset_int;
  wire [0:0] ch0_rxbufreset_int = 1'b0;
  wire [0:0] ch1_rxbufreset_int = 1'b0;
  wire [0:0] ch2_rxbufreset_int = 1'b0;
  wire [0:0] ch3_rxbufreset_int = 1'b0;
  wire [0:0] ch4_rxbufreset_int = 1'b0;
  wire [0:0] ch5_rxbufreset_int = 1'b0;
  wire [0:0] ch6_rxbufreset_int = 1'b0;
  wire [0:0] ch7_rxbufreset_int = 1'b0;
  assign rxbufreset_int[0:0] = ch0_rxbufreset_int;
  assign rxbufreset_int[1:1] = ch1_rxbufreset_int;
  assign rxbufreset_int[2:2] = ch2_rxbufreset_int;
  assign rxbufreset_int[3:3] = ch3_rxbufreset_int;
  assign rxbufreset_int[4:4] = ch4_rxbufreset_int;
  assign rxbufreset_int[5:5] = ch5_rxbufreset_int;
  assign rxbufreset_int[6:6] = ch6_rxbufreset_int;
  assign rxbufreset_int[7:7] = ch7_rxbufreset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxcdrfreqreset_int;
  wire [0:0] ch0_rxcdrfreqreset_int = 1'b0;
  wire [0:0] ch1_rxcdrfreqreset_int = 1'b0;
  wire [0:0] ch2_rxcdrfreqreset_int = 1'b0;
  wire [0:0] ch3_rxcdrfreqreset_int = 1'b0;
  wire [0:0] ch4_rxcdrfreqreset_int = 1'b0;
  wire [0:0] ch5_rxcdrfreqreset_int = 1'b0;
  wire [0:0] ch6_rxcdrfreqreset_int = 1'b0;
  wire [0:0] ch7_rxcdrfreqreset_int = 1'b0;
  assign rxcdrfreqreset_int[0:0] = ch0_rxcdrfreqreset_int;
  assign rxcdrfreqreset_int[1:1] = ch1_rxcdrfreqreset_int;
  assign rxcdrfreqreset_int[2:2] = ch2_rxcdrfreqreset_int;
  assign rxcdrfreqreset_int[3:3] = ch3_rxcdrfreqreset_int;
  assign rxcdrfreqreset_int[4:4] = ch4_rxcdrfreqreset_int;
  assign rxcdrfreqreset_int[5:5] = ch5_rxcdrfreqreset_int;
  assign rxcdrfreqreset_int[6:6] = ch6_rxcdrfreqreset_int;
  assign rxcdrfreqreset_int[7:7] = ch7_rxcdrfreqreset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxcdrreset_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxcommadeten_int;
  wire [0:0] ch0_rxcommadeten_int = 1'b1;
  wire [0:0] ch1_rxcommadeten_int = 1'b1;
  wire [0:0] ch2_rxcommadeten_int = 1'b1;
  wire [0:0] ch3_rxcommadeten_int = 1'b1;
  wire [0:0] ch4_rxcommadeten_int = 1'b1;
  wire [0:0] ch5_rxcommadeten_int = 1'b1;
  wire [0:0] ch6_rxcommadeten_int = 1'b1;
  wire [0:0] ch7_rxcommadeten_int = 1'b1;
  assign rxcommadeten_int[0:0] = ch0_rxcommadeten_int;
  assign rxcommadeten_int[1:1] = ch1_rxcommadeten_int;
  assign rxcommadeten_int[2:2] = ch2_rxcommadeten_int;
  assign rxcommadeten_int[3:3] = ch3_rxcommadeten_int;
  assign rxcommadeten_int[4:4] = ch4_rxcommadeten_int;
  assign rxcommadeten_int[5:5] = ch5_rxcommadeten_int;
  assign rxcommadeten_int[6:6] = ch6_rxcommadeten_int;
  assign rxcommadeten_int[7:7] = ch7_rxcommadeten_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxdfelpmreset_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxdlybypass_int;
  wire [0:0] ch0_rxdlybypass_int = 1'b1;
  wire [0:0] ch1_rxdlybypass_int = 1'b1;
  wire [0:0] ch2_rxdlybypass_int = 1'b1;
  wire [0:0] ch3_rxdlybypass_int = 1'b1;
  wire [0:0] ch4_rxdlybypass_int = 1'b1;
  wire [0:0] ch5_rxdlybypass_int = 1'b1;
  wire [0:0] ch6_rxdlybypass_int = 1'b1;
  wire [0:0] ch7_rxdlybypass_int = 1'b1;
  assign rxdlybypass_int[0:0] = ch0_rxdlybypass_int;
  assign rxdlybypass_int[1:1] = ch1_rxdlybypass_int;
  assign rxdlybypass_int[2:2] = ch2_rxdlybypass_int;
  assign rxdlybypass_int[3:3] = ch3_rxdlybypass_int;
  assign rxdlybypass_int[4:4] = ch4_rxdlybypass_int;
  assign rxdlybypass_int[5:5] = ch5_rxdlybypass_int;
  assign rxdlybypass_int[6:6] = ch6_rxdlybypass_int;
  assign rxdlybypass_int[7:7] = ch7_rxdlybypass_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxgearboxslip_int;
  wire [0:0] ch0_rxgearboxslip_int = 1'b0;
  wire [0:0] ch1_rxgearboxslip_int = 1'b0;
  wire [0:0] ch2_rxgearboxslip_int = 1'b0;
  wire [0:0] ch3_rxgearboxslip_int = 1'b0;
  wire [0:0] ch4_rxgearboxslip_int = 1'b0;
  wire [0:0] ch5_rxgearboxslip_int = 1'b0;
  wire [0:0] ch6_rxgearboxslip_int = 1'b0;
  wire [0:0] ch7_rxgearboxslip_int = 1'b0;
  assign rxgearboxslip_int[0:0] = ch0_rxgearboxslip_int;
  assign rxgearboxslip_int[1:1] = ch1_rxgearboxslip_int;
  assign rxgearboxslip_int[2:2] = ch2_rxgearboxslip_int;
  assign rxgearboxslip_int[3:3] = ch3_rxgearboxslip_int;
  assign rxgearboxslip_int[4:4] = ch4_rxgearboxslip_int;
  assign rxgearboxslip_int[5:5] = ch5_rxgearboxslip_int;
  assign rxgearboxslip_int[6:6] = ch6_rxgearboxslip_int;
  assign rxgearboxslip_int[7:7] = ch7_rxgearboxslip_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxlatclk_int;
  wire [0:0] ch0_rxlatclk_int = 1'b0;
  wire [0:0] ch1_rxlatclk_int = 1'b0;
  wire [0:0] ch2_rxlatclk_int = 1'b0;
  wire [0:0] ch3_rxlatclk_int = 1'b0;
  wire [0:0] ch4_rxlatclk_int = 1'b0;
  wire [0:0] ch5_rxlatclk_int = 1'b0;
  wire [0:0] ch6_rxlatclk_int = 1'b0;
  wire [0:0] ch7_rxlatclk_int = 1'b0;
  assign rxlatclk_int[0:0] = ch0_rxlatclk_int;
  assign rxlatclk_int[1:1] = ch1_rxlatclk_int;
  assign rxlatclk_int[2:2] = ch2_rxlatclk_int;
  assign rxlatclk_int[3:3] = ch3_rxlatclk_int;
  assign rxlatclk_int[4:4] = ch4_rxlatclk_int;
  assign rxlatclk_int[5:5] = ch5_rxlatclk_int;
  assign rxlatclk_int[6:6] = ch6_rxlatclk_int;
  assign rxlatclk_int[7:7] = ch7_rxlatclk_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxmcommaalignen_int;
  wire [0:0] ch0_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch1_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch2_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch3_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch4_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch5_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch6_rxmcommaalignen_int = 1'b1;
  wire [0:0] ch7_rxmcommaalignen_int = 1'b1;
  assign rxmcommaalignen_int[0:0] = ch0_rxmcommaalignen_int;
  assign rxmcommaalignen_int[1:1] = ch1_rxmcommaalignen_int;
  assign rxmcommaalignen_int[2:2] = ch2_rxmcommaalignen_int;
  assign rxmcommaalignen_int[3:3] = ch3_rxmcommaalignen_int;
  assign rxmcommaalignen_int[4:4] = ch4_rxmcommaalignen_int;
  assign rxmcommaalignen_int[5:5] = ch5_rxmcommaalignen_int;
  assign rxmcommaalignen_int[6:6] = ch6_rxmcommaalignen_int;
  assign rxmcommaalignen_int[7:7] = ch7_rxmcommaalignen_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxoobreset_int;
  wire [0:0] ch0_rxoobreset_int = 1'b0;
  wire [0:0] ch1_rxoobreset_int = 1'b0;
  wire [0:0] ch2_rxoobreset_int = 1'b0;
  wire [0:0] ch3_rxoobreset_int = 1'b0;
  wire [0:0] ch4_rxoobreset_int = 1'b0;
  wire [0:0] ch5_rxoobreset_int = 1'b0;
  wire [0:0] ch6_rxoobreset_int = 1'b0;
  wire [0:0] ch7_rxoobreset_int = 1'b0;
  assign rxoobreset_int[0:0] = ch0_rxoobreset_int;
  assign rxoobreset_int[1:1] = ch1_rxoobreset_int;
  assign rxoobreset_int[2:2] = ch2_rxoobreset_int;
  assign rxoobreset_int[3:3] = ch3_rxoobreset_int;
  assign rxoobreset_int[4:4] = ch4_rxoobreset_int;
  assign rxoobreset_int[5:5] = ch5_rxoobreset_int;
  assign rxoobreset_int[6:6] = ch6_rxoobreset_int;
  assign rxoobreset_int[7:7] = ch7_rxoobreset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxoscalreset_int;
  wire [0:0] ch0_rxoscalreset_int = 1'b0;
  wire [0:0] ch1_rxoscalreset_int = 1'b0;
  wire [0:0] ch2_rxoscalreset_int = 1'b0;
  wire [0:0] ch3_rxoscalreset_int = 1'b0;
  wire [0:0] ch4_rxoscalreset_int = 1'b0;
  wire [0:0] ch5_rxoscalreset_int = 1'b0;
  wire [0:0] ch6_rxoscalreset_int = 1'b0;
  wire [0:0] ch7_rxoscalreset_int = 1'b0;
  assign rxoscalreset_int[0:0] = ch0_rxoscalreset_int;
  assign rxoscalreset_int[1:1] = ch1_rxoscalreset_int;
  assign rxoscalreset_int[2:2] = ch2_rxoscalreset_int;
  assign rxoscalreset_int[3:3] = ch3_rxoscalreset_int;
  assign rxoscalreset_int[4:4] = ch4_rxoscalreset_int;
  assign rxoscalreset_int[5:5] = ch5_rxoscalreset_int;
  assign rxoscalreset_int[6:6] = ch6_rxoscalreset_int;
  assign rxoscalreset_int[7:7] = ch7_rxoscalreset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [23:0] rxoutclksel_int;
  wire [2:0] ch0_rxoutclksel_int = 3'b010;
  wire [2:0] ch1_rxoutclksel_int = 3'b010;
  wire [2:0] ch2_rxoutclksel_int = 3'b010;
  wire [2:0] ch3_rxoutclksel_int = 3'b010;
  wire [2:0] ch4_rxoutclksel_int = 3'b010;
  wire [2:0] ch5_rxoutclksel_int = 3'b010;
  wire [2:0] ch6_rxoutclksel_int = 3'b010;
  wire [2:0] ch7_rxoutclksel_int = 3'b010;
  assign rxoutclksel_int[2:0] = ch0_rxoutclksel_int;
  assign rxoutclksel_int[5:3] = ch1_rxoutclksel_int;
  assign rxoutclksel_int[8:6] = ch2_rxoutclksel_int;
  assign rxoutclksel_int[11:9] = ch3_rxoutclksel_int;
  assign rxoutclksel_int[14:12] = ch4_rxoutclksel_int;
  assign rxoutclksel_int[17:15] = ch5_rxoutclksel_int;
  assign rxoutclksel_int[20:18] = ch6_rxoutclksel_int;
  assign rxoutclksel_int[23:21] = ch7_rxoutclksel_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxpcommaalignen_int;
  wire [0:0] ch0_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch1_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch2_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch3_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch4_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch5_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch6_rxpcommaalignen_int = 1'b1;
  wire [0:0] ch7_rxpcommaalignen_int = 1'b1;
  assign rxpcommaalignen_int[0:0] = ch0_rxpcommaalignen_int;
  assign rxpcommaalignen_int[1:1] = ch1_rxpcommaalignen_int;
  assign rxpcommaalignen_int[2:2] = ch2_rxpcommaalignen_int;
  assign rxpcommaalignen_int[3:3] = ch3_rxpcommaalignen_int;
  assign rxpcommaalignen_int[4:4] = ch4_rxpcommaalignen_int;
  assign rxpcommaalignen_int[5:5] = ch5_rxpcommaalignen_int;
  assign rxpcommaalignen_int[6:6] = ch6_rxpcommaalignen_int;
  assign rxpcommaalignen_int[7:7] = ch7_rxpcommaalignen_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxpcsreset_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] rxpllclksel_int;
  wire [1:0] ch0_rxpllclksel_int = 2'b11;
  wire [1:0] ch1_rxpllclksel_int = 2'b11;
  wire [1:0] ch2_rxpllclksel_int = 2'b11;
  wire [1:0] ch3_rxpllclksel_int = 2'b11;
  wire [1:0] ch4_rxpllclksel_int = 2'b11;
  wire [1:0] ch5_rxpllclksel_int = 2'b11;
  wire [1:0] ch6_rxpllclksel_int = 2'b11;
  wire [1:0] ch7_rxpllclksel_int = 2'b11;
  assign rxpllclksel_int[1:0] = ch0_rxpllclksel_int;
  assign rxpllclksel_int[3:2] = ch1_rxpllclksel_int;
  assign rxpllclksel_int[5:4] = ch2_rxpllclksel_int;
  assign rxpllclksel_int[7:6] = ch3_rxpllclksel_int;
  assign rxpllclksel_int[9:8] = ch4_rxpllclksel_int;
  assign rxpllclksel_int[11:10] = ch5_rxpllclksel_int;
  assign rxpllclksel_int[13:12] = ch6_rxpllclksel_int;
  assign rxpllclksel_int[15:14] = ch7_rxpllclksel_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxpmareset_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [23:0] rxrate_int;
  wire [2:0] ch0_rxrate_int = 3'b000;
  wire [2:0] ch1_rxrate_int = 3'b000;
  wire [2:0] ch2_rxrate_int = 3'b000;
  wire [2:0] ch3_rxrate_int = 3'b000;
  wire [2:0] ch4_rxrate_int = 3'b000;
  wire [2:0] ch5_rxrate_int = 3'b000;
  wire [2:0] ch6_rxrate_int = 3'b000;
  wire [2:0] ch7_rxrate_int = 3'b000;
  assign rxrate_int[2:0] = ch0_rxrate_int;
  assign rxrate_int[5:3] = ch1_rxrate_int;
  assign rxrate_int[8:6] = ch2_rxrate_int;
  assign rxrate_int[11:9] = ch3_rxrate_int;
  assign rxrate_int[14:12] = ch4_rxrate_int;
  assign rxrate_int[17:15] = ch5_rxrate_int;
  assign rxrate_int[20:18] = ch6_rxrate_int;
  assign rxrate_int[23:21] = ch7_rxrate_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxratemode_int;
  wire [0:0] ch0_rxratemode_int = 1'b0;
  wire [0:0] ch1_rxratemode_int = 1'b0;
  wire [0:0] ch2_rxratemode_int = 1'b0;
  wire [0:0] ch3_rxratemode_int = 1'b0;
  wire [0:0] ch4_rxratemode_int = 1'b0;
  wire [0:0] ch5_rxratemode_int = 1'b0;
  wire [0:0] ch6_rxratemode_int = 1'b0;
  wire [0:0] ch7_rxratemode_int = 1'b0;
  assign rxratemode_int[0:0] = ch0_rxratemode_int;
  assign rxratemode_int[1:1] = ch1_rxratemode_int;
  assign rxratemode_int[2:2] = ch2_rxratemode_int;
  assign rxratemode_int[3:3] = ch3_rxratemode_int;
  assign rxratemode_int[4:4] = ch4_rxratemode_int;
  assign rxratemode_int[5:5] = ch5_rxratemode_int;
  assign rxratemode_int[6:6] = ch6_rxratemode_int;
  assign rxratemode_int[7:7] = ch7_rxratemode_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxslide_int;
  wire [0:0] ch0_rxslide_int = 1'b0;
  wire [0:0] ch1_rxslide_int = 1'b0;
  wire [0:0] ch2_rxslide_int = 1'b0;
  wire [0:0] ch3_rxslide_int = 1'b0;
  wire [0:0] ch4_rxslide_int = 1'b0;
  wire [0:0] ch5_rxslide_int = 1'b0;
  wire [0:0] ch6_rxslide_int = 1'b0;
  wire [0:0] ch7_rxslide_int = 1'b0;
  assign rxslide_int[0:0] = ch0_rxslide_int;
  assign rxslide_int[1:1] = ch1_rxslide_int;
  assign rxslide_int[2:2] = ch2_rxslide_int;
  assign rxslide_int[3:3] = ch3_rxslide_int;
  assign rxslide_int[4:4] = ch4_rxslide_int;
  assign rxslide_int[5:5] = ch5_rxslide_int;
  assign rxslide_int[6:6] = ch6_rxslide_int;
  assign rxslide_int[7:7] = ch7_rxslide_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] rxsysclksel_int;
  wire [1:0] ch0_rxsysclksel_int = 2'b10;
  wire [1:0] ch1_rxsysclksel_int = 2'b10;
  wire [1:0] ch2_rxsysclksel_int = 2'b10;
  wire [1:0] ch3_rxsysclksel_int = 2'b10;
  wire [1:0] ch4_rxsysclksel_int = 2'b10;
  wire [1:0] ch5_rxsysclksel_int = 2'b10;
  wire [1:0] ch6_rxsysclksel_int = 2'b10;
  wire [1:0] ch7_rxsysclksel_int = 2'b10;
  assign rxsysclksel_int[1:0] = ch0_rxsysclksel_int;
  assign rxsysclksel_int[3:2] = ch1_rxsysclksel_int;
  assign rxsysclksel_int[5:4] = ch2_rxsysclksel_int;
  assign rxsysclksel_int[7:6] = ch3_rxsysclksel_int;
  assign rxsysclksel_int[9:8] = ch4_rxsysclksel_int;
  assign rxsysclksel_int[11:10] = ch5_rxsysclksel_int;
  assign rxsysclksel_int[13:12] = ch6_rxsysclksel_int;
  assign rxsysclksel_int[15:14] = ch7_rxsysclksel_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] tx8b10ben_int;
  wire [0:0] ch0_tx8b10ben_int = 1'b1;
  wire [0:0] ch1_tx8b10ben_int = 1'b1;
  wire [0:0] ch2_tx8b10ben_int = 1'b1;
  wire [0:0] ch3_tx8b10ben_int = 1'b1;
  wire [0:0] ch4_tx8b10ben_int = 1'b1;
  wire [0:0] ch5_tx8b10ben_int = 1'b1;
  wire [0:0] ch6_tx8b10ben_int = 1'b1;
  wire [0:0] ch7_tx8b10ben_int = 1'b1;
  assign tx8b10ben_int[0:0] = ch0_tx8b10ben_int;
  assign tx8b10ben_int[1:1] = ch1_tx8b10ben_int;
  assign tx8b10ben_int[2:2] = ch2_tx8b10ben_int;
  assign tx8b10ben_int[3:3] = ch3_tx8b10ben_int;
  assign tx8b10ben_int[4:4] = ch4_tx8b10ben_int;
  assign tx8b10ben_int[5:5] = ch5_tx8b10ben_int;
  assign tx8b10ben_int[6:6] = ch6_tx8b10ben_int;
  assign tx8b10ben_int[7:7] = ch7_tx8b10ben_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [127:0] txctrl0_int;
  wire [15:0] ch0_txctrl0_int;
  wire [15:0] ch1_txctrl0_int;
  wire [15:0] ch2_txctrl0_int;
  wire [15:0] ch3_txctrl0_int;
  wire [15:0] ch4_txctrl0_int;
  wire [15:0] ch5_txctrl0_int;
  wire [15:0] ch6_txctrl0_int;
  wire [15:0] ch7_txctrl0_int;
  assign txctrl0_int[15:0] = ch0_txctrl0_int;
  assign txctrl0_int[31:16] = ch1_txctrl0_int;
  assign txctrl0_int[47:32] = ch2_txctrl0_int;
  assign txctrl0_int[63:48] = ch3_txctrl0_int;
  assign txctrl0_int[79:64] = ch4_txctrl0_int;
  assign txctrl0_int[95:80] = ch5_txctrl0_int;
  assign txctrl0_int[111:96] = ch6_txctrl0_int;
  assign txctrl0_int[127:112] = ch7_txctrl0_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [127:0] txctrl1_int;
  wire [15:0] ch0_txctrl1_int;
  wire [15:0] ch1_txctrl1_int;
  wire [15:0] ch2_txctrl1_int;
  wire [15:0] ch3_txctrl1_int;
  wire [15:0] ch4_txctrl1_int;
  wire [15:0] ch5_txctrl1_int;
  wire [15:0] ch6_txctrl1_int;
  wire [15:0] ch7_txctrl1_int;
  assign txctrl1_int[15:0] = ch0_txctrl1_int;
  assign txctrl1_int[31:16] = ch1_txctrl1_int;
  assign txctrl1_int[47:32] = ch2_txctrl1_int;
  assign txctrl1_int[63:48] = ch3_txctrl1_int;
  assign txctrl1_int[79:64] = ch4_txctrl1_int;
  assign txctrl1_int[95:80] = ch5_txctrl1_int;
  assign txctrl1_int[111:96] = ch6_txctrl1_int;
  assign txctrl1_int[127:112] = ch7_txctrl1_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] txctrl2_int;
  wire [7:0] ch0_txctrl2_int;
  wire [7:0] ch1_txctrl2_int;
  wire [7:0] ch2_txctrl2_int;
  wire [7:0] ch3_txctrl2_int;
  wire [7:0] ch4_txctrl2_int;
  wire [7:0] ch5_txctrl2_int;
  wire [7:0] ch6_txctrl2_int;
  wire [7:0] ch7_txctrl2_int;
  assign txctrl2_int[7:0] = ch0_txctrl2_int;
  assign txctrl2_int[15:8] = ch1_txctrl2_int;
  assign txctrl2_int[23:16] = ch2_txctrl2_int;
  assign txctrl2_int[31:24] = ch3_txctrl2_int;
  assign txctrl2_int[39:32] = ch4_txctrl2_int;
  assign txctrl2_int[47:40] = ch5_txctrl2_int;
  assign txctrl2_int[55:48] = ch6_txctrl2_int;
  assign txctrl2_int[63:56] = ch7_txctrl2_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] txpcsreset_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] txpllclksel_int;
  wire [1:0] ch0_txpllclksel_int = 2'b11;
  wire [1:0] ch1_txpllclksel_int = 2'b11;
  wire [1:0] ch2_txpllclksel_int = 2'b11;
  wire [1:0] ch3_txpllclksel_int = 2'b11;
  wire [1:0] ch4_txpllclksel_int = 2'b11;
  wire [1:0] ch5_txpllclksel_int = 2'b11;
  wire [1:0] ch6_txpllclksel_int = 2'b11;
  wire [1:0] ch7_txpllclksel_int = 2'b11;
  assign txpllclksel_int[1:0] = ch0_txpllclksel_int;
  assign txpllclksel_int[3:2] = ch1_txpllclksel_int;
  assign txpllclksel_int[5:4] = ch2_txpllclksel_int;
  assign txpllclksel_int[7:6] = ch3_txpllclksel_int;
  assign txpllclksel_int[9:8] = ch4_txpllclksel_int;
  assign txpllclksel_int[11:10] = ch5_txpllclksel_int;
  assign txpllclksel_int[13:12] = ch6_txpllclksel_int;
  assign txpllclksel_int[15:14] = ch7_txpllclksel_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] txpmareset_int;
  // This vector is not sliced because it is directly assigned in a debug core instance below

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] txsysclksel_int;
  wire [1:0] ch0_txsysclksel_int = 2'b10;
  wire [1:0] ch1_txsysclksel_int = 2'b10;
  wire [1:0] ch2_txsysclksel_int = 2'b10;
  wire [1:0] ch3_txsysclksel_int = 2'b10;
  wire [1:0] ch4_txsysclksel_int = 2'b10;
  wire [1:0] ch5_txsysclksel_int = 2'b10;
  wire [1:0] ch6_txsysclksel_int = 2'b10;
  wire [1:0] ch7_txsysclksel_int = 2'b10;
  assign txsysclksel_int[1:0] = ch0_txsysclksel_int;
  assign txsysclksel_int[3:2] = ch1_txsysclksel_int;
  assign txsysclksel_int[5:4] = ch2_txsysclksel_int;
  assign txsysclksel_int[7:6] = ch3_txsysclksel_int;
  assign txsysclksel_int[9:8] = ch4_txsysclksel_int;
  assign txsysclksel_int[11:10] = ch5_txsysclksel_int;
  assign txsysclksel_int[13:12] = ch6_txsysclksel_int;
  assign txsysclksel_int[15:14] = ch7_txsysclksel_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] cpllfbclklost_int;
  wire [0:0] ch0_cpllfbclklost_int;
  wire [0:0] ch1_cpllfbclklost_int;
  wire [0:0] ch2_cpllfbclklost_int;
  wire [0:0] ch3_cpllfbclklost_int;
  wire [0:0] ch4_cpllfbclklost_int;
  wire [0:0] ch5_cpllfbclklost_int;
  wire [0:0] ch6_cpllfbclklost_int;
  wire [0:0] ch7_cpllfbclklost_int;
  assign ch0_cpllfbclklost_int = cpllfbclklost_int[0:0];
  assign ch1_cpllfbclklost_int = cpllfbclklost_int[1:1];
  assign ch2_cpllfbclklost_int = cpllfbclklost_int[2:2];
  assign ch3_cpllfbclklost_int = cpllfbclklost_int[3:3];
  assign ch4_cpllfbclklost_int = cpllfbclklost_int[4:4];
  assign ch5_cpllfbclklost_int = cpllfbclklost_int[5:5];
  assign ch6_cpllfbclklost_int = cpllfbclklost_int[6:6];
  assign ch7_cpllfbclklost_int = cpllfbclklost_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] cplllock_int;
  wire [0:0] ch0_cplllock_int;
  wire [0:0] ch1_cplllock_int;
  wire [0:0] ch2_cplllock_int;
  wire [0:0] ch3_cplllock_int;
  wire [0:0] ch4_cplllock_int;
  wire [0:0] ch5_cplllock_int;
  wire [0:0] ch6_cplllock_int;
  wire [0:0] ch7_cplllock_int;
  assign ch0_cplllock_int = cplllock_int[0:0];
  assign ch1_cplllock_int = cplllock_int[1:1];
  assign ch2_cplllock_int = cplllock_int[2:2];
  assign ch3_cplllock_int = cplllock_int[3:3];
  assign ch4_cplllock_int = cplllock_int[4:4];
  assign ch5_cplllock_int = cplllock_int[5:5];
  assign ch6_cplllock_int = cplllock_int[6:6];
  assign ch7_cplllock_int = cplllock_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] cpllrefclklost_int;
  wire [0:0] ch0_cpllrefclklost_int;
  wire [0:0] ch1_cpllrefclklost_int;
  wire [0:0] ch2_cpllrefclklost_int;
  wire [0:0] ch3_cpllrefclklost_int;
  wire [0:0] ch4_cpllrefclklost_int;
  wire [0:0] ch5_cpllrefclklost_int;
  wire [0:0] ch6_cpllrefclklost_int;
  wire [0:0] ch7_cpllrefclklost_int;
  assign ch0_cpllrefclklost_int = cpllrefclklost_int[0:0];
  assign ch1_cpllrefclklost_int = cpllrefclklost_int[1:1];
  assign ch2_cpllrefclklost_int = cpllrefclklost_int[2:2];
  assign ch3_cpllrefclklost_int = cpllrefclklost_int[3:3];
  assign ch4_cpllrefclklost_int = cpllrefclklost_int[4:4];
  assign ch5_cpllrefclklost_int = cpllrefclklost_int[5:5];
  assign ch6_cpllrefclklost_int = cpllrefclklost_int[6:6];
  assign ch7_cpllrefclklost_int = cpllrefclklost_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtpowergood_int;
  wire [0:0] ch0_gtpowergood_int;
  wire [0:0] ch1_gtpowergood_int;
  wire [0:0] ch2_gtpowergood_int;
  wire [0:0] ch3_gtpowergood_int;
  wire [0:0] ch4_gtpowergood_int;
  wire [0:0] ch5_gtpowergood_int;
  wire [0:0] ch6_gtpowergood_int;
  wire [0:0] ch7_gtpowergood_int;
  assign ch0_gtpowergood_int = gtpowergood_int[0:0];
  assign ch1_gtpowergood_int = gtpowergood_int[1:1];
  assign ch2_gtpowergood_int = gtpowergood_int[2:2];
  assign ch3_gtpowergood_int = gtpowergood_int[3:3];
  assign ch4_gtpowergood_int = gtpowergood_int[4:4];
  assign ch5_gtpowergood_int = gtpowergood_int[5:5];
  assign ch6_gtpowergood_int = gtpowergood_int[6:6];
  assign ch7_gtpowergood_int = gtpowergood_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] gtrefclkmonitor_int;
  wire [0:0] ch0_gtrefclkmonitor_int;
  wire [0:0] ch1_gtrefclkmonitor_int;
  wire [0:0] ch2_gtrefclkmonitor_int;
  wire [0:0] ch3_gtrefclkmonitor_int;
  wire [0:0] ch4_gtrefclkmonitor_int;
  wire [0:0] ch5_gtrefclkmonitor_int;
  wire [0:0] ch6_gtrefclkmonitor_int;
  wire [0:0] ch7_gtrefclkmonitor_int;
  assign ch0_gtrefclkmonitor_int = gtrefclkmonitor_int[0:0];
  assign ch1_gtrefclkmonitor_int = gtrefclkmonitor_int[1:1];
  assign ch2_gtrefclkmonitor_int = gtrefclkmonitor_int[2:2];
  assign ch3_gtrefclkmonitor_int = gtrefclkmonitor_int[3:3];
  assign ch4_gtrefclkmonitor_int = gtrefclkmonitor_int[4:4];
  assign ch5_gtrefclkmonitor_int = gtrefclkmonitor_int[5:5];
  assign ch6_gtrefclkmonitor_int = gtrefclkmonitor_int[6:6];
  assign ch7_gtrefclkmonitor_int = gtrefclkmonitor_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [127:0] pcsrsvdout_int;
  wire [15:0] ch0_pcsrsvdout_int;
  wire [15:0] ch1_pcsrsvdout_int;
  wire [15:0] ch2_pcsrsvdout_int;
  wire [15:0] ch3_pcsrsvdout_int;
  wire [15:0] ch4_pcsrsvdout_int;
  wire [15:0] ch5_pcsrsvdout_int;
  wire [15:0] ch6_pcsrsvdout_int;
  wire [15:0] ch7_pcsrsvdout_int;
  assign ch0_pcsrsvdout_int = pcsrsvdout_int[15:0];
  assign ch1_pcsrsvdout_int = pcsrsvdout_int[31:16];
  assign ch2_pcsrsvdout_int = pcsrsvdout_int[47:32];
  assign ch3_pcsrsvdout_int = pcsrsvdout_int[63:48];
  assign ch4_pcsrsvdout_int = pcsrsvdout_int[79:64];
  assign ch5_pcsrsvdout_int = pcsrsvdout_int[95:80];
  assign ch6_pcsrsvdout_int = pcsrsvdout_int[111:96];
  assign ch7_pcsrsvdout_int = pcsrsvdout_int[127:112];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] resetexception_int;
  wire [0:0] ch0_resetexception_int;
  wire [0:0] ch1_resetexception_int;
  wire [0:0] ch2_resetexception_int;
  wire [0:0] ch3_resetexception_int;
  wire [0:0] ch4_resetexception_int;
  wire [0:0] ch5_resetexception_int;
  wire [0:0] ch6_resetexception_int;
  wire [0:0] ch7_resetexception_int;
  assign ch0_resetexception_int = resetexception_int[0:0];
  assign ch1_resetexception_int = resetexception_int[1:1];
  assign ch2_resetexception_int = resetexception_int[2:2];
  assign ch3_resetexception_int = resetexception_int[3:3];
  assign ch4_resetexception_int = resetexception_int[4:4];
  assign ch5_resetexception_int = resetexception_int[5:5];
  assign ch6_resetexception_int = resetexception_int[6:6];
  assign ch7_resetexception_int = resetexception_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [23:0] rxbufstatus_int;
  wire [2:0] ch0_rxbufstatus_int;
  wire [2:0] ch1_rxbufstatus_int;
  wire [2:0] ch2_rxbufstatus_int;
  wire [2:0] ch3_rxbufstatus_int;
  wire [2:0] ch4_rxbufstatus_int;
  wire [2:0] ch5_rxbufstatus_int;
  wire [2:0] ch6_rxbufstatus_int;
  wire [2:0] ch7_rxbufstatus_int;
  assign ch0_rxbufstatus_int = rxbufstatus_int[2:0];
  assign ch1_rxbufstatus_int = rxbufstatus_int[5:3];
  assign ch2_rxbufstatus_int = rxbufstatus_int[8:6];
  assign ch3_rxbufstatus_int = rxbufstatus_int[11:9];
  assign ch4_rxbufstatus_int = rxbufstatus_int[14:12];
  assign ch5_rxbufstatus_int = rxbufstatus_int[17:15];
  assign ch6_rxbufstatus_int = rxbufstatus_int[20:18];
  assign ch7_rxbufstatus_int = rxbufstatus_int[23:21];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxbyteisaligned_int;
  wire [0:0] ch0_rxbyteisaligned_int;
  wire [0:0] ch1_rxbyteisaligned_int;
  wire [0:0] ch2_rxbyteisaligned_int;
  wire [0:0] ch3_rxbyteisaligned_int;
  wire [0:0] ch4_rxbyteisaligned_int;
  wire [0:0] ch5_rxbyteisaligned_int;
  wire [0:0] ch6_rxbyteisaligned_int;
  wire [0:0] ch7_rxbyteisaligned_int;
  assign ch0_rxbyteisaligned_int = rxbyteisaligned_int[0:0];
  assign ch1_rxbyteisaligned_int = rxbyteisaligned_int[1:1];
  assign ch2_rxbyteisaligned_int = rxbyteisaligned_int[2:2];
  assign ch3_rxbyteisaligned_int = rxbyteisaligned_int[3:3];
  assign ch4_rxbyteisaligned_int = rxbyteisaligned_int[4:4];
  assign ch5_rxbyteisaligned_int = rxbyteisaligned_int[5:5];
  assign ch6_rxbyteisaligned_int = rxbyteisaligned_int[6:6];
  assign ch7_rxbyteisaligned_int = rxbyteisaligned_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxbyterealign_int;
  wire [0:0] ch0_rxbyterealign_int;
  wire [0:0] ch1_rxbyterealign_int;
  wire [0:0] ch2_rxbyterealign_int;
  wire [0:0] ch3_rxbyterealign_int;
  wire [0:0] ch4_rxbyterealign_int;
  wire [0:0] ch5_rxbyterealign_int;
  wire [0:0] ch6_rxbyterealign_int;
  wire [0:0] ch7_rxbyterealign_int;
  assign ch0_rxbyterealign_int = rxbyterealign_int[0:0];
  assign ch1_rxbyterealign_int = rxbyterealign_int[1:1];
  assign ch2_rxbyterealign_int = rxbyterealign_int[2:2];
  assign ch3_rxbyterealign_int = rxbyterealign_int[3:3];
  assign ch4_rxbyterealign_int = rxbyterealign_int[4:4];
  assign ch5_rxbyterealign_int = rxbyterealign_int[5:5];
  assign ch6_rxbyterealign_int = rxbyterealign_int[6:6];
  assign ch7_rxbyterealign_int = rxbyterealign_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxcommadet_int;
  wire [0:0] ch0_rxcommadet_int;
  wire [0:0] ch1_rxcommadet_int;
  wire [0:0] ch2_rxcommadet_int;
  wire [0:0] ch3_rxcommadet_int;
  wire [0:0] ch4_rxcommadet_int;
  wire [0:0] ch5_rxcommadet_int;
  wire [0:0] ch6_rxcommadet_int;
  wire [0:0] ch7_rxcommadet_int;
  assign ch0_rxcommadet_int = rxcommadet_int[0:0];
  assign ch1_rxcommadet_int = rxcommadet_int[1:1];
  assign ch2_rxcommadet_int = rxcommadet_int[2:2];
  assign ch3_rxcommadet_int = rxcommadet_int[3:3];
  assign ch4_rxcommadet_int = rxcommadet_int[4:4];
  assign ch5_rxcommadet_int = rxcommadet_int[5:5];
  assign ch6_rxcommadet_int = rxcommadet_int[6:6];
  assign ch7_rxcommadet_int = rxcommadet_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [127:0] rxctrl0_int;
  wire [15:0] ch0_rxctrl0_int;
  wire [15:0] ch1_rxctrl0_int;
  wire [15:0] ch2_rxctrl0_int;
  wire [15:0] ch3_rxctrl0_int;
  wire [15:0] ch4_rxctrl0_int;
  wire [15:0] ch5_rxctrl0_int;
  wire [15:0] ch6_rxctrl0_int;
  wire [15:0] ch7_rxctrl0_int;
  assign ch0_rxctrl0_int = rxctrl0_int[15:0];
  assign ch1_rxctrl0_int = rxctrl0_int[31:16];
  assign ch2_rxctrl0_int = rxctrl0_int[47:32];
  assign ch3_rxctrl0_int = rxctrl0_int[63:48];
  assign ch4_rxctrl0_int = rxctrl0_int[79:64];
  assign ch5_rxctrl0_int = rxctrl0_int[95:80];
  assign ch6_rxctrl0_int = rxctrl0_int[111:96];
  assign ch7_rxctrl0_int = rxctrl0_int[127:112];

  //--------------------------------------------------------------------------------------------------------------------
  wire [127:0] rxctrl1_int;
  wire [15:0] ch0_rxctrl1_int;
  wire [15:0] ch1_rxctrl1_int;
  wire [15:0] ch2_rxctrl1_int;
  wire [15:0] ch3_rxctrl1_int;
  wire [15:0] ch4_rxctrl1_int;
  wire [15:0] ch5_rxctrl1_int;
  wire [15:0] ch6_rxctrl1_int;
  wire [15:0] ch7_rxctrl1_int;
  assign ch0_rxctrl1_int = rxctrl1_int[15:0];
  assign ch1_rxctrl1_int = rxctrl1_int[31:16];
  assign ch2_rxctrl1_int = rxctrl1_int[47:32];
  assign ch3_rxctrl1_int = rxctrl1_int[63:48];
  assign ch4_rxctrl1_int = rxctrl1_int[79:64];
  assign ch5_rxctrl1_int = rxctrl1_int[95:80];
  assign ch6_rxctrl1_int = rxctrl1_int[111:96];
  assign ch7_rxctrl1_int = rxctrl1_int[127:112];

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] rxctrl2_int;
  wire [7:0] ch0_rxctrl2_int;
  wire [7:0] ch1_rxctrl2_int;
  wire [7:0] ch2_rxctrl2_int;
  wire [7:0] ch3_rxctrl2_int;
  wire [7:0] ch4_rxctrl2_int;
  wire [7:0] ch5_rxctrl2_int;
  wire [7:0] ch6_rxctrl2_int;
  wire [7:0] ch7_rxctrl2_int;
  assign ch0_rxctrl2_int = rxctrl2_int[7:0];
  assign ch1_rxctrl2_int = rxctrl2_int[15:8];
  assign ch2_rxctrl2_int = rxctrl2_int[23:16];
  assign ch3_rxctrl2_int = rxctrl2_int[31:24];
  assign ch4_rxctrl2_int = rxctrl2_int[39:32];
  assign ch5_rxctrl2_int = rxctrl2_int[47:40];
  assign ch6_rxctrl2_int = rxctrl2_int[55:48];
  assign ch7_rxctrl2_int = rxctrl2_int[63:56];

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] rxctrl3_int;
  wire [7:0] ch0_rxctrl3_int;
  wire [7:0] ch1_rxctrl3_int;
  wire [7:0] ch2_rxctrl3_int;
  wire [7:0] ch3_rxctrl3_int;
  wire [7:0] ch4_rxctrl3_int;
  wire [7:0] ch5_rxctrl3_int;
  wire [7:0] ch6_rxctrl3_int;
  wire [7:0] ch7_rxctrl3_int;
  assign ch0_rxctrl3_int = rxctrl3_int[7:0];
  assign ch1_rxctrl3_int = rxctrl3_int[15:8];
  assign ch2_rxctrl3_int = rxctrl3_int[23:16];
  assign ch3_rxctrl3_int = rxctrl3_int[31:24];
  assign ch4_rxctrl3_int = rxctrl3_int[39:32];
  assign ch5_rxctrl3_int = rxctrl3_int[47:40];
  assign ch6_rxctrl3_int = rxctrl3_int[55:48];
  assign ch7_rxctrl3_int = rxctrl3_int[63:56];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1023:0] rxdata_int;
  wire [127:0] ch0_rxdata_int;
  wire [127:0] ch1_rxdata_int;
  wire [127:0] ch2_rxdata_int;
  wire [127:0] ch3_rxdata_int;
  wire [127:0] ch4_rxdata_int;
  wire [127:0] ch5_rxdata_int;
  wire [127:0] ch6_rxdata_int;
  wire [127:0] ch7_rxdata_int;
  assign ch0_rxdata_int = rxdata_int[127:0];
  assign ch1_rxdata_int = rxdata_int[255:128];
  assign ch2_rxdata_int = rxdata_int[383:256];
  assign ch3_rxdata_int = rxdata_int[511:384];
  assign ch4_rxdata_int = rxdata_int[639:512];
  assign ch5_rxdata_int = rxdata_int[767:640];
  assign ch6_rxdata_int = rxdata_int[895:768];
  assign ch7_rxdata_int = rxdata_int[1023:896];

  //--------------------------------------------------------------------------------------------------------------------
  wire [63:0] rxdataextendrsvd_int;
  wire [7:0] ch0_rxdataextendrsvd_int;
  wire [7:0] ch1_rxdataextendrsvd_int;
  wire [7:0] ch2_rxdataextendrsvd_int;
  wire [7:0] ch3_rxdataextendrsvd_int;
  wire [7:0] ch4_rxdataextendrsvd_int;
  wire [7:0] ch5_rxdataextendrsvd_int;
  wire [7:0] ch6_rxdataextendrsvd_int;
  wire [7:0] ch7_rxdataextendrsvd_int;
  assign ch0_rxdataextendrsvd_int = rxdataextendrsvd_int[7:0];
  assign ch1_rxdataextendrsvd_int = rxdataextendrsvd_int[15:8];
  assign ch2_rxdataextendrsvd_int = rxdataextendrsvd_int[23:16];
  assign ch3_rxdataextendrsvd_int = rxdataextendrsvd_int[31:24];
  assign ch4_rxdataextendrsvd_int = rxdataextendrsvd_int[39:32];
  assign ch5_rxdataextendrsvd_int = rxdataextendrsvd_int[47:40];
  assign ch6_rxdataextendrsvd_int = rxdataextendrsvd_int[55:48];
  assign ch7_rxdataextendrsvd_int = rxdataextendrsvd_int[63:56];

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] rxdatavalid_int;
  wire [1:0] ch0_rxdatavalid_int;
  wire [1:0] ch1_rxdatavalid_int;
  wire [1:0] ch2_rxdatavalid_int;
  wire [1:0] ch3_rxdatavalid_int;
  wire [1:0] ch4_rxdatavalid_int;
  wire [1:0] ch5_rxdatavalid_int;
  wire [1:0] ch6_rxdatavalid_int;
  wire [1:0] ch7_rxdatavalid_int;
  assign ch0_rxdatavalid_int = rxdatavalid_int[1:0];
  assign ch1_rxdatavalid_int = rxdatavalid_int[3:2];
  assign ch2_rxdatavalid_int = rxdatavalid_int[5:4];
  assign ch3_rxdatavalid_int = rxdatavalid_int[7:6];
  assign ch4_rxdatavalid_int = rxdatavalid_int[9:8];
  assign ch5_rxdatavalid_int = rxdatavalid_int[11:10];
  assign ch6_rxdatavalid_int = rxdatavalid_int[13:12];
  assign ch7_rxdatavalid_int = rxdatavalid_int[15:14];

  //--------------------------------------------------------------------------------------------------------------------
  wire [47:0] rxheader_int;
  wire [5:0] ch0_rxheader_int;
  wire [5:0] ch1_rxheader_int;
  wire [5:0] ch2_rxheader_int;
  wire [5:0] ch3_rxheader_int;
  wire [5:0] ch4_rxheader_int;
  wire [5:0] ch5_rxheader_int;
  wire [5:0] ch6_rxheader_int;
  wire [5:0] ch7_rxheader_int;
  assign ch0_rxheader_int = rxheader_int[5:0];
  assign ch1_rxheader_int = rxheader_int[11:6];
  assign ch2_rxheader_int = rxheader_int[17:12];
  assign ch3_rxheader_int = rxheader_int[23:18];
  assign ch4_rxheader_int = rxheader_int[29:24];
  assign ch5_rxheader_int = rxheader_int[35:30];
  assign ch6_rxheader_int = rxheader_int[41:36];
  assign ch7_rxheader_int = rxheader_int[47:42];

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] rxheadervalid_int;
  wire [1:0] ch0_rxheadervalid_int;
  wire [1:0] ch1_rxheadervalid_int;
  wire [1:0] ch2_rxheadervalid_int;
  wire [1:0] ch3_rxheadervalid_int;
  wire [1:0] ch4_rxheadervalid_int;
  wire [1:0] ch5_rxheadervalid_int;
  wire [1:0] ch6_rxheadervalid_int;
  wire [1:0] ch7_rxheadervalid_int;
  assign ch0_rxheadervalid_int = rxheadervalid_int[1:0];
  assign ch1_rxheadervalid_int = rxheadervalid_int[3:2];
  assign ch2_rxheadervalid_int = rxheadervalid_int[5:4];
  assign ch3_rxheadervalid_int = rxheadervalid_int[7:6];
  assign ch4_rxheadervalid_int = rxheadervalid_int[9:8];
  assign ch5_rxheadervalid_int = rxheadervalid_int[11:10];
  assign ch6_rxheadervalid_int = rxheadervalid_int[13:12];
  assign ch7_rxheadervalid_int = rxheadervalid_int[15:14];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxosintdone_int;
  wire [0:0] ch0_rxosintdone_int;
  wire [0:0] ch1_rxosintdone_int;
  wire [0:0] ch2_rxosintdone_int;
  wire [0:0] ch3_rxosintdone_int;
  wire [0:0] ch4_rxosintdone_int;
  wire [0:0] ch5_rxosintdone_int;
  wire [0:0] ch6_rxosintdone_int;
  wire [0:0] ch7_rxosintdone_int;
  assign ch0_rxosintdone_int = rxosintdone_int[0:0];
  assign ch1_rxosintdone_int = rxosintdone_int[1:1];
  assign ch2_rxosintdone_int = rxosintdone_int[2:2];
  assign ch3_rxosintdone_int = rxosintdone_int[3:3];
  assign ch4_rxosintdone_int = rxosintdone_int[4:4];
  assign ch5_rxosintdone_int = rxosintdone_int[5:5];
  assign ch6_rxosintdone_int = rxosintdone_int[6:6];
  assign ch7_rxosintdone_int = rxosintdone_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxoutclkfabric_int;
  wire [0:0] ch0_rxoutclkfabric_int;
  wire [0:0] ch1_rxoutclkfabric_int;
  wire [0:0] ch2_rxoutclkfabric_int;
  wire [0:0] ch3_rxoutclkfabric_int;
  wire [0:0] ch4_rxoutclkfabric_int;
  wire [0:0] ch5_rxoutclkfabric_int;
  wire [0:0] ch6_rxoutclkfabric_int;
  wire [0:0] ch7_rxoutclkfabric_int;
  assign ch0_rxoutclkfabric_int = rxoutclkfabric_int[0:0];
  assign ch1_rxoutclkfabric_int = rxoutclkfabric_int[1:1];
  assign ch2_rxoutclkfabric_int = rxoutclkfabric_int[2:2];
  assign ch3_rxoutclkfabric_int = rxoutclkfabric_int[3:3];
  assign ch4_rxoutclkfabric_int = rxoutclkfabric_int[4:4];
  assign ch5_rxoutclkfabric_int = rxoutclkfabric_int[5:5];
  assign ch6_rxoutclkfabric_int = rxoutclkfabric_int[6:6];
  assign ch7_rxoutclkfabric_int = rxoutclkfabric_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxoutclkpcs_int;
  wire [0:0] ch0_rxoutclkpcs_int;
  wire [0:0] ch1_rxoutclkpcs_int;
  wire [0:0] ch2_rxoutclkpcs_int;
  wire [0:0] ch3_rxoutclkpcs_int;
  wire [0:0] ch4_rxoutclkpcs_int;
  wire [0:0] ch5_rxoutclkpcs_int;
  wire [0:0] ch6_rxoutclkpcs_int;
  wire [0:0] ch7_rxoutclkpcs_int;
  assign ch0_rxoutclkpcs_int = rxoutclkpcs_int[0:0];
  assign ch1_rxoutclkpcs_int = rxoutclkpcs_int[1:1];
  assign ch2_rxoutclkpcs_int = rxoutclkpcs_int[2:2];
  assign ch3_rxoutclkpcs_int = rxoutclkpcs_int[3:3];
  assign ch4_rxoutclkpcs_int = rxoutclkpcs_int[4:4];
  assign ch5_rxoutclkpcs_int = rxoutclkpcs_int[5:5];
  assign ch6_rxoutclkpcs_int = rxoutclkpcs_int[6:6];
  assign ch7_rxoutclkpcs_int = rxoutclkpcs_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxpmaresetdone_int;
  wire [0:0] ch0_rxpmaresetdone_int;
  wire [0:0] ch1_rxpmaresetdone_int;
  wire [0:0] ch2_rxpmaresetdone_int;
  wire [0:0] ch3_rxpmaresetdone_int;
  wire [0:0] ch4_rxpmaresetdone_int;
  wire [0:0] ch5_rxpmaresetdone_int;
  wire [0:0] ch6_rxpmaresetdone_int;
  wire [0:0] ch7_rxpmaresetdone_int;
  assign ch0_rxpmaresetdone_int = rxpmaresetdone_int[0:0];
  assign ch1_rxpmaresetdone_int = rxpmaresetdone_int[1:1];
  assign ch2_rxpmaresetdone_int = rxpmaresetdone_int[2:2];
  assign ch3_rxpmaresetdone_int = rxpmaresetdone_int[3:3];
  assign ch4_rxpmaresetdone_int = rxpmaresetdone_int[4:4];
  assign ch5_rxpmaresetdone_int = rxpmaresetdone_int[5:5];
  assign ch6_rxpmaresetdone_int = rxpmaresetdone_int[6:6];
  assign ch7_rxpmaresetdone_int = rxpmaresetdone_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxprgdivresetdone_int;
  wire [0:0] ch0_rxprgdivresetdone_int;
  wire [0:0] ch1_rxprgdivresetdone_int;
  wire [0:0] ch2_rxprgdivresetdone_int;
  wire [0:0] ch3_rxprgdivresetdone_int;
  wire [0:0] ch4_rxprgdivresetdone_int;
  wire [0:0] ch5_rxprgdivresetdone_int;
  wire [0:0] ch6_rxprgdivresetdone_int;
  wire [0:0] ch7_rxprgdivresetdone_int;
  assign ch0_rxprgdivresetdone_int = rxprgdivresetdone_int[0:0];
  assign ch1_rxprgdivresetdone_int = rxprgdivresetdone_int[1:1];
  assign ch2_rxprgdivresetdone_int = rxprgdivresetdone_int[2:2];
  assign ch3_rxprgdivresetdone_int = rxprgdivresetdone_int[3:3];
  assign ch4_rxprgdivresetdone_int = rxprgdivresetdone_int[4:4];
  assign ch5_rxprgdivresetdone_int = rxprgdivresetdone_int[5:5];
  assign ch6_rxprgdivresetdone_int = rxprgdivresetdone_int[6:6];
  assign ch7_rxprgdivresetdone_int = rxprgdivresetdone_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxratedone_int;
  wire [0:0] ch0_rxratedone_int;
  wire [0:0] ch1_rxratedone_int;
  wire [0:0] ch2_rxratedone_int;
  wire [0:0] ch3_rxratedone_int;
  wire [0:0] ch4_rxratedone_int;
  wire [0:0] ch5_rxratedone_int;
  wire [0:0] ch6_rxratedone_int;
  wire [0:0] ch7_rxratedone_int;
  assign ch0_rxratedone_int = rxratedone_int[0:0];
  assign ch1_rxratedone_int = rxratedone_int[1:1];
  assign ch2_rxratedone_int = rxratedone_int[2:2];
  assign ch3_rxratedone_int = rxratedone_int[3:3];
  assign ch4_rxratedone_int = rxratedone_int[4:4];
  assign ch5_rxratedone_int = rxratedone_int[5:5];
  assign ch6_rxratedone_int = rxratedone_int[6:6];
  assign ch7_rxratedone_int = rxratedone_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] rxresetdone_int;
  wire [0:0] ch0_rxresetdone_int;
  wire [0:0] ch1_rxresetdone_int;
  wire [0:0] ch2_rxresetdone_int;
  wire [0:0] ch3_rxresetdone_int;
  wire [0:0] ch4_rxresetdone_int;
  wire [0:0] ch5_rxresetdone_int;
  wire [0:0] ch6_rxresetdone_int;
  wire [0:0] ch7_rxresetdone_int;
  assign ch0_rxresetdone_int = rxresetdone_int[0:0];
  assign ch1_rxresetdone_int = rxresetdone_int[1:1];
  assign ch2_rxresetdone_int = rxresetdone_int[2:2];
  assign ch3_rxresetdone_int = rxresetdone_int[3:3];
  assign ch4_rxresetdone_int = rxresetdone_int[4:4];
  assign ch5_rxresetdone_int = rxresetdone_int[5:5];
  assign ch6_rxresetdone_int = rxresetdone_int[6:6];
  assign ch7_rxresetdone_int = rxresetdone_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [15:0] rxstartofseq_int;
  wire [1:0] ch0_rxstartofseq_int;
  wire [1:0] ch1_rxstartofseq_int;
  wire [1:0] ch2_rxstartofseq_int;
  wire [1:0] ch3_rxstartofseq_int;
  wire [1:0] ch4_rxstartofseq_int;
  wire [1:0] ch5_rxstartofseq_int;
  wire [1:0] ch6_rxstartofseq_int;
  wire [1:0] ch7_rxstartofseq_int;
  assign ch0_rxstartofseq_int = rxstartofseq_int[1:0];
  assign ch1_rxstartofseq_int = rxstartofseq_int[3:2];
  assign ch2_rxstartofseq_int = rxstartofseq_int[5:4];
  assign ch3_rxstartofseq_int = rxstartofseq_int[7:6];
  assign ch4_rxstartofseq_int = rxstartofseq_int[9:8];
  assign ch5_rxstartofseq_int = rxstartofseq_int[11:10];
  assign ch6_rxstartofseq_int = rxstartofseq_int[13:12];
  assign ch7_rxstartofseq_int = rxstartofseq_int[15:14];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] txpmaresetdone_int;
  wire [0:0] ch0_txpmaresetdone_int;
  wire [0:0] ch1_txpmaresetdone_int;
  wire [0:0] ch2_txpmaresetdone_int;
  wire [0:0] ch3_txpmaresetdone_int;
  wire [0:0] ch4_txpmaresetdone_int;
  wire [0:0] ch5_txpmaresetdone_int;
  wire [0:0] ch6_txpmaresetdone_int;
  wire [0:0] ch7_txpmaresetdone_int;
  assign ch0_txpmaresetdone_int = txpmaresetdone_int[0:0];
  assign ch1_txpmaresetdone_int = txpmaresetdone_int[1:1];
  assign ch2_txpmaresetdone_int = txpmaresetdone_int[2:2];
  assign ch3_txpmaresetdone_int = txpmaresetdone_int[3:3];
  assign ch4_txpmaresetdone_int = txpmaresetdone_int[4:4];
  assign ch5_txpmaresetdone_int = txpmaresetdone_int[5:5];
  assign ch6_txpmaresetdone_int = txpmaresetdone_int[6:6];
  assign ch7_txpmaresetdone_int = txpmaresetdone_int[7:7];

  //--------------------------------------------------------------------------------------------------------------------
  wire [7:0] txresetdone_int;
  wire [0:0] ch0_txresetdone_int;
  wire [0:0] ch1_txresetdone_int;
  wire [0:0] ch2_txresetdone_int;
  wire [0:0] ch3_txresetdone_int;
  wire [0:0] ch4_txresetdone_int;
  wire [0:0] ch5_txresetdone_int;
  wire [0:0] ch6_txresetdone_int;
  wire [0:0] ch7_txresetdone_int;
  assign ch0_txresetdone_int = txresetdone_int[0:0];
  assign ch1_txresetdone_int = txresetdone_int[1:1];
  assign ch2_txresetdone_int = txresetdone_int[2:2];
  assign ch3_txresetdone_int = txresetdone_int[3:3];
  assign ch4_txresetdone_int = txresetdone_int[4:4];
  assign ch5_txresetdone_int = txresetdone_int[5:5];
  assign ch6_txresetdone_int = txresetdone_int[6:6];
  assign ch7_txresetdone_int = txresetdone_int[7:7];


  // ===================================================================================================================
  // BUFFERS
  // ===================================================================================================================

  // Buffer the hb_gtwiz_reset_all_in input and logically combine it with the internal signal from the example
  // initialization block as well as the VIO-sourced reset
  wire hb_gtwiz_reset_all_vio_int;
  wire hb_gtwiz_reset_all_buf_int;
  wire hb_gtwiz_reset_all_init_int;
  wire hb_gtwiz_reset_all_int;

  IBUF ibuf_hb_gtwiz_reset_all_inst (
    .I (hb_gtwiz_reset_all_in),
    .O (hb_gtwiz_reset_all_buf_int)
  );

  assign hb_gtwiz_reset_all_int = hb_gtwiz_reset_all_buf_int || hb_gtwiz_reset_all_init_int || hb_gtwiz_reset_all_vio_int;

  // Globally buffer the free-running input clock
  wire hb_gtwiz_reset_clk_freerun_buf_int;

  BUFG bufg_clk_freerun_inst (
    .I (hb_gtwiz_reset_clk_freerun_in),
    .O (hb_gtwiz_reset_clk_freerun_buf_int)
  );

  // Instantiate a differential reference clock buffer for each reference clock differential pair in this configuration,
  // and assign the single-ended output of each differential reference clock buffer to the appropriate PLL input signal

  // Differential reference clock buffer for MGTREFCLK0_X0Y2
//  wire mgtrefclk0_x0y2_int;

//  IBUFDS_GTE4 #(
//    .REFCLK_EN_TX_PATH  (1'b0),
//    .REFCLK_HROW_CK_SEL (2'b00),
//    .REFCLK_ICNTL_RX    (2'b00)
//  ) IBUFDS_GTE4_MGTREFCLK0_X0Y2_INST (
//    .I     (mgtrefclk0_x0y2_p),
//    .IB    (mgtrefclk0_x0y2_n),
//    .CEB   (1'b0),
//    .O     (mgtrefclk0_x0y2_int),
//    .ODIV2 ()
//  );

  // Differential reference clock buffer for MGTREFCLK0_X0Y4
//  wire mgtrefclk0_x0y4_int;

//  IBUFDS_GTE4 #(
//    .REFCLK_EN_TX_PATH  (1'b0),
//    .REFCLK_HROW_CK_SEL (2'b00),
//    .REFCLK_ICNTL_RX    (2'b00)
//  ) IBUFDS_GTE4_MGTREFCLK0_X0Y4_INST (
//    .I     (mgtrefclk0_x0y4_p),
//    .IB    (mgtrefclk0_x0y4_n),
//    .CEB   (1'b0),
//    .O     (mgtrefclk0_x0y4_int),
//    .ODIV2 ()
//  );

  assign cm0_gtrefclk00_int = mgtrefclk0_x0y2_int;
  assign cm1_gtrefclk00_int = mgtrefclk0_x0y4_int;


  // ===================================================================================================================
  // USER CLOCKING RESETS
  // ===================================================================================================================

  // The TX user clocking helper block should be held in reset until the clock source of that block is known to be
  // stable. The following assignment is an example of how that stability can be determined, based on the selected TX
  // user clock source. Replace the assignment with the appropriate signal or logic to achieve that behavior as needed.
  assign hb0_gtwiz_userclk_tx_reset_int = ~(&txpmaresetdone_int);

  // The RX user clocking helper block should be held in reset until the clock source of that block is known to be
  // stable. The following assignment is an example of how that stability can be determined, based on the selected RX
  // user clock source. Replace the assignment with the appropriate signal or logic to achieve that behavior as needed.
  assign hb0_gtwiz_userclk_rx_reset_int = ~(&rxpmaresetdone_int);


  // ===================================================================================================================
  // PRBS STIMULUS, CHECKING, AND LINK MANAGEMENT
  // ===================================================================================================================

  // PRBS stimulus
  // -------------------------------------------------------------------------------------------------------------------

  // PRBS-based data stimulus module for transceiver channel 0
//  (* DONT_TOUCH = "TRUE" *)
//  gtwizard_ultrascale_0_example_stimulus_8b10b example_stimulus_inst0 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
//    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
//    .txctrl0_out                 (ch0_txctrl0_int),
//    .txctrl1_out                 (ch0_txctrl1_int),
//    .txctrl2_out                 (ch0_txctrl2_int),
//    .txdata_out                  (hb0_gtwiz_userdata_tx_int)
//  );

//  // PRBS-based data stimulus module for transceiver channel 1
//  (* DONT_TOUCH = "TRUE" *)
//  gtwizard_ultrascale_0_example_stimulus_8b10b example_stimulus_inst1 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
//    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
//    .txctrl0_out                 (ch1_txctrl0_int),
//    .txctrl1_out                 (ch1_txctrl1_int),
//    .txctrl2_out                 (ch1_txctrl2_int),
//    .txdata_out                  (hb1_gtwiz_userdata_tx_int)
//  );

//  // PRBS-based data stimulus module for transceiver channel 2
//  (* DONT_TOUCH = "TRUE" *)
//  gtwizard_ultrascale_0_example_stimulus_8b10b example_stimulus_inst2 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
//    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
//    .txctrl0_out                 (ch2_txctrl0_int),
//    .txctrl1_out                 (ch2_txctrl1_int),
//    .txctrl2_out                 (ch2_txctrl2_int),
//    .txdata_out                  (hb2_gtwiz_userdata_tx_int)
//  );

//  // PRBS-based data stimulus module for transceiver channel 3
//  (* DONT_TOUCH = "TRUE" *)
//  gtwizard_ultrascale_0_example_stimulus_8b10b example_stimulus_inst3 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
//    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
//    .txctrl0_out                 (ch3_txctrl0_int),
//    .txctrl1_out                 (ch3_txctrl1_int),
//    .txctrl2_out                 (ch3_txctrl2_int),
//    .txdata_out                  (hb3_gtwiz_userdata_tx_int)
//  );

//  // PRBS-based data stimulus module for transceiver channel 4
//  (* DONT_TOUCH = "TRUE" *)
//  gtwizard_ultrascale_0_example_stimulus_8b10b example_stimulus_inst4 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
//    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
//    .txctrl0_out                 (ch4_txctrl0_int),
//    .txctrl1_out                 (ch4_txctrl1_int),
//    .txctrl2_out                 (ch4_txctrl2_int),
//    .txdata_out                  (hb4_gtwiz_userdata_tx_int)
//  );

//  // PRBS-based data stimulus module for transceiver channel 5
//  (* DONT_TOUCH = "TRUE" *)
//  gtwizard_ultrascale_0_example_stimulus_8b10b example_stimulus_inst5 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
//    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
//    .txctrl0_out                 (ch5_txctrl0_int),
//    .txctrl1_out                 (ch5_txctrl1_int),
//    .txctrl2_out                 (ch5_txctrl2_int),
//    .txdata_out                  (hb5_gtwiz_userdata_tx_int)
//  );

//  // PRBS-based data stimulus module for transceiver channel 6
//  (* DONT_TOUCH = "TRUE" *)
//  gtwizard_ultrascale_0_example_stimulus_8b10b example_stimulus_inst6 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
//    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
//    .txctrl0_out                 (ch6_txctrl0_int),
//    .txctrl1_out                 (ch6_txctrl1_int),
//    .txctrl2_out                 (ch6_txctrl2_int),
//    .txdata_out                  (hb6_gtwiz_userdata_tx_int)
//  );

//  // PRBS-based data stimulus module for transceiver channel 7
//  (* DONT_TOUCH = "TRUE" *)
//  gtwizard_ultrascale_0_example_stimulus_8b10b example_stimulus_inst7 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
//    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
//    .txctrl0_out                 (ch7_txctrl0_int),
//    .txctrl1_out                 (ch7_txctrl1_int),
//    .txctrl2_out                 (ch7_txctrl2_int),
//    .txdata_out                  (hb7_gtwiz_userdata_tx_int)
//  );

  // PRBS checking
  // -------------------------------------------------------------------------------------------------------------------

  // Declare a signal vector of PRBS match indicators, with one indicator bit per transceiver channel
 // wire [7:0] prbs_match_int;

//  // PRBS-based data checking module for transceiver channel 0
//  gtwizard_ultrascale_0_example_checking_8b10b example_checking_inst0 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
//    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
//    .rxctrl0_in                  (ch0_rxctrl0_int),
//    .rxctrl1_in                  (ch0_rxctrl1_int),
//    .rxctrl2_in                  (ch0_rxctrl2_int),
//    .rxctrl3_in                  (ch0_rxctrl3_int),
//    .rxdata_in                   (hb0_gtwiz_userdata_rx_int),
//    .prbs_match_out              (prbs_match_int[0])
//  );

//  // PRBS-based data checking module for transceiver channel 1
//  gtwizard_ultrascale_0_example_checking_8b10b example_checking_inst1 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
//    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
//    .rxctrl0_in                  (ch1_rxctrl0_int),
//    .rxctrl1_in                  (ch1_rxctrl1_int),
//    .rxctrl2_in                  (ch1_rxctrl2_int),
//    .rxctrl3_in                  (ch1_rxctrl3_int),
//    .rxdata_in                   (hb1_gtwiz_userdata_rx_int),
//    .prbs_match_out              (prbs_match_int[1])
//  );

//  // PRBS-based data checking module for transceiver channel 2
//  gtwizard_ultrascale_0_example_checking_8b10b example_checking_inst2 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
//    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
//    .rxctrl0_in                  (ch2_rxctrl0_int),
//    .rxctrl1_in                  (ch2_rxctrl1_int),
//    .rxctrl2_in                  (ch2_rxctrl2_int),
//    .rxctrl3_in                  (ch2_rxctrl3_int),
//    .rxdata_in                   (hb2_gtwiz_userdata_rx_int),
//    .prbs_match_out              (prbs_match_int[2])
//  );

//  // PRBS-based data checking module for transceiver channel 3
//  gtwizard_ultrascale_0_example_checking_8b10b example_checking_inst3 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
//    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
//    .rxctrl0_in                  (ch3_rxctrl0_int),
//    .rxctrl1_in                  (ch3_rxctrl1_int),
//    .rxctrl2_in                  (ch3_rxctrl2_int),
//    .rxctrl3_in                  (ch3_rxctrl3_int),
//    .rxdata_in                   (hb3_gtwiz_userdata_rx_int),
//    .prbs_match_out              (prbs_match_int[3])
//  );

//  // PRBS-based data checking module for transceiver channel 4
//  gtwizard_ultrascale_0_example_checking_8b10b example_checking_inst4 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
//    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
//    .rxctrl0_in                  (ch4_rxctrl0_int),
//    .rxctrl1_in                  (ch4_rxctrl1_int),
//    .rxctrl2_in                  (ch4_rxctrl2_int),
//    .rxctrl3_in                  (ch4_rxctrl3_int),
//    .rxdata_in                   (hb4_gtwiz_userdata_rx_int),
//    .prbs_match_out              (prbs_match_int[4])
//  );

//  // PRBS-based data checking module for transceiver channel 5
//  gtwizard_ultrascale_0_example_checking_8b10b example_checking_inst5 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
//    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
//    .rxctrl0_in                  (ch5_rxctrl0_int),
//    .rxctrl1_in                  (ch5_rxctrl1_int),
//    .rxctrl2_in                  (ch5_rxctrl2_int),
//    .rxctrl3_in                  (ch5_rxctrl3_int),
//    .rxdata_in                   (hb5_gtwiz_userdata_rx_int),
//    .prbs_match_out              (prbs_match_int[5])
//  );

//  // PRBS-based data checking module for transceiver channel 6
//  gtwizard_ultrascale_0_example_checking_8b10b example_checking_inst6 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
//    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
//    .rxctrl0_in                  (ch6_rxctrl0_int),
//    .rxctrl1_in                  (ch6_rxctrl1_int),
//    .rxctrl2_in                  (ch6_rxctrl2_int),
//    .rxctrl3_in                  (ch6_rxctrl3_int),
//    .rxdata_in                   (hb6_gtwiz_userdata_rx_int),
//    .prbs_match_out              (prbs_match_int[6])
//  );

//  // PRBS-based data checking module for transceiver channel 7
//  gtwizard_ultrascale_0_example_checking_8b10b example_checking_inst7 (
//    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
//    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
//    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
//    .rxctrl0_in                  (ch7_rxctrl0_int),
//    .rxctrl1_in                  (ch7_rxctrl1_int),
//    .rxctrl2_in                  (ch7_rxctrl2_int),
//    .rxctrl3_in                  (ch7_rxctrl3_int),
//    .rxdata_in                   (hb7_gtwiz_userdata_rx_int),
//    .prbs_match_out              (prbs_match_int[7])
//  );

  // PRBS match and related link management
  // -------------------------------------------------------------------------------------------------------------------

  // Perform a bitwise NAND of all PRBS match indicators, creating a combinatorial indication of any PRBS mismatch
  // across all transceiver channels
//  wire prbs_error_any_async = ~(&prbs_match_int);
//  wire prbs_error_any_sync;

  // Synchronize the PRBS mismatch indicator the free-running clock domain, using a reset synchronizer with asynchronous
  // reset and synchronous removal
//  (* DONT_TOUCH = "TRUE" *)
//  gtwizard_ultrascale_0_example_reset_synchronizer reset_synchronizer_prbs_match_all_inst (
//    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
//    .rst_in (prbs_error_any_async),
//    .rst_out(prbs_error_any_sync)
//  );

  // Implement an example link status state machine using a simple leaky bucket mechanism. The link status indicates
  // the continual PRBS match status to both the top-level observer and the initialization state machine, while being
  // tolerant of occasional bit errors. This is an example and can be modified as necessary.
  localparam ST_LINK_DOWN = 1'b0;
  localparam ST_LINK_UP   = 1'b1;
  reg        sm_link      = ST_LINK_UP;
 // reg [6:0]  link_ctr     = 7'd0;

  //always @(posedge hb_gtwiz_reset_clk_freerun_buf_int) begin
 //   case (sm_link)
      // The link is considered to be down when the link counter initially has a value less than 67. When the link is
      // down, the counter is incremented on each cycle where all PRBS bits match, but reset whenever any PRBS mismatch
      // occurs. When the link counter reaches 67, transition to the link up state.
   //   ST_LINK_DOWN: begin
   //     if (prbs_error_any_sync !== 1'b0) begin
   //       link_ctr <= 7'd0;
    //    end
    //    else begin
   //       if (link_ctr < 7'd67)
   //         link_ctr <= link_ctr + 7'd1;
   //       else
   //         sm_link <= ST_LINK_UP;
   //     end
  //    end

      // When the link is up, the link counter is decreased by 34 whenever any PRBS mismatch occurs, but is increased by
      // only 1 on each cycle where all PRBS bits match, up to its saturation point of 67. If the link counter reaches
      // 0 (including rollover protection), transition to the link down state.
    //  ST_LINK_UP: begin
    //    if (prbs_error_any_sync !== 1'b0) begin
     //     if (link_ctr > 7'd33) begin
     //       link_ctr <= link_ctr - 7'd34;
     //       if (link_ctr == 7'd34)
     //         sm_link  <= ST_LINK_DOWN;
     //     end
     //     else begin
     //       link_ctr <= 7'd0;
     //       sm_link  <= ST_LINK_DOWN;
     //     end
     //   end
      //  else begin
     //     if (link_ctr < 7'd67)
     //       link_ctr <= link_ctr + 7'd1;
    //    end
   //   end
  //  endcase
 // end

  // Synchronize the latched link down reset input and the VIO-driven signal into the free-running clock domain
  wire link_down_latched_reset_vio_int;
  wire link_down_latched_reset_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_link_down_latched_reset_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (link_down_latched_reset_in || link_down_latched_reset_vio_int),
    .o_out  (link_down_latched_reset_sync)
  );

  // Reset the latched link down indicator when the synchronized latched link down reset signal is high. Otherwise, set
  // the latched link down indicator upon losing link. This indicator is available for user reference.
  //always @(posedge hb_gtwiz_reset_clk_freerun_buf_int) begin
  //  if (link_down_latched_reset_sync)
 //     link_down_latched_out <= 1'b0;
 //   else if (!sm_link)
 //     link_down_latched_out <= 1'b1;
//  end

  // Assign the link status indicator to the top-level two-state output for user reference
  //assign link_status_out = sm_link;


  // ===================================================================================================================
  // INITIALIZATION
  // ===================================================================================================================

  // Declare the receiver reset signals that interface to the reset controller helper block. For this configuration,
  // which uses the same PLL type for transmitter and receiver, the "reset RX PLL and datapath" feature is not used.
  wire hb_gtwiz_reset_rx_pll_and_datapath_int = 1'b0;
  wire hb_gtwiz_reset_rx_datapath_int;

  // Declare signals which connect the VIO instance to the initialization module for debug purposes
  wire       init_done_int;
  wire [3:0] init_retry_ctr_int;

  // Combine the receiver reset signals form the initialization module and the VIO to drive the appropriate reset
  // controller helper block reset input
  wire hb_gtwiz_reset_rx_pll_and_datapath_vio_int;
  wire hb_gtwiz_reset_rx_datapath_vio_int;
  wire hb_gtwiz_reset_rx_datapath_init_int;

  assign hb_gtwiz_reset_rx_datapath_int = hb_gtwiz_reset_rx_datapath_init_int || hb_gtwiz_reset_rx_datapath_vio_int;

  // The example initialization module interacts with the reset controller helper block and other example design logic
  // to retry failed reset attempts in order to mitigate bring-up issues such as initially-unavilable reference clocks
  // or data connections. It also resets the receiver in the event of link loss in an attempt to regain link, so please
  // note the possibility that this behavior can have the effect of overriding or disturbing user-provided inputs that
  // destabilize the data stream. It is a demonstration only and can be modified to suit your system needs.
  gtwizard_ultrascale_0_example_init example_init_inst (
    .clk_freerun_in  (hb_gtwiz_reset_clk_freerun_buf_int),
    .reset_all_in    (hb_gtwiz_reset_all_int),
    .tx_init_done_in (gtwiz_reset_tx_done_int),
    .rx_init_done_in (gtwiz_reset_rx_done_int),
    .rx_data_good_in (sm_link),
    .reset_all_out   (hb_gtwiz_reset_all_init_int),
    .reset_rx_out    (hb_gtwiz_reset_rx_datapath_init_int),
    .init_done_out   (init_done_int),
    .retry_ctr_out   (init_retry_ctr_int)
  );


  // ===================================================================================================================
  // VIO FOR HARDWARE BRING-UP AND DEBUG
  // ===================================================================================================================

  // Synchronize gtpowergood into the free-running clock domain for VIO usage
  wire [7:0] gtpowergood_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[0]),
    .o_out  (gtpowergood_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[1]),
    .o_out  (gtpowergood_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[2]),
    .o_out  (gtpowergood_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[3]),
    .o_out  (gtpowergood_vio_sync[3])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_4_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[4]),
    .o_out  (gtpowergood_vio_sync[4])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_5_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[5]),
    .o_out  (gtpowergood_vio_sync[5])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_6_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[6]),
    .o_out  (gtpowergood_vio_sync[6])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_7_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[7]),
    .o_out  (gtpowergood_vio_sync[7])
  );

  // Synchronize qpll0lock into the free-running clock domain for VIO usage
  wire [1:0] qpll0lock_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_qpll0lock_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (qpll0lock_int[0]),
    .o_out  (qpll0lock_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_qpll0lock_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (qpll0lock_int[1]),
    .o_out  (qpll0lock_vio_sync[1])
  );

  // Synchronize qpll1lock into the free-running clock domain for VIO usage
  wire [1:0] qpll1lock_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_qpll1lock_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (qpll1lock_int[0]),
    .o_out  (qpll1lock_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_qpll1lock_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (qpll1lock_int[1]),
    .o_out  (qpll1lock_vio_sync[1])
  );

  // Synchronize cplllock into the free-running clock domain for VIO usage
  wire [7:0] cplllock_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_cplllock_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (cplllock_int[0]),
    .o_out  (cplllock_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_cplllock_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (cplllock_int[1]),
    .o_out  (cplllock_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_cplllock_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (cplllock_int[2]),
    .o_out  (cplllock_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_cplllock_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (cplllock_int[3]),
    .o_out  (cplllock_vio_sync[3])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_cplllock_4_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (cplllock_int[4]),
    .o_out  (cplllock_vio_sync[4])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_cplllock_5_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (cplllock_int[5]),
    .o_out  (cplllock_vio_sync[5])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_cplllock_6_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (cplllock_int[6]),
    .o_out  (cplllock_vio_sync[6])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_cplllock_7_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (cplllock_int[7]),
    .o_out  (cplllock_vio_sync[7])
  );

  // Synchronize rxprgdivresetdone into the free-running clock domain for VIO usage
  wire [7:0] rxprgdivresetdone_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxprgdivresetdone_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxprgdivresetdone_int[0]),
    .o_out  (rxprgdivresetdone_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxprgdivresetdone_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxprgdivresetdone_int[1]),
    .o_out  (rxprgdivresetdone_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxprgdivresetdone_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxprgdivresetdone_int[2]),
    .o_out  (rxprgdivresetdone_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxprgdivresetdone_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxprgdivresetdone_int[3]),
    .o_out  (rxprgdivresetdone_vio_sync[3])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxprgdivresetdone_4_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxprgdivresetdone_int[4]),
    .o_out  (rxprgdivresetdone_vio_sync[4])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxprgdivresetdone_5_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxprgdivresetdone_int[5]),
    .o_out  (rxprgdivresetdone_vio_sync[5])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxprgdivresetdone_6_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxprgdivresetdone_int[6]),
    .o_out  (rxprgdivresetdone_vio_sync[6])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxprgdivresetdone_7_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxprgdivresetdone_int[7]),
    .o_out  (rxprgdivresetdone_vio_sync[7])
  );

  // Synchronize txpmaresetdone into the free-running clock domain for VIO usage
  wire [7:0] txpmaresetdone_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[0]),
    .o_out  (txpmaresetdone_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[1]),
    .o_out  (txpmaresetdone_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[2]),
    .o_out  (txpmaresetdone_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[3]),
    .o_out  (txpmaresetdone_vio_sync[3])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_4_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[4]),
    .o_out  (txpmaresetdone_vio_sync[4])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_5_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[5]),
    .o_out  (txpmaresetdone_vio_sync[5])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_6_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[6]),
    .o_out  (txpmaresetdone_vio_sync[6])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_7_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[7]),
    .o_out  (txpmaresetdone_vio_sync[7])
  );

  // Synchronize rxpmaresetdone into the free-running clock domain for VIO usage
  wire [7:0] rxpmaresetdone_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[0]),
    .o_out  (rxpmaresetdone_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[1]),
    .o_out  (rxpmaresetdone_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[2]),
    .o_out  (rxpmaresetdone_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[3]),
    .o_out  (rxpmaresetdone_vio_sync[3])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_4_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[4]),
    .o_out  (rxpmaresetdone_vio_sync[4])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_5_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[5]),
    .o_out  (rxpmaresetdone_vio_sync[5])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_6_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[6]),
    .o_out  (rxpmaresetdone_vio_sync[6])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_7_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[7]),
    .o_out  (rxpmaresetdone_vio_sync[7])
  );

  // Synchronize gtwiz_reset_tx_done into the free-running clock domain for VIO usage
  wire [0:0] gtwiz_reset_tx_done_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtwiz_reset_tx_done_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtwiz_reset_tx_done_int[0]),
    .o_out  (gtwiz_reset_tx_done_vio_sync[0])
  );

  // Synchronize gtwiz_reset_rx_done into the free-running clock domain for VIO usage
  wire [0:0] gtwiz_reset_rx_done_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_gtwiz_reset_rx_done_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtwiz_reset_rx_done_int[0]),
    .o_out  (gtwiz_reset_rx_done_vio_sync[0])
  );

  // Synchronize rxbufstatus into the free-running clock domain for VIO usage
  wire [23:0] rxbufstatus_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[0]),
    .o_out  (rxbufstatus_vio_sync[0])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[1]),
    .o_out  (rxbufstatus_vio_sync[1])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[2]),
    .o_out  (rxbufstatus_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[3]),
    .o_out  (rxbufstatus_vio_sync[3])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_4_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[4]),
    .o_out  (rxbufstatus_vio_sync[4])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_5_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[5]),
    .o_out  (rxbufstatus_vio_sync[5])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_6_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[6]),
    .o_out  (rxbufstatus_vio_sync[6])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_7_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[7]),
    .o_out  (rxbufstatus_vio_sync[7])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_8_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[8]),
    .o_out  (rxbufstatus_vio_sync[8])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_9_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[9]),
    .o_out  (rxbufstatus_vio_sync[9])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_10_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[10]),
    .o_out  (rxbufstatus_vio_sync[10])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_11_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[11]),
    .o_out  (rxbufstatus_vio_sync[11])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_12_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[12]),
    .o_out  (rxbufstatus_vio_sync[12])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_13_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[13]),
    .o_out  (rxbufstatus_vio_sync[13])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_14_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[14]),
    .o_out  (rxbufstatus_vio_sync[14])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_15_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[15]),
    .o_out  (rxbufstatus_vio_sync[15])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_16_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[16]),
    .o_out  (rxbufstatus_vio_sync[16])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_17_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[17]),
    .o_out  (rxbufstatus_vio_sync[17])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_18_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[18]),
    .o_out  (rxbufstatus_vio_sync[18])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_19_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[19]),
    .o_out  (rxbufstatus_vio_sync[19])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_20_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[20]),
    .o_out  (rxbufstatus_vio_sync[20])
  );

  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_21_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[21]),
    .o_out  (rxbufstatus_vio_sync[21])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_22_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[22]),
    .o_out  (rxbufstatus_vio_sync[22])
  );
  (* DONT_TOUCH = "TRUE" *)
  gtwizard_ultrascale_0_example_bit_synchronizer bit_synchronizer_vio_rxbufstatus_23_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxbufstatus_int[23]),
    .o_out  (rxbufstatus_vio_sync[23])
  );


  // Instantiate the VIO IP core for hardware bring-up and debug purposes, connecting relevant debug and analysis
  // signals which have been enabled during Wizard IP customization. This initial set of connected signals is
  // provided as a convenience and example, but more or fewer ports can be used as needed; simply re-customize and
  // re-generate the VIO instance, then connect any exposed signals that are needed. Signals which are synchronous to
  // clocks other than the free-running clock will require synchronization. For usage, refer to Vivado Design Suite
  // User Guide: Programming and Debugging (UG908)
//  gtwizard_ultrascale_0_vio_0 gtwizard_ultrascale_0_vio_0_inst (
//    .clk (hb_gtwiz_reset_clk_freerun_buf_int)
//    ,.probe_in0 (link_status_int)
//    ,.probe_in1 (link_down_latched_int)
//    ,.probe_in2 (init_done_int)
//    ,.probe_in3 (init_retry_ctr_int)
//    ,.probe_in4 (rxbyteisaligned_int)
//    ,.probe_in5 (qpll0lock_vio_sync)
//    ,.probe_in6 (gtwiz_userclk_rx_active_int)
//    ,.probe_in7 (rxbufstatus_vio_sync)
//    ,.probe_in8 (rxprgdivresetdone_vio_sync)
//    ,.probe_in9 (txpmaresetdone_vio_sync)
//    ,.probe_in10 (rxpmaresetdone_vio_sync)
//    ,.probe_in11 (gtwiz_reset_tx_done_vio_sync)
//    ,.probe_in12 (gtwiz_reset_rx_done_vio_sync)
//    ,.probe_in13 (gtwiz_userdata_rx_int)
//    ,.probe_in14 (gtwiz_userclk_rx_usrclk_int)
//    ,.probe_in15 (gtwiz_userclk_rx_usrclk2_int)
//    ,.probe_in16 (gtwiz_reset_rx_cdr_stable_int)
//    ,.probe_in17 (gtwiz_reset_rx_done_int)
//    ,.probe_in18 (rxbyterealign_int)
//    ,.probe_in19 (rxcommadet_int)
//   ,.probe_in20 (rxdatavalid_int)
//    ,.probe_in21 (rxheader_int)
//    ,.probe_in22 (rxheadervalid_int)
//    ,.probe_in23 (gtwiz_userclk_rx_usrclk_int)
//    ,.probe_in24 (rxpcommaalignen_int)
//    ,.probe_in25 (rxcommadeten_int)
//    ,.probe_in26 (rx8b10ben_int)
    


    
   

//    ,.probe_out0 (hb_gtwiz_reset_all_vio_int)
//    ,.probe_out1 (hb0_gtwiz_reset_tx_pll_and_datapath_int)
//    ,.probe_out2 (hb0_gtwiz_reset_tx_datapath_int)
//    ,.probe_out3 (hb_gtwiz_reset_rx_pll_and_datapath_vio_int)
//    ,.probe_out4 (hb_gtwiz_reset_rx_datapath_vio_int)
//    ,.probe_out5 (link_down_latched_reset_vio_int)
//    ,.probe_out6 (txpmareset_int)
//    ,.probe_out7 (rxpmareset_int)
//    ,.probe_out8 (txpcsreset_int)
//    ,.probe_out9 (gtwiz_userclk_tx_reset_in)
//    ,.probe_out10 (gtwiz_userclk_rx_reset_in)
//    ,.probe_out11 (rxcdrreset_int)
//  );


  // ===================================================================================================================
  // EXAMPLE WRAPPER INSTANCE
  // ===================================================================================================================

  // Instantiate the example design wrapper, mapping its enabled ports to per-channel internal signals and example
  // resources as appropriate
  gtwizard_ultrascale_0_example_wrapper example_wrapper_inst (
    .gtyrxn_in                               (gtyrxn_int)
   ,.gtyrxp_in                               (gtyrxp_int)
   ,.gtytxn_out                              (gtytxn_int)
   ,.gtytxp_out                              (gtytxp_int)
   ,.gtwiz_userclk_tx_reset_in               (gtwiz_userclk_tx_reset_int)
   ,.gtwiz_userclk_tx_srcclk_out             (gtwiz_userclk_tx_srcclk_int)
   ,.gtwiz_userclk_tx_usrclk_out             (gtwiz_userclk_tx_usrclk_int)
   ,.gtwiz_userclk_tx_usrclk2_out            (gtwiz_userclk_tx_usrclk2_int)
   ,.gtwiz_userclk_tx_active_out             (gtwiz_userclk_tx_active_int)
   ,.gtwiz_userclk_rx_reset_in               (gtwiz_userclk_rx_reset_int)
   ,.gtwiz_userclk_rx_srcclk_out             (gtwiz_userclk_rx_srcclk_int)
   ,.gtwiz_userclk_rx_usrclk_out             (gtwiz_userclk_rx_usrclk_int)
   ,.gtwiz_userclk_rx_usrclk2_out            (gtwiz_userclk_rx_usrclk2_int)
   ,.gtwiz_userclk_rx_active_out             (gtwiz_userclk_rx_active_int)
   ,.gtwiz_reset_clk_freerun_in              ({1{hb_gtwiz_reset_clk_freerun_buf_int}})
   ,.gtwiz_reset_all_in                      ({1{hb_gtwiz_reset_all_int}})
   ,.gtwiz_reset_tx_pll_and_datapath_in      (gtwiz_reset_tx_pll_and_datapath_int)
   ,.gtwiz_reset_tx_datapath_in              (gtwiz_reset_tx_datapath_int)
   ,.gtwiz_reset_rx_pll_and_datapath_in      ({1{hb_gtwiz_reset_rx_pll_and_datapath_int}})
   ,.gtwiz_reset_rx_datapath_in              ({1{hb_gtwiz_reset_rx_datapath_int}})
   ,.gtwiz_reset_rx_cdr_stable_out           (gtwiz_reset_rx_cdr_stable_int)
   ,.gtwiz_reset_tx_done_out                 (gtwiz_reset_tx_done_int)
   ,.gtwiz_reset_rx_done_out                 (gtwiz_reset_rx_done_int)
   ,.gtwiz_userdata_tx_in                    (gtwiz_userdata_tx_int)
   ,.gtwiz_userdata_rx_out                   (gtwiz_userdata_rx_int)
   ,.bgbypassb_in                            (bgbypassb_int)
   ,.bgmonitorenb_in                         (bgmonitorenb_int)
   ,.bgpdb_in                                (bgpdb_int)
   ,.bgrcalovrd_in                           (bgrcalovrd_int)
   ,.bgrcalovrdenb_in                        (bgrcalovrdenb_int)
   ,.gtnorthrefclk00_in                      (gtnorthrefclk00_int)
   ,.gtnorthrefclk01_in                      (gtnorthrefclk01_int)
   ,.gtnorthrefclk10_in                      (gtnorthrefclk10_int)
   ,.gtnorthrefclk11_in                      (gtnorthrefclk11_int)
   ,.gtrefclk00_in                           (gtrefclk00_int)
   ,.gtrefclk01_in                           (gtrefclk01_int)
   ,.gtrefclk10_in                           (gtrefclk10_int)
   ,.gtrefclk11_in                           (gtrefclk11_int)
   ,.gtsouthrefclk00_in                      (gtsouthrefclk00_int)
   ,.gtsouthrefclk01_in                      (gtsouthrefclk01_int)
   ,.gtsouthrefclk10_in                      (gtsouthrefclk10_int)
   ,.gtsouthrefclk11_in                      (gtsouthrefclk11_int)
   ,.pmarsvd0_in                             (pmarsvd0_int)
   ,.pmarsvd1_in                             (pmarsvd1_int)
   ,.qpll0clkrsvd0_in                        (qpll0clkrsvd0_int)
   ,.qpll0clkrsvd1_in                        (qpll0clkrsvd1_int)
   ,.qpll0lockdetclk_in                      (qpll0lockdetclk_int)
   ,.qpll0locken_in                          (qpll0locken_int)
   ,.qpll0pd_in                              (qpll0pd_int)
   ,.qpll0refclksel_in                       (qpll0refclksel_int)
   ,.qpll1clkrsvd0_in                        (qpll1clkrsvd0_int)
   ,.qpll1clkrsvd1_in                        (qpll1clkrsvd1_int)
   ,.qpll1lockdetclk_in                      (qpll1lockdetclk_int)
   ,.qpll1locken_in                          (qpll1locken_int)
   ,.qpll1pd_in                              (qpll1pd_int)
   ,.qpll1refclksel_in                       (qpll1refclksel_int)
   ,.qpllrsvd1_in                            (qpllrsvd1_int)
   ,.qpllrsvd2_in                            (qpllrsvd2_int)
   ,.qpllrsvd3_in                            (qpllrsvd3_int)
   ,.qpllrsvd4_in                            (qpllrsvd4_int)
   ,.rcalenb_in                              (rcalenb_int)
   ,.sdm0data_in                             (sdm0data_int)
   ,.sdm0reset_in                            (sdm0reset_int)
   ,.sdm0width_in                            (sdm0width_int)
   ,.sdm1data_in                             (sdm1data_int)
   ,.sdm1reset_in                            (sdm1reset_int)
   ,.sdm1width_in                            (sdm1width_int)
   ,.qpll0fbclklost_out                      (qpll0fbclklost_int)
   ,.qpll0lock_out                           (qpll0lock_int)
   ,.qpll0outclk_out                         (qpll0outclk_int)
   ,.qpll0outrefclk_out                      (qpll0outrefclk_int)
   ,.qpll0refclklost_out                     (qpll0refclklost_int)
   ,.qpll1fbclklost_out                      (qpll1fbclklost_int)
   ,.qpll1lock_out                           (qpll1lock_int)
   ,.qpll1outclk_out                         (qpll1outclk_int)
   ,.qpll1outrefclk_out                      (qpll1outrefclk_int)
   ,.qpll1refclklost_out                     (qpll1refclklost_int)
   ,.qplldmonitor0_out                       (qplldmonitor0_int)
   ,.qplldmonitor1_out                       (qplldmonitor1_int)
   ,.refclkoutmonitor0_out                   (refclkoutmonitor0_int)
   ,.refclkoutmonitor1_out                   (refclkoutmonitor1_int)
   ,.cfgreset_in                             (cfgreset_int)
   ,.cplllockdetclk_in                       (cplllockdetclk_int)
   ,.cplllocken_in                           (cplllocken_int)
   ,.cpllrefclksel_in                        (cpllrefclksel_int)
   ,.cpllreset_in                            (cpllreset_int)
   ,.eyescanreset_in                         (eyescanreset_int)
   ,.gtgrefclk_in                            (gtgrefclk_int)
   ,.gtnorthrefclk0_in                       (gtnorthrefclk0_int)
   ,.gtnorthrefclk1_in                       (gtnorthrefclk1_int)
   ,.gtrefclk0_in                            (gtrefclk0_int)
   ,.gtrefclk1_in                            (gtrefclk1_int)
   ,.gtsouthrefclk0_in                       (gtsouthrefclk0_int)
   ,.gtsouthrefclk1_in                       (gtsouthrefclk1_int)
   ,.resetovrd_in                            (resetovrd_int)
   ,.rx8b10ben_in                            (rx8b10ben_int)
   ,.rxbufreset_in                           (rxbufreset_int)
   ,.rxcdrfreqreset_in                       (rxcdrfreqreset_int)
   ,.rxcdrreset_in                           (rxcdrreset_int)
   ,.rxcommadeten_in                         (rxcommadeten_int)
   ,.rxdfelpmreset_in                        (rxdfelpmreset_int)
   ,.rxdlybypass_in                          (rxdlybypass_int)
   ,.rxgearboxslip_in                        (rxgearboxslip_int)
   ,.rxlatclk_in                             (rxlatclk_int)
   ,.rxmcommaalignen_in                      (rxmcommaalignen_int)
   ,.rxoobreset_in                           (rxoobreset_int)
   ,.rxoscalreset_in                         (rxoscalreset_int)
   ,.rxoutclksel_in                          (rxoutclksel_int)
   ,.rxpcommaalignen_in                      (rxpcommaalignen_int)
   ,.rxpcsreset_in                           (rxpcsreset_int)
   ,.rxpllclksel_in                          (rxpllclksel_int)
   ,.rxpmareset_in                           (rxpmareset_int)
   ,.rxrate_in                               (rxrate_int)
   ,.rxratemode_in                           (rxratemode_int)
   ,.rxslide_in                              (rxslide_int)
   ,.rxsysclksel_in                          (rxsysclksel_int)
   ,.tx8b10ben_in                            (tx8b10ben_int)
   ,.txctrl0_in                              (txctrl0_int)
   ,.txctrl1_in                              (txctrl1_int)
   ,.txctrl2_in                              (txctrl2_int)
   ,.txpcsreset_in                           (txpcsreset_int)
   ,.txpllclksel_in                          (txpllclksel_int)
   ,.txpmareset_in                           (txpmareset_int)
   ,.txsysclksel_in                          (txsysclksel_int)
   ,.cpllfbclklost_out                       (cpllfbclklost_int)
   ,.cplllock_out                            (cplllock_int)
   ,.cpllrefclklost_out                      (cpllrefclklost_int)
   ,.gtpowergood_out                         (gtpowergood_int)
   ,.gtrefclkmonitor_out                     (gtrefclkmonitor_int)
   ,.pcsrsvdout_out                          (pcsrsvdout_int)
   ,.resetexception_out                      (resetexception_int)
   ,.rxbufstatus_out                         (rxbufstatus_int)
   ,.rxbyteisaligned_out                     (rxbyteisaligned_int)
   ,.rxbyterealign_out                       (rxbyterealign_int)
   ,.rxcommadet_out                          (rxcommadet_int)
   ,.rxctrl0_out                             (rxctrl0_int)
   ,.rxctrl1_out                             (rxctrl1_int)
   ,.rxctrl2_out                             (rxctrl2_int)
   ,.rxctrl3_out                             (rxctrl3_int)
   ,.rxdata_out                              (rxdata_int)
   ,.rxdataextendrsvd_out                    (rxdataextendrsvd_int)
   ,.rxdatavalid_out                         (rxdatavalid_int)
   ,.rxheader_out                            (rxheader_int)
   ,.rxheadervalid_out                       (rxheadervalid_int)
   ,.rxosintdone_out                         (rxosintdone_int)
   ,.rxoutclkfabric_out                      (rxoutclkfabric_int)
   ,.rxoutclkpcs_out                         (rxoutclkpcs_int)
   ,.rxpmaresetdone_out                      (rxpmaresetdone_int)
   ,.rxprgdivresetdone_out                   (rxprgdivresetdone_int)
   ,.rxratedone_out                          (rxratedone_int)
   ,.rxresetdone_out                         (rxresetdone_int)
   ,.rxstartofseq_out                        (rxstartofseq_int)
   ,.txpmaresetdone_out                      (txpmaresetdone_int)
   ,.txresetdone_out                         (txresetdone_int)
);

  assign rxcommadet_out = rxcommadet_int;
  assign gtwiz_userdata_rx_out = gtwiz_userdata_rx_int;
  assign gtwiz_userclk_rx_usrclk_out = gtwiz_userclk_rx_usrclk_int;
  assign rxbyteisaligned_out = rxbyteisaligned_int;
  assign rxctrl0_out = rxctrl0_int;
  assign rxctrl1_out = rxctrl1_int;
  assign rxctrl2_out = rxctrl2_int;
  assign rxctrl3_out = rxctrl3_int;
  assign gtwiz_reset_rx_cdr_stable_out = gtwiz_reset_rx_cdr_stable_int;
  assign rxpcommaalignen_out = rxpcommaalignen_int;


endmodule
