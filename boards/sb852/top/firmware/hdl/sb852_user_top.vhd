-- user top level for SB852 scouting project --

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.algo_decl.all;
use work.datatypes.all;
use work.scouting_register_constants.all;

use std.textio.all;
use work.test_file_io.all;

entity scout_top is
    port (

    --  Picostreams
    clk                    : in std_logic;  -- 250 MHz clock from Micron infra
    m_dla_fifo_axis_tready : in std_logic; 
    s2o_rdy                : in  std_logic;
    
       -- pico stream in 2
    s2i_valid   : in  std_logic;
    s2i_rdy     : out std_logic;
    s2i_data    : in  std_logic_vector(255 downto 0);
    s2i_ow_dvld : in  std_logic_vector(1 downto 0);

    -- pico stream out 2
    s2o_valid   : out std_logic;
    s2o_data    : out std_logic_vector(255 downto 0);
    s2o_ow_dvld : out std_logic_vector(1 downto 0);
   
    ------------For DLA ---------
    m_dla_fifo_axis_tvalid : out std_logic; 
    m_dla_fifo_axis_tkeep  : out std_logic_vector(255 downto 0); 
    m_dla_fifo_axis_tdata  : out std_logic_vector(2047 downto 0); 
    m_dla_fifo_axis_tlast  : out std_logic; 
       
    rst : in std_logic;
   
    extra_clk : in std_logic; --200 MHz clock from external

    -- These are the standard PicoBus signals that we'll use to communicate
    -- with the rest of the system.
    PicoClk     : in  std_logic;   -- really slow clock 
    PicoRst     : in  std_logic;
    PicoRd      : in  std_logic;
    PicoWr      : in  std_logic;
    PicoAddr    : in  std_logic_vector(31 downto 0);
    PicoDataIn  : in  std_logic_vector(31 downto 0);
    PicoDataOut : out std_logic_vector(31 downto 0);

    -- MGT ref clock
    mgtrefclk0_x0y2_p : in std_logic;
    mgtrefclk0_x0y2_n : in std_logic;
    mgtrefclk0_x0y4_p : in std_logic;
    mgtrefclk0_x0y4_n : in std_logic;

    -- Serial data ports for transceiver channel 0
    ch0_gtyrxn_in  : in  std_logic;
    ch0_gtyrxp_in  : in  std_logic;
    ch0_gtytxn_out : out std_logic;
    ch0_gtytxp_out : out std_logic;

    -- Serial data ports for transceiver channel 1
    ch1_gtyrxn_in  : in  std_logic;
    ch1_gtyrxp_in  : in  std_logic;
    ch1_gtytxn_out : out std_logic;
    ch1_gtytxp_out : out std_logic;

    -- Serial data ports for transceiver channel 2
    ch2_gtyrxn_in  : in  std_logic;
    ch2_gtyrxp_in  : in  std_logic;
    ch2_gtytxn_out : out std_logic;
    ch2_gtytxp_out : out std_logic;

    -- Serial data ports for transceiver channel 3
    ch3_gtyrxn_in  : in  std_logic;
    ch3_gtyrxp_in  : in  std_logic;
    ch3_gtytxn_out : out std_logic;
    ch3_gtytxp_out : out std_logic;

    -- Serial data ports for transceiver channel 4
    ch4_gtyrxn_in  : in  std_logic;
    ch4_gtyrxp_in  : in  std_logic;
    ch4_gtytxn_out : out std_logic;
    ch4_gtytxp_out : out std_logic;

    -- Serial data ports for transceiver channel 5
    ch5_gtyrxn_in  : in  std_logic;
    ch5_gtyrxp_in  : in  std_logic;
    ch5_gtytxn_out : out std_logic;
    ch5_gtytxp_out : out std_logic;

    -- Serial data ports for transceiver channel 6
    ch6_gtyrxn_in  : in  std_logic;
    ch6_gtyrxp_in  : in  std_logic;
    ch6_gtytxn_out : out std_logic;
    ch6_gtytxp_out : out std_logic;

    -- Serial data ports for transceiver channel 7
    ch7_gtyrxn_in  : in  std_logic;
    ch7_gtyrxp_in  : in  std_logic;
    ch7_gtytxn_out : out std_logic;
    ch7_gtytxp_out : out std_logic;

    -- I2C 
    scl : inout std_logic_vector(1 downto 0);  -- serial clk
    sda : inout std_logic_vector(1 downto 0)   -- serial data
    );
end scout_top;


architecture Behavioral of scout_top is

    ----------------------  FOR SIMULATION---------------------------
    signal clk_sim : std_logic;
    signal m_dla_fifo_axis_tready_sim : std_logic := '1';
    signal s2o_rdy_sim : std_logic;
    -----------------------------------------------------------------

    -- various clocks
    signal clk_freerun, link_clk : std_logic;

    -- data paths between blocks
    signal d_gap_cleaner, d_align : ldata(7 downto 0);
    signal d_zs, q_zs, d_package, d_package_delayed, q_bril, d_bril, d_fifofiller, q_normalizer, d_algo, d_algo_reg, q_bril_buf, d_calib, d_trailer, d_bx, d_bx_reg : adata(7 downto 0);
    
    -- pico data out instances
    signal PicoDataOutCommon, PicoDataOutDemux, PicoDataOutBril : std_logic_vector(31 downto 0);
 
    -- control words between blocks
    signal d_ctrl_zs, q_ctrl_zs, d_ctrl_package, d_ctrl_package_delayed, d_ctrl_bril, d_ctrl_fifofiller, d_ctrl_calib, d_ctrl_trailer : acontrol;
    signal q_ctrl_normalizer, q_ctrl_bril, q_ctrl_bril_buf, d_ctrl_algo, d_ctrl_algo_reg, d_ctrl_bx, d_ctrl_bx_reg : acontrol;

    -- link data
    signal rx_data     : std_logic_vector(8 * 32 - 1 downto 0);
    signal rxctrl0_int : std_logic_vector(63 downto 0);
    signal rxctrl1_int : std_logic_vector(63 downto 0);
    signal rxctrl2_int : std_logic_vector(31 downto 0);
    signal rxctrl3_int : std_logic_vector(31 downto 0);

    -- data path for DLA
    signal mu_packet, mu_packet_r : std_logic_vector(2047 downto 0);

    -- various reset signals
    signal rst_aligner, rst_packager, rst_packager_n, rst_external : std_logic;

   
    signal stream_enable_mask : std_logic_vector(7 downto 0);
    signal waiting_orbit_end  : std_logic;

    signal mu_packet_valid, mu_packet_valid_r : std_logic := '0';


    signal m_axis_c2h_tvalid_0, m_axis_c2h_tready_0, m_axis_c2h_tlast_0 : std_logic;
    signal m_axis_c2h_tvalid_0_delayed, m_axis_c2h_tready_0_delayed, m_axis_c2h_tlast_0_delayed : std_logic;
    signal m_axis_c2h_tdata_0, m_axis_c2h_tdata_0_delayed : std_logic_vector(255 downto 0);
    signal m_axis_c2h_tkeep_0, m_axis_c2h_tkeep_0_delayed : std_logic_vector(31 downto 0);

    signal freq_mgtref_x0y2, freq_mgtref_x0y4, freq_link_clk : std_logic_vector(31 downto 0);
    
    signal mgtrefclks : std_logic_vector(N_REGION -1 downto 0);

    signal lid : TAuxInfo(4*7 downto 0);
    signal crc : TAuxInfo(4*7 downto 0);
  
    signal orbit_length_cnt       : unsigned(63 downto 0);

    signal data_rd_0, data_rd_1   : std_logic_vector(7 downto 0);
  
    -- link related signals
    signal hb_gtwiz_reset_clk_freerun_in              : std_logic;
    signal hb_gtwiz_reset_all_in                      : std_logic;
    signal link_down_latched_reset_in                 : std_logic;
    signal link_status_out                            : std_logic;
    signal link_down_latched_out                      : std_logic;
    signal hb0_gtwiz_reset_tx_pll_and_datapath_int    : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_datapath_int            : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_pll_and_datapath_int : std_logic_vector(0 downto 0);
    signal link_down_latched_reset_int            : std_logic_vector(0 downto 0);
    signal str_rd_0, str_wr_0, str_rd_1, str_wr_1     : std_logic;
    signal hb_gtwiz_reset_rx_datapath_int         : std_logic_vector(0 downto 0);
    signal cdr_stable                                 : std_logic := '0';

    --Debug output/input
    signal init_done_int                : std_logic_vector(0 downto 0);
    signal init_retry_ctr_int           : std_logic_vector(3 downto 0);
    signal gtpowergood_sync         : std_logic_vector(7 downto 0);
    signal txpmaresetdone_sync      : std_logic_vector(7 downto 0);
    signal rxpmaresetdone_sync      : std_logic_vector(7 downto 0);
    signal gtwiz_reset_tx_done_sync : std_logic_vector(0 downto 0);
    signal gtwiz_reset_rx_done_sync : std_logic_vector(0 downto 0);
    signal sRxbyteisaligned             : std_logic_vector(7 downto 0);
    signal loopback                     : std_logic_vector(3*4*2 - 1 downto 0);
    signal orbits_per_packet, orbits_per_packet_delayed          : std_logic_vector(15 downto 0);
    signal axi_backpressure_seen, axi_backpressure_seen_delayed  : std_logic;
    signal orbit_length, orbit_length_delayed                    : unsigned (63 downto 0);
    signal orbit_exceeds_size, orbit_exceeds_size_delayed        : std_logic;
    signal autorealign_counter, autorealign_counter_delayed      : unsigned (63 downto 0);
    signal filler_orbit_counter, filler_orbit_counter_delayed    : unsigned(63 downto 0);
    signal filler_dropped_orbit_counter, filler_dropped_orbit_counter_delayed : unsigned(63 downto 0);
  
    --in/out from picobus
    signal rst_external_picobus : std_logic := '0';
    signal link_enable_picobus  : std_logic;
    signal blocked_ro           : std_logic_vector(31 downto 0) := x"00000000";
    signal disable_zs           : std_logic;
    signal trigger_ila          : std_logic;
    signal trigger_bril_readout : std_logic;

    signal hb_gtwiz_reset_all_int : std_logic_vector(0 downto 0);

    -- MGT clock freq measurements
    signal mgtrefclk0_x0y2_int, mgtrefclk0_x0y4_int           : std_logic;
    signal mgtrefclk0_x0y2_int_meas, mgtrefclk0_x0y4_int_meas : std_logic;
    signal mgtrefclk0_x0y2_int_buf, mgtrefclk0_x0y4_int_buf   : std_logic;
 
    -- signals for DLA fifo
    signal s_dla_fifo_axis_tready : std_logic;

    signal q_ctrl_normalizer_valid : std_logic := '0';
  
    -- Ctrl reg - filled by picobus inputs
    signal ctrl_reg, ctrl_reg_delayed : SRegister((2 ** moni_ctrl_register_address_width) - 1 downto 0);
    
    -- For simulation
    constant t_period : time := 4 ns;
    constant t_halfperiod : time := t_period / 2;
    constant t_hold : time := t_period / 5;
    signal enable : std_logic_vector(7 downto 0);

    component BUFG_GT
        port(I : in std_logic; O : out std_logic);
    end component;

    component axi_distfifo_256x16
        port (
            s_aclk        : in std_logic;
            s_aresetn     : in std_logic;
            s_axis_tvalid : in std_logic;
            s_axis_tready : out std_logic;
            s_axis_tdata  : in std_logic_vector(255 downto 0);
            s_axis_tkeep  : in std_logic_vector(31 downto 0);
            s_axis_tlast  : in std_logic;
            s_axis_tuser  : in std_logic_vector(  1 downto 0);

            m_axis_tvalid : out std_logic;
            m_axis_tready : in std_logic;
            m_axis_tdata  : out std_logic_vector(255 downto 0);
            m_axis_tkeep  : out std_logic_vector(31 downto 0);
            m_axis_tlast  : out std_logic;
            m_axis_tuser  : out std_logic_vector(  1 downto 0)
        );
    end component;

    component axi_packet_fifo_2kx4k_generator
        port (
            s_aclk        : in  std_logic;
            s_aresetn     : in  std_logic;
            s_axis_tvalid : in  std_logic;
            s_axis_tready : out std_logic;
            s_axis_tdata  : in  std_logic_vector(2047 downto 0);
            s_axis_tkeep  : in  std_logic_vector(255 downto 0);
            s_axis_tlast  : in  std_logic;
            m_axis_tvalid : out std_logic;
            m_axis_tready : in  std_logic;
            m_axis_tdata  : out std_logic_vector(2047 downto 0);
            m_axis_tkeep  : out std_logic_vector(255 downto 0);
            m_axis_tlast  : out std_logic
        );
    end component; 

    component ila_0
        port (
            clk    : in std_logic;
            probe0 : in std_logic_vector(255 downto 0);
            probe1 : in std_logic_vector(255 downto 0);
            probe2 : in std_logic_vector(3 downto 0);
            probe3 : in std_logic_vector(15 downto 0);
            probe4 : in std_logic_vector(0 downto 0);
            probe5 : in std_logic_vector(0 downto 0);
            probe6 : in std_logic_vector(0 downto 0);
            probe7 : in std_logic_vector(31 downto 0);
            probe8 : in std_logic_vector(31 downto 0)
        );
    end component;

begin
non_simulated_code : if SIM = false generate
    -- Differential reference clock buffer for mgtrefclk0
    IBUFDS_GTE4_MGTREFCLK0_X0Y2_INST : IBUFDS_GTE4
        generic map (
            REFCLK_EN_TX_PATH  => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX    => "00")
        port map (
            I     => mgtrefclk0_x0y2_p,
            IB    => mgtrefclk0_x0y2_n,
            CEB   => '0',
            O     => mgtrefclk0_x0y2_int,
            ODIV2 => mgtrefclk0_x0y2_int_buf
        );

    -- Differential reference clock buffer for mgtrefclk0
    IBUFDS_GTE4_MGTREFCLK0_X0Y4_INST : IBUFDS_GTE4
        generic map (
            REFCLK_EN_TX_PATH  => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX    => "00")
        port map (
            I     => mgtrefclk0_x0y4_p,
            IB    => mgtrefclk0_x0y4_n,
            CEB   => '0',
            O     => mgtrefclk0_x0y4_int,
            ODIV2 => mgtrefclk0_x0y4_int_buf
        );
               
    gen_demux_ctrl : if ZS_TYPE = "DEMUX" generate
 
        demux_ctrl_inst : entity work.demux_ctrl
            port map(
                clk        => PicoClk,
                rst        => PicoRst,
                PicoWr     => PicoWr,
                PicoAddr   => PicoAddr,
                PicoDataIn => PicoDataIn,
                ctrl_reg   => ctrl_reg   
            );
        
        demux_moni_inst : entity work.demux_moni
            port map(
                clk         => PicoClk,
                rst         => PicoRst,
                PicoRd      => PicoRd,
                PicoAddr    => PicoAddr,
                PicoDataOut => PicoDataOutDemux,
                ctrl_reg    => ctrl_reg   
            );       
        
    end generate gen_demux_ctrl;
    
    gen_bril_ctrl : if BRIL = true generate
        
        bril_ctrl_inst : entity work.bril_ctrl
            port map(
                clk        => PicoClk,
                rst        => PicoRst,
                PicoWr     => PicoWr,
                PicoAddr   => PicoAddr,
                PicoDataIn => PicoDataIn,
                ctrl_reg   => ctrl_reg
            );
                    
        bril_moni_inst : entity work.bril_moni
            port map(
                clk         => PicoClk,
                rst         => PicoRst,
                PicoRd      => PicoRd,
                PicoAddr    => PicoAddr,
                PicoDataOut => PicoDataOutBril,
                ctrl_reg    => ctrl_reg   
            );       
    end generate gen_bril_ctrl;
         
    PicoDataOut <= PicoDataOutBril when (PicoAddr >= special_addr_boundry and ZS_TYPE = "UGMT" and BRIL = true) else PicoDataOutCommon;
       
    ---------   Register monitoring and control ---------
    reg_mon_ctrl : process(PicoClk)
    begin
    if rising_edge(PicoClk) then
    
        -- PICO WRITES
        if PicoWr = '1' then 
            if PicoAddr = std_logic_vector(to_unsigned(reset_board_address, 32)) then
                rst_external_picobus <= PicoDataIn(0);
                  
            elsif PicoAddr = std_logic_vector(to_unsigned(link_enable_address, 32)) then
                link_enable_picobus <= PicoDataIn(0);
                
            elsif PicoAddr = std_logic_vector(to_unsigned(hb_gtwiz_reset_all_address, 32)) then
                hb_gtwiz_reset_all_int <= PicoDataIn(0 downto 0);               
                          
            elsif PicoAddr = std_logic_vector(to_unsigned(disable_zs_address, 32)) then
                disable_zs <= PicoDataIn(0);
                
            elsif PicoAddr = std_logic_vector(to_unsigned(orbits_per_packet_address, 32)) then
                orbits_per_packet <= PicoDataIn(15 downto 0);
                          
           end if;           
        end if;    
             
    
        -- PICO READS
        if PicoRd = '1' then
            if PicoAddr = std_logic_vector(to_unsigned(deadbeef_address, 32)) then --32b signal
                PicoDataOutCommon <= x"DEADBEEF"; -- sanity/functionality check
                
            elsif PicoAddr = std_logic_vector(to_unsigned(blocked_readout_address, 32)) then --32b signal
                PicoDataOutCommon <= blocked_ro;
                
            elsif PicoAddr = std_logic_vector(to_unsigned(frequency_mgtrefclk_hertz_address, 32)) then -- 32b signal
                PicoDataOutCommon <= freq_mgtref_x0y2; 
                
            elsif PicoAddr = std_logic_vector(to_unsigned(orbits_per_packet_address, 32)) then
                PicoDataOutCommon(orbits_per_packet'length - 1 downto 0) <= orbits_per_packet; 
                PicoDataOutCommon(31 downto orbits_per_packet'length)    <= (others => '0');
                
            elsif PicoAddr = std_logic_vector(to_unsigned(axi_backpressure_seen_address, 32)) then -- 1b signal
                PicoDataOutCommon(0) <= axi_backpressure_seen;
                PicoDataOutCommon(31 downto 1) <= (others => '0'); 
                
            elsif PicoAddr = std_logic_vector(to_unsigned(autorealign_counter_address, 32)) then
                PicoDataOutCommon <= std_logic_vector(autorealign_counter(31 downto 0)); 
                
            elsif PicoAddr = std_logic_vector(to_unsigned(orbit_length_address, 32)) then
                PicoDataOutCommon <= std_logic_vector(orbit_length(31 downto 0));
                
            elsif PicoAddr = std_logic_vector(to_unsigned(filler_orbit_counter_address, 32)) then
                PicoDataOutCommon <= std_logic_vector(filler_orbit_counter(31 downto 0)); -- 64b internal, trunc to 32b       
                
            elsif PicoAddr = std_logic_vector(to_unsigned(filler_dropped_orbit_counter_address, 32)) then
                PicoDataOutCommon <= std_logic_vector(filler_dropped_orbit_counter(31 downto 0));       
                
            elsif PicoAddr = std_logic_vector(to_unsigned(rx_byte_is_aligned_info_address, 32)) then
                PicoDataOutCommon(sRxbyteisaligned'length -1 downto 0)  <= sRxbyteisaligned;
                PicoDataOutCommon(31 downto 8) <= (others => '0');
                
            elsif PicoAddr = std_logic_vector(to_unsigned(cdr_stable_info_address, 32)) then --1b signal
                PicoDataOutCommon(0 )           <= cdr_stable;
                PicoDataOutCommon(31 downto 1)  <= (others => '0');     
                       
            elsif PicoAddr = std_logic_vector(to_unsigned(algo_version_address, 32)) then
                PicoDataOutCommon(ALGO_REV'length -1 downto 0)  <= ALGO_REV;
                PicoDataOutCommon(31 downto ALGO_REV'length) <= (others => '0');     
                     
            elsif PicoAddr = std_logic_vector(to_unsigned(fw_type_address, 32))   then
                PicoDataOutCommon(FW_TYPE'length -1 downto 0)  <= FW_TYPE;
                PicoDataOutCommon(31 downto FW_TYPE'length) <= (others => '0');
                
            elsif PicoAddr = std_logic_vector(to_unsigned(freq_linkclk_hertz_address, 32)) then -- 32b signal
                PicoDataOutCommon <= freq_link_clk;
                              
            ----- In registers that can be read out for monitoring -----
            elsif PicoAddr = std_logic_vector(to_unsigned(reset_board_address, 32)) then -- 1b signal
                PicoDataOutCommon(0) <= rst_external_picobus;
                PicoDataOutCommon(31 downto 1) <= (others => '0');      
                    
            elsif PicoAddr = std_logic_vector(to_unsigned(link_enable_address, 32)) then -- 1b signal
                PicoDataOutCommon(0) <= link_enable_picobus;
                PicoDataOutCommon(31 downto 1) <= (others => '0');
                
            elsif PicoAddr = std_logic_vector(to_unsigned(hb_gtwiz_reset_all_address, 32)) then -- 1b signal
                PicoDataOutCommon(0 downto 0)  <= hb_gtwiz_reset_all_int;
                PicoDataOutCommon(31 downto 1) <= (others => '0');               
                
            elsif PicoAddr = std_logic_vector(to_unsigned(disable_zs_address, 32)) then
                PicoDataOutCommon(0) <= disable_zs; 
                PicoDataOutCommon(31 downto 1) <= (others => '0'); 
                
            elsif PicoAddr = std_logic_vector(to_unsigned(orbits_per_packet_address, 32)) then
                PicoDataOutCommon(15 downto 0) <= orbits_per_packet; 
                PicoDataOutCommon(31 downto 16)<= (others => '0');   
                               
            else
                PicoDataOutCommon <= x"C0DEFA11"; -- address not accesible
            end if;   
        else
            PicoDataOutCommon <= x"00000000"; -- when picoRd = 0
        end if;        
       
        -- PICO RST       
        if(PicoRst) then
            PicoDataOutCommon <= x"00000000";
        end if;
                
    end if;
    
    end process reg_mon_ctrl;
      
        
    rst_external <= rst_external_picobus;    
    
    U6 : BUFG_GT port map (I => mgtrefclk0_x0y4_int_buf, O => mgtrefclk0_x0y4_int_meas);
    U5 : BUFG_GT port map (I => mgtrefclk0_x0y2_int_buf, O => mgtrefclk0_x0y2_int_meas);
    
    
    inputs : entity work.inputs
        generic map(
            N_MGTREFCLKS => 2
        )
        port map(

            axi_clk => clk,
            hb_gtwiz_reset_all_int(0) => rst_external,
    
            mgtrefclks(0) => mgtrefclk0_x0y2_int,
            mgtrefclks(1) => mgtrefclk0_x0y4_int,
    
            gtrxn_in(0)  => ch0_gtyrxn_in,
            gtrxn_in(1)  => ch1_gtyrxn_in,
            gtrxn_in(2)  => ch2_gtyrxn_in,
            gtrxn_in(3)  => ch3_gtyrxn_in,
            gtrxn_in(4)  => ch4_gtyrxn_in,
            gtrxn_in(5)  => ch5_gtyrxn_in,
            gtrxn_in(6)  => ch6_gtyrxn_in,
            gtrxn_in(7)  => ch7_gtyrxn_in,

            gtrxp_in(0)  => ch0_gtyrxp_in,
            gtrxp_in(1)  => ch1_gtyrxp_in,
            gtrxp_in(2)  => ch2_gtyrxp_in,
            gtrxp_in(3)  => ch3_gtyrxp_in,
            gtrxp_in(4)  => ch4_gtyrxp_in,
            gtrxp_in(5)  => ch5_gtyrxp_in,
            gtrxp_in(6)  => ch6_gtyrxp_in,
            gtrxp_in(7)  => ch7_gtyrxp_in,

            gttxn_out(0) => ch0_gtytxn_out,
            gttxn_out(1) => ch1_gtytxn_out,
            gttxn_out(2) => ch2_gtytxn_out,
            gttxn_out(3) => ch3_gtytxn_out,
            gttxn_out(4) => ch4_gtytxn_out,
            gttxn_out(5) => ch5_gtytxn_out,
            gttxn_out(6) => ch6_gtytxn_out,
            gttxn_out(7) => ch7_gtytxn_out,
            
            gttxp_out(0) => ch0_gtytxp_out,   
            gttxp_out(1) => ch1_gtytxp_out,    
            gttxp_out(2) => ch2_gtytxp_out,    
            gttxp_out(3) => ch3_gtytxp_out,   
            gttxp_out(4) => ch4_gtytxp_out,    
            gttxp_out(5) => ch5_gtytxp_out,    
            gttxp_out(6) => ch6_gtytxp_out,   
            gttxp_out(7) => ch7_gtytxp_out,
    
            clk_freerun => extra_clk,
            clk         => link_clk,
    
            q         => d_gap_cleaner,
            oLinkData => rx_data,
    
            init_done_int => init_done_int,    
    
            link_down_latched_reset_int => link_down_latched_reset_int,
    
            txpmaresetdone_sync => txpmaresetdone_sync,
    
            rxctrl0_out => rxctrl0_int,
            rxctrl1_out => rxctrl1_int,
            rxctrl2_out => rxctrl2_int,
            rxctrl3_out => rxctrl3_int,
            
            cdr_stable(0) => cdr_stable,
            oRxbyteisaligned => sRxbyteisaligned,
            
            -- signals used by kcu1500 board, not SB852
            
            hb0_gtwiz_reset_tx_pll_and_datapath_int(0) => '0',
            hb_gtwiz_reset_rx_pll_and_datapath_int(0)  => '0',
            hb0_gtwiz_reset_tx_datapath_int(0)         => '0',
            hb_gtwiz_reset_rx_datapath_int(0)          => '0',
            loopback => (others => '0')
            
    ); 
      
    frequ_meas_link_clk : entity work.freq_meas
        port map (
            clk      => extra_clk,
            rst      => rst_packager,
            clk_meas => link_clk,
            freq     => freq_link_clk
        );       
  
    frequ_meas_mgtref_x0y2 : entity work.freq_meas
        port map (
            clk      => extra_clk,
            rst      => rst_packager,
            clk_meas => mgtrefclk0_x0y2_int_meas,
            freq     => freq_mgtref_x0y2
        ); 

    frequ_meas_mgtref_x0y4 : entity work.freq_meas
        port map (
            clk      => extra_clk,
            rst      => rst_packager,
            clk_meas => mgtrefclk0_x0y4_int_meas,
            freq     => freq_mgtref_x0y4
        ); 
  
    gap_cleaner : entity work.comma_gap_cleaner
        port map(
            clk => link_clk,
            rst => rst_packager,
            d   => d_gap_cleaner,
            q   => d_align
        );
      
end generate non_simulated_code;
 
stream_enable_mask <= x"00" when link_enable_picobus = '0' else x"FF";
  
global_reset : entity work.reset
    port map(
        clk_free     => clk,
        clk_link     => link_clk,
        clk_axi      => clk,
        clk_i2c      => '0',
        rst_global   => rst_external,
        rst_pll      => hb0_gtwiz_reset_tx_pll_and_datapath_int(0),
        rst_tx       => hb0_gtwiz_reset_tx_datapath_int(0),
        rst_rx       => hb_gtwiz_reset_rx_datapath_int(0),
        rst_algo     => open,
        rst_packager => rst_packager
    );

i2c_driver_1 : entity work.i2c_driver
    generic map(
        input_clk => 250_000_000,
        bus_clk   => 400_000
    )
    port map (
        clk     => clk,
        reset   => rst_packager,
        str_wr  => str_wr_0,
        str_rd  => str_rd_0,
        data_rd => data_rd_0,
        sda     => sda(0),
        scl     => scl(0)
    );

i2c_driver_2 : entity work.i2c_driver
    generic map(
        input_clk => 250_000_000,
        bus_clk   => 400_000
    )
    port map (
        clk     => clk,
        reset   => rst_packager,
        str_wr  => str_wr_1,
        str_rd  => str_rd_1,
        data_rd => data_rd_1,
        sda     => sda(1),
        scl     => scl(1)
    );

auto_realign_controller_1 : entity work.auto_realign_controller
    port map (
        axi_clk           => clk,
        rst_in            => rst_packager,
        clk_aligner       => link_clk,
        rst_aligner_out   => rst_aligner,
        misalignment_seen => orbit_exceeds_size_delayed,
        autoreset_count   => autorealign_counter
    );

align : entity work.bx_aware_aligner
    generic map (
        NSTREAMS => 4 * N_REGION)
    port map (
        clk_wr            => link_clk,
        rst               => rst_aligner,
        d                 => d_align,
        enable            => stream_enable_mask,
        waiting_orbit_end => waiting_orbit_end,
        clk_rd            => clk,
        q                 => d_zs,
        q_ctrl            => d_ctrl_zs
    );

d_fifofiller      <= q_zs      when disable_zs = '0' else d_zs;
d_ctrl_fifofiller <= q_ctrl_zs when disable_zs = '0' else d_ctrl_zs;

select_demux : if ZS_TYPE = "DEMUX" generate
    zs_demux : entity work.zs_demux
        generic map(
            NSTREAMS       => 4 * N_REGION,
            REG_ADDR_WIDTH => moni_ctrl_register_address_width
        )
        port map(
            clk      => clk,
            rst      => rst_packager,
            ctrl_reg => ctrl_reg_delayed,
            d        => d_zs,
            d_ctrl   => d_ctrl_zs,
            q        => q_zs,
            q_ctrl   => q_ctrl_zs
        );           
end generate select_demux;

select_ugmt : if ZS_TYPE = "UGMT" generate
    zs_ugmt : entity work.zs_ugmt
        generic map(
            NSTREAMS => 4 * N_REGION
        )
        port map(
            clk    => clk,
            rst    => rst_packager,
            d      => d_zs,
            d_ctrl => d_ctrl_zs,
            q      => q_zs,
            q_ctrl => q_ctrl_zs
        );
end generate; -- select_ugmt

d_bx_reg <= q_zs when disable_zs = '0' else d_zs;
d_ctrl_bx_reg <= q_ctrl_zs when disable_zs = '0' else d_ctrl_zs;

d_bx <= d_bx_reg;
d_ctrl_bx <= d_ctrl_bx_reg;

gen_bx_counter : entity work.bx_counter
    generic map(
        NSTREAMS => 4 * N_REGION
    )
    port map(
        clk      => clk,
        rst      => rst_packager,
        d        => d_bx,
        d_ctrl   => d_ctrl_bx,
        q        => d_calib,
        q_ctrl   => d_ctrl_calib
    );

is_calo_board_for_calib_sup : if ZS_TYPE = "DEMUX" generate
    suppress_calibration_sequence : entity work.suppress_calibration_data
        generic map(
            NSTREAMS       => 4 * N_REGION,
            REG_ADDR_WIDTH => moni_ctrl_register_address_width
        )
        port map(
            clk      => clk,
            rst      => rst_packager,
            ctrl_reg => ctrl_reg_delayed,
            d        => d_calib,
            d_ctrl   => d_ctrl_calib,
            q        => d_trailer,
            q_ctrl   => d_ctrl_trailer
        );
end generate; -- is_calo_board_for_calib_sup

not_calo_board_for_calib_sup : if ZS_TYPE /= "DEMUX" generate
    d_trailer      <= d_calib;
    d_ctrl_trailer <= d_ctrl_calib;
end generate; -- not_calo_board_for_calib_sup

gen_trailer : entity work.filled_bx_trailer_generator
    generic map(
        NSTREAMS => 4 * N_REGION
    )
    port map(
        clk    => clk,
        rst    => rst_packager,
        d      => d_trailer,
        d_ctrl => d_ctrl_trailer,
        q      => d_algo_reg,
        q_ctrl => d_ctrl_algo_reg
    );
   
gen_mdla_fifo : if MDLA = true generate

    normalizer : entity work.normalizer
        generic map ( 
            NSTREAMS => 4 * N_REGION
        )
        port map (
            clk       => clk,
            rst       => rst_packager,
            d         => q_zs,
            d_ctrl    => q_ctrl_zs,
            q         => q_normalizer,
            mu_packet => mu_packet,
            mu_packet_valid => mu_packet_valid
        );
      
    rst_packager_n          <= not rst_packager;
    q_ctrl_normalizer_valid <= q_ctrl_normalizer.valid and q_ctrl_normalizer.strobe;

    mu_packet_r       <= mu_packet;
    mu_packet_valid_r <= mu_packet_valid;
 
    dla_fifo : axi_packet_fifo_2kx4k_generator
        port map (
            --slave side
            s_aclk        => clk,
            s_aresetn     => rst_packager_n,
            s_axis_tvalid => mu_packet_valid_r,
            s_axis_tready => s_dla_fifo_axis_tready,
            s_axis_tdata  => mu_packet_r,
            s_axis_tkeep  => (others => '1'),
            s_axis_tlast  => '0',
            -- master side
            m_axis_tvalid => m_dla_fifo_axis_tvalid,
            m_axis_tready => m_dla_fifo_axis_tready,
            m_axis_tdata  => m_dla_fifo_axis_tdata,
            m_axis_tkeep  => m_dla_fifo_axis_tkeep,
            m_axis_tlast  => m_dla_fifo_axis_tlast
        );

end generate gen_mdla_fifo;
  
gen_bril : if BRIL = true generate
    scout_bril : entity work.scout_bril
        generic map (
            NSTREAMS => 4 * N_REGION)
        port map(
            clk    => clk,
            rst    => rst_packager,
            d      => d_calib,
            d_ctrl => d_ctrl_calib,
            q      => q_bril,
            q_ctrl => q_ctrl_bril,
 --           trigger_bril_readout_i => trigger_bril_readout  
            trigger_bril_readout_i => '0'

        );        
   
    bril_fifo_fill : process(clk)
        begin
            for i in d_package'range loop
                m_axis_c2h_tdata_0(31 + i*32 downto i*32) <= d_package_delayed(i);
            end loop;  
            m_axis_c2h_tvalid_0 <= d_ctrl_package_delayed.valid;
            m_axis_c2h_tkeep_0  <= x"FFFFFFFF";
            m_axis_c2h_tlast_0  <= '0';
        end process;
        
end generate gen_bril;

--- Used to monitor whether the PC is throttling readout 
calc_blocked_readouts : process(clk) is 
begin
    if rising_edge(clk) then 
        if s2o_rdy = '0' and m_axis_c2h_tvalid_0 = '1' then
            blocked_ro <= blocked_ro + '1';
        end if;
    end if;
end process; 

q_bril_buf <= q_bril;
q_ctrl_bril_buf <= q_ctrl_bril;

d_algo      <= q_bril_buf      when BRIL = true else d_algo_reg;
d_ctrl_algo <= q_ctrl_bril_buf when BRIL = true else d_ctrl_algo_reg;  

algo : entity work.algo
generic map(
    NSTREAMS => 4 * N_REGION
)

port map(
    clk    => clk,
    rst    => rst_packager,
    d      => d_algo,
    d_ctrl => d_ctrl_algo,
    q      => d_package,
    q_ctrl => d_ctrl_package
);

       
-- delay line to help with timing 
cross_to_slr1_delay_line : entity work.cross_to_slr1_delay_line
generic map(
    delay_length => 12
)
port map(
    clk                       => clk,
    orbits_per_packet         => orbits_per_packet,
    orbits_per_packet_out     => orbits_per_packet_delayed,
    d_package                 => d_package,
    d_package_out             => d_package_delayed,
    d_ctrl_package            => d_ctrl_package,
    d_ctrl_package_out        => d_ctrl_package_delayed,
    filler_orbit_counter      => filler_orbit_counter,
    filler_orbit_counter_out  => filler_orbit_counter_delayed,
    axi_backpressure_seen     => axi_backpressure_seen,
    axi_backpressure_seen_out => axi_backpressure_seen_delayed,
    orbit_length              => orbit_length,
    orbit_length_out          => orbit_length_delayed,
    orbit_exceeds_size        => orbit_exceeds_size,
    orbit_exceeds_size_out    => orbit_exceeds_size_delayed,
    autorealign_counter       => autorealign_counter,
    autorealign_counter_out   => autorealign_counter_delayed,
    ctrl_reg                  => ctrl_reg,
    ctrl_reg_out              => ctrl_reg_delayed,
    filler_dropped_orbit_counter       => filler_dropped_orbit_counter,
    filler_dropped_orbit_counter_out   => filler_dropped_orbit_counter_delayed
 );


-- Small FIFO that buffers the data 
distrRAM_slr0arrival : axi_distfifo_256x16
port map(
    s_aclk        => clk,
    s_aresetn     => not rst_packager,
    s_axis_tvalid => m_axis_c2h_tvalid_0,
    s_axis_tready => m_axis_c2h_tready_0_delayed,
    s_axis_tdata  => m_axis_c2h_tdata_0,
    s_axis_tkeep  => m_axis_c2h_tkeep_0,
    s_axis_tlast  => m_axis_c2h_tlast_0,
    s_axis_tuser  => (others => '0'), 

    m_axis_tvalid => m_axis_c2h_tvalid_0_delayed,
    m_axis_tready => m_axis_c2h_tready_0,
    m_axis_tdata  => m_axis_c2h_tdata_0_delayed,
    m_axis_tkeep  => m_axis_c2h_tkeep_0_delayed,
    m_axis_tlast  => m_axis_c2h_tlast_0_delayed,
    m_axis_tuser  => open
    );

gen_fifo_filler : if BRIL /= true generate
 
    fifo_filler : entity work.fifo_filler
        generic map(
            NSTREAMS => 4 * N_REGION)
        port map(
            d_clk                  => clk,
            rst                    => rst_packager,
            orbits_per_packet      => unsigned(orbits_per_packet_delayed),
            d                      => d_package_delayed,
            d_ctrl                 => d_ctrl_package_delayed,
            m_aclk                 => clk,
            m_axis_tvalid          => m_axis_c2h_tvalid_0,
            m_axis_tready          => m_axis_c2h_tready_0_delayed,
            m_axis_tdata           => m_axis_c2h_tdata_0,
            m_axis_tkeep           => m_axis_c2h_tkeep_0,
            m_axis_tlast           => m_axis_c2h_tlast_0,
            dropped_orbit_counter  => filler_dropped_orbit_counter,
            orbit_counter          => filler_orbit_counter,
            axi_backpressure_seen  => axi_backpressure_seen,
            orbit_length           => orbit_length,   
            orbit_exceeds_size     => orbit_exceeds_size,
            in_autorealign_counter => autorealign_counter_delayed
            );  
             
end generate gen_fifo_filler;
 
-- sends data out over the pico stream number 2. 
stream_out : process (clk)
begin
    if rising_edge(clk) then
        if(rst_packager = '1') then
            m_axis_c2h_tready_0 <= '0';
        end if;
        
        if(s2o_rdy = '1') then
            m_axis_c2h_tready_0 <= '1';
        else
            m_axis_c2h_tready_0 <= '0';
        end if;

        if BRIL = false then
        
            if (m_axis_c2h_tvalid_0_delayed = '1' and s2o_rdy = '1' and m_axis_c2h_tready_0_delayed = '1') then
                s2o_data(255 downto 0) <= m_axis_c2h_tdata_0_delayed(255 downto 0);
                s2o_valid   <= '1';
                s2o_ow_dvld <= "11";
            else
                s2o_valid   <= '0';
                s2o_ow_dvld <= "00";
            end if;
    
        else --bril = true

            if (m_axis_c2h_tvalid_0_delayed = '1' and s2o_rdy = '1') then
                s2o_data <= m_axis_c2h_tdata_0_delayed;
                s2o_valid   <= '1';
                s2o_ow_dvld <= "11";
            else -- data is not valid *or* outstream is not ready
                s2o_valid   <= '0';
                s2o_ow_dvld <= "00";
            end if;

        end if;
    end if;
end process;
  
-----------------------------------------------
--            FOR SIMULATION ONLY            --
-----------------------------------------------

runtb_gen : if SIM = true generate
    runtb : process
        variable i : integer := 0;
        variable j : integer := 0;
        variable frame : ldata( 7 downto 0);
      
        file F  : text open read_mode is "../../../../../../../../../scripts/common/testfile_5new2.ptrn";
      
        begin
  
        orbits_per_packet <= x"0001";
        disable_zs <= '1';
        stream_enable_mask <= x"FF";
        s_dla_fifo_axis_tready <= '1' after 10 us;
        trigger_bril_readout <= '0';
        s2o_rdy_sim <= '0';
            
        for j in 0 to 7 loop
            enable(j) <= '1';
            d_align(j).data <= (others => '0');
            d_align(j).valid  <= '0';
            d_align(j).strobe <= '0';
            d_align(j).done   <= '0';
        end loop;
        
        for i in 0 to 20000 loop
            link_clk <= '0';
            clk_sim  <= '0';
            wait for t_halfperiod;
            link_clk <= '1';
            clk_sim  <= '1';
            wait for t_hold;
  
            -- change signals i.e mock reset
            if i>=5 and i <=30 then
                rst_external <= '1';
       
            else
                rst_external <= '0';

            end if;
            
            if i >=200 and i <=300 then
                str_wr_1    <= '1';
                s2o_rdy_sim <= '0';
            else
                str_wr_1    <= '0';
                s2o_rdy_sim <= '1';
            end if;
    
            wait for (t_halfperiod - t_hold);
    
        end loop; -- 0-->20000 loop
        
        while ( not endfile(F) ) loop
            link_clk <= '0';
            clk_sim <= '0';
            wait for t_halfperiod;
            link_clk <= '1';
            clk_sim <= '1';
            wait for t_hold;
             
            ReadFrame(f, 8, frame);
            d_align <= frame;
            
            wait for (t_halfperiod - t_hold);
        end loop; -- not endfile loop
  
        for i in 0 to 20000 loop
            link_clk <= '0';
            clk_sim  <= '0';
            wait for t_halfperiod;
            link_clk <= '1';
            clk_sim  <= '1';
            wait for t_hold;
            wait for (t_halfperiod - t_hold);
        end loop;

    end process runtb;  

end generate runtb_gen;

    link_ila : ila_0
        port map(
            clk                    => clk,
            probe0(63 downto 0)    => (others => '0'),
            probe0(95 downto 64)   => (others => '0'),
            probe0(127 downto 96)  => (others => '0'),
            probe0(159 downto 128) => (others => '0'),
            probe0(191 downto 160) => (others => '0'),
            probe0(223 downto 192) => (others => '0'),
            probe0(255 downto 224) => (others => '0'),
            probe1(31 downto 0)    => q_bril(0),
            probe1(63 downto 32)   => q_bril(1),
            probe1(95 downto 64)   => q_bril(2),
            probe1(127 downto 96)  => q_bril(3),
            probe1(159 downto 128) => q_bril(4),
            probe1(191 downto 160) => q_bril(5),
            probe1(223 downto 192) => q_bril(6),
            probe1(255 downto 224) => q_bril(7),
            probe2(0)              => q_ctrl_bril.valid,
            probe2(1)              => q_ctrl_bril.strobe,
            probe2(2)              => q_ctrl_bril.bx_start,
            probe2(3)              => q_ctrl_bril.last,
            probe3(15 downto 0)    => (others => '0'),
            probe4(0)              => trigger_bril_readout,
            probe5(0)              => '0',
            probe6(0)              => rst_packager,
            probe7                 => (others => '0'),
            probe8                 => (others => '0')
        );

end Behavioral;
