library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.top_decl.all;
use work.datatypes.all;
use work.scouting_register_constants.all;

use IEEE.NUMERIC_STD.all;

entity demux_ctrl is
    port (
        clk         : in  std_logic;
        rst         : in  std_logic;
        PicoWr      : in  std_logic;
        PicoAddr    : in std_logic_vector(31 downto 0);
        PicoDataIn  : in std_logic_vector(31 downto 0);
        ctrl_reg    : out SRegister((2**moni_ctrl_register_address_width) -1 downto 0)        
    );
end demux_ctrl;

architecture Behavioral of demux_ctrl is

begin
    demux_ctrl_proc : process (clk, PicoAddr, PicoWr, rst) is
    begin
        if rising_edge(clk) then        
            if PicoWr = '1' then                
                if PicoAddr =  std_logic_vector(to_unsigned(zs_threshold_tau_0p5GeV_address, 32)) then
                    ctrl_reg(zs_threshold_tau_0p5GeV_address)(8 downto 0) <= PicoDataIn(8 downto 0);
                
                elsif PicoAddr =  std_logic_vector(to_unsigned(zs_threshold_sum_0p5GeV_address, 32)) then
                ctrl_reg(zs_threshold_sum_0p5GeV_address)(8 downto 0) <= PicoDataIn(8 downto 0);
               
                elsif PicoAddr =  std_logic_vector(to_unsigned(zs_threshold_empty_0p5GeV_address, 32))then
                    ctrl_reg(zs_threshold_empty_0p5GeV_address)(8 downto 0) <= PicoDataIn(8 downto 0);
                    
                elsif PicoAddr =  std_logic_vector(to_unsigned(zs_threshold_eg_0p5GeV_address, 32))then
                    ctrl_reg(zs_threshold_eg_0p5GeV_address)(8 downto 0) <= PicoDataIn(8 downto 0);              
                    
                elsif PicoAddr =  std_logic_vector(to_unsigned(zs_threshold_jet_0p5GeV_address, 32))then
                    ctrl_reg(zs_threshold_jet_0p5GeV_address)(8 downto 0) <= PicoDataIn(8 downto 0); 
                    
                elsif PicoAddr =  std_logic_vector(to_unsigned(suppress_calibration_sequence_address, 32))then
                    ctrl_reg(suppress_calibration_sequence_address)(0 downto 0) <= PicoDataIn(0 downto 0);
                end if;            
            end if; -- picowr
        end if; -- rising edge
    end process demux_ctrl_proc;
end Behavioral;


