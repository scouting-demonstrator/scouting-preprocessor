library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.top_decl.all;
use work.datatypes.all;
use work.scouting_register_constants.all;

use IEEE.NUMERIC_STD.all;

entity bril_ctrl is
    port (
        clk         : in  std_logic;
        rst         : in  std_logic;
        PicoWr      : in  std_logic;
        PicoAddr    : in std_logic_vector(31 downto 0);
        PicoDataIn  : in std_logic_vector(31 downto 0);
        ctrl_reg    : out SRegister((2**moni_ctrl_register_address_width) -1 downto 0)        
    );
end bril_ctrl;

architecture Behavioral of bril_ctrl is

begin
    bril_ctrl_proc : process (clk, PicoAddr, PicoWr, rst) is
    begin
        if rising_edge(clk) then        
            if PicoWr = '1' then                
                if PicoAddr =  std_logic_vector(to_unsigned(trigger_bril_readout_address, 32)) then
                    ctrl_reg(trigger_bril_readout_address)(0) <= PicoDataIn(0);
                end if;  
            end if; -- picowr
        end if; -- rising edge
    end process bril_ctrl_proc;
end Behavioral;
