library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.top_decl.all;
use work.datatypes.all;
use work.scouting_register_constants.all;

use IEEE.NUMERIC_STD.all;

entity bril_moni is
    port (
        clk         : in  std_logic;
        rst         : in  std_logic;
        PicoRd      : in  std_logic;
        PicoAddr    : in std_logic_vector(31 downto 0);
        PicoDataOut : out std_logic_vector(31 downto 0);
        ctrl_reg    : in SRegister((2**moni_ctrl_register_address_width) -1 downto 0)        
    );
end bril_moni;

architecture Behavioral of bril_moni is

begin
    bril_moni_proc : process (clk, PicoAddr, PicoRd, rst) is
    begin
        if rising_edge(clk) then          
            if PicoRd = '1' then              
                if  PicoAddr = std_logic_vector(to_unsigned(trigger_bril_readout_address, 32)) then              
                    PicoDataOut(0)   <= ctrl_reg(trigger_bril_readout_address)(0);
                    PicoDataOut(31 downto 1)  <= (others => '0'); 
                    
                else
                    PicoDataOut <= x"B411FA11"; -- address not accesible
                end if;   
            end if; --picoRd         
        end if; -- rising edge
    end process bril_moni_proc;
end Behavioral;


