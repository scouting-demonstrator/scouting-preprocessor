library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.top_decl.all;
use work.datatypes.all;
use work.scouting_register_constants.all;

use IEEE.NUMERIC_STD.all;

entity demux_moni is
    port (
        clk         : in  std_logic;
        rst         : in  std_logic;
        PicoRd      : in  std_logic;
        PicoAddr    : in std_logic_vector(31 downto 0);
        PicoDataOut : out std_logic_vector(31 downto 0);
        ctrl_reg    : in SRegister((2**moni_ctrl_register_address_width) -1 downto 0)        
    );
end demux_moni;

architecture Behavioral of demux_moni is

begin
    demux_moni_proc : process (clk, PicoAddr, PicoRd, rst) is
    begin
        if rising_edge(clk) then          
            if PicoRd = '1' then              
                if  PicoAddr = std_logic_vector(to_unsigned(zs_threshold_tau_0p5GeV_address, 32)) then              
                    PicoDataOut(8 downto 0)   <= ctrl_reg(zs_threshold_tau_0p5GeV_address)(8 downto 0);
                    PicoDataOut(31 downto 9)  <= (others => '0'); 
                    
                elsif PicoAddr = std_logic_vector(to_unsigned(zs_threshold_sum_0p5GeV_address, 32)) then
                    PicoDataOut(8 downto 0)   <= ctrl_reg(zs_threshold_sum_0p5GeV_address)(8 downto 0);
                    PicoDataOut(31 downto 9)      <= (others => '0'); 
                    
                elsif PicoAddr = std_logic_vector(to_unsigned(zs_threshold_empty_0p5GeV_address, 32)) then
                    PicoDataOut(8 downto 0) <= ctrl_reg(zs_threshold_empty_0p5GeV_address)(8 downto 0);
                    PicoDataOut(31 downto 9)    <= (others => '0'); 
                    
                elsif PicoAddr = std_logic_vector(to_unsigned(zs_threshold_eg_0p5GeV_address, 32)) then
                    PicoDataOut(8 downto 0)    <= ctrl_reg(zs_threshold_eg_0p5GeV_address)(8 downto 0);   
                    PicoDataOut(31 downto 9)   <= (others => '0'); 
                    
                elsif PicoAddr = std_logic_vector(to_unsigned(zs_threshold_jet_0p5GeV_address, 32)) then
                    PicoDataOut(8 downto 0)   <= ctrl_reg(zs_threshold_jet_0p5GeV_address)(8 downto 0);   
                    PicoDataOut(31 downto 9)  <= (others => '0');             
                    
                elsif PicoAddr = std_logic_vector(to_unsigned(suppress_calibration_sequence_address, 32)) then
                    PicoDataOut(0 downto 0)   <= ctrl_reg(suppress_calibration_sequence_address)(0 downto 0);   
                    PicoDataOut(31 downto 1)  <= (others => '0'); 
                else
                    PicoDataOut <= x"C0DEFA11"; -- address not accesible
                end if;   
            end if; --picoRd         
        end if; -- rising edge
    end process demux_moni_proc;
end Behavioral;


