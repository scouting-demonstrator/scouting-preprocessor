# package-flash.tcl. Script for packaging pico framework bitfiles into tgz format expected by the api

puts "Package flash in [pwd]"
set current_dir [pwd]
set bitfile [exec find . -name "*.bit"]
set tgzfile [file tail [file rootname $bitfile]]
write_cfgmem -force -format BIN -size 128 -interface SPIx4 -loadbit {up 0x0 products/scouting_build.bit} pico.bin    
exec echo 0x852 > device_id
exec tar cfz $tgzfile.tgz device_id pico.bin
exec rm -rf device_id pico.bin pico.prm
exec ls -lh
exec cp $tgzfile.tgz products/

