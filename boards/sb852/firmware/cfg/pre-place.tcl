set_property -name "steps.place_design.tcl.pre" -value "[file normalize "../../src/scouting-preprocessor/boards/sb852/firmware/cfg/clocks.tcl"]" -objects [get_runs impl_1]
set_property -name "steps.write_bitstream.tcl.post" -value "[file normalize "../../src/scouting-preprocessor/boards/sb852/firmware/cfg/package_flash.tcl"]" -objects [get_runs impl_1]
