library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL; 
use work.datatypes.all;

entity scout_bril_calc is
    generic (
        NSTREAMS : integer 
        );
    port (
        clk    : in  std_logic;
        rst    : in  std_logic;
        d      : in  adata(NSTREAMS-1 downto 0);
        d_ctrl : in  acontrol;
        q      : out adata(NSTREAMS-1 downto 0);
        q_ctrl : out acontrol;
        mu_count_o : out std_logic_vector(5 downto 0);
        bx_id_o : out std_logic_vector(12 downto 0);
        orbit_id_o : out std_logic_vector(31 downto 0);
        q_valid_o : out std_logic
        );
end scout_bril_calc;

architecture Behavioral of scout_bril_calc is

    signal d1, d2, d3, d4, d5, d6 : adata(NSTREAMS-1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5, d_ctrl6 : acontrol;

begin
    count_mu : process(clk, rst) is
            variable mu_counter : integer range 0 to 255;
            variable q_valid : boolean;

    begin

        if rst = '1' then
            d1 <= (others => AWORD_NULL); d_ctrl1 <=('0', '0', '0', '0', '0', 1);
            d2 <= (others => AWORD_NULL); d_ctrl2 <=('0', '0', '0', '0', '0', 1);
            d3 <= (others => AWORD_NULL); d_ctrl3 <=('0', '0', '0', '0', '0', 1);
            d4 <= (others => AWORD_NULL); d_ctrl4 <=('0', '0', '0', '0', '0', 1);
            d5 <= (others => AWORD_NULL); d_ctrl5 <=('0', '0', '0', '0', '0', 1);
            d6 <= (others => AWORD_NULL); d_ctrl6 <=('0', '0', '0', '0', '0', 1);
            mu_counter := 0;
            q_valid := false;

        else
           if clk'event and clk = '1' then
                d1 <= d;  d_ctrl1 <= d_ctrl;
                d2 <= d1; d_ctrl2 <= d_ctrl1;
                d3 <= d2; d_ctrl3 <= d_ctrl2;
                d4 <= d3; d_ctrl4 <= d_ctrl3;
                d5 <= d4; d_ctrl5 <= d_ctrl4;
                d6 <= d5; d_ctrl6 <= d_ctrl5;
                q <= d5; q_ctrl <= d_ctrl5;
                q_valid := false;
                
                if d_ctrl5.valid ='1' and d_ctrl5.strobe='1' and d_ctrl5.bx_start = '1' then
                    mu_counter := 0;
                    bx_id_o <= std_logic_vector(to_unsigned(d_ctrl4.bx_counter, bx_id_o'length));
                    orbit_id_o <= d5(0)(31 downto 0);
                    for i in d'range loop
                        if d3(i)(18 downto 10) /= "000000000" then   -- pt is in bits 18 downto 10
                            mu_counter := mu_counter + 1;
                        end if;
                        if d1(i)(18 downto 10) /= "000000000" then -- d3 has 1st word of 1st muon, d1 has 1st word of 2nd muon
                            mu_counter := mu_counter + 1;
                        end if;
                    end loop;
               q_valid := true;
               end if;
               mu_count_o <= std_logic_vector(to_unsigned(mu_counter, mu_count_o'length));
               q_valid_o <= '1' when q_valid else '0';
                
            end if;
        end if;
    end process count_mu;

end Behavioral;

