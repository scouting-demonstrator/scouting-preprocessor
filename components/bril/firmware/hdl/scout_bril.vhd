library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL; 
use IEEE.NUMERIC_STD_UNSIGNED.ALL; 
use work.datatypes.all;
use work.top_decl.all;

library unisim;
use unisim.vcomponents.all;

entity scout_bril is
generic (
    NSTREAMS : integer 
);
port (
    clk    : in  std_logic;
    rst    : in  std_logic;
    d      : in  adata(NSTREAMS-1 downto 0);
    d_ctrl : in  acontrol;
    q      : out adata(NSTREAMS-1 downto 0);
    q_ctrl : out acontrol;
    new_nibble_d : out std_logic;
    mu_count_out : out std_logic_vector(5 downto 0);
    bx_id_out : out std_logic_vector(12 downto 0);
    d_valid_bril_o : out std_logic;
    bril_fifo_data_valid_o :out std_logic;
    trigger_bril_readout_i : in std_logic
);
end scout_bril;

architecture Behavioral of scout_bril is

-- For Bril

    signal lumi_section : std_logic_vector(31 downto 0) := x"00000000";
    signal lumi_nibble : std_logic_vector(31 downto 0)  := x"00000000";
    signal d_valid_bril : std_logic :='0';
    signal orbit_id, orbit_id_buf : std_logic_vector(31 downto 0); -- called iteration id in bril code
    signal orbit_id_prev : std_logic_vector(31 downto 0); -- called iteration id in bril code
    signal bx_id : std_logic_vector(12 downto 0); -- called bin_id in bril code
    signal mu_count : std_logic_vector(5 downto 0); -- called increment in bril
     -- one clock buffer versions
    signal orbit_id_d : std_logic_vector(31 downto 0); -- called iteration id in bril code
    signal bx_id_d : std_logic_vector(12 downto 0); -- called bin_id in bril code
    signal mu_count_d : std_logic_vector(5 downto 0); -- called increment in bril code
    signal trigger_bril_readout, trigger_bril_readout_d, trigger_bril_readout_sim, rising_bril_trigger : std_logic := '0';
    signal new_nibble : std_logic := '0';
    signal d_valid_bril_d : std_logic := '0';

    --bril histo outputs
    signal bril_fifo_data : std_logic_vector(31 downto 0);
    signal bril_fifo_data_valid : std_logic;
    signal bril_init_done : std_logic;
    signal bril_fifo_rd_en : std_logic := '0';
    signal bril_fifo_empty : std_logic;
    signal bril_words_cnt : std_logic_vector(31 downto 0);
    
    signal bril_header : adata(8 downto 0); 
    signal q_bril_calc                   : adata(4*N_REGION -1 downto 0);
    signal q_ctrl_bril_calc              : acontrol;
    
    signal q_int                    : adata(NSTREAMS-1 downto 0);
    signal q_ctrl_int               : acontrol;  


begin

trigger_bril_readout <= trigger_bril_readout_i or trigger_bril_readout_sim;

rising_bril_trigger_detect : process (clk,trigger_bril_readout)
begin

  if rising_edge(clk) then

    if (trigger_bril_readout = '1') then
      trigger_bril_readout_d <= '0';
    else
      -- delay input by 1 clock
      trigger_bril_readout_d <= trigger_bril_readout;

      -- detect rising edge
      rising_bril_trigger <= trigger_bril_readout and (not trigger_bril_readout_d);
    end if;

  end if;
end process;

scout_bril_calc : entity work.scout_bril_calc
    generic map (
        NSTREAMS => 4* N_REGION)
    port map (
        clk    => clk,
        rst    => rst,
        d      => d,
        d_ctrl => d_ctrl,
        q      => q_bril_calc,
        q_ctrl => q_ctrl_bril_calc,
        mu_count_o => mu_count,
        bx_id_o => bx_id,
        orbit_id_o => orbit_id,
        q_valid_o => d_valid_bril
);

mu_count_out <= mu_count_d;
orbit_id_buf <= orbit_id;
bx_id_out <= bx_id_d;
d_valid_bril_o <= d_valid_bril;
bril_fifo_data_valid_o <= bril_fifo_data_valid;

--for sim
gen_new_nibble: if SIM = true generate
    new_nibble <= '1' after 500 us;
else generate
    new_nibble <= '1' when d_valid_bril = '1' and orbit_id(13 downto 0) = 0  and orbit_id_prev(13 downto 0) > 0 and rst /= '1' else '0';
    lumi_nibble(19 downto 0) <= orbit_id(31 downto 12);
    lumi_section(13 downto 0) <= orbit_id(31 downto 18) + '1';
end generate;

luminibblecheck: process(clk) is 
    variable lumi_orbit : integer range 0 to 16383;
begin
    if rising_edge(clk) then
        -- variable assign
        lumi_orbit := to_integer(unsigned(orbit_id(13 downto 0)));
        -- 
        if rst = '1' then
            orbit_id_prev <= (others => '0');
            mu_count_d <= (others => '0');
            orbit_id_d <= (others => '0');
            bx_id_d <= (others => '0');
            d_valid_bril_d <= '0';
        else
            if d_valid_bril = '1' then
                orbit_id_prev <= orbit_id;
                orbit_id_d <= std_logic_vector(to_unsigned(lumi_orbit, orbit_id'length));
                bx_id_d <= bx_id;
                mu_count_d <= mu_count;
                d_valid_bril_d <= '1';
            else
                orbit_id_d <= (others => '0');
                bx_id_d <= (others => '0');
                mu_count_d <= (others => '0');
                d_valid_bril_d <= '0';
            end if;
        end if;
    end if;
end process luminibblecheck;

new_nibble_d <= new_nibble OR rising_bril_trigger;

bril_histogram_top_async_inst: entity work.bril_histogram_top_async
 generic map (
    -- identification of the histogram
    HISTOGRAM_TYPE                          => 20,
    HISTOGRAM_ID                            => 1,
    -- extenral new histogram
    USE_EXTERNAL_NEW_HISTOGRAM              => true,
    -- histogram parameters
    NUMBER_OF_BINS                          => 3564, --3564 bx per orbit
    COUNTER_WIDTH                           => 16, --per BX per lumi nibble count
    INCREMENT_WIDTH                         => 6, --per BX
    -- memory
    ORBIT_BX_COUNTER_WIDTH                  => 16,
    -- accumulation parameters
    NUMBER_OF_UNITS                         => 1,
    UNIT_INCREMENT_WIDTH                    => 6,
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE  => 4,
    UNIT_ERROR_COUNTER_WIDTH                => 3,
    CHECK_ERRORS                            => false,
    STORE_ERRORS                            => false
)
port map(
    -- core signals
    async_reset_i                   => rst,
    clk_i                           => clk,
    -- tcds data
    lhc_fill_i                      => x"00000000", --not known by scouting firmware
    cms_run_i                       => x"00000000", --not known by scouting firmware
    lumi_section_i                  => lumi_section,
    lumi_nibble_i                   => lumi_nibble,
    -- new histogram
    new_histogram_ext_i             => new_nibble_d,
    -- counter signals  
    valid_i		  	            	=> d_valid_bril_d,
    iteration_id_i(12 downto 0)     => orbit_id_d(12 downto 0),
    iteration_id_i(15 downto 13)    => (others => '0'),
    iteration_counter_i             => orbit_id_buf(15 downto 0),
    bin_id_i(12 downto 0)           => bx_id_d,
    bin_id_i(15 downto 13)          => (others => '0'),
    increment_i                     => mu_count_d, 
    error_i                         => (others => '0'),
    mask_i                          => (others => '0'),         
    -- fifo interface
    fifo_rd_clk_i                   => clk,   
    fifo_rd_en_i                    => bril_fifo_rd_en,    
    fifo_empty_o                    => bril_fifo_empty,    
    fifo_words_cnt_o                => bril_words_cnt, 
    fifo_data_o                     => bril_fifo_data,
    fifo_data_valid_o               => bril_fifo_data_valid,
    -- module is ready
    init_done_o			            => bril_init_done
);

-- formats the bril histogram output such that it can be sent out over the DMA one word each frame, with a counter for the Nth word in the histogram
brilpack: process(clk) is 
    variable histoword_counter : integer range 0 to 4000;
begin
    if rising_edge(clk) then
        if rst = '1' then
            -- reset handling
            bril_fifo_rd_en <= '0';
            histoword_counter := 0;
            bril_fifo_rd_en <= '0';
            for i in bril_header'range loop
                bril_header(i) <= x"00000000";
            end loop;
        else
            --logic
            if bril_fifo_data_valid = '0' then
               histoword_counter := 0;
            end if;
            
            if bril_init_done = '1' and bril_fifo_data_valid = '1' and bril_fifo_empty = '0' then 
                if bril_fifo_rd_en = '0' then -- i.e first clock of being ready to read data
                    bril_fifo_rd_en <= '1';
                    q_int(0) <= x"00000000";                                  
                    for i in 4*N_REGION -1 downto 1 loop
                        q_int(i) <= (others => '0');
                    end loop;
                else
                    bril_fifo_rd_en <= '0';
                end if;
            else
                bril_fifo_rd_en <= '0';
            end if;
            
            if bril_fifo_rd_en = '1' then --i.e not the first clock of being ready to read data
                for i in 4*N_REGION -1 downto 2 loop
                    q_int(i) <= (others => '0');
                end loop;
	        q_int(0) <= bril_fifo_data;
                q_int(1) <= std_logic_vector(to_unsigned(histoword_counter, 32));
                q_ctrl_int <= (valid => '1', strobe => '1', bx_start => '0', last => '0', header => '0', bx_counter => 1);
                histoword_counter := histoword_counter + 1;
            else 
                for i in 4*N_REGION -1 downto 0 loop
                    q_int(i) <= (others => '0');
                    q_ctrl_int <= (valid => '0', strobe => '0', bx_start => '0', last => '0', header => '0', bx_counter => 1);
                end loop;

            end if;
        end if;
    end if;
end process brilpack;

q <= q_int;
q_ctrl <= q_ctrl_int;

end Behavioral;
