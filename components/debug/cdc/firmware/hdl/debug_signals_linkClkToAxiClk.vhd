library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.VComponents.all;

use work.datatypes.all;

entity debug_signals_linkClkToAxiClk is
    generic (
        N_REGION : natural
    );
    port (
        clk_link               : in std_logic;
        clk_axi                : in std_logic;
        sRxbyteisaligned_in    : in std_logic_vector(4 * N_REGION - 1 downto 0);
        waiting_orbit_end_in   : in std_logic;
        rst_algo_in            : in std_logic;
        rst_aligner_in         : in std_logic;
        d_align_in             : in ldata(3 + (N_REGION - 1) * 4 downto 0);
        lid_in                 : in TAuxInfo(4 * N_REGION - 1 downto 0);
        crc_in                 : in TAuxInfo(4 * N_REGION - 1 downto 0);
        sRxbyteisaligned_sync  : out std_logic_vector(4 * N_REGION - 1 downto 0);
        waiting_orbit_end_sync : out std_logic;
        rst_algo_sync          : out std_logic;
        rst_aligner_sync       : out std_logic;
        d_align_sync           : out ldata(3 + (N_REGION - 1) * 4 downto 0);
        lid_sync               : out TAuxInfo(4 * N_REGION - 1 downto 0);
        crc_sync               : out TAuxInfo(4 * N_REGION - 1 downto 0)
    );
end debug_signals_linkClkToAxiClk;

architecture rtl of debug_signals_linkClkToAxiClk is

begin

    sync_sRxbyteisaligned_to_clk_axi : entity work.cdc_array
        generic map(
            bit_width => sRxbyteisaligned_in'length
        )
        port map(
            clk_src  => clk_link,
            clk_dest => clk_axi,
            d        => sRxbyteisaligned_in,
            q        => sRxbyteisaligned_sync);

    sync_waiting_orbit_end_to_clk_axi : entity work.cdc_single
        port map(
            clk_src  => clk_link,
            clk_dest => clk_axi,
            d        => waiting_orbit_end_in,
            q        => waiting_orbit_end_sync);

    sync_rst_algo_to_clk_axi : entity work.cdc_single
        port map(
            clk_src  => clk_link,
            clk_dest => clk_axi,
            d        => rst_algo_in,
            q        => rst_algo_sync);

    sync_rst_aligner_to_clk_axi : entity work.cdc_single
        port map(
            clk_src  => clk_link,
            clk_dest => clk_axi,
            d        => rst_aligner_in,
            q        => rst_aligner_sync);

    sync_d_align : for chan in d_align_in'range generate
        sync_d_align_valid_to_clk_axi : entity work.cdc_single
            port map(
                clk_src  => clk_link,
                clk_dest => clk_axi,
                d        => d_align_in(chan).valid,
                q        => d_align_sync(chan).valid);
        sync_d_align_strobe_to_clk_axi : entity work.cdc_single
            port map(
                clk_src  => clk_link,
                clk_dest => clk_axi,
                d        => d_align_in(chan).strobe,
                q        => d_align_sync(chan).strobe);
        sync_d_align_done_to_clk_axi : entity work.cdc_single
            port map(
                clk_src  => clk_link,
                clk_dest => clk_axi,
                d        => d_align_in(chan).done,
                q        => d_align_sync(chan).done);
        sync_d_align_data_to_clk_axi : entity work.cdc_array
            generic map(
                bit_width => d_align_in(chan).data'length
            )
            port map(
                clk_src  => clk_link,
                clk_dest => clk_axi,
                d        => d_align_in(chan).data,
                q        => d_align_sync(chan).data);
    end generate sync_d_align;

    sync_link_trailer : for chan in lid_in'range generate
        sync_lid_to_clk_axi : entity work.cdc_array
            generic map(
                bit_width => lid_in(chan)'length
            )
            port map(
                clk_src  => clk_link,
                clk_dest => clk_axi,
                d        => lid_in(chan),
                q        => lid_sync(chan));
        sync_crc_to_clk_axi : entity work.cdc_array
            generic map(
                bit_width => crc_in(chan)'length
            )
            port map(
                clk_src  => clk_link,
                clk_dest => clk_axi,
                d        => crc_in(chan),
                q        => crc_sync(chan));
    end generate sync_link_trailer;

end rtl;

