library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.VComponents.all;

use work.datatypes.all;

entity debug_signals_freeClkToAxiClk is
    generic (
        N_REGION : natural
    );
    port (
        clk_free            : in std_logic;
        clk_axi             : in std_logic;
        init_retry_ctr_in   : in std_logic_vector(3 downto 0);
        cdr_stable_in       : in std_logic;
        init_retry_ctr_sync : out std_logic_vector(3 downto 0);
        cdr_stable_sync     : out std_logic
    );
end debug_signals_freeClkToAxiClk;

architecture rtl of debug_signals_freeClkToAxiClk is

begin

    sync_init_retr_ctr_to_clk_axi : entity work.cdc_array
        generic map(
            bit_width => init_retry_ctr_in'length
        )
        port map(
            clk_src  => clk_free,
            clk_dest => clk_axi,
            d        => init_retry_ctr_in,
            q        => init_retry_ctr_sync);

    sync_cdr_stable_to_clk_axi : entity work.cdc_single
        port map(
            clk_src  => clk_free,
            clk_dest => clk_axi,
            d        => cdr_stable_in,
            q        => cdr_stable_sync);

end rtl;

