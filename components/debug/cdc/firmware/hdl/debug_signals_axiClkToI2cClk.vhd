library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.VComponents.all;

entity debug_signals_axiClkToI2cClk is
    port (
        clk_axi      : in std_logic;
        clk_i2c      : in std_logic;
        rst_i2c_in   : in std_logic;
        str_rd_in    : in std_logic;
        str_wr_in    : in std_logic;
        rst_i2c_sync : out std_logic;
        str_rd_sync  : out std_logic;
        str_wr_sync  : out std_logic
    );
end debug_signals_axiClkToI2cClk;

architecture rtl of debug_signals_axiClkToI2cClk is

begin

    sync_rst_to_clk_i2c : entity work.cdc_single
        port map(
            clk_src  => clk_axi,
            clk_dest => clk_i2c,
            d        => rst_i2c_in,
            q        => rst_i2c_sync);

    sync_str_rd_to_clk_i2c : entity work.cdc_single
        port map(
            clk_src  => clk_axi,
            clk_dest => clk_i2c,
            d        => str_rd_in,
            q        => str_rd_sync);

    sync_str_wr_to_clk_i2c : entity work.cdc_single
        port map(
            clk_src  => clk_axi,
            clk_dest => clk_i2c,
            d        => str_wr_in,
            q        => str_wr_sync);

end rtl;

