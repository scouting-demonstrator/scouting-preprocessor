library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.VComponents.all;
use work.top_decl.all;

entity debug_signals_axiClkToLinkClk is
    port (
        clk_axi                 : in std_logic;
        clk_link                : in std_logic;
        stream_enable_mask_in   : in std_logic_vector(4 * N_REGION - 1 downto 0);
        disable_zs_in           : in std_logic;
        stream_enable_mask_sync : out std_logic_vector(4 * N_REGION - 1 downto 0);
        disable_zs_sync         : out std_logic
    );
end debug_signals_axiClkToLinkClk;

architecture rtl of debug_signals_axiClkToLinkClk is

begin

    sync_stream_enable_mask_to_clk_link : entity work.cdc_array
        generic map(
            bit_width => stream_enable_mask_in'length
        )
        port map(
            clk_src  => clk_axi,
            clk_dest => clk_link,
            d        => stream_enable_mask_in,
            q        => stream_enable_mask_sync);

    sync_disable_zs_to_clk_link : entity work.cdc_single
        port map(
            clk_src  => clk_axi,
            clk_dest => clk_link,
            d        => disable_zs_in,
            q        => disable_zs_sync);

end rtl;

