library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.datatypes.all;

entity tstream_delay_line is
    generic (
        delay_length : integer := 6
    );
    port (
        clk     : in  std_logic;
        d       : in  adata(7 downto 0);
        d_ctrl  : in  acontrol;
        q       : out adata(7 downto 0);
        q_ctrl  : out acontrol
    );
end tstream_delay_line;

architecture Behavioral of tstream_delay_line is
    signal d_store      : TStream_aframe(delay_length - 1 downto 0);
    signal d_ctrl_store : TStream_acontrol(delay_length - 1 downto 0);
begin

    delay_line : process (clk)
    begin
        if rising_edge(clk) then

            d_store <= d_store(d_store'high-1 downto 0) & d;
            q       <= d_store(d_store'high);

            d_ctrl_store <= d_ctrl_store(d_ctrl_store'high-1 downto 0) & d_ctrl;
            q_ctrl       <= d_ctrl_store(d_ctrl_store'high);

        end if;
    end process delay_line;

end Behavioral;
