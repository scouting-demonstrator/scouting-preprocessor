library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.datatypes.all;





entity pattern_gen is
    generic (
        NSTREAMS                : natural
    );
    port (
        clk                     : in  std_logic;
        rst                     : in  std_logic;
        q                       : out ldata(NSTREAMS - 1 downto 0);
        GEN_ORBIT_FULL_LENGTH   : integer := ORBIT_LENGTH;
        GEN_ORBIT_DATA_LENGTH   : integer := 150
    );
end pattern_gen;





architecture Behavioral of pattern_gen is

    signal q_int            : ldata(NSTREAMS - 1 downto 0);

    -- generator
    type TBCounterVec is array(natural range <>) of natural range 0 to 3564;
    type TOCounterVec is array(natural range <>) of natural;
    type TEvtWordsVec is array(natural range <>) of natural range 0 to 5;
    signal ocounter : TOCounterVec(NSTREAMS - 1 downto 0) := (others => 0);
    signal bcounter : TBCounterVec(NSTREAMS - 1 downto 0) := (others => 0);
    signal evt_word : TEvtWordsVec(NSTREAMS - 1 downto 0) := (others => 0);

begin

    ----------------------------------------------------------------------------
    --
    -- Generate test data
    --
    ----------------------------------------------------------------------------
    generate_patterns : process(clk, rst)
    begin
        if rst = '1' then
            ocounter <= (others => 0);
            bcounter <= (others => 0);
            evt_word <= (others => 0);
            q_int    <= (others => LWORD_NULL);
        elsif clk'event and clk = '1' then
            -- loop over channels
            for i in (NSTREAMS-1) downto 0 loop
                ---- update counters
                if evt_word(i) < 5 then
                    evt_word(i)     <= evt_word(i)+1;
                elsif evt_word(i) = 5 then
                    evt_word(i)     <= 0;
                    if bcounter(i) < GEN_ORBIT_FULL_LENGTH then
                        bcounter(i) <= bcounter(i)+1;
                    elsif bcounter(i) = GEN_ORBIT_FULL_LENGTH then
                        bcounter(i) <= 0;
                        ocounter(i) <= ocounter(i)+1;
                    else
                        bcounter(i) <= 0;
                    end if;
                else
                    evt_word(i)     <= 0;
                end if;

                ---- data generation
                -- header
                if bcounter(i) = 0 then
                    if evt_word(i) = 5 then
                        q_int(i).done   <= '0';
                        q_int(i).valid  <= '1';
                        q_int(i).strobe <= '1';
                        q_int(i).data   <= std_logic_vector(to_unsigned(ocounter(i), 32));
                    else
                        q_int(i).done   <= '0';
                        q_int(i).strobe <= '0';
                        q_int(i).valid  <= '0';
                        q_int(i).data   <= x"00000000";  -- COMMA
                    end if;
                -- orbit payload
                elsif bcounter(i) > 0 and bcounter(i) < (GEN_ORBIT_DATA_LENGTH+1) then
                    if    evt_word(i) = 0 then q_int(i).data <= std_logic_vector(to_unsigned(ocounter(i), 32));
                    elsif evt_word(i) = 1 then q_int(i).data <= std_logic_vector(to_unsigned(bcounter(i), 32));
                    elsif evt_word(i) = 2 then q_int(i).data <= "000000001" & "1100" & "10000000000" & std_logic_vector(to_unsigned(i, 8));
                    elsif evt_word(i) = 3 then q_int(i).data <= x"ffffff"                            & std_logic_vector(to_unsigned(i, 8));
                    elsif evt_word(i) = 4 then q_int(i).data <= "000000001" & "1100" & "10000000000" & std_logic_vector(to_unsigned(i, 8));
                    elsif evt_word(i) = 5 then q_int(i).data <= x"ffffff"                            & std_logic_vector(to_unsigned(i, 8));
                    else                       q_int(i).data <= x"ffffffff";
                    end if;

                    q_int(i).done   <= '0';
                    q_int(i).valid  <= '1';
                    q_int(i).strobe <= '1';
                -- orbit gap
                else
                    q_int(i).done   <= '0';
                    q_int(i).strobe <= '0';
                    q_int(i).valid  <= '0';
                    q_int(i).data   <= x"00000000";
                end if;
            end loop;
        end if;
    end process;
    ----------------------------------------------------------------------------

    -- output generated data
    q <= q_int;

end Behavioral;
