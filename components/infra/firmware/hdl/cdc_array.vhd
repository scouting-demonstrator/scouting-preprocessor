
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.VComponents.all;

library xpm;
use xpm.vcomponents.all;

entity cdc_array is
    generic (
        bit_width : positive
    );
    port (
        clk_src  : in std_logic;
        clk_dest : in std_logic;
        d        : in std_logic_vector(bit_width - 1 downto 0);
        q        : out std_logic_vector(bit_width - 1 downto 0)
    );
end cdc_array;

architecture rtl of cdc_array is

begin

    -- xpm_cdc_array_single: Single-bit Array Synchronizer
    -- Xilinx Parameterized Macro, version 2021.1
    xpm_cdc_array_single_inst : xpm_cdc_array_single
    generic map(
        DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
        INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG  => 1,        -- DECIMAL; 0=do not register input, 1=register input
        WIDTH          => bit_width -- DECIMAL; range: 1-1024
    )
    port map(
        dest_out => q,
        dest_clk => clk_dest, -- 1-bit input: Clock signal for the destination clock domain.
        src_clk  => clk_src,  -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in   => d
    );
    -- End of xpm_cdc_array_single_inst instantiation

end rtl;
