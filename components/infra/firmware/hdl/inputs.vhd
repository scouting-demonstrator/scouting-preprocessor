-- inputs
--
-- Optical serial inputs to 40 MHz scouting demonstrator
--
-- D. R. May 2018
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.datatypes.all;

entity inputs is
    generic (
        ORBIT        : natural := ORBIT_LENGTH;
        BYPASS_LINKS : boolean := false;
        QUADS        : natural := N_REGION;
        N_MGTREFCLKS : natural := N_REGION
        );
    port (
        axi_clk : in std_logic;
        
        gtrxn_in  : in std_logic_vector(4*QUADS -1 downto 0);
        gtrxp_in  : in std_logic_vector(4*QUADS -1 downto 0);
        gttxn_out : out std_logic_vector(4*QUADS -1 downto 0);
        gttxp_out : out std_logic_vector(4*QUADS -1 downto 0);
      
        mgtrefclks    : in std_logic_vector(N_MGTREFCLKS -1  downto 0);
        clk_freerun                                : in  std_logic;
        clk_freerun_buf                            : out std_logic;
        init_done_int                              : out std_logic_vector(0 downto 0);
        init_retry_ctr_int                         : out std_logic_vector(3 downto 0);
        gtpowergood_sync                           : out std_logic_vector(4*QUADS - 1 downto 0);
        txpmaresetdone_sync                        : out std_logic_vector(4*QUADS - 1 downto 0);
        rxpmaresetdone_sync                        : out std_logic_vector(4*QUADS - 1 downto 0);
        gtwiz_reset_tx_done_sync                   : out std_logic_vector(0 downto 0);
        gtwiz_reset_rx_done_sync                   : out std_logic_vector(0 downto 0);
        hb_gtwiz_reset_all_int                     : in  std_logic_vector(0 downto 0);
        hb0_gtwiz_reset_tx_pll_and_datapath_int    : in  std_logic_vector(0 downto 0);
        hb0_gtwiz_reset_tx_datapath_int            : in  std_logic_vector(0 downto 0);
        hb_gtwiz_reset_rx_pll_and_datapath_int     : in  std_logic_vector(0 downto 0);
        hb_gtwiz_reset_rx_datapath_int             : in  std_logic_vector(0 downto 0);
        link_down_latched_reset_int                : in  std_logic_vector(0 downto 0);        
    	loopback                                   : in  std_logic_vector(3*4*QUADS - 1 downto 0);
        clk                                        : out std_logic;
        q                                          : out ldata(4*QUADS - 1 downto 0);
        oCommaDet                                  : out std_logic_vector(4*QUADS - 1 downto 0);

        oLinkData : out std_logic_vector(8 * 32 - 1 downto 0);

        cdr_stable  : out std_logic_vector(0 downto 0);
        rxctrl0_out : out std_logic_vector(63 downto 0);
        rxctrl1_out : out std_logic_vector(63 downto 0);
        rxctrl2_out : out std_logic_vector(31 downto 0);
        rxctrl3_out : out std_logic_vector(31 downto 0);

        oRxbyteisaligned : out std_logic_vector(4*QUADS - 1 downto 0)
        );
end inputs;

architecture Behavioral of inputs is

    signal rx_data : std_logic_vector(4*QUADS * 32 - 1 downto 0);
    signal tx_data : std_logic_vector(4*QUADS * 32 - 1 downto 0);
    signal usr_clk : std_logic;

    type TBCounterVec is array(natural range <>) of natural range 0 to 3564;
    type TOCounterVec is array(natural range <>) of natural;
    type TEvtWordsVec is array(natural range <>) of natural range 0 to 5;
    signal ocounter : TOCounterVec(7 downto 0) := (others => 1);
    signal bcounter : TBCounterVec(7 downto 0) := (0, 1, 2, 3, 4, 5, 6, 7);
    signal evt_word : TEvtWordsVec(7 downto 0) := (others => 0);

    signal sCommaDet : std_logic_vector(4*QUADS - 1 downto 0);
    signal q_int     : ldata(7 downto 0);
    signal sCharisk  : std_logic_vector(63 downto 0);

    signal rxctrl0_int : std_logic_vector(QUADS * 64 - 1 downto 0);
    signal rxctrl1_int : std_logic_vector(QUADS * 64 - 1 downto 0);
    signal rxctrl2_int : std_logic_vector(QUADS * 32 - 1 downto 0);
    signal rxctrl3_int : std_logic_vector(QUADS * 32 - 1 downto 0);

begin

    gen_links_gth : if BYPASS_LINKS = false and BOARD="KCU1500" generate
        gth_wrapper : entity work.gtwizard_ultrascale_0_example_top
            port map
            (
                -- Differential reference clock inputs
                mgtrefclk0 => mgtrefclks(0),

                -- Serial data ports for transceiver channel 0
                ch0_gthrxn_in  => gtrxn_in(0),
                ch0_gthrxp_in  => gtrxp_in(0),
                ch0_gthtxn_out => gttxn_out(0),
                ch0_gthtxp_out => gttxp_out(0),

                -- Serial data ports for transceiver channel 1
                ch1_gthrxn_in  => gtrxn_in(1),
                ch1_gthrxp_in  => gtrxp_in(1),
                ch1_gthtxn_out => gttxn_out(1),
                ch1_gthtxp_out => gttxp_out(1),

                -- Serial data ports for transceiver channel 2
                ch2_gthrxn_in  => gtrxn_in(2),
                ch2_gthrxp_in  => gtrxp_in(2),
                ch2_gthtxn_out => gttxn_out(2),
                ch2_gthtxp_out => gttxp_out(2),

                -- Serial data ports for transceiver channel 3
                ch3_gthrxn_in  => gtrxn_in(3),
                ch3_gthrxp_in  => gtrxp_in(3),
                ch3_gthtxn_out => gttxn_out(3),
                ch3_gthtxp_out => gttxp_out(3),

                -- Serial data ports for transceiver channel 4
                ch4_gthrxn_in  => gtrxn_in(4),
                ch4_gthrxp_in  => gtrxp_in(4),
                ch4_gthtxn_out => gttxn_out(4),
                ch4_gthtxp_out => gttxp_out(4),

                -- Serial data ports for transceiver channel 5
                ch5_gthrxn_in  => gtrxn_in(5),
                ch5_gthrxp_in  => gtrxp_in(5),
                ch5_gthtxn_out => gttxn_out(5),
                ch5_gthtxp_out => gttxp_out(5),

                -- Serial data ports for transceiver channel 6
                ch6_gthrxn_in  => gtrxn_in(6),
                ch6_gthrxp_in  => gtrxp_in(6),
                ch6_gthtxn_out => gttxn_out(6),
                ch6_gthtxp_out => gttxp_out(6),

                -- Serial data ports for transceiver channel 7
                ch7_gthrxn_in  => gtrxn_in(7),
                ch7_gthrxp_in  => gtrxp_in(7),
                ch7_gthtxn_out => gttxn_out(7),
                ch7_gthtxp_out => gttxp_out(7),

                -- User-provided ports for reset helper block(s)
                hb_gtwiz_reset_clk_freerun_in  => clk_freerun,
                hb_gtwiz_reset_clk_freerun_out => clk_freerun_buf,
                init_done_int                               => init_done_int(0),
                init_retry_ctr_int                          => init_retry_ctr_int,
                gtpowergood_vio_sync                        => gtpowergood_sync,
                txpmaresetdone_vio_sync                     => txpmaresetdone_sync,
                rxpmaresetdone_vio_sync                     => rxpmaresetdone_sync,
                gtwiz_reset_tx_done_vio_sync                => gtwiz_reset_tx_done_sync(0),
                gtwiz_reset_rx_done_vio_sync                => gtwiz_reset_rx_done_sync(0),
                hb_gtwiz_reset_all_vio_int                  => hb_gtwiz_reset_all_int,
	            hb0_gtwiz_reset_tx_pll_and_datapath_int     => hb0_gtwiz_reset_tx_pll_and_datapath_int,
                hb0_gtwiz_reset_tx_datapath_int             => hb0_gtwiz_reset_tx_datapath_int,
                hb_gtwiz_reset_rx_pll_and_datapath_vio_int  => hb_gtwiz_reset_rx_pll_and_datapath_int,
                hb_gtwiz_reset_rx_datapath_vio_int          => hb_gtwiz_reset_rx_datapath_int,
                link_down_latched_reset_vio_int             => link_down_latched_reset_int,
	            loopback                                    => loopback,
                rxbyteisaligned_out                         => oRxbyteisaligned,

                -- PRBS-based link status ports
                rxcommadet_out                => open,
                gtwiz_userdata_rx_out         => rx_data(4*QUADS * 32 - 1 downto 0),
                gtwiz_userdata_tx_in          => tx_data(4*QUADS * 32 - 1 downto 0),
                charisk_in                    => sCharisk(QUADS * 32 - 1 downto 0),
                rxctrl0_out                   => rxctrl0_int,  -- If RXCTRL3 is low: Corresponding rxdata byte is a KWORD
                rxctrl1_out                   => rxctrl1_int,
                rxctrl2_out                   => rxctrl2_int,  -- Corresponding byte is a comma.
                rxctrl3_out                   => rxctrl3_int,  -- Can't decode the word.
                gtwiz_userclk_rx_usrclk_out   => usr_clk,
                gtwiz_reset_rx_cdr_stable_out => cdr_stable
                );
    end generate gen_links_gth;

    gen_links_gty : if BYPASS_LINKS = false and BOARD = "SB852" generate 
        gty_wrapper : entity work.gt_ultrascale_custom_top
            port map
            (
                mgtrefclk0_x0y2_int => mgtrefclks(0),
                mgtrefclk0_x0y4_int => mgtrefclks(1),
                
                ch0_gtyrxn_in  => gtrxn_in(0),
                ch0_gtyrxp_in  => gtrxn_in(0),
                ch0_gtytxn_out => gttxn_out(0),
                ch0_gtytxp_out => gttxp_out(0),
                
                ch1_gtyrxn_in  => gtrxn_in(1),
                ch1_gtyrxp_in  => gtrxn_in(1),
                ch1_gtytxn_out => gttxn_out(1),
                ch1_gtytxp_out => gttxp_out(1),
                
                ch2_gtyrxn_in  => gtrxn_in(2),
                ch2_gtyrxp_in  => gtrxn_in(2),
                ch2_gtytxn_out => gttxn_out(2),
                ch2_gtytxp_out => gttxp_out(2),
                
                ch3_gtyrxn_in  => gtrxn_in(3),
                ch3_gtyrxp_in  => gtrxn_in(3),
                ch3_gtytxn_out => gttxn_out(3),
                ch3_gtytxp_out => gttxp_out(3),
                
                ch4_gtyrxn_in  => gtrxn_in(4),
                ch4_gtyrxp_in  => gtrxn_in(4),
                ch4_gtytxn_out => gttxn_out(4),
                ch4_gtytxp_out => gttxp_out(4),
                
                ch5_gtyrxn_in  => gtrxn_in(5),
                ch5_gtyrxp_in  => gtrxn_in(5),
                ch5_gtytxn_out => gttxn_out(5),
                ch5_gtytxp_out => gttxp_out(5),
                
                ch6_gtyrxn_in  => gtrxn_in(6),
                ch6_gtyrxp_in  => gtrxn_in(6),
                ch6_gtytxn_out => gttxn_out(6),
                ch6_gtytxp_out => gttxp_out(6),
                
                ch7_gtyrxn_in  => gtrxn_in(7),
                ch7_gtyrxp_in  => gtrxn_in(7),
                ch7_gtytxn_out => gttxn_out(7),
                ch7_gtytxp_out => gttxp_out(7),
                
                hb_gtwiz_reset_clk_freerun_in => clk_freerun,
                hb_gtwiz_reset_all_in => hb_gtwiz_reset_all_int,
                
                link_down_latched_reset_in       => hb_gtwiz_reset_all_int,
                gtwiz_userdata_rx_out            => rx_data(255 downto 0),
                gtwiz_userclk_rx_usrclk_out      => usr_clk,
                init_done_int                    => init_done_int(0),
                link_down_latched_reset_vio_int  => link_down_latched_reset_int,
                
                -- signals from MGT that encode information on rx words
                rxctrl0_out                   => rxctrl0_int,  -- If RXCTRL3 is low: Corresponding rxdata byte is a KWORD
                rxctrl1_out                   => rxctrl1_int,
                rxctrl2_out                   => rxctrl2_int,  -- Corresponding byte is a comma.
                rxctrl3_out                   => rxctrl3_int,  -- Can't decode the word.
                
                txpmaresetdone_vio_sync => txpmaresetdone_sync,   
                rxpmaresetdone_vio_sync => rxpmaresetdone_sync, 
                    
                rxbyteisaligned_out => oRxbyteisaligned,
                gtwiz_reset_rx_cdr_stable_out => cdr_stable
    
            );
    
    end generate gen_links_gty;

    bypass_mgt : if BYPASS_LINKS = true generate
        usr_clk <= axi_clk;
    end generate;

    clk       <= usr_clk;
    oCommaDet <= sCommaDet;


    data_assignment : process(rx_data, q_int, rxctrl0_int, rxctrl2_int, rxctrl3_int)
    begin
        for i in 0 to 4*QUADS-1 loop  -- TODO: This needs to be made aware of the number of quads.
            if BYPASS_LINKS = true then
                q(i) <= q_int(i);
            end if;
            if BYPASS_LINKS = false then
                q(i).data <= rx_data(i*32 + 31 downto i*32);
                q(i).done <= '0';
                if rxctrl3_int(i*8) = '0' and rxctrl0_int(i*16) = '1' and rxctrl2_int(i*8) = '1' then  -- Comma
                    sCommaDet(i) <= '1';
                    q(i).valid   <= '0';
                    q(i).strobe  <= '1';
                elsif rxctrl3_int(i*8 + 3 downto i*8) = "0000" and rxctrl0_int(i*16 + 3 downto i*16) = "1111" then  -- Padding (0xF7F7F7F7)
                    sCommaDet(i) <= '0';
                    q(i).valid   <= '1';
                    q(i).strobe  <= '0';
                else
                    sCommaDet(i) <= '0';
                    q(i).valid   <= '1';
                    q(i).strobe  <= '1';
                end if;
            end if;
        end loop;
    end process;
    oLinkData(4*QUADS*32 - 1 downto 0) <= rx_data;

    rxctrl0_out <= rxctrl0_int(63 downto 0);
    rxctrl1_out <= rxctrl1_int(63 downto 0);
    rxctrl2_out <= rxctrl2_int(31 downto 0);
    rxctrl3_out <= rxctrl3_int(31 downto 0);

    create_testdata : process(usr_clk)
        variable send_padding  : std_logic_vector(QUADS*4-1 downto 0);
    begin
        if usr_clk'event and usr_clk = '1' then
            -- Create test data now
            for i in QUADS*4-1 downto 0 loop  -- Number of channels            
                if ((bcounter(i)+2*i) mod 4 = 0) and (evt_word(i) = 0) and (send_padding(i) = '0') then
                    send_padding(i) := '1';
                elsif evt_word(i) < 5 then
                    send_padding(i) := '0';
                    evt_word(i)     <= evt_word(i)+1;
                elsif evt_word(i) = 5 then
                    send_padding(i) := '0';
                    evt_word(i)     <= 0;
                    if bcounter(i) < ORBIT then
                        bcounter(i) <= bcounter(i)+1;
                    elsif bcounter(i) = ORBIT then
                        bcounter(i) <= 1;
                        ocounter(i) <= ocounter(i)+1;
                    else
                        bcounter(i) <= 1;
                    end if;
                else
                    send_padding(i) := '0';
                    evt_word(i)     <= 0;
                end if;

                if send_padding(i) = '1' then
                    q_int(i).data                  <= x"F7F7F7F7";  -- Padding
                    q_int(i).valid                 <= '1';
                    q_int(i).strobe                <= '0';
                    q_int(i).done                  <= '0';
                    tx_data(i*32 + 31 downto i*32) <= x"F7F7F7F7";  -- Padding
                    sCharisk(i*8 + 7 downto i*8)   <= "00001111";
                elsif bcounter(i) > 0 and bcounter(i) < ORBIT-5 then  -- Arbitrary obit gap
                    if evt_word(i) = 0 then
                        tx_data(i*32 + 31 downto i*32) <= std_logic_vector(to_unsigned(bcounter(i), 32));
                        q_int(i).data                  <= std_logic_vector(to_unsigned(bcounter(i), 32));
                    elsif evt_word(i) = 1 then
                        tx_data(i*32 + 31 downto i*32) <= std_logic_vector(to_unsigned(ocounter(i), 32));
                        q_int(i).data                  <= std_logic_vector(to_unsigned(ocounter(i), 32));
                    elsif (evt_word(i) = 2) or (evt_word(i) = 4) then
                        tx_data(i*32 + 31 downto i*32) <= "000000001" & "1100" & "100000000" & "0000000000";
                        q_int(i).data                  <= "000000001" & "1100" & "100000000" & "0000000000";
                    elsif (evt_word(i) = 3) or (evt_word(i) = 5) then
                        tx_data(i*32 + 31 downto i*32) <= (others => '1');
--                        tx_data(i*32 + 31 downto i*32)  <= "00000000000" & "0000000001" & "0111111" & "10" & "00";
                        q_int(i).data                  <= (others => '1');
--                        q_int(i).data  <= "00000000000" & "0000000001" & "0111111" & "10" & "00";
                    else
                        tx_data(i*32 + 31 downto i*32) <= (others => '1');
                        q_int(i).data                  <= (others => '1');
                    end if;

                    q_int(i).done                <= '0';
                    q_int(i).valid               <= '1';
                    q_int(i).strobe              <= '1';
                    sCharisk(i*8 + 7 downto i*8) <= "00000000";
                elsif bcounter(i) = ORBIT-5 and (evt_word(i) = 1 or evt_word(i) = 2) then
                    q_int(i).done                  <= '0';
                    q_int(i).strobe                <= '1';
                    q_int(i).valid                 <= '1';
                    q_int(i).data                  <= "10101010101010101010101010101010";  -- If I see this pattern I'm transmitting "CRCs".
                    tx_data(i*32 + 31 downto i*32) <= "10101010101010101010101010101010";
                    sCharisk(i*8 + 7 downto i*8)   <= "00000000";
                else
                    q_int(i).done                  <= '0';
                    q_int(i).strobe                <= '1';
                    q_int(i).valid                 <= '0';
                    q_int(i).data                  <= x"505050BC";  -- COMMA
                    tx_data(i*32 + 31 downto i*32) <= x"505050BC";  -- COMMA
                    sCharisk(i*8 + 7 downto i*8)   <= "00000001";
                end if;
            end loop;

        end if;
    end process;

    -- TODO: OR the status signals here.

end Behavioral;
