library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.top_decl.all;
use work.algo_decl.all;
use work.datatypes.all;

use IEEE.NUMERIC_STD.ALL;

entity check_crc is
    generic (
        NSTREAMS : natural := 4*N_REGION
    );
    port (
        clk           : in  std_logic;
        rst           : in  std_logic;
        d             : in  ldata(NSTREAMS - 1 downto 0);
        crc           : in  TAuxInfo(NSTREAMS - 1 downto 0);
        crc_err_count : out TCounter32b(NSTREAMS - 1 downto 0)
    );
end check_crc;

architecture Behavioral of check_crc is

  signal enabled, rst_crc, rst_crc_del, crc_en : std_logic_vector(NSTREAMS - 1 downto 0);

  signal crcs_comp : TAuxInfo(NSTREAMS - 1 downto 0);

  signal crc_err_count_int : TCounter32b(NSTREAMS - 1 downto 0);

begin

  -- We use the CRC module directly from the MP7 repository here.
  -- crc_en indicates whether a given word is taken into account for the CRC computation. We only use words that are neither comma (i.e. valid) not padding (i.e. strobe).
  -- rst_crc clears the CRC logic, we do this when the orbit (and therefore the packet) ends.
  calc_crcs: for i in d'range generate
    crc_en(i) <= d(i).valid and d(i).strobe;
    rst_crc(i) <= not (d(i).valid);
    olcrc_inst: entity work.outputlogic_32b_crc
      port map(
        clk => clk,
        rst => rst_crc(i),
        crc_en => crc_en(i),
        data_in => d(i).data,
        crc_out => crcs_comp(i));
  end generate;

  -- To protect us from computing an incorrect CRC value when the logic starts in the middle of an orbit we begin with a disabled CRC logic and only enable it once the first orbit ends.
  -- To note: This is expected to happen only once, immediately after the entire logic has been reset. In that case it is likely that the links come up in the middle of an orbit and we
  -- therefore compute an incomplete CRC value. (Later parts of the logic are protected by the aligner.)
  process(clk)
  begin
    if rising_edge(clk) then
      rst_crc_del  <= rst_crc;

      for i in enabled'range loop
        if rst = '1' then
          enabled(i) <= '0';
        else
          if rst_crc(i) = '1' and rst_crc_del(i) = '0' then
            enabled(i) <= '1';
          end if;
        end if;
      end loop;
    end if;
  end process;

  -- To compare the CRCs computed above with those obtained from the links we detect when the CRC logic is being reset and then increment the counter if we detect a mismatch.
  process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        crc_err_count_int <= (others => (others => '0'));
      else
        for i in crc_err_count'range loop
          if enabled(i) = '1' and rst_crc(i) = '1' and rst_crc_del(i) = '0' and (crcs_comp(i) /= crc(i)) then
            crc_err_count_int(i) <= crc_err_count_int(i) + 1;
          end if;
        end loop;
      end if;
      crc_err_count <= crc_err_count_int;
    end if;
  end process;

end Behavioral;

