library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use WORK.datatypes.all;
use work.top_decl.all;

entity orbit_and_bx_aware_aligner is
    generic (
        NSTREAMS : integer
    );
    port (
        clk_wr            : in std_logic;
        clk_rd            : in std_logic;
        rst               : in std_logic;                               -- active high pulse
        enable            : in std_logic_vector(NSTREAMS - 1 downto 0); -- 1 for an enabled channel
        waiting_orbit_end : out std_logic;
        d                 : in ldata(NSTREAMS - 1 downto 0);
        q                 : out adata(NSTREAMS - 1 downto 0);
        q_ctrl            : out acontrol
    );
end orbit_and_bx_aware_aligner;

architecture Behavioral of orbit_and_bx_aware_aligner is

    subtype aword_with_last is std_logic_vector(32 downto 0);
    type adata_with_last is array (natural range <>) of aword_with_last;

    type t_state is (GAP, IDLE, HEADER,
                     D6, D5, D4, D3, D2, D1,
                     NEW_HEADER,
                     T16, T15, T14, T13, T12, T11, T10, T9, T8, T7, T6, T5, T4, T3, T2, T1, PT_PLACEHOLDER,
                     E12, E11, E10, E9, E8, E7, E6, E5, E4, E3, E2, E1,
                     START_GAP_OR_IDLE);
    signal State     : t_state;
    signal LastState : t_state;

    signal all_fifos_contain_bx : std_logic;
    signal reset_all_fifos      : std_logic;

    signal q_fifo            : adata_with_last(NSTREAMS - 1 downto 0);
    signal fifo_prog_empty   : std_logic_vector(NSTREAMS - 1 downto 0);
    signal fifo_empty        : std_logic_vector(NSTREAMS - 1 downto 0);
    signal fifo_almost_empty : std_logic_vector(NSTREAMS - 1 downto 0);
    signal fifo_full         : std_logic_vector(NSTREAMS - 1 downto 0);
    signal fifo_rd_en        : std_logic;

    signal header_data       : adata(NSTREAMS - 1 downto 0);


    signal ctrl_i : acontrol;
    type t_ResetState is (DONE, WAIT_ORBIT_END, R1, R2, R3, A1, A2, A3, A4, A5, A6, A7, A8, WAIT_ORBIT2_END);
    signal ResetState : t_ResetState;
    --    signal rst_d : std_logic;
    signal all_streams_not_valid_d         : std_logic;
    signal reset_read_sm, reset_read_sm_rd : std_logic;

    signal write_inhibit : std_logic;

    signal bx_counter : integer range 0 to 3564;

    component aligner_fifo is
        port (
            wr_clk       : in std_logic                      := '0';
            rd_clk       : in std_logic                      := '0';
            rst          : in std_logic                      := '0';
            prog_empty   : out std_logic                     := '1';
            wr_en        : in std_logic                      := '0';
            rd_en        : in std_logic                      := '0';
            din          : in std_logic_vector(32 downto 0)  := (others => '0');
            dout         : out std_logic_vector(32 downto 0) := (others => '0');
            full         : out std_logic                     := '0';
            empty        : out std_logic                     := '1';
            almost_empty : out std_logic                     := '1');

    end component;

begin

    --
    -- PER STREAM FIFO
    --
    per_stream_fifo : for i in NSTREAMS - 1 downto 0 generate

        signal wen  : std_logic;
        signal d1   : lword;
        signal last : std_logic;
        signal din  : std_logic_vector(32 downto 0);
    begin

        generate_last : process (clk_wr) is
        begin
            if clk_wr'event and clk_wr = '1' then
                d1 <= d(i);
            end if;
        end process generate_last;

        last <= d1.valid and not d(i).valid;
        din  <= last & d1.data;
        wen  <= d1.valid and d1.strobe and enable(i) and (not write_inhibit);

        fifo_inst : aligner_fifo
        port map(
            wr_clk       => clk_wr,
            rd_clk       => clk_rd,
            rst          => reset_all_fifos,
            prog_empty   => fifo_prog_empty(i),
            wr_en        => wen,
            rd_en        => fifo_rd_en,
            din          => din,
            dout         => q_fifo(i),
            full         => fifo_full(i),
            empty        => fifo_empty(i),
            almost_empty => fifo_almost_empty(i));

    end generate per_stream_fifo;
    --
    -- upon a rising edge of the reset signal:
    --   -- wait until an orbit end is detected (all channels becoming not valid)
    --   -- wait 2 clocks
    --   -- reset fifos for 3 clocks
    --   -- wait 16 clocks
    --   -- during the whole reset time reset the read logic.
    --
    reset_logic : process (clk_wr, rst) is
        variable not_valid : boolean;
    begin
        if rst = '1' then
            ResetState              <= WAIT_ORBIT_END;
            waiting_orbit_end       <= '1';
            write_inhibit           <= '1';
            reset_read_sm           <= '1';
            all_streams_not_valid_d <= '1'; -- set this to 1 so that we don't detect an orbit gap start if the reset is released in the orbit gap
        else
            if (clk_wr'event and clk_wr = '1') then

                not_valid := true;
                for i in NSTREAMS - 1 downto 0 loop
                    if enable(i) = '1' and d(i).valid = '1' then
                        not_valid := false;
                    end if;
                end loop;

                if (not_valid) then
                    all_streams_not_valid_d <= '1';
                else
                    all_streams_not_valid_d <= '0';
                end if;

                case ResetState is
                    when WAIT_ORBIT_END => -- wait for a rising edge of all streams not valid
                        reset_read_sm <= '1';
                        if not_valid and all_streams_not_valid_d = '0' then
                            reset_all_fifos   <= '1';
                            ResetState        <= R1;
                            waiting_orbit_end <= '0';
                        end if;
                    when R1 => ResetState <= R2;
                    when R2 => ResetState <= R3;
                    when R3 => ResetState <= A1;
                        reset_all_fifos       <= '0';
                    when A1 => ResetState <= A2;
                    when A2 => ResetState <= A3;
                    when A3 => ResetState <= A4;
                    when A4 => ResetState <= A5;
                    when A5 => ResetState <= A6;
                    when A6 => ResetState <= A7;
                    when A7 => ResetState <= A8;
                    when A8 => ResetState <= WAIT_ORBIT2_END;
                        reset_read_sm         <= '0';
                        waiting_orbit_end     <= '1';
                    when WAIT_ORBIT2_END =>
                        if not_valid and all_streams_not_valid_d = '0' then
                            write_inhibit     <= '0';
                            ResetState        <= DONE;
                            waiting_orbit_end <= '0';
                        end if;
                    when DONE => reset_all_fifos <= '0';
                        reset_read_sm                <= '0';
                        write_inhibit                <= '0';
                end case;
            end if;
        end if;
    end process reset_logic;

    -- sync reset to clk_rd
    sync_reset_read_sm : entity work.cdc_single
        port map(
            clk_src  => clk_wr,
            clk_dest => clk_rd,
            d        => reset_read_sm,
            q        => reset_read_sm_rd
        );



    --
    -- READ LOGIC
    --

    -- check that all prog_empty are 0
    all_fifos_contain_bx <= '1' when (fifo_prog_empty and enable) = (NSTREAMS - 1 downto 0 => '0')
        else
        '0';

    read_logic : process (clk_rd, reset_read_sm) is
        variable i        : integer;
        variable all_last : boolean;
--        variable bx_counter : integer range 1 to 3564;
    begin
        if reset_read_sm_rd = '1' then
            State           <= GAP;
            ctrl_i.valid    <= '0';
            ctrl_i.strobe   <= '0';
            ctrl_i.bx_start <= '0';
            ctrl_i.last     <= '0';
            q_ctrl.valid    <= '0';
            q_ctrl.strobe   <= '0';
            q_ctrl.bx_start <= '0';
            q_ctrl.last     <= '0';
            q               <= (others => AWORD_NULL);
            q_ctrl.header   <= '0';
            bx_counter      <= 0;
            header_data     <= (others => AWORD_NULL);
        else
            if clk_rd'event and clk_rd = '1' then

                all_last := true;
                for i in q_fifo'range loop
                    if (q_fifo(i)(32) = '0') and (enable(i) = '1') then
                        all_last := false;
                    end if;
                end loop;

                LastState <= State;

                case State is

                    when GAP =>
                        if (all_fifos_contain_bx = '1') then
                            State      <= HEADER; -- First word in orbit is the header.
                            fifo_rd_en <= '1';
                        else
                            fifo_rd_en <= '0';
                        end if;

                    when IDLE =>
                        if (all_fifos_contain_bx = '1') then
                            if bx_counter < 3564 then -- we are in the middle of the orbit, continue
                                State      <= D6;
                                fifo_rd_en <= '1';
                            else -- we are already at BX #3564, go into trailer logic
                                State <= T16;
                                fifo_rd_en <= '0';
                            end if;
                        end if;

                    when HEADER =>
                        bx_counter <= 3554; -- valid goes high at BX 3555
                        State <= D6;

                    when D6 =>
                        bx_counter <= bx_counter + 1;
                        State <= D5;

                    when D5 =>
                        State <= D4;

                    when D4 =>
                        State <= D3;

                    when D3 =>
                        State <= D2;

                    when D2 =>
                        State <= D1;

                    when T16 => State <= T15;
                    when T15 => State <= T14;
                    when T14 => State <= T13;
                    when T13 => State <= T12;
                    when T12 => State <= T11;
                    when T11 => State <= T10;
                    when T10 => State <= T9;
                    when T9  => State <= T8;
                    when T8  => State <= T7;
                    when T7  => State <= T6;
                    when T6  => State <= T5;
                    when T5  => State <= T4;
                    when T4  => State <= T3;
                    when T3  => State <= T2;
                    when T2  => State <= T1;
                    when T1  => State <= PT_PLACEHOLDER;
                    when PT_PLACEHOLDER => State <= E12;
                    when E12 => State <= E11;
                    when E11 => State <= E10;
                    when E10 => State <= E9;
                    when E9  => State <= E8;
                    when E8  => State <= E7;
                    when E7  => State <= E6;
                    when E6  => State <= E5;
                    when E5  => State <= E4;
                    when E4  => State <= E3;
                    when E3  => State <= E2;
                    when E2  => State <= E1;
                    when E1  => State <= NEW_HEADER;

                    when NEW_HEADER =>
                        State <= D6;
                        fifo_rd_en <= '1';
                        bx_counter <= 0;

                    when D1 =>
                        if (all_fifos_contain_bx = '1') then
                            if bx_counter < 3564 then -- we are in the middle of the orbit, continue
                                State <= D6;
                                fifo_rd_en <= '1';
                            else -- we are already at BX #3564, go into trailer logic
                                State <= T16;
                                fifo_rd_en <= '0';
                            end if;
                        else
                            State      <= START_GAP_OR_IDLE;
                            fifo_rd_en <= '0';
                        end if;

                    when START_GAP_OR_IDLE =>
                        if (all_fifos_contain_bx = '1') then
                            if bx_counter < 3564 then -- we are in the middle of the orbit, continue
                                State <= D6;
                                fifo_rd_en <= '1';
                            else -- we are already at BX #3564, go into trailer logic
                                State <= T16;
                                fifo_rd_en <= '0';
                            end if;
                        else
                            if all_last then
                                State <= GAP;
                            else
                                State <= IDLE; -- Still in orbit, but no data left in FIFO.
                            end if;
                        end if;

                end case;

                --
                -- Multiplexer and last flip flop
                --
                if LastState = GAP or (LastState = START_GAP_OR_IDLE and State = GAP) then
                    q <= (others => AWORD_NULL);
                elsif LastState = IDLE or (LastState = START_GAP_OR_IDLE and State /= GAP) then -- assume a GAP takes at least 2 clocks, otherwise it is an IDLE
                    q <= (others => AWORD_PAD);
                elsif LastState = HEADER and State = D6 then                                    -- gather header and store it for later replacement
                    for i in NSTREAMS - 1 downto 0 loop
                        if enable(i) = '1' then
                            header_data(i) <= q_fifo(i)(31 downto 0);
                        else
                            header_data(i) <= AWORD_NULL;
                        end if;
                    end loop;
                    q <= (others => AWORD_NULL);
                else
                    for i in NSTREAMS - 1 downto 0 loop
                        if enable(i) = '1' then
                            q(i) <= q_fifo(i)(31 downto 0);
                        else
                            q(i) <= AWORD_NULL;
                        end if;
                    end loop;
                end if;

                case LastState is
                    -- old mp7 orbit header (to ignore)
                    when HEADER =>
                        q_ctrl.valid    <= '1';
                        q_ctrl.strobe   <= '0';
                        q_ctrl.bx_start <= '0';
                        q_ctrl.last     <= '0';
                        q_ctrl.header   <= '0'; -- '0' because this header is not needed anymore
                    -- new orbit header at start of BX 1 of new orbit
                    when NEW_HEADER =>
                        q               <= header_data; -- write saved header (stored header frame)
                        q_ctrl.valid    <= '1';
                        q_ctrl.strobe   <= '1';
                        q_ctrl.bx_start <= '0';
                        q_ctrl.last     <= '0';
                        q_ctrl.header   <= '1';
                    -- nex BX start (frame 1)
                    when D6 =>
                        q_ctrl.valid      <= '1';
                        q_ctrl.strobe     <= '1';
                        q_ctrl.bx_start   <= '1';
                        q_ctrl.last       <= '0';
                        q_ctrl.header     <= '0';
                        q_ctrl.bx_counter <= bx_counter;
                    -- BX frame 2-5
                    when D5 to D2 =>
                        q_ctrl.valid      <= '1';
                        q_ctrl.strobe     <= '1';
                        q_ctrl.bx_start   <= '0';
                        q_ctrl.last       <= '0';
                        q_ctrl.header     <= '0';
                        q_ctrl.bx_counter <= bx_counter;
                    -- BX frame 6
                    when D1 =>
                        q_ctrl.valid      <= '1';
                        q_ctrl.strobe     <= '1';
                        q_ctrl.bx_start   <= '0';
                        q_ctrl.bx_counter <= bx_counter;
                        -- TODO: "last" bit should be perhaps a "last_bx" bit
                        if (bx_counter = 3564) then
                            q_ctrl.last <= '1';
                        else
                            q_ctrl.last <= '0';
                        end if;
                        q_ctrl.header <= '0';
                    -- reserve space for orbit trailer (write null data)
                    when T16 to E1 =>
                        for i in NSTREAMS - 1 downto 0 loop
                            q(i) <= AWORD_NULL;
                        end loop;
                        q_ctrl.valid      <= '0';
                        q_ctrl.strobe     <= '0';
                        q_ctrl.bx_start   <= '0';
                        q_ctrl.last       <= '0';
                        q_ctrl.header     <= '0';
                        q_ctrl.bx_counter <= bx_counter;
                    when START_GAP_OR_IDLE =>
                        if (State = GAP) then
                            q_ctrl.valid <= '0';
                        else
                            q_ctrl.valid <= '1';
                        end if;
                        q_ctrl.strobe   <= '0';
                        q_ctrl.bx_start <= '0';
                        q_ctrl.last     <= '0';
                        q_ctrl.header   <= '0';
                    when GAP =>
                        q_ctrl.valid    <= '0';
                        q_ctrl.strobe   <= '0';
                        q_ctrl.bx_start <= '0';
                        q_ctrl.last     <= '0';
                        q_ctrl.header   <= '0';
                    when IDLE =>
                        q_ctrl.valid    <= '1';
                        q_ctrl.strobe   <= '0';
                        q_ctrl.bx_start <= '0';
                        q_ctrl.last     <= '0';
                        q_ctrl.header   <= '0';
                end case;

            end if;
        end if;
    end process read_logic;

end Behavioral;