---------------------------------------------------------------------------------------------------
-- This block delays the signals by 'delay_length' clocks to improve timing at the SLR crossing 
---------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.datatypes.all;

entity cross_to_slr1_delay_line is
    generic (
        delay_length : integer := 6;
        N_REGIONS : integer := 2
        );
    port (
        clk : in std_logic;
        orbits_per_packet                : in std_logic_vector(15 downto 0);
        orbits_per_packet_out            : out std_logic_vector(15 downto 0);
        d_package                        : in adata; 
        d_package_out                    : out adata;
        d_ctrl_package                   : in acontrol; 
        d_ctrl_package_out               : out acontrol;
        filler_orbit_counter             : in unsigned(63 downto 0);
        filler_orbit_counter_out         : out unsigned(63 downto 0);
        axi_backpressure_seen            : in std_logic;
        axi_backpressure_seen_out        : out std_logic;
        orbit_length                     : in unsigned (63 downto 0);
        orbit_length_out                 : out unsigned (63 downto 0);
        orbit_exceeds_size               : in std_logic;
        orbit_exceeds_size_out           : out std_logic;
        autorealign_counter              : in unsigned(63 downto 0);
        autorealign_counter_out          : out unsigned(63 downto 0);
        filler_dropped_orbit_counter     : in unsigned(63 downto 0);
        filler_dropped_orbit_counter_out : out unsigned(63 downto 0);
        ctrl_reg                         : in SRegister;
        ctrl_reg_out                     : out SRegister
        );
end cross_to_slr1_delay_line;
architecture Behavioral of cross_to_slr1_delay_line is
    signal filler_orbit_counter_store         : Unsigned64bDelayline(delay_length -1 downto 0);
    signal filler_dropped_orbit_counter_store : Unsigned64bDelayline(delay_length -1  downto 0);
    signal axi_backpressure_seen_store        : std_logic_vector(delay_length -1 downto 0);
    signal orbits_per_packet_store            : LogicVector16bDelayline(delay_length -1 downto 0);
    signal orbit_length_store                 : Unsigned64bDelayline(delay_length -1 downto 0);
    signal orbit_exceeds_size_store           : std_logic_vector(delay_length -1 downto 0);
    signal autorealign_counter_store          : Unsigned64bDelayline(delay_length -1 downto 0);
    signal d_ctrl_package_store               : AControlDelayline(delay_length -1 downto 0);
    signal d_package_store                    : ADataDelayline(delay_length -1 downto 0);
    signal ctrl_reg_store                     : SRegisterDelayline(delay_length -1 downto 0);
begin
    
    delay_line : process (clk)
    begin
        if rising_edge(clk) then

            orbits_per_packet_store <= orbits_per_packet_store(orbits_per_packet_store'high-1 downto 0) & orbits_per_packet;
            orbits_per_packet_out   <= orbits_per_packet_store(orbits_per_packet_store'high);
        
            d_package_store <= d_package_store(d_package_store'high-1 downto 0) & d_package;
            d_package_out   <= d_package_store(d_package_store'high);
        
            d_ctrl_package_store <= d_ctrl_package_store(d_ctrl_package_store'high-1 downto 0) & d_ctrl_package;
            d_ctrl_package_out   <= d_ctrl_package_store(d_ctrl_package_store'high);
        
            filler_dropped_orbit_counter_store <= filler_dropped_orbit_counter_store(filler_dropped_orbit_counter_store'high-1 downto 0) & filler_dropped_orbit_counter;
            filler_dropped_orbit_counter_out   <= filler_dropped_orbit_counter_store(filler_dropped_orbit_counter_store'high);
        
            filler_orbit_counter_store <= filler_orbit_counter_store(filler_orbit_counter_store'high-1 downto 0) & filler_orbit_counter;
            filler_orbit_counter_out   <= filler_orbit_counter_store(filler_orbit_counter_store'high);
        
            axi_backpressure_seen_store <= axi_backpressure_seen_store(axi_backpressure_seen_store'high-1 downto 0) & axi_backpressure_seen;
            axi_backpressure_seen_out   <= axi_backpressure_seen_store(axi_backpressure_seen_store'high);
        
            orbit_length_store <= orbit_length_store(orbit_length_store'high-1 downto 0) & orbit_length;
            orbit_length_out   <= orbit_length_store(orbit_length_store'high);
        
            orbit_exceeds_size_store <= orbit_exceeds_size_store(orbit_exceeds_size_store'high-1 downto 0) & orbit_exceeds_size;
            orbit_exceeds_size_out   <= orbit_exceeds_size_store(orbit_exceeds_size_store'high);
        
            autorealign_counter_store <= autorealign_counter_store(autorealign_counter_store'high-1 downto 0) & autorealign_counter;
            autorealign_counter_out   <= autorealign_counter_store(autorealign_counter_store'high);
            
            ctrl_reg_store <= ctrl_reg_store(ctrl_reg_store'high-1 downto 0) & ctrl_reg;
            ctrl_reg_out   <= ctrl_reg_store(ctrl_reg_store'high);

        end if;
    end process delay_line;

end Behavioral;
