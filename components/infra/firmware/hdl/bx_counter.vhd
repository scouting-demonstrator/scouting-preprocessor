library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.datatypes.all;

use IEEE.NUMERIC_STD.all;

entity bx_counter is
    generic (
        NSTREAMS : integer
    );
    port (
        clk    : in std_logic;
        rst    : in std_logic;
        d      : in adata(NSTREAMS - 1 downto 0);
        d_ctrl : in acontrol;
        q      : out adata(NSTREAMS - 1 downto 0);
        q_ctrl : out acontrol
    );
end bx_counter;

architecture Behavioral of bx_counter is
begin

    gen_bx_counter : process (clk, rst) is
        variable bx_counter : integer range 1 to 3564; -- 3564 BXs per orbit 
    begin
        if rst = '1' then
            bx_counter := 1; -- No way to know at which point in the orbit we come out of reset so we just pretend we're at the start of it.
        elsif rising_edge(clk) then
            if d_ctrl.valid = '0' then
                bx_counter := 3554; -- Valid goes high at 3555.
            elsif d_ctrl.bx_start = '1' then
                if bx_counter < 3564 then
                    bx_counter := bx_counter + 1;
                else
                    bx_counter := 1;
                end if;
            end if;
            q_ctrl.bx_counter <= bx_counter;
            q_ctrl.valid      <= d_ctrl.valid;
            q_ctrl.strobe     <= d_ctrl.strobe;
            q_ctrl.bx_start   <= d_ctrl.bx_start;
            q_ctrl.last       <= d_ctrl.last;
            q_ctrl.header     <= d_ctrl.header;
            q                 <= d;
        end if;
    end process;

end Behavioral;
