library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.datatypes.all;





entity bw_or_ugt is
    generic (
        bufsize     : integer := 10;
        N_STREAM_i  : integer := 18
    );
    port (
        clk         : in  std_logic;
        rst         : in  std_logic;
        d           : in  adata(N_STREAM_i - 1 downto 0);
        d_ctrl      : in  acontrol;
        q           : out adata(7 downto 0);
        q_ctrl      : out acontrol;
        disable_zs  : in  std_logic;
        mask_algo   : in  std_logic_vector(511 downto 0)
    );
end bw_or_ugt;





architecture Behavioral of bw_or_ugt is

    type adata_buffer    is array (natural range <>) of adata(N_STREAM_i - 1 downto 0);
    type acontrol_buffer is array (natural range <>) of acontrol;

    signal d_buf      : adata_buffer(bufsize - 1 downto 0);
    signal d_ctrl_buf : acontrol_buffer(bufsize - 1 downto 0);

    signal d_bw_or    : std_logic_vector(511 downto 0);
    signal d1_finor   : std_logic_vector( 63 downto 0);
    signal d2_finor   : std_logic_vector(  7 downto 0);
    signal q_finor    : std_logic;

begin

    bw_or_ugt : process (clk, rst) is
        variable suppress : boolean;
    begin
        if rst = '1' then

            d_buf      <= (others => (others => AWORD_NULL));
            d_ctrl_buf <= (others => ACONTROL_NULL);

            d_bw_or    <= (others => '0');
            d1_finor   <= (others => '0');
            d2_finor   <= (others => '0');

            q_finor    <= '0';

        else
            if clk'event and clk = '1' then

                d_buf(0)                   <= d;
                d_buf(d_buf'high downto 1) <= d_buf(d_buf'high - 1 downto 0);
                q                          <= d_buf(d_buf'high)(7 downto 0);

                d_ctrl_buf(0)                        <= d_ctrl;
                d_ctrl_buf(d_ctrl_buf'high downto 1) <= d_ctrl_buf(d_ctrl_buf'high - 1 downto 0);
                q_ctrl                               <= d_ctrl_buf(d_ctrl_buf'high);


                if d_ctrl_buf(d_ctrl_buf'high-4).valid    = '1' and
                   d_ctrl_buf(d_ctrl_buf'high-4).strobe   = '1' and
                   d_ctrl_buf(d_ctrl_buf'high-4).bx_start = '1'     then

                    -- MP7 board               #1                         #2                         #3                         #4                         #5                         #6
                    d_bw_or( 31 downto   0) <= d_buf(d_buf'high-4)( 0) or d_buf(d_buf'high-4)( 3) or d_buf(d_buf'high-4)( 6) or d_buf(d_buf'high-4)( 9) or d_buf(d_buf'high-4)(12) or d_buf(d_buf'high-4)(15);
                    d_bw_or( 63 downto  32) <= d_buf(d_buf'high-5)( 0) or d_buf(d_buf'high-5)( 3) or d_buf(d_buf'high-5)( 6) or d_buf(d_buf'high-5)( 9) or d_buf(d_buf'high-5)(12) or d_buf(d_buf'high-5)(15);
                    d_bw_or( 95 downto  64) <= d_buf(d_buf'high-6)( 0) or d_buf(d_buf'high-6)( 3) or d_buf(d_buf'high-6)( 6) or d_buf(d_buf'high-6)( 9) or d_buf(d_buf'high-6)(12) or d_buf(d_buf'high-6)(15);
                    d_bw_or(127 downto  96) <= d_buf(d_buf'high-7)( 0) or d_buf(d_buf'high-7)( 3) or d_buf(d_buf'high-7)( 6) or d_buf(d_buf'high-7)( 9) or d_buf(d_buf'high-7)(12) or d_buf(d_buf'high-7)(15);
                    d_bw_or(159 downto 128) <= d_buf(d_buf'high-8)( 0) or d_buf(d_buf'high-8)( 3) or d_buf(d_buf'high-8)( 6) or d_buf(d_buf'high-8)( 9) or d_buf(d_buf'high-8)(12) or d_buf(d_buf'high-8)(15);
                    d_bw_or(191 downto 160) <= d_buf(d_buf'high-9)( 0) or d_buf(d_buf'high-9)( 3) or d_buf(d_buf'high-9)( 6) or d_buf(d_buf'high-9)( 9) or d_buf(d_buf'high-9)(12) or d_buf(d_buf'high-9)(15);

                    d_bw_or(223 downto 192) <= d_buf(d_buf'high-4)( 1) or d_buf(d_buf'high-4)( 4) or d_buf(d_buf'high-4)( 7) or d_buf(d_buf'high-4)(10) or d_buf(d_buf'high-4)(13) or d_buf(d_buf'high-4)(16);
                    d_bw_or(255 downto 224) <= d_buf(d_buf'high-5)( 1) or d_buf(d_buf'high-5)( 4) or d_buf(d_buf'high-5)( 7) or d_buf(d_buf'high-5)(10) or d_buf(d_buf'high-5)(13) or d_buf(d_buf'high-5)(16);
                    d_bw_or(287 downto 256) <= d_buf(d_buf'high-6)( 1) or d_buf(d_buf'high-6)( 4) or d_buf(d_buf'high-6)( 7) or d_buf(d_buf'high-6)(10) or d_buf(d_buf'high-6)(13) or d_buf(d_buf'high-6)(16);
                    d_bw_or(319 downto 288) <= d_buf(d_buf'high-7)( 1) or d_buf(d_buf'high-7)( 4) or d_buf(d_buf'high-7)( 7) or d_buf(d_buf'high-7)(10) or d_buf(d_buf'high-7)(13) or d_buf(d_buf'high-7)(16);
                    d_bw_or(351 downto 320) <= d_buf(d_buf'high-8)( 1) or d_buf(d_buf'high-8)( 4) or d_buf(d_buf'high-8)( 7) or d_buf(d_buf'high-8)(10) or d_buf(d_buf'high-8)(13) or d_buf(d_buf'high-8)(16);
                    d_bw_or(383 downto 352) <= d_buf(d_buf'high-9)( 1) or d_buf(d_buf'high-9)( 4) or d_buf(d_buf'high-9)( 7) or d_buf(d_buf'high-9)(10) or d_buf(d_buf'high-9)(13) or d_buf(d_buf'high-9)(16);

                    d_bw_or(415 downto 384) <= d_buf(d_buf'high-4)( 2) or d_buf(d_buf'high-4)( 5) or d_buf(d_buf'high-4)( 8) or d_buf(d_buf'high-4)(11) or d_buf(d_buf'high-4)(14) or d_buf(d_buf'high-4)(17);
                    d_bw_or(447 downto 416) <= d_buf(d_buf'high-5)( 2) or d_buf(d_buf'high-5)( 5) or d_buf(d_buf'high-5)( 8) or d_buf(d_buf'high-5)(11) or d_buf(d_buf'high-5)(14) or d_buf(d_buf'high-5)(17);
                    d_bw_or(479 downto 448) <= d_buf(d_buf'high-6)( 2) or d_buf(d_buf'high-6)( 5) or d_buf(d_buf'high-6)( 8) or d_buf(d_buf'high-6)(11) or d_buf(d_buf'high-6)(14) or d_buf(d_buf'high-6)(17);
                    d_bw_or(511 downto 480) <= d_buf(d_buf'high-7)( 2) or d_buf(d_buf'high-7)( 5) or d_buf(d_buf'high-7)( 8) or d_buf(d_buf'high-7)(11) or d_buf(d_buf'high-7)(14) or d_buf(d_buf'high-7)(17);

                end if;


                -- d_ctrl_buf'high - 3 ...
                if d_ctrl_buf(d_ctrl_buf'high-3).valid    = '1' and
                   d_ctrl_buf(d_ctrl_buf'high-3).strobe   = '1' and
                   d_ctrl_buf(d_ctrl_buf'high-3).bx_start = '1'     then

                    for i in d1_finor'range loop
                        d1_finor(i) <= (d_bw_or(i*8 + 7) and (not mask_algo(i*8 + 7))) or
                                       (d_bw_or(i*8 + 6) and (not mask_algo(i*8 + 6))) or
                                       (d_bw_or(i*8 + 5) and (not mask_algo(i*8 + 5))) or
                                       (d_bw_or(i*8 + 4) and (not mask_algo(i*8 + 4))) or
                                       (d_bw_or(i*8 + 3) and (not mask_algo(i*8 + 3))) or
                                       (d_bw_or(i*8 + 2) and (not mask_algo(i*8 + 2))) or
                                       (d_bw_or(i*8 + 1) and (not mask_algo(i*8 + 1))) or
                                       (d_bw_or(i*8 + 0) and (not mask_algo(i*8 + 0)));
                    end loop;

                else
                    d1_finor <= (others => '0');
                end if;


                -- d_ctrl_buf'high - 2 ...
                if d_ctrl_buf(d_ctrl_buf'high-2).valid    = '1' and
                   d_ctrl_buf(d_ctrl_buf'high-2).strobe   = '1' and
                   d_ctrl_buf(d_ctrl_buf'high-2).bx_start = '1'     then

                    for i in d2_finor'range loop
                        d2_finor(i) <= d1_finor(i*8 + 7) or
                                       d1_finor(i*8 + 6) or
                                       d1_finor(i*8 + 5) or
                                       d1_finor(i*8 + 4) or
                                       d1_finor(i*8 + 3) or
                                       d1_finor(i*8 + 2) or
                                       d1_finor(i*8 + 1) or
                                       d1_finor(i*8 + 0);
                    end loop;

                else
                    d2_finor <= (others => '0');
                end if;


                -- d_ctrl_buf'high - 1 ...
                if d_ctrl_buf(d_ctrl_buf'high-1).valid    = '1' and
                   d_ctrl_buf(d_ctrl_buf'high-1).strobe   = '1' and
                   d_ctrl_buf(d_ctrl_buf'high-1).bx_start = '1'     then

                    q_finor <= d2_finor(7) or
                               d2_finor(6) or
                               d2_finor(5) or
                               d2_finor(4) or
                               d2_finor(3) or
                               d2_finor(2) or
                               d2_finor(1) or
                               d2_finor(0);

                else
                    q_finor <= '0';
                end if;


                if d_ctrl_buf(d_ctrl_buf'high).valid    = '1' and
                   d_ctrl_buf(d_ctrl_buf'high).strobe   = '1' and
                   d_ctrl_buf(d_ctrl_buf'high).bx_start = '1'     then

                    -- Reshuffle and Rewrite for output
                    for i in q'range loop
                        q(i)                     <= d_bw_or(  0 + 32*(i+1) - 1 downto   0 + 32*i);
                        d_buf(d_buf'high    )(i) <= d_bw_or(256 + 32*(i+1) - 1 downto 256 + 32*i);
                        d_buf(d_buf'high - 1)(i) <= AWORD_NULL;

                        d_buf(d_buf'high - 3)(i) <= (others => '0');
                        d_buf(d_buf'high - 4)(i) <= (others => '0');
                    end loop;
                    d_buf(d_buf'high - 1)(0) <= d_buf(d_buf'high-4)( 2);
                    d_buf(d_buf'high - 1)(1) <= d_buf(d_buf'high-5)( 2);
                    d_buf(d_buf'high - 1)(2) <= d_buf(d_buf'high-4)( 5);
                    d_buf(d_buf'high - 1)(3) <= d_buf(d_buf'high-5)( 5);
                    d_buf(d_buf'high - 1)(4) <= d_buf(d_buf'high-4)( 8);
                    d_buf(d_buf'high - 1)(5) <= d_buf(d_buf'high-5)( 8);
                    d_buf(d_buf'high - 1)(6) <= d_buf(d_buf'high-4)(11);
                    d_buf(d_buf'high - 1)(7) <= d_buf(d_buf'high-5)(11);

                    d_buf(d_buf'high - 2)(0) <= d_buf(d_buf'high-4)( 2);
                    d_buf(d_buf'high - 2)(1) <= d_buf(d_buf'high-5)( 2);
                    d_buf(d_buf'high - 2)(2) <= d_buf(d_buf'high-4)( 5);
                    d_buf(d_buf'high - 2)(3) <= d_buf(d_buf'high-5)( 5);
                    d_buf(d_buf'high - 2)(4) <= x"cafecafe";
                    d_buf(d_buf'high - 2)(5) <= x"cafecafe";
                    d_buf(d_buf'high - 2)(6) <= x"cafecafe";
                    d_buf(d_buf'high - 2)(7) <= x"cafecafe";

                    q_ctrl.strobe                          <= '1';
                    d_ctrl_buf(d_ctrl_buf'high    ).strobe <= '1';
                    d_ctrl_buf(d_ctrl_buf'high - 1).strobe <= '1';
                    d_ctrl_buf(d_ctrl_buf'high - 2).strobe <= '1';
                    d_ctrl_buf(d_ctrl_buf'high - 3).strobe <= '1';
                    d_ctrl_buf(d_ctrl_buf'high - 4).strobe <= '1';

                    -- Zero Suppression
                    suppress := true;
                    if q_finor = '1' or disable_zs = '1' then
                        suppress := false;
                    end if;

                    if suppress then
                        q_ctrl.strobe                          <= '0';
                        d_ctrl_buf(d_ctrl_buf'high    ).strobe <= '0';
                        d_ctrl_buf(d_ctrl_buf'high - 1).strobe <= '0';
                        d_ctrl_buf(d_ctrl_buf'high - 2).strobe <= '0';
                        d_ctrl_buf(d_ctrl_buf'high - 3).strobe <= '0';
                        d_ctrl_buf(d_ctrl_buf'high - 4).strobe <= '0';
                    end if;
                end if;

            end if;
        end if;
    end process bw_or_ugt;

end Behavioral;
