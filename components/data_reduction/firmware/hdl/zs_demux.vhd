library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.datatypes.all;
use work.scouting_register_constants.all;

use IEEE.NUMERIC_STD.all;

entity zs_demux is
    generic (
        NSTREAMS       : integer;
        REG_ADDR_WIDTH : integer
    );
    port (
        clk      : in std_logic;
        rst      : in std_logic;
        ctrl_reg : in SRegister(2 ** REG_ADDR_WIDTH - 1 downto 0);
        d        : in adata(NSTREAMS - 1 downto 0);
        d_ctrl   : in acontrol;
        q        : out adata(NSTREAMS - 1 downto 0);
        q_ctrl   : out acontrol
    );
end zs_demux;

architecture Behavioral of zs_demux is

    signal d1, d2, d3, d4, d5                          : adata(NSTREAMS - 1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5 : acontrol;

    type CutList is array(NSTREAMS - 1 downto 0) of unsigned(8 downto 0);
    signal zs_cuts : CutList;

begin

    zs : process (clk, rst) is
        variable suppress : boolean;
        variable v_etwidth : natural range 0 to 16;
    begin
        if rst = '1' then
            d1      <= (others => AWORD_NULL);
            d_ctrl1 <= ACONTROL_NULL;
            d2      <= (others => AWORD_NULL);
            d_ctrl2 <= ACONTROL_NULL;
            d3      <= (others => AWORD_NULL);
            d_ctrl3 <= ACONTROL_NULL;
            d4      <= (others => AWORD_NULL);
            d_ctrl4 <= ACONTROL_NULL;
            d5      <= (others => AWORD_NULL);
            d_ctrl5 <= ACONTROL_NULL;
        else
            if clk'event and clk = '1' then
                zs_cuts(7) <= unsigned(ctrl_reg(zs_threshold_tau_0p5GeV_address)(zs_threshold_tau_0p5GeV_offset + zs_threshold_tau_0p5GeV_width - 1 downto zs_threshold_tau_0p5GeV_offset));
                zs_cuts(6) <= unsigned(ctrl_reg(zs_threshold_tau_0p5GeV_address)(zs_threshold_tau_0p5GeV_offset + zs_threshold_tau_0p5GeV_width - 1 downto zs_threshold_tau_0p5GeV_offset));
                zs_cuts(5) <= unsigned(ctrl_reg(zs_threshold_sum_0p5GeV_address)(zs_threshold_sum_0p5GeV_offset + zs_threshold_sum_0p5GeV_width - 1 downto zs_threshold_sum_0p5GeV_offset));
                zs_cuts(4) <= unsigned(ctrl_reg(zs_threshold_empty_0p5GeV_address)(zs_threshold_empty_0p5GeV_offset + zs_threshold_empty_0p5GeV_width - 1 downto zs_threshold_empty_0p5GeV_offset));
                zs_cuts(3) <= unsigned(ctrl_reg(zs_threshold_eg_0p5GeV_address)(zs_threshold_eg_0p5GeV_offset + zs_threshold_eg_0p5GeV_width - 1 downto zs_threshold_eg_0p5GeV_offset));
                zs_cuts(2) <= unsigned(ctrl_reg(zs_threshold_eg_0p5GeV_address)(zs_threshold_eg_0p5GeV_offset + zs_threshold_eg_0p5GeV_width - 1 downto zs_threshold_eg_0p5GeV_offset));
                zs_cuts(1) <= unsigned(ctrl_reg(zs_threshold_jet_0p5GeV_address)(zs_threshold_jet_0p5GeV_offset + zs_threshold_jet_0p5GeV_width - 1 downto zs_threshold_jet_0p5GeV_offset));
                zs_cuts(0) <= unsigned(ctrl_reg(zs_threshold_jet_0p5GeV_address)(zs_threshold_jet_0p5GeV_offset + zs_threshold_jet_0p5GeV_width - 1 downto zs_threshold_jet_0p5GeV_offset));

                q       <= d5;
                q_ctrl  <= d_ctrl5;
                d5      <= d4;
                d_ctrl5 <= d_ctrl4;
                d4      <= d3;
                d_ctrl4 <= d_ctrl3;
                d3      <= d2;
                d_ctrl3 <= d_ctrl2;
                d2      <= d1;
                d_ctrl2 <= d_ctrl1;
                d1      <= d;
                d_ctrl1 <= d_ctrl;
                if d_ctrl5.valid = '1' and d_ctrl5.strobe = '1' and d_ctrl5.bx_start = '1' then
                    suppress := true;
                    for i in d'range loop
                        case i is
                            when 0|1 =>    -- jets
                                v_etwidth := 11;
                            when 5 =>      -- sums
                                v_etwidth := 12;
                            when others => -- egamma and tau
                                v_etwidth := 9;
                        end case;
                    if  unsigned(d5(i)(v_etwidth -1  downto 0)) > zs_cuts(i) or
                        unsigned(d4(i)(v_etwidth -1  downto 0)) > zs_cuts(i) or
                        unsigned(d3(i)(v_etwidth -1  downto 0)) > zs_cuts(i) or
                        unsigned(d2(i)(v_etwidth -1  downto 0)) > zs_cuts(i) or
                        unsigned(d1(i)(v_etwidth -1  downto 0)) > zs_cuts(i) or
                        unsigned(d(i)(v_etwidth -1  downto 0)) > zs_cuts(i) then
                            suppress := false;
                        end if;
                    end loop;
                    if suppress then
                        q_ctrl.strobe  <= '0';
                        d_ctrl5.strobe <= '0';
                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process zs;

end Behavioral;
