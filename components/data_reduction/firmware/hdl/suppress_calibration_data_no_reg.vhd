library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.top_decl.all;
use work.datatypes.all;

use IEEE.NUMERIC_STD.all;

entity suppress_calibration_data is
    generic (
        NSTREAMS       : integer
    );
    port (
        clk      : in  std_logic;
        rst      : in  std_logic;
        d        : in  adata(NSTREAMS - 1 downto 0);
        d_ctrl   : in  acontrol;
        q        : out adata(NSTREAMS - 1 downto 0);
        q_ctrl   : out acontrol
    );
end suppress_calibration_data;

architecture Behavioral of suppress_calibration_data is

    signal q_suppressed      : adata(NSTREAMS - 1 downto 0);
    signal q_suppressed_ctrl : acontrol;

begin

    q      <= q_suppressed;
    q_ctrl <= q_suppressed_ctrl;

    suppress_calib_sequence : process (clk, rst) is
        variable warning_test_enable_received : boolean;
        variable calib_sequence_in_this_orbit : boolean;
    begin
        if rising_edge(clk) then
            -- We don't want to start suppressing immediately when we recieve the BGo in the header, so we store receipt.
            if d_ctrl.header = '1' then
                if d(0)(BGO_WARNING_TEST_ENABLE) = '1' then -- TODO: This should probably be per channel?
                    warning_test_enable_received := True;
                else
                    warning_test_enable_received := False;
                end if;
            end if;

            -- Once we enter the new orbit we check if we received WarningTestEnable previously. If so we suppress in this orbit.
            if d_ctrl.bx_counter = 1 then
                if warning_test_enable_received then
                    calib_sequence_in_this_orbit := True;
                else
                    calib_sequence_in_this_orbit := False;
                end if;
            end if;

            if d_ctrl.bx_counter > LAST_BX_BEFORE_CALIBRATION_SEQUENCE and calib_sequence_in_this_orbit and d_ctrl.header = '0' then
                q_suppressed_ctrl.strobe     <= '0';
                q_suppressed_ctrl.valid      <= d_ctrl.valid;
                q_suppressed_ctrl.last       <= d_ctrl.last;
                q_suppressed_ctrl.bx_start   <= d_ctrl.bx_start;
                q_suppressed_ctrl.header     <= d_ctrl.header;
                q_suppressed_ctrl.bx_counter <= d_ctrl.bx_counter;
            else
                q_suppressed_ctrl <= d_ctrl;
            end if;
            q_suppressed <= d;
        end if;
    end process;
end Behavioral;

