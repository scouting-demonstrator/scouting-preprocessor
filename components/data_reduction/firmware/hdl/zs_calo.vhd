library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.datatypes.all;

use IEEE.NUMERIC_STD.all;

entity zs_calo is
    generic (
        NSTREAMS       : integer
    );
    port (
        clk        : in std_logic;
        rst        : in std_logic;
        et_cut_jet : in unsigned(15 downto 0);
        et_cut_eg  : in unsigned(15 downto 0);
        et_cut_sum : in unsigned(15 downto 0);
        et_cut_tau : in unsigned(15 downto 0);
        d          : in adata(NSTREAMS - 1 downto 0);
        d_ctrl     : in acontrol;
        q_jet      : out adata(1 downto 0);
        q_eg       : out adata(1 downto 0);
        q_sum      : out adata(1 downto 0);
        q_tau      : out adata(1 downto 0);
        q_ctrl_jet : out acontrol;
        q_ctrl_eg  : out acontrol;
        q_ctrl_sum : out acontrol;
        q_ctrl_tau : out acontrol
    );
end zs_calo;

architecture Behavioral of zs_calo is

    signal d1, d2, d3, d4, d5, q                               : adata(NSTREAMS - 1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5, q_ctrl : acontrol;

    signal d1_jet, d2_jet, d3_jet, d4_jet, d5_jet                           : adata(1 downto 0);
    signal d1_eg , d2_eg , d3_eg , d4_eg , d5_eg                            : adata(1 downto 0);
    signal d1_tau, d2_tau, d3_tau, d4_tau, d5_tau                           : adata(1 downto 0);
    signal d1_sum, d2_sum, d3_sum, d4_sum, d5_sum                           : adata(1 downto 0);
    signal d_ctrl1_jet, d_ctrl2_jet, d_ctrl3_jet, d_ctrl4_jet, d_ctrl5_jet  : acontrol;
    signal d_ctrl1_eg , d_ctrl2_eg , d_ctrl3_eg , d_ctrl4_eg , d_ctrl5_eg   : acontrol;
    signal d_ctrl1_tau, d_ctrl2_tau, d_ctrl3_tau, d_ctrl4_tau, d_ctrl5_tau  : acontrol;
    signal d_ctrl1_sum, d_ctrl2_sum, d_ctrl3_sum, d_ctrl4_sum, d_ctrl5_sum  : acontrol;

begin

    zs : process (clk, rst) is
        variable suppress_jet : boolean;
        variable suppress_eg  : boolean;
        variable suppress_sum : boolean;
        variable suppress_tau : boolean;
        variable v_etwidth : natural range 0 to 16;
    begin
        if rst = '1' then
            d1      <= (others => AWORD_NULL);
            d_ctrl1 <= ACONTROL_NULL;
            d2      <= (others => AWORD_NULL);
            d_ctrl2 <= ACONTROL_NULL;
            d3      <= (others => AWORD_NULL);
            d_ctrl3 <= ACONTROL_NULL;
            d4      <= (others => AWORD_NULL);
            d_ctrl4 <= ACONTROL_NULL;
            d5      <= (others => AWORD_NULL);
            d_ctrl5 <= ACONTROL_NULL;
            q       <= (others => AWORD_NULL);
            q_ctrl  <= ACONTROL_NULL;
            d1_jet      <= (others => AWORD_NULL);
            d_ctrl1_jet <= ACONTROL_NULL;
            d2_jet      <= (others => AWORD_NULL);
            d_ctrl2_jet <= ACONTROL_NULL;
            d3_jet      <= (others => AWORD_NULL);
            d_ctrl3_jet <= ACONTROL_NULL;
            d4_jet      <= (others => AWORD_NULL);
            d_ctrl4_jet <= ACONTROL_NULL;
            d5_jet      <= (others => AWORD_NULL);
            d_ctrl5_jet <= ACONTROL_NULL;
            d1_eg      <= (others => AWORD_NULL);
            d_ctrl1_eg <= ACONTROL_NULL;
            d2_eg      <= (others => AWORD_NULL);
            d_ctrl2_eg <= ACONTROL_NULL;
            d3_eg      <= (others => AWORD_NULL);
            d_ctrl3_eg <= ACONTROL_NULL;
            d4_eg      <= (others => AWORD_NULL);
            d_ctrl4_eg <= ACONTROL_NULL;
            d5_eg      <= (others => AWORD_NULL);
            d_ctrl5_eg <= ACONTROL_NULL;
            d1_sum      <= (others => AWORD_NULL);
            d_ctrl1_sum <= ACONTROL_NULL;
            d2_sum      <= (others => AWORD_NULL);
            d_ctrl2_sum <= ACONTROL_NULL;
            d3_sum      <= (others => AWORD_NULL);
            d_ctrl3_sum <= ACONTROL_NULL;
            d4_sum      <= (others => AWORD_NULL);
            d_ctrl4_sum <= ACONTROL_NULL;
            d5_sum      <= (others => AWORD_NULL);
            d_ctrl5_sum <= ACONTROL_NULL;
            d1_tau      <= (others => AWORD_NULL);
            d_ctrl1_tau <= ACONTROL_NULL;
            d2_tau      <= (others => AWORD_NULL);
            d_ctrl2_tau <= ACONTROL_NULL;
            d3_tau      <= (others => AWORD_NULL);
            d_ctrl3_tau <= ACONTROL_NULL;
            d4_tau      <= (others => AWORD_NULL);
            d_ctrl4_tau <= ACONTROL_NULL;
            d5_tau      <= (others => AWORD_NULL);
            d_ctrl5_tau <= ACONTROL_NULL;
        else
            if clk'event and clk = '1' then
                q  <= d5;  q_ctrl  <= d_ctrl5;
                d5 <= d4;  d_ctrl5 <= d_ctrl4;
                d4 <= d3;  d_ctrl4 <= d_ctrl3;
                d3 <= d2;  d_ctrl3 <= d_ctrl2;
                d2 <= d1;  d_ctrl2 <= d_ctrl1;
                d1 <= d;   d_ctrl1 <= d_ctrl;

                q_jet  <= d5_jet;        q_ctrl_jet  <= d_ctrl5_jet;
                d5_jet <= d4_jet;        d_ctrl5_jet <= d_ctrl4_jet;
                d4_jet <= d3_jet;        d_ctrl4_jet <= d_ctrl3_jet;
                d3_jet <= d2_jet;        d_ctrl3_jet <= d_ctrl2_jet;
                d2_jet <= d1_jet;        d_ctrl2_jet <= d_ctrl1_jet;
                d1_jet <= d(1 downto 0); d_ctrl1_jet <= d_ctrl;

                q_eg   <= d5_eg;         q_ctrl_eg  <= d_ctrl5_eg;
                d5_eg  <= d4_eg;         d_ctrl5_eg <= d_ctrl4_eg;
                d4_eg  <= d3_eg;         d_ctrl4_eg <= d_ctrl3_eg;
                d3_eg  <= d2_eg;         d_ctrl3_eg <= d_ctrl2_eg;
                d2_eg  <= d1_eg;         d_ctrl2_eg <= d_ctrl1_eg;
                d1_eg  <= d(3 downto 2); d_ctrl1_eg <= d_ctrl;

                q_sum  <= d5_sum;        q_ctrl_sum  <= d_ctrl5_sum;
                d5_sum <= d4_sum;        d_ctrl5_sum <= d_ctrl4_sum;
                d4_sum <= d3_sum;        d_ctrl4_sum <= d_ctrl3_sum;
                d3_sum <= d2_sum;        d_ctrl3_sum <= d_ctrl2_sum;
                d2_sum <= d1_sum;        d_ctrl2_sum <= d_ctrl1_sum;
                d1_sum <= d(5 downto 4); d_ctrl1_sum <= d_ctrl;

                q_tau  <= d5_tau;        q_ctrl_tau  <= d_ctrl5_tau;
                d5_tau <= d4_tau;        d_ctrl5_tau <= d_ctrl4_tau;
                d4_tau <= d3_tau;        d_ctrl4_tau <= d_ctrl3_tau;
                d3_tau <= d2_tau;        d_ctrl3_tau <= d_ctrl2_tau;
                d2_tau <= d1_tau;        d_ctrl2_tau <= d_ctrl1_tau;
                d1_tau <= d(7 downto 6); d_ctrl1_tau <= d_ctrl;

                if d_ctrl5.valid = '1' and d_ctrl5.strobe = '1' and d_ctrl5.bx_start = '1' then
                    suppress_jet := true;
                    suppress_eg  := true;
                    suppress_tau := true;
                    suppress_sum := true;
                    for i in d'range loop
                        case i is
                            -- jets
                            when 0|1 =>
                                v_etwidth := 11;
                                if  unsigned(d5(i)(v_etwidth -1  downto 0)) > et_cut_jet or
                                    unsigned(d4(i)(v_etwidth -1  downto 0)) > et_cut_jet or
                                    unsigned(d3(i)(v_etwidth -1  downto 0)) > et_cut_jet or
                                    unsigned(d2(i)(v_etwidth -1  downto 0)) > et_cut_jet or
                                    unsigned(d1(i)(v_etwidth -1  downto 0)) > et_cut_jet or
                                    unsigned(d(i) (v_etwidth -1  downto 0)) > et_cut_jet then
                                    suppress_jet := false;
                                end if;

                            -- egammas
                            when 2|3 =>
                                v_etwidth := 9;
                                if  unsigned(d5(i)(v_etwidth -1  downto 0)) > et_cut_eg or
                                    unsigned(d4(i)(v_etwidth -1  downto 0)) > et_cut_eg or
                                    unsigned(d3(i)(v_etwidth -1  downto 0)) > et_cut_eg or
                                    unsigned(d2(i)(v_etwidth -1  downto 0)) > et_cut_eg or
                                    unsigned(d1(i)(v_etwidth -1  downto 0)) > et_cut_eg or
                                    unsigned(d(i) (v_etwidth -1  downto 0)) > et_cut_eg then
                                    suppress_eg := false;
                                end if;

                            -- sums
                            when 5 =>
                                v_etwidth := 12;
                                if  unsigned(d5(i)(v_etwidth -1  downto 0)) > et_cut_sum or
                                    unsigned(d4(i)(v_etwidth -1  downto 0)) > et_cut_sum or
                                    unsigned(d3(i)(v_etwidth -1  downto 0)) > et_cut_sum or
                                    unsigned(d2(i)(v_etwidth -1  downto 0)) > et_cut_sum or
                                    unsigned(d1(i)(v_etwidth -1  downto 0)) > et_cut_sum or
                                    unsigned(d(i) (v_etwidth -1  downto 0)) > et_cut_sum then
                                    suppress_sum := false;
                                end if;

                            -- taus
                            when 6|7 =>
                                v_etwidth := 9;
                                if  unsigned(d5(i)(v_etwidth -1  downto 0)) > et_cut_tau or
                                    unsigned(d4(i)(v_etwidth -1  downto 0)) > et_cut_tau or
                                    unsigned(d3(i)(v_etwidth -1  downto 0)) > et_cut_tau or
                                    unsigned(d2(i)(v_etwidth -1  downto 0)) > et_cut_tau or
                                    unsigned(d1(i)(v_etwidth -1  downto 0)) > et_cut_tau or
                                    unsigned(d(i) (v_etwidth -1  downto 0)) > et_cut_tau then
                                    suppress_tau := false;
                                end if;
                            -- ???
                            when others =>
                                null;
                        end case;
                    end loop;

                    if suppress_jet then
                        q_ctrl_jet.strobe  <= '0';  q_jet  <= q (1 downto 0);
                        d_ctrl5_jet.strobe <= '0';  d5_jet <= d5(1 downto 0);
                        d_ctrl4_jet.strobe <= '0';  d4_jet <= d4(1 downto 0);
                        d_ctrl3_jet.strobe <= '0';  d3_jet <= d3(1 downto 0);
                        d_ctrl2_jet.strobe <= '0';  d2_jet <= d2(1 downto 0);
                        d_ctrl1_jet.strobe <= '0';  d1_jet <= d1(1 downto 0);
                    end if;

                    if suppress_eg then
                        q_ctrl_eg.strobe  <= '0';  q_eg  <= q (3 downto 2);
                        d_ctrl5_eg.strobe <= '0';  d5_eg <= d5(3 downto 2);
                        d_ctrl4_eg.strobe <= '0';  d4_eg <= d4(3 downto 2);
                        d_ctrl3_eg.strobe <= '0';  d3_eg <= d3(3 downto 2);
                        d_ctrl2_eg.strobe <= '0';  d2_eg <= d2(3 downto 2);
                        d_ctrl1_eg.strobe <= '0';  d1_eg <= d1(3 downto 2);
                    end if;

                    if (suppress_jet and suppress_eg) or suppress_sum then
                        q_ctrl_sum.strobe  <= '0';  q_sum  <= q (5 downto 4);
                        d_ctrl5_sum.strobe <= '0';  d5_sum <= d5(5 downto 4);
                        d_ctrl4_sum.strobe <= '0';  d4_sum <= d4(5 downto 4);
                        d_ctrl3_sum.strobe <= '0';  d3_sum <= d3(5 downto 4);
                        d_ctrl2_sum.strobe <= '0';  d2_sum <= d2(5 downto 4);
                        d_ctrl1_sum.strobe <= '0';  d1_sum <= d1(5 downto 4);
                    end if;

                    if suppress_tau then
                        q_ctrl_tau.strobe  <= '0';  q_tau  <= q (7 downto 6);
                        d_ctrl5_tau.strobe <= '0';  d5_tau <= d5(7 downto 6);
                        d_ctrl4_tau.strobe <= '0';  d4_tau <= d4(7 downto 6);
                        d_ctrl3_tau.strobe <= '0';  d3_tau <= d3(7 downto 6);
                        d_ctrl2_tau.strobe <= '0';  d2_tau <= d2(7 downto 6);
                        d_ctrl1_tau.strobe <= '0';  d1_tau <= d1(7 downto 6);
                    end if;
                end if;
            end if;
        end if;
    end process zs;

end Behavioral;
