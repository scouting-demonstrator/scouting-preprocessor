library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.datatypes.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity zs_bmtf is
    generic (
        NSTREAMS : integer
    );
    port (
        clk    : in std_logic;
        rst    : in std_logic;
        d      : in adata(NSTREAMS - 1 downto 0);
        d_ctrl : in acontrol;
        q      : out adata(NSTREAMS - 1 downto 0);
        q_ctrl : out acontrol
    );
end zs_bmtf;

architecture Behavioral of zs_bmtf is

    signal d1, d2, d3, d4, d5, d6                               : adata(NSTREAMS - 1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5, d_ctrl6 : acontrol;

begin

    zs : process (clk, rst) is
        variable suppress : boolean;
    begin
        if rst = '1' then
            d1      <= (others => AWORD_NULL);
            d_ctrl1 <= ACONTROL_NULL;
            d2      <= (others => AWORD_NULL);
            d_ctrl2 <= ACONTROL_NULL;
            d3      <= (others => AWORD_NULL);
            d_ctrl3 <= ACONTROL_NULL;
            d4      <= (others => AWORD_NULL);
            d_ctrl4 <= ACONTROL_NULL;
            d5      <= (others => AWORD_NULL);
            d_ctrl5 <= ACONTROL_NULL;
        else
            if clk'event and clk = '1' then
                q       <= d5;
                q_ctrl  <= d_ctrl5;
                d5      <= d4;
                d_ctrl5 <= d_ctrl4;
                d4      <= d3;
                d_ctrl4 <= d_ctrl3;
                d3      <= d2;
                d_ctrl3 <= d_ctrl2;
                d2      <= d1;
                d_ctrl2 <= d_ctrl1;
                d1      <= d;
                d_ctrl1 <= d_ctrl;
                if d_ctrl5.valid = '1' and d_ctrl5.strobe = '1' and d_ctrl5.bx_start = '1' then
                    suppress := true;
                    for i in d'range loop
                        if d5(i)(0)  /= '0' or
                           d4(i)(16) /= '0' or
                           d2(i)(0)  /= '0' or
                           d1(i)(16) /= '0' then
                            suppress := false;
                        end if;
                    end loop;

                    if suppress then
                        q_ctrl.strobe  <= '0';
                        d_ctrl5.strobe <= '0';
                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process zs;

end Behavioral;
