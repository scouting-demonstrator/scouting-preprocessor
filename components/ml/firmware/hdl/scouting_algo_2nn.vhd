-- algo with two neural networks
-- Since a neural network module can accept only 1 GMT muon per clock cycle, we can't do up to 8 muons per BX with a single NN.
-- Thus, we instantiate two identical NNs, one taking the first 4 possible muons and the other one taking the other possible 4.
-- This construct prepares the muons for input to the HLS4ML core

library IEEE;
use IEEE.std_logic_1164.all;
use std.textio.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;

entity algo is
   generic (
        NSTREAMS : integer
    );
    port (
        clk    : in  std_logic;
        rst    : in  std_logic;
        d      : in  adata;
        d_ctrl : in  acontrol;
        q      : out adata;
        q_ctrl : out acontrol
        );
end algo;

architecture Behavioral of algo is
    
    -- CONSTANTS
    constant precision_t : integer := 18; -- <18,6> precision
    constant precision_f : integer := 6;
    constant latency     : integer := 13; --depends on the model

    constant nn_number   : integer := 2;
    constant lane_number : integer := 4;
    constant out_time    : integer := latency + 10; -- when to start putting NN output into frame
    constant buff_limit  : integer := latency + 11;

    constant front_filling_e0 : std_logic_vector((precision_f + 8 - 9) - 1 downto 0)           := (others => '0'); -- 8 for division by 256, 9 for number of bits
    constant front_filling_e1 : std_logic_vector((precision_f + 8 - 9) - 1 downto 0)           := (others => '1');
    constant front_filling_p0 : std_logic_vector((precision_f + 8 - 10) - 1 downto 0)          := (others => '0');
    constant end_filling      : std_logic_vector((precision_t - precision_f - 8) - 1 downto 0) := (others => '0'); -- 8 for division by 256, 9 for number of bits

    -- TYPES AND SUBTYPES
    SUBTYPE int_part is integer range 4 * precision_t - 1 downto 4 * precision_t - precision_f;
    
    --NN
    type nn_inp_array  is array (nn_number - 1 downto 0) of std_logic_vector(precision_t * 4 - 1 downto 0);
    type nn_out_array  is array (nn_number - 1 downto 0) of std_logic_vector(precision_t - 1 downto 0);
    type nn_size_array is array (nn_number - 1 downto 0) of std_logic_vector(15 downto 0);
    
    -- Input-output array types
    type muon_array   is array (lane_number - 1 downto 0) of std_logic_vector(precision_t * 4 - 1 downto 0);
    type output_array is array (lane_number - 1 downto 0) of std_logic_vector(precision_t - 1 downto 0);
    type all_output   is array (2 downto 0) of output_array;
    
    -- Buffer types
    type frame_buffer   is array(buff_limit - 1 downto 0) of adata(NSTREAMS - 1 downto 0);
    type control_buffer is array(buff_limit - 1 downto 0) of acontrol;
    type muon_buffer    is array(buff_limit - 1 downto 0) of muon_array;
    type flag_buffer    is array(buff_limit - 1 downto 0) of std_logic_vector(lane_number - 1 downto 0);
    type timing_buffer  is array(buff_limit - 1 downto 0) of std_logic;
    
    -- SIGNALS
    -- NN signals        
    signal start_signals        : std_logic_vector (nn_number - 1 downto 0) := (others => '1');
    signal done_signals         : std_logic_vector (nn_number - 1 downto 0);
    signal idle_signals         : std_logic_vector (nn_number - 1 downto 0);
    signal ready_signals        : std_logic_vector (nn_number - 1 downto 0);
    signal inp_vld_signals      : std_logic_vector (nn_number - 1 downto 0) := (others => '1');
    signal size_in_vld_signals  : std_logic_vector (nn_number - 1 downto 0);
    signal out0_vld_signals     : std_logic_vector (nn_number - 1 downto 0);
    signal out1_vld_signals     : std_logic_vector (nn_number - 1 downto 0);
    signal out2_vld_signals     : std_logic_vector (nn_number - 1 downto 0);
    signal size_out_vld_signals : std_logic_vector (nn_number - 1 downto 0);
    signal out0_signals         : nn_out_array;
    signal out1_signals         : nn_out_array;
    signal out2_signals         : nn_out_array;
    signal size_in1_signals     : nn_size_array;
    signal size_out1_signals    : nn_size_array;
    signal muon_in              : nn_inp_array;

    -- Buffers
    signal d_buffer          : frame_buffer;
    signal d_ctrl_buffer     : control_buffer;
    signal muon1_flag_buffer : flag_buffer;
    signal muon2_flag_buffer : flag_buffer;
    signal start_time_buffer : timing_buffer;

    -- Input-output signals
    signal muon_1 : muon_array;
    signal muon_2 : muon_array;
    
    signal out_signals_1 : all_output;
    signal out_signals_2 : all_output;
    
    -- Flags
    signal inp_flag   : std_logic := '0';
    signal new_muon   : std_logic := '0';
    signal muon1_flag : std_logic_vector(lane_number - 1 downto 0) := (others => '0');
    signal muon2_flag : std_logic_vector(lane_number - 1 downto 0) := (others => '0');

    -- Counters    
    signal out_counter  : std_logic_vector (1 downto 0) := (others => '0');
    signal muon_counter : std_logic_vector (1 downto 0) := (others => '0');
    
    
    -- Signals to write txt
    constant nn_sim      : boolean := false;
    constant semicol     : string := ";"; -- for txt
    constant comma       : string := ","; -- for txt
    constant zero_out    : std_logic_vector(precision_t - 1 downto 0) := (others => '0'); -- for txt
    signal txt_counter   : std_logic_vector(11 downto 0)              := (others => '0'); -- for txt
    signal muon1_buffer  : muon_buffer; -- for txt
    signal muon2_buffer  : muon_buffer; -- for txt
begin
      
    nn_GENERATE_FOR: for i in nn_number - 1 downto 0 generate
        nn_i : entity work.nn7
            port map(
                ap_clk => clk,
                ap_rst => rst,
                ap_start => start_signals(i),
                ap_done => done_signals(i),
                ap_idle => idle_signals(i),
                ap_ready => ready_signals(i),
                fc1_input_V_ap_vld => inp_vld_signals(i),
                fc1_input_V => muon_in(i),
                layer11_out_0_V => out0_signals(i),
                layer11_out_0_V_ap_vld => out0_vld_signals(i),
                layer11_out_1_V => out1_signals(i),
                layer11_out_1_V_ap_vld => out1_vld_signals(i),
                layer11_out_2_V => out2_signals(i),
                layer11_out_2_V_ap_vld => out2_vld_signals(i),
                const_size_in_1 => size_in1_signals(i),
                const_size_in_1_ap_vld => size_in_vld_signals(i),
                const_size_out_1 => size_out1_signals(i),
                const_size_out_1_ap_vld => size_out_vld_signals(i) );
                
    end generate nn_GENERATE_FOR;
  
    data_normalization : process (clk, rst) is

        -- Variables for writing outputs to txt
          variable fstatus  : file_open_status;
          variable out_line : line;
          variable inp_line : line;
          FILE out_file : TEXT OPEN WRITE_MODE IS "out_values";
          FILE inp_file : TEXT OPEN WRITE_MODE IS "inp_values";
          
        begin
        
        if rst = '1' then 
            inp_flag       <= '0';
            new_muon       <= '0'; 
            muon1_flag     <= (others => '0');
            muon2_flag     <= (others => '0');  
            out_counter    <= (others => '0');
            muon_counter   <= (others => '0');
            if nn_sim then
                txt_counter    <= (others => '0');
            end if;       
                      
            muon_1         <= (others => (others => '0'));
            muon_2         <= (others => (others => '0'));
            
            out_signals_1  <= (others => (others => (others => '0')));
            out_signals_2  <= (others => (others => (others => '0')));


        elsif clk'event and clk = '1' then
            
            d_buffer(0)                      <= d;        
            d_buffer(d_buffer'high downto 1) <= d_buffer(d_buffer'high - 1 downto 0);

            d_ctrl_buffer(0)                           <= d_ctrl;
            d_ctrl_buffer(d_ctrl_buffer'high downto 1) <= d_ctrl_buffer(d_ctrl_buffer'high - 1 downto 0);
            
            q      <= d_buffer(d_buffer'high);
            q_ctrl <= d_ctrl_buffer(d_ctrl_buffer'high);
            
            if d_ctrl_buffer(4).valid = '1' and d_ctrl_buffer(4).strobe = '1' and d_ctrl_buffer(4).bx_start = '1' then -- Bx with muon
                new_muon   <= '1';
                inp_flag   <= '1';
                muon1_flag <= "0000";
                muon2_flag <= "0000";
                
                for i in 0 to 3 loop
                    if d_buffer(2)(i)(18 downto 10) /= "000000000" then -- pt is in bits 18 downto 10 -- 1st muon exists

                        if d_buffer(2)(i)(31) = '0' then
                            muon_1(i)(2 * precision_t  -1 downto precision_t) <= front_filling_e0 & d_buffer(2)(i)(31 downto 23) & end_filling; -- eta_ext 9 bits --d4 21 downto 13
                        else
                            muon_1(i)(2 * precision_t  -1 downto precision_t) <= front_filling_e1 & d_buffer(2)(i)(31 downto 23) & end_filling; -- eta_ext 9 bits
                        end if;

                        if d_buffer(1)(i)(1) = '0' then -- negative charge
                            muon_1(i)(4 * precision_t  -1 downto 3 * precision_t) <= (int_part => '1', others => '0');
                        else
                            muon_1(i)(4 * precision_t  -1 downto 3 * precision_t) <= (66 => '1', others => '0');
                        end if;

                        muon_1(i)(precision_t  -1 downto 0) <= front_filling_p0 & d_buffer(2)(i)(9 downto 0) & end_filling; -- phi_ext 10 bits -- 20 downto 11
                        muon_1(i)(3 * precision_t  -1 downto 2 * precision_t)  <= front_filling_e0 & d_buffer(2)(i)(18 downto 10) & end_filling; -- pt_ext 10
                        muon1_flag(i) <= '1';    
                    else
                        muon_1(i) <= (others => '0');
                    end if;
                    
                    if d_buffer(0)(i)(18 downto 10) /= "000000000" then -- pt is in bits 18 downto 10 -- 2nd muon exists
                    
                        if d_buffer(0)(i)(31) = '0' then
                            muon_2(i)(2 * precision_t  -1 downto precision_t) <= front_filling_e0 & d_buffer(0)(i)(31 downto 23) & end_filling; -- eta_ext 9 bits
                        else
                            muon_2(i)(2 * precision_t  -1 downto precision_t) <= front_filling_e1 & d_buffer(0)(i)(31 downto 23) & end_filling; -- eta_ext 9 bits
                        end if;

                        if d(i)(2) = '0' then -- negative charge
                            muon_2(i)(4 * precision_t  -1 downto 3 * precision_t) <= (int_part => '1', others => '0');
                        else
                            muon_2(i)(4 * precision_t  -1 downto 3 * precision_t) <= (66 => '1', others => '0');
                        end if;

                        muon_2(i)(precision_t  -1 downto 0) <= front_filling_p0 & d_buffer(0)(i)(9 downto 0) & end_filling; -- phi_ext 10 bits
                        muon_2(i)(3 * precision_t  -1 downto 2 * precision_t)  <= front_filling_e0 & d_buffer(0)(i)(18 downto 10) & end_filling; -- pt_ext 10
                        muon2_flag(i) <= '1';
                    else
                        muon_2(i) <= (others => '0');
                    end if;
                end loop;
            end if;
            
            muon1_buffer(0) <= muon_1;
            muon2_buffer(0) <= muon_2;

            muon1_buffer(muon1_buffer'high downto 1) <= muon1_buffer(muon1_buffer'high - 1 downto 0);
            muon2_buffer(muon2_buffer'high downto 1) <= muon2_buffer(muon2_buffer'high - 1 downto 0);

            muon1_flag_buffer(0) <= muon1_flag;
            muon2_flag_buffer(0) <= muon2_flag;       
       
            muon1_flag_buffer(muon1_flag_buffer'high downto 1) <= muon1_flag_buffer(muon1_flag_buffer'high - 1 downto 0);
            muon2_flag_buffer(muon2_flag_buffer'high downto 1) <= muon2_flag_buffer(muon2_flag_buffer'high - 1 downto 0);

            start_time_buffer(0)                               <= '0';
            start_time_buffer(start_time_buffer'high downto 1) <= start_time_buffer(start_time_buffer'high - 1 downto 0);
            
            if inp_flag = '1' then
                if ready_signals(0) = '1' then
                    if new_muon = '1' then
                        start_time_buffer(0) <= '1';
                        
                        muon_in(0) <= muon_1(to_integer(unsigned(muon_counter)));
                        muon_in(1) <= muon_2(to_integer(unsigned(muon_counter)));
                        
                        muon_counter <= std_logic_vector(unsigned(muon_counter) + "1");
                        if muon_counter = "11" then
                            new_muon <= '0';
                        end if;
                    end if;    
                end if;
                
                if done_signals(0) = '1' then 
                    if start_time_buffer(latency) = '1' then 
                    
                        out_signals_1(0)(to_integer(unsigned(out_counter))) <= out0_signals(0);
                        out_signals_1(1)(to_integer(unsigned(out_counter))) <= out1_signals(0);
                        out_signals_1(2)(to_integer(unsigned(out_counter))) <= out2_signals(0);

                        out_signals_2(0)(to_integer(unsigned(out_counter))) <= out0_signals(1);
                        out_signals_2(1)(to_integer(unsigned(out_counter))) <= out1_signals(1);
                        out_signals_2(2)(to_integer(unsigned(out_counter))) <= out2_signals(1);
                        
                        out_counter <= std_logic_vector(unsigned(out_counter) + "1");
                    end if;
                end if;

            end if;
            if d_ctrl_buffer(out_time).valid = '1' and d_ctrl_buffer(out_time).strobe = '1' and d_ctrl_buffer(out_time).bx_start = '1' then  -- +10 clock cycle delay
                if txt_counter /= x"fff" then
                    for i in 0 to 3 loop
                        if muon1_flag_buffer(out_time - 6)(i) = '1'then -- 1st muon exists 
                            if nn_sim then
                                hwrite(out_line, out_signals_1(0)(i)(precision_t - 4 downto precision_t - 13), left, 3);
                                write(out_line, comma, left, 1);
                                hwrite(out_line, out_signals_1(1)(i)(precision_t - 4 downto precision_t - 12), left, 3);
                                write(out_line, comma, left, 1);
                                hwrite(out_line, out_signals_1(2)(i)(precision_t - 1 downto precision_t - 9), left, 3);
                                write(out_line, semicol, left, 1);
                            end if;

                            -- frame 2
                            d_buffer(out_time - 1)(i + 4)(9  downto  0) <= out_signals_1(0)(i)(precision_t - 4 downto precision_t - 13);
                            d_buffer(out_time - 1)(i + 4)(31 downto 23) <= out_signals_1(1)(i)(precision_t - 4 downto precision_t - 12);
                            d_buffer(out_time - 1)(i + 4)(18 downto 10) <= out_signals_1(2)(i)(precision_t - 1 downto precision_t - 9);

                        else
                            if nn_sim then
                                hwrite(out_line, zero_out, left, 4);
                                write(out_line, comma, left, 1);
                                hwrite(out_line, zero_out, left, 4);
                                write(out_line, comma, left, 1);
                                hwrite(out_line, zero_out, left, 4);
                                write(out_line, semicol, left, 1);
                             end if;
  
                            d_buffer(out_time - 1)(i + 4)(9  downto  0) <= (others => '0');
                            d_buffer(out_time - 1)(i + 4)(31 downto 23) <= (others => '0');
                            d_buffer(out_time - 1)(i + 4)(18 downto 10) <= (others => '0');
                            
                            d_buffer(out_time - 2)(i + 4)(9  downto  0) <= (others => '0');
                            d_buffer(out_time - 2)(i + 4)(31 downto 23) <= (others => '0');
                            d_buffer(out_time - 2)(i + 4)(18 downto 10) <= (others => '0');

                        end if;
                        if muon2_flag_buffer(out_time - 6)(i) = '1'then -- 2nd muon exists 
                            if nn_sim then
                                hwrite(out_line, out_signals_2(0)(i)(precision_t - 4 downto precision_t - 13), left, 3);
                                write(out_line, comma, left, 1);
                                hwrite(out_line, out_signals_2(0)(i)(precision_t - 4 downto precision_t - 12), left, 3);
                                write(out_line, comma, left, 1);
                                hwrite(out_line, out_signals_2(0)(i)(precision_t - 1 downto precision_t - 9), left, 3);
                                write(out_line, semicol, left, 1);
                            end if;

                            -- frame 4
                            d_buffer(out_time - 3)(i + 4)(9  downto  0) <= out_signals_2(0)(i)(precision_t - 4 downto precision_t - 13);
                            d_buffer(out_time - 3)(i + 4)(31 downto 23) <= out_signals_2(1)(i)(precision_t - 4 downto precision_t - 12);
                            d_buffer(out_time - 3)(i + 4)(18 downto 10) <= out_signals_2(2)(i)(precision_t - 1 downto precision_t - 9);

                        else
                            if nn_sim then                        
                                hwrite(out_line, zero_out, left, 4);
                                write(out_line, comma, left, 1);
                                hwrite(out_line, zero_out, left, 4);
                                write(out_line, comma, left, 1);
                                hwrite(out_line, zero_out, left, 4);
                                write(out_line, semicol, left, 1);
                            end if;
                            
                            d_buffer(out_time - 3)(i + 4)(9  downto  0) <= (others => '0');
                            d_buffer(out_time - 3)(i + 4)(31 downto 23) <= (others => '0');
                            d_buffer(out_time - 3)(i + 4)(18 downto 10) <= (others => '0');
                            
                            d_buffer(out_time - 4)(i + 4)(9  downto  0) <= (others => '0');
                            d_buffer(out_time - 4)(i + 4)(31 downto 23) <= (others => '0');
                            d_buffer(out_time - 4)(i + 4)(18 downto 10) <= (others => '0');

                        end if;
                            if nn_sim then
                                hwrite(inp_line, muon1_buffer(out_time - 6)(i), left, 18);
                                write(inp_line, semicol, left, 1);
                                hwrite(inp_line, muon2_buffer(out_time - 6)(i), left, 18);
                                write(inp_line, semicol, left, 1);
                            end if;
                    end loop;

                    if nn_sim then                        
                        writeline(out_file, out_line);
                        writeline(inp_file, inp_line);
                    end if;
                    txt_counter <= std_logic_vector(unsigned(txt_counter) + "1");
                end if;
            end if;
        end if;
    end process;
    
end Behavioral;
