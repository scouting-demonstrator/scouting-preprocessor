library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.datatypes.all;

entity normalizer is
    generic (
        NSTREAMS : integer 
        );
    port (
        clk    : in  std_logic;
        rst    : in  std_logic;
        d      : in  adata(NSTREAMS-1 downto 0);
        d_ctrl : in  acontrol;
        q      : out adata(NSTREAMS-1 downto 0);
        mu_packet_valid : out std_logic := '0';
        mu_packet : out std_logic_vector(2047 downto 0) := (others => '0')
        );
end normalizer;

architecture Behavioral of normalizer is

    signal d1, d2, d3, d4, d5, d6 : adata(NSTREAMS-1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5, d_ctrl6 : acontrol;
    
begin

    normalize: process(clk, rst) is
      variable valid_muon : boolean;
    begin
        if rst = '1' then
            d1 <= (others => AWORD_NULL); d_ctrl1 <=('0', '0', '0', '0', '0', 1);
            d2 <= (others => AWORD_NULL); d_ctrl2 <=('0', '0', '0', '0', '0', 1);
            d3 <= (others => AWORD_NULL); d_ctrl3 <=('0', '0', '0', '0', '0', 1);
            d4 <= (others => AWORD_NULL); d_ctrl4 <=('0', '0', '0', '0', '0', 1);
            d5 <= (others => AWORD_NULL); d_ctrl5 <=('0', '0', '0', '0', '0', 1);
            d6 <= (others => AWORD_NULL); d_ctrl6 <=('0', '0', '0', '0', '0', 1);
        else
           if clk'event and clk = '1' then
                d5 <= d4; d_ctrl5 <= d_ctrl4;
                d4 <= d3; d_ctrl4 <= d_ctrl3;
                d3 <= d2; d_ctrl3 <= d_ctrl2;
                d2 <= d1; d_ctrl2 <= d_ctrl1;
                d1 <= d;  d_ctrl1 <= d_ctrl;
                
                valid_muon := false;
                
                if d_ctrl5.valid ='1' and d_ctrl5.strobe='1' and d_ctrl5.bx_start = '1' then
                 
                    for i in d'range loop
                                        
                       if d3(i)(18 downto 10) /= "000000000" and d4(i)(31) = '0' then -- pt is in bits 18 downto 10, -- check for intermediate muon
                         --       i*48 + 16xNparam  + offset downto i*256 + 16*Nparam
                         mu_packet(      i*256 + 0 + 8 downto    i*256  +  0)  <=  d3(i)(18 downto 10); -- pT
                         mu_packet(      i*256 + 16 + 9 downto   i*256 +  16)  <=  d2(i)(20 downto 11); -- phi_raw
                         mu_packet(      i*256 + 32 + 8 downto   i*256 +  32)  <=  d4(i)(29 downto 21); -- eta_raw
                         mu_packet(      i*256 + 48 + 1 downto   i*256 +  48)  <=  d2(i)(3  downto  2); -- charge + charge valid
                         mu_packet(      i*256 + 64 + 3 downto   i*256 +  64)  <=  d3(i)(22 downto 19); -- quality
                         mu_packet(      i*256 + 80 + 12 downto  i*256 +  80)  <=  d4(i)(12 downto  0); -- bunch counter
                         mu_packet(      i*256 + 96 + 15 downto  i*256 +  96)  <=  d5(i)(15 downto  0); -- orbit counter part 1
                         mu_packet(      i*256 + 112 + 15 downto i*256 + 112)  <=  d5(i)(31 downto 16); -- orbit number part 2
                         valid_muon := true;
                       else
                        mu_packet(       i*256 + 127      downto i*256      ) <= (others=>'0'); -- set muon words to 0 if no valid muon
                       end if;
                       
                       if d1(i)(18 downto 10) /= "000000000" and d4(i)(31) = '0' then -- d3 has 1st word of 1st muon, d1 has 1st word of 2nd muon, -- check for intermediate muon
                         mu_packet(128 + i*256 + 0  + 8  downto 128 + i*256 + 0)   <=  d1(i)(18 downto 10); -- pT
                         mu_packet(128 + i*256 + 16 + 9  downto 128 + i*256 + 16)  <=  d(i)( 20 downto 11); -- phi_raw
                         mu_packet(128 + i*256 + 32 + 8  downto 128 + i*256 + 32)  <=  d4(i)(21 downto 13); -- eta_raw
                         mu_packet(128 + i*256 + 48 + 1  downto 128 + i*256 + 48)  <=  d(i)(  3 downto  2); -- charge + charge valid
                         mu_packet(128 + i*256 + 64 + 3  downto 128 + i*256 + 64)  <=  d1(i)(22 downto 19); -- quality
                         mu_packet(128 + i*256 + 80 + 12 downto 128 + i*256 + 80)  <=  d4(i)(12 downto  0); -- bunch counter
                         mu_packet(128 + i*256 + 96 + 15 downto 128 + i*256 + 96)  <=  d5(i)(15 downto  0); -- orbit counter part 1
                         mu_packet(128 + i*256 + 112 +15 downto 128 + i*256 + 112) <=  d5(i)(31 downto 16); -- orbit number part 2
                         valid_muon := true;
                       else
                         mu_packet(128 + i*256 + 127     downto 128 + i*256      ) <= (others=>'0');  -- set muon words to 0 if no valid muon
                       end if;
                                                 
                   end loop;
                   
                    if valid_muon = true then
                     mu_packet_valid <= '1';
                     else
                     mu_packet_valid <= '0';
                     end if;    
                 end if;

               end if;
            end if;
    end process normalize;

end Behavioral;