-- Common data types
--
-- D. R., May 2018

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;


use work.top_decl.all;
use work.algo_decl.all;

package datatypes is
    constant LWORD_WIDTH : integer := 32;
    type lword is
    record
        data   : std_logic_vector(LWORD_WIDTH - 1 downto 0);
        valid  : std_logic;
        done   : std_logic;
        strobe : std_logic;
    end record;
    type ldata is array(natural range <>) of lword;
    constant LWORD_NULL : lword             := ((others => '0'), '0', '0', '0');
    constant LWORD_PAD  : lword             := (x"F7F7F7F7", '1', '0', '1');
    constant LDATA_NULL : ldata(0 downto 0) := (0       => LWORD_NULL);

    subtype aword is std_logic_vector(LWORD_WIDTH - 1 downto 0);
    type adata is array(natural range <>) of aword;
    type acontrol is record
        valid      : std_logic;
        strobe     : std_logic;
        bx_start   : std_logic;
        last       : std_logic;
        header     : std_logic;
        bx_counter : integer range 1 to 3564;
    end record;

    type SRegister is array (natural range <>) of std_logic_vector(31 downto 0);

    constant moni_ctrl_register_address_width : natural := full_register_address_width  - 1;

    -- delay lines
    type ADataDelayline is array(natural range <>) of adata(4*N_REGION -1 downto 0);
    type AControlDelayline is array(natural range <>) of acontrol;
    type Unsigned64bDelayline is array(natural range <>) of unsigned(63 downto 0);
    type Unsigned16bDelayline is array(natural range <>) of unsigned(15 downto 0);
    type LogicVector16bDelayline is array(natural range <>) of std_logic_vector(15 downto 0);
    type SRegisterDelayline is array(natural range <>) of SRegister(2 ** moni_ctrl_register_address_width - 1 downto 0);

    constant AWORD_NULL    : aword             := (others => '0');
    constant AWORD_PAD     : aword             := x"F7F7F7F7";
    constant ADATA_NULL    : adata(0 downto 0) := (0 => AWORD_NULL);
    constant ACONTROL_NULL : acontrol          := ('0', '0', '0', '0', '0', 1);

    type TLinkBuffer is array (natural range <>) of ldata(7 downto 0);  -- This needs to stay 8 wide because it is sometimes used as a temporal buffer

    type TAuxInfo is array (natural range <>) of std_logic_vector(31 downto 0);

    type state_buf is (direct_read, fill_buffer, wait_s1, drain_buffer, wait_s2);

    type TReadPointer is array (natural range <>) of unsigned(3 downto 0);

    type TCounter32b is array (natural range <>) of unsigned(31 downto 0);
    type TCounter64b is array (natural range <>) of unsigned(63 downto 0);

    type TBus32b is array (natural range <>) of std_logic_vector(31 downto 0);

    type TStream_aframe is array (natural range <>) of adata(7 downto 0);
    type TStream_acontrol is array (natural range <>) of acontrol;

    type TQuadMap is array (natural range <>) of integer;

    -- DEBUG
    type state_vec is array (natural range <>) of state_buf;
    type TWordCnt is array (natural range <>) of unsigned(31 downto 0);

end datatypes;
