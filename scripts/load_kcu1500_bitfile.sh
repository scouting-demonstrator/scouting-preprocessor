#!/usr/bin/env bash

set -e # Exit on error

if [ $# -lt 2 ]; then
    echo "#### Usage:"
    echo "## " "$0" "board_name bitfile_repo_path [vivado_edition]"
    echo "# Example " "$0" "kcu1500_ugmt /opt/l1scouting-hardware/bitfiles vivado_lab"
    echo "## Exiting.."
    exit 1
elif [ $# == 2 ]; then
    echo 'vivado_edition not specified, defaulting to Vivado_lab'
    VIVADO_EDITION="vivado_lab"
else
    VIVADO_EDITION=$3
fi
BOARD=$1
bitfile_repo_path=$2

board_setup () {
    printf "setting up $BOARD"
    curl -X POST -F "value=1" localhost:8080/$BOARD/rst_i2c/write
    exit_status=$?
    sleep 1e-02
    curl -X POST -F "value=0" localhost:8080/$BOARD/rst_i2c/write
    exit_status=$(($? + $exit_status))
    sleep 1e-02
    curl -X POST -F "value=1" localhost:8080/$BOARD/str_wr/write
    exit_status=$(($? + $exit_status))
    sleep 1e-02
    curl -X POST -F "value=0" localhost:8080/$BOARD/str_wr/write
    exit_status=$(($? + $exit_status))
    sleep 1e-02
    return $exit_status
}

load_bitfile (){
    $VIVADO_EDITION -mode batch -source "${bitfile_repo_path}"/currently_used/vivado_load_bitfile.tcl 
}

echo "Now loading bitfile for the first time, this could take a moment.."
# We replace the bitfile repo path here in a slightly convoluted way:
# As the directory isn't usually writable by our user (and shouldn't be!) we can't do a simple sed even though the tcl file is writable.
# Instead we use a temporary file and then copy it over to the correct location.
sed "s,#BITFILE_REPO_PATH#,${bitfile_repo_path},g" "${bitfile_repo_path}"/currently_used/vivado_load_bitfile.tcl >/tmp/vivado_load_bitfile.tcl && cat /tmp/vivado_load_bitfile.tcl | sudo tee "${bitfile_repo_path}"/currently_used/vivado_load_bitfile.tcl
load_bitfile
# Rescan PCIe bus
echo "Rescanning the PCIe bus"
sudo /usr/sbin/pcie_reconnect_xilinx.sh
sudo service scone start
for i in 1 2 3 4 5; do sleep 2; board_setup && break || sleep 1; done
sudo service scone stop
echo "Now loading bitfile for the second time, this could take a moment.."
load_bitfile

# Rescan PCIe bus
echo "Rescanning the PCIe bus"
sudo /usr/sbin/pcie_reconnect_xilinx.sh
sleep 1e-02

echo "load bitfile script completed"

