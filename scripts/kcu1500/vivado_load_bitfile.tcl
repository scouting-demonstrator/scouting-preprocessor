;# This can be called with `vivado_lab -mode batch -source vivado_load_bitfile.tcl`
open_hw
connect_hw_server -url localhost:3121
current_hw_target [get_hw_targets */xilinx_tcf/Xilinx/1234-tulA]
set_property PARAM.FREQUENCY 15000000 [get_hw_targets */xilinx_tcf/Xilinx/1234-tulA]
open_hw_target
set_property PROGRAM.FILE {#BITFILE_REPO_PATH#/currently_used/scouting_build.bit} [get_hw_devices xcku115_0]
set_property PROBES.FILE {#BITFILE_REPO_PATH#/currently_used/scouting_build.ltx} [get_hw_devices xcku115_0]
set_property FULL_PROBES.FILE {#BITFILE_REPO_PATH#/currently_used/scouting_build.ltx} [get_hw_devices xcku115_0]
current_hw_device [get_hw_devices xcku115_0]
refresh_hw_device [lindex [get_hw_devices xcku115_0] 0]
program_hw_devices [get_hw_devices xcku115_0]
refresh_hw_device [lindex [get_hw_devices xcku115_0] 0]

