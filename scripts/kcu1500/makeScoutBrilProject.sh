#!/bin/bash
set -e # Exit on error.
if [ -f buildToolSetup.sh ] ; then
    source buildToolSetup.sh
fi

# Check environmental variables
if [ -z ${XILINX_VIVADO:+x} ]; then
    echo "Xilinx Vivado environment has not been sourced. Exiting."
    exit 1
else
    echo "Found Xilinx Vivado at" ${XILINX_VIVADO}
fi

if [ -z "${BUILD_DIR}" ]; then
    echo "Environment variable BUILD_DIR is unset. Exiting."
    exit 1
else
    echo "BUILD_DIR is ${BUILD_DIR}"
fi

if [ -z "${MP7FW_TAG}" ]; then
    echo "Environment variable  MP7FW_TAG is unset. Exiting."
    exit 1
else
    echo "MP7FW_TAG is ${MP7FW_TAG}"
fi

# Generate decoder
cp boards/kcu1500/addrtab/address_table_ugmt.json boards/kcu1500/addrtab/address_table.json
mkdir -p boards/kcu1500/firmware/hdl/
scripts/common/generate_decoder.py boards/kcu1500/addrtab/address_table.json boards/kcu1500/firmware/hdl/decoder_constants_kcu1500.vhd ugmt kcu1500

mkdir $BUILD_DIR
pushd $BUILD_DIR
ipbb init scouting
mkdir scouting/src/scouting-preprocessor
pushd scouting/src
if [ -z ${CI_JOB_TOKEN+x} ]; then
  ipbb add git -b $MP7FW_TAG https://:@gitlab.cern.ch:8443/cms-cactus/firmware/mp7.git
else
  ipbb add git -b $MP7FW_TAG https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/cms-cactus/firmware/mp7.git
fi
popd
pushd scouting/src/scouting-preprocessor
ln -sf ../../../../projects
ln -sf ../../../../components
ln -sf ../../../../boards
popd
pushd scouting/proj
ipbb add git https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/bril-phase2/bril_histogram.git
popd
ln -sf ../../../../build/scouting/src/bril_histogramm
pushd scouting
ipbb proj create vivado scouting_build scouting-preprocessor:projects/kcu1500/bril top_scouting_bril.dep
ipbb toolbox check-dep vivado scouting-preprocessor:projects/kcu1500/bril top_scouting_bril.dep
pushd proj/scouting_build/
ipbb vivado project
popd
popd
