;# This can be called with `vivado_lab -mode batch -source vivado_load_bitfile.tcl`
if { $argc != 1 } {
    puts "The tcl script requires a board index to be inputed"
    puts "Please try again."
} else {
    lassign $argv boardIdx
}
open_hw
connect_hw_server -url localhost:3121
set hwTargetList [get_hw_targets]
set targetName [lindex $hwTargetList $boardIdx]
current_hw_target [get_hw_targets $targetName]
open_hw_target
current_hw_device [get_hw_devices xcvu37p_0]

boot_hw_device  [lindex [get_hw_devices xcvu37p_0] 0]
refresh_hw_device [lindex [get_hw_devices xcvu37p_0] 0]
