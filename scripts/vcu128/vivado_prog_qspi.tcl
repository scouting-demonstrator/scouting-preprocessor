;# This can be called with `vivado_lab -mode batch -source vivado_load_bitfile.tcl`
if { $argc != 1 } {
    puts "The tcl script requires a board index to be inputed"
    puts "Please try again."
} else {
    lassign $argv boardIdx
}
open_hw
connect_hw_server -url localhost:3121
set hwTargetList [get_hw_targets]
set targetName [lindex $hwTargetList $boardIdx]
current_hw_target [get_hw_targets $targetName]
open_hw_target
current_hw_device [get_hw_devices xcvu37p_0]

create_hw_cfgmem -hw_device [lindex [get_hw_devices xcvu37p_0] 0] [lindex [get_cfgmem_parts {mt25qu02g-spi-x1_x2_x4}] 0]
set_property PROGRAM.FILES [list "#BITFILE_REPO_PATH#/currently_used/scouting_build.bin" ] [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices xcvu37p_0] 0]]
set_property PROGRAM.PRM_FILE {} [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices xcvu37p_0] 0]]
set_property PROGRAM.UNUSED_PIN_TERMINATION {pull-none} [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices xcvu37p_0] 0]]
set_property PROGRAM.BLANK_CHECK  0 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices xcvu37p_0] 0]]
set_property PROGRAM.ERASE  1 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices xcvu37p_0] 0]]
set_property PROGRAM.CFG_PROGRAM  1 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices xcvu37p_0] 0]]
set_property PROGRAM.VERIFY  1 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices xcvu37p_0] 0]]
set_property PROGRAM.CHECKSUM  0 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices xcvu37p_0] 0]]
refresh_hw_device [lindex [get_hw_devices xcvu37p_0] 0]
startgroup
create_hw_bitstream -hw_device [lindex [get_hw_devices xcvu37p_0] 0] [get_property PROGRAM.HW_CFGMEM_BITFILE [ lindex [get_hw_devices xcvu37p_0] 0]]; program_hw_devices [lindex [get_hw_devices xcvu37p_0] 0]; refresh_hw_device [lindex [get_hw_devices xcvu37p_0] 0];
program_hw_cfgmem -hw_cfgmem [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices xcvu37p_0] 0]]

boot_hw_device  [lindex [get_hw_devices xcvu37p_0] 0]
refresh_hw_device [lindex [get_hw_devices xcvu37p_0] 0]
