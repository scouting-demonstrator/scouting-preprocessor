;# This can be called with `vivado_lab -mode batch -source vivado_load_bitfile.tcl`
if { $argc != 1 } {
    puts "The tcl script requires a board index to be inputed"
    puts "Please try again."
} else {
    lassign $argv boardIdx
}
open_hw
connect_hw_server -url localhost:3121
set hwTargetList [get_hw_targets]
set targetName [lindex $hwTargetList $boardIdx]
current_hw_target [get_hw_targets $targetName]
open_hw_target
current_hw_device [get_hw_devices xcvu37p_0]

set_property PROBES.FILE {#BITFILE_REPO_PATH#/currently_used/#BOARD_INDEX#/scouting_build.ltx} [get_hw_devices xcvu37p_0]
set_property FULL_PROBES.FILE {#BITFILE_REPO_PATH#/currently_used/#BOARD_INDEX#/scouting_build.ltx} [get_hw_devices xcvu37p_0]
set_property PROGRAM.FILE {#BITFILE_REPO_PATH#/currently_used/#BOARD_INDEX#/scouting_build.bit} [get_hw_devices xcvu37p_0]
program_hw_devices [get_hw_devices xcvu37p_0]
refresh_hw_device [lindex [get_hw_devices xcvu37p_0] 0]
