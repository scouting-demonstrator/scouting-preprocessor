#!/usr/bin/env python3

#-------------------------------------------------------------------------------
#
# Import packages
#
#-------------------------------------------------------------------------------
import re
import sys
import os
import optparse
#-------------------------------------------------------------------------------





#-------------------------------------------------------------------------------
#
# Command-line parser
#
#-------------------------------------------------------------------------------
usage = "usage: %prog [options]"
parser = optparse.OptionParser(usage)
parser.add_option("-v", "--verbose",   action="store_true", dest="verbose",                  default=False,                          help="Increase verbosity")
parser.add_option("-a", "--addrpath",  action="store",      dest="addrpath",  type="string", default="boards/vcu128/addrtab/pyhal/", help="Address structure path")
parser.add_option("-b", "--boardname", action="store",      dest="boardname", type="string", default="vcu128",                       help="Board name")
parser.add_option("-t", "--boardtype", action="store",      dest="boardtype", type="string", default="bmtftcp",                      help="Board type")
(options, args) = parser.parse_args()
#-------------------------------------------------------------------------------





#-------------------------------------------------------------------------------
#
# Create the Structure
#
#-------------------------------------------------------------------------------
# debug mode
VERBOSE   = options.verbose     # Verbosity
ADDRPATH  = options.addrpath    # Address structure file
BOARDNAME = options.boardname   # Board name
BOARDTYPE = options.boardtype   # Board type

# declare the array for the structure
# "+" specifies a level: Module_name      number_of_module    Number_of Level_in primary module
structure      = [[ 0 for k in range(4)] for i in range(300)]
add_table_file = []
#-------------------------------------------------------------------------------





#-------------------------------------------------------------------------------
#
# read the structure
# and create the 2D Array ['Level', 'Module', '#number of iteration(s)', '#level(s) 1 or 2']
#
#-------------------------------------------------------------------------------
file = open(f"{ADDRPATH}/{BOARDNAME}_{BOARDTYPE}_address_structure.txt","r")
line = file.readline()
structure_line = 0
while line:
    # create 2D array with the item lines
    if line[0:1]=='+' or line[0:1]=='0':
        structure[structure_line][0] = line.split()[0]      # Config / First Level / Second Level
        structure[structure_line][1] = line.split()[1]      # Module name
        add_table_file.append(line.split()[1]+".txt")       # Module name + .txt: file used below to create the tables
        structure[structure_line][2] = line.split()[2]      # number of times the module is used

        # specifies a primary module
        if line.split()[0] == '+':
            master_level = structure_line  # store the module number as a Master

        # Specifies in a primary module how many secondary module(s) is inserted in this primaruy module
        if line.split()[0] == '++':
            structure[master_level][3] = structure[master_level][3] + 1

        structure_line += 1

    line = file.readline()
file.close()

print(add_table_file)
nb_module = len(add_table_file)
#-------------------------------------------------------------------------------





#-------------------------------------------------------------------------------
#
# Create the items 3D array
#
#-------------------------------------------------------------------------------
# define the array for the items (Functions name)
modules = [[["--" for k in range(6)] for i in range(1000)]  for j in range(nb_module)]

# This dictionnary informs the place of the module in the 3D array above
module_name_place = {'' : 0}

# informs the number of item per module
nb_item_per_module = [0 for k in range(nb_module)]

# dictionnary with module name:
# memory space need by the module in number of w64b words (Should multiply by 8 to be in bytes addressing)
module_add_space= {'' : 0}
#-------------------------------------------------------------------------------





#-------------------------------------------------------------------------------
#
# create the array of items for all molules
# read all item of all modules and generate a 3D Array [module][Items][item fields]
#
#-------------------------------------------------------------------------------
for i in range(0, nb_module):
    # read each file module
    file = open(ADDRPATH + "/items/" + add_table_file[i], "r")
    line = file.readline()
    cnt = 0
    compute_module_space = 0
    module_name, ext = os.path.splitext(add_table_file[i])
    while line:
        # check if line is empty and treat empty lines like comment lines
        if (re.match('^\s*$', line)):
            comment = "--"
        else:
            # check if the line start with '--'
            # then it is a commented line
            comment = line.split()[0][0:2]

        if (comment != '--'):
            # create a 3D array with the item lines
            modules[i][cnt] = line.split()

            # if in a line of module the 'second column' and 'third column'
            # have the same value it is a item space
            # otherwise it is a subset of the item 'second column'
            if ( modules[i][cnt][0] == modules[i][cnt][1]):
                # count number of offsets per modules (1 offset when Name = SubName)
                compute_module_space = compute_module_space + 1

            cnt += 1
        line = file.readline()

    file.close()
    # count number of item(s) per module
    nb_item_per_module[i] = cnt

    module_name_place[module_name] = i
    module_add_space[module_name] = compute_module_space  # add one empty space
#-------------------------------------------------------------------------------





#-------------------------------------------------------------------------------
#
# Read address table version
#
#-------------------------------------------------------------------------------
# read address_table_version file and value
atv_file  = open(ADDRPATH + "address_table_version.txt", "r")
atv_value = int(atv_file.readline())

# close the address_table_version file
atv_file.close()
#-------------------------------------------------------------------------------





#-------------------------------------------------------------------------------
#
# Create the files
#   VHDL file
#
#-------------------------------------------------------------------------------
# number use to create offset number of the same Module
address_item = 0    # incr at each item

# write the headers
vhdl_file = open(ADDRPATH + "../../dth/firmware/hdl/address_table_" + BOARDNAME + ".vhd", "w")
vhdl_file.write("library ieee;\n")
vhdl_file.write("library work;\n")
vhdl_file.write("use ieee.std_logic_1164.all;\n")
vhdl_file.write("use ieee.std_logic_arith.all;\n")
vhdl_file.write("use ieee.numeric_std.all;\n")
vhdl_file.write("\n")
vhdl_file.write("\n")
vhdl_file.write("package address_table is\n")
vhdl_file.write("\n")

# Write the address_table_version value
# wr_line = "     constant address_table_version_Value                               : integer := 16#"+'%0*x' % (4, atv_value)+"#;\n\n"
wr_line = "    constant address_table_version_Value                                 : integer := 16#"+'%0*x' % (4, atv_value)+"#;\n\n"
vhdl_file.write(wr_line)

# iterate all modules in primary level
for i in range(0, structure_line):

    # primary module
    if structure[i][0]=="+":

        # new module
        module_name_i = structure[i][1]

        # calculate the offset
        offset_space  = module_add_space[module_name_i]     # Space per offset

        if "scouting" in module_name_i:
            print(module_name_i)

        # compute the complete space (primary and secondary level
        if structure[i][3] != 0:    # check if a secondary level is associated to this primary level
            for j in range(structure[i][3]):
                # space of module on next line of structure * number of module
                offset_space = offset_space + ((module_add_space[structure[i+1+j][1]] * int(structure[i+1+j][2])))


        #-----------------------------------------------------------------------
        # FIRST LEVEL
        #-----------------------------------------------------------------------
        ### write the OFFSET constants in VHDL file
        # module name + number of offset
        wr_line = "type "+module_name_i+"_offset_constant is array (0 to "+str(int(structure[i][2])-1)+") of integer;\n"
        vhdl_file.write(wr_line)

        # module name + module name
        wr_line = "    constant  Offset_"+module_name_i+" : "+module_name_i+"_offset_constant := ("
        # loop on number time the same module
        for j in range(int(structure[i][2])):
            wr_line = wr_line + "16#" + '%0*x' % (4, (offset_space*j)) + "#"
            wr_line = wr_line + ","

        wr_line = wr_line + "others => 0);\n"
        vhdl_file.write(wr_line)
        vhdl_file.write(" \n")


        ### Write all ITEMS in the VHDL file of the primary module
        for j in range(nb_item_per_module[module_name_place[module_name_i]]):
            if ( modules[module_name_place[module_name_i]][j][0] == modules[module_name_place[module_name_i]][j][1]):
                if "scouting" in module_name_i:
                    item = '{:60s}'.format(modules[module_name_place[module_name_i]][j][0] + "_func")
                else:
                    item = '{:60s}'.format(modules[module_name_place[module_name_i]][j][0])

                wr_line = "    constant "+item+"                               : integer := 16#"+'%0*X' % (4, address_item)+"#;"
                vhdl_file.write(wr_line)
                vhdl_file.write("\n")

                address_item = address_item + 1

        vhdl_file.write("\n")



        #-----------------------------------------------------------------------
        # SECOND LEVEL
        #-----------------------------------------------------------------------
        ### write the items of the second level
        second_level_address_item = 0

        for k in range(structure[i][3]):
            next_module_name = structure[i+1+k][1]

            second_offset_space  = module_add_space[next_module_name]       # Space per offset + the oactual offset of the local place

            # Write the OFFSET constants in VHDL file
            # write the address table for C++
            # module name + number of offset
            wr_line = "type "+module_name_i+"_"+next_module_name+"_offset_constant is array (0 to "+str(int(structure[I+1+K][2])-1)+") of integer;\n"
            vhdl_file.write(wr_line)

            # module name + module name
            wr_line = "    constant  Offset_" + module_name_i + "_" + next_module_name + " : " + module_name_i + "_" + next_module_name + "_offset_constant := ("
            # loop on number time the same module
            for j in range(int(structure[i+1+k][2])):
                wr_line = wr_line + "16#" + '%0*x' % (4, ((second_offset_space*j)+address_item)) + "#"
                wr_line = wr_line + ","

            wr_line = wr_line + "others => 0);\n"
            vhdl_file.write(wr_line)
            vhdl_file.write("\n")


            # Write all ITEMS in the VHDL file of the primary module
            for j in range(nb_item_per_module[module_name_place[next_module_name]]):
                if ( modules[module_name_place[next_module_name]][j][0] == modules[module_name_place[next_module_name]][j][1]):
                    item = '{:60s}'.format(module_name_i+"_"+modules[module_name_place[next_module_name]][j][0])
                    wr_line = "    constant " + item + "                               : integer := 16#" + '%0*x' % (4, second_level_address_item) + "#;"
                    vhdl_file.write(wr_line)
                    vhdl_file.write("\n")

                    address_item              = address_item + 1
                    second_level_address_item = second_level_address_item + 1

            address_item = address_item + (int(structure[i+1+k][2])-1) * (module_add_space[next_module_name])

            vhdl_file.write("\n")


        # update the address_item with the offset place spaces
        address_item = address_item + (int(structure[i][2])-1) * (offset_space)


vhdl_file.write("\n")

# write address table of the second level
vhdl_file.write("-- ************************************************************************** ")
vhdl_file.write("\n")
vhdl_file.write("-- Declare the items of the second level (Which will be used in the VHDL files ")
vhdl_file.write("\n")
vhdl_file.write("-- ************************************************************************** ")
vhdl_file.write("\n")

for i in range(0, structure_line):
    # each second level satrt with address 0 the offset will place the item in the tree
    second_level_address_item = 0

    module_name = structure[i][1]

    if structure[i][0] == "++":
        check_already_done = "false"
        for k in range(0, i-1):
            if structure[k][1] == module_name:
                check_already_done = "true"

        if check_already_done == "false":
            for j in range(nb_item_per_module[module_name_place[module_name]]):

                if (modules[module_name_place[module_name]][j][0] == modules[module_name_place[module_name]][j][1]):
                    item = '{:60s}'.format(modules[module_name_place[module_name]][j][0])
                    wr_line = "    constant "+item+"                               : integer := 16#" + '%0*x' % (4, second_level_address_item) + "#;"
                    vhdl_file.write(wr_line)
                    vhdl_file.write("\n")
                    second_level_address_item = second_level_address_item + 1

        vhdl_file.write("\n")

vhdl_file.write("end package address_table;\n")
vhdl_file.close()

print("VHDL address_table_" + BOARDNAME + ".vhd file done")
#-------------------------------------------------------------------------------





#-------------------------------------------------------------------------------
#
# Create the Files
#   HAL file
#   Create a Offset json file used nyt the python file
#
#-------------------------------------------------------------------------------
# numbers used to create offset number of the same Module
address_item  = 0               # incr at each item
conf_add_item = 0
bigger_offset = 0

hal_file = open(f"{ADDRPATH}/address_map_{BOARDNAME}_{BOARDTYPE}.dat", "w")
hal_file.write("****************************************************************************************************************************\n")
hal_file.write("****************************************************************************************************************************\n")
hal_file.write("* VCU128 address map                                                                                                                                                                                                             \n")
hal_file.write("****************************************************************************************************************************\n")
hal_file.write("*  item                                 accessmode      bar     address                     mask        read    write   description******\n")
hal_file.write("****************************************************************************************************************************\n")
hal_file.write("*       \n")

# write the address_table_version first in the JSON File
offset_json_file = open(f"{ADDRPATH}/address_map_{BOARDNAME}_{BOARDTYPE}.json", "w")
offset_json_file.write("{ \n")

offset_json_file.write("\"AddressTableVersion\"   : [ ")
offset_json_file.write(str(atv_value))
offset_json_file.write(" ],")
offset_json_file.write("\n")

# iterate all modules in primary level
for i in range(0, structure_line):

    if structure[i][0] == "0": # PCI configuration

        module_name_i = structure[i][1]

        # write PCIe configuration of the card (if need)
        hal_file.write("*-------------------------------*\n")
        hal_file.write("*-- PCI configuration  \n")
        hal_file.write("*-------------------------------*\n")
        hal_file.write("* \n")

        # Write all configuration items
        for j in range(nb_item_per_module[module_name_place[module_name_i]]):
            if (modules[module_name_place[module_name_i]][j][0] == modules[module_name_place[module_name_i]][j][1]):
                item = '{:100s}'.format(modules[module_name_place[module_name_i]][j][1])
                hal_wr_line = item + " configuration    " + '%0*X' % (8, conf_add_item) + "   " + modules[module_name_place[module_name_i]][j][2][9:17]

                if   modules[module_name_place[module_name_i]][j][3] == "rw": hal_wr_line = hal_wr_line + "  1   1  "
                elif modules[module_name_place[module_name_i]][j][3] == "r" : hal_wr_line = hal_wr_line + "  1   0  "
                elif modules[module_name_place[module_name_i]][j][3] == "w" : hal_wr_line = hal_wr_line + "  0   1  "
                else                                                        : hal_wr_line = hal_wr_line + "  1   1  "

                if   len(modules[module_name_place[module_name_i]][j]) > 4  : hal_wr_line = hal_wr_line + "   " + ' '.join(modules[module_name_place[module_name_i]][j][4:])

                hal_file.write(hal_wr_line)
                hal_file.write("\n")
                local_address = conf_add_item
                conf_add_item = conf_add_item + 4


        hal_file.write("*\n")

    elif structure[i][0] == "+":

        # new module
        module_name_i = structure[i][1]

        # calculate the offset
        offset_space  = module_add_space[module_name_i]*8       # Space per offset

        # compute the complete space (primary and secondary level
        if structure[i][3] != 0:                                # check if a secondary level is associated to this primary level
            for j in range(structure[j][3]):
                # space of module on next line of structure * number of module
                offset_space = offset_space + (module_add_space[structure[i+1+j][1]] * 8 * int(structure[i+1+j][2]))


        #-----------------------------------------------------------------------
        # FIRST LEVEL
        #-----------------------------------------------------------------------
        # write the address table for HAL
        # write as comment (the item_offset is not supported)
        hal_file.write("*-------------------------------*\n")
        hal_file.write("*-- Module name: "+module_name_i+"\n")
        hal_file.write("*-------------------------------*\n")
        hal_wr_line = "* offset list : "
        for j in range(int(structure[i][2])):
            hal_wr_line = hal_wr_line + hex(offset_space*j) + " ; "

        bigger_offset = bigger_offset + ((offset_space * int(structure[i][2])))

        hal_wr_line = hal_wr_line + "\n"
        hal_file.write(hal_wr_line)
        hal_file.write("* \n")

        # create the Offset list of the Item
        offwr_line = ("\""+module_name_i+"\"  : [")
        for j in range(int(structure[i][2])):
                offwr_line = offwr_line + str(offset_space*j)+" , "

        offwr_line = offwr_line + ("0],\n") # Close the line
        offset_json_file.write(offwr_line)

        # write all ITEMS in the HAL file of the primary module
        for j in range(nb_item_per_module[module_name_place[module_name_i]]):
            if (modules[module_name_place[module_name_i]][j][0] == modules[module_name_place[module_name_i]][j][1]):
                item = '{:100s}'.format(modules[module_name_place[module_name_i]][j][0])
                hal_wr_line = item + "memory    0   " + '%0*X' % (8, address_item) + "   " + modules[module_name_place[module_name_i]][j][2][1:17]

                if   modules[module_name_place[module_name_i]][j][3] == "rw": hal_wr_line = hal_wr_line + "  1   1  "
                elif modules[module_name_place[module_name_i]][j][3] == "r" : hal_wr_line = hal_wr_line + "  1   0  "
                elif modules[module_name_place[module_name_i]][j][3] == "w" : hal_wr_line = hal_wr_line + "  0   1  "
                else                                                        : hal_wr_line = hal_wr_line + "  1   1  "

                if   len(modules[module_name_place[module_name_i]][j]) > 4  : hal_wr_line = hal_wr_line + "   " + ' '.join(modules[module_name_place[module_name_i]][j][4:])

                hal_file.write(hal_wr_line)
                hal_file.write("\n")
                local_address = address_item
                address_item = address_item + 8

            if (modules[module_name_place[module_name_i]][j][0] != modules[module_name_place[module_name_i]][j][1]):
                item = '{:100s}'.format(modules[module_name_place[module_name_i]][j][0]+"_"+modules[module_name_place[module_name_i]][j][1])
                hal_wr_line = item + "memory    0   " + '%0*X' % (8, local_address) + "   " + modules[module_name_place[module_name_i]][j][2][1:17]

                if   modules[module_name_place[module_name_i]][j][3] == "rw": hal_wr_line = hal_wr_line + "  1   1  "
                elif modules[module_name_place[module_name_i]][j][3] == "r" : hal_wr_line = hal_wr_line + "  1   0  "
                elif modules[module_name_place[module_name_i]][j][3] == "w" : hal_wr_line = hal_wr_line + "  0   1  "
                else                                                        : hal_wr_line = hal_wr_line + "  1   1  "

                if   len(modules[module_name_place[module_name_i]][j]) > 4  : hal_wr_line = hal_wr_line + "   " + ' '.join(modules[module_name_place[module_name_i]][j][4:])

                hal_file.write(hal_wr_line)
                hal_file.write("\n")

        hal_file.write("*\n")
        #-----------------------------------------------------------------------



        #-----------------------------------------------------------------------
        # SECOND LEVEL
        #-----------------------------------------------------------------------
        # Write the items of the second level
        second_level_address_item = 0

        for k in range(structure[i][3]):
            next_module_name = structure[i+1+k][1]

            second_offset_space = module_add_space[next_module_name]*8             # Space per offset

            # write the address table for HAL
            # write as comment (the item_Offset is not supported
            hal_file.write("*-------------------------------*\n")
            hal_file.write("*-- *\n")
            hal_file.write("*-- Module name: "+next_module_name+"\n")
            hal_file.write("* This is a Module at the second level, the offset of the first level coresponding should be added !*\n")
            hal_file.write("*-------------------------------*\n")
            hal_wr_line = "* offset list : "
            for j in range(int(structure[i+1+k][2])):
                hal_wr_line = hal_wr_line + hex((second_offset_space*j)+address_item) + " ; "

            hal_wr_line = hal_wr_line + "\n"
            hal_file.write(hal_wr_line)
            hal_file.write("* \n")

            # create the Offset list of the Item
            offwr_line = ("\""+module_name_i+"_"+next_module_name+"\"  : [")
            for j in range(int(structure[i][2])):
                offwr_line = offwr_line + str((second_offset_space*j)+address_item) + " , "

            offwr_line = offwr_line + ("0],\n") # Close the line
            offset_json_file.write(offwr_line)


            # write all ITEMS in the HAL file of the primary module
            for j in range(nb_item_per_module[module_name_place[next_module_name]]):
                if ( modules[module_name_place[next_module_name]][j][0] == modules[module_name_place[next_module_name]][j][1]):
                    item = '{:100s}'.format(module_name_i + "_" + modules[module_name_place[next_module_name]][j][0])
                    hal_wr_line = item + "memory    0   " + '%0*X' % (8, second_level_address_item) + "   " + modules[module_name_place[next_module_name]][j][2][1:17]

                    if   modules[module_name_place[next_module_name]][j][3] == "rw": hal_wr_line = hal_wr_line + "  1   1  "
                    elif modules[module_name_place[next_module_name]][j][3] == "r" : hal_wr_line = hal_wr_line + "  1   0  "
                    elif modules[module_name_place[next_module_name]][j][3] == "w" : hal_wr_line = hal_wr_line + "  0   1  "
                    else                                                           : hal_wr_line = hal_wr_line + "  1   1  "

                    if len(modules[module_name_place[next_module_name]][j]) > 4    : hal_wr_line = hal_wr_line + "   " + ' '.join(modules[module_name_place[next_module_name]][j][4:])

                    hal_file.write(hal_wr_line)
                    hal_file.write("\n")
                    local_address             = second_level_address_item
                    address_item              = address_item + 8
                    second_level_address_item = second_level_address_item + 8

                if (modules[module_name_place[next_module_name]][j][0] != modules[module_name_place[next_module_name]][j][1]):
                    item = '{:102s}'.format(module_name_i + "_" + modules[module_name_place[next_module_name]][j][0] + "_" + modules[module_name_place[next_module_name]][j][1])
                    hal_wr_line = item + "memory    0   " + '%0*X' % (8, local_address) + "   " + modules[module_name_place[next_module_name]][j][2][1:17]

                    if   modules[module_name_place[next_module_name]][j][3] == "rw": hal_wr_line = hal_wr_line + "  1   1"
                    elif modules[module_name_place[next_module_name]][j][3] == "r" : hal_wr_line = hal_wr_line + "  1   0"
                    elif modules[module_name_place[next_module_name]][j][3] == "w" : hal_wr_line = hal_wr_line + "  0   1"
                    else                                                           : hal_wr_line = hal_wr_line + "  1   1"

                    if len(modules[module_name_place[next_module_name]][j]) > 4    : hal_wr_line = hal_wr_line + "   " + ' '.join(modules[module_name_place[next_module_name]][j][4:])

                    hal_file.write(hal_wr_line)
                    hal_file.write("\n")

            address_item = address_item + (int(structure[i+1+k][2])-1) * (module_add_space[next_module_name]) * 8



        # update the address_item with the offset place spaces
        address_item = address_item + (int(structure[i][2])-1) * (offset_space)

# write the final address of the table
hal_file.write("***************\n")
hal_file.write("* Bigest offset\n")
hal_file.write('{:100s}'.format("Max_address_item") + "memory    0   " + '%0*X' % (8, bigger_offset + 0x10) + "   ffffffffffffffff  1   1\n")

# finish the offset json file
offset_json_file.write("\"Nul_Offset\" : [0] \n")
offset_json_file.write("} \n")


# Close the HAL address map file
hal_file.close()
print("HAL address_table_" + BOARDNAME + ".dat file done")
