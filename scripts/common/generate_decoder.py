#!/usr/bin/env python3

import click
import json

@click.command()
@click.argument('addrtab_json', type=click.File())
@click.argument('decoder_vhdl', type=click.File('w'))
@click.argument('board_type', type=click.Choice(["ugmt", "bril", "demux", "bmtftcp"], case_sensitive=False))
@click.argument('board_name', type=click.Choice(["kcu1500", "sb852", "vcu128"], case_sensitive=False), default="kcu1500")
def main(addrtab_json, decoder_vhdl, board_type, board_name):
    addrtab_choice = board_name + '_' + board_type
    addrtab = json.load(addrtab_json)
    dump_vhdl_header(decoder_vhdl)
    for reg_name, reg_info in addrtab[addrtab_choice]['registers'].items():
        dump_register_dict_to_vhdl(decoder_vhdl, reg_name, reg_info, board_name)
    dump_vhdl_trailer(decoder_vhdl)

def dump_register_dict_to_vhdl(decoder_vhdl, reg_name, reg_info, board_name):
    if board_name == "sb852": # except for the zs_thresholds, offset and width are not used for sb852 as everything is treated as a 32b word
        dump_address_sb852(decoder_vhdl, reg_name, reg_info['address'], reg_info['writable'])
    else:
        dump_address_kcu1500(decoder_vhdl, reg_name, reg_info['address'], reg_info['writable'])
    dump_vhdl_const(decoder_vhdl, reg_name, 'offset', reg_info['offset'])
    dump_vhdl_const(decoder_vhdl, reg_name, 'width', reg_info['width'])

def dump_address_kcu1500(decoder_vhdl, reg_name, addr_value, writable):
    addr_value_int = int(addr_value, 16)
    if not writable:
        addr_value_int -= 0x200  # Using 8 bit wide register address, so full AXI address is 10 bits due to byte addressing. Therefore need 2**9 here.
    addr_value_int //= 4
    dump_vhdl_const(decoder_vhdl, reg_name, 'address', addr_value_int)

# sb852 addresses are handled differently, are multiples of 4 and are continuous with read/write and read only
def dump_address_sb852(decoder_vhdl, reg_name, addr_value, writable):
    addr_value_int = int(addr_value, 16)
    dump_vhdl_const(decoder_vhdl, reg_name, 'address', addr_value_int)

def dump_vhdl_const(decoder_vhdl, reg_name, reg_field_name, reg_field_value):
    const_name = f'{reg_name}_{reg_field_name}'
    decoder_vhdl.write(f'\tconstant {const_name:<40} : natural := {reg_field_value};\n')

def dump_vhdl_header(decoder_vhdl):
    decoder_vhdl.write('''library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package scouting_register_constants is\n''')

def dump_vhdl_trailer(decoder_vhdl):
    decoder_vhdl.write('end scouting_register_constants;\n')


if __name__ == "__main__":
    main()
