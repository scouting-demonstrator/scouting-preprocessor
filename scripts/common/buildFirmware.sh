#!/bin/bash
set -e # Exit on error.
if [ -f buildToolSetup.sh ] ; then
    source buildToolSetup.sh
fi
if [ -z ${XILINX_VIVADO:+x} ] ; then
    echo "Xilinx Vivado environment has not been sourced. Exiting."
    exit 1
else
    echo "Found Xilinx Vivado at" ${XILINX_VIVADO}
fi

cd $BUILD_DIR
cd scouting/proj/scouting_build/
ipbb vivado synth -j4 impl -j4
ipbb vivado bitfile package
