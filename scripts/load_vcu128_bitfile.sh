#!/usr/bin/env bash

set -e # Exit on error

if [ $# -lt 4 ]; then
    echo "#### Usage:"
    echo "## " "$0" "board_name board_idx bitfile_repo_path isFirstLoad [vivado_edition]"
    echo "# Example " "$0" "vcu128_bmtf 0 /opt/l1scouting-hardware/bitfiles 1 vivado_lab"
    echo "## Exiting.."
    exit 1
elif [ $# == 4 ]; then
    echo 'vivado_edition not specified, defaulting to Vivado_lab'
    VIVADO_EDITION="vivado_lab"
else
    VIVADO_EDITION=$5
fi
BOARD=$1
BOARD_INDEX=$2
bitfile_repo_path=$3
ISFIRSTLOAD=$4

load_bitfile (){
    $VIVADO_EDITION -mode batch -source "${bitfile_repo_path}"/currently_used/"${BOARD_INDEX}"/vivado_load_bitfile.tcl -tclargs ${BOARD_INDEX}
}

sudo service scone stop
if [ ${ISFIRSTLOAD} == 0 ]; then
    echo "Reading PCIe configuration registers"
    regs=$(/usr/sbin/pcie_reconnect_hal.sh ${bitfile_repo_path}/currently_used/${BOARD_INDEX}/address_map_${BOARD}.dat 0x10dc 0x01b5 ${BOARD_INDEX} read)
    echo "Now reloading bitfile, this could take a moment.."
else
    echo "Now loading bitfile for the first time, this could take a moment.."
fi
# We replace the bitfile repo path here in a slightly convoluted way:
# As the directory isn't usually writable by our user (and shouldn't be!) we can't do a simple sed even though the tcl file is writable.
# Instead we use a temporary file and then copy it over to the correct location.
sed "s,#BITFILE_REPO_PATH#,${bitfile_repo_path},g" "${bitfile_repo_path}"/currently_used/"${BOARD_INDEX}"/vivado_load_bitfile.tcl | sed "s,#BOARD_INDEX#,${BOARD_INDEX},g" > /tmp/vivado_load_bitfile.tcl && cat /tmp/vivado_load_bitfile.tcl | sudo tee "${bitfile_repo_path}"/currently_used/"${BOARD_INDEX}"/vivado_load_bitfile.tcl
load_bitfile
if [ ${ISFIRSTLOAD} == 0 ]; then
    # Rescan PCIe bus
    echo "Writing back PCIe configuration registers"
    /usr/sbin/pcie_reconnect_hal.sh ${bitfile_repo_path}/currently_used/${BOARD_INDEX}/address_map_${BOARD}.dat 0x10dc 0x01b5 ${BOARD_INDEX} write "$regs"
fi
sudo service scone start

echo "load bitfile script completed"
