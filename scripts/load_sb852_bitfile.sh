#!/usr/bin/env bash

set -e # Exit on error

if [ $# -lt 1 ]; then
    echo "#### Usage:"
    echo "## " "$0" "bitfile_repo_path"
    echo "# Example " "$0" "/opt/l1scouting-hardware/bitfiles"
    echo "## Exiting.."
    exit 1
fi
bitfile_repo_path=$2

/usr/lib/MicronBitfileLoader ${bitfile_repo_path}/currently_used/scouting_build.tgz

echo "load bitfile script completed"
