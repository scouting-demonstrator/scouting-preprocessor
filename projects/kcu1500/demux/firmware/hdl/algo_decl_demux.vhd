-- algo_decl for Layer-2 Demux scouting
--
-- Constants for the whole device
--
-- D. R. May 2018

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package algo_decl is

    constant FW_TYPE : std_logic_vector(7 downto 0) := X"02"; -- Identify the board (0x2 is Demux scouting).

    constant ZS_TYPE : string := "DEMUX";
    
    constant BRIL : boolean := false;

end algo_decl;
