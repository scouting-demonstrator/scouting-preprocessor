-- top_decl_common
--
-- Constants for the whole device
--
-- D. R. May 2018

library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.algo_decl.all;

package top_decl is

    constant ALGO_REV        : std_logic_vector(23 downto 0) := X"03" & X"00" & X"01"; -- Major, minor, patch.
    constant LHC_BUNCH_COUNT : integer                       := 3564;
    constant N_REGION        : positive                      := 2;

    constant ORBIT_LENGTH                        : natural := 3564;
    constant BGO_WARNING_TEST_ENABLE             : natural := 31;   -- Position of the WarningTestEnable BGo within the MP7 link header.
    constant LAST_BX_BEFORE_CALIBRATION_SEQUENCE : natural := 3282; -- TestEnable BGo comes in 3283 as of which time only calibration triggers are sent out. Actual calibration L1A is in 3490.
    constant SIM   : boolean := false;
    constant BOARD : string := "KCU1500";
    constant full_register_address_width : natural := 8;
end top_decl;
