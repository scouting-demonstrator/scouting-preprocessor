-- algo_decl for BRIL scouting
--
-- Constants for the whole device
--
-- D. R. May 2018

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package algo_decl is

    constant FW_TYPE : std_logic_vector(7 downto 0) := X"03"; -- Identify the board (0x3 is BRIL scouting).

    constant ZS_TYPE : string := "UGMT";
    
    constant BRIL : boolean := true;
end algo_decl;
