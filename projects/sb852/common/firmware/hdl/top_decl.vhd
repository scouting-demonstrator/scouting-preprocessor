-- top_decl
--
-- Constants for the whole device
--
-- D. R. May 2018

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.algo_decl.all;
package top_decl is

--The eight MSBs indicate the scouting board type where the following codes are currently in use:
--0x0 ... Prototype board until late 2021
--0x1 ... uGMT inputs
--0x2 ... Layer-2 Demux inputs
--0x5 ... uGMT inputs, bril histogramming enabled

    constant ALGO_REV        : std_logic_vector(23 downto 0) := X"03" & X"00" & X"01"; -- First octed identifies the board, then major, minor, patch.
    constant FW_REV          : std_logic_vector(31 downto 0) := FW_TYPE & ALGO_REV;
    constant LHC_BUNCH_COUNT : integer                       := 3564;
    constant N_REGION        : positive                      := 2;

    constant ORBIT_LENGTH : natural := 3564;
    constant BGO_WARNING_TEST_ENABLE             : natural := 31;   -- Position of the WarningTestEnable BGo within the MP7 link header.
    constant LAST_BX_BEFORE_CALIBRATION_SEQUENCE : natural := 3282; -- TestEnable BGo comes in 3283 as of which time only calibration triggers are sent out. Actual calibration L1A is in 3490.
    constant SIM   : boolean := false;
    constant MDLA  : boolean := false;
    constant BOARD : string  := "SB852";
    constant full_register_address_width : natural := 12;
    constant special_addr_boundry : natural := 292;
end top_decl;
