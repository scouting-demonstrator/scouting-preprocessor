-- algo_decl for uGT scouting
--
-- Constants for the whole device
--
-- R. Ardino September 2022

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package algo_decl is

    constant FW_TYPE  : std_logic_vector(7 downto 0) := X"12"; -- Identify the board (0x20 is bmtf scouting)

    constant N_REGION : positive := 7;
    constant N_STREAM : positive := 24;

end algo_decl;
