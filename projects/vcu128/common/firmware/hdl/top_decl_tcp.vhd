-- top_decl
--
-- Constants for the whole device
--
-- R. Ardino September 2022

library ieee;
use ieee.std_logic_1164.all;

use work.algo_decl.all;

package top_decl is

--The eight MSBs indicate the scouting board type where the following codes are currently in use:
--0x00 ... Prototype board until late 2021
--0x01 ... uGMT inputs
--0x02 ... Layer-2 Demux inputs
--0x05 ... uGMT inputs, bril histogramming enabled
--0x10 ... uGT decision bits
--0x20 ... BMTF stub inputs

    constant ALGO_REV        : std_logic_vector(23 downto 0) := X"01" & X"00" & X"00"; -- major, minor, patch.
    constant FW_REV          : std_logic_vector(31 downto 0) := FW_TYPE & ALGO_REV;
    constant LHC_BUNCH_COUNT : integer                       := 3564;

    constant ORBIT_LENGTH                        : natural := 3564;
    constant BGO_WARNING_TEST_ENABLE             : natural := 31;   -- Position of the WarningTestEnable BGo within the MP7 link header.
    constant LAST_BX_BEFORE_CALIBRATION_SEQUENCE : natural := 3282; -- TestEnable BGo comes in 3283 as of which time only calibration triggers are sent out. Actual calibration L1A is in 3490.
    constant SIM                                 : boolean := false;
    constant BOARD                               : string := "VCU128";
    constant full_register_address_width         : natural := 8;

end top_decl;
